﻿Imports Interop.StdBE900
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle

Public Class FrmTerminarRP

    Const MSGERRO1 As String = "O tempo despendido não se encontra preenchido"
    Const MSGERRO2 As String = "Não é possivel introduzir tempo, somente pode introduzir tempos até ao dia ##DIA##"
    Const MSGPERGUNTA1 As String = "Deseja realmente remover o registo?"
    Dim limpar As Boolean


    Dim idRegisto_ As String
    Property idRegisto() As String
        Get
            idRegisto = idRegisto_
        End Get
        Set(ByVal value As String)
            idRegisto_ = value
        End Set
    End Property

    Dim funcionario_ As String
    Property Funcionario() As String
        Get
            Funcionario = funcionario_
        End Get
        Set(ByVal value As String)
            funcionario_ = value
        End Set
    End Property


    Dim centroTrabalho_ As List(Of String)
    Property CentroTrabalho() As List(Of String)
        Get
            CentroTrabalho = centroTrabalho_
        End Get
        Set(ByVal value As List(Of String))
            centroTrabalho_ = value
        End Set
    End Property

    Dim artigo_ As List(Of String)
    Property Artigo() As List(Of String)
        Get
            Artigo = artigo_
        End Get
        Set(ByVal value As List(Of String))
            artigo_ = value
        End Set
    End Property

    Dim molde_ As String
    Property Molde() As String
        Get
            Molde = molde_
        End Get
        Set(ByVal value As String)
            molde_ = value
        End Set
    End Property

    Dim peca_ As String
    Property Peca() As String
        Get
            Peca = peca_
        End Get
        Set(ByVal value As String)
            peca_ = value
        End Set
    End Property


    Dim operacao_ As String
    Property Operacao() As String
        Get
            Operacao = operacao_
        End Get
        Set(ByVal value As String)
            operacao_ = value
        End Set
    End Property


    Dim dataInicio_ As DateTime
    Property DataInicio() As DateTime
        Get
            DataInicio = dataInicio_
        End Get
        Set(ByVal value As DateTime)
            dataInicio_ = value
        End Set
    End Property


    Dim dataFim_ As DateTime
    Property DataFim() As DateTime
        Get
            DataFim = dataFim_
        End Get
        Set(ByVal value As DateTime)
            dataFim_ = value
        End Set
    End Property

    Dim idLinhas_ As List(Of String)
    Property idLinhas() As List(Of String)
        Get
            idLinhas = idLinhas_
        End Get
        Set(ByVal value As List(Of String))
            idLinhas_ = value
        End Set
    End Property


    Dim tempo_ As Double = 0
    Property Tempo() As Double
        Get
            Tempo = tempo_
        End Get
        Set(ByVal value As Double)
            tempo_ = value
        End Set
    End Property

    Dim estado_ As String
    Property Estado() As String
        Get
            Estado = estado_
        End Get
        Set(ByVal value As String)
            estado_ = value
        End Set
    End Property

    Dim observacoes_ As String
    Property Observacoes() As String
        Get
            Observacoes = observacoes_
        End Get
        Set(ByVal value As String)
            observacoes_ = value
        End Set
    End Property

    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Dim mensagemDocumento_ As String
    ReadOnly Property MensagemDocumento() As String
        Get
            MensagemDocumento = mensagemDocumento_
        End Get

    End Property

    Private Sub FrmTerminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        botaoSeleccionado_ = 3
        limpar = True
        iniciarForm()
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"

            Control = .Add(xtpControlButton, 2, " Remover", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Remover"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    Private Sub iniciarForm()
        mensagemDocumento_ = ""
        iniciarToolBox()
        If dataInicio_ = "#12:00:00 AM#" Then
            dataInicio_ = Now
        End If
        If dataFim_ = "#12:00:00 AM#" Then
            dataFim_ = Now 'DateAdd(DateInterval.Minute, 1, dataInicio_)
        End If

        dtpData.Value = CDate(dataInicio_)
        ' If DateDiff(DateInterval.Minute, dataInicio_, dataFim_) < 50 Then

        If tempo_ <> 0 Then
            txtTempo.Text = CStr(tempo_)
        Else
            txtTempo.Text = CStr(calcularTempo())
        End If
        txtTempo.Focus()

        Dim m As MotorRP
        Dim lista As StdBELista
        Dim dt As DataTable
        m = MotorRP.GetInstance
        lista = m.daListaEstadosTrabalho()
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        cmbEstado.DataSource = dt
        cmbEstado.DisplayMember = "Descricao"
        cmbEstado.ValueMember = "Descricao"

        If estado_ <> "" Then
            cmbEstado.Text = estado_
        End If

        txtObservacoes.Text = observacoes_


    End Sub

    'funcao para calcula a quantidade com base na mao de obra
    Private Function calcularTempo() As Double
        Dim quant As Double
        Dim minutos As Long
        Dim tempoMinimo As Double
        Dim strTempo As String

        Dim m As MotorRP

        m = MotorRP.GetInstance
        strTempo = m.daValorStringIni("DOCUMENTORP", "TEMPO", "0,25")
        If IsNumeric(strTempo) Then
            tempoMinimo = CDbl(strTempo)
        Else
            tempoMinimo = 0.25
        End If
        'devolve a diferenca em minutos
        minutos = DateDiff("n", dataInicio_, dataFim_, vbMonday, vbUseSystem)
        If minutos <= 15 Then
            quant = 1
        Else
            quant = minutos / 15
            If quant > Math.Round(quant) Then
                quant = Math.Round(quant) + 1
            Else
                quant = Math.Round(quant)
            End If
        End If

        calcularTempo = quant * tempoMinimo
    End Function

    'Private Sub txtTempo_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtTempo.Validating

    '    e.Cancel = IsNumeric(txtTempo.Text)
    'End Sub



    Private Sub colocarTempo(ByVal valor As String)
        If limpar Then
            txtTempo.Text = ""
            limpar = False
        End If
        If valor = "" Then
            txtTempo.Text = ""
        Else
            txtTempo.Text = txtTempo.Text + valor
        End If
    End Sub



    Private Sub habilitarBotoes(ByVal b As Boolean)
        btn1.Enabled = b
        If b = True Then btn2.Enabled = b
        '   btn2.Enabled = b
        btn3.Enabled = b
        btn4.Enabled = b
        If b = True Then btn5.Enabled = b
        btn6.Enabled = b
        If b = True Then btn7.Enabled = b
        btn8.Enabled = b
        btn9.Enabled = b
        btn0.Enabled = b
        btnP.Enabled = b
    End Sub

    Private Sub btnP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnP.Click
        habilitarBotoes(False)
        colocarTempo(",")
    End Sub

    Private Sub btnC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnC.Click
        colocarTempo("")
        habilitarBotoes(True)
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        colocarTempo(btn1.Tag)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        colocarTempo(btn2.Tag)
        If btn0.Enabled = False Then
            btn2.Enabled = False
            btn7.Enabled = False
        End If
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        colocarTempo(btn3.Tag)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        colocarTempo(btn4.Tag)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        colocarTempo(btn5.Tag)
        If btn0.Enabled = False Then
            btn2.Enabled = False
            btn5.Enabled = False
        End If
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        colocarTempo(btn6.Tag)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        colocarTempo(btn7.Tag)
        If btn0.Enabled = False Then
            btn2.Enabled = False
            btn7.Enabled = False
        End If
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        colocarTempo(btn8.Tag)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        colocarTempo(btn9.Tag)
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        colocarTempo(btn0.Tag)
    End Sub


    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        botaoSeleccionado_ = e.control.Index

        ' terminar o registo
        If e.control.Index = 1 Then
            terminarServico()
        End If

        'remover o registo
        If e.control.Index = 2 Then
            If MsgBox(MSGPERGUNTA1, MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
            Dim m As MotorRP
            m = MotorRP.GetInstance
            m.removerRegisto(funcionario_, artigo_, molde_, peca_, operacao_, idLinhas_)
            '  clicouConfirmar_ = True
            MsgBox("Registo removido com sucesso", MsgBoxStyle.Information)
            Me.Close()
        End If

        'fechar o serviço
        If e.control.Index = 3 Then
            Me.Close()
        End If
    End Sub


    Private Function podeIntroduzirTempos(numdias As Integer, dt As DateTime, userAdmin As Boolean, ByRef msg As String) As Integer

        If userAdmin Or numdias = -1 Then
            Return True
        End If

        Dim dtBase As DateTime
        Dim dias As Long

        dtBase = Now

        dtBase = FormatDateTime(dtBase, DateFormat.ShortDate)

        dias = DateDiff(DateInterval.Day, dtBase, dt)

        If dias > 0 Then
            msg = "Atenção está a introduzir pontos com data superior à data actual"
            Return False
        End If

        If dtBase.DayOfWeek = DayOfWeek.Monday Then
            dtBase = dtBase.AddDays(-2)
        End If

        dtBase = FormatDateTime(dtBase.AddDays(-numdias), DateFormat.ShortDate)

        dias = DateDiff(DateInterval.Day, dt, dtBase)

        If dias <= 0 Then
            Return True
        Else
            msg = MSGERRO2
            msg = Replace(msg, "##DIA##", dtBase.ToShortDateString)
            Return False
        End If


    End Function

    ''' <summary>
    ''' função que permite o registo dos pontos no sistema
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub terminarServico()

        Dim m As MotorRP
        Dim msg As String
        m = MotorRP.GetInstance
        msg = ""
        Dim tempoPontos As Boolean

        tempoPontos = podeIntroduzirTempos(m.numDiasRegisto, FormatDateTime(dtpData.Value, DateFormat.ShortDate), m.getFuncionarioAdministrador, msg)

        If Not tempoPontos Then


            MsgBox(msg, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Dim idLinha As String
        If Not IsNumeric(txtTempo.Text) Then
            MsgBox(MSGERRO1, MsgBoxStyle.Exclamation)
        Else


            dataInicio_ = FormatDateTime(dtpData.Value, DateFormat.ShortDate) + " " + FormatDateTime(dataInicio_, DateFormat.ShortTime)
            dataFim_ = FormatDateTime(dtpData.Value, DateFormat.ShortDate) + " " + FormatDateTime(dataFim_, DateFormat.ShortTime)
            Dim i As Integer
            Dim art As String
            Dim ct As String
            Dim idPonto As String

            m.apagarLinhasDocumentosInternos(idLinhas)
            idPonto = m.consultaValor("SELECT NEWID()")
            For i = 0 To artigo_.Count - 1
                art = artigo_(i)
                ct = centroTrabalho_(i)

                idLinha = m.actualizarDocumentoInterno(funcionario_, art, molde_, peca_, operacao_, dtpData.Value, CDbl(txtTempo.Text), "", dataInicio_, dataFim_, cmbEstado.Text, txtObservacoes.Text, idPonto)
                If idLinha <> "" Then
                    'm.TerminarOperacaoFinal(centroTrabalho_, artigo_, molde_, peca_, operacao_, dataInicio_, dataFim_, CDbl(txtTempo.Text), cmbEstado.Text, txtObservacoes.Text, idRegisto_, idLinha_)
                    m.TerminarOperacaoFinal(ct, art, molde_, peca_, operacao_, dataInicio_, dataFim_, CDbl(txtTempo.Text), cmbEstado.Text, txtObservacoes.Text, idRegisto_, idLinha, idPonto)
                    gravarMensagem(idLinha)
                End If

            Next

            Me.Hide()
        End If
    End Sub


    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        botaoSeleccionado_ = 1
        terminarServico()
    End Sub

    Private Sub gravarMensagem(idLinha As String)
        Dim m As MotorRP
        m = MotorRP.GetInstance
        Dim lista As StdBELista
        lista = m.consulta("SELECT TipoDoc,Serie,Numdoc FROM CabecInternos WHERE id in (select idcabecinternos FROM LinhasInternos WHERE id='" + idLinha + "') ")

        If Not lista.NoFim Then
            mensagemDocumento_ = "Documento: " + lista.Valor("TipoDoc") + " / " + lista.Valor("Serie") + "/ " + CStr(lista.Valor("Numdoc"))
        End If
    End Sub
End Class