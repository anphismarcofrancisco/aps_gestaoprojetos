﻿Public Class FrmPergunta

    Dim texto As String
    Dim botaoSeleccionado As Integer

    Private Sub FrmPergunta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblPergunta.Text = texto
        botaoSeleccionado = -1
    End Sub

    Public Sub setTexto(ByVal texto As String)
        Me.texto = texto
    End Sub

    Public Function getBotaoSeleccionado() As Integer
        Return botaoSeleccionado
    End Function

    Private Sub btnGravar_Click(sender As Object, e As EventArgs) Handles btnGravar.Click
        botaoSeleccionado = 0
        Me.Hide()
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        botaoSeleccionado = 1
        Me.Hide()
    End Sub

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        botaoSeleccionado = 2
        Me.Hide()
    End Sub
End Class