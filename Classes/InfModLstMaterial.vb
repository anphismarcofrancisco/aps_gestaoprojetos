﻿Public Class InfModLstMaterial

    Public Sub New()

    End Sub

    Dim TIPODOCLM_ As String = ""
    Property TIPODOCLM() As String
        Get
            TIPODOCLM = TIPODOCLM_
        End Get
        Set(ByVal value As String)
            TIPODOCLM_ = value
        End Set
    End Property


    Dim SERIELM_ As String = ""
    Property SERIELM() As String
        Get
            SERIELM = SERIELM_
        End Get
        Set(ByVal value As String)
            SERIELM_ = value
        End Set
    End Property

    Dim MAPALM_ As String = ""
    Property MAPALM() As String
        Get
            MAPALM = MAPALM_
        End Get
        Set(ByVal value As String)
            MAPALM_ = value
        End Set
    End Property


    Dim TIPODOCCOT_ As String = ""
    Property TIPODOCCOT() As String
        Get
            TIPODOCCOT = TIPODOCCOT_
        End Get
        Set(ByVal value As String)
            TIPODOCCOT_ = value
        End Set
    End Property


    Dim SERIECOT_ As String = ""
    Property SERIECOT() As String
        Get
            SERIECOT = SERIECOT_
        End Get
        Set(ByVal value As String)
            SERIECOT_ = value
        End Set
    End Property


    Dim MAPACOT_ As String = ""
    Property MAPACOT() As String
        Get
            MAPAECF = MAPACOT_
        End Get
        Set(ByVal value As String)
            MAPACOT_ = value
        End Set

    End Property

    Dim TIPODOCECF_ As String = ""
    Property TIPODOCECF() As String
        Get
            TIPODOCECF = TIPODOCECF_
        End Get
        Set(ByVal value As String)
            TIPODOCECF_ = value
        End Set
    End Property


    Dim SERIEECF_ As String = ""
    Property SERIEECF() As String
        Get
            SERIEECF = SERIEECF_
        End Get
        Set(ByVal value As String)
            SERIEECF_ = value
        End Set
    End Property

    Dim MAPAECF_ As String = ""
    Property MAPAECF() As String
        Get
            MAPAECF = MAPAECF_
        End Get
        Set(ByVal value As String)
            MAPAECF_ = value
        End Set
    End Property



    Dim TIPODOCSTK_ As String = ""
    Property TIPODOCSTK() As String
        Get
            TIPODOCSTK = TIPODOCSTK_
        End Get
        Set(ByVal value As String)
            TIPODOCSTK_ = value
        End Set
    End Property


    Dim SERIESTK_ As String = ""
    Property SERIESTK() As String
        Get
            SERIESTK = SERIESTK_
        End Get
        Set(ByVal value As String)
            SERIESTK_ = value
        End Set
    End Property


    Dim TIPODOCALT_ As String = ""
    Property TIPODOCALT() As String
        Get
            TIPODOCALT = TIPODOCALT_
        End Get
        Set(ByVal value As String)
            TIPODOCALT_ = value
        End Set
    End Property


    Dim SERIEALT_ As String = ""
    Property SERIEALT() As String
        Get
            SERIEALT = SERIEALT_
        End Get
        Set(ByVal value As String)
            SERIEALT_ = value
        End Set

    End Property

    Dim ARTIGO_ As String = ""
    Property ARTIGO() As String
        Get
            ARTIGO = ARTIGO_
        End Get
        Set(ByVal value As String)
            ARTIGO_ = value
        End Set

    End Property



    Dim EXCEL_INICIO_ As String = ""
    Property EXCEL_INICIO() As String
        Get
            EXCEL_INICIO = EXCEL_INICIO_
        End Get
        Set(ByVal value As String)
            EXCEL_INICIO_ = value
        End Set

    End Property



    Dim EMAILLLM_ As String = ""
    Property EMAILLM() As String
        Get
            EMAILLM = EMAILLLM_
        End Get
        Set(ByVal value As String)
            EMAILLLM_ = value
        End Set

    End Property

    Dim EMAILCOT_ As String = ""
    Property EMAILCOT() As String
        Get
            EMAILCOT = EMAILCOT_
        End Get
        Set(ByVal value As String)
            EMAILCOT_ = value
        End Set

    End Property


    Dim EMAILECF_ As String = ""
    Property EMAILECF() As String
        Get
            EMAILECF = EMAILECF_
        End Get
        Set(ByVal value As String)
            EMAILECF_ = value
        End Set

    End Property



    Dim IMPRIMIEAOENVIAR_ As String = "0"
    Property IMPRIMIEAOENVIAR() As String
        Get
            IMPRIMIEAOENVIAR = IMPRIMIEAOENVIAR_
        End Get
        Set(ByVal value As String)
            IMPRIMIEAOENVIAR_ = value
        End Set

    End Property


    Dim CSVgerarFicheiro_ As Boolean
    Property CSVgerarFicheiro() As Boolean
        Get
            CSVgerarFicheiro = CSVgerarFicheiro_
        End Get
        Set(ByVal value As Boolean)
            CSVgerarFicheiro_ = value
        End Set

    End Property


    Dim CSVviewSQL_ As String
    Property CSVviewSQL() As String
        Get
            CSVviewSQL = CSVviewSQL_
        End Get
        Set(ByVal value As String)
            CSVviewSQL_ = value
        End Set

    End Property

    Dim CSVNomeFicheiro_ As String
    Property CSVNomeFicheiro() As String
        Get
            CSVNomeFicheiro = CSVNomeFicheiro_
        End Get
        Set(ByVal value As String)
            CSVNomeFicheiro_ = value
        End Set
    End Property


    Dim RefDocOrigLM_ As Boolean
    Property RefDocOrigLM() As Boolean
        Get
            RefDocOrigLM = RefDocOrigLM_
        End Get
        Set(ByVal value As Boolean)
            RefDocOrigLM_ = value
        End Set
    End Property

    Dim RefDocOrigCOT_ As Boolean
    Property RefDocOrigCOT() As Boolean
        Get
            RefDocOrigCOT = RefDocOrigCOT_
        End Get
        Set(ByVal value As Boolean)
            RefDocOrigCOT_ = value
        End Set
    End Property

    Dim RefDocOrigECF_ As Boolean
    Property RefDocOrigECF() As Boolean
        Get
            RefDocOrigECF = RefDocOrigECF_
        End Get
        Set(ByVal value As Boolean)
            RefDocOrigECF_ = value
        End Set
    End Property


    Dim somenteAdmin_ As Boolean
    Property somenteAdmin() As Boolean
        Get
            somenteAdmin = somenteAdmin_
        End Get
        Set(ByVal value As Boolean)
            somenteAdmin_ = value
        End Set
    End Property


    Dim nomeFicheiroPDFECF_ As String
    Property nomeFicheiroPDFECF() As String
        Get
            nomeFicheiroPDFECF = nomeFicheiroPDFECF_
        End Get
        Set(ByVal value As String)
            nomeFicheiroPDFECF_ = value
        End Set

    End Property

    Dim nomeFicheiroPDFCOT_ As String
    Property nomeFicheiroPDFCOT() As String
        Get
            nomeFicheiroPDFCOT = nomeFicheiroPDFCOT_
        End Get
        Set(ByVal value As String)
            nomeFicheiroPDFCOT_ = value
        End Set

    End Property

    Dim nomeFicheiroPDFLST_ As String
    Property nomeFicheiroPDFLST() As String
        Get
            nomeFicheiroPDFLST = nomeFicheiroPDFLST_
        End Get
        Set(ByVal value As String)
            nomeFicheiroPDFLST_ = value
        End Set

    End Property

    ''' <summary>
    ''' Permite indicar o index do campo Inicial da lista
    ''' </summary>
    Dim indexCampoInicial_ As Integer
    Property indexCampoInicial() As Integer
        Get
            indexCampoInicial = indexCampoInicial_
        End Get
        Set(ByVal value As Integer)
            indexCampoInicial_ = value
        End Set
    End Property


    ''' <summary>
    ''' Função que permite inverter as cores de encomendado/verificado
    ''' </summary>
    Dim corInvertida_ As Boolean
    Property corInvertida() As Boolean
        Get
            corInvertida = corInvertida_
        End Get
        Set(ByVal value As Boolean)
            corInvertida_ = value
        End Set
    End Property



    ''' <summary>
    ''' Função que permite inverter as cores de encomendado/verificado
    ''' </summary>
    Dim validaLinhasRemovidas_ As Boolean
    Property validaLinhasRemovidas() As Boolean
        Get
            validaLinhasRemovidas = validaLinhasRemovidas_
        End Get
        Set(ByVal value As Boolean)
            validaLinhasRemovidas_ = value
        End Set

    End Property

End Class
