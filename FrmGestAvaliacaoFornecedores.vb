﻿
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900
Imports AxXtremeReportControl
Imports System.ComponentModel
Imports Interop.StdPlatBS900
Imports PlatAPSNET

Public Class FrmGestAvaliacaoFornecedores


    Const COLUMN_TIPOFORN As Integer = 0
    Const COLUMN_FORNECEDOR As Integer = 1
    Const COLUMN_FORNECEDORCERTIFICADO As Integer = 2
    Const COLUMN_FORNECEDORDATACERTIFICACAO As Integer = 3
    Const COLUMN_FORNECEDORCONFORME As Integer = 4
    Const COLUMN_DIFERENCASDIAS As Integer = 5
    Const COLUMN_AVALIACAO_CERT As Integer = 6
    Const COLUMN_AVALIACAO_CONF As Integer = 7
    Const COLUMN_AVALIACAO_PRAZ As Integer = 8
    Const COLUMN_AVALIACAO_TJ As Integer = 9
    Const COLUMN_AVALIACAO_FINAL As Integer = 10
    Const COLUMN_NOTAS As Integer = 11


    Const COR_EDITAVEL As String = "cccccc"
    Const COR_NOTAA As String = "6495ed"
    Const COR_NOTAB As String = "48d1cc"
    Const COR_NOTAC As String = "605DCF"

    Const modulo As String = "C"


    Dim PlataformaPrimavera As StdPlatBS
    Dim objConfApl As StdBSConfApl
    Dim fileINI As String
    Dim ficheiroConfiguracao As Configuracao


    Private Sub FrmAvaliacaoFornecedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        iniciarToolBox()
        inicializarAno()


        gridRegistos.AutoColumnSizing = False
        gridRegistos.AutoColumnSizing = False
        gridRegistos.ShowGroupBox = True
        gridRegistos.ShowItemsInGroups = False
        gridRegistos.AutoColumnSizing = True
        gridRegistos.AllowEdit = True

        inserirColunasRegistos()

        actualizar()

    End Sub
    Private Sub inicializarAno()
        Dim ano As Double
        For ano = 2016 To Year(Now)
            lstAno.Items.Add(CStr(ano))
        Next
        lstAno.Text = Year(Now)
    End Sub
    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls

            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"
            Control.BeginGroup = True

            Control = .Add(xtpControlSplitButtonPopup, 2, " Actualizar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Actualizar"
            Control.BeginGroup = True

            Control = .Add(xtpControlButton, 3, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow



            Control = .Add(xtpControlButton, 41, " Verificar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Verificar"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 4, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With

    End Sub

    Private Sub inserirColunasRegistos()

        Dim Column As ReportColumn
        Column = inserirColuna(COLUMN_TIPOFORN, "Tipo Forn.", 80, False, True)
        Column = inserirColuna(COLUMN_FORNECEDOR, "Fornecedor", 300, False, True)
        Column = inserirColuna(COLUMN_FORNECEDORCERTIFICADO, "Forn. Certificado", 50, False, True)
        Column = inserirColuna(COLUMN_FORNECEDORDATACERTIFICACAO, "Data Certificado", 100, False, True)
        Column = inserirColuna(COLUMN_FORNECEDORCONFORME, "Forn. Conforme", 100, False, True)
        Column = inserirColuna(COLUMN_DIFERENCASDIAS, "Dif. Dias", 100, False, True)
        Column = inserirColuna(COLUMN_AVALIACAO_CERT, "P. Cert.", 50, True, True)
        Column = inserirColuna(COLUMN_AVALIACAO_CONF, "P Conf.", 50, True, True)
        Column = inserirColuna(COLUMN_AVALIACAO_PRAZ, "P Prazos", 50, True, True)
        Column = inserirColuna(COLUMN_AVALIACAO_TJ, "P TJ", 50, True, True)
        Column = inserirColuna(COLUMN_AVALIACAO_FINAL, "Av. Final", 50, False, True)
        Column = inserirColuna(COLUMN_NOTAS, "Notas", 300, True, True)
        Column.Alignment = xtpAlignmentRight


    End Sub

    Private Function inserirColuna(ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = gridRegistos.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter

        inserirColuna = Column

    End Function

    Private Sub CommandBarsFrame1_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case 1 : gravaravaliacoes()
            Case 2 : actualizar()
            Case 4 : Me.Close()
            Case 3 : imprimirDoc()
            Case 41 : conferirFornecedor()
        End Select
    End Sub

    Private Sub gravaresair()
        If MsgBox("Deseja gravar as últimas alterações?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Gravar") = MsgBoxResult.Yes Then
            gravaravaliacoes()
        End If


    End Sub

    ''' <summary>
    ''' Função para gravar todas as avaliações conferidas
    ''' </summary>
    Private Sub gravaravaliacoes()

        Dim linha As ReportRecord

        Dim dt As DataTable
        Dim m As Motor
        Dim consulta As String = ""
        Dim tipoForn As String
        Dim tipoFornDesc As String
        Dim nomeForn As String

        m = Motor.GetInstance()

        'Verificar se a grid tem registos
        If gridRegistos.Rows.Count > 0 Then

            'Por cada linha irá verificar se existe e realizará o update
            For Each linha In gridRegistos.Records


                tipoForn = linha.Item(COLUMN_TIPOFORN).Tag
                tipoFornDesc = linha.Item(COLUMN_TIPOFORN).Value
                nomeForn = linha.Item(COLUMN_FORNECEDOR).Tag

                consulta = "SELECT CDU_ID FROM TDU_AVALIACAOFORNECEDORES WHERE CDU_ENTIDADE='" + linha.Tag + "' AND CDU_ANO='" + lstAno.Text + "' and CDU_TIPOFORN='" + tipoForn + "'"

                dt = m.consultaDataTable(consulta)

                If dt.Rows.Count > 0 Then

                    'FAZ UPDATE
                    m.executarComando("UPDATE TDU_AVALIACAOFORNECEDORES SET CDU_FORNCONFORME='" + linha.Item(COLUMN_FORNECEDORCONFORME).Value.ToString + "',CDU_FORNCONFORMEA='" + linha.Item(COLUMN_AVALIACAO_CONF).Value.ToString + "',CDU_DIFDIAS='" + linha.Item(COLUMN_DIFERENCASDIAS).Value.ToString + "',CDU_DIFDIASA='" + linha.Item(COLUMN_AVALIACAO_PRAZ).Value.ToString + "',CDU_CERTIFICADOA='" + linha.Item(COLUMN_AVALIACAO_CERT).Value.ToString + "',CDU_AVALIACAO='" + linha.Item(COLUMN_AVALIACAO_TJ).Value.ToString + "',CDU_NOTAS='" + linha.Item(COLUMN_NOTAS).Value.ToString + "',CDU_AVALIACAOFINAL='" + linha.Item(COLUMN_AVALIACAO_FINAL).Value.ToString + "'   WHERE CDU_ENTIDADE='" + linha.Tag + "' AND CDU_ANO='" + lstAno.Text + "' and CDU_TipoForn='" + tipoForn + "'")

                Else
                    'FAZ INSERT
                    m.executarComando("INSERT INTO TDU_AVALIACAOFORNECEDORES(CDU_ENTIDADE,CDU_NOME,CDU_ANO,CDU_FORNCONFORME,CDU_FORNCONFORMEA,CDU_DIFDIAS,CDU_DIFDIASA,CDU_CERTIFICADOA,CDU_AVALIACAO,CDU_NOTAS,CDU_AVALIACAOFINAL,CDU_TIPOFORN,CDU_TIPOFORNDESC) VALUES ('" + linha.Tag + "','" + nomeForn + "','" + lstAno.Text + "','" + linha.Item(COLUMN_FORNECEDORCONFORME).Value.ToString + "','" + linha.Item(COLUMN_AVALIACAO_CONF).Value.ToString + "','" + linha.Item(COLUMN_DIFERENCASDIAS).Value.ToString + "','" + linha.Item(COLUMN_AVALIACAO_PRAZ).Value.ToString + "','" + linha.Item(COLUMN_AVALIACAO_CERT).Value.ToString + "','" + linha.Item(COLUMN_AVALIACAO_TJ).Value.ToString + "','" + linha.Item(COLUMN_NOTAS).Value.ToString + "','" + linha.Item(COLUMN_AVALIACAO_FINAL).Value.ToString + "','" + tipoForn + "','" + tipoFornDesc + "')")

                End If

            Next

            MsgBox("Alterações gravadas com sucesso.", MsgBoxStyle.Information)

        End If


    End Sub
    ''' <summary>
    ''' Imrimir mapa de avaliações
    ''' </summary>
    Private Sub imprimirDoc()
        Try
            imprimirMapa("TJ_AVFOR", "W", Motor.GetInstance.getEmpresa)
            'imprimirDetalhes("TJ_AVFOR", "W", Motor.GetInstance.getEmpresa)
        Catch ex As Exception
            MsgBox("Erro ao imprimir o mapa de Avaliação: " + ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub actualizar()
        Dim entidade As String
        entidade = txtEntidade.Text
        procurarFornecedores(entidade)
    End Sub


    'Private Sub procurarFornecedores1(entidade As String)
    '    Dim m As Motor
    '    Dim dt As DataTable
    '    Dim consulta As String
    '    Dim restricao As String
    '    Dim tipodoc As String

    '    restricao = "AND not LC.DataEntrega is null AND not LC.CDU_DataRecepcao  is null "
    '    m = Motor.GetInstance()
    '    tipodoc = "'" + m.consultaEntradaIniFile("AVALIACAOFORNECEDORES", "TIPODOCUMENTO") + "'"




    '    consulta = "  select TDU_TiposFornecedor.CDU_Descricao as CDU_TipoFornecedor, Fornecedor, Nome,CDU_Certificado,CDU_CertificadoData,
    '                   isnull((select avg(datediff(day,DataEntrega,CDU_DataRecepcao)*tdui.CDU_VALOR) from linhascompras LC inner join CabecCompras CD on lC.IdCabecCompras=CD.Id left join TDU_IMPACTO as tdui on lc.CDU_IMPACTO = tdui.CDU_IMPACTO WHERE tipodoc in (" + tipodoc + ") and serie='" + lstAno.Text + "' and F.Fornecedor=CD.Entidade " + restricao + " ),0) as CDU_Prazos,
    '                    isnull((select sum(case isnull(lc.cdu_conforme,0)  WHEN 1 THEN 1 ELSE 0 END) from linhascompras LC inner join CabecCompras CD on lC.IdCabecCompras=CD.Id WHERE tipodoc in (" + tipodoc + ")  and serie='" + lstAno.Text + "' and F.Fornecedor=CD.Entidade " + restricao + "  ),0)  as CDU_NTotalLinhasConf
    '                    ,isnull((select case count(*) when 0 then 0 Else count(*) END  from linhascompras LC inner join CabecCompras CD on lC.IdCabecCompras=CD.Id WHERE tipodoc in (" + tipodoc + ") and serie='" + lstAno.Text + "' and F.Fornecedor=CD.Entidade " + restricao + "  ),0) as CDU_NTotalLinhas
    '                     from Fornecedores F LEFT OUTER JOIN TDU_TiposFornecedor ON F.CDU_TipoFornecedor=TDU_TiposFornecedor.CDU_Tipo WHERE NOT F.CDU_TipoFornecedor is null "

    '    If entidade <> "" Then
    '        consulta += " AND F.Fornecedor='" + entidade + "' "
    '    End If
    '    dt = m.consultaDataTable(consulta)

    '    inserirColunasRegistos(dt)
    '    gridRegistos.SelectedRows.DeleteAll()
    'End Sub



    Private Sub procurarFornecedores(entidade As String)
        Dim m As Motor
        Dim dt As DataTable
        Dim consulta As String
        Dim restricao As String
        Dim tipodoc As String
        Dim empresas As String
        Dim listaempresas As String()

        restricao = "AND not LC.DataEntrega is null AND not LC.CDU_DataRecepcao  is null "
        m = Motor.GetInstance()
        tipodoc = "'" + m.consultaEntradaIniFile("AVALIACAOFORNECEDORES", "TIPODOCUMENTO") + "'"
        empresas = m.consultaEntradaIniFile("AVALIACAOFORNECEDORES", "EMPRESAS")
        listaempresas = empresas.Split(",")



        criarViewFornecedores(listaempresas)

        consulta = ""

        consulta += daConsultaDadosEmpresas(True, tipodoc, listaempresas)

        consulta += " UNION "

        consulta += daConsultaDadosEmpresas(False, tipodoc, listaempresas)


        consulta = "SELECT * FROM (" + consulta + ") as tmpQuery "

        If entidade <> "" Then
            consulta += " WHERE Fornecedor ='" + entidade + "' "
        End If


        consulta += " ORDER BY CDU_TipoFornecedor, Fornecedor"

        If empresas <> "" Then
            dt = m.consultaDataTable(consulta)
        End If
        'InputBox("", "", consulta)
        inserirColunasRegistos(dt)
        gridRegistos.SelectedRows.DeleteAll()
    End Sub

    Private Function daConsultaDadosEmpresas(servicoPrincipal As Boolean, tipoDoc As String, listaempresas As String()) As String
        Dim consulta As String
        Dim restricao As String
        Dim modo As String
        Dim m As Motor
        Dim prefixo As String


        m = Motor.GetInstance()


        If servicoPrincipal Then
            modo = "''"
            prefixo = "F."
        Else
            modo = "TF.cdu_tipoartigo"
            prefixo = "TF."
        End If

        consulta = "Select " + modo + " as TipoArtigo,  TDU_TIPOSFORNECEDOR.CDU_TIPO,TDU_TIPOSFORNECEDOR.CDU_DESCRICAO  As CDU_TipoFornecedor, F.Fornecedor, Nome," + prefixo + "CDU_Certificado," + prefixo + "CDU_CertificadoData,
                       isnull((select ("
        Dim empresa As String

        For i = 0 To listaempresas.Count - 1
            empresa = listaempresas(i)

            If servicoPrincipal Then
                restricao = "not Artigo.TipoArtigo in (select cdu_tipoartigo FROM PRI" + empresa + "..TDU_FORNECEDOR_TIPOSFORNECEDOR WHERE CDU_Fornecedor=F.Fornecedor and not cdu_tipoartigo is null ) "
            Else
                restricao = "Artigo.TipoArtigo=TF.cdu_tipoartigo"
            End If

            consulta += "(select isnull(avg(datediff(day,DataEntrega,CDU_DataRecepcao)*tdui.CDU_VALOR),0) from PRI" + empresa + "..linhascompras LC inner join PRI" + empresa + "..CabecCompras CD on lC.IdCabecCompras=CD.Id left join PRI" + empresa + "..TDU_IMPACTO as tdui on lc.CDU_IMPACTO = tdui.CDU_IMPACTO inner join PRI" + empresa + "..artigo on LC.Artigo=Artigo.Artigo  WHERE " + restricao + " and tipodoc in (" + tipoDoc + ") and serie='" + lstAno.Text + "' and F.Fornecedor=CD.Entidade AND not LC.DataEntrega is null AND not LC.CDU_DataRecepcao  is null)"
            If i <> listaempresas.Count - 1 Then
                consulta += " + "
            End If
        Next

        consulta += ")/" + listaempresas.Count.ToString() + "),0) as CDU_Prazos,"
        consulta += "isnull((select("


        For i = 0 To listaempresas.Count - 1
            empresa = listaempresas(i)

            If servicoPrincipal Then
                restricao = "not Artigo.TipoArtigo in (select cdu_tipoartigo FROM PRI" + empresa + "..TDU_FORNECEDOR_TIPOSFORNECEDOR WHERE CDU_Fornecedor=F.Fornecedor and not cdu_tipoartigo is null ) "
            Else
                restricao = "Artigo.TipoArtigo=TF.cdu_tipoartigo"
            End If


            consulta += " (select isnull(sum(case isnull(lc.cdu_conforme,0)  WHEN 1 THEN 1 ELSE 0 END),0) from PRI" + empresa + "..linhascompras LC inner join PRI" + empresa + "..CabecCompras CD on lC.IdCabecCompras=CD.Id inner join PRI" + empresa + "..artigo on LC.Artigo=Artigo.Artigo  WHERE " + restricao + " and tipodoc in (" + tipoDoc + ")  and serie='" + lstAno.Text + "' and F.Fornecedor=CD.Entidade AND not LC.DataEntrega is null AND not LC.CDU_DataRecepcao  is null)"
            If i <> listaempresas.Count - 1 Then
                consulta += " + "
            End If
        Next

        consulta += ")),0)  as CDU_NTotalLinhasConf,"
        consulta += "isnull((select ("


        For i = 0 To listaempresas.Count - 1
            empresa = listaempresas(i)

            If servicoPrincipal Then
                restricao = "not Artigo.TipoArtigo in (select cdu_tipoartigo FROM PRI" + empresa + "..TDU_FORNECEDOR_TIPOSFORNECEDOR WHERE CDU_Fornecedor=F.Fornecedor and not cdu_tipoartigo is null ) "
            Else
                restricao = "Artigo.TipoArtigo=TF.cdu_tipoartigo"
            End If

            consulta += "(select isnull(count(*),0)  from PRI" + empresa + "..linhascompras LC inner join PRI" + empresa + "..CabecCompras CD on lC.IdCabecCompras=CD.Id inner join PRI" + empresa + "..artigo on LC.Artigo=Artigo.Artigo  WHERE " + restricao + " and tipodoc in (" + tipoDoc + ") and serie='" + lstAno.Text + "' and F.Fornecedor=CD.Entidade AND not LC.DataEntrega is null AND not LC.CDU_DataRecepcao  is null )"
            If i <> listaempresas.Count - 1 Then
                consulta += " + "
            End If
        Next

        consulta += " )),0) as CDU_NTotalLinhas From PRI" + m.Empresa + "..APS_AF_FORNECEDORES F "

        If servicoPrincipal Then
            consulta += "INNER JOIN TDU_TIPOSFORNECEDOR  on TDU_TIPOSFORNECEDOR.CDU_TIPO=F.CDU_TIPOFORNECEDOR"
        Else
            consulta += "INNER JOIN TDU_FORNECEDOR_TIPOSFORNECEDOR TF ON F.Fornecedor=TF.CDU_Fornecedor INNER JOIN TDU_TIPOSFORNECEDOR on TDU_TIPOSFORNECEDOR.CDU_tipo=TF.cdu_TipoServico and not TF.cdu_tipoartigo is null"
        End If
        Return consulta
    End Function
    Private Sub criarViewFornecedores(listaempresas As String())
        Dim m As Motor
        Dim existeVIEW As Boolean
        Dim view As String
        Dim view1 As String
        Dim empresa As String
        Dim arr As New ArrayList



        m = Motor.GetInstance()
        existeVIEW = m.consultaValor("SELECT count(*) FROM sys.views WHERE object_id = OBJECT_ID('APS_AF_FORNECEDORES')") <> "0"

        empresa = m.Empresa

        If Not existeVIEW Then
            view = "CREATE VIEW APS_AF_FORNECEDORES AS "
        Else
            view = "ALTER VIEW APS_AF_FORNECEDORES AS "

        End If

        view1 = "select TDU_TiposFornecedor.CDU_DESCRICAO, Fornecedor, Nome,CDU_Certificado,CDU_CertificadoData,CDU_TIPOFORNECEDOR
                from PRI" + empresa + "..Fornecedores F LEFT OUTER JOIN PRI" + empresa + "..TDU_TiposFornecedor ON F.CDU_TipoFornecedor=TDU_TiposFornecedor.CDU_Tipo WHERE NOT F.CDU_TipoFornecedor is null"


        Dim i, j As Integer

        For i = 0 To listaempresas.Count - 1

            If listaempresas(i) <> empresa And listaempresas(i) <> "" Then
                empresa = listaempresas(i)
                view1 += " UNION select TDU_TiposFornecedor.CDU_DESCRICAO, Fornecedor, Nome,CDU_Certificado,CDU_CertificadoData,CDU_TIPOFORNECEDOR
                from PRI" + empresa + "..Fornecedores F LEFT OUTER JOIN PRI" + empresa + "..TDU_TiposFornecedor ON F.CDU_TipoFornecedor=TDU_TiposFornecedor.CDU_Tipo WHERE NOT F.CDU_TipoFornecedor is null"
                For j = 0 To arr.Count - 1
                    If arr(j) <> empresa Then
                        view1 += " And Not F.Fornecedor In (Select Fornecedor FROM  PRI" + arr(j) + ".. Fornecedores WHERE NOT CDU_TipoFornecedor is null) "
                    End If
                Next j

            End If
            arr.Add(empresa)
        Next

        m.executarComando(view + view1)

    End Sub


    Private Sub btnForn_Click(sender As Object, e As EventArgs) Handles btnForn.Click
        abrirFormLista()
    End Sub


    Private Sub abrirFormLista()

        Dim frmLista As FrmLista
        frmLista = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(4)
        If modulo = "C" Then
            frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, CDU_Descricao as TipoFornecedor FROM APS_AF_FORNECEDORES")
            frmLista.setCaption("Fornecedores")
        Else
            frmLista.setComando("SELECT TOP 100 PERCENT Cliente,Nome, Fac_Mor as Morada,Fac_Tel as 'Telefone', Fac_Fax FROM Clientes WHERE ClienteAnulado=0 ORDER BY Cliente")
            frmLista.setCaption("Clientes")
        End If
        ' frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
        ' frmLista.setCaption("Fornecedores")
        frmLista.ShowFilter(True)
        frmLista.setalinhamentoColunas(alinhamentoColunas)
        frmLista.ShowDialog()

        If frmLista.seleccionouOK Then
            txtEntidade.Text = frmLista.getValor(0)
            txtNomeEntidade.Text = frmLista.getValor(1)
            '  actualizarDadosFornecedor(frmLista.getValor(0))
        End If
    End Sub

    Private Sub txtFornecedor_Change(sender As Object, e As EventArgs) Handles txtEntidade.Change
        Dim nome As String
        Dim m As Motor
        Dim modulo
        m = Motor.GetInstance
        nome = ""

        modulo = "C"

        If modulo = "C" Then nome = m.consultaValor("SELECT nome from Fornecedores where fornecedor like '" + txtEntidade.Text + "'")
        If modulo = "V" Then nome = m.consultaValor("SELECT nome from Clientes where cliente like '" + txtEntidade.Text + "'")

        txtNomeEntidade.Text = nome
        If nome <> "" Then
            If modulo = "C" Then actualizarDadosFornecedor(txtEntidade.Text)
        End If
    End Sub


    Private Sub actualizarDadosFornecedor(fornecedor As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.consulta("SELECT Nome,NumContrib from fornecedores where fornecedor='" + fornecedor + "'")
        If Not lista.NoFim Then
            txtNomeEntidade.Text = lista.Valor("Nome")
        End If
    End Sub


    Private Sub inserirColunasRegistos(dt As DataTable)

        Dim m As Motor
        m = Motor.GetInstance
        Dim conforme As Double
        Dim conforme1 As Double
        Dim conforme2 As Double
        Dim row As DataRow
        gridRegistos.Records.DeleteAll()

        If Not dt Is Nothing Then
            For Each dtrow In dt.Rows
                conforme1 = m.NuloToDouble(dtrow("CDU_NTotalLinhasConf"))
                conforme2 = m.NuloToDouble(dtrow("CDU_NTotalLinhas"))

                If conforme2 = "0" Then
                    conforme = 0
                Else
                    conforme = (conforme1 / conforme2) * 100
                End If

                inserirLinhaRegisto(dtrow, gridRegistos, m.NuloToString(dtrow("CDU_TipoFornecedor")), m.NuloToString(dtrow("CDU_TIPO")), m.NuloToString(dtrow("Fornecedor")), m.NuloToString(dtrow("Nome")), m.NuloToBoolean(dtrow("CDU_Certificado")), m.NuloToString(dtrow("CDU_CertificadoData")), conforme, conforme1, conforme2, m.NuloToString(dtrow("CDU_Prazos")))
            Next
        End If

        gridRegistos.Populate()
    End Sub


    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, tipoFornecedor As String, tipoFornecedorID As String, fornecedor As String, nome As String, certificado As Boolean, DataCertificado As String, conformidade As String, NTotalLinhasConf As String, NTotalLinhas As String, difdias As String)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim editavel As Boolean
        Dim lista As StdBELista
        Dim m As Motor
        m = Motor.GetInstance
        editavel = True

        'Saber se já existe algum registo anterior gravado
        lista = m.consulta("SELECT * FROM TDU_AvaliacaoFornecedores WHERE CDU_Entidade='" + fornecedor + "' and CDU_Ano='" + lstAno.Text + "' and CDU_TipoForn='" + tipoFornecedorID + "'")


        Dim existeAvaliacao As Boolean = False
        Dim avaliacao As String

        'verificar se é necessário recalcular
        If Not lista.NoFim Then
            avaliacao = m.NuloToString(lista.Valor("CDU_Avaliacao"))
            If avaliacao <> "0" Then
                existeAvaliacao = True
            End If
        End If

        record = grid.Records.Add()
        record.Tag = fornecedor


        Item = record.AddItem(tipoFornecedor)
        Item.Tag = tipoFornecedorID
        Item.Editable = editavel
        Item = record.AddItem(fornecedor + " - " + nome)
        Item.Editable = editavel
        Item.Tag = nome

        'Certificado
        If certificado Then
            Item = record.AddItem("Sim")
        Else
            Item = record.AddItem("Não")
        End If
        Item.Editable = editavel
        Item.Tag = dtRow("TipoArtigo")

        'Data Certificado
        If IsDate(DataCertificado) Then
            '       If certificado Then
            Item = record.AddItem(DataCertificado)
        Else
            Item = record.AddItem("-")
        End If
        Item.Editable = editavel


        'Conformidade
        Item = record.AddItem(FormatNumber(CDbl(conformidade), 0).ToString)
        Item.Caption = Item.Value + " %  - " + NTotalLinhasConf + " de " + NTotalLinhas
        Item.Editable = editavel
        'If lista.NoFim Then
        '    Item.Tooltip = conformidade
        'Else
        '    Item.Tooltip = m.NuloToString(lista.Valor("CDU_FornConforme"))
        'End If

        'Difdias
        Item = record.AddItem(FormatNumber(CDbl(difdias), 0).ToString)
        Item.Editable = editavel
        Item.Tooltip = difdias

        'If lista.NoFim Then
        '    Item.Tooltip = difdias
        'Else
        '    Item.Tooltip = m.NuloToString(lista.Valor("CDU_Difdias"))
        'End If

        'Avaliação Certificado
        editavel = True
        Dim avcertificado As String = ""
        If Not lista.NoFim And existeAvaliacao Then
            'Caso já haja avaliação
            avcertificado = m.NuloToString(lista.Valor("CDU_CERTIFICADOA"))
            Item = record.AddItem(avcertificado)
            Item.Tooltip = buscarAvaliacaoCertificado(certificado, DataCertificado)
            Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
            Item.Tag = Item.Value
        Else
            avcertificado = buscarAvaliacaoCertificado(certificado, DataCertificado)
            Item = record.AddItem(avcertificado)
            Item.Tooltip = avcertificado
            Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
            Item.Tag = Item.Value
        End If



        'Avaliação Conformidade
        editavel = True

        If Not lista.NoFim And existeAvaliacao Then

            'Caso já haja avaliação
            Dim conformidadereal As String = conformidade

            conformidade = m.NuloToString(lista.Valor("CDU_FornConformeA"))
            Item = record.AddItem(conformidade)
            Item.Tooltip = buscarAvaliacaoFornConforme(conformidadereal)
            Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
            Item.Tag = Item.Value
        Else
            conformidade = buscarAvaliacaoFornConforme(conformidade)
            Item = record.AddItem(conformidade)
            Item.Tooltip = conformidade
            Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
            Item.Tag = Item.Value
        End If

        'Avaliação DifDias
        Item.Editable = editavel
        If Not lista.NoFim And existeAvaliacao Then
            'Caso já haja avaliação
            Dim difdiasreal As String = difdias

            difdias = m.NuloToString(lista.Valor("CDU_DifdiasA"))
            Item = record.AddItem(difdias)
            Item.Tooltip = buscarAvaliacaoFornDifDias(difdiasreal)
            Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
            Item.Tag = Item.Value
        Else
            difdias = buscarAvaliacaoFornDifDias(difdias)
            Item = record.AddItem(difdias)
            Item.Tooltip = difdias
            Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
            Item.Tag = Item.Value
        End If

        'Avaliacao geral
        Item.Editable = editavel


        If Not lista.NoFim And existeAvaliacao Then
            avaliacao = m.NuloToString(lista.Valor("CDU_Avaliacao"))
        Else
            avaliacao = "0"
        End If

        Item = record.AddItem(avaliacao)
        Item.BackColor = System.Convert.ToUInt32(HexToDecimal(COR_EDITAVEL))
        Item.Tag = Item.Value


        'Avaliacao calculada
        Item.Editable = editavel

        If Not lista.NoFim And existeAvaliacao Then
            avaliacao = m.NuloToString(lista.Valor("CDU_AvaliacaoFinal"))
        Else
            avaliacao = calcularAvaliacaoFinal(conformidade, avcertificado, difdias, avaliacao)
        End If

        Item = record.AddItem(avaliacao)
        'atribuir a cor da avaliação
        Item.BackColor = atualizarCorAvaliacaoFinal(avaliacao)


        'Notas
        Item.Editable = editavel
        Dim notas As String
        If Not lista.NoFim Then
            notas = m.NuloToString(lista.Valor("CDU_Notas"))
        Else
            notas = ""
        End If
        Item = record.AddItem(notas)
        Item.Editable = editavel
    End Sub
    'Função para classificar se as entregas da mercadoria foram cnformes
    Private Function buscarAvaliacaoFornConforme(conformidade As String) As String

        Dim avaliacao As Integer = CInt(conformidade)

        Select Case avaliacao
            Case < 25
                Return "0"
            Case < 50
                Return "1"
            Case < 75
                Return "2"
            Case Else
                Return "3"
        End Select

    End Function
    'Função para avaliar a diferença de dias e atribuir nota final
    Private Function buscarAvaliacaoFornDifDias(difdias As String) As String

        Dim avaliacao As Integer = CInt(difdias)

        Select Case avaliacao
            Case < 1
                Return "3"
            Case < 5
                Return "2"
            Case < 10
                Return "1"
            Case Else
                Return "0"
        End Select


    End Function

    Private Function buscarAvaliacaoCertificado(certificado As String, dataCertificado As String) As String
        If certificado Then
            Return "3"
        Else
            If IsDate(dataCertificado) Then
                Return "2"
            Else
                Return "1"
            End If

        End If

    End Function

    Private Function calcularAvaliacaoFinal(conforme As String, certificado As String, difDias As String, avaliacao As String) As String

        Dim soma As Double
        Dim media As Double

        soma = CDbl(conforme) + CDbl(difDias) + CDbl(avaliacao) + CDbl(certificado)

        media = soma / 4

        Select Case media
            Case <= 1
                Return "C"
                     Case <= 2
                Return "B"
            Case Else
                Return "A"

        End Select



    End Function
    'Função para atribuir a cor da célula da avaliação final consoante a nota
    Private Function atualizarCorAvaliacaoFinal(ByVal avaliacao As String) As String

        Select Case avaliacao
            Case "A"
                Return System.Convert.ToUInt32(HexToDecimal(COR_NOTAA))
            Case "B"
                Return System.Convert.ToUInt32(HexToDecimal(COR_NOTAB))
            Case Else
                Return System.Convert.ToUInt32(HexToDecimal(COR_NOTAC))

        End Select

    End Function


    Private Sub gridRegistos_ValueChanged(sender As Object, e As _DReportControlEvents_ValueChangedEvent) Handles gridRegistos.ValueChanged

        Dim valido As Boolean

        Select Case e.column.Index

            Case COLUMN_AVALIACAO_CERT
                valido = validarValoresAvaliacao(0, 3, e.item.Value)
            Case COLUMN_AVALIACAO_CONF
                valido = validarValoresAvaliacao(0, 3, e.item.Value)
            Case COLUMN_AVALIACAO_PRAZ
                valido = validarValoresAvaliacao(0, 3, e.item.Value)
            Case COLUMN_AVALIACAO_TJ
                valido = validarValoresAvaliacao(0, 3, e.item.Value)
            Case Else
                valido = True
        End Select

        If valido Then

            'Atualizar a cor e avaliação final
            gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_FINAL).Value = calcularAvaliacaoFinal(gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_CONF).Value.ToString, gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_CERT).Value.ToString, gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_PRAZ).Value.ToString, gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_TJ).Value.ToString)
            gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_FINAL).BackColor = atualizarCorAvaliacaoFinal(gridRegistos.Records(e.row.Index).Item(COLUMN_AVALIACAO_FINAL).Value)

        Else
            MsgBox("Preencha a avaliação com os valores corretos.", MsgBoxStyle.Critical)
            e.item.Value = e.item.Tag
        End If



    End Sub

    Private Function validarValoresAvaliacao(ByVal valorinf As Integer, ByVal valorsup As Integer, ByVal valor As Integer) As Boolean

        If valor >= valorinf And valor <= valorsup Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub FrmGestAvaliacaoFornecedores_SizeChanged(sender As Object, e As EventArgs) Handles MyBase.SizeChanged
        gridRegistos.Height = Me.Height - gridRegistos.Top - 50
        gridRegistos.Width = Me.Width - (gridRegistos.Left * 2) - 50
    End Sub



    Public Function HexToDecimal(ByVal HexString As String) As Integer
        Dim HexColor As Char() = HexString.ToCharArray()
        Dim DecimalColor As Integer = 0
        Dim iLength As Integer = HexColor.Length - 1
        Dim iDecimalNumber As Integer

        Dim cHexValue As Char
        For Each cHexValue In HexColor
            If Char.IsNumber(cHexValue) Then
                iDecimalNumber = Integer.Parse(cHexValue.ToString())
            Else
                iDecimalNumber = Convert.ToInt32(cHexValue) - 55
            End If

            DecimalColor += iDecimalNumber * (Convert.ToInt32(Math.Pow(16, iLength)))
            iLength -= 1
        Next cHexValue
        Return DecimalColor
    End Function

    Private Sub FrmGestAvaliacaoFornecedores_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then
            gravaresair()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub lstAno_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAno.SelectedIndexChanged
        actualizar()
    End Sub


    Private Sub imprimirMapa(ByVal report As String, ByVal destiny As String, codEmpresa As String)
        Try

            PlataformaPrimavera = New StdPlatBS

            Dim ficheiroIni As IniFile
            Dim seg As Seguranca = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

            fileINI = Application.StartupPath + "\CONFIG.ini"
            ficheiroIni = New IniFile(fileINI)

            ficheiroConfiguracao = Configuracao.GetInstance(fileINI)



            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "UTILIZADOR", ""))
            objConfApl.PwdUtilizador = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "PASSWORD", ""))
            objConfApl.LicVersaoMinima = "9.00"

            PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")

            Dim p As New System.Windows.Forms.PrintDialog
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, 2)

            PlataformaPrimavera.Mapas.Inicializar("ERP")

            PlataformaPrimavera.Mapas.DefinicaoImpressoraEx2(p.PrinterSettings.PrinterName, "winspool", "winspool", "", CRPEOrientacaoFolha.ofPortrait, 1, CRPETipoFolha.tfA4, 23, 23, 1, p.PrinterSettings.Duplex, 1, 1, 0)


            PlataformaPrimavera.Mapas.SelectionFormula = "{TDU_AVALIACAOFORNECEDORES.CDU_ANO}='" + lstAno.Text + "'"

            PlataformaPrimavera.Mapas.ImprimeListagem(report, "Mapa", destiny, 1, "S", , , , , , True)

            'PlataformaPrimavera.Mapas.ImprimeListagem(sReport:=report, sTitulo:="Mapa", sDestino:=destiny, iNumCopias:=1, sDocumento:="S", blnModal:=True)
            PlataformaPrimavera.Mapas.TerminaJanelas()

            PlataformaPrimavera.FechaPlataformaEmpresa()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub



    Private Sub conferirFornecedor()


        Try
            Dim forn As String
            Dim modo As String

            If gridRegistos.SelectedRows.Count > 0 Then

                Dim row As ReportRow

                row = gridRegistos.SelectedRows(0)
                forn = row.Record.Tag
                modo = row.Record(COLUMN_FORNECEDORCERTIFICADO).Tag

                imprimirDetalhes(modo, forn, Motor.GetInstance.getEmpresa)
            Else
                MsgBox("Tem de Seleccionar um fornecedor", MsgBoxStyle.Exclamation)
            End If
            'imprimirDetalhes("TJ_AVFOR", "W", Motor.GetInstance.getEmpresa)
        Catch ex As Exception
            MsgBox("Erro ao conferir as linhas: " + ex.Message, MsgBoxStyle.Exclamation)
        End Try


    End Sub


    Private Sub imprimirDetalhes(modo As String, fornecedor As String, codEmpresa As String)
        Try
            Dim m As Motor
            Dim tipodoc As String
            Dim empresas As String
            Dim listaempresas As String()

            m = Motor.GetInstance()
            tipodoc = "'" + m.consultaEntradaIniFile("AVALIACAOFORNECEDORES", "TIPODOCUMENTO") + "'"
            empresas = m.consultaEntradaIniFile("AVALIACAOFORNECEDORES", "EMPRESAS")

            listaempresas = empresas.Split(",")


            PlataformaPrimavera = New StdPlatBS

            Dim ficheiroIni As IniFile
            Dim seg As Seguranca = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

            fileINI = Application.StartupPath + "\CONFIG.ini"
            ficheiroIni = New IniFile(fileINI)

            ficheiroConfiguracao = Configuracao.GetInstance(fileINI)



            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "UTILIZADOR", ""))
            objConfApl.PwdUtilizador = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "PASSWORD", ""))
            objConfApl.LicVersaoMinima = "9.00"

            PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")



            Dim consulta As String
            Dim empresa As String
            Dim restricao As String


            'If servicoPrincipal Then
            '    restricao = "not Artigo.TipoArtigo in (select cdu_tipoartigo FROM PRI" + m.Empresa + "..TDU_FORNECEDOR_TIPOSFORNECEDOR WHERE CDU_Fornecedor=F.Fornecedor and not cdu_tipoartigo is null ) "
            '    modo = "''"
            'Else
            '    restricao = "Artigo.TipoArtigo=TF.cdu_tipoartigo"
            '    modo = "TF.cdu_tipoartigo"
            'End If



            consulta = "SELECT * FROM ("




            For i = 0 To listaempresas.Count - 1
                empresa = listaempresas(i)
                If modo = "" Then
                    restricao = "not Artigo.TipoArtigo in (select cdu_tipoartigo FROM PRI" + empresa + "..TDU_FORNECEDOR_TIPOSFORNECEDOR WHERE CDU_Fornecedor='" + fornecedor + "' and not cdu_tipoartigo is null ) "
                Else
                    restricao = "Artigo.TipoArtigo='" + modo + "'"
                End If

                consulta += "select '" + empresa + "' as Empresa,  CD.TipoDoc, CD.Serie, CD.NumDoc, CD.DataDoc, LC.Artigo, LC.Descricao, LC.CDU_ObraN1, DateDiff(Day, DataEntrega, CDU_DataRecepcao) * tdui.CDU_VALOR As Prazos, DataEntrega, CDU_DataRecepcao, tdui.CDU_DESCRICAO, tdui.CDU_VALOR, Lc.CDU_CONFORME,LC.NumLinha 
                            From PRI" + empresa + "..linhascompras LC inner Join  PRI" + empresa + "..CabecCompras CD on lC.IdCabecCompras=CD.Id left Join  PRI" + empresa + "..TDU_IMPACTO As tdui On lc.CDU_IMPACTO = tdui.CDU_IMPACTO INNER JOIN PRI" + empresa + "..Artigo on LC.Artigo=Artigo.Artigo
                            Where " + restricao + " and  tipodoc In (" + tipodoc + ") And serie='" + lstAno.Text + "' and CD.Entidade='" + fornecedor + "' AND not LC.DataEntrega is null AND not LC.CDU_DataRecepcao  is null "
                If i <> listaempresas.Count - 1 Then
                    consulta += " UNION "
                End If

            Next

            consulta += ") as tmpQuery order by  Empresa,TipoDoc,Serie,NumDoc,NumLinha"



            PlataformaPrimavera.Listas.GetF4SQL("Registos", consulta, "")

            'PlataformaPrimavera.Mapas.ImprimeListagem(sReport:=report, sTitulo:="Mapa", sDestino:=destiny, iNumCopias:=1, sDocumento:="S", blnModal:=True)
            PlataformaPrimavera.Mapas.TerminaJanelas()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


End Class