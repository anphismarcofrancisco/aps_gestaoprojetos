﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900

Public Class FrmStocks

    Const COLUMN_ARTIGO As Integer = 0
    ' Const COLUMN_ARMAZEM As Integer = 2
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTSTOCK As Integer = 2
    Const COLUMN_QUANTIDADE As Integer = 3
    Const COLUMN_PROJECTO As Integer = 3

    Dim arrCampo As ArrayList
    Dim arrNome As ArrayList

    Dim aplIniciada As Boolean
    Dim gravou As Boolean = False


    Private Sub FrmStocks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.WindowState = FormWindowState.Maximized

        Dim m As Motor
        m = Motor.GetInstance
        aplIniciada = False

        lblText.Text = m.EmpresaDescricao
        Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario

        gridArtigosStock.Tag = ""

        If Not m.getApresentaProjectos Then
            lblMoldes.Text = "Armazéns"
        End If

        iniciarToolBox()
        inicializarMoldes()

        inicializarDocumentos()

        arrCampo = m.buscarCampos("I")
        arrNome = m.buscarNomesCampos("I")


        gridArtigos.AutoColumnSizing = False
        gridArtigos.AutoColumnSizing = False
        '  gridArtigos.ShowGroupBox = True
        ' gridArtigos.ShowItemsInGroups = False

        gridArtigosStock.AutoColumnSizing = False
        gridArtigosStock.AutoColumnSizing = False

        gridArtigosStock.AllowEdit = True
        gridArtigosStock.EditOnClick = True

        ' gridArtigosStock.ShowGroupBox = True
        ' gridArtigosStock.ShowItemsInGroups = False

        inserirColunasRegistos(1, gridArtigos)
        inserirColunasRegistos(2, gridArtigosStock)

        buscarArtigos(gridArtigos, txtArtigo.Text)

        'gridArtigos.SortOrder.DeleteAll()
        'gridArtigos.SortOrder.Add(gridArtigos.Columns(COLUMN_ARTIGO))
        'gridArtigosStock.SortOrder.DeleteAll()
        'gridArtigos.SortOrder.Add(gridArtigos.Columns(COLUMN_ARTIGO))

        aplIniciada = True


        txtArtigo.Focus()
        SendKeys.Send("{RIGHT}")


        If proj <> "" Then
            actualizarFormularioByListaMateriais(proj)
        End If
    End Sub

    Dim dtArtigos As DataTable
    Public Sub setDataTable(dtArtigos As DataTable)
        Me.dtArtigos = dtArtigos
    End Sub

    Dim proj As String
    Public Sub setProjeto(proj As String)
        Me.proj = proj
    End Sub

    Private Sub inicializarDocumentos()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstDocumentos.SelectedIndex = -1
        lista = m.daListaDocumentos(True)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstDocumentos.ValueMember = "Codigo"
        lstDocumentos.DisplayMember = "Nome"
        lstDocumentos.DataSource = dt
        lstDocumentos.SelectedValue = m.DocumentoStock
        m.AplicacaoInicializada = True
    End Sub

    Private Sub inicializarMoldes()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstMoldes.SelectedIndex = -1
        lista = m.daListaMoldes(txtPesqMolde.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstMoldes.DataSource = dt
        lstMoldes.ValueMember = "Codigo"
        lstMoldes.DisplayMember = "Nome"
        lstMoldes.ClearSelected()
    End Sub


    Private Sub iniciarToolBox()
        Dim m As Motor
        m = Motor.GetInstance
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Limpar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Limpar"

            'Control = .Add(xtpControlButton, 2, " Imprimir", -1, False)
            'Control.BeginGroup = True
            'Control.DescriptionText = "Imprimir"
            'Control.Style = xtpButtonIconAndCaptionBelow
            Control = .Add(xtpControlButton, 4, " Registos", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Registos"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 5, " Encomendas", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Encomendas"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = m.getApresentaEncomendas

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub inserirColunasRegistos(ByVal indexGrid As Integer, ByVal grid As AxXtremeReportControl.AxReportControl)

        Dim m As Motor
        m = Motor.GetInstance

        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", 175, False, True)
        '  Column = inserirColuna(grid, COLUMN_ARMAZEM, "Armazém", 100, False, True)
        If indexGrid = 1 Then
            Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", 120, False, True)
        Else
            Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", 120, False, True)
        End If


        Column = inserirColuna(grid, COLUMN_QUANTSTOCK, "Quant Stk", 80, False, True)
        Column.Alignment = xtpAlignmentRight
        If indexGrid = 2 Then
            Column = inserirColuna(grid, COLUMN_QUANTIDADE, "Quant.", 80, False, True)
            Column.Alignment = xtpAlignmentRight

            'alteracao para colocar colunas consuante o descriminado no config 21-01-2013
            For i = 0 To arrCampo.Count - 1
                Column = inserirColuna(grid, COLUMN_PROJECTO + i + 1, arrNome(i), 60, False, True)
                Column.Tag = m.consultaEntradaIniFile("DOCUMENTO", arrCampo(i))
                Column.EditOptions.AllowEdit = True
                Column.Alignment = xtpAlignmentRight
            Next

        End If


    End Sub

    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Sub txtPesqMolde_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqMolde.Change
        inicializarMoldes()
    End Sub

    Private Sub buscarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim listaExclusao As String
        Dim m As Motor

        Me.Cursor = Cursors.WaitCursor
        If documento <> "" Then
            lblEmModoEdicao.Text = "Documento " + documento + " em modo de edição"
            lblEmModoEdicao.Visible = True
        Else
            lblEmModoEdicao.Visible = False
        End If
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        grid.Records.DeleteAll()
        listaExclusao = daListaArtigosExcluidos()
        Dim view As String
        view = m.daViewStocks()
        dtRegistos = m.daListaArtigos(IIf(m.getApresentaProjectos, view, "APS_GP_Artigos1"), pesquisa, "", listaExclusao)
        inserirLinhasRegistos(m, dtRegistos, grid)
        grid.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub inserirLinhasRegistos(m As Motor, ByVal dtRegistos As DataTable, ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(m, dtrow, grid)
        Next
    End Sub

    Private Sub inserirLinhaRegisto(m As Motor, ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional ByVal valorStk As Double = -1, Optional c1 As String = "", Optional c2 As String = "", Optional c3 As String = "", Optional c4 As String = "")
        Dim record As ReportRecord
        Dim Item As ReportRecordItem

        record = grid.Records.Add()

        '  record.PreviewText = "asd"
        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo"))

        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = 1

        If m.getApresentaProjectos Then
            Item = record.AddItem(dtRow("StkActual"))
        Else
            If aplIniciada Then
                If lstMoldes.SelectedValue Is Nothing Then
                    Item = record.AddItem(dtRow("StkActual"))
                Else
                    Dim stk As String
                    stk = m.consultaValor("SELECT StkActual FROM ArtigoArmazem WHERE Artigo='" + dtRow("Artigo") + "' and Armazem='" + lstMoldes.SelectedValue + "'")
                    If stk = "" Then stk = "0"
                    Item = record.AddItem(stk)
                End If
            Else
                Item = record.AddItem(dtRow("StkActual"))
            End If
        End If

        If grid.Name = "gridArtigosStock" Then
            If valorStk < 0 Then valorStk = valorStk * -1
            Item = record.AddItem(valorStk)

            For i = 0 To arrCampo.Count - 1
                If dtRow.Table.Columns.IndexOf(arrCampo(i)) <> -1 Then
                    Item = record.AddItem(m.NuloToString(dtRow(arrCampo(i))))
                Else
                    If iscampoFerramenta(arrCampo(i)) Then
                        Select Case UCase(arrCampo(i))
                            Case "CDU_FERNUMCOMPARTIMENTO" : Item = record.AddItem(c1)
                            Case "CDU_FERNUMEQUIPAMENTO" : Item = record.AddItem(c2)
                            Case "CDU_FERRAZAOTROCA" : Item = record.AddItem(c3)
                            Case "CDU_FERACCAOREALIZAR" : Item = record.AddItem(c4)
                        End Select
                    Else
                        Item = record.AddItem("")
                    End If

                End If
            Next

        End If

    End Sub

    Private Function iscampoFerramenta(campo As String) As Boolean
        iscampoFerramenta = False
        Select Case UCase(campo)
            Case "CDU_FERNUMCOMPARTIMENTO" : iscampoFerramenta = True
            Case "CDU_FERNUMEQUIPAMENTO" : iscampoFerramenta = True
            Case "CDU_FERRAZAOTROCA" : iscampoFerramenta = True
            Case "CDU_FERACCAOREALIZAR" : iscampoFerramenta = True
        End Select

        Return iscampoFerramenta
    End Function

    'Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

    '    ' buscarArtigos(gridArtigos, txtArtigo.Text)

    'End Sub

    Private Sub btnDPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDPA.Click
        If gridArtigos.SelectedRows.Count > 0 Then
            enviarArtigoParaSaida(gridArtigos.SelectedRows(0).Record.Item(COLUMN_ARTIGO).Value, gridArtigos.SelectedRows(0).Record.Item(COLUMN_DESCRICAO).Value, gridArtigos.SelectedRows(0).Index)
        End If
    End Sub

    Private Function enviarArtigoParaSaida(artigo As String, descricao As String, index As Integer) As Boolean
        Dim dtrow As DataRow
        Dim m As Motor
        m = Motor.GetInstance
        enviarArtigoParaSaida = True

        If artigo <> "" Then
            Dim i As Integer

            Dim topindex As Integer
            i = 0
            If Not m.artigoValido(artigo, descricao, True) Then
                MsgBox("O Artigo selecionado não é está configurado para movimentar Stock.", MsgBoxStyle.Information, "Anphis")
                enviarArtigoParaSaida = False
                Exit Function
            End If

           
            topindex = gridArtigos.TopRowIndex
            Dim dt As DataTable

            dt = m.consultaDataTable("SELECT * FROM ARTIGO WHERE ARTIGO='" + artigo + "'") ' gridArtigos.SelectedRows(i).Record.Tag

            If dt.Rows.Count = 0 Then Exit Function

            dtrow = dt.Rows(0)

            Dim indexForm As Integer

            indexForm = m.buscarForm(artigo)


            Dim botaoSeleccionado As Integer
            Dim valor As Double
            Dim c1 As String = ""
            Dim c2 As String = ""
            Dim c3 As String = ""
            Dim c4 As String = ""

            If indexForm = 1 Then
                Dim f1 As FrmQt
                f1 = New FrmQt
                f1.Valor = 1
                f1.setValorArtigo(artigo)
                f1.ShowDialog()
                botaoSeleccionado = f1.BotaoSeleccionado
                valor = f1.Valor
            End If

            Dim f2 As FrmQtFerramentas
            If indexForm = 2 Then
                f2 = New FrmQtFerramentas
                f2.Valor = 1
                f2.setValorArtigo(artigo)
                f2.ShowDialog()
                botaoSeleccionado = f2.BotaoSeleccionado
                valor = f2.Valor
                c1 = f2.getValorCampo(1)
                c2 = f2.getValorCampo(2)
                c3 = f2.getValorCampo(3)
                c4 = f2.getValorCampo(4)
            End If


            If botaoSeleccionado = 1 Then
                inserirLinhaRegisto(m, dtrow, gridArtigosStock, valor, c1, c2, c3, c4)
                gridArtigosStock.Populate()

                'If index <> -1 Then
                '    gridArtigos.SelectedRows(i).Record.Visible = False
                '    gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Tag = 0
                '    gridArtigos.Populate()
                '    gridArtigos.TopRowIndex = topindex
                '    If index + 1 < gridArtigos.Rows.Count Then
                '        gridArtigos.SelectedRows.Add(gridArtigos.Rows(index))
                '        gridArtigos.Navigator.MoveToRow(index)
                '    End If
                'End If
            End If
            lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
            lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
            Me.Activate()
            txtArtigo.Focus()
        Else
            enviarArtigoParaSaida = False
        End If
    End Function

    Private Sub seleccionarBotao()

    End Sub
    Private Sub btnEPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPA.Click
        retirarArtigoSaida()
    End Sub

    Private Sub retirarArtigoSaida()
        If gridArtigosStock.SelectedRows.Count > 0 Then
            Dim index As Integer
            '  Dim index1 As Integer
            Dim record As ReportRecord
            Dim i As Integer
            i = 0
            ' For i = 0 To gridArtigosStock.SelectedRows.Count - 1
            'ir buscar o index dos artigos a movimentar stock
            index = gridArtigosStock.SelectedRows(i).Record.Index
            'ir buscar o index da  grelha dos artigos 
            'buscar a linha
            record = gridArtigosStock.SelectedRows(i).Record
            gridArtigosStock.Records.RemoveAt(index)
            ' Next i
            colocarArtigoVisivel(record(COLUMN_ARTIGO).Value)
            gridArtigosStock.Populate()

            'buscarArtigos(gridArtigos, txtArtigo.Text)
            txtArtigo.Focus()
        End If
    End Sub

    Private Sub colocarArtigoVisivel(artigo As String)
        Dim row As ReportRecord
        For Each row In gridArtigos.Records
            If LCase(row(COLUMN_ARTIGO).Value) = LCase(artigo) Then
                row.Visible = True
            End If
        Next
        gridArtigos.Populate()
    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Index
            Case 1 : actualizar(True)
            Case 2 : abrirEditorRegistos()
            Case 3 : abrirEditorEncomendas()
            Case 4 : If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then Me.Close()
        End Select
    End Sub

    Private Sub actualizar(Optional ByVal pergunta As Boolean = False)

        If pergunta Then
            If MsgBox("Deseja realmente actualizar e limpar a informação existente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
        End If

        lblEmModoEdicao.Visible = False
        txtPesqMolde.Text = ""
        txtArtigo.Text = ""
        gridArtigosStock.Tag = ""
        '   aplicarFiltro()
        gridArtigos.Records.DeleteAll()
        gridArtigosStock.Records.DeleteAll()
        gridArtigosStock.Populate()
        inicializarMoldes()
        buscarArtigos(gridArtigos, txtArtigo.Text)

        txtArtigo.Focus()
        gravou = False
    End Sub

    Private Function daListaArtigosExcluidos() As String
        daListaArtigosExcluidos = ""
        Dim row As ReportRecord
        '  If gridArtigosStock.Records.Count > 0 Then
        For Each row In gridArtigosStock.Records
            If daListaArtigosExcluidos = "" Then
                daListaArtigosExcluidos = "'" + row.Item(COLUMN_ARTIGO).Value + "'"
            Else
                daListaArtigosExcluidos = daListaArtigosExcluidos + ",'" + row.Item(COLUMN_ARTIGO).Value + "'"
            End If
        Next

        '  End If

    End Function

    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        Dim m As Motor
        m = Motor.GetInstance

        If Not validarGravacao() Then
            Exit Sub
        End If
       
        ' m.consultaEntradaIniFile("I_"+lstDocumentos.SelectedValue+""_) 
        Me.Cursor = Cursors.WaitCursor

        Dim idCabecDoc As String
        idCabecDoc = gridArtigosStock.Tag

        idCabecDoc = m.actualizarDocumentoInterno(lstDocumentos.SelectedValue, m.Funcionario, lstMoldes.SelectedValue, dtpData.Value, gridArtigosStock.Records, idCabecDoc)

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" Then
            gravou = True
            MsgBox(lstDocumentos.Text + " realidada com sucesso.", MsgBoxStyle.Information)

            If dtArtigos Is Nothing Then
                actualizar()
            Else
                Me.Hide()
            End If

        End If
    End Sub


    Public ReadOnly Property GravouDocumento() As Boolean
        Get
            Return gravou
        End Get

    End Property

    Public Function buscarLinhas() As DataTable

        Dim dt As DataTable
        Dim row As ReportRecord
        Dim r As DataRow
        dt = New DataTable
        dt = dtArtigos.Clone()


        For Each row In gridArtigosStock.Records
            r = row.Tag
            dt.ImportRow(r)
        Next
        Return dt
    End Function
    Private Function validarGravacao() As Boolean
        Dim m As Motor
        m = Motor.GetInstance

        validarGravacao = False

        If MsgBox("Deseja realmente efectuar a gravação da " + lstDocumentos.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Function

        If lstMoldes.SelectedValue = Nothing Then
            MsgBox("Tem de seleccionar um " + IIf(m.getApresentaProjectos, "Molde", "Armazém"), MsgBoxStyle.Critical)
            Exit Function
        End If

        Dim row As ReportRecord
        Dim i As Integer
        Dim valida As String

        For Each row In gridArtigosStock.Records
            For i = 0 To arrCampo.Count - 1
                valida = m.consultaEntradaIniFile("DOCUMENTO", "S_" + lstDocumentos.SelectedValue + "_" + arrCampo(i))
                If valida = "1" And m.NuloToString(row(COLUMN_PROJECTO + i + 1).Value) = "" And m.NuloToString(row(COLUMN_ARTIGO).Value) <> "" Then
                    MsgBox("O campo " + arrNome(i) + " não se encontra preenchido", MsgBoxStyle.Critical)
                    Exit Function
                End If
            Next
        Next

        Return True

    End Function
    Private Sub gridArtigos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigos.RowDblClick
        ' enviarArtigoParaSaida()
    End Sub

    Private Sub gridArtigosStock_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigosStock.RowDblClick
        ' retirarArtigoSaida()

        Dim m As Motor
        m = Motor.GetInstance

        Dim indexForm As Integer

        indexForm = m.buscarForm(e.row.Record.Item(COLUMN_ARTIGO).Value)


        If indexForm = 1 Then
            Dim f As FrmQt
            f = New FrmQt
            f.setValorArtigo(e.row.Record.Item(COLUMN_ARTIGO).Value)
            f.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
            f.ShowDialog()
            If f.BotaoSeleccionado = 1 Then
                e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Valor
            End If
            f = Nothing
        Else
            Dim ff As FrmQtFerramentas
            ff = New FrmQtFerramentas
            ff.setValorArtigo(e.row.Record.Item(COLUMN_ARTIGO).Value)
            ff.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
            Dim indexInicio As Integer
            indexInicio = COLUMN_PROJECTO + 1

            If arrCampo.IndexOf("CDU_FERNumCompartimento") <> 0 Then
                ff.setValorCampo(1, e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERNumCompartimento")).Value)
            End If
            If arrCampo.IndexOf("CDU_FERNumEquipamento") <> 0 Then
                ff.setValorCampo(2, e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERNumEquipamento")).Value)
            End If

            If arrCampo.IndexOf("CDU_FERRazaoTroca") <> 0 Then
                ff.setValorCampo(3, e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERRazaoTroca")).Value)
            End If

            If arrCampo.IndexOf("CDU_FERAccaoRealizar") <> 0 Then
                ff.setValorCampo(4, e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERAccaoRealizar")).Value)
            End If

            ff.ShowDialog()

            If ff.BotaoSeleccionado = 1 Then
                e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERNumCompartimento")).Value = ff.getValorCampo(1)
                e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERNumEquipamento")).Value = ff.getValorCampo(2)
                e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERRazaoTroca")).Value = ff.getValorCampo(3)
                e.row.Record.Item(indexInicio + arrCampo.IndexOf("CDU_FERAccaoRealizar")).Value = ff.getValorCampo(4)
                e.row.Record.Item(COLUMN_QUANTIDADE).Value = ff.Valor

            End If

        End If
        Me.Activate()
        gridArtigosStock.Populate()
    End Sub


    Private Sub abrirEditorRegistos()

        Dim m As Motor
        m = Motor.GetInstance
        If Not m.isDocumentoFE(lstDocumentos.SelectedValue) Then
            Dim f As FrmEditarStocks
            f = New FrmEditarStocks
            f.documento = lstDocumentos.SelectedValue
            f.ShowDialog()
            If f.clicouConfirmar Then
                actualizarFormulario(f.getIdLinha, f.getValorLinha(0), f.getValorLinha(6))
            End If
        Else

            Dim f As FrmEditarStocksFE
            f = New FrmEditarStocksFE
            f.documento = lstDocumentos.SelectedValue
            f.ShowDialog()
            If f.clicouConfirmar Then
                actualizarFormulario(f.getIdLinha, f.getValorLinha(0), f.getValorLinha(6))
            End If
        End If
    End Sub

    Private Sub actualizarFormulario(ByVal id As String, ByVal documento As String, ByVal projecto As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosStock.Tag = id
        Dim m As Motor
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        dtRegistos = m.daListaArtigosDocumento(id, "I")
        Dim dt As Date
        lstMoldes.SelectedValue = projecto
        dt = m.daDataDocumento(id, dtpData.Value, "I")
        dtpData.Value = dt
        Dim dtrow As DataRow
        gridArtigosStock.Records.DeleteAll()
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(m, dtrow, gridArtigosStock, dtrow("Quantidade"))
        Next
        txtArtigo.Text = ""
        buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        gridArtigosStock.Populate()

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"

        Me.Cursor = Cursors.Default
    End Sub



    Private Sub actualizarFormularioByListaMateriais(projecto As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosStock.Tag = ""
        Dim m As Motor
        m = Motor.GetInstance

        Dim dt As Date
        lstMoldes.SelectedValue = projecto
        dt = Now
        dtpData.Value = dt
        Dim dtrow As DataRow
        gridArtigosStock.Records.DeleteAll()

        For Each dtrow In dtArtigos.Rows
            inserirLinhaRegisto(m, dtrow, gridArtigosStock, dtrow("Quantidade"))
        Next
        txtArtigo.Text = ""
        buscarArtigos(gridArtigos, txtArtigo.Text, "")
        gridArtigosStock.Populate()

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"

        Me.Cursor = Cursors.Default
    End Sub
    Private Sub btnDAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDAT.Click

    End Sub

    Private Sub abrirEditorEncomendas()
        Dim fEnc As FrmEncomendas
        fEnc = New FrmEncomendas
        Me.Hide()
        fEnc.ShowDialog()
        Me.Show()

    End Sub
  
    'Private Sub txtArtigo_KeyDownEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyDownEvent)

    '    If e.keyCode = Keys.Enter Then
    '        aplicarFiltro()
    '    End If
    'End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click
        aplicarFiltro()
    End Sub

    Private Sub filtrarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim row As ReportRecord
        Dim valor() As String
        Dim i As Integer
        Dim descricao As String = ""

        pesquisa = Trim(pesquisa)

        Dim m As Motor
        m = Motor.GetInstance

        Dim invalido As Boolean = True

        If m.existeArtigo(pesquisa) Then
            descricao = m.consultaValor("SELECT Descricao FROM Artigo WHERE artigo='" + pesquisa + "'")
            If m.artigoValido(pesquisa, descricao, True) Then
                invalido = False
            End If
        End If

        If invalido Then
            Me.Cursor = Cursors.WaitCursor
            For Each row In grid.Records
                row.Visible = True
                If pesquisa <> "" Then
                    valor = pesquisa.Split(" ")
                    For i = 0 To valor.Length - 1
                        If InStr(LCase(row(COLUMN_ARTIGO).Value), LCase(valor(i))) = 0 And InStr(LCase(row(COLUMN_DESCRICAO).Value), LCase(valor(i))) = 0 Then
                            row.Visible = False
                        End If
                    Next
                End If
            Next

            grid.Populate()


            gridArtigos.SelectedRows.DeleteAll()

            If gridArtigos.Rows.Count > 0 Then
                gridArtigos.SelectedRows.Add(gridArtigos.Rows(0))
            End If

            lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
            lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"

            Me.Cursor = Cursors.Default
        Else
            If enviarArtigoParaSaida(pesquisa, descricao, -1) Then
                txtArtigo.Text = ""
                aplicarFiltro()
            End If


        End If

    End Sub


    Private Sub aplicarFiltro()
        filtrarArtigos(gridArtigos, formatarTexto(txtArtigo.Text))
    End Sub

    Private Function formatarTexto(texto As String) As String
        texto = Replace(texto, "$$$$", "-")
        texto = Replace(texto, "$$$", ",")
        texto = Replace(texto, "$$", "/")
        texto = Replace(texto, "$", " ")
        Return texto
    End Function
    Private Sub lstDocumentos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstDocumentos.SelectedIndexChanged
        If gridArtigosStock.Tag <> "" Then
            actualizar()
        End If
    End Sub

    Private Sub lstMoldes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstMoldes.SelectedIndexChanged
        Dim m As Motor
        m = Motor.GetInstance
        If Not m.getApresentaProjectos Then
            buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub

   
    Private Sub gridArtigosStock_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles gridArtigosStock.PreviewKeyDown
        If e.KeyCode = Keys.F4 Then
            If gridArtigosStock.FocusedColumn Is Nothing Then Exit Sub
            If gridArtigosStock.FocusedColumn.Tag = "" Then Exit Sub
            Dim frmLista As FrmLista
            frmLista = New FrmLista
            ' Dim alinhamentoColunas() As Integer
            ' ReDim alinhamentoColunas(4)
            frmLista.setComando(gridArtigosStock.FocusedColumn.Tag)
            frmLista.setCaption(gridArtigosStock.FocusedColumn.Caption)
            ' frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
            ' frmLista.setCaption("Fornecedores")
            frmLista.ShowFilter(True)
            '   FrmLista.setalinhamentoColunas(alinhamentoColunas)
            frmLista.ShowDialog()

            If frmLista.seleccionouOK Then
                gridArtigosStock.SelectedRows(0).Record(gridArtigosStock.FocusedColumn.Index).Value = frmLista.getValor(0)
                gridArtigosStock.SelectedRows(0).Record(gridArtigosStock.FocusedColumn.Index).Caption = frmLista.getValor(0)
                'actualizarDadosFornecedor(frmLista.getValor(0))
            End If

        End If
    End Sub

    
    Private Sub txtArtigo_KeyDown(sender As Object, e As KeyEventArgs) Handles txtArtigo.KeyDown
        If e.KeyCode = Keys.Enter Then
            aplicarFiltro()
        End If
    End Sub

    'Private Sub txtArtigo_KeyDownEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyDownEvent)


    'End Sub

   
    Private Sub FrmStocks_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        lstMoldes.Location = New Point(Me.Width - lstMoldes.Width - 30, lstMoldes.Location.Y)

        txtPesqMolde.Location = New Point(lstMoldes.Location.X, txtPesqMolde.Location.Y)
        btnGravar.Location = New Point(lstMoldes.Location.X, btnGravar.Location.Y)
        lblMoldes.Location = New Point(lstMoldes.Location.X, lblMoldes.Location.Y)
        lstDocumentos.Location = New Point(btnGravar.Location.X - lstDocumentos.Size.Width - 10, btnGravar.Location.Y)
        dtpData.Location = New Point(lstDocumentos.Location.X + lstDocumentos.Width - dtpData.Width, dtpData.Location.Y)
        lblData.Left = dtpData.Location.X - lblData.Width - 5
        gridArtigosStock.Width = lstMoldes.Location.X - gridArtigosStock.Location.X - 5

        gridArtigos.Height = Me.Height - gridArtigos.Location.Y - 60
        gridArtigosStock.Height = Me.Height - gridArtigosStock.Location.Y - 60
        lstMoldes.Height = Me.Height - lstMoldes.Location.Y - 60
        lblRegisto1.Top = Me.Height - 60
        lblRegisto2.Top = Me.Height - 60
        lblRegisto1.Left = gridArtigos.Location.X
        lblRegisto2.Left = gridArtigosStock.Location.X
    End Sub
End Class