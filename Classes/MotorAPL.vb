﻿Public Class MotorAPL
    Dim m As Motor
    Public Sub New(motor As Motor)
        m = motor
    End Sub

    Public Sub inicializarAplicacao(codEmpresa As String)
        '   If Not existeTabela("TDU_PostosTrabalho") Then
        'If MsgBox("Atenção, toda irá ser criada toda a estrutura de dados para o bom funcionamento da aplicação!" + vbCrLf + vbCrLf + "Deseja Continuar?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
        criarExtruturaAPLBase()
        criarExtruturaAPLRP()
        criarExtruturaAPLLM()
        '     MsgBox("Extrutura de dados criada com sucesso!" + vbCrLf + vbCrLf + "Reinicie a aplicação", MsgBoxStyle.Information)
        'End If
        '  End If

        aplicarversao001()
        aplicarversao002()
        aplicarversao003()
        aplicarversao004()
        aplicarversao005()
        aplicarversao006()
        aplicarversao007()
        aplicarversao008()
        aplicarversao009()
        aplicarversao010()
        aplicarversao011()
        aplicarversao012()
        aplicarversao013()
        aplicarversao014()
        aplicarversao015()
    End Sub


    Private Sub criarExtruturaAPLBase()
        criarCamposBase()
        criarTabelasBase()
        criarviewsBase(False)
    End Sub

    Private Sub criarCamposBase()
        Dim comando As String
        If Not existeCampo("Funcionarios", "CDU_PassWord") Then
            comando = "ALTER TABLE Funcionarios Add CDU_PassWord Nvarchar(50)"
            m.executarComando(comando, False)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='Funcionarios' and campo='CDU_PassWord'"
            m.executarComando(comando, False)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_PassWord','Password','Password',1,1,NULL,NULL,NULL,0)"
            m.executarComando(comando, False)
            comando = "UPDATE Funcionarios set  CDU_PassWord='123'"
            m.executarComando(comando, False)
        End If


        If Not existeCampo("Funcionarios", "CDU_Administrador") Then
            comando = "ALTER TABLE Funcionarios Add CDU_Administrador Bit"
            m.executarComando(comando, False)
            comando = "DELETE FROM  StdCamposVar WHERE Tabela='Funcionarios' and campo='CDU_Administrador'"
            m.executarComando(comando, False)
            comando = "INSERT INTO StdCamposVar ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_Administrador','Administrador','Administrador',1,2,NULL,NULL,NULL,0)"
            m.executarComando(comando, False)
        End If

    End Sub
    Private Sub criarTabelasBase()
        Dim comando As String
        If Not existeTabela("TDU_PostosTrabalho") Then
            'Tabela TDU_PostosTrabalho
            comando = " CREATE TABLE [TDU_PostosTrabalho](" &
                        "[CDU_Posto] [nvarchar](50) NOT NULL," &
                        "[CDU_Descricao] [nvarchar](100) NULL," &
                        "[CDU_Activo] [bit]  NULL," &
                        "PRIMARY KEY CLUSTERED " &
                        "([CDU_Posto] ASC)) ON [PRIMARY]"
            m.executarComando(comando, False)
        End If
    End Sub

    Private Sub criarviewsBase(actualizar As Boolean)
        Dim comando As String
        'APS_GP_Funcionarios
        If Not existeView("APS_GP_Funcionarios") Then
            comando = "CREATE VIEW [APS_GP_Funcionarios] AS SELECT TOP (10000000)   Funcionarios.* FROM Funcionarios,Situacoes WHERE Funcionarios.Situacao=Situacoes.Situacao AND Tipo <>2 ORDER BY Nome"
            m.executarComando(comando, False)
        Else
            If actualizar Then
                comando = "ALTER VIEW [APS_GP_Funcionarios] AS SELECT TOP (10000000)   Funcionarios.* FROM Funcionarios,Situacoes WHERE Funcionarios.Situacao=Situacoes.Situacao AND Tipo <>2 ORDER BY Nome"
                m.executarComando(comando, False)
            End If
        End If
    End Sub

    Private Sub criarExtruturaAPLRP()
        criarTabelasRP()
        associarTabelasStdcamposVarRP()
        criarViewsRP()
    End Sub
    Private Sub criarTabelasRP()
        Dim comando As String

        'Tabela Funcionarios Campo CDU_Posto
        If Not existeCampo("Funcionarios", "CDU_Posto") Then
            comando = "ALTER TABLE Funcionarios Add CDU_Posto Nvarchar(50)"
            m.executarComando(comando)
        End If

        'Tabela Funcionarios Campo CDU_PassWord
        If Not existeCampo("Funcionarios", "CDU_PassWord") Then
            comando = "ALTER TABLE Funcionarios Add CDU_PassWord Nvarchar(50)"
            m.executarComando(comando)
        End If

        'Tabela Funcionarios Campo CDU_Administrador
        If Not existeCampo("Funcionarios", "CDU_Administrador") Then
            comando = "ALTER TABLE Funcionarios Add CDU_Administrador bit"
            m.executarComando(comando)
        End If

        'Tabela LinhasInternos Campo CDU_DataObra
        If Not existeCampo("LinhasInternos", "CDU_DataObra") Then
            comando = "ALTER TABLE LinhasInternos Add CDU_DataObra datetime"
            m.executarComando(comando, False)
        End If

        'Tabela LinhasInternos Campo CDU_ObraN1
        If Not existeCampo("LinhasInternos", "CDU_ObraN1") Then
            comando = "ALTER TABLE LinhasInternos Add CDU_ObraN1 Nvarchar(100)"
            m.executarComando(comando, False)
        End If

        'Tabela LinhasInternos Campo CDU_ObraN2

        If Not existeCampo("LinhasInternos", "CDU_ObraN2") Then
            comando = "ALTER TABLE LinhasInternos Add CDU_ObraN2 Nvarchar(100)"
            m.executarComando(comando, False)
        End If

        comando = "ALTER TABLE LinhasInternos Alter Column CDU_ObraN1 Nvarchar(100)"
        m.executarComando(comando, False)
        comando = "ALTER TABLE LinhasInternos Alter Column CDU_ObraN2 Nvarchar(100)"
        m.executarComando(comando, False)

        'Tabela TDU_Postos
        If Not existeTabela("TDU_Postos") Then
            comando = "CREATE TABLE [TDU_Postos]( " &
               "[CDU_Posto] [nvarchar](48) NOT NULL," &
               "[CDU_Descricao] [nvarchar](50) NULL," &
               "[CDU_Artigo] [nvarchar](48) NULL," &
               "PRIMARY KEY CLUSTERED " &
               "([CDU_Posto] Asc )) ON [PRIMARY]"
            m.executarComando(comando)

        End If

        If Not existeTabela("TDU_PostosTrabalho") Then
            'Tabela TDU_PostosTrabalho
            comando = " CREATE TABLE [TDU_PostosTrabalho](" &
             "[CDU_Posto] [nvarchar](50) NOT NULL," &
             "[CDU_Descricao] [nvarchar](100) NULL," &
             "[CDU_Activo] [bit]  NULL," &
             "PRIMARY KEY CLUSTERED " &
             "([CDU_Posto] ASC)) ON [PRIMARY]"
            m.executarComando(comando, False)
        End If
        'Tabela TDU_RegistoFolhaPonto
        If Not existeTabela("TDU_RegistoFolhaPonto") Then
            comando = "CREATE TABLE [dbo].[TDU_RegistoFolhaPonto](" &
                 "[CDU_ID] [uniqueidentifier] NOT NULL," &
                 "[CDU_Molde] [nvarchar](40) NOT NULL," &
                 "[CDU_Peca] [nvarchar](100) NULL," &
                 "[CDU_Operacao] [nvarchar](100) NULL," &
                 "[CDU_Funcionario] [nvarchar](100) NULL," &
                 "[CDU_Posto] [nvarchar](100) NULL," &
                 "[CDU_DataInicial] [datetime] NULL," &
                 "[CDU_DataFinal] [datetime] NULL," &
                 "[CDU_Tempo] [float] NULL," &
                 "[CDU_IdLinha] [uniqueidentifier] NULL," &
                 "[CDU_EstadoTrabalho] [nvarchar](100) NULL," &
                 "[CDU_Observacoes] [ntext] NULL," &
                 "[CDU_IdPonto] [uniqueidentifier] NULL," &
                 "CONSTRAINT [PK_TDU_RegistoFolhaPonto] PRIMARY KEY CLUSTERED " &
                "([CDU_ID] Asc)) ON [PRIMARY]"
            m.executarComando(comando)

            comando = "ALTER TABLE [dbo].[TDU_RegistoFolhaPonto] ADD  CONSTRAINT [DF_TDU_RegistoFolhaPonto_CDU_ID]  DEFAULT (newid()) FOR [CDU_ID]"
            m.executarComando(comando)

            comando = "ALTER TABLE [dbo].[TDU_RegistoFolhaPonto] ADD  DEFAULT (newid()) FOR [CDU_IdPonto]"
            m.executarComando(comando)
        End If
        'Tabela TDU_UtilizadoresPostos
        If Not existeTabela("TDU_UtilizadoresPostos") Then
            comando = " CREATE TABLE [dbo].[TDU_UtilizadoresPostos](" &
                 "[CDU_Utilizador] [nvarchar](10) NOT NULL," &
                 "[CDU_Posto] [nvarchar](48) NOT NULL," &
                 "PRIMARY KEY CLUSTERED " &
                 "(	[CDU_Utilizador] ASC,[CDU_Posto] Asc)) ON [PRIMARY]"
            m.executarComando(comando)
        End If
        'Tabela TDU_UtilizadoresPermissoes
        If Not existeTabela("TDU_UtilizadoresPermissoes") Then
            comando = " CREATE TABLE [dbo].[TDU_UtilizadoresPermissoes](" &
                 "[CDU_ID] [uniqueidentifier] NOT NULL," &
                 "[CDU_Utilizador] [nvarchar](10) NOT NULL," &
                 "[CDU_UtilizadorPermissao] [nvarchar](10) NOT NULL," &
                 "CONSTRAINT [PK_TDU_UtilizadoresPermissoes] PRIMARY KEY CLUSTERED " &
                 "([CDU_ID] Asc)) ON [PRIMARY]"
            m.executarComando(comando)

            comando = "ALTER TABLE [dbo].[TDU_UtilizadoresPermissoes] ADD  CONSTRAINT [DF_TDU_UtilizadoresPermissoes_CDU_ID]  DEFAULT (newid()) FOR [CDU_ID]"
            m.executarComando(comando)
        End If

        'TDU_EstadosTrabalho
        If Not existeTabela("TDU_EstadosTrabalho") Then
            comando = " CREATE TABLE [dbo].[TDU_EstadosTrabalho](" &
                  "[CDU_Estado] [nvarchar](10) NOT NULL," &
                  "[CDU_Descricao] [nvarchar](100) NOT NULL," &
                  "CONSTRAINT [PK_TDU_TDU_EstadoTrabalho] PRIMARY KEY CLUSTERED " &
                  "([CDU_Estado] Asc)) ON [PRIMARY]"
            m.executarComando(comando)
        End If


        'TDU_UtilizadoresOperacoes


        If Not existeTabela("TDU_UtilizadoresOperacoes") Then
            comando = " CREATE TABLE [dbo].[TDU_UtilizadoresOperacoes](" &
                 "[CDU_ID] [uniqueidentifier] NOT NULL," &
                 "[CDU_Utilizador] [nvarchar](10) NOT NULL," &
                 "[CDU_Operacao] [nvarchar](100) NOT NULL," &
                 "CONSTRAINT [PK_TDU_UtilizadoresOperacoes] PRIMARY KEY CLUSTERED " &
                 "([CDU_ID] Asc)) ON [PRIMARY]"
            m.executarComando(comando)

            comando = " ALTER TABLE [dbo].[TDU_UtilizadoresOperacoes] ADD  CONSTRAINT [DF_TDU_UtilizadoresOperacoes_CDU_ID]  DEFAULT (newid()) FOR [CDU_ID]"
            m.executarComando(comando)
        End If

        If Not existeTabela("TDU_ObraN1") Then
            comando = " CREATE TABLE [TDU_ObraN1](" &
          "[CDU_ObraN1] [nvarchar](100) NOT NULL," &
          "[CDU_Descricao] [nvarchar](100) NOT NULL," &
          "[CDU_Inactivo] [bit] NULL," &
          "PRIMARY KEY CLUSTERED " &
          "([CDU_ObraN1] ASC)) ON [PRIMARY]"
            m.executarComando(comando, False)
        End If

        If Not existeTabela("TDU_ObraN2") Then
            comando = " CREATE TABLE [TDU_ObraN2](" &
            "[CDU_ObraN2] [nvarchar](100) NOT NULL," &
            "[CDU_Descricao] [nvarchar](100) NOT NULL," &
            "PRIMARY KEY CLUSTERED " &
            "([CDU_ObraN2] ASC)) ON [PRIMARY]"
            m.executarComando(comando, False)
        End If
    End Sub

    Private Sub associarTabelasStdcamposVarRP()
        Dim comando As String

        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_Postos'"
        m.executarComando(comando)
        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_UtilizadoresPostos'"
        m.executarComando(comando)
        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_UtilizadoresPermissoes'"
        m.executarComando(comando)
        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_UtilizadoresOperacoes'"
        m.executarComando(comando)
        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_EstadosTrabalho'"
        m.executarComando(comando)
        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_ObraN1'"
        m.executarComando(comando)
        comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_ObraN2'"
        m.executarComando(comando)

        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_Postos','ERP')"
        m.executarComando(comando)
        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_UtilizadoresPostos','ERP')"
        m.executarComando(comando)
        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_UtilizadoresPermissoes','ERP')"
        m.executarComando(comando)
        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_UtilizadoresOperacoes','ERP')"
        m.executarComando(comando)
        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_EstadosTrabalho','ERP')"
        m.executarComando(comando)
        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_ObraN1','ERP')"
        m.executarComando(comando)
        comando = "INSERT INTO [StdTabelasVar]([Tabela],[Apl]) VALUES ('TDU_ObraN2','ERP')"
        m.executarComando(comando)

        'TDU_Postos
        comando = "DELETE FROM StdCamposVar WHERE Tabela='Funcionarios' and Campo='CDU_Posto'"
        m.executarComando(comando)
        comando = "DELETE FROM StdCamposVar WHERE Tabela='Funcionarios' and Campo='CDU_Password'"
        m.executarComando(comando)
        comando = "DELETE FROM StdCamposVar WHERE Tabela='Funcionarios' and Campo='CDU_Administrador'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('Funcionarios','CDU_Posto','Posto','Posto',1,1,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('Funcionarios','CDU_Password','Password','Password',1,2,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('Funcionarios','CDU_Administrador','Administrador','Administrador',1,3,NULL,NULL,NULL,0)"
        m.executarComando(comando)

        ''Artigo
        'comando = "DELETE FROM StdCamposVar WHERE Tabela='Artigo' and Campo='CDU_ArtigoGP'"
        'executarComando(comando)
        'comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('Artigo','CDU_ArtigoGP','ArtigoGP','ArtigoGP',1,1,NULL,NULL,NULL,0)"
        'executarComando(comando)

        'TDU_Postos
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_Postos'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('TDU_Postos','CDU_Posto','Posto','Posto',1,1,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('TDU_Postos','CDU_Descricao','Descricao','Descricao',1,2,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('TDU_Postos','CDU_Artigo','Artigo','Artigo',1,3,NULL,NULL,'SELECT * FROM ARTIGO ORDER BY Artigo',0)"
        m.executarComando(comando)

        'TDU_UtilizadoresPostos
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_UtilizadoresPostos'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('TDU_UtilizadoresPostos','CDU_Utilizador','Utilizador','Utilizador',1,1,NULL,NULL,'SELECT [Codigo],[Nome] FROM [Funcionarios]',0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('TDU_UtilizadoresPostos','CDU_Posto','Posto','Posto',1,2,NULL,NULL,'SELECT [CDU_Posto],[CDU_Descricao] FROM [TDU_Postos]',0)"
        m.executarComando(comando)

        'LinhasInternos
        comando = "DELETE FROM StdCamposVar WHERE Tabela='LinhasInternos' and Campo='CDU_DataObra'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('LinhasInternos','CDU_DataObra','Data','Data',1,1,NULL,NULL,NULL,0)"
        m.executarComando(comando)

        comando = "DELETE FROM StdCamposVar WHERE Tabela='LinhasInternos' and Campo='CDU_ObraN1'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('LinhasInternos','CDU_ObraN1','Peça','Peça',1,2,NULL,NULL,NULL,0)"
        m.executarComando(comando)

        comando = "DELETE FROM StdCamposVar WHERE Tabela='LinhasInternos' and Campo='CDU_ObraN2'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES('LinhasInternos','CDU_ObraN2','Operação','Operação',1,3,NULL,NULL,NULL,0)"
        m.executarComando(comando)


        'TDU_UtilizadoresPermissoes
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_UtilizadoresPermissoes'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_UtilizadoresPermissoes' ,'CDU_Utilizador','Utilizador','Utilizador',1,1 ,NULL,NULL,'SELECT [Codigo],[Nome] FROM [Funcionarios]',0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_UtilizadoresPermissoes' ,'CDU_UtilizadorPermissao','Utilizador Permissao','Utilizador Permissao',1,2,NULL,NULL,'SELECT [Codigo],[Nome] FROM [Funcionarios]',0)"
        m.executarComando(comando)

        'TDU_UtilizadoresOperacoes
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_UtilizadoresOperacoes'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_UtilizadoresOperacoes' ,'CDU_Utilizador','Utilizador','Utilizador',1,1 ,NULL,NULL,'SELECT [Codigo],[Nome] FROM [Funcionarios]',0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_UtilizadoresOperacoes' ,'CDU_Operacao','Operacao','Operacao',1,2,NULL,NULL,'SELECT [CDU_Descricao] FROM [TDU_ObraN2]',0)"
        m.executarComando(comando)

        'TDU_EstadosTrabalho
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_EstadosTrabalho'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_EstadosTrabalho','CDU_Estado','Estado','Estado',1,1,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_EstadosTrabalho','CDU_Descricao' ,'Descricao' ,'Descricao' ,1,2,NULL,NULL,NULL,0)"
        m.executarComando(comando)

        'TDU_ObraN1
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_ObraN1'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_ObraN1','CDU_ObraN1','ObraN1','ObraN1',1,1,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_ObraN1','CDU_Descricao' ,'Descricao' ,'Descricao' ,1,2,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_ObraN1','CDU_Inactivo' ,'Inactivo' ,'Inactivo' ,1,3,NULL,NULL,NULL,0)"
        m.executarComando(comando)

        'TDU_ObraN2
        comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_ObraN2'"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_ObraN2','CDU_ObraN2','ObraN2','ObraN2',1,1,NULL,NULL,NULL,0)"
        m.executarComando(comando)
        comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_ObraN2','CDU_Descricao' ,'Descricao' ,'Descricao' ,1,2,NULL,NULL,NULL,0)"
        m.executarComando(comando)


    End Sub

    Private Sub criarViewsRP()
        Dim comando As String

        'APS_GP_Funcionarios
        If Not existeView("APS_GP_Funcionarios") Then
            comando = "CREATE VIEW [APS_GP_Funcionarios] AS SELECT TOP (10000000)  Funcionarios.* FROM Funcionarios,Situacoes WHERE Funcionarios.Situacao=Situacoes.Situacao AND Tipo <>2 ORDER BY Nome"
            m.executarComando(comando)
        End If

        'APS_GP_Projectos
        If Not existeView("APS_GP_Projectos") Then
            comando = "CREATE VIEW [APS_GP_Projectos] AS SELECT TOP (10000000)  Codigo, Codigo AS Nome FROM COP_Obras WHERE Estado = 'ABTO' ORDER BY Codigo"
            m.executarComando(comando)
        End If

        'APS_GP_Operacoes
        If Not existeView("APS_GP_Operacoes") Then
            comando = "CREATE VIEW [APS_GP_Operacoes] AS SELECT TOP (10000000) CDU_Descricao AS Codigo, CDU_Descricao AS Nome FROM TDU_ObraN2 ORDER BY Codigo"
            m.executarComando(comando)
        End If


        '[APS_GP_Pecas]
        If Not existeView("APS_GP_Pecas") Then
            comando = "CREATE VIEW [APS_GP_Pecas] AS SELECT TOP (10000000) CDU_ObraN1 AS Codigo, CDU_ObraN1 AS Nome, CDU_Inactivo FROM  dbo.TDU_ObraN1 WHERE (isnULL(CDU_Inactivo,0) = 0) ORDER BY Codigo"
            m.executarComando(comando)
        End If

        '[APS_GP_Postos]
        If Not existeView("APS_GP_Postos") Then
            comando = "CREATE VIEW [dbo].[APS_GP_Postos] AS SELECT  TOP (10000000) CDU_Posto, CDU_Artigo AS Codigo, CDU_Descricao  AS Nome FROM dbo.TDU_Postos ORDER BY CDU_Posto"
            m.executarComando(comando)
        End If


        '[APS_GP_Moldes]
        If Not existeView("APS_GP_Moldes") Then
            comando = "CREATE VIEW [dbo].[APS_GP_Moldes] AS SELECT  TOP (10000000) Codigo, Codigo AS Nome FROM COP_Obras WHERE   (Estado = 'ABTO') ORDER BY Codigo"
            m.executarComando(comando)
        End If



    End Sub


    Private Sub criarExtruturaAPLLM()
        criarViewsLM()
        associarTabelasStdcamposVarLM()
    End Sub

    Private Sub criarViewsLM()

        Dim comando As String

        If Not existeView("APS_GP_Artigos") Then
            comando = "CREATE VIEW  APS_GP_Artigos AS SELECT Artigo, Descricao as Nome, StkActual FROM Artigo WHERE ArtigoAnulado=0"
            m.executarComando(comando, True)
        End If

        If Not existeView("APS_GP_Artigos1") Then
            comando = "CREATE VIEW  APS_GP_Artigos1 AS SELECT Artigo, Descricao as Nome, StkActual FROM Artigo WHERE ArtigoAnulado=0"
            m.executarComando(comando, True)
        End If

    End Sub


    Private Sub associarTabelasStdcamposVarLM()
        Dim comando As String

        If Not existeCampo("CabecCompras", "CDU_CabVar1") Then
            comando = "ALTER TABLE CabecCompras Add CDU_CabVar1 NVarchar(10)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='CabecCompras' and campo='CDU_CabVar1'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('CabecCompras','CDU_CabVar1','Funcionario','Funcionario',1,1,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If

        If Not existeCampo("CabecCompras", "CDU_CabVar2") Then
            comando = "ALTER TABLE CabecCompras Add CDU_CabVar2 NVarchar(10)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='CabecCompras' and campo='CDU_CabVar2'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('CabecCompras','CDU_CabVar2','DataFatura','DataFatura',1,2,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If


        If Not existeCampo("CabecCompras", "CDU_Conferido") Then
            comando = "ALTER TABLE CabecCompras Add CDU_Conferido bit"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='CabecCompras' and campo='CDU_Conferido'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('CabecCompras','CDU_Conferido','Conferido','Conferido',1,3,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If


        If Not existeCampo("LinhasCompras", "CDU_ObraN1") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN1 Nvarchar(10)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN1'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN1','ObraN1','ObraN1',1,1,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If

        If Not existeCampo("LinhasCompras", "CDU_ObraN2") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN2 Nvarchar(10)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN2'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN2','ObraN2','ObraN2',1,2,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If

        If Not existeCampo("LinhasCompras", "CDU_ObraN3") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN3 Nvarchar(255)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN3'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN3','ObraN3','ObraN3',1,3,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If

        If Not existeCampo("LinhasCompras", "CDU_ObraN4") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN4 Nvarchar(255)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN4'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN4','ObraN4','ObraN4',1,4,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If


        If Not existeCampo("LinhasCompras", "CDU_ObraN5") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN5 Nvarchar(255)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN5'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN5','ObraN5','ObraN5',1,5,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If

        If Not existeCampo("LinhasCompras", "CDU_ObraN6") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN6 Nvarchar(255)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN6'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN6','CDU_ObraN6','ObraN6',1,6,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If


        If Not existeCampo("LinhasCompras", "CDU_ObraN7") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN7 Nvarchar(255)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN7'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN7','CDU_ObraN7','ObraN7',1,7,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If


        If Not existeCampo("LinhasCompras", "CDU_ObraN8") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_ObraN8 Nvarchar(255)"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_ObraN8'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN8','CDU_ObraN8','ObraN8',1,8,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If


        If Not existeCampo("LinhasCompras", "CDU_DataRecepcao") Then
            comando = "ALTER TABLE LinhasCompras Add CDU_DataRecepcao datetime"
            m.executarComando(comando, True)
            comando = "DELETE FROM  StdCAmposVAR WHERE Tabela='LinhasCompras' and campo='CDU_DataRecepcao'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_DataRecepcao','Data Recepcao','Data Recepcao',1,6,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If




    End Sub


    Private Function existeView(view As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = " Select count(*) from sys.objects where name ='" + view + "' and type='V'"
        valor = m.consultaValor(consulta)
        Return valor <> "0"
    End Function

    Private Function existeTabela(tabela As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = " select count(*) from sys.objects where name ='" + tabela + "' and type='U'"
        valor = m.consultaValor(consulta)
        Return valor <> "0"
    End Function

    Public Function existeCampo(tabela As String, campo As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = "select count(*) from sys.columns where name ='" + campo + "' and OBJECT_ID('" + tabela + "')=object_id"
        valor = m.consultaValor(consulta)
        Return valor <> "0"
    End Function


    Public Function daTipoCampo(tabela As String, campo As String) As String
        If existeCampo(tabela, campo) Then
            Dim consulta As String
            Dim valor As String
            consulta = "select sys.types.name from sys.columns inner join   sys.types on sys.columns.system_type_id=sys.types.system_type_id  where sys.columns.name ='" + campo + "' and OBJECT_ID('" + tabela + "')=sys.columns.object_id"
            valor = m.consultaValor(consulta)
            Return valor.ToLower()
        Else
            Return ""
        End If
    End Function
    'Private Function existeDocumentoCompra(documento As String) As Boolean
    '    Return bso.Comercial.TabCompras.Existe(documento)
    'End Function


    Public Function NuloToBoolean(ByVal obj) As Boolean
        If IsDBNull(obj) Then
            NuloToBoolean = 0
        Else
            NuloToBoolean = CBool(obj)
        End If

    End Function

    Public Function NuloToDate(ByVal obj As Object, ByVal def As DateTime) As DateTime
        If IsDBNull(obj) Then
            NuloToDate = def
        Else
            If Not IsDate(obj) Then
                NuloToDate = def
            Else
                NuloToDate = obj
            End If
        End If
    End Function

    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                NuloToDouble = 0
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Public Function NuloToString(ByVal obj) As String
        If IsDBNull(obj) Then
            NuloToString = ""
        Else
            NuloToString = CStr(obj)
        End If

    End Function


    Private Sub aplicarversao001()
        Dim comando As String

        If Not existeTabela("TDU_EQUIV_LSTM_ARTIGO") Then
            comando = "CREATE TABLE [dbo].[TDU_EQUIV_LSTM_ARTIGO]([CDU_ID] [uniqueidentifier] Not NULL,[CDU_MARCALSTM] [nvarchar](255) NULL, [CDU_DESCRICAOLSTM] [nvarchar](255) NULL,[CDU_REFERENCIALSTM] [nvarchar](255) NULL,[CDU_MARCAARTIGO] [nvarchar](255) NULL,[CDU_DESCRICAOARTIGO] [nvarchar](255) NULL,[CDU_REFERENCIAARTIGO] [nvarchar](255) NULL,[CDU_FORMATOARTIGO] [nvarchar](255) NULL, [CDU_FORMATACAOARTIGO] [Int] NULL,[CDU_ARTIGO] [nvarchar](48) NULL, Constraint [PK_TDU_EQUIV_LSTM_ARTIGO] PRIMARY KEY CLUSTERED ([CDU_ID] Asc )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] ) ON [PRIMARY]"
            m.executarComando(comando, True)
            comando = "ALTER TABLE [dbo].[TDU_EQUIV_LSTM_ARTIGO] ADD  CONSTRAINT [TDU_EQUIV_LSTM_ARTIGO_CDU_ID_DF]  DEFAULT (newid()) FOR [CDU_ID]"
            m.executarComando(comando, True)

            comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_EQUIV_LSTM_ARTIGO'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdTabelasVar VALUES ('TDU_EQUIV_LSTM_ARTIGO','ERP')"
            m.executarComando(comando, True)

            comando = "DELETE FROM StdCAmposVAR WHERE Tabela='TDU_EQUIV_LSTM_ARTIGO'"
            m.executarComando(comando, True)

            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_MARCALSTM','CDU_MARCALSTM','CDU_MARCALSTM',1,1,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_DESCRICAOLSTM','CDU_DESCRICAOLSTM','CDU_DESCRICAOLSTM',1,2,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_REFERENCIALSTM','CDU_REFERENCIALSTM','CDU_REFERENCIALSTM',1,3,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_MARCAARTIGO','CDU_MARCAARTIGO','CDU_MARCAARTIGO',1,4,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_DESCRICAOARTIGO','CDU_DESCRICAOARTIGO','CDU_DESCRICAOARTIGO',1,5,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_REFERENCIAARTIGO','CDU_REFERENCIAARTIGO','CDU_REFERENCIAARTIGO',1,6,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_FORMATOARTIGO','CDU_FORMATOARTIGO','CDU_FORMATOARTIGO',1,7,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_FORMATACAOARTIGO','CDU_FORMATACAOARTIGO','CDU_FORMATACAOARTIGO',1,8,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_LSTM_ARTIGO','CDU_ARTIGO','CDU_ARTIGO','CDU_ARTIGO',1,9,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If

        If Not existeTabela("TDU_EQUIV_FORMATO_ARTIGO") Then
            comando = "CREATE TABLE [dbo].[TDU_EQUIV_FORMATO_ARTIGO]([CDU_ID] [int] NULL,[CDU_Descricao] [nvarchar](255) NULL,[CDU_CodigoVB] [ntext] NULL) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
            m.executarComando(comando, True)

            comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_EQUIV_FORMATO_ARTIGO'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdTabelasVar VALUES ('TDU_EQUIV_FORMATO_ARTIGO','ERP')"
            m.executarComando(comando, True)


            comando = "DELETE FROM StdCAmposVAR WHERE Tabela='TDU_EQUIV_FORMATO_ARTIGO'"
            m.executarComando(comando, True)


            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_FORMATO_ARTIGO','CDU_ID','CDU_ID','CDU_ID',1,1,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_FORMATO_ARTIGO','CDU_Descricao','CDU_Descricao','CDU_Descricao',1,2,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_EQUIV_FORMATO_ARTIGO','CDU_CodigoVB','CDU_CodigoVB','CDU_CodigoVB',1,3,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
        End If



        If Not existeTabela("TDU_GP_CONFIGGRID") Then
            comando = "CREATE TABLE [TDU_GP_CONFIGGRID]([CDU_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_TDU_GP_CONFIGGRID_CDU_ID]  DEFAULT (newid()),	[CDU_APL] [nvarchar](50) NULL,	[CDU_FORM] [nvarchar](50) NULL,	[CDU_GRID] [nvarchar](50) NULL,	[CDU_CAMPO] [nvarchar](50) NULL,	[CDU_DESCRICAO] [nvarchar](50) NULL,	[CDU_TAMANHO] [int] NULL,	[CDU_SUBQUERY] [nvarchar](255) NULL,	[CDU_ORDEM] [int] NULL,	[CDU_Administrador] [bit] NULL, CONSTRAINT [PK_TDU_GP_CONFIGGRID] PRIMARY KEY CLUSTERED (	[CDU_ID] ASC))"

            m.executarComando(comando, True)

            comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_GP_CONFIGGRID'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdTabelasVar VALUES ('TDU_GP_CONFIGGRID','ERP')"
            m.executarComando(comando, True)


            comando = "DELETE FROM StdCAmposVAR WHERE Tabela='TDU_GP_CONFIGGRID'"
            m.executarComando(comando, True)


            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_APL','CDU_APL','CDU_APL',1,1,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_FORM','CDU_FORM','CDU_FORM',1,2,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_GRID','CDU_GRID','CDU_GRID',1,3,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_CAMPO','CDU_CAMPO','CDU_CAMPO',1,4,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_DESCRICAO','CDU_DESCRICAO','CDU_DESCRICAO',1,5,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_TAMANHO','CDU_TAMANHO','CDU_TAMANHO',1,6,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_SUBQUERY','CDU_SUBQUERY','CDU_SUBQUERY',1,7,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_ORDEM','CDU_ORDEM','CDU_ORDEM',1,8,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_Administrador','CDU_Administrador','CDU_Administrador',1,9,NULL,NULL,NULL,0)"
            m.executarComando(comando, True)

            inserirDadosVersao1()
        End If

    End Sub


    Private Sub inserirDadosVersao1()
        Dim comando As String
        'comando="DELETE FROM TDU_GP_CONFIGGRID"
        'm.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'68b56232-233a-49a7-97cf-040b877974b0', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_ObraN1', N'Peça', 69, NULL, 0, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'1c44610b-7d9c-443f-a56e-0806364cd7b0', N'M', N'FrmListaEncomendas', N'gridArtigos', N'CDU_ObraN3', N'Dimensões', 155, NULL, 4, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'91a0b836-5c74-4ccb-ade0-0ca4945f1895', N'M', N'FrmListaEncomendas', N'gridArtigos', N'NUMEROALTERACOES', N'N. Alt.', 50, NULL, 9, NULL)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'c5b911ce-0324-4fc7-b933-0e988a7ddd36', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'PrecUnit', N'P. Unit.', 50, NULL, 6, NULL)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'5f105390-88cd-4da7-9662-1bcee717d61f', N'M', N'FrmListaEncomendas', N'gridArtigos', N'Descricao', N'Descrição', 230, NULL, 2, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'3caea0b4-5591-43b5-b2b4-29b794680b7f', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'CDU_ObraN1', N'Peça', 69, NULL, 1, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'756aebcf-4539-4bce-a50c-4cbe3cb23d06', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_ObraN3', N'Dimensões', 155, NULL, 3, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'eac6b3d7-1c42-4893-ae8d-60241fdada1d', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'Artigo', N'Artigo', 80, N'SELECT Artigo, Nome, StkActual  FROM APS_GP_Artigos', 7, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'5ff9c032-ac8d-4668-a885-6f57a2e62c3f', N'M', N'FrmListaEncomendas', N'gridArtigos', N'Quantidade', N'Quant.', 60, NULL, 3, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'52cfe0e7-784f-42de-afde-74eb10e26249', N'M', N'FrmListaEncomendas', N'gridArtigos', N'CHECKBOX', NULL, 20, NULL, 0, 1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'f0522d15-9dff-40f9-ace1-7d42cae8ff7f', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'Descricao', N'Descrição', 230, NULL, 2, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'da0bf5b9-aae7-4937-ae5a-9ec58144db1e', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_DataRecepcao', N'Dt. Recepção', 100, NULL, 7, NULL)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'aee7ed97-2645-4a78-a94f-9f2836abe898', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'CDU_ObraN3', N'Dimensões', 155, NULL, 4, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'20476402-0f6a-4da1-bfaa-ad931bb258e4', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'PrecUnit', N'P. Unit.', 50, NULL, 6, NULL)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'7db9ad34-6d86-42eb-a522-ade46ec115f8', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CHECKBOX', NULL, 20, NULL, 8, 1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'2780bc25-5ec1-467c-ab23-ae6427da2d33', N'M', N'FrmListaEncomendas', N'gridArtigos', N'Artigo', N'Artigo', 80, N'SELECT Artigo,  Nome, StkActual  FROM APS_GP_Artigos', 7, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'0beb2080-6754-4a00-90ae-afc37de36e0e', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'Artigo', N'Artigo', 80, N'SELECT Artigo,  Nome, StkActual  FROM APS_GP_Artigos', 5, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'2ce16968-9759-4c83-8128-b245c972b7cc', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'CDU_ObraN4', N'Material', 90, NULL, 5, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'0383995f-9a2f-4541-af0b-b320637e0833', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'Quantidade', N'Quant.', 60, NULL, 2, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'847572b7-a98a-4857-b55a-b9ae1493164e', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'Quantidade', N'Quant.', 60, NULL, 3, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'7a45e7f2-151c-4695-9358-c1326a373c66', N'M', N'FrmListaEncomendas', N'gridArtigos', N'CDU_ObraN4', N'Material', 90, NULL, 5, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'9ca293ff-e41c-4984-9d72-d6ce4eda6fc1', N'M', N'FrmListaEncomendas', N'gridArtigos', N'FORNECEDOR', N'Fornecedor', 150, NULL, 8, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'8268cacc-5d58-4ffb-a83d-d7c8e521c8a4', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'Descricao', N'Descrição', 230, NULL, 1, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'00fc1473-7d15-4f73-b420-e88de147b8a7', N'M', N'FrmListaEncomendas', N'gridArtigos', N'PrecUnit', N'P. Unit.', 50, NULL, 6, NULL)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'06118d7b-4156-4f25-b631-f0110db6a1d5', N'M', N'FrmListaEncomendas', N'gridArtigos', N'CDU_ObraN1', N'Peça', 69, N'Select * from TDU_ObraN1', 1, 0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'3e1530c0-eabe-46ff-9eab-f13139988a40', N'M', N'FrmListaEncomendas', N'gridArtigosCOT', N'CHECKBOX', NULL, 20, NULL, 0, 1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador]) VALUES (N'1c06d284-d9c9-4d57-8e38-ff27b251205f', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_ObraN4', N'Material', 90, NULL, 4, 0)"
        m.executarComando(comando, True)

    End Sub


    Public Sub aplicarversao002()


        If Not existeCampo("LinhasInternos", "CDU_DataObraFinal") Then
            m.executarComando("ALTER TABLE LinhasInternos ADD CDU_DataObraFinal datetime", False)
            m.executarComando("UPDATE LinhasInternos  set CDU_DataObraFinal=TabelaOrigem.CDU_DataFinal FROM LinhasInternos TabelaDestino INNER JOIN TDU_RegistoFolhaPonto TabelaOrigem ON  TabelaDestino.Id= TabelaOrigem.CDU_IdLinha", False)

            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasInternos','CDU_DataObraFinal','DataObraFinal','DataObraFinal',1,4,NULL,NULL,NULL,0)", False)
        End If
        If Not existeCampo("LinhasInternos", "CDU_EstadoTrabalho") Then
            m.executarComando("ALTER TABLE LinhasInternos ADD CDU_EstadoTrabalho nvarchar(100)", False)
            m.executarComando("UPDATE LinhasInternos  set CDU_EstadoTrabalho=TabelaOrigem.CDU_EstadoTrabalho FROM LinhasInternos TabelaDestino INNER JOIN TDU_RegistoFolhaPonto TabelaOrigem ON  TabelaDestino.Id= TabelaOrigem.CDU_IdLinha", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasInternos','CDU_EstadoTrabalho','EstadoTrabalho','EstadoTrabalho',1,5,NULL,NULL,NULL,0)", False)
        End If
        If Not existeCampo("LinhasInternos", "CDU_Observacoes") Then
            m.executarComando("ALTER TABLE LinhasInternos ADD CDU_Observacoes ntext", False)
            m.executarComando("UPDATE LinhasInternos  set CDU_Observacoes=TabelaOrigem.CDU_Observacoes FROM LinhasInternos TabelaDestino INNER JOIN TDU_RegistoFolhaPonto TabelaOrigem ON  TabelaDestino.Id= TabelaOrigem.CDU_IdLinha", False)
            m.executarComando("UPDATE LinhasInternos  set IdLinhaOrigemCopia=TabelaOrigem.CDU_IdPonto FROM LinhasInternos TabelaDestino INNER JOIN TDU_RegistoFolhaPonto TabelaOrigem ON  TabelaDestino.Id= TabelaOrigem.CDU_IdLinha", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasInternos','CDU_Observacoes','Observacoes','Observacoes',1,6,NULL,NULL,NULL,0)", False)

            m.executarComando("UPDATE LinhasInternos  set IdLinhaOrigemCopia= newid() where IdLinhaOrigemCopia Is null And IdCabecInternos in (select id from CabecInternos where TipoDoc='" + m.consultaEntradaIniFile("DOCUMENTORP", "TIPODOC") + "')")
        End If




    End Sub

    Public Sub aplicarversao003()
        Dim comando As String

        If Not existeTabela("TDU_PostosOperacoes") Then
            comando = "CREATE TABLE [TDU_PostosOperacoes]([CDU_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_TDU_PostosOperacoes_CDU_ID]  DEFAULT (newid()),[CDU_Posto] [nvarchar](10) NOT NULL,[CDU_Operacao] [nvarchar](100) NOT NULL, CONSTRAINT [PK_TDUPostosOperacoes] PRIMARY KEY CLUSTERED ([CDU_ID] ASC)) ON [PRIMARY]"

            m.executarComando(comando, True)

            comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_PostosOperacoes'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdTabelasVar VALUES ('TDU_PostosOperacoes','ERP')"
            m.executarComando(comando, True)


            'TDU_PostosOperacoes
            comando = "DELETE FROM StdCamposVar WHERE Tabela='TDU_PostosOperacoes'"
            m.executarComando(comando)
            comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_PostosOperacoes' ,'CDU_Posto','Posto','Posto',1,1 ,NULL,NULL,'SELECT CDU_Posto as [Posto],[Nome] FROM [APS_GP_Postos]',0)"
            m.executarComando(comando)
            comando = "INSERT INTO [StdCamposVar]([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])VALUES ('TDU_PostosOperacoes' ,'CDU_Operacao','Operacao','Operacao',1,2,NULL,NULL,'SELECT [Codigo] FROM [APS_GP_Operacoes]',0)"
            m.executarComando(comando)


        End If
    End Sub


    Public Sub aplicarversao004()
        Try
            Dim nome As String
            nome = m.consultaValor("SELECT PrimeiroNome+' '+PrimeiroApelido as Nome  FROM APS_GP_Funcionarios WHERE Codigo='000'")
        Catch ex As Exception
            Dim comando As String
            comando = "ALTER VIEW [APS_GP_Funcionarios] AS SELECT TOP (10000000)   Funcionarios.* FROM Funcionarios,Situacoes WHERE Funcionarios.Situacao=Situacoes.Situacao AND Tipo <>2 ORDER BY Nome"
            m.executarComando(comando, False)
        End Try


    End Sub


    Public Sub aplicarversao005()
        If Not existeCampo("Funcionarios", "CDU_AdicionaPecas") Then
            m.executarComando("ALTER TABLE Funcionarios ADD CDU_AdicionaPecas Bit", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_AdicionaPecas','Adiciona Pecas','Adiciona Pecas',1,4,NULL,NULL,NULL,0)", False)
            m.executarComando("UPDATE Funcionarios set CDU_AdicionaPecas=CDU_Administrador", False)
            Dim comando As String
            comando = "ALTER VIEW [APS_GP_Funcionarios] AS SELECT TOP (10000000)   Funcionarios.* FROM Funcionarios,Situacoes WHERE Funcionarios.Situacao=Situacoes.Situacao AND Tipo <>2 ORDER BY Nome"
            m.executarComando(comando, False)

        End If
    End Sub

    Public Sub aplicarversao006()
        If Not existeCampo("TDU_GP_CONFIGGRID", "CDU_EDITAVEL") Then
            m.executarComando("ALTER TABLE TDU_GP_CONFIGGRID ADD CDU_EDITAVEL Bit", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('TDU_GP_CONFIGGRID','CDU_EDITAVEL','EDITAVEL','Editavel',1,10,NULL,NULL,NULL,0)", False)
            m.executarComando("UPDATE TDU_GP_CONFIGGRID set CDU_EDITAVEL=1", False)

            actualizarCamposGrelha006()
        End If




        'If Not existeCampo("CabecCompras", "CDU_IDDocOrigem") Then
        '    m.executarComando("ALTER TABLE CabecCompras ADD CDU_IDDocOrigem nvarchar(50)", False)
        '    m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('CabecCompras','CDU_IDDocOrigem','IdDocOrigem','IdDocOrigem',1,13,NULL,NULL,NULL,0)", False)

        'End If

    End Sub



    Private Sub actualizarCamposGrelha006()
        Dim comando As String

        comando = "DELETE FROM TDU_GP_CONFIGGRID WHERE CDU_GRID='gridArtigosENC' or CDU_GRID='gridArtigosENT'"
        m.executarComando(comando, True)

        comando = "DELETE FROM TDU_GP_CONFIGGRID WHERE CDU_FORM='FrmListaEncomendasECF' and  CDU_GRID='gridArtigos'"
        m.executarComando(comando, True)


        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'68b56232-233a-49a7-97cf-040b877974b0', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_ObraN1', N'Peça', 69, NULL, 0, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'8268cacc-5d58-4ffb-a83d-d7c8e521c8a4', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'Descricao', N'Descrição', 230, NULL, 1, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'756aebcf-4539-4bce-a50c-4cbe3cb23d06', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_ObraN3', N'Dimensões', 155, NULL, 2, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'1c06d284-d9c9-4d57-8e38-ff27b251205f', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_ObraN4', N'Material', 90, NULL, 3, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'0beb2080-6754-4a00-90ae-afc37de36e0e', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'Artigo', N'Artigo', 80, N'SELECT Artigo,  Nome, StkActual  FROM APS_GP_Artigos', 4, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'c5b911ce-0324-4fc7-b933-0e988a7ddd36', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'PrecUnit', N'P. Unit.', 50, NULL, 5, NULL,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'0383995f-9a2f-4541-af0b-b320637e0833', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'Quantidade', N'Qt. Enc.', 60, NULL, 6, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'4899f979-aed6-41c0-bd6f-98288dc6b48b', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'QUANTENT', N'Qt. Ent.', 60, NULL, 7, 0,0)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'96f54a4f-7790-40a6-9a5b-e1d7fa6a4b8d', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'QUANTFALTA', N'Qt. Qt.', 60, NULL, 8, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'dc0d25a8-9d85-4f7b-a3e8-77fd5be9a06e', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'DataEntrega', N'Data Entrega', 100, NULL, 9, NULL,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'da0bf5b9-aae7-4937-ae5a-9ec58144db1e', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CDU_DataRecepcao', N'Dt. Recepção', 100, NULL, 10, NULL,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'7db9ad34-6d86-42eb-a522-ade46ec115f8', N'M', N'FrmListaEncomendas', N'gridArtigosENC', N'CHECKBOX', NULL, 20, NULL, 11, 1,1)"
        m.executarComando(comando, True)


        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'4c1bca83-b8de-4053-8bec-908d437be8f2', N'M', N'FrmListaEncomendas', N'gridArtigosENT', N'CDU_ObraN1', N'Peça', 69, NULL, 0, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'fe80ed33-4b61-46a6-9e24-a1c083303ded', N'M', N'FrmListaEncomendas', N'gridArtigosENT', N'Descricao', N'Descrição', 230, NULL, 1, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'60910109-b050-47bb-9ea2-791b395f4189', N'M', N'FrmListaEncomendas', N'gridArtigosENT', N'CDU_ObraN3', N'Dimensões', 155, NULL, 2, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'bd67a550-4eee-4cc7-839d-1382fe2b5d25', N'M', N'FrmListaEncomendas', N'gridArtigosENT', N'CDU_ObraN4', N'Material', 90, NULL, 3, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'855c6f4a-1b14-467f-b10a-2fa5a989896f', N'M', N'FrmListaEncomendas', N'gridArtigosENT', N'Quantidade', N'Quant', 60, NULL, 4, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'c3028c2d-c203-4682-bc78-2420192b8c25', N'M', N'FrmListaEncomendas', N'gridArtigosENT', N'CDU_DataRecepcao', N'Dt. Recepção', 100, NULL, 5, NULL,1)"
        m.executarComando(comando, True)



        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'f70f225f-4c55-4897-8daa-4fb7cb09d189', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'CDU_ObraN1', N'Peça', 69, NULL, 0, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'1e429d38-340d-4d46-b309-91b239046f43', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'Descricao', N'Descrição', 230, NULL, 1, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'9A56954C-031A-4593-99AC-EEBEDADA623C', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'Quantidade', N'Quant.', 60, NULL, 2, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'0B5AF35F-FB95-4F02-BBA7-9AC73D7EB759', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'CDU_ObraN3', N'Dimensões', 155, NULL, 3, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'17BFDD4B-86FC-42AB-8FD4-E15A9B87DA2A', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'CDU_ObraN4', N'Material', 90, NULL, 4, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'6A41BA12-2E4D-4146-B46F-381430871F2D', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'Artigo', N'Artigo', 80, N'SELECT Artigo,  Nome, StkActual  FROM APS_GP_Artigos', 5, 0,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'BF0033C0-902F-413C-B20A-172527FF7AAA', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'PrecUnit', N'P. Unit.', 50, NULL, 6, NULL,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'708707DB-0873-4708-849F-16819E0CD1B0', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'DataEntrega', N'Data Entrega', 100, NULL, 7, NULL,1)"
        m.executarComando(comando, True)
        comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (N'1E3FD0EC-EAD7-45A9-8321-01E9EE155ACE', N'M', N'FrmListaEncomendasECF', N'gridArtigos', N'CHECKBOX', NULL, 0, NULL, 8, 1,1)"
        m.executarComando(comando, True)







    End Sub


    Private Sub aplicarversao007()

        Dim comando As String
        comando = "UPDATE TDU_GP_CONFIGGRID set CDU_SUBQUERY='SELECT Fornecedor,Nome FROM Fornecedores WHERE FornecedorAnulado=0' WHERE   (CDU_GRID = 'gridArtigos') AND (CDU_FORM = 'FrmListaEncomendas') and CDU_CAMPO='FORNECEDOR'"
        m.executarComando(comando, True)

        If m.consultaValor("SELECT COUNT(*) FROM StdCamposVAR WHERE Tabela='LinhasCompras' and Campo='CDU_ObraN6'") = "0" Then
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN6','CDU_ObraN6','CDU_ObraN6',1,11,NULL,NULL,NULL,0)", False)
        End If

        If m.consultaValor("SELECT COUNT(*) FROM StdCamposVAR WHERE Tabela='LinhasCompras' and Campo='CDU_ObraN7'") = "0" Then
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN7','CDU_ObraN7','CDU_ObraN7',1,12,NULL,NULL,NULL,0)", False)
        End If

        If m.consultaValor("SELECT COUNT(*) FROM StdCamposVAR WHERE Tabela='LinhasCompras' and Campo='CDU_ObraN8'") = "0" Then
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ObraN8','CDU_ObraN8','CDU_ObraN8',1,13,NULL,NULL,NULL,0)", False)
        End If

    End Sub



    Private Sub aplicarversao008()


        Dim totalColunas As Integer
        Dim actualizar As Boolean

        actualizar = False
        totalColunas = m.consultaValor("SELECT isnull(COUNT(*),0) FROM TDU_GP_CONFIGGRID WHERE CDU_FORM='FrmListaEncomendas' and  CDU_GRID='gridArtigos'")
        Dim comando As String

        If m.consultaValor("Select COUNT(*) FROM TDU_GP_CONFIGGRID WHERE CDU_FORM='FrmListaEncomendas' and  CDU_GRID='gridArtigos' AND CDU_CAMPO='DataEntrega'") = "0" Then
            comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (NewID(), N'M', N'FrmListaEncomendas', N'gridArtigos', N'DataEntrega', N'Dt. Entrega', 100, NULL, " + CStr(totalColunas - 2) + ", 0,0)"
            m.executarComando(comando, True)
            actualizar = True
        End If

        If m.consultaValor("Select COUNT(*) FROM TDU_GP_CONFIGGRID WHERE CDU_FORM='FrmListaEncomendas' and  CDU_GRID='gridArtigos' AND CDU_CAMPO='CDU_DataRecepcao'") = "0" Then
            comando = "INSERT [dbo].[TDU_GP_CONFIGGRID]([CDU_ID], [CDU_APL], [CDU_FORM], [CDU_GRID], [CDU_CAMPO], [CDU_DESCRICAO], [CDU_TAMANHO], [CDU_SUBQUERY], [CDU_ORDEM], [CDU_Administrador],[CDU_Editavel]) VALUES (NewID(), N'M', N'FrmListaEncomendas', N'gridArtigos', N'CDU_DataRecepcao', N'Dt. Recepção', 100, NULL, " + CStr(totalColunas - 1) + ", 0,0)"
            m.executarComando(comando, True)
            actualizar = True
        End If


        If actualizar Then
            comando = "UPDATE [TDU_GP_CONFIGGRID] set CDU_Ordem=" + CStr(totalColunas) + " WHERE CDU_FORM='FrmListaEncomendas' and  CDU_GRID='gridArtigos' AND CDU_CAMPO='FORNECEDOR'"
            m.executarComando(comando, True)
        End If

        If actualizar Then
            comando = "UPDATE [TDU_GP_CONFIGGRID] set CDU_Ordem=" + CStr(totalColunas + 1) + " WHERE CDU_FORM='FrmListaEncomendas' and  CDU_GRID='gridArtigos' AND CDU_CAMPO='NUMEROALTERACOES'"
            m.executarComando(comando, True)
        End If


        If actualizar Then
            Dim motor As MotorLM
            motor = MotorLM.GetInstance()
            comando = " UPDATE linhascompras Set CDU_DataRecepcao=NULL, DataEntrega = NULL where IdCabecCompras in (select id from CabecCompras where tipodoc='" + Motor.InformModLstMaterial.TIPODOCLM + "')"
            m.executarComando(comando, True)
        End If

    End Sub


    Private Sub aplicarversao009()

        If Not existeCampo("Funcionarios", "CDU_AdministradorLM") Then
            m.executarComando("ALTER TABLE Funcionarios ADD CDU_AdministradorLM Bit", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_AdministradorLM','ADM LM','Editavel',1,11,NULL,NULL,NULL,0)", False)
            m.executarComando("UPDATE Funcionarios set CDU_AdministradorLM=CDU_Administrador", False)

            m.executarComando("UPDATE StdCAmposVAR set Descricao='ADM Total',Texto='ADM Total' WHERE Campo='CDU_Administrador' and Tabela='Funcionarios'", False)
        End If

        If Not existeCampo("Funcionarios", "CDU_AdministradorRP") Then
            m.executarComando("ALTER TABLE Funcionarios ADD CDU_AdministradorRP Bit", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_AdministradorRP','ADM RP','Editavel',1,12,NULL,NULL,NULL,0)", False)
            m.executarComando("UPDATE Funcionarios set CDU_AdministradorRP=CDU_Administrador", False)
        End If

    End Sub

    Private Sub aplicarversao010()
        If Not existeCampo("CabecCompras", "CDU_DataListaMat") Then
            m.executarComando("ALTER TABLE CabecCompras ADD CDU_DataListaMat Datetime", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('CabecCompras','CDU_DataListaMat','Data Lista Mat','Data Lista Mat',1,11,NULL,NULL,NULL,0)", False)
            m.executarComando("UPDATE CabecCompras set CDU_DataListaMat=DataIntroducao WHERE TipoDoc='LST'", False)
        End If
    End Sub

    Private Sub aplicarversao011()

        Dim valor As String

        valor = m.consultaValor("SELECT count(*) FROM TDU_GP_CONFIGGRID WHERE CDU_FORM='FrmListaEncomendas' and CDU_GRID='gridArtigosENT' and CDU_CAMPO='CDU_ObraN8'")

        If valor = "0" Or valor = "" Then
            m.executarComando("INSERT INTO TDU_GP_CONFIGGRID (CDU_APL, CDU_FORM, CDU_GRID, CDU_CAMPO, CDU_DESCRICAO, CDU_TAMANHO, CDU_SUBQUERY, CDU_ORDEM, CDU_Administrador, CDU_EDITAVEL) VALUES ('M','FrmListaEncomendas','gridArtigosENT','CDU_ObraN8','Recp. por','90',NULL,6,0,0)", False)
        End If

        valor = m.consultaValor("SELECT count(*) FROM TDU_GP_CONFIGGRID WHERE CDU_FORM='FrmListaEncomendas' and CDU_GRID='gridArtigosENT' and CDU_CAMPO='CDU_DocRecp'")

        If valor = "0" Or valor = "" Then
            m.executarComando("INSERT INTO TDU_GP_CONFIGGRID (CDU_APL, CDU_FORM, CDU_GRID, CDU_CAMPO, CDU_DESCRICAO, CDU_TAMANHO, CDU_SUBQUERY, CDU_ORDEM, CDU_Administrador, CDU_EDITAVEL) VALUES ('M','FrmListaEncomendas','gridArtigosENT','CDU_DocRecp','Doc Recp.','90',NULL,7,0,0)", False)
        End If

    End Sub


    Private Sub aplicarversao012()
        Dim index As String

        If Not existeCampo("LinhasCompras", "CDU_DocRecp") Then
            index = daIndexProximoIndex("LinhasCompras")
            m.executarComando("ALTER TABLE LinhasCompras ADD CDU_DocRecp nvarchar(255)", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_DocRecp','Doc. Recepção','Doc. Recepção',1," + index + ",NULL,NULL,NULL,0)", False)

        End If

        'If Not existeCampo("Funcionarios", "CDU_Seccao") Then
        '    index = daIndexProximoIndex("Funcionarios")
        '    m.executarComando("ALTER TABLE Funcionarios ADD CDU_Seccao nvarchar(255)", False)
        '    m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_Seccao','Secção','Secção',1," + index + ",NULL,NULL,NULL,0)", False)
        'End If

    End Sub

    Private Sub aplicarversao013()
        Dim index As String

        If Not existeCampo("DocumentosCompra", "CDU_TipoDocGerar") Then
            index = daIndexProximoIndex("DocumentosCompra")
            m.executarComando("ALTER TABLE DocumentosCompra ADD CDU_TipoDocGerar nvarchar(5)", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('DocumentosCompra','CDU_TipoDocGerar','TipoDoc Gerar','TipoDoc Gerar',1," + index + ",NULL,NULL,NULL,0)", False)

        End If


        If Not existeCampo("DocumentosVenda", "CDU_TipoDocGerar") Then
            index = daIndexProximoIndex("DocumentosVenda")
            m.executarComando("ALTER TABLE DocumentosVenda ADD CDU_TipoDocGerar nvarchar(5)", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('DocumentosVenda','CDU_TipoDocGerar','TipoDoc Gerar','TipoDoc Gerar',1," + index + ",NULL,NULL,NULL,0)", False)

        End If

        'If Not existeCampo("Funcionarios", "CDU_Seccao") Then
        '    index = daIndexProximoIndex("Funcionarios")
        '    m.executarComando("ALTER TABLE Funcionarios ADD CDU_Seccao nvarchar(255)", False)
        '    m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('Funcionarios','CDU_Seccao','Secção','Secção',1," + index + ",NULL,NULL,NULL,0)", False)
        'End If

    End Sub


    Private Sub aplicarversao014()
        Dim index As String
        Dim comando As String

        If Not existeTabela("TDU_GP_LIGACAOPROJETOS") Then
            comando = "CREATE TABLE [TDU_GP_LIGACAOPROJETOS]([CDU_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_TDU_GP_LIGACAOPROJETOS_CDU_ID]  DEFAULT (newid()),	[CDU_ProjetoOrigem] [nvarchar](50) NULL,	[CDU_TipoDoc] [nvarchar](50) NULL,	[CDU_BDDestino] [nvarchar](50) NULL,	[CDU_ProjetoDestino] [nvarchar](50) NULL,	[CDU_idDoc] [uniqueidentifier] NULL,CONSTRAINT [PK_TDU_GP_LIGACAOPROJETOS] PRIMARY KEY CLUSTERED (	[CDU_ID] ASC))"

            m.executarComando(comando, True)

            comando = "DELETE FROM StdTabelasVar WHERE Tabela='TDU_GP_LIGACAOPROJETOS'"
            m.executarComando(comando, True)
            comando = "INSERT INTO StdTabelasVar VALUES ('TDU_GP_LIGACAOPROJETOS','ERP')"
            m.executarComando(comando, True)


            comando = "DELETE FROM StdCAmposVAR WHERE Tabela='TDU_GP_LIGACAOPROJETOS'"
            m.executarComando(comando, True)

        End If


        If Not existeCampo("LinhasCompras", "CDU_Empresa") Then
            index = daIndexProximoIndex("LinhasCompras")
            m.executarComando("ALTER TABLE LinhasCompras ADD CDU_Empresa nvarchar(50)", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_Empresa','Projeto Associado','Projeto Assoc.',1," + index + ",NULL,NULL,NULL,0)", False)
        End If


        If Not existeCampo("LinhasCompras", "CDU_ProjetoAssociado") Then
            index = daIndexProximoIndex("LinhasCompras")
            m.executarComando("ALTER TABLE LinhasCompras ADD CDU_ProjetoAssociado nvarchar(50)", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_ProjetoAssociado','Projeto Associado','Projeto Assoc.',1," + index + ",NULL,NULL,NULL,0)", False)
        End If

    End Sub



    Private Sub aplicarversao015()
        Dim index As String
        If Not existeCampo("LinhasCompras", "CDU_FuncAlteracao") Then
            index = daIndexProximoIndex("LinhasCompras")
            m.executarComando("ALTER TABLE LinhasCompras ADD CDU_FuncAlteracao nvarchar(50)", False)
            m.executarComando("INSERT INTO StdCAmposVAR ([Tabela],[Campo],[Descricao],[Texto],[Visivel],[Ordem],[Pagina],[ValorDefeito],[Query],[ExportarTTE])  VALUES ('LinhasCompras','CDU_FuncAlteracao','Func. Alteracao','Func. Alteracao.',1," + index + ",NULL,NULL,NULL,0)", False)

            m.executarComando("UPDATE  LinhasCompras SET LinhasCompras.CDU_FuncAlteracao = APS_GP_Funcionarios.Nome FROM LinhasCompras TabelaDestino INNER Join CabecCompras TabelaOrigem On TabelaDestino.IdCabecCompras = TabelaOrigem.id And TabelaOrigem.TipoDoc ='ALT'  Left join APS_GP_Funcionarios On TabelaOrigem.Utilizador= APS_GP_Funcionarios.Codigo ")
            m.executarComando("UPDATE  LinhasCompras SET LinhasCompras.CDU_FuncAlteracao = APS_GP_Funcionarios.Nome FROM LinhasCompras TabelaDestino INNER Join CabecCompras TabelaOrigem On TabelaDestino.IdCabecCompras = TabelaOrigem.id And TabelaOrigem.TipoDoc ='LST'  Left join APS_GP_Funcionarios On TabelaOrigem.Utilizador= APS_GP_Funcionarios.Codigo ")


        End If



    End Sub


    Private Function daIndexProximoIndex(tabela As String)
        Dim index As String
        index = m.consultaValor("SELECT MAX([Ordem]) FROM StdCAmposVAR WHERE Tabela='" + tabela + "'")
        If index = "" Then
            index = "1"
        End If
        Return index
    End Function


End Class
