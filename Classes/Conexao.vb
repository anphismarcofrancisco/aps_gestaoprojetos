﻿
Public Class Conexao
    Dim conexao As String
    Public Sub New(ByVal conexao)
        Me.conexao = conexao
    End Sub

    Protected Overridable Function getConexao() As String
        Return conexao
    End Function

    Public Function testarConexao() As String
        Dim conn As OleDb.OleDbConnection
        Try
            conn = New OleDb.OleDbConnection()
            conn.ConnectionString = conexao
            conn.Open()
            desligarConexao(conn)
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Overridable Function buscarConexao() As OleDb.OleDbConnection
        Dim conn As OleDb.OleDbConnection
        Try
            conn = New OleDb.OleDbConnection()
            conn.ConnectionString = conexao
            conn.Open()
            Return conn
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub desligarConexao(ByVal conn As OleDb.OleDbConnection)
        Try
            If conn Is Nothing Then Exit Sub
            conn.Close()
            conn = Nothing
        Catch ex As Exception
        End Try
    End Sub

    Public Sub executarComando(ByVal comando As OleDb.OleDbCommand)
        comando.ExecuteNonQuery()
    End Sub

    Public Sub executarComando(ByVal comand As String)
        Dim conn As OleDb.OleDbConnection
        Dim comando As OleDb.OleDbCommand
        Try
            conn = buscarConexao()

            comando = conn.CreateCommand
            comando.CommandText = comand
            comando.ExecuteNonQuery()

            desligarConexao(conn)
        Catch ex As Exception

        End Try
    End Sub

    Public Function executarConsulta(ByVal strQuery As String) As DataTable
        Dim conn As OleDb.OleDbConnection
        Dim adapter As OleDb.OleDbDataAdapter
        Dim myTable As DataTable = New DataTable()
        conn = Nothing
        Try
            conn = buscarConexao()
            adapter = New OleDb.OleDbDataAdapter(strQuery, conn)
            adapter.Fill(myTable)
            desligarConexao(conn)
            Return myTable
        Catch ex As Exception
            desligarConexao(conn)
            Return Nothing
        End Try
    End Function

    Public Function executarConsulta(ByVal comando As OleDb.OleDbCommand) As DataTable
        Dim conn As OleDb.OleDbConnection
        Dim adapter As OleDb.OleDbDataAdapter
        Dim myTable As DataTable = New DataTable()
        conn = Nothing
        Try
            conn = buscarConexao()
            comando.Connection = conn
            adapter = New OleDb.OleDbDataAdapter(comando)
            adapter.Fill(myTable)
            desligarConexao(conn)
            Return myTable
        Catch ex As Exception
            MsgBox(ex.Message)
            desligarConexao(conn)
            Return Nothing
        End Try
    End Function

    Public Function executarScalar(ByVal comando As OleDb.OleDbCommand, Optional ByVal defaultValue As Object = 0) As Object
        Dim conn As OleDb.OleDbConnection
        Dim obj As Object
        Dim myTable As DataTable = New DataTable()
        Dim desligar As Boolean = False
        conn = Nothing
        Try
            If comando.Connection Is Nothing Then
                conn = buscarConexao()
                comando.Connection = conn
                desligar = True
            End If

            obj = comando.ExecuteScalar()

            If desligar = True Then desligarConexao(conn)

            If obj Is Nothing Then
                Return defaultValue
            Else
                Return obj
            End If
        Catch ex As Exception
            '  MsgBox(ex.Message, MsgBoxStyle.Critical)
            If desligar = True Then desligarConexao(conn)
            Return defaultValue
        End Try
    End Function


    Public Function executarStoredProcedure(sp As String, id As String, dataInicial As String, dataFinal As String) As DataTable

        Dim conn As OleDb.OleDbConnection
        Dim comando As OleDb.OleDbCommand
        Dim adapter As OleDb.OleDbDataAdapter
        Dim myTable As DataTable = New DataTable()
        conn = Nothing
        Try
            conn = buscarConexao()
            comando = New OleDb.OleDbCommand(sp, conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Cliente", id)
            comando.Parameters.AddWithValue("@DataInicial", dataInicial)
            comando.Parameters.AddWithValue("@DataFinal", dataFinal)
            adapter = New OleDb.OleDbDataAdapter(comando)
            adapter.Fill(myTable)
            desligarConexao(conn)
            Return myTable
        Catch ex As Exception
            desligarConexao(conn)
            Return Nothing
        End Try
    End Function

End Class
