﻿Imports PlatAPSNET
Imports Interop.GcpBE900
Imports Interop.StdBE900
Imports Interop.ErpBS900
Imports System.IO
Imports ADODB
Imports Interop.StdPlatBS900

Public Class MotorRP

    Shared myInstance As MotorRP
    Dim bso As ErpBS
    Dim nomeEmpresa_ As String
    Dim codEmpresa As String
    Dim utilizador As String
    Dim password As String
    Dim tipoPlat As String
    Dim ficheiroAccess_ As String
    Dim file As String
    Dim log As TextBox
    Dim funcionario_ As String
    Dim modoFuncionamento As String
    Dim numLicenciamentos As Integer
    Dim StrnumLicenciamentos As String
    Dim objConfApl As StdBSConfApl
    Dim apliInicializada As Boolean
    Dim mostraNomeCliente As Boolean
    Dim integraLST As Boolean

    Dim numdias As Integer


    Dim multiselected As Boolean

    Private Sub New()
        Try

            '  AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf CurrentDomain_AssemblyResolve

            Dim ficheiroConfiguracao As Configuracao
            Dim ficheiroIni As IniFile

            ficheiroConfiguracao = Configuracao.GetInstance
            ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)

            bso = New ErpBS
            Dim seg As Seguranca
            seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")


            utilizador = ficheiroIni.GetString("ERP", "UTILIZADOR", "")
            utilizador = seg.decifrarStringTripleDES(utilizador)
            password = ficheiroIni.GetString("ERP", "PASSWORD", "")
            password = seg.decifrarStringTripleDES(password)
            codEmpresa = ficheiroIni.GetString("ERP", "EMPRESA", "")
            tipoPlat = ficheiroIni.GetString("ERP", "TIPOPLAT", 1)

            StrnumLicenciamentos = ficheiroIni.GetString("ERP", "NUMPOSTOS", "-1")

            tipoDoc_ = ficheiroIni.GetString("DOCUMENTORP", "TIPODOC", "")
            serie_ = ficheiroIni.GetString("DOCUMENTORP", "SERIE", "")
            tipoEntidade_ = ficheiroIni.GetString("DOCUMENTORP", "TIPOENTIDADE", "")
            modoFuncionamento = ficheiroIni.GetString("DOCUMENTORP", "MODO", ficheiroIni.GetString("ERP", "MODO", "M"))

            multiselected = ficheiroIni.GetString("DOCUMENTORP", "MULTIPLASELECCAO", "0") = "1"
            mostraNomeCliente = ficheiroIni.GetString("DOCUMENTORP", "MOSTRACLIENTE", "0") = "1"

            integraLST = ficheiroIni.GetString("DOCUMENTORP", "INTEGRALSTMAT", "0") = "1"

            numdias = ficheiroIni.GetInteger("DOCUMENTORP", "NUMDIASPONTOS", -1)


            If StrnumLicenciamentos = "-1" Then
                StrnumLicenciamentos = "999999"
            Else
                If StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos) Then
                    StrnumLicenciamentos = "0"
                Else
                    StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos)
                End If
            End If

            If IsNumeric(StrnumLicenciamentos) Then
                numLicenciamentos = CInt(StrnumLicenciamentos)
            Else
                numLicenciamentos = 0
            End If

            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = utilizador
            objConfApl.PwdUtilizador = password
            objConfApl.LicVersaoMinima = "9.00"

            '   MsgBox(tipoPlat + codEmpresa + utilizador + password + objConfApl.Instancia)
            bso.AbreEmpresaTrabalho(tipoPlat, codEmpresa, utilizador, password, , objConfApl.Instancia)






        Catch ex As Exception
            MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub




    Private Function existeCampo(tabela As String, campo As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = " select count(*) from sys.columns where name ='" + campo + "' and OBJECT_ID('" + tabela + "')=object_id"
        valor = consultaValor(consulta)
        Return valor <> "0"
    End Function


    ''' <summary>
    ''' Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Funcionario() As String
        Get
            Funcionario = funcionario_
        End Get
        Set(ByVal value As String)
            funcionario_ = value
        End Set
    End Property


    Dim tipoDoc_ As String = ""
    Property TipoDoc() As String
        Get
            TipoDoc = tipoDoc_
        End Get
        Set(ByVal value As String)
            tipoDoc_ = value
        End Set
    End Property

    Dim serie_ As String = ""
    Property Serie() As String
        Get
            Serie = serie_
        End Get
        Set(ByVal value As String)
            serie_ = value
        End Set
    End Property


    Dim tipoEntidade_ As String = "U"
    Property TipoEntidade() As String
        Get
            TipoEntidade = tipoEntidade_
        End Get
        Set(ByVal value As String)
            tipoEntidade_ = value
        End Set
    End Property

    Public Sub setLog(ByVal log_ As TextBox)
        log = log_
    End Sub

    Public Sub fechaEmpresa()
        bso.FechaEmpresaTrabalho()
    End Sub

    Public Shared Function GetInstance() As MotorRP
        If myInstance Is Nothing Then
            myInstance = New MotorRP
        End If
        Return myInstance
    End Function


    ReadOnly Property MostraCliente() As Boolean
        Get
            MostraCliente = mostraNomeCliente
        End Get

    End Property

    ReadOnly Property numDiasRegisto() As Integer
        Get
            Return numdias
        End Get

    End Property



    ReadOnly Property integraLstaMateriais() As Boolean
        Get
            integraLstaMateriais = integraLST
        End Get

    End Property

    ReadOnly Property MultiplaSeleccao() As Boolean
        Get
            MultiplaSeleccao = multiselected
        End Get

    End Property

    Private Function buscarIva(ByVal taxa As Double) As String
        Return consultaValor("SELECT Iva FROM iva WHERE Taxa=" + taxa.ToString())
    End Function

    Private Function buscarTaxaIva(ByVal codIva As String) As String
        Return consultaValor("SELECT Taxa FROM iva WHERE iva=" + codIva)
    End Function

    ''' <summary>
    ''' Função que permite a execução de um comando 
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function executarComando(ByVal comando As String, Optional mostraMsg As Boolean = True)
        Try
            bso.DSO.Plat.ExecSql.ExecutaXML(comando)
        Catch ex As Exception
            If mostraMsg Then MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

        Return ""
    End Function

    Public Function consulta(ByVal query As String) As StdBELista
        Return bso.Consulta(query)
    End Function

    Public Function consultaValor(ByVal query As String) As String
        Dim lista As StdBELista
        lista = bso.Consulta(query)
        If lista.NoFim Then
            Return ""
        Else
            Return lista.Valor(0).ToString
        End If
    End Function


    ''' <summary>
    ''' Função que permite validar a password do respectivo funcionario
    ''' </summary>
    ''' <param name="funcionario"></param>
    ''' <param name="password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function validaPassword(ByVal funcionario As String, ByVal password As String) As Boolean
        Try
            Dim numLinhas As String
            numLinhas = consultaValor("SELECT  COUNT(*) FROM Funcionarios WHERE Codigo='" + funcionario + "' and CDU_Password='" + password + "'")
            Return numLinhas = "1"
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function validaPostos(posto As String) As Boolean
        Dim numLinhas As Integer

        numLinhas = CInt(consultaValor("SELECT COUNT(*) FROM TDU_POSTOSTRABALHO"))

        If numLinhas <= numLicenciamentos Then

            numLinhas = CInt(consultaValor("SELECT COUNT(*) FROM TDU_POSTOSTRABALHO WHERE CDU_POSTO='" + posto + "'"))

            If numLinhas = 0 Then
                executarComando("INSERT INTO TDU_POSTOSTRABALHO(CDU_POSTO,CDU_ACTIVO) VALUES ('" + posto + "',1) ")
            Else
                actualizarPosto(posto, "1")
            End If
            Return True
        Else
            '  MsgBox("Não existem postos disponiveis, desactive algum dos postos", MsgBoxStyle.Critical)
            Return False
        End If


    End Function

    Public Sub actualizarPosto(posto As String, estado As String)
        executarComando("UPDATE TDU_POSTOSTRABALHO set CDU_ACTIVO=" + estado + " WHERE CDU_POSTO='" + posto + "'")
    End Sub

    ' ''' <summary>
    ' ''' Função que permite ir buscar o posto local
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Public Function daPosto() As String
    '    Dim ficheiroConfiguracao As Configuracao
    '    Dim ficheiroIni As IniFile

    '    ficheiroConfiguracao = Configuracao.GetInstance
    '    ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
    '    daPosto = ficheiroIni.GetString("ERP", "POSTO", "")
    '    ficheiroIni = Nothing
    'End Function


    Public Function convertRecordSetToDataTable(ByVal MyRs As ADODB.Recordset) As DataTable
        'Create and fill the dataset from the recordset and populate grid from Dataset. 
        Dim myDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter()
        Dim myDS As DataSet = New DataSet("MyTable")
        myDA.Fill(myDS, MyRs, "MyTable")
        Return myDS.Tables("MyTable")
    End Function


    Public Function firstDayMonth(ByVal data As Date) As Date
        Dim firstDay As DateTime
        firstDay = CDate("01-" + CStr(Month(data)) + "-" + CStr(Year(data)))
        Return firstDay
    End Function

    Public Function lastDayMonth(ByVal data As Date) As Date
        Dim lastDay As Date
        lastDay = CDate(data)
        lastDay = DateAdd(DateInterval.Month, 1, lastDay)
        lastDay = Convert.ToDateTime("1-" & Month(lastDay).ToString() & "-" & Year(lastDay).ToString())
        lastDay = DateAdd(DateInterval.Day, -1, lastDay)
        Return lastDay
    End Function

    Public Function daListaRegistos(ByVal centroTrabalho As String, ByVal molde As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim restricaoCT As String
        Dim restricaoMolde As String

        restricaoCT = ""
        restricaoMolde = ""

        If centroTrabalho <> "0" Then
            restricaoCT = "AND LinhasInternos.Artigo='" + centroTrabalho + "'"
        End If

        If molde <> "0" Then
            restricaoMolde = "AND COP_Obras.Codigo='" + molde + "'"
        End If

        If func = "0" Then
            lista = consulta("Select LinhasInternos.Estado, CabecInternos.Entidade as Codigo, CabecInternos.Nome as Funcionario, APS_GP_Postos.Nome as [Centro Trabalho],COP_Obras.Codigo as Molde,APS_GP_Pecas.Nome as Peça,APS_GP_Operacoes.Nome as Operação, LinhasInternos.CDU_DataObra as Data, Quantidade as Tempo,LinhasInternos.CDU_EstadoTrabalho as EstadoTrabalho, LinhasInternos.CDU_Observacoes as Observacoes,LinhasInternos.Id,LinhasInternos.Id CDU_IdLinha,LinhasInternos.IdLinhaOrigemCopia CDU_IdPonto FROM  LinhasInternos INNER JOIN CabecInternos  ON LinhasInternos.IdCabecInternos = CabecInternos.Id INNER JOIN COP_Obras on COP_Obras.ID=LinhasInternos.ObraID INNER JOIN APS_GP_Postos On LinhasInternos.Artigo=APS_GP_Postos.Codigo LEFT OUTER JOIN APS_GP_Operacoes On LinhasInternos.CDU_ObraN2=APS_GP_Operacoes.Codigo LEFT OUTER JOIN APS_GP_Pecas On LinhasInternos.CDU_ObraN1=APS_GP_Pecas.Codigo   WHERE CDU_DataObra>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataObra<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CabecInternos.Entidade IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT + " " + restricaoMolde + " order by CDU_DataObra")
        Else
            lista = consulta("Select LinhasInternos.Estado, CabecInternos.Entidade as Codigo, CabecInternos.Nome as Funcionario, APS_GP_Postos.Nome as [Centro Trabalho],COP_Obras.Codigo as Molde,APS_GP_Pecas.Nome as Peça,APS_GP_Operacoes.Nome as Operação, LinhasInternos.CDU_DataObra as Data, Quantidade as Tempo,LinhasInternos.CDU_EstadoTrabalho as EstadoTrabalho, LinhasInternos.CDU_Observacoes as Observacoes,LinhasInternos.Id,LinhasInternos.Id CDU_IdLinha,LinhasInternos.IdLinhaOrigemCopia CDU_IdPonto FROM  LinhasInternos INNER JOIN CabecInternos  ON LinhasInternos.IdCabecInternos = CabecInternos.Id INNER JOIN COP_Obras on COP_Obras.ID=LinhasInternos.ObraID INNER JOIN APS_GP_Postos On LinhasInternos.Artigo=APS_GP_Postos.Codigo LEFT OUTER JOIN APS_GP_Operacoes On LinhasInternos.CDU_ObraN2=APS_GP_Operacoes.Codigo LEFT OUTER JOIN APS_GP_Pecas On LinhasInternos.CDU_ObraN1=APS_GP_Pecas.Codigo  WHERE  CDU_DataObra>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataObra<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CabecInternos.entidade='" + func + "' " + restricaoCT + " " + restricaoMolde + " order by CDU_DataObra")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="func"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistos(ByVal centroTrabalho As String, ByVal molde As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As Double

        Dim restricaoCT As String
        Dim restricaoM As String

        restricaoCT = ""
        restricaoM = ""

        If centroTrabalho <> "0" Then
            restricaoCT = "AND LinhasInternos.Artigo='" + centroTrabalho + "'"
        End If

        If molde <> "0" Then
            restricaoM = "AND COP_Obras.Codigo='" + molde + "'"
        End If

        If func = "0" Then
            daTotalRegistos = consultaValor("Select ISNULL(SUM(Quantidade),0) as Tempo  FROM LinhasInternos inner join CabecInternos ON LinhasInternos.idCabecInternos=CabecInternos.Id  INNER JOIN COP_Obras on COP_Obras.ID=LinhasInternos.ObraID WHERE  CDU_DataObra>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataObra<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CabecInternos.Entidade IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT + restricaoM)
        Else
            daTotalRegistos = consultaValor("Select ISNULL(SUM( Quantidade),0) as Tempo FROM LinhasInternos inner join CabecInternos ON LinhasInternos.idCabecInternos=CabecInternos.Id INNER JOIN COP_Obras on COP_Obras.ID=LinhasInternos.ObraID WHERE  CDU_DataObra>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataObra<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CabecInternos.Entidade ='" + func + "' " + restricaoCT + restricaoM)
        End If
    End Function

    Public Function daListaFuncionariosPermissoes(Optional mostraTodos As Boolean = True) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        If mostraTodos Then
            lista = consulta("Select '0' as Codigo,'Todos' as Nome,'0' as Ordenacao UNION  SELECT Codigo,Nome,Nome as Ordenacao FROM Funcionarios WHERE Codigo='" + funcionario_ + "' UNION SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Funcionarios,TDU_UTILIZADORESPERMISSOES where TDU_UTILIZADORESPERMISSOES.cdu_utilizadorPermissao=APS_GP_Funcionarios.Codigo and TDU_UTILIZADORESPERMISSOES.CDU_Utilizador='" + funcionario_ + "' ORDER BY Ordenacao")
        Else
            lista = consulta("SELECT Codigo,Nome,Nome as Ordenacao FROM Funcionarios WHERE Codigo='" + funcionario_ + "' UNION SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Funcionarios,TDU_UTILIZADORESPERMISSOES where TDU_UTILIZADORESPERMISSOES.cdu_utilizadorPermissao=APS_GP_Funcionarios.Codigo and TDU_UTILIZADORESPERMISSOES.CDU_Utilizador='" + funcionario_ + "' ORDER BY Ordenacao")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todos os postos ao qual o funcionario tem acesso
    ''' </summary>
    ''' <param name="funcionario"></param>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaPostos(ByVal funcionario As String, ByVal restricao As String, Optional ByVal colocaTodos As Boolean = False) As StdBELista
        Try
            Dim lista As StdBELista
            Dim sqlTodos As String
            Dim ordenacao As String
            sqlTodos = ""
            ordenacao = ""
            If colocaTodos Then
                sqlTodos = "SELECT '0' as Codigo,'Todos' as Nome,'0' as Ordenacao  UNION "
            End If
            ordenacao = "ORDER BY Ordenacao"

            If restricao <> "" Then
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') and Nome LIKE '%" + restricao + "%' " + ordenacao)
            Else
                ' InputBox("", "", sqlTodos + "SELECT Codigo,Nome FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') ORDER BY Nome")
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') " + ordenacao)
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todos os funcionarios ou os funcionarios de um determinado posto
    ''' </summary>
    ''' <param name="postoTrabalho"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaFuncionarios(ByVal restricao As String, Optional ByVal postoTrabalho As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                restricao = " AND Nome LIKE '%" + restricao + "%'"
            End If
            If postoTrabalho <> "" Then
                lista = consulta("SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Funcionarios WHERE CDU_Posto='" + postoTrabalho + "' " + restricao + "ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Funcionarios Where 1=1 " + restricao + " ORDER BY Nome")
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaMoldes(Optional ByVal restricao As String = "", Optional ByVal colocaTodos As Boolean = False) As StdBELista
        Try
            Dim lista As StdBELista
            Dim sqlTodos As String
            Dim ordenacao As String
            sqlTodos = ""
            ordenacao = ""
            If colocaTodos Then
                sqlTodos = "SELECT '0' as Codigo,'Todos' as Nome,0  as Ordenacao  UNION "
            End If
            ordenacao = "ORDER BY Ordenacao,Codigo"

            If restricao <> "" Then
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,1 as Ordenacao FROM APS_GP_Moldes WHERE Nome LIKE '%" + restricao + "%' " + ordenacao)
            Else
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,1 as Ordenacao FROM APS_GP_Moldes " + ordenacao)
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaPecas(molde As String, Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            '  If Not integraLST Then
            If restricao <> "" Then
                    lista = consulta("SELECT * FROM APS_GP_Pecas WHERE Nome LIKE '%" + restricao + "%' Order by Nome")
                Else
                    lista = consulta("SELECT * FROM APS_GP_Pecas Order By Nome")
                End If
            ' Else
            ' lista = daListaPecasListaMateriais(restricao)
            ' End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaOperacoes(posto As String, ByVal funcionario As String, Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            Dim valor As String

            valor = consultaValor("SELECT count(*) FROM TDU_PostosOperacoes WHERE CDU_Posto='" + posto + "'")

            If valor = "0" Then
                If restricao <> "" Then
                    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "') and Nome LIKE '%" + restricao + "%' Order By Nome")
                Else
                    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "') Order By Nome")
                End If
            Else
                If restricao <> "" Then
                    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "' and CDU_Operacao in (SELECT CDU_Operacao FROM TDU_PostosOperacoes WHERE CDU_Posto='" + posto + "')) and Nome LIKE '%" + restricao + "%' Order By Nome")
                Else
                    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "' and CDU_Operacao in (SELECT CDU_Operacao FROM TDU_PostosOperacoes WHERE CDU_Posto='" + posto + "')) Order By Nome")
                End If
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    '''  Ir buscar a lista de estados de trabalho
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaEstadosTrabalho() As StdBELista
        Try
            Dim lista As StdBELista
            lista = consulta("SELECT CDU_Estado as Codigo,CDU_Descricao as Descricao FROM TDU_EstadosTrabalho Order By CDU_Descricao")
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub removerRegisto(ByVal func As String, ByVal postos As List(Of String), ByVal molde As String, ByVal peca As String, ByVal operacao As String, ByVal idLinha As List(Of String))
        Dim comando As String = ""
        Dim valor As String
        If idLinha.Count > 0 Then
            For Each valor In idLinha
                comando = "DELETE FROM  TDU_RegistoFolhaPonto WHERE CDU_Idlinha='" + valor + "'"
                executarComando(comando)
                removerLinhaDocumentoVenda(valor)
            Next
        Else
            For Each valor In postos
                comando = "DELETE FROM  TDU_RegistoFolhaPonto WHERE CDU_Funcionario='" + func + "' AND CDU_Molde='" + molde + "' AND CDU_Peca='" + peca + "' AND CDU_Operacao='" + operacao + "' AND CDU_Posto='" + valor + "' and CDU_IdLinha IS NULL"
                executarComando(comando)
                Dim id As String
                id = consultaValor("SELECT LinhasInternos.id from LinhasInternos INNER JOIN CabecInternos on LinhasInternos.IdCabecInternos=CabecInternos.Id INNER JOIN COP_Obras On LinhasInternos.ObraID=COP_Obras.Id WHERE CabecInternos.Entidade='" + func + "' and COP_Obras.Codigo='" + molde + "' and LinhasInternos.CDU_ObraN1='" + peca + "' and  LinhasInternos.CDU_ObraN2='" + operacao + "' and LinhasInternos.Artigo='" + valor + "' and LinhasInternos.Quantidade=0 and LinhasInternos.CDU_DataObraFinal IS NULL")
                removerLinhaDocumentoVenda(id)
            Next
        End If

    End Sub
    ''' <summary>
    ''' funcão que permite iniciar uma operacao
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="molde"></param>
    ''' <param name="peca"></param>
    ''' <param name="operacao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function iniciarOperacao(artigo As String, ByVal posto As String, ByVal molde As String, ByVal peca As String, ByVal operacao As String, idPonto As String, postos As List(Of String)) As String
        Dim msg As String
        Dim comando As String
        iniciarOperacao = ""
        '  If idLinha = "" Then
        msg = obraOperacaoJaIniciada(artigo, postos, molde, peca, operacao, "", )
        If msg = "" Then
            Try
                Dim idLinha As String
                idLinha = consultaValor("SELECT NEWID()")

                idLinha = actualizarDocumentoInterno(funcionario_, artigo, molde, peca, operacao, CDate(Now), 0, idLinha, CDate(Now), Nothing, "", "", idPonto)

                comando = "UPDATE LInhasInternos set CDU_DataObraFinal=NULL WHERE id='" + idLinha + "'"
                executarComando(comando)

                comando = "INSERT INTO TDU_RegistoFolhaPonto(CDU_ID,CDU_Molde,CDU_Peca,CDU_Operacao,CDU_Funcionario,CDU_Posto,CDU_DataInicial,CDU_idPonto) VALUES(NEWID(),'" + molde + "','" + peca + "','" + operacao + "','" + funcionario_ + "','" + posto + "',Convert(datetime,'" + CStr(Now) + "',105),'" + idPonto + "')"
                executarComando(comando)

            Catch ex As Exception
                iniciarOperacao = "Motor L-339 : " + ex.Message
            End Try
        Else
            iniciarOperacao = vbCrLf + vbCrLf + "O Posto " + posto + " já se encontra iniciado com o utilizador " + msg
        End If
        'Else
        '    comando = "UPDATE TDU_RegistoFolhaPonto set CDU_Posto='" + posto + "', CDU_Molde='" + molde + "' ,CDU_Peca='" + peca + "',CDU_Operacao=='" + operacao + "',CDU_DataInicial=Convert(datetime,'" + CStr(Now) + "',105) WHERE CDU_idLinha='" + idLinha + "'"
        '    executarComando(comando)
        'End If
    End Function


    Public Sub TerminarOperacaoFinal(ByVal posto As String, ByVal artigo As String, ByVal molde As String, ByVal peca As String, ByVal operacao As String, ByVal dataInicial As DateTime, ByVal dataFinal As DateTime, ByVal tempo As Double, ByVal estado As String, ByVal observacoes As String, ByVal idRegisto As String, ByVal idlinha As String, Id_Ponto As String)

        Dim comando As String
        Dim lista As StdBELista

        If idRegisto = "" Then
            '   InputBox("", "", "SELECT CDU_ID FROM TDU_RegistoFolhaPonto WHERE CDU_Molde='" + molde + "' and  CDU_Peca='" + peca + "' AND CDU_Operacao='" + operacao + "' and CDU_Posto='" + posto + "' and CDU_Funcionario='" + funcionario_ + "' AND CDU_Idlinha IS NULL")
            lista = consulta("SELECT CDU_ID,CDU_IDPonto FROM TDU_RegistoFolhaPonto WHERE CDU_Molde='" + molde + "' and  CDU_Peca='" + peca + "' AND CDU_Operacao='" + operacao + "' and CDU_Posto='" + posto + "' and CDU_Funcionario='" + funcionario_ + "' AND CDU_Idlinha IS NULL")
            If Not lista.NoFim Then
                idRegisto = lista.Valor("CDU_IDPonto")
            End If
        End If
        If idRegisto = "" Then
            comando = "INSERT INTO TDU_RegistoFolhaPonto(CDU_ID,CDU_Molde,CDU_Peca,CDU_Operacao,CDU_Funcionario,CDU_Posto,CDU_DataInicial,CDU_DataFinal,CDU_Tempo,CDU_EstadoTrabalho,CDU_Observacoes,CDU_IdLinha,CDU_idPonto) VALUES(NEWID(),'" + molde + "','" + peca + "','" + operacao + "','" + funcionario_ + "','" + posto + "',Convert(datetime,'" + CStr(dataInicial) + "',105),Convert(datetime,'" + CStr(dataFinal) + "',105)," + Replace(tempo, ",", ".") + ",'" + estado + "','" + observacoes + "','" + idlinha + "','" + Id_Ponto + "')"
        Else
            comando = "DELETE FROM TDU_RegistoFolhaPonto WHERE CDU_IdPonto='" + idRegisto + "'"
            executarComando(comando)
            'comando = "UPDATE TDU_RegistoFolhaPonto SET  CDU_Molde='" + molde + "', CDU_Peca='" + peca + "', CDU_Operacao='" + operacao + "', CDU_Posto='" + posto + "',CDU_DataFinal=Convert(datetime,'" + CStr(dataFinal) + "',105),CDU_Tempo=" + Replace(tempo, ",", ".") + ",CDU_EstadoTrabalho='" + estado + "',CDU_Observacoes='" + observacoes + "',CDU_IdLinha='" + idlinha + "' WHERE CDU_Id='" + idRegisto + "'"
            comando = "INSERT INTO TDU_RegistoFolhaPonto(CDU_ID,CDU_Molde,CDU_Peca,CDU_Operacao,CDU_Funcionario,CDU_Posto,CDU_DataInicial,CDU_DataFinal,CDU_Tempo,CDU_EstadoTrabalho,CDU_Observacoes,CDU_IdLinha,CDU_idPonto) VALUES(NEWID(),'" + molde + "','" + peca + "','" + operacao + "','" + funcionario_ + "','" + posto + "',Convert(datetime,'" + CStr(dataInicial) + "',105),Convert(datetime,'" + CStr(dataFinal) + "',105)," + Replace(tempo, ",", ".") + ",'" + estado + "','" + observacoes + "','" + idlinha + "','" + Id_Ponto + "')"

        End If
        executarComando(comando)
    End Sub

    ''' <summary>
    ''' Função que permite validar se um posto já se encontra inicializado
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="molde"></param>
    ''' <param name="peca"></param>
    ''' <param name="operacao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obraOperacaoJaIniciada(posto As String, ByVal postos As List(Of String), ByVal molde As String, ByVal peca As String, ByVal operacao As String, ByRef funcionarioPosto As String, Optional ByVal mostraMSG As Boolean = True) As String

        If postos.Count > 0 Then
            Dim lista As StdBELista
            lista = consulta("SELECT IdLinhaOrigemCopia as CDU_IdPonto, CabecInternos.Entidade as CDU_Funcionario, CabecInternos.Nome as CDU_FuncionarioNome FROM LinhasInternos INNER JOIN CabecInternos ON LinhasInternos.IdCabecInternos=CabecInternos.Id  WHERE Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
            obraOperacaoJaIniciada = ""
            If Not lista.NoFim Then
                Dim total As Integer
                Dim idPonto As String = lista.Valor("CDU_IdPonto")

                If idPonto = "" Then Exit Function
                total = consultaValor("SELECT Count(*) FROM LinhasInternos WHERE IdLinhaOrigemCopia='" + lista.Valor("CDU_IdPonto") + "' and Quantidade=0 and CDU_DataObraFinal IS NULL ")

                If total > postos.Count Then
                    Return "Existem Postos que se encontram incializados e que não se encontram seleccionados" + vbCrLf + vbCrLf + buscarRegistosPostosNaoSeleccionados(idPonto, postos)
                End If

                obraOperacaoJaIniciada += validarPontosMesmoRegisto(posto, idPonto, postos, mostraMSG)

                funcionarioPosto = lista.Valor("CDU_Funcionario")

                If obraOperacaoJaIniciada = "" Then
                    obraOperacaoJaIniciada = funcionarioPosto
                End If
            End If
        End If
    End Function

    Private Function buscarRegistosPostosNaoSeleccionados(idPonto As String, postos As List(Of String)) As String
        Dim lista As StdBELista
        Dim encontrou As Boolean
        buscarRegistosPostosNaoSeleccionados = ""
        lista = consulta("SELECT TDU_Postos.CDU_Descricao as CDU_Posto FROM  LinhasInternos INNER JOIN TDU_Postos ON  LinhasInternos.Artigo=TDU_Postos.CDU_Artigo WHERE IdLinhaOrigemCopia='" + idPonto + "'")

        While Not lista.NoFim
            encontrou = True
            For Each posto In postos
                If posto = lista.Valor("CDU_Posto") Then
                    encontrou = True
                    Exit For
                End If
            Next
            If Not encontrou Then
                buscarRegistosPostosNaoSeleccionados += lista.Valor("CDU_Posto") + vbCrLf
            End If
            lista.Seguinte()
        End While
    End Function

    Private Function validarPontosMesmoRegisto(postoOrig As String, idPonto As String, postos As List(Of String), mostraMSG As Boolean) As String
        Dim posto As String
        Dim id As String
        Dim lista As StdBELista
        Dim erro As String = ""
        erro = ""
        For Each posto In postos
            lista = consulta("SELECT APS_GP_Postos.Nome as CDU_Posto, CabecInternos.Entidade as  CDU_Funcionario,COp_Obras.Codigo as CDU_Molde, CDU_ObraN1 as CDU_Peca, CDU_ObraN2 as CDU_Operacao, IdLinhaOrigemCopia as CDU_IdPonto FROM LinhasInternos INNER JOIN CabecInternos ON LinhasInternos.IdCabecInternos=CabecInternos.Id INNER JOIN COP_Obras on LinhasInternos.ObraID=COP_Obras.ID  INNER JOIN APS_GP_Postos On APS_GP_Postos.Codigo=LInhasInternos.Artigo WHERE  Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
            If Not lista.NoFim Then
                If lista.Valor("CDU_IdPonto") <> idPonto Or lista.Valor("CDU_Funcionario") <> funcionario_ Then
                    If erro = "" Then erro += "O erro do Posto '" + postoOrig + "' não pertende ao mesmo registo dos postos:" + vbCrLf
                    erro = lista.Valor("CDU_Funcionario")
                    If mostraMSG Then
                        erro = consultaValor("Select Nome FROM Funcionarios where Codigo='" + lista.Valor("CDU_Funcionario") + "'")
                        erro += vbCrLf + vbCrLf + "Centro Trabalho: " + lista.Valor("CDU_Posto")
                        erro += vbCrLf + "Molde: " + lista.Valor("CDU_Molde")
                        erro += vbCrLf + "Peça: " + lista.Valor("CDU_Peca")
                        erro += vbCrLf + "Operação: " + lista.Valor("CDU_Operacao") 'consultaValor("SELECT CDU_Descricao FROM TDU_ObraN2 WHERE CDU_ObraN2='" + lista.Valor("CDU_Operacao") + "'")
                    End If
                End If
            End If
        Next

        Return erro
    End Function
    ''' <summary>
    ''' Função que permite validar se um posto já se encontra inicializado
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="molde"></param>
    ''' <param name="peca"></param>
    ''' <param name="operacao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obraPodeSerTerminada(ByVal posto As String, ByVal molde As String, ByVal peca As String, ByVal operacao As String, Optional ByVal mostraMSG As Boolean = True) As String
        Dim lista As StdBELista
        lista = consulta("SELECT APS_GP_Postos.Nome as CDU_Posto, CDU_Obran1 as  CDU_Peca, CDU_ObraN2 as CDU_Operacao, CabecInternos.entidade as CDU_Funcionario FROM LinhasInternos inner Join APS_GP_Postos ON LinhasInternos.Artigo=APS_GP_Postos.Codigo INNER JOIN COP_Obras on LinhasInternos.ObraID=COP_Obras.ID INNER JOIN CabecInternos ON LinhasInternos.Idcabecinternos=CabecInternos.ID WHERE COP_Obras.Codigo='" + molde + "' AND CDU_ObraN1='" + peca + "' AND CDU_ObraN2='" + operacao + "' AND Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        obraPodeSerTerminada = ""
        If lista.NoFim Then
            obraPodeSerTerminada = funcionario_  'lista.Valor("CDU_Funcionario")
            If mostraMSG Then
                lista = consulta("SELECT APS_GP_Postos.Nome as CDU_Posto, CDU_Obran1 as  CDU_Peca, CDU_ObraN2 as CDU_Operacao, CabecInternos.entidade as CDU_Funcionario,COP_Obras.Codigo as CDU_Molde  FROM LinhasInternos inner Join APS_GP_Postos ON LinhasInternos.Artigo=APS_GP_Postos.Codigo INNER JOIN COP_Obras on LinhasInternos.ObraID=COP_Obras.ID INNER JOIN CabecInternos ON LinhasInternos.Idcabecinternos=CabecInternos.ID WHERE Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
                If Not lista.NoFim Then
                    obraPodeSerTerminada = vbCrLf + vbCrLf + consultaValor("Select Nome FROM Funcionarios where Codigo='" + lista.Valor("CDU_Funcionario") + "'")
                    obraPodeSerTerminada += vbCrLf + vbCrLf + "Centro Trabalho: " + lista.Valor("CDU_Posto")
                    obraPodeSerTerminada += vbCrLf + "Molde: " + lista.Valor("CDU_Molde")
                    obraPodeSerTerminada += vbCrLf + "Peça: " + lista.Valor("CDU_Peca")
                    obraPodeSerTerminada += vbCrLf + "Operação: " + lista.Valor("CDU_Operacao") 'consultaValor("SELECT CDU_Descricao FROM TDU_ObraN2 WHERE CDU_ObraN2='" + lista.Valor("CDU_Operacao") + "'")
                End If
            End If
        End If
    End Function


    Public Function TerminarOperacao(ByVal postos As List(Of String), ByVal artigo As List(Of String), ByVal molde As String, ByVal peca As String, ByVal operacao As String, ByVal idRegisto As String, ByVal idLinha As List(Of String), ByRef msg As String) As Integer
        Return abrirFormTerminar(postos, artigo, molde, peca, operacao, idRegisto, idLinha, msg)
    End Function

    Public Function abrirFormTerminar(ByVal postos As List(Of String), ByVal artigos As List(Of String), ByVal molde As String, ByVal peca As String, ByVal operacao As String, ByVal idRegisto As String, ByVal idLinhas As List(Of String), ByRef msg As String) As Integer
        Dim lista As StdBELista
        Dim artigo As String
        lista = New StdBELista
        If idRegisto = "" Then
            If artigos.Count > 0 Then
                artigo = artigos(0)
                lista = consulta("SELECT CDU_DataObra as CDU_DataInicial, CDU_DataObraFinal as CDU_DataFinal,Quantidade as CDU_Tempo,CabecInternos.Entidade as CDU_Funcionario, CDU_EstadoTrabalho,CDU_Observacoes, idLinhaOrigemCopia,LinhasInternos.Id FROM LinhasInternos Inner JOIN CabecInternos  ON LinhasInternos.IdCabecInternos=CabecInternos.Id INNER JOIN COP_Obras on LinhasInternos.ObraID=COP_Obras.ID  WHERE Codigo='" + molde + "' AND CDU_ObraN1='" + peca + "' AND CDU_ObraN2='" + operacao + "' AND Artigo='" + artigo + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
            End If
        Else
            lista = consulta("SELECT  CDU_DataObra as CDU_DataInicial, CDU_DataObraFinal as CDU_DataFinal,Quantidade as CDU_Tempo,CabecInternos.Entidade as CDU_Funcionario, CDU_EstadoTrabalho,CDU_Observacoes,idLinhaOrigemCopia,LinhasInternos.Id FROM LinhasInternos Inner JOIN CabecInternos  ON LinhasInternos.IdCabecInternos=Cabecinternos.id INNER JOIN COP_Obras on LinhasInternos.ObraID=COP_Obras.ID WHERE IdLinhaOrigemCopia='" + idRegisto + "'")
        End If

        Dim frmT As FrmTerminarRP
        frmT = New FrmTerminarRP
        frmT.Funcionario = funcionario_

        If Not lista.NoFim Then
            frmT.DataInicio = NuloToDate(lista.Valor("CDU_DataInicial"))
            frmT.DataFim = NuloToDate(lista.Valor("CDU_DataFinal"))
            frmT.Tempo = NuloToDouble(lista.Valor("CDU_Tempo"))
            If frmT.Tempo < 0 Then
                frmT.Tempo = frmT.Tempo * -1
            End If
            frmT.Funcionario = lista.Valor("CDU_Funcionario")
            frmT.Estado = lista.Valor("CDU_EstadoTrabalho")
            frmT.Observacoes = lista.Valor("CDU_Observacoes")
            If idRegisto = "" Then
                idRegisto = lista.Valor("idLinhaOrigemCopia")
                While Not lista.NoFim
                    idLinhas.Add(lista.Valor("ID"))
                    lista.Seguinte()
                End While
            End If

        End If

        frmT.idRegisto = idRegisto
        frmT.idLinhas = idLinhas
        frmT.CentroTrabalho = postos
        frmT.Artigo = artigos
        frmT.Molde = molde
        frmT.Peca = peca
        frmT.Operacao = operacao

        Try
            frmT.ShowDialog()
            abrirFormTerminar = frmT.BotaoSeleccionado
            msg = frmT.MensagemDocumento
            frmT.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return 3
        End Try

    End Function
    Public Function NuloToDate(ByVal obj) As DateTime
        If IsDBNull(obj) Then
            NuloToDate = Now
        Else
            If Not IsDate(obj) Then
                NuloToDate = Now
            Else
                NuloToDate = obj
            End If
        End If
    End Function

    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                NuloToDouble = 0
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Public Sub apagarLinhasDocumentosInternos(idLinhas As List(Of String))
        Dim id As String
        Dim idLinha As String
        Dim docI As GcpBEDocumentoInterno
        Dim index As Integer
        docI = Nothing

        If idLinhas.Count = 0 Then
            Return
        End If

        idLinha = idLinhas(0)

        If idLinha <> "" Then
            id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idLinha + "'")
            If id <> "" Then
                docI = bso.Comercial.Internos.EditaID(id)
            Else
                idLinha = ""
            End If
        End If

        'permite verificar se a data da linha é alterada e caso seja para retirar do documento e inserir noutro
        If Not docI Is Nothing Then
            For Each idLinha In idLinhas
                index = 1
                For Each linha In docI.Linhas
                    If linha.ID = idLinha Then
                        docI.Linhas.Remove(index)
                        Exit For
                    End If
                    index = index + 1
                Next
            Next
            bso.Comercial.Internos.Actualiza(docI)
            docI = Nothing
            idLinha = ""
        End If

    End Sub

    Public Function actualizarDocumentoInterno(ByVal func As String, ByVal artigo As String, ByVal molde As String, ByVal peca As String, ByVal operacao As String, ByVal data As Date, ByVal tempo As Double, ByVal idLinha As String, dataInicial As Date, DataFinal As Date, estado As String, observacoes As String, idPonto As String) As String
        Dim docI As GcpBEDocumentoInterno
        Dim linhaI As GcpBELinhaDocumentoInterno
        Dim linha As GcpBELinhaDocumentoInterno
        Dim id As String
        Dim index As Integer
        docI = Nothing
        Dim dataI As String
        dataI = getDateDocument(data)
        idLinha = UCase(idLinha)
        If idLinha <> "" Then
            id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idLinha + "'")
            If id <> "" Then
                docI = bso.Comercial.Internos.EditaID(id)
            Else
                idLinha = ""
            End If
            'permite verificar se a data da linha é alterada e caso seja para retirar do documento e inserir noutro
            If Not docI Is Nothing Then
                If CDate(dataI) <> docI.Data Then
                    index = 1
                    For Each linha In docI.Linhas
                        If UCase(linha.ID) = idLinha Then
                            docI.Linhas.Remove(index)
                            Exit For
                        End If
                        index = index + 1
                    Next
                    bso.Comercial.Internos.Actualiza(docI)
                    docI = Nothing
                    idLinha = ""
                End If
            End If
        End If

        If idLinha = "" Then
            id = buscarIdDocumento(func, dataI)
            If id <> "" Then
                docI = bso.Comercial.Internos.EditaID(id)
            End If
        End If
        '
        ' MsgBox("1")
        If docI Is Nothing Then
            docI = New GcpBEDocumentoInterno
            docI.Tipodoc = tipoDoc_
            If serie_ <> "" Then
                docI.Serie = serie_
            Else
                docI.Serie = bso.Comercial.Series.DaSerieDefeito("N", tipoDoc_, CDate(dataI))
            End If
            docI.Entidade = func
            docI.TipoEntidade = tipoEntidade_
            bso.Comercial.Internos.PreencheDadosRelacionados(docI)

            docI.Nome = Left(docI.Nome, 50)
            docI.Morada = Left(docI.Morada, 50)
            docI.Localidade = Left(docI.Localidade, 50)
            docI.CodPostalLocalidade = Left(docI.CodPostalLocalidade, 50)

            docI.Data = CDate(dataI)
            docI.DataVencimento = docI.Data
        End If

        linhaI = Nothing
        If idLinha <> "" Then
            For Each linha In docI.Linhas
                If UCase(linha.ID) = idLinha Then
                    linhaI = linha
                    Exit For
                End If
            Next
        End If
        '  MsgBox("2")
        If linhaI Is Nothing Then
            bso.Comercial.Internos.AdicionaLinha(docI, artigo, , , , , , tempo)
            linhaI = docI.Linhas(docI.Linhas.NumItens)
        Else
            linhaI.Artigo = artigo
            linhaI.Descricao = bso.Comercial.Artigos.DaValorAtributo(artigo, "Descricao")
        End If
        '  MsgBox("3")
        linhaI.Quantidade = tempo
        ' linhaI.PrecoUnitario = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")).PVP1
        linhaI.ObraID = bso.Comercial.Projectos.DaValorAtributo(molde, "ID")
        linhaI.CamposUtil("CDU_ObraN1").Valor = Left(peca, 100)
        linhaI.CamposUtil("CDU_ObraN2").Valor = Left(operacao, 100)
        linhaI.CamposUtil("CDU_DataObra").Valor = dataInicial

        If linhaI.CamposUtil.Existe("CDU_Funcionario") Then
            linhaI.CamposUtil("CDU_Funcionario").Valor = funcionario_
        End If

        If linhaI.CamposUtil.Existe("CDU_DataObraFinal") Then
            If IsNothing(DataFinal) Then
                linhaI.CamposUtil("CDU_DataObraFinal").Valor = DBNull.Value
            Else
                linhaI.CamposUtil("CDU_DataObraFinal").Valor = DataFinal
            End If

        End If

        If linhaI.CamposUtil.Existe("CDU_EstadoTrabalho") Then
            linhaI.CamposUtil("CDU_EstadoTrabalho").Valor = estado
        End If

        If linhaI.CamposUtil.Existe("CDU_Observacoes") Then
            linhaI.CamposUtil("CDU_Observacoes").Valor = observacoes
        End If

        linhaI.IdLinhaOrigemCopia = idPonto

        idLinha = linhaI.ID

        '  MsgBox("4")
        Try
            bso.Comercial.Internos.Actualiza(docI)
            Return idLinha
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function


    Private Sub removerLinhaDocumentoVenda(ByVal idLinha As String)
        Dim id As String
        Dim docI As GcpBEDocumentoInterno
        Dim linha As GcpBELinhaDocumentoInterno

        id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idLinha + "'")
        If id = "" Then
            Exit Sub
        Else
            docI = bso.Comercial.Internos.EditaID(id)
        End If

        Dim i As Integer
        i = 1
        For Each linha In docI.Linhas
            If linha.ID = idLinha Then
                ' linhaI = linha
                docI.Linhas.Remove(i)
                Exit For
            End If
            i = i + 1
        Next

        Try
            bso.Comercial.Internos.Actualiza(docI)
        Catch ex As Exception

        End Try


    End Sub

    '''' <summary>
    '''' Função que permite criar toda a extrutura da BD exitente
    '''' </summary>
    '''' <remarks></remarks>
    'Private Sub inicializarAplicacao()
    '    Dim lista As StdBELista
    '    lista = consulta("SELECT *  FROM sysobjects WHERE  name='APS_GP_Funcionarios'")
    '    If lista.NoFim Then
    '        executarComando("CREATE VIEW APS_GP_Funcionarios AS SELECT TOP (10000000) dbo.Funcionarios.* FROM Funcionarios")
    '    End If
    'End Sub

    ''' <summary>
    ''' Funcao que permite validar se uma peça existe
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function pecaExiste(ByVal peca As String) As Boolean
        Dim peca1 As String
        peca1 = consultaValor("SELECT CDU_ObraN1 FROm TDU_ObraN1 WHERE CDU_ObraN1='" + peca + "'")
        pecaExiste = peca1 <> ""
    End Function

    ''' <summary>
    ''' Função que permite inserir uma peça no sistema
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <param name="descricao"></param>
    ''' <remarks></remarks>
    Public Sub inserirPeca(ByVal peca As String, ByVal descricao As String)
        executarComando("INSERT INTO TDU_ObraN1(CDu_ObraN1,CDU_Descricao) VALUES('" + peca + "','" + descricao + "')")
    End Sub


    Private Function getDateDocument(ByVal data As Date) As String
        Dim dataI As String
        dataI = ""
        If modoFuncionamento = "D" Then
            dataI = CStr(data.Day) + "-" + CStr(data.Month) + "-" + CStr(data.Year)
        End If

        If modoFuncionamento = "S" Then
            data = data.AddDays(-data.DayOfWeek + 1)
            dataI = CStr(data.Day) + "-" + CStr(data.Month) + "-" + CStr(data.Year)
        End If

        If modoFuncionamento = "M" Then
            dataI = "01" + "-" + CStr(Month(data)) + "-" + CStr(Year(data))
        End If
        Return dataI
    End Function
    Private Function buscarIdDocumento(ByVal func As String, ByVal data As Date) As String
        Dim dataI As String
        Dim serie As String
        dataI = getDateDocument(data)

        If serie_ <> "" Then
            serie = serie_
        Else
            serie = bso.Comercial.Series.DaSerieDefeito("N", tipoDoc_, dataI)
        End If

        buscarIdDocumento = consultaValor("SELECT Id FROM CabecInternos where Entidade='" + func + "' and TipoDoc='" + tipoDoc_ + "' and Serie='" + serie + "' and Data=Convert(Datetime,'" + dataI + "',105)")
    End Function

    Public Function daValorStringIni(ByVal seccao As String, ByVal entrada As String, Optional ByVal valordefeito As String = "") As String
        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        Return ficheiroIni.GetString(seccao, entrada, valordefeito)
    End Function

    Public Sub gravarValorStringIni(ByVal seccao As String, ByVal entrada As String, ByVal valor As String)
        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        ficheiroIni.WriteString(seccao, entrada, valor)
    End Sub

    Public Function getFuncionarioAdministrador() As Boolean


        Dim m As Motor

        m = Motor.GetInstance

        Return m.VerficarFuncionarioAdm(funcionario_, "RP")

        'Dim valor As String
        'Dim func As String
        'func = funcionario_
        'Try
        '    valor = consultaValor("Select IsNull(CDU_Administrador,0) FROM Funcionarios WHERE Codigo='" + func + "'")
        '    'InputBox("", "", "Select IsNull(CDU_Administrador,0) FROM Funcionarios WHERE Codigo='" + func + "' --> " + valor)
        '    Return LCase(valor) = "true"
        'Catch ex As Exception
        '    Return True
        'End Try

    End Function


    Public Function getFuncionarioAddPecas() As Boolean
        Dim valor As String
        Dim func As String
        func = funcionario_
        Try
            valor = consultaValor("Select IsNull(CDU_AdicionaPecas,0) FROM Funcionarios WHERE Codigo='" + func + "'")
            Return LCase(valor) = "true"
        Catch ex As Exception
            Return True
        End Try

    End Function


    Property AplicacaoInicializada() As Boolean
        Get
            AplicacaoInicializada = apliInicializada
        End Get
        Set(ByVal value As Boolean)
            apliInicializada = value
        End Set
    End Property


    Public Sub setEmpresa(ByVal empresa_ As String)
        If codEmpresa <> empresa_ Then
            Try
                bso.FechaEmpresaTrabalho()
                bso.AbreEmpresaTrabalho(tipoPlat, empresa_, utilizador, password)
                codEmpresa = empresa_
            Catch ex As Exception
                MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            End Try
        End If

    End Sub

    Public Function daNomeClienteProjecto(projecto As String) As String
        Dim cliente As String
        cliente = consultaValor("SELECT Clientes.Cliente + ' - ' + Clientes.Nome  FROM CLientes INNER JOIN COP_Obras On Clientes.Cliente=COP_Obras.ERPEntidadeA and COP_Obras.Codigo='" + projecto + "'")
        Return cliente
    End Function


    Public Function daMoldeSeleccionado(posto As String) As String
        Dim molde As String
        molde = consultaValor("SELECT cop_obras.Codigo FROM LinhasInternos INNER JOIN COP_Obras ON LinhasInternos.ObraID=COP_Obras.Id  WHERE Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        Return molde
    End Function

    Public Function daPecaSeleccionado(posto As String) As String
        Dim peca As String
        peca = consultaValor("SELECT CDu_ObraN1 FROM LinhasInternos WHERE Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        Return peca
    End Function

    Public Function daOperacaoSeleccionado(posto As String) As String
        Dim operacao As String
        operacao = consultaValor("SELECT CDU_ObraN2 FROM LinhasInternos WHERE Artigo='" + posto + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        Return operacao
    End Function



    Public Function daPostoSeleccionadoMolde(molde As String) As String
        Dim posto As String
        posto = consultaValor("SELECT LinhasInternos.Artigo FROM LinhasInternos INNER JOIN COP_Obras ON LinhasInternos.ObraID=COP_Obras.Id  WHERE COP_Obras.Codigo='" + molde + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        Return posto
    End Function

    Public Function daPostoSeleccionadoPeca(peca As String) As String
        Dim posto As String
        posto = consultaValor("SELECT Artigo FROM LinhasInternos WHERE CDu_ObraN1='" + peca + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        Return posto
    End Function

    Public Function daPostoSeleccionadoOperacao(operacao As String) As String
        Dim posto As String
        posto = consultaValor("SELECT Artigo FROM LinhasInternos WHERE CDU_ObraN2='" + operacao + "' and Quantidade=0 and CDU_DataObraFinal IS NULL")
        Return posto
    End Function

    Private Sub daListaPecasListaMateriais(restricao As String)
        Dim doc As String


    End Sub

End Class
