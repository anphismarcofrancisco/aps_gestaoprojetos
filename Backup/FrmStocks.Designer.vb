﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmStocks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmStocks))
        Me.gridArtigos = New AxXtremeReportControl.AxReportControl
        Me.gridArtigosStock = New AxXtremeReportControl.AxReportControl
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame
        Me.lblMoldes = New System.Windows.Forms.Label
        Me.txtPesqMolde = New AxXtremeSuiteControls.AxFlatEdit
        Me.lstMoldes = New System.Windows.Forms.ListBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblData = New System.Windows.Forms.Label
        Me.dtpData = New System.Windows.Forms.DateTimePicker
        Me.txtArtigo = New AxXtremeSuiteControls.AxFlatEdit
        Me.lblText = New System.Windows.Forms.Label
        Me.lblEmModoEdicao = New System.Windows.Forms.Label
        Me.btnGravar = New System.Windows.Forms.Button
        Me.btnEAT = New System.Windows.Forms.Button
        Me.btnDAT = New System.Windows.Forms.Button
        Me.btnEPA = New System.Windows.Forms.Button
        Me.btnDPA = New System.Windows.Forms.Button
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqMolde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridArtigos
        '
        Me.gridArtigos.Location = New System.Drawing.Point(12, 147)
        Me.gridArtigos.Name = "gridArtigos"
        Me.gridArtigos.OcxState = CType(resources.GetObject("gridArtigos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigos.Size = New System.Drawing.Size(451, 529)
        Me.gridArtigos.TabIndex = 33
        Me.gridArtigos.Tag = "0"
        '
        'gridArtigosStock
        '
        Me.gridArtigosStock.Location = New System.Drawing.Point(537, 109)
        Me.gridArtigosStock.Name = "gridArtigosStock"
        Me.gridArtigosStock.OcxState = CType(resources.GetObject("gridArtigosStock.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosStock.Size = New System.Drawing.Size(540, 567)
        Me.gridArtigosStock.TabIndex = 34
        Me.gridArtigosStock.Tag = "1"
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(1233, 80)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 36
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(12, 2)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(373, 55)
        Me.CommandBarsFrame1.TabIndex = 35
        '
        'lblMoldes
        '
        Me.lblMoldes.AutoSize = True
        Me.lblMoldes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoldes.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblMoldes.Location = New System.Drawing.Point(1078, 77)
        Me.lblMoldes.Name = "lblMoldes"
        Me.lblMoldes.Size = New System.Drawing.Size(66, 25)
        Me.lblMoldes.TabIndex = 44
        Me.lblMoldes.Text = "Molde"
        '
        'txtPesqMolde
        '
        Me.txtPesqMolde.Location = New System.Drawing.Point(1083, 109)
        Me.txtPesqMolde.Name = "txtPesqMolde"
        Me.txtPesqMolde.OcxState = CType(resources.GetObject("txtPesqMolde.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqMolde.Size = New System.Drawing.Size(180, 32)
        Me.txtPesqMolde.TabIndex = 43
        '
        'lstMoldes
        '
        Me.lstMoldes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMoldes.FormattingEnabled = True
        Me.lstMoldes.ItemHeight = 25
        Me.lstMoldes.Location = New System.Drawing.Point(1083, 147)
        Me.lstMoldes.Name = "lstMoldes"
        Me.lstMoldes.Size = New System.Drawing.Size(180, 529)
        Me.lstMoldes.TabIndex = 42
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(7, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 25)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Artigos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(532, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(129, 25)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Artigos Saida"
        '
        'lblData
        '
        Me.lblData.AutoSize = True
        Me.lblData.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblData.Location = New System.Drawing.Point(937, 16)
        Me.lblData.Name = "lblData"
        Me.lblData.Size = New System.Drawing.Size(44, 20)
        Me.lblData.TabIndex = 48
        Me.lblData.Text = "Data"
        '
        'dtpData
        '
        Me.dtpData.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpData.Location = New System.Drawing.Point(941, 39)
        Me.dtpData.Name = "dtpData"
        Me.dtpData.Size = New System.Drawing.Size(153, 35)
        Me.dtpData.TabIndex = 47
        '
        'txtArtigo
        '
        Me.txtArtigo.Location = New System.Drawing.Point(12, 109)
        Me.txtArtigo.Name = "txtArtigo"
        Me.txtArtigo.OcxState = CType(resources.GetObject("txtArtigo.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtArtigo.Size = New System.Drawing.Size(451, 32)
        Me.txtArtigo.TabIndex = 49
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.Location = New System.Drawing.Point(476, 9)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(215, 39)
        Me.lblText.TabIndex = 51
        Me.lblText.Text = "TJ Aços, Lda"
        '
        'lblEmModoEdicao
        '
        Me.lblEmModoEdicao.AutoSize = True
        Me.lblEmModoEdicao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmModoEdicao.ForeColor = System.Drawing.Color.LimeGreen
        Me.lblEmModoEdicao.Location = New System.Drawing.Point(262, 48)
        Me.lblEmModoEdicao.Name = "lblEmModoEdicao"
        Me.lblEmModoEdicao.Size = New System.Drawing.Size(288, 25)
        Me.lblEmModoEdicao.TabIndex = 52
        Me.lblEmModoEdicao.Text = "Documento em modo de edição"
        '
        'btnGravar
        '
        Me.btnGravar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGravar.Image = CType(resources.GetObject("btnGravar.Image"), System.Drawing.Image)
        Me.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGravar.Location = New System.Drawing.Point(1114, 12)
        Me.btnGravar.Name = "btnGravar"
        Me.btnGravar.Size = New System.Drawing.Size(140, 62)
        Me.btnGravar.TabIndex = 50
        Me.btnGravar.Text = " Gravar   "
        Me.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGravar.UseVisualStyleBackColor = True
        '
        'btnEAT
        '
        Me.btnEAT.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PDirT
        Me.btnEAT.Location = New System.Drawing.Point(469, 537)
        Me.btnEAT.Name = "btnEAT"
        Me.btnEAT.Size = New System.Drawing.Size(62, 62)
        Me.btnEAT.TabIndex = 40
        Me.btnEAT.UseVisualStyleBackColor = True
        Me.btnEAT.Visible = False
        '
        'btnDAT
        '
        Me.btnDAT.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PEsqT
        Me.btnDAT.Location = New System.Drawing.Point(469, 188)
        Me.btnDAT.Name = "btnDAT"
        Me.btnDAT.Size = New System.Drawing.Size(62, 62)
        Me.btnDAT.TabIndex = 39
        Me.btnDAT.UseVisualStyleBackColor = True
        Me.btnDAT.Visible = False
        '
        'btnEPA
        '
        Me.btnEPA.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PEsq
        Me.btnEPA.Location = New System.Drawing.Point(469, 411)
        Me.btnEPA.Name = "btnEPA"
        Me.btnEPA.Size = New System.Drawing.Size(62, 62)
        Me.btnEPA.TabIndex = 38
        Me.btnEPA.UseVisualStyleBackColor = True
        '
        'btnDPA
        '
        Me.btnDPA.Image = CType(resources.GetObject("btnDPA.Image"), System.Drawing.Image)
        Me.btnDPA.Location = New System.Drawing.Point(469, 323)
        Me.btnDPA.Name = "btnDPA"
        Me.btnDPA.Size = New System.Drawing.Size(62, 62)
        Me.btnDPA.TabIndex = 37
        Me.btnDPA.UseVisualStyleBackColor = True
        '
        'FrmStocks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1269, 688)
        Me.Controls.Add(Me.lblEmModoEdicao)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.btnGravar)
        Me.Controls.Add(Me.txtArtigo)
        Me.Controls.Add(Me.lblData)
        Me.Controls.Add(Me.dtpData)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblMoldes)
        Me.Controls.Add(Me.txtPesqMolde)
        Me.Controls.Add(Me.lstMoldes)
        Me.Controls.Add(Me.btnEAT)
        Me.Controls.Add(Me.btnDAT)
        Me.Controls.Add(Me.btnEPA)
        Me.Controls.Add(Me.btnDPA)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.gridArtigosStock)
        Me.Controls.Add(Me.gridArtigos)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(1308, 726)
        Me.MinimumSize = New System.Drawing.Size(1024, 726)
        Me.Name = "FrmStocks"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anphis - Stocks - "
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqMolde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridArtigos As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigosStock As AxXtremeReportControl.AxReportControl
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents btnDPA As System.Windows.Forms.Button
    Friend WithEvents btnEPA As System.Windows.Forms.Button
    Friend WithEvents btnEAT As System.Windows.Forms.Button
    Friend WithEvents btnDAT As System.Windows.Forms.Button
    Friend WithEvents lblMoldes As System.Windows.Forms.Label
    Friend WithEvents txtPesqMolde As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lstMoldes As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblData As System.Windows.Forms.Label
    Friend WithEvents dtpData As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtArtigo As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents btnGravar As System.Windows.Forms.Button
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents lblEmModoEdicao As System.Windows.Forms.Label
End Class
