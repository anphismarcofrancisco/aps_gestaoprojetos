﻿Imports PlatAPSNET
Public Class emailCDO
    Inherits email
    Dim ficheiroIni As IniFile
    Dim objemail As CDO.Message
    Dim caminhoHTML As String
    Public Sub New(ByVal entrada As String)
        MyBase.New(entrada)
        objemail = New CDO.Message
    End Sub

    Public Overrides Sub adicionarAnexo(ByVal ficheiro As String, ByVal primeiravez As Boolean)
        objemail.AddAttachment(ficheiro)
    End Sub

    Public Overrides Function enviar(ByVal enderecoDestino As String, ByVal assunto As String, ByVal corpo As String, ByVal corpoHTML As String) As String
        Try
            objemail.From = getEmailOrigem()
            objemail.BCC = getEmailBCC()
            objemail.CC = getEmailCC()
            objemail.To = enderecoDestino
            objemail.HTMLBody = corpo & corpoHTML
            objemail.HTMLBody = corpo
            objemail.Subject = assunto
            objemail.Send()
            Return ""
        Catch
            Return Err.Description
        End Try
    End Function
End Class
