USE [master]
GO

USE [PRIDEMO]
GO

PRINT 'Criar Tabela TDU_ARTWINREST...'
CREATE TABLE [TDU_ARTWINREST] (ID UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newsequentialid(),[CDU_ArtigoPR] [nvarchar](48) NOT NULL,[CDU_ArtigoWinR] [nvarchar](48) NULL,[CDU_SERIE] [nvarchar](10) NULL) ON [PRIMARY]
GO
PRINT 'Tabela criada com sucesso...'
GO

PRINT 'Criar Tabela TDU_ARMAZEM...'
CREATE TABLE [TDU_ARMAZEM] (ID UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newsequentialid(),[CDU_ARMAZEM] [nvarchar](20) NOT NULL,[CDU_SERIE] [nvarchar](10) NOT NULL) ON [PRIMARY]
GO
PRINT 'Tabela criada com sucesso...'
GO

PRINT 'Criar Tabela TDU_COMPOSICAO...'
CREATE TABLE [TDU_COMPOSICAO] (ID UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newsequentialid(),[CDU_DOCVENDA] [nvarchar](10) NOT NULL,[CDU_DOCCOMPOSICAO] [nvarchar](10) NULL,[CDU_SERIE] [nvarchar](10) NOT NULL) ON [PRIMARY]
GO
PRINT 'Tabela criada com sucesso...'
GO

PRINT 'Criar Tabela TDU_CAIXA...'
CREATE TABLE [TDU_CAIXA] (ID UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newsequentialid(),[CDU_CAIXA] [nvarchar](10) NOT NULL,[CDU_SERIE] [nvarchar](10) NULL,) ON [PRIMARY]
GO
PRINT 'Tabela criada com sucesso...'
GO

PRINT 'Criar Tabela TDU_IVA...'
CREATE TABLE [TDU_IVA] (ID UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newsequentialid(),[CDU_CODIVA] [nvarchar](10) NOT NULL,[CDU_SAFT] [nvarchar](10) NULL,) ON [PRIMARY]
GO
PRINT 'Tabela criada com sucesso...'
GO

PRINT 'Criar Tabela TDU_DOCUMENTO...'
CREATE TABLE [TDU_DOCUMENTO] (ID UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newsequentialid(),[CDU_DocumentoOrig] [nvarchar](48) NOT NULL,[CDU_DocumentoDest] [nvarchar](48) NULL,[CDU_Serie] [nvarchar](10) NULL) ON [PRIMARY]
GO
PRINT 'Tabela criada com sucesso...'
GO


PRINT 'Cria��o de Tabelas Conclu�da'

GO
PRINT 'Criar Strored Procedures'


GO
/****** Object:  StoredProcedure [dbo].[SPU_EQUIVALENCIA_CRUD]    Script Date: 12/29/2015 15:59:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPU_EQUIVALENCIA_CRUD]
      @Action VARCHAR(10)
      ,@Id uniqueidentifier=NULL
      ,@ArtigoPR VARCHAR(100) = NULL
      ,@ArtigoWinR VARCHAR(100) = NULL
      ,@Serie VARCHAR(100) = NULL
AS
BEGIN
      SET NOCOUNT ON;
 
      --SELECT
    IF @Action = 'SELECT'
      BEGIN
            SELECT ID,CDU_ArtigoPR, CDU_ArtigoWinR, CDU_Serie
            FROM TDU_ARTWINREST
      END
 
      --INSERT
    IF @Action = 'INSERT'
      BEGIN
            INSERT INTO TDU_ARTWINREST(CDU_ArtigoPR, CDU_ArtigoWinR, CDU_Serie)
            VALUES (@ArtigoPR, @ArtigoWinR, @Serie)
      END
 
      --UPDATE
    IF @Action = 'UPDATE'
      BEGIN
            UPDATE TDU_ARTWINREST
            SET CDU_ArtigoPR = @ArtigoPR, CDU_ArtigoWinR = @ArtigoWinR, CDU_Serie=@Serie
            WHERE ID = @Id
      END
 
      --DELETE
    IF @Action = 'DELETE'
      BEGIN
            DELETE FROM TDU_ARTWINREST
            WHERE ID = @Id
      END
END



GO
/****** Object:  StoredProcedure [dbo].[SPU_ARMAZEM_CRUD]    Script Date: 12/29/2015 15:59:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPU_ARMAZEM_CRUD]
      @Action VARCHAR(10)
      ,@Id uniqueidentifier=NULL
      ,@Armazem VARCHAR(100) = NULL
      ,@Serie VARCHAR(100) = NULL
      
AS
BEGIN
      SET NOCOUNT ON;
 
      --SELECT
    IF @Action = 'SELECT'
      BEGIN
            SELECT ID,CDU_ARMAZEM, CDU_SERIE
            FROM TDU_ARMAZEM
      END
 
      --INSERT
    IF @Action = 'INSERT'
      BEGIN
            INSERT INTO TDU_ARMAZEM(CDU_ARMAZEM, CDU_SERIE)
            VALUES (@ARMAZEM, @Serie)
      END
 
      --UPDATE
    IF @Action = 'UPDATE'
      BEGIN
            UPDATE TDU_ARMAZEM
            SET CDU_ARMAZEM = @Armazem, CDU_SERIE = @Serie
            WHERE ID = @Id
      END
 
      --DELETE
    IF @Action = 'DELETE'
      BEGIN
            DELETE FROM TDU_ARMAZEM
            WHERE ID = @Id
      END
END

GO
/****** Object:  StoredProcedure [dbo].[SPU_CAIXA_CRUD]    Script Date: 12/29/2015 15:59:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPU_CAIXA_CRUD]
      @Action VARCHAR(10)
      ,@Id uniqueidentifier=NULL
      ,@Caixa VARCHAR(100) = NULL
      ,@Serie VARCHAR(100) = NULL
      
AS
BEGIN
      SET NOCOUNT ON;
 
      --SELECT
    IF @Action = 'SELECT'
      BEGIN
            SELECT ID,CDU_CAIXA, CDU_SERIE
            FROM TDU_CAIXA
      END
 
      --INSERT
    IF @Action = 'INSERT'
      BEGIN
            INSERT INTO TDU_CAIXA(CDU_CAIXA, CDU_SERIE)
            VALUES (@Caixa, @Serie)
      END
 
      --UPDATE
    IF @Action = 'UPDATE'
      BEGIN
            UPDATE TDU_CAIXA
            SET CDU_CAIXA = @Caixa, CDU_SERIE = @Serie
            WHERE ID = @Id
      END
 
      --DELETE
    IF @Action = 'DELETE'
      BEGIN
            DELETE FROM TDU_CAIXA
            WHERE ID = @Id
      END
END


GO
/****** Object:  StoredProcedure [dbo].[SPU_COMPOSICAO_CRUD]    Script Date: 12/29/2015 15:59:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPU_COMPOSICAO_CRUD]
      @Action VARCHAR(10)
      ,@Id uniqueidentifier=NULL
      ,@DocVenda VARCHAR(10) = NULL
      ,@DocComposicao VARCHAR(10) = NULL
	  ,@Serie VARCHAR(10) = NULL
      
AS
BEGIN
      SET NOCOUNT ON;
 
      --SELECT
    IF @Action = 'SELECT'
      BEGIN
            SELECT ID,CDU_DOCVENDA, CDU_DOCCOMPOSICAO ,CDU_SERIE
            FROM TDU_COMPOSICAO
      END
 
      --INSERT
    IF @Action = 'INSERT'
      BEGIN
            INSERT INTO TDU_COMPOSICAO(CDU_DOCVENDA, CDU_DOCCOMPOSICAO, CDU_SERIE)
            VALUES (@DocVenda, @DocComposicao, @Serie)
      END
 
      --UPDATE
    IF @Action = 'UPDATE'
      BEGIN
            UPDATE TDU_COMPOSICAO
            SET CDU_DOCVENDA = @DocVenda, CDU_DOCCOMPOSICAO = @DocComposicao, CDU_SERIE = @Serie
            WHERE ID = @Id
      END
 
      --DELETE
    IF @Action = 'DELETE'
      BEGIN
            DELETE FROM TDU_COMPOSICAO
            WHERE ID = @Id
      END
END

GO
/****** Object:  StoredProcedure [dbo].[SPU_IVA_CRUD]    Script Date: 12/29/2015 15:59:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPU_IVA_CRUD]
      @Action VARCHAR(10)
      ,@Id uniqueidentifier=NULL
      ,@CodIVA VARCHAR(10) = NULL
      ,@SAFT VARCHAR(10) = NULL
	        
AS
BEGIN
      SET NOCOUNT ON;
 
      --SELECT
    IF @Action = 'SELECT'
      BEGIN
            SELECT ID,CDU_CODIVA, CDU_SAFT
            FROM TDU_IVA
      END
 
      --INSERT
    IF @Action = 'INSERT'
      BEGIN
            INSERT INTO TDU_IVA(CDU_CODIVA, CDU_SAFT)
            VALUES (@CodIVA, @SAFT)
      END
 
      --UPDATE
    IF @Action = 'UPDATE'
      BEGIN
            UPDATE TDU_IVA
            SET CDU_CODIVA = @CodIVA, CDU_SAFT = @SAFT
            WHERE ID = @Id
      END
 
      --DELETE
    IF @Action = 'DELETE'
      BEGIN
            DELETE FROM TDU_IVA
            WHERE ID = @Id
      END
END

GO
/****** Object:  StoredProcedure [dbo].[SPU_DOCUMENTO_CRUD]    Script Date: 01/25/2016 14:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPU_DOCUMENTO_CRUD]
      @Action VARCHAR(10)
      ,@Id uniqueidentifier=NULL
      ,@DocumentoOrig VARCHAR(100) = NULL
      ,@DocumentoDest VARCHAR(100) = NULL
      ,@Serie VARCHAR(100) = NULL
AS
BEGIN
      SET NOCOUNT ON;
 
      --SELECT
    IF @Action = 'SELECT'
      BEGIN
            SELECT ID,CDU_DocumentoOrig, CDU_DocumentoDest, CDU_Serie
            FROM TDU_Documento
      END
 
      --SELECT
    IF @Action = 'EQUIVALENCIA'
      BEGIN
            SELECT ID,CDU_DocumentoOrig, CDU_DocumentoDest, CDU_Serie
            FROM TDU_Documento
			WHERE CDU_DocumentoOrig=@DocumentoOrig
      END
  
 
      --INSERT
    IF @Action = 'INSERT'
      BEGIN
            INSERT INTO TDU_Documento(CDU_DocumentoOrig, CDU_DocumentoDest, CDU_Serie)
            VALUES (@DocumentoOrig, @DocumentoDest, @Serie)
      END
 
      --UPDATE
    IF @Action = 'UPDATE'
      BEGIN
            UPDATE TDU_Documento
            SET CDU_DocumentoOrig = @DocumentoOrig, CDU_DocumentoDest = @DocumentoDest, CDU_Serie=@Serie
            WHERE ID = @Id
      END
 
      --DELETE
    IF @Action = 'DELETE'
      BEGIN
            DELETE FROM TDU_Documento
            WHERE ID = @Id
      END
END
