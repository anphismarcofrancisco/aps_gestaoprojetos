﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports Interop.StdBE900

Public Class FrmQtDescr

    Const MSGERRO1 As String = "Valor de stock Inválido"
    Const MSGPERGUNTA1 As String = "Deseja realmente remover o registo?"
    Dim limpar As Boolean


    Dim valor_ As Double = 0
    Property Valor() As Double
        Get
            If IsNumeric(txtQuant.Text) Then
                Valor = CDbl(txtQuant.Text)
            Else
                Valor = 1
            End If
        End Get
        Set(ByVal value As Double)
            valor_ = value
        End Set
    End Property

    Dim valorNome_ As String = ""
    Property ValorNome() As String
        Get
            ValorNome = txtDescricao.Text
        End Get
        Set(ByVal value As String)
            valorNome_ = value
        End Set
    End Property

    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Private Sub FrmTerminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        botaoSeleccionado_ = 3
        limpar = True
        iniciarForm()
        txtQuant.Text = valor_
        txtDescricao.Text = valorNome_
        '  txtDescricao.Focused = True
        txtDescricao.Focus()

    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Confirmar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Confirmar"

            Control = .Add(xtpControlButton, 2, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    Private Sub iniciarForm()
        iniciarToolBox()
        txtQuant.Text = 1
        txtQuant.Focus()
    End Sub


    Private Sub colocarQuantidadeStk(ByVal valor As String)
        If limpar Then
            txtQuant.Text = ""
            limpar = False
        End If
        If valor = "" Then
            txtQuant.Text = ""
        Else
            If IsNumeric(txtQuant.Text + valor) Then txtQuant.Text = txtQuant.Text + valor
        End If
    End Sub



    Private Sub habilitarBotoes(ByVal b As Boolean)
        btn1.Enabled = b
        btn2.Enabled = b
        btn3.Enabled = b
        btn4.Enabled = b
        If b = True Then btn5.Enabled = b
        btn6.Enabled = b
        btn7.Enabled = b
        btn8.Enabled = b
        btn9.Enabled = b
        btn0.Enabled = b
        btnP.Enabled = b
    End Sub

    Private Sub btnP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnP.Click
        '  habilitarBotoes(False)
        colocarQuantidadeStk(",")
    End Sub

    Private Sub btnC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnC.Click
        colocarQuantidadeStk("")
        ' habilitarBotoes(True)
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        colocarQuantidadeStk(btn1.Tag)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        colocarQuantidadeStk(btn2.Tag)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        colocarQuantidadeStk(btn3.Tag)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        colocarQuantidadeStk(btn4.Tag)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        colocarQuantidadeStk(btn5.Tag)
        'If btn0.Enabled = False Then
        '    btn5.Enabled = False
        'End If
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        colocarQuantidadeStk(btn6.Tag)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        colocarQuantidadeStk(btn7.Tag)
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        colocarQuantidadeStk(btn8.Tag)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        colocarQuantidadeStk(btn9.Tag)
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        colocarQuantidadeStk(btn0.Tag)
    End Sub


    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        botaoSeleccionado_ = e.control.Index

        ' terminar o registo
        If e.control.Index = 1 Then
            If IsNumeric(txtQuant.Text) Then
                Me.Hide()
            Else
                MsgBox(MSGERRO1, MsgBoxStyle.Critical)
            End If

        End If


        'fechar o serviço
        If e.control.Index = 2 Then
            Me.Close()
        End If
    End Sub




    Private Sub ImageManager_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageManager.Enter

    End Sub
End Class