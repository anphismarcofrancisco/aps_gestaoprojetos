﻿
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment


Public Class FrmGestEncomendasRegistos

    Dim clicouConfirmar_ As Boolean


    Const COLUMN_DOCUMENTO As Integer = 0
    Const COLUMN_ARTIGO As Integer = 1
    Const COLUMN_DESCRICAO As Integer = 2
    Const COLUMN_QUANTIDADE As Integer = 3
    Const COLUMN_STKACTUAL As Integer = 4
    Const COLUMN_ULTPRECO As Integer = 5
    Const COLUMN_ULTFORN As Integer = 6
    Const COLUMN_ESTADO As Integer = 7



    Const COR_BRANCO As UInteger = 4294967295
    Const COR_COT As UInteger = 12432256
    Const COR_ECF As String = "6495ed"
    Const COR_VERMELHO As String = "605DCF"
    Const COR_INTERNO As String = "4682b4"
    Const COR_VERIFICADO As String = "48d1cc"
    Const COR_CONFERIDO As String = "bebebe"
    Const COR_ARTIGODEFAULT As String = "87CEEB"

    ReadOnly Property clicouConfirmar() As Boolean
        Get
            clicouConfirmar = clicouConfirmar_
        End Get

    End Property

    Dim tipodoc_ As String
    Property TipoDocumento() As String
        Get
            TipoDocumento = tipodoc_
        End Get

        Set(value As String)
            tipodoc_ = value
        End Set
    End Property


    Private Sub FrmGestEncomendasRegistos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim m As Motor
        m = Motor.GetInstance
        iniciarToolBox()
        dtpDataI.Value = m.DayStart(Now) 'm.firstDayMonth(Now)
        dtpDataF.Value = m.DayEnd(Now) 'm.lastDayMonth(Now)

        gridRegistos.AutoColumnSizing = False
        gridRegistos.AutoColumnSizing = False
        gridRegistos.ShowGroupBox = True
        gridRegistos.ShowItemsInGroups = False

        '  buscarListaFuncionariosPermissoes()
        inserirColunasRegistos()
        gridRegistos.SortOrder.DeleteAll()

        gridRegistos.SortOrder.Add(gridRegistos.Columns(COLUMN_DOCUMENTO))

        buscarRegistos()
        clicouConfirmar_ = False

    End Sub





    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Actualizar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Actualizar"

            Control = .Add(xtpControlButton, 2, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Public Function getIdLinha() As String
        getIdLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    getIdLinha = gridRegistos.SelectedRows(0).Record.Tag.ToString
                End If
            End If
        End If
    End Function

    Public Function getValorLinha(ByVal coluna As Integer) As String
        getValorLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    getValorLinha = gridRegistos.SelectedRows(0).Record(coluna).Value
                End If
            End If
        End If
    End Function

    Private Sub gridRegistos_CellMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs)
        clicouConfirmar_ = True
        Me.Close()
    End Sub

    Private Sub inserirColunasRegistos()
        Dim Column As ReportColumn
        Column = inserirColuna(True, COLUMN_DOCUMENTO, "Registo", 180, False, True)
        Column = inserirColuna(True, COLUMN_ARTIGO, "Artigo", 100, False, True)
        Column = inserirColuna(True, COLUMN_DESCRICAO, "Descrição", 330, False, True)
        Column = inserirColuna(True, COLUMN_QUANTIDADE, "Quantidade", 100, False, True)
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(True, COLUMN_STKACTUAL, "Stk Actual", 100, False, True)
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(True, COLUMN_ULTPRECO, "PCU", 100, False, True)
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(True, COLUMN_ULTFORN, "Ult. Forn.", 100, False, True)
        Column.Alignment = xtpAlignmentRight
    End Sub

    Private Function inserirColuna(ByVal isGridSE As Boolean, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = gridRegistos.Columns.Add(index, texto, tamanho, Resizable)
        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function

    Private Sub buscarRegistos()
        Dim m As MotorGE
        Dim total As Double
        m = MotorGE.GetInstance

        gridRegistos.Records.DeleteAll()
        gridRegistos.HeaderRecords.DeleteAll()
        gridRegistos.FooterRecords.DeleteAll()
        Dim dtRegistos As DataTable
        dtRegistos = m.daListaRegistos(tipodoc_, dtpDataI.Value, dtpDataF.Value)

        inserirLinhasRegistos(dtRegistos)
        If gridRegistos.GroupsOrder.Count = 0 Then
            gridRegistos.GroupsOrder.DeleteAll()
            gridRegistos.GroupsOrder.Add(gridRegistos.Columns(buscarIndexColunaDocumento))
            gridRegistos.Columns(buscarIndexColunaDocumento).Visible = False
        End If

        '  total = m.daTotalRegistos(tipodoc_, dtpDataI.Value, dtpDataF.Value, "P")

        inserirLinhaTotal(total)
        gridRegistos.Populate()
        '  inserirSubtotais()
    End Sub

    Private Function buscarIndexColunaDocumento() As Integer

        Dim col As ReportColumn
        For Each col In gridRegistos.Columns
            If col.Caption = "Documento" Then
                Return col.Index
            End If
        Next
        Return COLUMN_DOCUMENTO
    End Function
    Private Sub inserirLinhaTotal(ByVal total As Double)
        Dim frr As ReportRecord

        gridRegistos.ShowFooterRows = True

        frr = gridRegistos.FooterRecords.Add()

        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("Total")
        frr.AddItem(total)
        frr.AddItem("")



    End Sub
    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow)
        Next
    End Sub



    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim documento As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim estado As String
        Dim cor As UInteger
        Dim quantidade As Double

        estado = m.NuloToString(dtRow("Estado"))
        cor = buscarCor(estado)
        record = gridRegistos.Records.Add()
        record.Tag = dtRow("ID")
        documento = ""
        documento = m.NuloToString(dtRow("TipoDoc")) + " | " + m.NuloToString(dtRow("Serie")) + " | " + m.NuloToString(dtRow("NumDoc")) + " de " + FormatDateTime(dtRow("DataDoc"), DateFormat.ShortDate)
        Item = record.AddItem(documento)
        Item.Caption = documento
        Item.Tag = dtRow("ID")
        Item.BackColor = cor
        Item = record.AddItem(dtRow("Artigo"))
        Item.Tag = "Artigo"
        Item.BackColor = cor
        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = "Descricao"
        Item.BackColor = cor
        quantidade = m.NuloToDouble(dtRow("Quantidade"))
        If quantidade < 0 Then quantidade = quantidade * -1
        Item = record.AddItem(quantidade)
        Item.Caption = Formatar(Item.Value)
        Item.Tag = "Quantidade"
        Item.BackColor = cor
        Item = record.AddItem(dtRow("StkActual"))
        Item.Caption = Formatar(Item.Value)
        Item.Tag = "StkActual"
        Item.BackColor = cor
        Item = record.AddItem(dtRow("PCUltimo"))
        Item.Caption = Formatar(Item.Value)
        Item.BackColor = cor
        Item.Tag = "PCUltimo"
        Item.BackColor = cor
        Item = record.AddItem(dtRow("UltimoFornecedor"))
        Item.Tag = "UltimoFornecedor"
        Item.BackColor = cor
        ' aplicarFormatoLinha(estado, Item)
    End Sub

    Private Sub aplicarFormatoLinha(ByVal estado As String, ByVal item As ReportRecordItem)

        Select Case estado
            Case "N" : item.BackColor = 12632256
            Case "F" : item.BackColor = 14671839
            Case "T" : item.BackColor = 10000000
            Case "A" : item.BackColor = 12632900
            Case "P" : item.BackColor = 12632900
            Case "B" : item.BackColor = RGB(255, 255, 255)
            Case "I" : item.BackColor = RGB(255, 255, 0)
            Case "C" : item.BackColor = RGB(255, 255, 255)
            Case "G" : item.BackColor = RGB(255, 255, 255)
        End Select


    End Sub

    Private Function buscarCor(estado As String) As UInteger
        Dim cor As UInteger
        cor = COR_BRANCO

        Select Case estado
            Case "" : cor = COR_BRANCO
            Case "I" : cor = System.Convert.ToUInt32(HexToDecimal(COR_INTERNO))
            Case "P" : cor = System.Convert.ToUInt32(HexToDecimal(COR_VERMELHO))
            Case "C" : cor = COR_COT
            Case "E" : cor = System.Convert.ToUInt32(HexToDecimal(COR_ECF)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "V" : cor = System.Convert.ToUInt32(HexToDecimal(COR_VERIFICADO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "F" : cor = System.Convert.ToUInt32(HexToDecimal(COR_CONFERIDO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))

        End Select
        Return cor
    End Function

    Public Function Formatar(item As String) As String
        Return FormatNumber(item, 2)
    End Function


    Public Function HexToDecimal(ByVal HexString As String) As Integer
        Dim HexColor As Char() = HexString.ToCharArray()
        Dim DecimalColor As Integer = 0
        Dim iLength As Integer = HexColor.Length - 1
        Dim iDecimalNumber As Integer

        Dim cHexValue As Char
        For Each cHexValue In HexColor
            If Char.IsNumber(cHexValue) Then
                iDecimalNumber = Integer.Parse(cHexValue.ToString())
            Else
                iDecimalNumber = Convert.ToInt32(cHexValue) - 55
            End If

            DecimalColor += iDecimalNumber * (Convert.ToInt32(Math.Pow(16, iLength)))
            iLength -= 1
        Next cHexValue
        Return DecimalColor
    End Function


    Private Function verificarDocumentoJaTransformado(ByVal idCabecDoc As String, ByVal estado As String) As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim count As Integer
        count = m.consultaValor("Select count(*) from linhasdoc where IdLinhaOrigemCopia in (select id from LinhasInternos where IdCabecInternos='" + idCabecDoc + "')")
        If count > 0 Then
            estado = "T"
        End If
        Return estado
    End Function

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute

        ' Dim rowCT As DataRowView
        ' rowCT = cmbCentroTrabalho.SelectedItem
        Dim m As Motor
        m = Motor.GetInstance
        Select Case e.control.Id
            Case 1 : buscarRegistos()
            Case 2 : imprimir()
            Case 3 : Me.Close()
                ' Case ID_SERVICO_CONFERIDO : actualizarServico(IIf(m.getMOLDULOP = "I", "A", "P"))
                '  Case ID_SERVICO_ABRIR : actualizarServico(IIf(m.getMOLDULOP = "I", "B", "G"))
        End Select
    End Sub
    Private Sub imprimir()
        'gridRegistos.PrintPreviewOptions.Title = ""
        gridRegistos.PrintOptions.Header.FormatString = "Registos de Folhas Ponto" + vbCrLf + vbCrLf + "De " + CStr(dtpDataI.Value) + " a " + CStr(dtpDataF.Value)
        gridRegistos.PrintOptions.Header.Font.Size = 12
        gridRegistos.PrintOptions.BlackWhitePrinting = True
        gridRegistos.PrintOptions.Landscape = False
        gridRegistos.PrintPreview(True)
    End Sub

    Private Sub FrmEditar_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        gridRegistos.Width = Me.Width - (45)
        gridRegistos.Height = Me.Height - (gridRegistos.Top + 45)
    End Sub

    Private Sub gridRegistos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridRegistos.RowDblClick


        clicouConfirmar_ = True
        Me.Close()

        'If Not e.item Is Nothing Then
        '    Dim estado As String
        '    estado = e.item.Record.Item(COLUMN_ESTADO).Value
        '    If estado = "B" Or estado = "I" Or estado = "C" Or estado = "G" Then
        '        Dim m As Motor
        '        m = Motor.GetInstance
        '        If e.item.Record.Item(COLUMN_CODIGOFUNC).Value = m.Funcionario Or m.FuncionarioAdm Then
        '            clicouConfirmar_ = True
        '            Me.Close()
        '        Else
        '            mostrarMensagem("O")
        '        End If
        '    Else
        '        mostrarMensagem(estado)
        '    End If
        'End If
    End Sub

    Private Sub mostrarMensagem(ByVal estado As String)
        Dim m As Motor
        m = Motor.GetInstance
        Select Case estado
            Case "N" : MsgBox("O registo já se encontra Anulado.", MsgBoxStyle.Exclamation)
            Case "F" : MsgBox("O registo já se encontra Fechado.", MsgBoxStyle.Exclamation)
            Case "T" : MsgBox("O registo já se encontra Transformado em Factura.", MsgBoxStyle.Exclamation)
            Case "O" : MsgBox("O registo seleccionado é de outro funcionário.", MsgBoxStyle.Exclamation)
            Case "A" : MsgBox("O registo já se encontra conferido. " + IIf(m.FuncionarioAdm, vbCrLf + vbCrLf + "Tem de Abrir o Serviço para o poder editar.", ""), MsgBoxStyle.Exclamation)
            Case "P" : MsgBox("O registo já se encontra conferido. " + IIf(m.FuncionarioAdm, vbCrLf + vbCrLf + "Tem de Abrir o Serviço para o poder editar.", ""), MsgBoxStyle.Exclamation)
        End Select
    End Sub

    Private Sub inserirSubtotais()
        '   gridRegistos.ReCalc(True)
        Dim m As Motor
        Dim row As ReportRow
        Dim groupRow As ReportGroupRow
        m = Motor.GetInstance
        For i = 0 To gridRegistos.Rows.Count - 1
            row = gridRegistos.Rows(i)
            If row.GroupRow Then
                groupRow = row

                'groupRow.GroupFormat = " [SubTotal $=%.02f]"
                groupRow.GroupCaption = groupRow.GroupCaption + " - " + "Qtd: " + CStr(buscarTotaisGroup(groupRow))  ' CStr(m.daTotalRegistosCT(row.Childs(0).Record.Item(COLUMN_POSTO).Value, dtpDataI.Value, dtpDataF.Value)) + " H"
                'gridRegistos.GroupsOrder.
                'groupRow.GroupFormula = "SUMSUB(R*C2:R*C4)" 'Old notation
                '  groupRow.GroupFormula = "SUMSUB(C2:C3) SUMSUB(C3:C4)" 'New (short) notation
                ' groupRow.GroupCaption = "x"
            End If
        Next

    End Sub

    Private Function buscarTotaisGroup(ByVal groupRow As ReportGroupRow) As Double
        Dim total As Double
        total = 0
        Dim linha As ReportRow
        For Each linha In groupRow.Childs
            If Not linha.GroupRow Then
                total = total + linha.Record(COLUMN_QUANTIDADE).Value
            Else
                total = total + buscarTotaisGroup(linha)
            End If
        Next
        Return total
    End Function



    'Private Sub gridRegistos_MouseUpEvent(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridRegistos.MouseUpEvent
    '    Dim popup As CommandBar
    '    Dim Control As CommandBarControl
    '    Dim estado As String


    '    If (e.button = 2 And Not gridRegistos.FocusedRow Is Nothing) Then
    '        Dim m As Motor
    '        m = Motor.GetInstance
    '        estado = gridRegistos.FocusedRow.Record(COLUMN_ESTADO).Value
    '        'caso o estado esteja no estado B - aberto ou  "A" aprovado
    '        If (estado = "A" Or estado = "B" Or estado = "C" Or estado = "P" Or estado = "G") And m.FuncionarioAdm Then
    '            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
    '            With popup.Controls
    '                Control = .Add(xtpControlButton, IIf((estado = "B" Or estado = "C" Or estado = "G"), ID_SERVICO_CONFERIDO, ID_SERVICO_ABRIR), IIf((estado = "B" Or estado = "C" Or estado = "G"), "Serviço Conferido", "Abrir Serviço"), -1, False)
    '                Control.BeginGroup = True
    '            End With
    '            popup.ShowPopup()
    '        End If
    '    End If
    'End Sub

    Private Sub actualizarServico(ByVal estado As String)
        If Not gridRegistos.FocusedRow Is Nothing Then
            Dim m As Motor
            Dim id As String
            Dim idRegisto As String
            m = Motor.GetInstance
            id = gridRegistos.FocusedRow.Record(COLUMN_ESTADO).Tag
            idRegisto = gridRegistos.FocusedRow.Record.Tag.ToString
            gridRegistos.FocusedRow.Record(COLUMN_ESTADO).Value = estado
            If m.getMOLDULOP = "I" Then
                m.executarComando("UPDATE CabecInternos set Estado='" + estado + "' WHERE ID='" + id + "'")
                If estado = "A" Then
                    If m.getGRAVAFINALIZAR = False Then
                        m.gravarDocumento(idRegisto)
                    End If
                End If
            Else
                If m.getGRAVAFINALIZAR = False Then
                    If estado = "P" Then
                        m.gravarDocumento(idRegisto)
                    Else
                        m.removerDocumentoVenda(id)
                        m.executarComando("UPDATE TDU_APSCabecRegistoFolhaPonto set CDU_IdCabec=NULL WHERE CDU_Id='" + idRegisto + "'")
                        m.executarComando("UPDATE TDU_APSLinhasRegistoFolhaPonto set CDU_IdCabecInternos=NULL WHERE CDU_IdTDUCabec='" + idRegisto + "'")
                    End If
                Else
                    m.executarComando("UPDATE CabecDocStatus set Estado='" + estado + "' WHERE IDCabecDOc='" + id + "'")
                End If
            End If
            actualizarCoresGrelha(idRegisto, estado)
            gridRegistos.Populate()
            MsgBox("Serviço actualizado para o estado " + IIf(estado = "A" Or estado = "P", "Aprovado", "Aberto") + " com sucesso", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub actualizarCoresGrelha(ByVal idCabecDoc As String, ByVal estado As String)
        Dim row As ReportRow
        Dim i As Integer
        For Each row In gridRegistos.Rows
            If Not row.GroupRow Then
                If row.Record.Tag.ToString = idCabecDoc Then
                    row.Record(COLUMN_ESTADO).Value = estado
                    For i = 0 To gridRegistos.Columns.Count - 1
                        aplicarFormatoLinha(estado, row.Record.Item(i))
                    Next
                End If
            End If
        Next
    End Sub





End Class
