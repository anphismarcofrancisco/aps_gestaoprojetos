﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment

Public Class FrmArtigo

    Const COLUMN_ARTIGO As Integer = 0
    Const COLUMN_ARMAZEM As Integer = 1
    Const COLUMN_DESCRICAO As Integer = 2
    Const COLUMN_QUANTSTOCK As Integer = 3
    Const COLUMN_QUANTIDADE As Integer = 4

    Dim dtArtigos_ As DataTable
    Property TabelaArtigos() As DataTable
        Get
            TabelaArtigos = dtArtigos_
        End Get
        Set(ByVal value As DataTable)
            dtArtigos_ = value
        End Set
    End Property

    Dim viewArtigo_ As String
    Property ViewArtigo() As String
        Get
            ViewArtigo = viewArtigo_
        End Get
        Set(ByVal value As String)
            viewArtigo_ = value
        End Set
    End Property

    Dim armazemCliente_ As String
    Property ArmazemCliente() As String
        Get
            ArmazemCliente = armazemCliente_
        End Get
        Set(ByVal value As String)
            armazemCliente_ = value
        End Set
    End Property
    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property
    Private Sub FrmArtigo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gridArtigos.AutoColumnSizing = False
        gridArtigos.AutoColumnSizing = False
        '  gridArtigos.ShowGroupBox = True
        ' gridArtigos.ShowItemsInGroups = False

        iniciarToolBox()

        gridArtigosStock.AutoColumnSizing = False
        gridArtigosStock.AutoColumnSizing = False
        ' gridArtigosStock.ShowGroupBox = True
        ' gridArtigosStock.ShowItemsInGroups = False

        inserirColunasRegistos(1, gridArtigos)
        inserirColunasRegistos(2, gridArtigosStock)

        Dim dtrow As DataRow
        gridArtigosStock.Records.DeleteAll()
        If Not dtArtigos_ Is Nothing Then
            For Each dtrow In dtArtigos_.Rows
                inserirLinhaRegisto(dtrow, gridArtigosStock, dtrow("Quantidade"))
            Next
        End If
        txtArtigo.Text = ""
        buscarArtigos(gridArtigos, txtArtigo.Text)
        gridArtigosStock.Populate()


    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Confirmar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Confirmar"

            Control = .Add(xtpControlButton, 2, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub inserirColunasRegistos(ByVal indexGrid As Integer, ByVal grid As AxXtremeReportControl.AxReportControl)

        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", 150, False, True)
        Column = inserirColuna(grid, COLUMN_ARMAZEM, "Armazém", 100, False, True)
        Column.Visible = False
        Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", 200, False, True)
        Column = inserirColuna(grid, COLUMN_QUANTSTOCK, "Quant Stk", 80, False, True)
        Column.Alignment = xtpAlignmentRight
        If indexGrid = 2 Then
            Column = inserirColuna(grid, COLUMN_QUANTIDADE, "Quant.", 80, False, True)
            Column.Alignment = xtpAlignmentRight
        End If


    End Sub

    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function
    Private Sub buscarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim listaExclusao As String
        Dim m As Motor

        'If documento <> "" Then
        '    lblEmModoEdicao.Text = "Documento " + documento + " em modo de edição"
        '    lblEmModoEdicao.Visible = True
        'Else
        '    lblEmModoEdicao.Visible = False
        'End If
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        grid.Records.DeleteAll()
        listaExclusao = daListaArtigosExcluidos()
        dtRegistos = m.daListaArtigos(viewArtigo_, pesquisa, armazemCliente_, daListaArtigosExcluidos)
        inserirLinhasRegistos(dtRegistos, grid)
        grid.Populate()
    End Sub

    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable, ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow, grid)
        Next
    End Sub

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional ByVal valorStk As Double = -1)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim m As Motor

        m = Motor.GetInstance

        record = grid.Records.Add()
        '  record.PreviewText = "asd"
        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo"))
        Item = record.AddItem(dtRow("Armazem"))
        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = 1
        ' Item = record.AddItem(m.daStockActual(viewArtigo_, dtRow("Artigo")))
        Item = record.AddItem(dtRow("StkActual"))
        If grid.Name = "gridArtigosStock" Then
            If valorStk < 0 Then valorStk = valorStk * -1
            Item = record.AddItem(valorStk)
        End If

    End Sub

    Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

        buscarArtigos(gridArtigos, txtArtigo.Text)

    End Sub

    Private Function daListaArtigosExcluidos() As String
        daListaArtigosExcluidos = ""
        Dim row As ReportRecord
        '  If gridArtigosStock.Records.Count > 0 Then
        For Each row In gridArtigosStock.Records
            If daListaArtigosExcluidos = "" Then
                daListaArtigosExcluidos = "'" + row.Item(COLUMN_ARTIGO).Value + "'"
            Else
                daListaArtigosExcluidos = daListaArtigosExcluidos + ",'" + row.Item(COLUMN_ARTIGO).Value + "'"
            End If
        Next

    End Function

    Private Sub btnDPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDPA.Click
        enviarArtigoParaSaida()
    End Sub

    Private Sub enviarArtigoParaSaida()
        Dim dtrow As DataRow
        Dim m As Motor
        m = Motor.GetInstance
        If gridArtigos.SelectedRows.Count > 0 Then
            Dim i As Integer
            Dim index As Integer
            i = 0
            If Not m.artigoValido(gridArtigos.SelectedRows(i).Record.Item(COLUMN_ARTIGO).Value, gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Value, False) Then
                MsgBox("O Artigo selecionado não é está configurado para movimentar Stock.", MsgBoxStyle.Information, "Anphis")
                Exit Sub
            End If
            '  For i = 0 To gridArtigos.SelectedRows.Count - 1
            index = gridArtigos.SelectedRows(i).Record.Index
            dtrow = gridArtigos.SelectedRows(i).Record.Tag
            Dim f As FrmQt
            f = New FrmQt
            f.Valor = 1
            f.ShowDialog()
            If f.BotaoSeleccionado = 1 Then
                inserirLinhaRegisto(dtrow, gridArtigosStock, f.Valor)

                gridArtigos.SelectedRows(i).Record.Visible = False
                gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Tag = 0
                ' Next i
                gridArtigosStock.Populate()
                gridArtigos.Populate()
            End If
            Me.Show()
            'Me.Activate()
        End If
    End Sub

    Private Sub btnEPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPA.Click
        retirarArtigoSaida()
    End Sub

    Private Sub retirarArtigoSaida()
        If gridArtigosStock.SelectedRows.Count > 0 Then
            Dim index As Integer
            '  Dim index1 As Integer
            Dim record As ReportRecord
            Dim i As Integer
            i = 0
            ' For i = 0 To gridArtigosStock.SelectedRows.Count - 1
            'ir buscar o index dos artigos a movimentar stock
            index = gridArtigosStock.SelectedRows(i).Record.Index
            'ir buscar o index da  grelha dos artigos 
            'buscar a linha
            record = gridArtigosStock.SelectedRows(i).Record
            gridArtigosStock.Records.RemoveAt(index)
            ' Next i
            gridArtigosStock.Populate()
            buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub

    Private Sub gridArtigosStock_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigosStock.RowDblClick
        ' retirarArtigoSaida()
        Dim f As FrmQt
        f = New FrmQt
        f.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
        f.ShowDialog()
        If f.BotaoSeleccionado = 1 Then
            e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Valor
        End If
        f = Nothing
        Me.Activate()
        gridArtigosStock.Populate()
    End Sub

   
    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        botaoSeleccionado_ = e.control.Index

        ' terminar o registo
        If e.control.Index = 1 Then
            actualizarDataTableArtigo()
            Me.Hide()

        End If


        'fechar o serviço
        If e.control.Index = 2 Then
            Me.Close()
        End If
    End Sub


    Private Sub actualizarDataTableArtigo()
        dtArtigos_.Rows.Clear()
        Dim dtRow As DataRow
        Dim row As ReportRow
        For Each row In gridArtigosStock.Rows
            dtRow = dtArtigos_.NewRow()
            dtRow(COLUMN_ARTIGO) = row.Record(COLUMN_ARTIGO).Value
            dtRow(COLUMN_ARMAZEM) = row.Record(COLUMN_ARMAZEM).Value
            dtRow(COLUMN_DESCRICAO) = row.Record(COLUMN_DESCRICAO).Value
            dtRow(COLUMN_QUANTIDADE) = row.Record(COLUMN_QUANTIDADE).Value
            dtRow(COLUMN_QUANTSTOCK) = row.Record(COLUMN_QUANTSTOCK).Value
            dtArtigos_.Rows.Add(dtRow)
        Next
    End Sub
End Class