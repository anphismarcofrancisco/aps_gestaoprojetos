﻿Public Class Registo
    Public Sub New()
        linhasRegistoMaterialRTJ_ = New List(Of LinhaRegisto)
        linhasRegistoMaterialCliente_ = New List(Of LinhaRegisto)
    End Sub
    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_MAOOBRA() As String
        Get
            MATERIAL_MAOOBRA = "MO"
        End Get
    End Property

    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_LIMPEZA() As String
        Get
            MATERIAL_LIMPEZA = "ML"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_RTJ() As String
        Get
            MATERIAL_RTJ = "MR"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_CLIENTE() As String
        Get
            MATERIAL_CLIENTE = "MC"
        End Get
    End Property

    ''' <summary>
    ''' ID do Registo
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_ID_ As String
    Property ID() As String
        Get
            ID = CDU_ID_
        End Get
        Set(ByVal value As String)
            CDU_ID_ = value
        End Set
    End Property


    ''' <summary>
    ''' Funcionario
    ''' </summary>
    ''' <remarks></remarks>
    Dim Funcionario_ As String
    Property Funcionario() As String
        Get
            Funcionario = Funcionario_
        End Get
        Set(ByVal value As String)
            Funcionario_ = value
        End Set
    End Property

    ''' <summary>
    ''' Tipo Entidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim TipoEntidade_ As String
    Property TipoEntidade() As String
        Get
            TipoEntidade = TipoEntidade_
        End Get
        Set(ByVal value As String)
            TipoEntidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' Entidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim Entidade_ As String
    Property Entidade() As String
        Get
            Entidade = Entidade_
        End Get
        Set(ByVal value As String)
            Entidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' Posto
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Posto_ As String
    Property Posto() As String
        Get
            Posto = CDU_Posto_
        End Get
        Set(ByVal value As String)
            CDU_Posto_ = value
        End Set
    End Property

    ''' <summary>
    ''' Operacao
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Operacao_ As String
    Property Operacao() As String
        Get
            Operacao = CDU_Operacao_
        End Get
        Set(ByVal value As String)
            CDU_Operacao_ = value
        End Set
    End Property


    ''' <summary>
    ''' Molde
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Molde_ As String
    Property Molde() As String
        Get
            Molde = CDU_Molde_
        End Get
        Set(ByVal value As String)
            CDU_Molde_ = value
        End Set
    End Property


    ''' <summary>
    ''' Requisicao
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Requisicao_ As String
    Property Requisicao() As String
        Get
            Requisicao = CDU_Requisicao_
        End Get
        Set(ByVal value As String)
            CDU_Requisicao_ = value
        End Set
    End Property


    ''' <summary>
    ''' Id Cabec Stock Material Cliente
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_IdCabecStkMC_ As String
    Property CDU_IdCabecStkMaterialCliente() As String
        Get
            CDU_IdCabecStkMaterialCliente = CDU_IdCabecStkMC_
        End Get
        Set(ByVal value As String)
            CDU_IdCabec_ = value
        End Set
    End Property

    ''' <summary>
    ''' Id Cabec Internos
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_IdCabec_ As String
    Property CDU_IdCabec() As String
        Get
            CDU_IdCabec = CDU_IdCabec_
        End Get
        Set(ByVal value As String)
            CDU_IdCabec_ = value
        End Set
    End Property

    ''' <summary>
    ''' DataInicial 
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_DataInicial_ As String
    Property DataInicial() As String
        Get
            DataInicial = CDU_DataInicial_
        End Get
        Set(ByVal value As String)
            CDU_DataInicial_ = value
        End Set
    End Property

    ''' <summary>
    ''' Final 
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_DataFinal_ As String
    Property DataFinal() As String
        Get
            DataFinal = CDU_DataFinal_
        End Get
        Set(ByVal value As String)
            CDU_DataFinal_ = value
        End Set
    End Property


    ''' <summary>
    ''' Tempo 
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Tempo_ As String
    Property Tempo() As String
        Get
            Tempo = CDU_Tempo_
        End Get
        Set(ByVal value As String)
            CDU_Tempo_ = value
        End Set
    End Property


    ''' <summary>
    ''' TempoDescontado 
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_TempoDescontado_ As String
    Property TempoDescontado() As String
        Get
            TempoDescontado = CDU_TempoDescontado_
        End Get
        Set(ByVal value As String)
            CDU_TempoDescontado_ = value
        End Set
    End Property

    ''' <summary>
    ''' Observacoes 
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Observacoes_ As String
    Property Observacoes() As String
        Get
            Observacoes = CDU_Observacoes_
        End Get
        Set(ByVal value As String)
            CDU_Observacoes_ = value
        End Set
    End Property


    ''' <summary>
    ''' Observacoes 
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_Problema_ As String
    Property Problema() As String
        Get
            Problema = CDU_Problema_
        End Get
        Set(ByVal value As String)
            CDU_Problema_ = value
        End Set
    End Property
    ''' <summary>
    ''' Armazem Default 
    ''' </summary>
    ''' <remarks></remarks>
    Dim ArmazemDefault_ As String
    Property ArmazemDefault() As String
        Get
            ArmazemDefault = ArmazemDefault_
        End Get
        Set(ByVal value As String)
            ArmazemDefault_ = value
        End Set
    End Property




    ''' <summary>
    ''' LinhaRegistoMaoObra
    ''' </summary>
    ''' <remarks></remarks>
    Dim linhaRegistoMaoObra_ As LinhaRegisto
    Property LinhaRegistoMaoObra() As LinhaRegisto
        Get
            LinhaRegistoMaoObra = linhaRegistoMaoObra_
        End Get
        Set(ByVal value As LinhaRegisto)
            linhaRegistoMaoObra_ = value
        End Set
    End Property

    ''' <summary>
    ''' LinhaRegistoMaoObra
    ''' </summary>
    ''' <remarks></remarks>
    Dim linhaRegistoMaterialLimpeza_ As LinhaRegisto
    Property LinhaRegistoMaterialLimpeza() As LinhaRegisto
        Get
            LinhaRegistoMaterialLimpeza = linhaRegistoMaterialLimpeza_
        End Get
        Set(ByVal value As LinhaRegisto)
            linhaRegistoMaterialLimpeza_ = value
        End Set
    End Property

    Dim linhasRegistoMaterialRTJ_ As List(Of LinhaRegisto)

    Public Sub adicionaLinhasMaterialRTJ(ByVal dtMRTJ As DataTable, Optional ByVal id As String = "")
        Dim row As DataRow
        Dim linha As LinhaRegisto
        For Each row In dtMRTJ.Rows
            linha = New LinhaRegisto
            linha.Artigo = row("Artigo")
            linha.Armazem = row("Armazem")
            If linha.Armazem = "" Then linha.Armazem = ArmazemDefault_
            linha.Quantidade = row("Quantidade")
            linha.TipoArtigo = MATERIAL_RTJ
            If dtMRTJ.Columns.Contains("CDU_ID") Then
                linha.ID = row("CDU_ID").ToString
            End If
            linhasRegistoMaterialRTJ_.Add(linha)
        Next
    End Sub

    ReadOnly Property LinhasRegistosUMR() As List(Of LinhaRegisto)
        Get
            LinhasRegistosUMR = linhasRegistoMaterialRTJ_
        End Get
    End Property

    Dim linhasRegistoMaterialCliente_ As List(Of LinhaRegisto)
    Public Sub adicionaLinhasMaterialCliente(ByVal dtMC As DataTable, Optional ByVal id As String = "")
        Dim row As DataRow
        Dim linha As LinhaRegisto
        For Each row In dtMC.Rows
            linha = New LinhaRegisto
            linha.Artigo = row("Artigo")
            linha.Armazem = row("Armazem")
            If linha.Armazem = "" Then linha.Armazem = ArmazemDefault_
            linha.Quantidade = row("Quantidade")
            linha.TipoArtigo = MATERIAL_CLIENTE
            If dtMC.Columns.Contains("CDU_ID") Then
                linha.ID = row("CDU_ID").ToString
            End If
            linhasRegistoMaterialCliente_.Add(linha)
        Next
    End Sub

    ReadOnly Property LinhasRegistosUMC() As List(Of LinhaRegisto)
        Get
            LinhasRegistosUMC = linhasRegistoMaterialCliente_
        End Get
    End Property

    Public Sub adicionaRegistoMaterialMaoObra(ByVal artigo As String, ByVal quantidade As Double, Optional ByVal id As String = "")
        Dim linha As LinhaRegisto
        linha = New LinhaRegisto
        linha.Artigo = artigo
        linha.Armazem = ArmazemDefault_
        linha.Quantidade = quantidade
        linha.TipoArtigo = MATERIAL_MAOOBRA
        linhaRegistoMaoObra_ = linha
        If id <> "" Then
            linha.ID = id
        End If
    End Sub

    Public Sub adicionaRegistoMaterialLimpeza(ByVal artigo As String, ByVal quantidade As Double, Optional ByVal id As String = "")

        If quantidade <> 0 Then
            Dim linha As LinhaRegisto
            linha = New LinhaRegisto
            linha.Artigo = artigo
            linha.Armazem = ArmazemDefault_
            linha.Quantidade = quantidade
            linha.TipoArtigo = MATERIAL_LIMPEZA
            linhaRegistoMaterialLimpeza_ = linha
            If id <> "" Then
                linha.ID = id
            End If
        End If
    End Sub


    ReadOnly Property LinhasRegistos() As List(Of LinhaRegisto)
        Get
            Dim lista As List(Of LinhaRegisto)
            lista = New List(Of LinhaRegisto)
            lista.Add(linhaRegistoMaoObra_)
            lista.Add(linhaRegistoMaterialLimpeza_)
            Dim linha As LinhaRegisto
            Dim i As Integer

            For i = 0 To linhasRegistoMaterialRTJ_.Count - 1
                linha = linhasRegistoMaterialRTJ_(i)
                lista.Add(linha)
            Next

            For i = 0 To linhasRegistoMaterialCliente_.Count - 1
                linha = linhasRegistoMaterialCliente_(i)
                lista.Add(linha)
            Next

            LinhasRegistos = lista
        End Get

    End Property


End Class
