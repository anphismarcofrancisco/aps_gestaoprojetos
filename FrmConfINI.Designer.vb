﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfINI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ID As System.Windows.Forms.ColumnHeader
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfINI))
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabLicencimento = New System.Windows.Forms.TabPage()
        Me.LicLstView = New System.Windows.Forms.ListView()
        Me.Empresa = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Módulo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Limite = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.NumPostos = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LicBtnLicenca = New System.Windows.Forms.Button()
        Me.LictxtLicenciamento = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabERP = New System.Windows.Forms.TabPage()
        Me.lblModulos = New System.Windows.Forms.Label()
        Me.ERPLstBoxModulos = New System.Windows.Forms.CheckedListBox()
        Me.ERPbtnArt = New System.Windows.Forms.Button()
        Me.lblArmazem = New System.Windows.Forms.Label()
        Me.lblARTMATLimpeza = New System.Windows.Forms.Label()
        Me.ERPbtnArm = New System.Windows.Forms.Button()
        Me.ERPtxtArtMaterialDescricao = New System.Windows.Forms.TextBox()
        Me.ERPtxtArmazemDescricao = New System.Windows.Forms.TextBox()
        Me.ERPtxtArtMaterial = New System.Windows.Forms.TextBox()
        Me.ERPtxtArmazem = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ERPLstBoxEmpresas = New System.Windows.Forms.CheckedListBox()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.ERPcmbEmpresaDefault = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ERPcmbModulo = New System.Windows.Forms.ComboBox()
        Me.ERPtxtPasswordERP = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ERPtxtUtilizadorERP = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ERPtxtAplicacao = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ERPtxtInstancia = New System.Windows.Forms.TextBox()
        Me.lblInstancia = New System.Windows.Forms.Label()
        Me.lblTipoPlataforma = New System.Windows.Forms.Label()
        Me.ERPcmbTipoPlataforma = New System.Windows.Forms.ComboBox()
        Me.TabRegistoPontos = New System.Windows.Forms.TabPage()
        Me.TbRP = New System.Windows.Forms.TabControl()
        Me.TabG = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RPlblTamanhoPostos = New System.Windows.Forms.Label()
        Me.RPlblTamanhoOperacoes = New System.Windows.Forms.Label()
        Me.RPTxtTamanhoPostos = New System.Windows.Forms.TextBox()
        Me.RPTxtTamanhoOperaçõ = New System.Windows.Forms.TextBox()
        Me.RPTxtTamanhoProjectos = New System.Windows.Forms.TextBox()
        Me.RPlblTamanhoPeca = New System.Windows.Forms.Label()
        Me.RPLblTamanhoProjectos = New System.Windows.Forms.Label()
        Me.RPTxtTamanhoPeça = New System.Windows.Forms.TextBox()
        Me.RPckbApresentaBInicio = New System.Windows.Forms.CheckBox()
        Me.RPcmbDocumento = New System.Windows.Forms.Button()
        Me.lblRPSerie = New System.Windows.Forms.Label()
        Me.RPcmbModoRegisto = New System.Windows.Forms.ComboBox()
        Me.lblRPTipoDocumento = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RPcmbSerie = New System.Windows.Forms.Button()
        Me.RPckbMultiplaSelecção = New System.Windows.Forms.CheckBox()
        Me.lblTipoEntidade = New System.Windows.Forms.Label()
        Me.RPcmbTipoEntidade = New System.Windows.Forms.ComboBox()
        Me.RPtxtRPDocumentoDescricao = New System.Windows.Forms.TextBox()
        Me.RPtxtRPSerieDescricao = New System.Windows.Forms.TextBox()
        Me.RPtxtRPDocumento = New System.Windows.Forms.TextBox()
        Me.RPtxtRPSerie = New System.Windows.Forms.TextBox()
        Me.TabOP = New System.Windows.Forms.TabPage()
        Me.RPbtnRemovePostos = New System.Windows.Forms.Button()
        Me.RPbtnAddPostos = New System.Windows.Forms.Button()
        Me.RPbtnRemoveOP = New System.Windows.Forms.Button()
        Me.RPbtnAddOP = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.RPlstOperacao1 = New System.Windows.Forms.ListBox()
        Me.RPlstPostos1 = New System.Windows.Forms.ListBox()
        Me.TabP = New System.Windows.Forms.TabPage()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblOperacao = New System.Windows.Forms.Label()
        Me.RPlstFuncionariosPerm = New System.Windows.Forms.ListBox()
        Me.RPlstFuncionarios = New System.Windows.Forms.ListBox()
        Me.RPlstOperacao = New System.Windows.Forms.ListBox()
        Me.RPlstPostos = New System.Windows.Forms.ListBox()
        Me.TabPontos = New System.Windows.Forms.TabPage()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.PCBtnArmazem = New System.Windows.Forms.Button()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PCtxtArtigoLimpezaDescricao = New System.Windows.Forms.TextBox()
        Me.PCBtnArtigo = New System.Windows.Forms.Button()
        Me.PCtxtArtigoLimpeza = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabVendasEncomendas = New System.Windows.Forms.TabPage()
        Me.txtMoeda = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCampoArtPreco = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabListaMateriais = New System.Windows.Forms.TabPage()
        Me.TabStocks = New System.Windows.Forms.TabPage()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.SLstBoxDocumentos = New System.Windows.Forms.CheckedListBox()
        Me.SckbApresentaEncomenda = New System.Windows.Forms.CheckBox()
        Me.StxtSerieDescricao = New System.Windows.Forms.TextBox()
        Me.ScmbSerie = New System.Windows.Forms.Button()
        Me.StxtSerie = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.StxtDocumentoDescricao = New System.Windows.Forms.TextBox()
        Me.ScmbDocumento = New System.Windows.Forms.Button()
        Me.StxtDocumento = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SEmpresa = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ScmbTipoEntidade = New System.Windows.Forms.ComboBox()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        ID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TabControl.SuspendLayout()
        Me.TabLicencimento.SuspendLayout()
        Me.TabERP.SuspendLayout()
        Me.TabRegistoPontos.SuspendLayout()
        Me.TbRP.SuspendLayout()
        Me.TabG.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabOP.SuspendLayout()
        Me.TabP.SuspendLayout()
        Me.TabPontos.SuspendLayout()
        Me.TabVendasEncomendas.SuspendLayout()
        Me.TabStocks.SuspendLayout()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ID
        '
        ID.Text = "ID"
        ID.Width = 0
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabLicencimento)
        Me.TabControl.Controls.Add(Me.TabERP)
        Me.TabControl.Controls.Add(Me.TabRegistoPontos)
        Me.TabControl.Controls.Add(Me.TabPontos)
        Me.TabControl.Controls.Add(Me.TabVendasEncomendas)
        Me.TabControl.Controls.Add(Me.TabListaMateriais)
        Me.TabControl.Controls.Add(Me.TabStocks)
        Me.TabControl.Location = New System.Drawing.Point(6, 60)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(985, 536)
        Me.TabControl.TabIndex = 0
        '
        'TabLicencimento
        '
        Me.TabLicencimento.Controls.Add(Me.LicLstView)
        Me.TabLicencimento.Controls.Add(Me.Label2)
        Me.TabLicencimento.Controls.Add(Me.LicBtnLicenca)
        Me.TabLicencimento.Controls.Add(Me.LictxtLicenciamento)
        Me.TabLicencimento.Controls.Add(Me.Label11)
        Me.TabLicencimento.Location = New System.Drawing.Point(4, 22)
        Me.TabLicencimento.Name = "TabLicencimento"
        Me.TabLicencimento.Padding = New System.Windows.Forms.Padding(3)
        Me.TabLicencimento.Size = New System.Drawing.Size(977, 510)
        Me.TabLicencimento.TabIndex = 5
        Me.TabLicencimento.Text = "Licenciamento"
        Me.TabLicencimento.UseVisualStyleBackColor = True
        '
        'LicLstView
        '
        Me.LicLstView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {ID, Me.Empresa, Me.Módulo, Me.Limite, Me.NumPostos})
        Me.LicLstView.Location = New System.Drawing.Point(7, 70)
        Me.LicLstView.Name = "LicLstView"
        Me.LicLstView.Size = New System.Drawing.Size(568, 185)
        Me.LicLstView.TabIndex = 20
        Me.LicLstView.UseCompatibleStateImageBehavior = False
        Me.LicLstView.View = System.Windows.Forms.View.Details
        '
        'Empresa
        '
        Me.Empresa.Text = "Empresa"
        Me.Empresa.Width = 150
        '
        'Módulo
        '
        Me.Módulo.Text = "Módulo"
        Me.Módulo.Width = 200
        '
        'Limite
        '
        Me.Limite.Text = "Limite"
        Me.Limite.Width = 100
        '
        'NumPostos
        '
        Me.NumPostos.Text = "Num Postos"
        Me.NumPostos.Width = 100
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Empresas / Módulos"
        '
        'LicBtnLicenca
        '
        Me.LicBtnLicenca.Location = New System.Drawing.Point(406, 25)
        Me.LicBtnLicenca.Name = "LicBtnLicenca"
        Me.LicBtnLicenca.Size = New System.Drawing.Size(121, 23)
        Me.LicBtnLicenca.TabIndex = 13
        Me.LicBtnLicenca.Text = "Carregar Licença"
        Me.LicBtnLicenca.UseVisualStyleBackColor = True
        '
        'LictxtLicenciamento
        '
        Me.LictxtLicenciamento.Location = New System.Drawing.Point(8, 28)
        Me.LictxtLicenciamento.Name = "LictxtLicenciamento"
        Me.LictxtLicenciamento.Size = New System.Drawing.Size(378, 20)
        Me.LictxtLicenciamento.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 12)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Licenciamento"
        '
        'TabERP
        '
        Me.TabERP.Controls.Add(Me.lblModulos)
        Me.TabERP.Controls.Add(Me.ERPLstBoxModulos)
        Me.TabERP.Controls.Add(Me.ERPbtnArt)
        Me.TabERP.Controls.Add(Me.lblArmazem)
        Me.TabERP.Controls.Add(Me.lblARTMATLimpeza)
        Me.TabERP.Controls.Add(Me.ERPbtnArm)
        Me.TabERP.Controls.Add(Me.ERPtxtArtMaterialDescricao)
        Me.TabERP.Controls.Add(Me.ERPtxtArmazemDescricao)
        Me.TabERP.Controls.Add(Me.ERPtxtArtMaterial)
        Me.TabERP.Controls.Add(Me.ERPtxtArmazem)
        Me.TabERP.Controls.Add(Me.Label12)
        Me.TabERP.Controls.Add(Me.ERPLstBoxEmpresas)
        Me.TabERP.Controls.Add(Me.lblEmpresa)
        Me.TabERP.Controls.Add(Me.ERPcmbEmpresaDefault)
        Me.TabERP.Controls.Add(Me.Label10)
        Me.TabERP.Controls.Add(Me.ERPcmbModulo)
        Me.TabERP.Controls.Add(Me.ERPtxtPasswordERP)
        Me.TabERP.Controls.Add(Me.Label4)
        Me.TabERP.Controls.Add(Me.ERPtxtUtilizadorERP)
        Me.TabERP.Controls.Add(Me.Label3)
        Me.TabERP.Controls.Add(Me.ERPtxtAplicacao)
        Me.TabERP.Controls.Add(Me.Label1)
        Me.TabERP.Controls.Add(Me.ERPtxtInstancia)
        Me.TabERP.Controls.Add(Me.lblInstancia)
        Me.TabERP.Controls.Add(Me.lblTipoPlataforma)
        Me.TabERP.Controls.Add(Me.ERPcmbTipoPlataforma)
        Me.TabERP.Location = New System.Drawing.Point(4, 22)
        Me.TabERP.Name = "TabERP"
        Me.TabERP.Padding = New System.Windows.Forms.Padding(3)
        Me.TabERP.Size = New System.Drawing.Size(977, 510)
        Me.TabERP.TabIndex = 0
        Me.TabERP.Text = "ERP"
        Me.TabERP.UseVisualStyleBackColor = True
        '
        'lblModulos
        '
        Me.lblModulos.AutoSize = True
        Me.lblModulos.Location = New System.Drawing.Point(307, 90)
        Me.lblModulos.Name = "lblModulos"
        Me.lblModulos.Size = New System.Drawing.Size(131, 13)
        Me.lblModulos.TabIndex = 32
        Me.lblModulos.Text = "Módulos Visiveis Utilizador"
        Me.lblModulos.Visible = False
        '
        'ERPLstBoxModulos
        '
        Me.ERPLstBoxModulos.FormattingEnabled = True
        Me.ERPLstBoxModulos.Location = New System.Drawing.Point(310, 107)
        Me.ERPLstBoxModulos.Name = "ERPLstBoxModulos"
        Me.ERPLstBoxModulos.Size = New System.Drawing.Size(147, 109)
        Me.ERPLstBoxModulos.TabIndex = 31
        Me.ERPLstBoxModulos.Visible = False
        '
        'ERPbtnArt
        '
        Me.ERPbtnArt.Location = New System.Drawing.Point(437, 22)
        Me.ERPbtnArt.Name = "ERPbtnArt"
        Me.ERPbtnArt.Size = New System.Drawing.Size(34, 20)
        Me.ERPbtnArt.TabIndex = 25
        Me.ERPbtnArt.Text = "..."
        Me.ERPbtnArt.UseVisualStyleBackColor = True
        '
        'lblArmazem
        '
        Me.lblArmazem.AutoSize = True
        Me.lblArmazem.Location = New System.Drawing.Point(307, 47)
        Me.lblArmazem.Name = "lblArmazem"
        Me.lblArmazem.Size = New System.Drawing.Size(50, 13)
        Me.lblArmazem.TabIndex = 27
        Me.lblArmazem.Text = "Armazem"
        '
        'lblARTMATLimpeza
        '
        Me.lblARTMATLimpeza.AutoSize = True
        Me.lblARTMATLimpeza.Location = New System.Drawing.Point(307, 5)
        Me.lblARTMATLimpeza.Name = "lblARTMATLimpeza"
        Me.lblARTMATLimpeza.Size = New System.Drawing.Size(74, 13)
        Me.lblARTMATLimpeza.TabIndex = 23
        Me.lblARTMATLimpeza.Text = "Artigo Material"
        '
        'ERPbtnArm
        '
        Me.ERPbtnArm.Location = New System.Drawing.Point(437, 64)
        Me.ERPbtnArm.Name = "ERPbtnArm"
        Me.ERPbtnArm.Size = New System.Drawing.Size(34, 20)
        Me.ERPbtnArm.TabIndex = 29
        Me.ERPbtnArm.Text = "..."
        Me.ERPbtnArm.UseVisualStyleBackColor = True
        '
        'ERPtxtArtMaterialDescricao
        '
        Me.ERPtxtArtMaterialDescricao.Location = New System.Drawing.Point(477, 22)
        Me.ERPtxtArtMaterialDescricao.Name = "ERPtxtArtMaterialDescricao"
        Me.ERPtxtArtMaterialDescricao.Size = New System.Drawing.Size(261, 20)
        Me.ERPtxtArtMaterialDescricao.TabIndex = 26
        '
        'ERPtxtArmazemDescricao
        '
        Me.ERPtxtArmazemDescricao.Location = New System.Drawing.Point(477, 64)
        Me.ERPtxtArmazemDescricao.Name = "ERPtxtArmazemDescricao"
        Me.ERPtxtArmazemDescricao.Size = New System.Drawing.Size(261, 20)
        Me.ERPtxtArmazemDescricao.TabIndex = 30
        '
        'ERPtxtArtMaterial
        '
        Me.ERPtxtArtMaterial.Location = New System.Drawing.Point(310, 21)
        Me.ERPtxtArtMaterial.Name = "ERPtxtArtMaterial"
        Me.ERPtxtArtMaterial.Size = New System.Drawing.Size(121, 20)
        Me.ERPtxtArtMaterial.TabIndex = 24
        '
        'ERPtxtArmazem
        '
        Me.ERPtxtArmazem.Location = New System.Drawing.Point(310, 64)
        Me.ERPtxtArmazem.Name = "ERPtxtArmazem"
        Me.ERPtxtArmazem.Size = New System.Drawing.Size(121, 20)
        Me.ERPtxtArmazem.TabIndex = 28
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(138, 91)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(137, 13)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Empresas Visiveis Utilizador"
        '
        'ERPLstBoxEmpresas
        '
        Me.ERPLstBoxEmpresas.FormattingEnabled = True
        Me.ERPLstBoxEmpresas.Location = New System.Drawing.Point(141, 108)
        Me.ERPLstBoxEmpresas.Name = "ERPLstBoxEmpresas"
        Me.ERPLstBoxEmpresas.Size = New System.Drawing.Size(147, 109)
        Me.ERPLstBoxEmpresas.TabIndex = 21
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Location = New System.Drawing.Point(138, 5)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(85, 13)
        Me.lblEmpresa.TabIndex = 17
        Me.lblEmpresa.Text = "Empresa Default"
        '
        'ERPcmbEmpresaDefault
        '
        Me.ERPcmbEmpresaDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ERPcmbEmpresaDefault.FormattingEnabled = True
        Me.ERPcmbEmpresaDefault.Location = New System.Drawing.Point(141, 21)
        Me.ERPcmbEmpresaDefault.Name = "ERPcmbEmpresaDefault"
        Me.ERPcmbEmpresaDefault.Size = New System.Drawing.Size(147, 21)
        Me.ERPcmbEmpresaDefault.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(138, 45)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Módulo Default"
        '
        'ERPcmbModulo
        '
        Me.ERPcmbModulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ERPcmbModulo.FormattingEnabled = True
        Me.ERPcmbModulo.Location = New System.Drawing.Point(141, 61)
        Me.ERPcmbModulo.Name = "ERPcmbModulo"
        Me.ERPcmbModulo.Size = New System.Drawing.Size(147, 21)
        Me.ERPcmbModulo.TabIndex = 14
        '
        'ERPtxtPasswordERP
        '
        Me.ERPtxtPasswordERP.Location = New System.Drawing.Point(9, 191)
        Me.ERPtxtPasswordERP.Name = "ERPtxtPasswordERP"
        Me.ERPtxtPasswordERP.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.ERPtxtPasswordERP.Size = New System.Drawing.Size(121, 20)
        Me.ERPtxtPasswordERP.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 174)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Password ERP"
        '
        'ERPtxtUtilizadorERP
        '
        Me.ERPtxtUtilizadorERP.Location = New System.Drawing.Point(9, 147)
        Me.ERPtxtUtilizadorERP.Name = "ERPtxtUtilizadorERP"
        Me.ERPtxtUtilizadorERP.Size = New System.Drawing.Size(121, 20)
        Me.ERPtxtUtilizadorERP.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Utilzador ERP"
        '
        'ERPtxtAplicacao
        '
        Me.ERPtxtAplicacao.Location = New System.Drawing.Point(9, 107)
        Me.ERPtxtAplicacao.Name = "ERPtxtAplicacao"
        Me.ERPtxtAplicacao.Size = New System.Drawing.Size(121, 20)
        Me.ERPtxtAplicacao.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Aplicação"
        '
        'ERPtxtInstancia
        '
        Me.ERPtxtInstancia.Location = New System.Drawing.Point(9, 62)
        Me.ERPtxtInstancia.Name = "ERPtxtInstancia"
        Me.ERPtxtInstancia.Size = New System.Drawing.Size(121, 20)
        Me.ERPtxtInstancia.TabIndex = 4
        '
        'lblInstancia
        '
        Me.lblInstancia.AutoSize = True
        Me.lblInstancia.Location = New System.Drawing.Point(6, 45)
        Me.lblInstancia.Name = "lblInstancia"
        Me.lblInstancia.Size = New System.Drawing.Size(50, 13)
        Me.lblInstancia.TabIndex = 3
        Me.lblInstancia.Text = "Instância"
        '
        'lblTipoPlataforma
        '
        Me.lblTipoPlataforma.AutoSize = True
        Me.lblTipoPlataforma.Location = New System.Drawing.Point(6, 5)
        Me.lblTipoPlataforma.Name = "lblTipoPlataforma"
        Me.lblTipoPlataforma.Size = New System.Drawing.Size(81, 13)
        Me.lblTipoPlataforma.TabIndex = 1
        Me.lblTipoPlataforma.Text = "Tipo Plataforma"
        '
        'ERPcmbTipoPlataforma
        '
        Me.ERPcmbTipoPlataforma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ERPcmbTipoPlataforma.FormattingEnabled = True
        Me.ERPcmbTipoPlataforma.Location = New System.Drawing.Point(9, 21)
        Me.ERPcmbTipoPlataforma.Name = "ERPcmbTipoPlataforma"
        Me.ERPcmbTipoPlataforma.Size = New System.Drawing.Size(121, 21)
        Me.ERPcmbTipoPlataforma.TabIndex = 0
        '
        'TabRegistoPontos
        '
        Me.TabRegistoPontos.Controls.Add(Me.TbRP)
        Me.TabRegistoPontos.Location = New System.Drawing.Point(4, 22)
        Me.TabRegistoPontos.Name = "TabRegistoPontos"
        Me.TabRegistoPontos.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRegistoPontos.Size = New System.Drawing.Size(977, 510)
        Me.TabRegistoPontos.TabIndex = 2
        Me.TabRegistoPontos.Text = "Registo Pontos"
        Me.TabRegistoPontos.UseVisualStyleBackColor = True
        '
        'TbRP
        '
        Me.TbRP.Controls.Add(Me.TabG)
        Me.TbRP.Controls.Add(Me.TabOP)
        Me.TbRP.Controls.Add(Me.TabP)
        Me.TbRP.Location = New System.Drawing.Point(3, 3)
        Me.TbRP.Name = "TbRP"
        Me.TbRP.SelectedIndex = 0
        Me.TbRP.Size = New System.Drawing.Size(966, 499)
        Me.TbRP.TabIndex = 1
        '
        'TabG
        '
        Me.TabG.Controls.Add(Me.GroupBox1)
        Me.TabG.Controls.Add(Me.RPckbApresentaBInicio)
        Me.TabG.Controls.Add(Me.RPcmbDocumento)
        Me.TabG.Controls.Add(Me.lblRPSerie)
        Me.TabG.Controls.Add(Me.RPcmbModoRegisto)
        Me.TabG.Controls.Add(Me.lblRPTipoDocumento)
        Me.TabG.Controls.Add(Me.Label5)
        Me.TabG.Controls.Add(Me.RPcmbSerie)
        Me.TabG.Controls.Add(Me.RPckbMultiplaSelecção)
        Me.TabG.Controls.Add(Me.lblTipoEntidade)
        Me.TabG.Controls.Add(Me.RPcmbTipoEntidade)
        Me.TabG.Controls.Add(Me.RPtxtRPDocumentoDescricao)
        Me.TabG.Controls.Add(Me.RPtxtRPSerieDescricao)
        Me.TabG.Controls.Add(Me.RPtxtRPDocumento)
        Me.TabG.Controls.Add(Me.RPtxtRPSerie)
        Me.TabG.Location = New System.Drawing.Point(4, 22)
        Me.TabG.Name = "TabG"
        Me.TabG.Padding = New System.Windows.Forms.Padding(3)
        Me.TabG.Size = New System.Drawing.Size(958, 473)
        Me.TabG.TabIndex = 1
        Me.TabG.Text = "Geral"
        Me.TabG.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RPlblTamanhoPostos)
        Me.GroupBox1.Controls.Add(Me.RPlblTamanhoOperacoes)
        Me.GroupBox1.Controls.Add(Me.RPTxtTamanhoPostos)
        Me.GroupBox1.Controls.Add(Me.RPTxtTamanhoOperaçõ)
        Me.GroupBox1.Controls.Add(Me.RPTxtTamanhoProjectos)
        Me.GroupBox1.Controls.Add(Me.RPlblTamanhoPeca)
        Me.GroupBox1.Controls.Add(Me.RPLblTamanhoProjectos)
        Me.GroupBox1.Controls.Add(Me.RPTxtTamanhoPeça)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 146)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(565, 94)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Geral Grelhas"
        '
        'RPlblTamanhoPostos
        '
        Me.RPlblTamanhoPostos.AutoSize = True
        Me.RPlblTamanhoPostos.Location = New System.Drawing.Point(5, 28)
        Me.RPlblTamanhoPostos.Name = "RPlblTamanhoPostos"
        Me.RPlblTamanhoPostos.Size = New System.Drawing.Size(87, 13)
        Me.RPlblTamanhoPostos.TabIndex = 22
        Me.RPlblTamanhoPostos.Text = "Tamanho Postos"
        '
        'RPlblTamanhoOperacoes
        '
        Me.RPlblTamanhoOperacoes.AutoSize = True
        Me.RPlblTamanhoOperacoes.Location = New System.Drawing.Point(423, 28)
        Me.RPlblTamanhoOperacoes.Name = "RPlblTamanhoOperacoes"
        Me.RPlblTamanhoOperacoes.Size = New System.Drawing.Size(107, 13)
        Me.RPlblTamanhoOperacoes.TabIndex = 28
        Me.RPlblTamanhoOperacoes.Text = "Tamanho Operações"
        '
        'RPTxtTamanhoPostos
        '
        Me.RPTxtTamanhoPostos.Location = New System.Drawing.Point(6, 44)
        Me.RPTxtTamanhoPostos.Name = "RPTxtTamanhoPostos"
        Me.RPTxtTamanhoPostos.Size = New System.Drawing.Size(121, 20)
        Me.RPTxtTamanhoPostos.TabIndex = 21
        '
        'RPTxtTamanhoOperaçõ
        '
        Me.RPTxtTamanhoOperaçõ.Location = New System.Drawing.Point(424, 44)
        Me.RPTxtTamanhoOperaçõ.Name = "RPTxtTamanhoOperaçõ"
        Me.RPTxtTamanhoOperaçõ.Size = New System.Drawing.Size(121, 20)
        Me.RPTxtTamanhoOperaçõ.TabIndex = 27
        '
        'RPTxtTamanhoProjectos
        '
        Me.RPTxtTamanhoProjectos.Location = New System.Drawing.Point(143, 44)
        Me.RPTxtTamanhoProjectos.Name = "RPTxtTamanhoProjectos"
        Me.RPTxtTamanhoProjectos.Size = New System.Drawing.Size(121, 20)
        Me.RPTxtTamanhoProjectos.TabIndex = 23
        '
        'RPlblTamanhoPeca
        '
        Me.RPlblTamanhoPeca.AutoSize = True
        Me.RPlblTamanhoPeca.Location = New System.Drawing.Point(282, 28)
        Me.RPlblTamanhoPeca.Name = "RPlblTamanhoPeca"
        Me.RPlblTamanhoPeca.Size = New System.Drawing.Size(85, 13)
        Me.RPlblTamanhoPeca.TabIndex = 26
        Me.RPlblTamanhoPeca.Text = "Tamanho Peças"
        '
        'RPLblTamanhoProjectos
        '
        Me.RPLblTamanhoProjectos.AutoSize = True
        Me.RPLblTamanhoProjectos.Location = New System.Drawing.Point(142, 28)
        Me.RPLblTamanhoProjectos.Name = "RPLblTamanhoProjectos"
        Me.RPLblTamanhoProjectos.Size = New System.Drawing.Size(99, 13)
        Me.RPLblTamanhoProjectos.TabIndex = 24
        Me.RPLblTamanhoProjectos.Text = "Tamanho Projectos"
        '
        'RPTxtTamanhoPeça
        '
        Me.RPTxtTamanhoPeça.Location = New System.Drawing.Point(283, 44)
        Me.RPTxtTamanhoPeça.Name = "RPTxtTamanhoPeça"
        Me.RPTxtTamanhoPeça.Size = New System.Drawing.Size(121, 20)
        Me.RPTxtTamanhoPeça.TabIndex = 25
        '
        'RPckbApresentaBInicio
        '
        Me.RPckbApresentaBInicio.AutoSize = True
        Me.RPckbApresentaBInicio.Location = New System.Drawing.Point(7, 123)
        Me.RPckbApresentaBInicio.Name = "RPckbApresentaBInicio"
        Me.RPckbApresentaBInicio.Size = New System.Drawing.Size(133, 17)
        Me.RPckbApresentaBInicio.TabIndex = 20
        Me.RPckbApresentaBInicio.Text = "Apresenta Botão Inicio"
        Me.RPckbApresentaBInicio.UseVisualStyleBackColor = True
        '
        'RPcmbDocumento
        '
        Me.RPcmbDocumento.Location = New System.Drawing.Point(269, 20)
        Me.RPcmbDocumento.Name = "RPcmbDocumento"
        Me.RPcmbDocumento.Size = New System.Drawing.Size(34, 20)
        Me.RPcmbDocumento.TabIndex = 11
        Me.RPcmbDocumento.Text = "..."
        Me.RPcmbDocumento.UseVisualStyleBackColor = True
        '
        'lblRPSerie
        '
        Me.lblRPSerie.AutoSize = True
        Me.lblRPSerie.Location = New System.Drawing.Point(139, 45)
        Me.lblRPSerie.Name = "lblRPSerie"
        Me.lblRPSerie.Size = New System.Drawing.Size(31, 13)
        Me.lblRPSerie.TabIndex = 13
        Me.lblRPSerie.Text = "Série"
        '
        'RPcmbModoRegisto
        '
        Me.RPcmbModoRegisto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RPcmbModoRegisto.FormattingEnabled = True
        Me.RPcmbModoRegisto.Items.AddRange(New Object() {"Diária", "Semanal", "Mensal"})
        Me.RPcmbModoRegisto.Location = New System.Drawing.Point(5, 59)
        Me.RPcmbModoRegisto.Name = "RPcmbModoRegisto"
        Me.RPcmbModoRegisto.Size = New System.Drawing.Size(125, 21)
        Me.RPcmbModoRegisto.TabIndex = 2
        '
        'lblRPTipoDocumento
        '
        Me.lblRPTipoDocumento.AutoSize = True
        Me.lblRPTipoDocumento.Location = New System.Drawing.Point(139, 3)
        Me.lblRPTipoDocumento.Name = "lblRPTipoDocumento"
        Me.lblRPTipoDocumento.Size = New System.Drawing.Size(62, 13)
        Me.lblRPTipoDocumento.TabIndex = 9
        Me.lblRPTipoDocumento.Text = "Documento"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(2, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Modo"
        '
        'RPcmbSerie
        '
        Me.RPcmbSerie.Location = New System.Drawing.Point(269, 62)
        Me.RPcmbSerie.Name = "RPcmbSerie"
        Me.RPcmbSerie.Size = New System.Drawing.Size(34, 20)
        Me.RPcmbSerie.TabIndex = 15
        Me.RPcmbSerie.Text = "..."
        Me.RPcmbSerie.UseVisualStyleBackColor = True
        '
        'RPckbMultiplaSelecção
        '
        Me.RPckbMultiplaSelecção.AutoSize = True
        Me.RPckbMultiplaSelecção.Location = New System.Drawing.Point(7, 100)
        Me.RPckbMultiplaSelecção.Name = "RPckbMultiplaSelecção"
        Me.RPckbMultiplaSelecção.Size = New System.Drawing.Size(145, 17)
        Me.RPckbMultiplaSelecção.TabIndex = 17
        Me.RPckbMultiplaSelecção.Text = "Multipla Selecção Postos"
        Me.RPckbMultiplaSelecção.UseVisualStyleBackColor = True
        '
        'lblTipoEntidade
        '
        Me.lblTipoEntidade.AutoSize = True
        Me.lblTipoEntidade.Location = New System.Drawing.Point(2, 3)
        Me.lblTipoEntidade.Name = "lblTipoEntidade"
        Me.lblTipoEntidade.Size = New System.Drawing.Size(73, 13)
        Me.lblTipoEntidade.TabIndex = 5
        Me.lblTipoEntidade.Text = "Tipo Entidade"
        '
        'RPcmbTipoEntidade
        '
        Me.RPcmbTipoEntidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RPcmbTipoEntidade.FormattingEnabled = True
        Me.RPcmbTipoEntidade.Location = New System.Drawing.Point(5, 19)
        Me.RPcmbTipoEntidade.Name = "RPcmbTipoEntidade"
        Me.RPcmbTipoEntidade.Size = New System.Drawing.Size(123, 21)
        Me.RPcmbTipoEntidade.TabIndex = 4
        '
        'RPtxtRPDocumentoDescricao
        '
        Me.RPtxtRPDocumentoDescricao.Location = New System.Drawing.Point(309, 20)
        Me.RPtxtRPDocumentoDescricao.Name = "RPtxtRPDocumentoDescricao"
        Me.RPtxtRPDocumentoDescricao.Size = New System.Drawing.Size(261, 20)
        Me.RPtxtRPDocumentoDescricao.TabIndex = 12
        '
        'RPtxtRPSerieDescricao
        '
        Me.RPtxtRPSerieDescricao.Location = New System.Drawing.Point(309, 62)
        Me.RPtxtRPSerieDescricao.Name = "RPtxtRPSerieDescricao"
        Me.RPtxtRPSerieDescricao.Size = New System.Drawing.Size(261, 20)
        Me.RPtxtRPSerieDescricao.TabIndex = 16
        '
        'RPtxtRPDocumento
        '
        Me.RPtxtRPDocumento.Location = New System.Drawing.Point(142, 20)
        Me.RPtxtRPDocumento.Name = "RPtxtRPDocumento"
        Me.RPtxtRPDocumento.Size = New System.Drawing.Size(121, 20)
        Me.RPtxtRPDocumento.TabIndex = 10
        '
        'RPtxtRPSerie
        '
        Me.RPtxtRPSerie.Location = New System.Drawing.Point(142, 62)
        Me.RPtxtRPSerie.Name = "RPtxtRPSerie"
        Me.RPtxtRPSerie.Size = New System.Drawing.Size(121, 20)
        Me.RPtxtRPSerie.TabIndex = 14
        '
        'TabOP
        '
        Me.TabOP.Controls.Add(Me.RPbtnRemovePostos)
        Me.TabOP.Controls.Add(Me.RPbtnAddPostos)
        Me.TabOP.Controls.Add(Me.RPbtnRemoveOP)
        Me.TabOP.Controls.Add(Me.RPbtnAddOP)
        Me.TabOP.Controls.Add(Me.Label20)
        Me.TabOP.Controls.Add(Me.Label21)
        Me.TabOP.Controls.Add(Me.RPlstOperacao1)
        Me.TabOP.Controls.Add(Me.RPlstPostos1)
        Me.TabOP.Location = New System.Drawing.Point(4, 22)
        Me.TabOP.Name = "TabOP"
        Me.TabOP.Padding = New System.Windows.Forms.Padding(3)
        Me.TabOP.Size = New System.Drawing.Size(958, 473)
        Me.TabOP.TabIndex = 2
        Me.TabOP.Text = "Operações/Postos"
        Me.TabOP.UseVisualStyleBackColor = True
        '
        'RPbtnRemovePostos
        '
        Me.RPbtnRemovePostos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Delete
        Me.RPbtnRemovePostos.Location = New System.Drawing.Point(829, 81)
        Me.RPbtnRemovePostos.Name = "RPbtnRemovePostos"
        Me.RPbtnRemovePostos.Size = New System.Drawing.Size(35, 37)
        Me.RPbtnRemovePostos.TabIndex = 40
        Me.RPbtnRemovePostos.UseVisualStyleBackColor = True
        '
        'RPbtnAddPostos
        '
        Me.RPbtnAddPostos.Image = CType(resources.GetObject("RPbtnAddPostos.Image"), System.Drawing.Image)
        Me.RPbtnAddPostos.Location = New System.Drawing.Point(829, 38)
        Me.RPbtnAddPostos.Name = "RPbtnAddPostos"
        Me.RPbtnAddPostos.Size = New System.Drawing.Size(35, 37)
        Me.RPbtnAddPostos.TabIndex = 39
        Me.RPbtnAddPostos.UseVisualStyleBackColor = True
        '
        'RPbtnRemoveOP
        '
        Me.RPbtnRemoveOP.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Delete
        Me.RPbtnRemoveOP.Location = New System.Drawing.Point(157, 81)
        Me.RPbtnRemoveOP.Name = "RPbtnRemoveOP"
        Me.RPbtnRemoveOP.Size = New System.Drawing.Size(35, 37)
        Me.RPbtnRemoveOP.TabIndex = 38
        Me.RPbtnRemoveOP.UseVisualStyleBackColor = True
        '
        'RPbtnAddOP
        '
        Me.RPbtnAddOP.Image = CType(resources.GetObject("RPbtnAddOP.Image"), System.Drawing.Image)
        Me.RPbtnAddOP.Location = New System.Drawing.Point(157, 38)
        Me.RPbtnAddOP.Name = "RPbtnAddOP"
        Me.RPbtnAddOP.Size = New System.Drawing.Size(35, 37)
        Me.RPbtnAddOP.TabIndex = 37
        Me.RPbtnAddOP.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label20.Location = New System.Drawing.Point(508, 12)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(72, 25)
        Me.Label20.TabIndex = 36
        Me.Label20.Text = "Postos"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label21.Location = New System.Drawing.Point(202, 10)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(99, 25)
        Me.Label21.TabIndex = 35
        Me.Label21.Text = "Operação"
        '
        'RPlstOperacao1
        '
        Me.RPlstOperacao1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RPlstOperacao1.FormattingEnabled = True
        Me.RPlstOperacao1.ItemHeight = 16
        Me.RPlstOperacao1.Location = New System.Drawing.Point(198, 38)
        Me.RPlstOperacao1.Name = "RPlstOperacao1"
        Me.RPlstOperacao1.Size = New System.Drawing.Size(298, 420)
        Me.RPlstOperacao1.TabIndex = 33
        '
        'RPlstPostos1
        '
        Me.RPlstPostos1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RPlstPostos1.FormattingEnabled = True
        Me.RPlstPostos1.ItemHeight = 16
        Me.RPlstPostos1.Location = New System.Drawing.Point(513, 38)
        Me.RPlstPostos1.Name = "RPlstPostos1"
        Me.RPlstPostos1.Size = New System.Drawing.Size(298, 420)
        Me.RPlstPostos1.TabIndex = 34
        '
        'TabP
        '
        Me.TabP.Controls.Add(Me.Label22)
        Me.TabP.Controls.Add(Me.Label23)
        Me.TabP.Controls.Add(Me.Label24)
        Me.TabP.Controls.Add(Me.lblOperacao)
        Me.TabP.Controls.Add(Me.RPlstFuncionariosPerm)
        Me.TabP.Controls.Add(Me.RPlstFuncionarios)
        Me.TabP.Controls.Add(Me.RPlstOperacao)
        Me.TabP.Controls.Add(Me.RPlstPostos)
        Me.TabP.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabP.Location = New System.Drawing.Point(4, 22)
        Me.TabP.Name = "TabP"
        Me.TabP.Padding = New System.Windows.Forms.Padding(3)
        Me.TabP.Size = New System.Drawing.Size(958, 473)
        Me.TabP.TabIndex = 0
        Me.TabP.Text = "Permissões"
        Me.TabP.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label22.Location = New System.Drawing.Point(745, 12)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(184, 25)
        Me.Label22.TabIndex = 33
        Me.Label22.Text = "Funcionários Permi."
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label23.Location = New System.Drawing.Point(482, 10)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(72, 25)
        Me.Label23.TabIndex = 32
        Me.Label23.Text = "Postos"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label24.Location = New System.Drawing.Point(10, 12)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(124, 25)
        Me.Label24.TabIndex = 31
        Me.Label24.Text = "Funcionários"
        '
        'lblOperacao
        '
        Me.lblOperacao.AutoSize = True
        Me.lblOperacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperacao.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblOperacao.Location = New System.Drawing.Point(209, 12)
        Me.lblOperacao.Name = "lblOperacao"
        Me.lblOperacao.Size = New System.Drawing.Size(99, 25)
        Me.lblOperacao.TabIndex = 30
        Me.lblOperacao.Text = "Operação"
        '
        'RPlstFuncionariosPerm
        '
        Me.RPlstFuncionariosPerm.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RPlstFuncionariosPerm.FormattingEnabled = True
        Me.RPlstFuncionariosPerm.ItemHeight = 16
        Me.RPlstFuncionariosPerm.Location = New System.Drawing.Point(750, 38)
        Me.RPlstFuncionariosPerm.Name = "RPlstFuncionariosPerm"
        Me.RPlstFuncionariosPerm.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.RPlstFuncionariosPerm.Size = New System.Drawing.Size(192, 420)
        Me.RPlstFuncionariosPerm.TabIndex = 29
        '
        'RPlstFuncionarios
        '
        Me.RPlstFuncionarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RPlstFuncionarios.FormattingEnabled = True
        Me.RPlstFuncionarios.ItemHeight = 16
        Me.RPlstFuncionarios.Location = New System.Drawing.Point(15, 38)
        Me.RPlstFuncionarios.Name = "RPlstFuncionarios"
        Me.RPlstFuncionarios.Size = New System.Drawing.Size(192, 420)
        Me.RPlstFuncionarios.TabIndex = 28
        '
        'RPlstOperacao
        '
        Me.RPlstOperacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RPlstOperacao.FormattingEnabled = True
        Me.RPlstOperacao.ItemHeight = 16
        Me.RPlstOperacao.Location = New System.Drawing.Point(214, 38)
        Me.RPlstOperacao.Name = "RPlstOperacao"
        Me.RPlstOperacao.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.RPlstOperacao.Size = New System.Drawing.Size(262, 420)
        Me.RPlstOperacao.TabIndex = 26
        '
        'RPlstPostos
        '
        Me.RPlstPostos.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RPlstPostos.FormattingEnabled = True
        Me.RPlstPostos.ItemHeight = 16
        Me.RPlstPostos.Location = New System.Drawing.Point(482, 38)
        Me.RPlstPostos.Name = "RPlstPostos"
        Me.RPlstPostos.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.RPlstPostos.Size = New System.Drawing.Size(262, 420)
        Me.RPlstPostos.TabIndex = 27
        '
        'TabPontos
        '
        Me.TabPontos.Controls.Add(Me.TextBox8)
        Me.TabPontos.Controls.Add(Me.PCBtnArmazem)
        Me.TabPontos.Controls.Add(Me.TextBox7)
        Me.TabPontos.Controls.Add(Me.Label7)
        Me.TabPontos.Controls.Add(Me.PCtxtArtigoLimpezaDescricao)
        Me.TabPontos.Controls.Add(Me.PCBtnArtigo)
        Me.TabPontos.Controls.Add(Me.PCtxtArtigoLimpeza)
        Me.TabPontos.Controls.Add(Me.Label6)
        Me.TabPontos.Location = New System.Drawing.Point(4, 22)
        Me.TabPontos.Name = "TabPontos"
        Me.TabPontos.Size = New System.Drawing.Size(977, 510)
        Me.TabPontos.TabIndex = 3
        Me.TabPontos.Text = "Pontos Clientes"
        Me.TabPontos.UseVisualStyleBackColor = True
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(173, 75)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(367, 20)
        Me.TextBox8.TabIndex = 12
        '
        'PCBtnArmazem
        '
        Me.PCBtnArmazem.Location = New System.Drawing.Point(133, 74)
        Me.PCBtnArmazem.Name = "PCBtnArmazem"
        Me.PCBtnArmazem.Size = New System.Drawing.Size(34, 20)
        Me.PCBtnArmazem.TabIndex = 11
        Me.PCBtnArmazem.Text = "..."
        Me.PCBtnArmazem.UseVisualStyleBackColor = True
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(6, 75)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(121, 20)
        Me.TextBox7.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 58)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 13)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Armazem Default"
        '
        'PCtxtArtigoLimpezaDescricao
        '
        Me.PCtxtArtigoLimpezaDescricao.Location = New System.Drawing.Point(173, 29)
        Me.PCtxtArtigoLimpezaDescricao.Name = "PCtxtArtigoLimpezaDescricao"
        Me.PCtxtArtigoLimpezaDescricao.Size = New System.Drawing.Size(367, 20)
        Me.PCtxtArtigoLimpezaDescricao.TabIndex = 8
        '
        'PCBtnArtigo
        '
        Me.PCBtnArtigo.Location = New System.Drawing.Point(133, 29)
        Me.PCBtnArtigo.Name = "PCBtnArtigo"
        Me.PCBtnArtigo.Size = New System.Drawing.Size(34, 20)
        Me.PCBtnArtigo.TabIndex = 7
        Me.PCBtnArtigo.Text = "..."
        Me.PCBtnArtigo.UseVisualStyleBackColor = True
        '
        'PCtxtArtigoLimpeza
        '
        Me.PCtxtArtigoLimpeza.Location = New System.Drawing.Point(6, 29)
        Me.PCtxtArtigoLimpeza.Name = "PCtxtArtigoLimpeza"
        Me.PCtxtArtigoLimpeza.Size = New System.Drawing.Size(121, 20)
        Me.PCtxtArtigoLimpeza.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Artigo Limpeza"
        '
        'TabVendasEncomendas
        '
        Me.TabVendasEncomendas.Controls.Add(Me.txtMoeda)
        Me.TabVendasEncomendas.Controls.Add(Me.Label9)
        Me.TabVendasEncomendas.Controls.Add(Me.txtCampoArtPreco)
        Me.TabVendasEncomendas.Controls.Add(Me.Label8)
        Me.TabVendasEncomendas.Location = New System.Drawing.Point(4, 22)
        Me.TabVendasEncomendas.Name = "TabVendasEncomendas"
        Me.TabVendasEncomendas.Padding = New System.Windows.Forms.Padding(3)
        Me.TabVendasEncomendas.Size = New System.Drawing.Size(977, 510)
        Me.TabVendasEncomendas.TabIndex = 4
        Me.TabVendasEncomendas.Text = "Vendas \ Compras"
        Me.TabVendasEncomendas.UseVisualStyleBackColor = True
        '
        'txtMoeda
        '
        Me.txtMoeda.Location = New System.Drawing.Point(8, 64)
        Me.txtMoeda.Name = "txtMoeda"
        Me.txtMoeda.Size = New System.Drawing.Size(121, 20)
        Me.txtMoeda.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(5, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Moeda"
        '
        'txtCampoArtPreco
        '
        Me.txtCampoArtPreco.Location = New System.Drawing.Point(8, 22)
        Me.txtCampoArtPreco.Name = "txtCampoArtPreco"
        Me.txtCampoArtPreco.Size = New System.Drawing.Size(121, 20)
        Me.txtCampoArtPreco.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(5, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Campo Artigo Preço"
        '
        'TabListaMateriais
        '
        Me.TabListaMateriais.Location = New System.Drawing.Point(4, 22)
        Me.TabListaMateriais.Name = "TabListaMateriais"
        Me.TabListaMateriais.Padding = New System.Windows.Forms.Padding(3)
        Me.TabListaMateriais.Size = New System.Drawing.Size(977, 510)
        Me.TabListaMateriais.TabIndex = 6
        Me.TabListaMateriais.Text = "Lista Materiais"
        Me.TabListaMateriais.UseVisualStyleBackColor = True
        '
        'TabStocks
        '
        Me.TabStocks.Controls.Add(Me.Label19)
        Me.TabStocks.Controls.Add(Me.SLstBoxDocumentos)
        Me.TabStocks.Controls.Add(Me.SckbApresentaEncomenda)
        Me.TabStocks.Controls.Add(Me.StxtSerieDescricao)
        Me.TabStocks.Controls.Add(Me.ScmbSerie)
        Me.TabStocks.Controls.Add(Me.StxtSerie)
        Me.TabStocks.Controls.Add(Me.Label17)
        Me.TabStocks.Controls.Add(Me.StxtDocumentoDescricao)
        Me.TabStocks.Controls.Add(Me.ScmbDocumento)
        Me.TabStocks.Controls.Add(Me.StxtDocumento)
        Me.TabStocks.Controls.Add(Me.Label18)
        Me.TabStocks.Controls.Add(Me.Label15)
        Me.TabStocks.Controls.Add(Me.SEmpresa)
        Me.TabStocks.Controls.Add(Me.Label16)
        Me.TabStocks.Controls.Add(Me.ScmbTipoEntidade)
        Me.TabStocks.Location = New System.Drawing.Point(4, 22)
        Me.TabStocks.Name = "TabStocks"
        Me.TabStocks.Padding = New System.Windows.Forms.Padding(3)
        Me.TabStocks.Size = New System.Drawing.Size(977, 510)
        Me.TabStocks.TabIndex = 7
        Me.TabStocks.Text = "Stocks"
        Me.TabStocks.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 93)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(124, 13)
        Me.Label19.TabIndex = 34
        Me.Label19.Text = "Documentos Disponiveis"
        '
        'SLstBoxDocumentos
        '
        Me.SLstBoxDocumentos.FormattingEnabled = True
        Me.SLstBoxDocumentos.Location = New System.Drawing.Point(6, 109)
        Me.SLstBoxDocumentos.Name = "SLstBoxDocumentos"
        Me.SLstBoxDocumentos.Size = New System.Drawing.Size(147, 109)
        Me.SLstBoxDocumentos.TabIndex = 33
        '
        'SckbApresentaEncomenda
        '
        Me.SckbApresentaEncomenda.AutoSize = True
        Me.SckbApresentaEncomenda.Location = New System.Drawing.Point(169, 109)
        Me.SckbApresentaEncomenda.Name = "SckbApresentaEncomenda"
        Me.SckbApresentaEncomenda.Size = New System.Drawing.Size(172, 17)
        Me.SckbApresentaEncomenda.TabIndex = 32
        Me.SckbApresentaEncomenda.Text = "Apresenta Módulo Encomenda"
        Me.SckbApresentaEncomenda.UseVisualStyleBackColor = True
        '
        'StxtSerieDescricao
        '
        Me.StxtSerieDescricao.Location = New System.Drawing.Point(307, 62)
        Me.StxtSerieDescricao.Name = "StxtSerieDescricao"
        Me.StxtSerieDescricao.Size = New System.Drawing.Size(272, 20)
        Me.StxtSerieDescricao.TabIndex = 31
        '
        'ScmbSerie
        '
        Me.ScmbSerie.Location = New System.Drawing.Point(267, 62)
        Me.ScmbSerie.Name = "ScmbSerie"
        Me.ScmbSerie.Size = New System.Drawing.Size(34, 20)
        Me.ScmbSerie.TabIndex = 30
        Me.ScmbSerie.Text = "..."
        Me.ScmbSerie.UseVisualStyleBackColor = True
        '
        'StxtSerie
        '
        Me.StxtSerie.Location = New System.Drawing.Point(140, 62)
        Me.StxtSerie.Name = "StxtSerie"
        Me.StxtSerie.Size = New System.Drawing.Size(121, 20)
        Me.StxtSerie.TabIndex = 29
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(137, 45)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(31, 13)
        Me.Label17.TabIndex = 28
        Me.Label17.Text = "Série"
        '
        'StxtDocumentoDescricao
        '
        Me.StxtDocumentoDescricao.Location = New System.Drawing.Point(307, 20)
        Me.StxtDocumentoDescricao.Name = "StxtDocumentoDescricao"
        Me.StxtDocumentoDescricao.Size = New System.Drawing.Size(272, 20)
        Me.StxtDocumentoDescricao.TabIndex = 27
        '
        'ScmbDocumento
        '
        Me.ScmbDocumento.Location = New System.Drawing.Point(267, 20)
        Me.ScmbDocumento.Name = "ScmbDocumento"
        Me.ScmbDocumento.Size = New System.Drawing.Size(34, 20)
        Me.ScmbDocumento.TabIndex = 26
        Me.ScmbDocumento.Text = "..."
        Me.ScmbDocumento.UseVisualStyleBackColor = True
        '
        'StxtDocumento
        '
        Me.StxtDocumento.Location = New System.Drawing.Point(140, 20)
        Me.StxtDocumento.Name = "StxtDocumento"
        Me.StxtDocumento.Size = New System.Drawing.Size(121, 20)
        Me.StxtDocumento.TabIndex = 25
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(137, 3)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(99, 13)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "Documento Default"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 2)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(48, 13)
        Me.Label15.TabIndex = 23
        Me.Label15.Text = "Empresa"
        '
        'SEmpresa
        '
        Me.SEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SEmpresa.FormattingEnabled = True
        Me.SEmpresa.Location = New System.Drawing.Point(6, 18)
        Me.SEmpresa.Name = "SEmpresa"
        Me.SEmpresa.Size = New System.Drawing.Size(125, 21)
        Me.SEmpresa.TabIndex = 22
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(3, 46)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(73, 13)
        Me.Label16.TabIndex = 21
        Me.Label16.Text = "Tipo Entidade"
        '
        'ScmbTipoEntidade
        '
        Me.ScmbTipoEntidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ScmbTipoEntidade.FormattingEnabled = True
        Me.ScmbTipoEntidade.Location = New System.Drawing.Point(6, 62)
        Me.ScmbTipoEntidade.Name = "ScmbTipoEntidade"
        Me.ScmbTipoEntidade.Size = New System.Drawing.Size(123, 21)
        Me.ScmbTipoEntidade.TabIndex = 20
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(225, 30)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 65
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(0, -1)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(204, 55)
        Me.CommandBarsFrame1.TabIndex = 64
        '
        'FrmConfINI
        '
        Me.ClientSize = New System.Drawing.Size(1001, 608)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.TabControl)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConfINI"
        Me.Text = "Configuração Sistema"
        Me.TabControl.ResumeLayout(False)
        Me.TabLicencimento.ResumeLayout(False)
        Me.TabLicencimento.PerformLayout()
        Me.TabERP.ResumeLayout(False)
        Me.TabERP.PerformLayout()
        Me.TabRegistoPontos.ResumeLayout(False)
        Me.TbRP.ResumeLayout(False)
        Me.TabG.ResumeLayout(False)
        Me.TabG.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabOP.ResumeLayout(False)
        Me.TabOP.PerformLayout()
        Me.TabP.ResumeLayout(False)
        Me.TabP.PerformLayout()
        Me.TabPontos.ResumeLayout(False)
        Me.TabPontos.PerformLayout()
        Me.TabVendasEncomendas.ResumeLayout(False)
        Me.TabVendasEncomendas.PerformLayout()
        Me.TabStocks.ResumeLayout(False)
        Me.TabStocks.PerformLayout()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents TabERP As System.Windows.Forms.TabPage
    Friend WithEvents ERPtxtPasswordERP As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ERPtxtUtilizadorERP As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ERPtxtAplicacao As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ERPtxtInstancia As System.Windows.Forms.TextBox
    Friend WithEvents lblInstancia As System.Windows.Forms.Label
    Friend WithEvents lblTipoPlataforma As System.Windows.Forms.Label
    Friend WithEvents ERPcmbTipoPlataforma As System.Windows.Forms.ComboBox
    Friend WithEvents TabRegistoPontos As System.Windows.Forms.TabPage
    Friend WithEvents TabPontos As System.Windows.Forms.TabPage
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents PCBtnArmazem As System.Windows.Forms.Button
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PCtxtArtigoLimpezaDescricao As System.Windows.Forms.TextBox
    Friend WithEvents PCBtnArtigo As System.Windows.Forms.Button
    Friend WithEvents PCtxtArtigoLimpeza As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TabVendasEncomendas As System.Windows.Forms.TabPage
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents txtMoeda As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCampoArtPreco As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TabLicencimento As System.Windows.Forms.TabPage
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents ERPcmbEmpresaDefault As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ERPcmbModulo As System.Windows.Forms.ComboBox
    Friend WithEvents LicBtnLicenca As System.Windows.Forms.Button
    Friend WithEvents LictxtLicenciamento As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LicLstView As System.Windows.Forms.ListView
    Friend WithEvents Empresa As System.Windows.Forms.ColumnHeader
    Friend WithEvents Módulo As System.Windows.Forms.ColumnHeader
    Friend WithEvents Limite As System.Windows.Forms.ColumnHeader
    Friend WithEvents NumPostos As System.Windows.Forms.ColumnHeader
    Friend WithEvents ERPLstBoxEmpresas As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TabListaMateriais As System.Windows.Forms.TabPage
    Friend WithEvents TabStocks As System.Windows.Forms.TabPage
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents SLstBoxDocumentos As System.Windows.Forms.CheckedListBox
    Friend WithEvents SckbApresentaEncomenda As System.Windows.Forms.CheckBox
    Friend WithEvents StxtSerieDescricao As System.Windows.Forms.TextBox
    Friend WithEvents ScmbSerie As System.Windows.Forms.Button
    Friend WithEvents StxtSerie As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents StxtDocumentoDescricao As System.Windows.Forms.TextBox
    Friend WithEvents ScmbDocumento As System.Windows.Forms.Button
    Friend WithEvents StxtDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SEmpresa As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ScmbTipoEntidade As System.Windows.Forms.ComboBox
    Friend WithEvents TbRP As TabControl
    Friend WithEvents TabG As TabPage
    Friend WithEvents RPckbApresentaBInicio As CheckBox
    Friend WithEvents RPcmbDocumento As Button
    Friend WithEvents lblRPSerie As Label
    Friend WithEvents RPcmbModoRegisto As ComboBox
    Friend WithEvents lblRPTipoDocumento As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents RPcmbSerie As Button
    Friend WithEvents RPckbMultiplaSelecção As CheckBox
    Friend WithEvents lblTipoEntidade As Label
    Friend WithEvents RPcmbTipoEntidade As ComboBox
    Friend WithEvents RPtxtRPDocumentoDescricao As TextBox
    Friend WithEvents RPtxtRPSerieDescricao As TextBox
    Friend WithEvents RPtxtRPDocumento As TextBox
    Friend WithEvents RPtxtRPSerie As TextBox
    Friend WithEvents TabOP As TabPage
    Friend WithEvents RPbtnRemovePostos As Button
    Friend WithEvents RPbtnAddPostos As Button
    Friend WithEvents RPbtnRemoveOP As Button
    Friend WithEvents RPbtnAddOP As Button
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents RPlstOperacao1 As ListBox
    Friend WithEvents RPlstPostos1 As ListBox
    Friend WithEvents TabP As TabPage
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents lblOperacao As Label
    Friend WithEvents RPlstFuncionariosPerm As ListBox
    Friend WithEvents RPlstFuncionarios As ListBox
    Friend WithEvents RPlstOperacao As ListBox
    Friend WithEvents RPlstPostos As ListBox
    Friend WithEvents RPLblTamanhoProjectos As Label
    Friend WithEvents RPTxtTamanhoProjectos As TextBox
    Friend WithEvents RPlblTamanhoPostos As Label
    Friend WithEvents RPTxtTamanhoPostos As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RPlblTamanhoOperacoes As Label
    Friend WithEvents RPTxtTamanhoOperaçõ As TextBox
    Friend WithEvents RPlblTamanhoPeca As Label
    Friend WithEvents RPTxtTamanhoPeça As TextBox
    Friend WithEvents ERPbtnArt As Button
    Friend WithEvents lblArmazem As Label
    Friend WithEvents lblARTMATLimpeza As Label
    Friend WithEvents ERPbtnArm As Button
    Friend WithEvents ERPtxtArtMaterialDescricao As TextBox
    Friend WithEvents ERPtxtArmazemDescricao As TextBox
    Friend WithEvents ERPtxtArtMaterial As TextBox
    Friend WithEvents ERPtxtArmazem As TextBox
    Friend WithEvents lblModulos As Label
    Friend WithEvents ERPLstBoxModulos As CheckedListBox
End Class
