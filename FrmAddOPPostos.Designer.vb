﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddOPPostos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAddOPPostos))
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.lblDescrição = New System.Windows.Forms.Label()
        Me.txtDescricao = New System.Windows.Forms.TextBox()
        Me.lblArtigo = New System.Windows.Forms.Label()
        Me.txtArtigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescricaoArt = New System.Windows.Forms.TextBox()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.btnLimpar = New System.Windows.Forms.Button()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(-3, 0)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(287, 55)
        Me.CommandBarsFrame1.TabIndex = 30
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodigo.Location = New System.Drawing.Point(10, 74)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(57, 25)
        Me.lblCodigo.TabIndex = 33
        Me.lblCodigo.Text = "Peça"
        '
        'txtCodigo
        '
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.Location = New System.Drawing.Point(128, 71)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(273, 30)
        Me.txtCodigo.TabIndex = 32
        '
        'lblDescrição
        '
        Me.lblDescrição.AutoSize = True
        Me.lblDescrição.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescrição.Location = New System.Drawing.Point(10, 124)
        Me.lblDescrição.Name = "lblDescrição"
        Me.lblDescrição.Size = New System.Drawing.Size(99, 25)
        Me.lblDescrição.TabIndex = 35
        Me.lblDescrição.Text = "Descrição"
        '
        'txtDescricao
        '
        Me.txtDescricao.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescricao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescricao.Location = New System.Drawing.Point(128, 124)
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.Size = New System.Drawing.Size(273, 30)
        Me.txtDescricao.TabIndex = 34
        '
        'lblArtigo
        '
        Me.lblArtigo.AutoSize = True
        Me.lblArtigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArtigo.Location = New System.Drawing.Point(10, 170)
        Me.lblArtigo.Name = "lblArtigo"
        Me.lblArtigo.Size = New System.Drawing.Size(63, 25)
        Me.lblArtigo.TabIndex = 37
        Me.lblArtigo.Text = "Artigo"
        '
        'txtArtigo
        '
        Me.txtArtigo.BackColor = System.Drawing.SystemColors.Window
        Me.txtArtigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArtigo.Location = New System.Drawing.Point(128, 170)
        Me.txtArtigo.Name = "txtArtigo"
        Me.txtArtigo.ReadOnly = True
        Me.txtArtigo.Size = New System.Drawing.Size(195, 30)
        Me.txtArtigo.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 219)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 25)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Descrição"
        '
        'txtDescricaoArt
        '
        Me.txtDescricaoArt.BackColor = System.Drawing.SystemColors.MenuBar
        Me.txtDescricaoArt.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescricaoArt.Location = New System.Drawing.Point(128, 219)
        Me.txtDescricaoArt.Name = "txtDescricaoArt"
        Me.txtDescricaoArt.ReadOnly = True
        Me.txtDescricaoArt.Size = New System.Drawing.Size(273, 30)
        Me.txtDescricaoArt.TabIndex = 38
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Mais
        Me.btnPesquisar.Location = New System.Drawing.Point(329, 170)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(34, 35)
        Me.btnPesquisar.TabIndex = 40
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'btnLimpar
        '
        Me.btnLimpar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Delete
        Me.btnLimpar.Location = New System.Drawing.Point(367, 170)
        Me.btnLimpar.Name = "btnLimpar"
        Me.btnLimpar.Size = New System.Drawing.Size(34, 35)
        Me.btnLimpar.TabIndex = 41
        Me.btnLimpar.UseVisualStyleBackColor = True
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(353, 12)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 66
        '
        'FrmAddOPPostos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 253)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.btnLimpar)
        Me.Controls.Add(Me.btnPesquisar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDescricaoArt)
        Me.Controls.Add(Me.lblArtigo)
        Me.Controls.Add(Me.txtArtigo)
        Me.Controls.Add(Me.lblDescrição)
        Me.Controls.Add(Me.txtDescricao)
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(900, 900)
        Me.MinimizeBox = False
        Me.Name = "FrmAddOPPostos"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Adicionar Peças"
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents lblDescrição As System.Windows.Forms.Label
    Friend WithEvents txtDescricao As System.Windows.Forms.TextBox
    Friend WithEvents lblArtigo As System.Windows.Forms.Label
    Friend WithEvents txtArtigo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDescricaoArt As System.Windows.Forms.TextBox
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents btnLimpar As System.Windows.Forms.Button
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
End Class
