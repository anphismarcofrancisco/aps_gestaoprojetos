﻿Module Formula

    Public Function Evaluer(ByVal Txt As String) As String
        Dim i As Integer, oNB As Integer, fNB As Integer
        Dim P1 As Integer, P2 As Integer
        Dim Buff As String
        Dim T As String
        'Pour les calculs y faut un point à la place de la virgule
        Txt = Replace(Txt, ",", ".")
        'Voir s'il y a des (
        For i = 1 To Len(Txt)
            If Mid(Txt, i, 1) = "(" Then oNB = oNB + 1
        Next i
        'S'il y a des ( (ouvrantes), voir si elle sont validée par  des ) (fermantes)
        If oNB > 0 Then
            For i = 1 To Len(Txt)
                If Mid(Txt, i, 1) = ")" Then fNB = fNB + 1
            Next i
        Else
            'Pas de parenthèse, Evalue directement le calcul
            Evaluer = EvalueExpression(Txt)
            Exit Function
        End If
        If oNB <> fNB Then
            'Les parenthèses ne sont pas concordantes, mettre  message erreur parenthèse
            Exit Function
        End If

        While oNB > 0
            'recherche la dernière parenthèse ouvrante
            P1 = InStrRev(Txt, "(")
            'Recherche la parenthèse fermante de l'expression
            P2 = InStr(Mid(Txt, P1 + 1), ")")
            'Evalue l'expression qui est entre parenthèses
            Buff = EvalueExpression(Mid(Txt, P1 + 1, P2 - 1))
            'Remplacer l'expression par le résultat et supprimer les parenthèses
            Txt = Left(Txt, P1 - 1) & Buff & Mid(Txt, P1 + P2 + 1)
            oNB = oNB - 1
        End While
        'plus de parenthèse, évaluer la dernière expression
        Evaluer = EvalueExpression(Txt)

    End Function
    Function EvalueExpression(a As String) As String
        Dim T As Integer, S As Integer
        Dim B As String, i As Integer, C As Boolean
        Dim c1 As Double, c2 As Double, Signe As Integer
        Dim R As String, Fin As Boolean, z As Integer

        'enlever les espace
        a = Replace(a, " ", "")

        While Not Fin
            For i = 1 To Len(a)
                T = Asc(Mid(a, i, 1))
                If T < 48 And T <> 46 Or i = Len(a) Then
                    If C Then 'évalue
                        If i = Len(a) Then
                            c2 = Val(Mid(a, S))
                        Else
                            c2 = Val(Mid(a, S, i - S))
                        End If
                        R = Str(CalculSimple(c1, c2, Signe))
                        If i = Len(a) Then
                            Fin = True
                        Else
                            a = Trim(R & Mid(a, i))
                            C = False
                        End If
                        Exit For
                    Else 'sépare le 1er chiffre
                        c1 = Val(Left(a, i - 1))
                        Signe = T
                        S = i + 1
                        C = True
                    End If
                End If
            Next i
        End While
        'remplacer l'expression par le résultat
        EvalueExpression = Trim(R)
    End Function

    Function CalculSimple(n1 As Double, n2 As Double, Signe As Integer) As Double
        Select Case Signe
            Case 43 ' +
                CalculSimple = n1 + n2
            Case 45 ' -
                CalculSimple = n1 - n2
            Case 42 ' *
                CalculSimple = n1 * n2
            Case 47 ' /
                CalculSimple = n1 / n2
                'Ici, ajouter d'autre calcul...
        End Select
    End Function


End Module
