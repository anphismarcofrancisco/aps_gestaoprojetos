﻿
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports StdBE800

Public Class FrmEditarRegistos

    Dim clicouConfirmar_ As Boolean


    Const COLUMN_DOCUMENTO As Integer = 0
    Const COLUMN_CODIGOFUNC As Integer = 1
    Const COLUMN_NOMEFUNC As Integer = 2
    Const COLUMN_Armazem As Integer = 3
    Const COLUMN_ARTIGO As Integer = 4
    Const COLUMN_DESCRICAO As Integer = 5
    Const COLUMN_QUANTIDADE As Integer = 6
    Const COLUMN_PROJECTO As Integer = 7
    Const COLUMN_ESTADO As Integer = 8

    Const ID_SERVICO_CONFERIDO As Integer = 80
    Const ID_SERVICO_ABRIR As Integer = 81



    '  Const COLUMN_DATA As Integer = 6

    ReadOnly Property clicouConfirmar() As Boolean
        Get
            clicouConfirmar = clicouConfirmar_
        End Get

    End Property

    Private Sub FrmEditar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim m As Motor
        m = Motor.GetInstance
        iniciarToolBox()
        dtpDataI.Value = m.firstDayMonth(Now)
        dtpDataF.Value = m.lastDayMonth(Now)

        gridRegistos.AutoColumnSizing = False
        gridRegistos.AutoColumnSizing = False
        gridRegistos.ShowGroupBox = True
        gridRegistos.ShowItemsInGroups = False

        buscarListaFuncionariosPermissoes()
        inserirColunasRegistos()
        gridRegistos.SortOrder.DeleteAll()

        gridRegistos.SortOrder.Add(gridRegistos.Columns(COLUMN_DOCUMENTO))

        buscarRegistos("", cmbFuncionarios.SelectedValue)
        clicouConfirmar_ = False

    End Sub



    Private Sub buscarListaFuncionariosPermissoes()
        Dim dt As DataTable
        Dim m As Motor
        m = Motor.GetInstance
        dt = m.daListaFuncionariosPermissoes()
        cmbFuncionarios.DataSource = dt
        cmbFuncionarios.ValueMember = "Codigo"
        cmbFuncionarios.DisplayMember = "Nome"
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Actualizar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Actualizar"

            Control = .Add(xtpControlButton, 2, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Public Function getIdLinha() As String
        getIdLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    getIdLinha = gridRegistos.SelectedRows(0).Record.Tag.ToString
                End If
            End If
        End If
    End Function

    Public Function getValorLinha(ByVal coluna As Integer) As String
        getValorLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    getValorLinha = gridRegistos.SelectedRows(0).Record(coluna).Value
                End If
            End If
        End If
    End Function

    Private Sub gridRegistos_CellMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs)
        clicouConfirmar_ = True
        Me.Close()
    End Sub

    Private Sub inserirColunasRegistos()

        Dim Column As ReportColumn
        Column = inserirColuna(True, COLUMN_DOCUMENTO, "Registo", 180, False, True)
        Column = inserirColuna(True, COLUMN_CODIGOFUNC, "CODIGOFUNC", 180, False, True)
        Column.Visible = False
        Column = inserirColuna(True, COLUMN_NOMEFUNC, "Funcionário", 180, False, True)
        Column = inserirColuna(True, COLUMN_Armazem, "Armazém", 50, False, True)
        Column = inserirColuna(True, COLUMN_ARTIGO, "Artigo", 120, False, True)
        Column = inserirColuna(True, COLUMN_DESCRICAO, "Descrição", 330, False, True)
        Column = inserirColuna(True, COLUMN_QUANTIDADE, "Quantidade", 100, False, True)
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(True, COLUMN_PROJECTO, "Projecto", 100, False, True)
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(True, COLUMN_ESTADO, "ESTADO", 100, False, True)
        Column.Visible = False
    End Sub

    Private Function inserirColuna(ByVal isGridSE As Boolean, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = gridRegistos.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function

    Private Sub buscarRegistos(ByVal centroTrabalho As String, ByVal funcionario As String)
        Dim m As Motor
        Dim total As Double
        m = Motor.GetInstance

        gridRegistos.Records.DeleteAll()
        gridRegistos.HeaderRecords.DeleteAll()
        gridRegistos.FooterRecords.DeleteAll()
        Dim dtRegistos As DataTable
        dtRegistos = m.daListaRegistosPontos(centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value)

        inserirLinhasRegistos(dtRegistos)
        If gridRegistos.GroupsOrder.Count = 0 Then
            gridRegistos.GroupsOrder.DeleteAll()
            gridRegistos.GroupsOrder.Add(gridRegistos.Columns(buscarIndexColunaDocumento))
            gridRegistos.Columns(buscarIndexColunaDocumento).Visible = False
        End If

        total = m.daTotalRegistos(centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value, "P")

        inserirLinhaTotal(total)
        gridRegistos.Populate()
        '  inserirSubtotais()
    End Sub

    Private Function buscarIndexColunaDocumento() As Integer

        Dim col As ReportColumn
        For Each col In gridRegistos.Columns
            If col.Caption = "Documento" Then
                Return col.Index
            End If
        Next
        Return COLUMN_DOCUMENTO
    End Function
    Private Sub inserirLinhaTotal(ByVal total As Double)
        Dim frr As ReportRecord

        gridRegistos.ShowFooterRows = True

        frr = gridRegistos.FooterRecords.Add()

        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("Total")
        frr.AddItem(total)
        frr.AddItem("")



    End Sub
    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow)
        Next
    End Sub



    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim documento As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim estado As String
        estado = m.NuloToString(dtRow("Estado"))
        If estado = "" Then
            If IsDBNull(dtRow("CDU_DataFinal")) Then
                estado = "I"
            Else
                estado = "C"
            End If
        End If
        record = gridRegistos.Records.Add()
        record.Tag = dtRow("ID")

        If Not IsDBNull(dtRow("IdCabecInternos")) Then
            estado = verificarDocumentoJaTransformado(dtRow("IdCabecInternos").ToString, estado)
        End If
        If estado <> "P" Then
            estado = estado
        End If
        documento = ""
        If IsDBNull(dtRow("Serie")) Then
            documento = m.NuloToString(dtRow("Nome")) + " de " + FormatDateTime(dtRow("Data"), DateFormat.ShortDate) + " | " + m.NuloToString(dtRow("Molde")) + " | " + m.NuloToString(dtRow("Requisicao")) + " | " + m.NuloToString(dtRow("Problema"))
        Else
            documento = m.NuloToString(dtRow("TipoDoc")) + " | " + m.NuloToString(dtRow("Serie")) + " | " + m.NuloToString(dtRow("NumDoc")) + " de " + m.NuloToString(dtRow("Nome")) + " de " + FormatDateTime(dtRow("Data"), DateFormat.ShortDate) + " | " + m.NuloToString(dtRow("Molde")) + " | " + m.NuloToString(dtRow("Requisicao")) + " | " + m.NuloToString(dtRow("Problema"))
        End If

        Item = record.AddItem(documento)
        Item.Caption = documento
        Item.Tag = dtRow("ID")
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Utilizador"))
        Item.Tag = "Entidade"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("NomeFunc"))
        Item.Tag = "Nome"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Armazem"))
        Item.Tag = "Armazem"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Artigo"))
        Item.Tag = "Artigo"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = "Descricao"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Quantidade"))
        Item.Tag = "Quantidade"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Projecto"))
        Item.Tag = "Projecto"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(estado)
        If IsDBNull(dtRow("IdCabecInternos")) Then
            Item.Tag = ""
        Else
            Item.Tag = dtRow("IdCabecInternos").ToString
        End If
        aplicarFormatoLinha(estado, Item)


    End Sub

    Private Sub aplicarFormatoLinha(ByVal estado As String, ByVal item As ReportRecordItem)

        Select Case estado
            Case "N" : item.BackColor = 12632256
            Case "F" : item.BackColor = 14671839
            Case "T" : item.BackColor = 10000000
            Case "A" : item.BackColor = 12632900
            Case "P" : item.BackColor = 12632900
            Case "B" : item.BackColor = RGB(255, 255, 255)
            Case "I" : item.BackColor = RGB(255, 255, 0)
            Case "C" : item.BackColor = RGB(255, 255, 255)
            Case "G" : item.BackColor = RGB(255, 255, 255)
        End Select


    End Sub

    Private Function verificarDocumentoJaTransformado(ByVal idCabecDoc As String, ByVal estado As String) As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim count As Integer
        count = m.consultaValor("Select count(*) from linhasdoc where IdLinhaOrigemCopia in (select id from LinhasInternos where IdCabecInternos='" + idCabecDoc + "')")
        If count > 0 Then
            estado = "T"
        End If
        Return estado
    End Function

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute

        ' Dim rowCT As DataRowView
        ' rowCT = cmbCentroTrabalho.SelectedItem
        Dim m As Motor
        m = Motor.GetInstance
        Select Case e.control.Id
            Case 1 : buscarRegistos("", cmbFuncionarios.SelectedValue)
            Case 2 : imprimir()
            Case 3 : Me.Close()
            Case ID_SERVICO_CONFERIDO : actualizarServico(IIf(m.getMOLDULOP = "I", "A", "P"))
            Case ID_SERVICO_ABRIR : actualizarServico(IIf(m.getMOLDULOP = "I", "B", "G"))
        End Select
    End Sub
    Private Sub imprimir()
        'gridRegistos.PrintPreviewOptions.Title = ""
        gridRegistos.PrintOptions.Header.FormatString = "Registos de Folhas Ponto" + vbCrLf + vbCrLf + "De " + CStr(dtpDataI.Value) + " a " + CStr(dtpDataF.Value)
        gridRegistos.PrintOptions.Header.Font.Size = 12
        gridRegistos.PrintOptions.BlackWhitePrinting = True
        gridRegistos.PrintOptions.Landscape = False
        gridRegistos.PrintPreview(True)
    End Sub

    Private Sub FrmEditar_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        gridRegistos.Width = Me.Width - (45)
        gridRegistos.Height = Me.Height - (gridRegistos.Top + 45)
    End Sub

    Private Sub gridRegistos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridRegistos.RowDblClick


        If Not e.item Is Nothing Then
            Dim estado As String
            estado = e.item.Record.Item(COLUMN_ESTADO).Value
            If estado = "B" Or estado = "I" Or estado = "C" Or estado = "G" Then
                Dim m As Motor
                m = Motor.GetInstance
                If e.item.Record.Item(COLUMN_CODIGOFUNC).Value = m.Funcionario Or m.FuncionarioAdm Then
                    clicouConfirmar_ = True
                    Me.Close()
                Else
                    mostrarMensagem("O")
                End If
            Else
                mostrarMensagem(estado)
            End If
        End If
    End Sub

    Private Sub mostrarMensagem(ByVal estado As String)
        Dim m As Motor
        m = Motor.GetInstance
        Select Case estado
            Case "N" : MsgBox("O registo já se encontra Anulado.", MsgBoxStyle.Exclamation)
            Case "F" : MsgBox("O registo já se encontra Fechado.", MsgBoxStyle.Exclamation)
            Case "T" : MsgBox("O registo já se encontra Transformado em Factura.", MsgBoxStyle.Exclamation)
            Case "O" : MsgBox("O registo seleccionado é de outro funcionário.", MsgBoxStyle.Exclamation)
            Case "A" : MsgBox("O registo já se encontra conferido. " + IIf(m.FuncionarioAdm, vbCrLf + vbCrLf + "Tem de Abrir o Serviço para o poder editar.", ""), MsgBoxStyle.Exclamation)
            Case "P" : MsgBox("O registo já se encontra conferido. " + IIf(m.FuncionarioAdm, vbCrLf + vbCrLf + "Tem de Abrir o Serviço para o poder editar.", ""), MsgBoxStyle.Exclamation)
        End Select
    End Sub

    Private Sub inserirSubtotais()
        '   gridRegistos.ReCalc(True)
        Dim m As Motor
        Dim row As ReportRow
        Dim groupRow As ReportGroupRow
        m = Motor.GetInstance
        For i = 0 To gridRegistos.Rows.Count - 1
            row = gridRegistos.Rows(i)
            If row.GroupRow Then
                groupRow = row

                'groupRow.GroupFormat = " [SubTotal $=%.02f]"
                groupRow.GroupCaption = groupRow.GroupCaption + " - " + "Qtd: " + CStr(buscarTotaisGroup(groupRow))  ' CStr(m.daTotalRegistosCT(row.Childs(0).Record.Item(COLUMN_POSTO).Value, dtpDataI.Value, dtpDataF.Value)) + " H"
                'gridRegistos.GroupsOrder.
                'groupRow.GroupFormula = "SUMSUB(R*C2:R*C4)" 'Old notation
                '  groupRow.GroupFormula = "SUMSUB(C2:C3) SUMSUB(C3:C4)" 'New (short) notation
                ' groupRow.GroupCaption = "x"
            End If
        Next

    End Sub

    Private Function buscarTotaisGroup(ByVal groupRow As ReportGroupRow) As Double
        Dim total As Double
        total = 0
        Dim linha As ReportRow
        For Each linha In groupRow.Childs
            If Not linha.GroupRow Then
                total = total + linha.Record(COLUMN_QUANTIDADE).Value
            Else
                total = total + buscarTotaisGroup(linha)
            End If
        Next
        Return total
    End Function

   
   
    Private Sub gridRegistos_MouseUpEvent(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridRegistos.MouseUpEvent
        Dim popup As CommandBar
        Dim Control As CommandBarControl
        Dim estado As String


        If (e.button = 2 And Not gridRegistos.FocusedRow Is Nothing) Then
            Dim m As Motor
            m = Motor.GetInstance
            estado = gridRegistos.FocusedRow.Record(COLUMN_ESTADO).Value
            'caso o estado esteja no estado B - aberto ou  "A" aprovado
            If (estado = "A" Or estado = "B" Or estado = "C" Or estado = "P" Or estado = "G") And m.FuncionarioAdm Then
                popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
                With popup.Controls
                    Control = .Add(xtpControlButton, IIf((estado = "B" Or estado = "C" Or estado = "G"), ID_SERVICO_CONFERIDO, ID_SERVICO_ABRIR), IIf((estado = "B" Or estado = "C" Or estado = "G"), "Serviço Conferido", "Abrir Serviço"), -1, False)
                    Control.BeginGroup = True
                End With
                popup.ShowPopup()
            End If
        End If
    End Sub

    Private Sub actualizarServico(ByVal estado As String)
        If Not gridRegistos.FocusedRow Is Nothing Then
            Dim m As Motor
            Dim id As String
            Dim idRegisto As String
            m = Motor.GetInstance
            id = gridRegistos.FocusedRow.Record(COLUMN_ESTADO).Tag
            idRegisto = gridRegistos.FocusedRow.Record.Tag.ToString
            gridRegistos.FocusedRow.Record(COLUMN_ESTADO).Value = estado
            If m.getMOLDULOP = "I" Then
                m.executarComando("UPDATE CabecInternos set Estado='" + estado + "' WHERE ID='" + id + "'")
                If estado = "A" Then
                    If m.getGRAVAFINALIZAR = False Then
                        m.gravarDocumento(idRegisto)
                    End If
                End If
            Else
                If m.getGRAVAFINALIZAR = False Then
                    If estado = "P" Then
                        m.gravarDocumento(idRegisto)
                    Else
                        m.removerDocumentoVenda(id)
                        m.executarComando("UPDATE TDU_APSCabecRegistoFolhaPonto set CDU_IdCabec=NULL WHERE CDU_Id='" + idRegisto + "'")
                        m.executarComando("UPDATE TDU_APSLinhasRegistoFolhaPonto set CDU_IdCabecInternos=NULL WHERE CDU_IdTDUCabec='" + idRegisto + "'")
                    End If
                Else
                    m.executarComando("UPDATE CabecDocStatus set Estado='" + estado + "' WHERE IDCabecDOc='" + id + "'")
                End If
                End If
            actualizarCoresGrelha(idRegisto, estado)
            gridRegistos.Populate()
            MsgBox("Serviço actualizado para o estado " + IIf(estado = "A" Or estado = "P", "Aprovado", "Aberto") + " com sucesso", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub actualizarCoresGrelha(ByVal idCabecDoc As String, ByVal estado As String)
        Dim row As ReportRow
        Dim i As Integer
        For Each row In gridRegistos.Rows
            If Not row.GroupRow Then
                If row.Record.Tag.ToString = idCabecDoc Then
                    row.Record(COLUMN_ESTADO).Value = estado
                    For i = 0 To gridRegistos.Columns.Count - 1
                        aplicarFormatoLinha(estado, row.Record.Item(i))
                    Next
                End If
            End If
        Next
    End Sub


  
   
End Class
