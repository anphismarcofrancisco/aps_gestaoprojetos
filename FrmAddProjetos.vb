﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle

Public Class FrmAddProjetos

    Const MSGERRO1 As String = "O Projeto não se encontra preenchido"
    Const MSGERRO2 As String = "A Descrição do projeto se encontra preenchida"
    Const MSGERRO3 As String = "O projeto ""PROJETO"" já existe!"
    Const MSGERRO4 As String = "O projeto ""PROJETO"" inserido com sucesso!"

    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Dim peca As String
    Public Sub setObjecto(objecto As String)
        peca = objecto
    End Sub



    Private Sub FrmAddPecas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        inicializarForm()
        txtProjeto.Text = peca
        botaoSeleccionado_ = 3
    End Sub

    Private Sub inicializarForm()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"

            'Control = .Add(xtpControlButton, 2, " Remover", -1, False)
            'Control.BeginGroup = True
            'Control.DescriptionText = "Remover"
            'Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub


    Private Sub inicializarProjeto(strProjeto As String)
        txtDescricao.Text = ""


        If strProjeto <> "" Then
            Dim m As Motor
            m = Motor.GetInstance
            If m.projetoExiste(strProjeto) Then
                Dim dt As DataTable
                dt = m.consultaDataTable("SELECT Codigo,Descricao FROM COP_Obras WHERE Codigo='" + strProjeto + "'")


                If dt.Rows.Count > 0 Then
                    txtProjeto.Text = m.NuloToString(dt.Rows(0)("Codigo"))
                    txtDescricao.Text = m.NuloToString(dt.Rows(0)("Descricao"))
                End If
            End If
        End If
    End Sub
    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        If e.control.Index = 1 Then
            adicionarProjeto()
        End If
        If e.control.Index = 2 Then
            botaoSeleccionado_ = 2
            Me.Close()
        End If
    End Sub

    Private Sub adicionarProjeto()
        Dim projeto As String
        Dim descricao As String


        projeto = txtProjeto.Text.Trim
        descricao = txtDescricao.Text.Trim

        If projeto = "" Then
            MsgBox(MSGERRO1, MsgBoxStyle.Critical)
            txtProjeto.Focus()
            Exit Sub
        End If

        If descricao = "" Then
            MsgBox(MSGERRO2, MsgBoxStyle.Critical)
            txtDescricao.Focus()
            Exit Sub
        End If



        Dim m As Motor
        m = Motor.GetInstance()

        If m.projetoExiste(peca) Then
            MsgBox(MSGERRO3.Replace("PROJETO", projeto), MsgBoxStyle.Critical)
            txtProjeto.Focus()
        Else
            m.inserirProjeto(projeto, descricao)
            MsgBox(MSGERRO4.Replace("PROJETO", projeto), MsgBoxStyle.Information)
            Me.Close()
            botaoSeleccionado_ = 1
        End If

    End Sub

    Private Sub txtProjeto_TextChanged(sender As Object, e As EventArgs) Handles txtProjeto.TextChanged
        inicializarProjeto(txtProjeto.Text)
    End Sub
End Class