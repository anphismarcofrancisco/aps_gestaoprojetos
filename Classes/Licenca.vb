﻿Imports System.IO
Imports PlatAPSNET

Public Class Licenca

    Dim seg As Seguranca
    Dim ini As IniFile
    Dim hashLicencimento As Hashtable



    Public Sub New(ficheiroConf As String, Optional numeroLicenciamentos As Integer = 0)

        hashLicencimento = New Hashtable()

        seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")
        ini = New IniFile(ficheiroConf)

        If ini.GetString("ERP", "LICENCA", "") <> "" Then

            ' licenca = seg.decifrarStringTripleDES(ini.GetString("ERP", "LICENCA", ""))

            lerLicenca(seg.decifrarStringTripleDES(ini.GetString("ERP", "LICENCA", "")), numeroLicenciamentos)

            ' ativarLicenca(seg.decifrarStringTripleDES(licenca))

        End If


    End Sub

    Public Sub New()
        hashLicencimento = New Hashtable()
    End Sub

    ''<description> ativa licença e devolve a mensagem de sucesso/insucesso
    'Private Function ativarLicenca(ByVal _licenca As String) As String
    '    Dim licence() As String

    '    'Se conter o # é valida
    '    If _licenca.Contains("#") Then

    '        'Faz split da licença
    '        licence = _licenca.Split("#")
    '        If licence.Count = 3 Then

    '            'Listas
    '            empresas = licence(0).Split(",")
    '            modulos = licence(1)
    '            datalimite = licence(2)

    '            'Registar a licença no ficheiro
    '            registarLicenca(_licenca)

    '        Else
    '            'Caso não tenha #
    '            Return "A licença está incompleta."

    '        End If
    '    Else
    '        Return "A licença é inválida."
    '    End If

    '    Return ""

    'End Function

    '<description> ativa licença e devolve a mensagem de sucesso/insucesso
    Private Sub registarLicenca(ByVal _licenca As String)

        Try

            ini.WriteString("ERP", "LICENCA", seg.encriptarStringTripleDES(_licenca))

        Catch ex As Exception
            MsgBox("Houve um problema ao registar a licença: " + ex.Message)
        End Try

    End Sub

    Public Function validarLicenca(empresa As String, modulo As String, ByRef msg As String) As Boolean
        Dim licemp As EmpresasLicenciamento
        '  empresa = "RTJ"
        licemp = hashLicencimento(empresa)

        If Not licemp Is Nothing Then
            msg = licemp.validarModulo(modulo)
            Return msg = ""
        Else
            msg = "A empresa " + empresa + " não se encontra licenciada por nenhum dos módulos."
            Return False
        End If


    End Function


    Public Function daNumeroPostos(empresa As String) As Integer
        Dim licemp As EmpresasLicenciamento
        licemp = hashLicencimento(empresa)

        If Not licemp Is Nothing Then
            Return licemp.NumPostos
        Else
            Return 0
        End If
    End Function

    ''' <summary>
    ''' Função que permite ir buscar o numero de empresas disponiveis
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaEmpresas() As String
        Dim empresas As String = ""
        For Each empresa In hashLicencimento.Keys
            If empresas <> "" Then
                empresas += ","
            End If
            empresas = empresas + "'" + empresa + "'"
        Next
        Return empresas
    End Function

    Public Function daListaModulosEmpresa(empresa As String) As String
        Dim licemp As EmpresasLicenciamento
        licemp = hashLicencimento(empresa)

        If Not licemp Is Nothing Then
            Return licemp.Modulos
        Else
            Return 0
        End If
    End Function

    Public Function daCaracteristicasLicencaEmpresa(empresa As String) As EmpresasLicenciamento
        Dim licemp As EmpresasLicenciamento
        licemp = hashLicencimento(empresa)

        If Not licemp Is Nothing Then
            Return licemp
        Else
            Return Nothing
        End If
    End Function


    Public Sub lerLicenca(licenca As String, Optional numeroLicenciamentos As Integer = 0)
        Dim licenca1() As String

        licenca1 = licenca.Split("|")

        For i = 0 To licenca1.Length - 1
            carregarLicencaEmpresas(licenca1(i), numeroLicenciamentos)
        Next

    End Sub

    Private Sub carregarLicencaEmpresas(linhaLicenca As String, Optional numeroLicenciamentos As Integer = 0)
        Dim _licenca() As String
        Dim empresas() As String
        Dim empresa As String
        Dim modulos As String
        Dim numPostos As Integer
        Dim dataLimite As String = ""

        numPostos = -1
        _licenca = linhaLicenca.Split("#")

        empresas = _licenca(0).Split(",")
        For i = 0 To empresas.Length - 1
            empresa = empresas(i)
            modulos = _licenca(1)
            If _licenca.Length > 2 Then
                datalimite = _licenca(2)
            End If
            If _licenca.Length > 3 Then
                If IsNumeric(_licenca(3)) Then
                    numPostos = CInt(_licenca(3))
                Else
                    numPostos = numeroLicenciamentos
                End If
            Else
                numPostos = numeroLicenciamentos
            End If
            carregarLicencaEmpresa(empresa, modulos, datalimite, numPostos)
        Next
    End Sub

    Private Sub carregarLicencaEmpresa(empresa As String, modulos As String, datalimite As String, numpostos As Integer)
        Dim licemp As EmpresasLicenciamento
        Dim novo As Boolean = True
        Dim modulo As String = ""
        Dim modulosDisponiveis As String = ""
        Dim strmodulosAPL As String = ""
        Dim modulosAPL() As String
        licemp = hashLicencimento(empresa)



        If licemp Is Nothing Then
            licemp = New EmpresasLicenciamento
        Else
            novo = False
        End If

        modulosAPL = ini.GetString("ERP", "MODULOS", "").Split(",")

        For Each modulo In modulosAPL

            If modulos.ToUpper.IndexOf(modulo.ToUpper()) <> -1 Then
                modulosDisponiveis += modulo
                If modulosDisponiveis <> "" Then
                    modulosDisponiveis += ","
                End If
            End If

        Next



        licemp.Empresa = empresa
        licemp.Modulos = modulosDisponiveis
        licemp.DataLimite = datalimite
        licemp.NumPostos = numpostos
        If novo Then hashLicencimento.Add(empresa, licemp)

    End Sub

    Public Function possuiModulo(modulo As String) As Boolean
        Dim empresas As String = ""
        Dim emplic As EmpresasLicenciamento
        For Each empresa In hashLicencimento.Keys
            emplic = hashLicencimento(empresa)
            If emplic.Modulos.ToUpper.IndexOf(modulo.ToUpper()) <> -1 Then
                Return True
            End If
        Next
        Return False
    End Function


    'Public Function eValida() As Boolean

    '    If Not IsNothing(empresas) And Not IsNothing(modulos) And Not IsNothing(datalimite) Then
    '        If empresas.Count > 0 And modulos <> "" And datalimite > Date.Now.Date Then
    '            Return True
    '        Else
    '            Return False
    '        End If
    '    Else
    '        Return False
    '    End If
    'End Function
End Class
