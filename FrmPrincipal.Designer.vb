﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPrincipal))
        Me.lstMoldes = New System.Windows.Forms.ListBox()
        Me.lstPecas = New System.Windows.Forms.ListBox()
        Me.lstOperacao = New System.Windows.Forms.ListBox()
        Me.lblPostos = New System.Windows.Forms.Label()
        Me.lblMoldes = New System.Windows.Forms.Label()
        Me.lblPecas = New System.Windows.Forms.Label()
        Me.lblOperacao = New System.Windows.Forms.Label()
        Me.lstPostos = New System.Windows.Forms.ListBox()
        Me.lblREdicao = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnGestConf = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.btnParar = New System.Windows.Forms.Button()
        Me.btnIniciar = New System.Windows.Forms.Button()
        Me.btnAddP = New System.Windows.Forms.Button()
        Me.cmbFuncionarios = New System.Windows.Forms.ComboBox()
        Me.lblNomeCliente = New System.Windows.Forms.Label()
        Me.txtPesqOperacao = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtPesqPecas = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtPesqMolde = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtPesqPosto = New AxXtremeSuiteControls.AxFlatEdit()
        CType(Me.txtPesqOperacao, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqPecas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqMolde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqPosto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstMoldes
        '
        Me.lstMoldes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable
        Me.lstMoldes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMoldes.FormattingEnabled = True
        Me.lstMoldes.ItemHeight = 25
        Me.lstMoldes.Location = New System.Drawing.Point(172, 77)
        Me.lstMoldes.Name = "lstMoldes"
        Me.lstMoldes.Size = New System.Drawing.Size(200, 404)
        Me.lstMoldes.TabIndex = 3
        '
        'lstPecas
        '
        Me.lstPecas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstPecas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPecas.FormattingEnabled = True
        Me.lstPecas.ItemHeight = 25
        Me.lstPecas.Location = New System.Drawing.Point(390, 77)
        Me.lstPecas.Name = "lstPecas"
        Me.lstPecas.Size = New System.Drawing.Size(253, 404)
        Me.lstPecas.TabIndex = 4
        '
        'lstOperacao
        '
        Me.lstOperacao.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstOperacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOperacao.FormattingEnabled = True
        Me.lstOperacao.ItemHeight = 25
        Me.lstOperacao.Location = New System.Drawing.Point(661, 79)
        Me.lstOperacao.Name = "lstOperacao"
        Me.lstOperacao.Size = New System.Drawing.Size(315, 404)
        Me.lstOperacao.TabIndex = 5
        '
        'lblPostos
        '
        Me.lblPostos.AutoSize = True
        Me.lblPostos.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostos.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblPostos.Location = New System.Drawing.Point(7, 13)
        Me.lblPostos.Name = "lblPostos"
        Me.lblPostos.Size = New System.Drawing.Size(154, 25)
        Me.lblPostos.TabIndex = 13
        Me.lblPostos.Text = "Centro Trabalho"
        '
        'lblMoldes
        '
        Me.lblMoldes.AutoSize = True
        Me.lblMoldes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoldes.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblMoldes.Location = New System.Drawing.Point(172, 13)
        Me.lblMoldes.Name = "lblMoldes"
        Me.lblMoldes.Size = New System.Drawing.Size(66, 25)
        Me.lblMoldes.TabIndex = 14
        Me.lblMoldes.Text = "Molde"
        '
        'lblPecas
        '
        Me.lblPecas.AutoSize = True
        Me.lblPecas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPecas.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblPecas.Location = New System.Drawing.Point(385, 13)
        Me.lblPecas.Name = "lblPecas"
        Me.lblPecas.Size = New System.Drawing.Size(57, 25)
        Me.lblPecas.TabIndex = 15
        Me.lblPecas.Text = "Peça"
        '
        'lblOperacao
        '
        Me.lblOperacao.AutoSize = True
        Me.lblOperacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperacao.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblOperacao.Location = New System.Drawing.Point(656, 13)
        Me.lblOperacao.Name = "lblOperacao"
        Me.lblOperacao.Size = New System.Drawing.Size(99, 25)
        Me.lblOperacao.TabIndex = 16
        Me.lblOperacao.Text = "Operação"
        '
        'lstPostos
        '
        Me.lstPostos.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable
        Me.lstPostos.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPostos.FormattingEnabled = True
        Me.lstPostos.ItemHeight = 25
        Me.lstPostos.Location = New System.Drawing.Point(12, 77)
        Me.lstPostos.Name = "lstPostos"
        Me.lstPostos.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstPostos.Size = New System.Drawing.Size(154, 404)
        Me.lstPostos.TabIndex = 8
        '
        'lblREdicao
        '
        Me.lblREdicao.AutoSize = True
        Me.lblREdicao.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblREdicao.ForeColor = System.Drawing.Color.OliveDrab
        Me.lblREdicao.Location = New System.Drawing.Point(812, 13)
        Me.lblREdicao.Name = "lblREdicao"
        Me.lblREdicao.Size = New System.Drawing.Size(315, 22)
        Me.lblREdicao.TabIndex = 17
        Me.lblREdicao.Text = "Registo Ponto em modo de edição"
        Me.lblREdicao.Visible = False
        '
        'ToolTip
        '
        Me.ToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        '
        'btnGestConf
        '
        Me.btnGestConf.Image = Global.APS_GestaoProjectos.My.Resources.Resources.configure16
        Me.btnGestConf.Location = New System.Drawing.Point(1135, 4)
        Me.btnGestConf.Name = "btnGestConf"
        Me.btnGestConf.Size = New System.Drawing.Size(34, 34)
        Me.btnGestConf.TabIndex = 36
        Me.ToolTip.SetToolTip(Me.btnGestConf, "Menu Configuração")
        Me.btnGestConf.UseVisualStyleBackColor = True
        Me.btnGestConf.Visible = False
        '
        'btnAdd
        '
        Me.btnAdd.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        Me.btnAdd.Location = New System.Drawing.Point(610, 41)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(34, 34)
        Me.btnAdd.TabIndex = 18
        Me.ToolTip.SetToolTip(Me.btnAdd, "Adicionar Peça")
        Me.btnAdd.UseVisualStyleBackColor = True
        Me.btnAdd.Visible = False
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Registos
        Me.btnEditar.Location = New System.Drawing.Point(993, 41)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(176, 65)
        Me.btnEditar.TabIndex = 7
        Me.ToolTip.SetToolTip(Me.btnEditar, "Permite Consultar e Editar Registos")
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Fechar
        Me.btnSair.Location = New System.Drawing.Point(993, 418)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(176, 65)
        Me.btnSair.TabIndex = 6
        Me.ToolTip.SetToolTip(Me.btnSair, "Sair")
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'btnParar
        '
        Me.btnParar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PFinalizar
        Me.btnParar.Location = New System.Drawing.Point(993, 263)
        Me.btnParar.Name = "btnParar"
        Me.btnParar.Size = New System.Drawing.Size(176, 65)
        Me.btnParar.TabIndex = 1
        Me.ToolTip.SetToolTip(Me.btnParar, "Permite Terminar e Registar Operação")
        Me.btnParar.UseVisualStyleBackColor = True
        '
        'btnIniciar
        '
        Me.btnIniciar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PDir
        Me.btnIniciar.Location = New System.Drawing.Point(993, 173)
        Me.btnIniciar.Name = "btnIniciar"
        Me.btnIniciar.Size = New System.Drawing.Size(176, 65)
        Me.btnIniciar.TabIndex = 0
        Me.ToolTip.SetToolTip(Me.btnIniciar, "Permite Iniciar a Operação")
        Me.btnIniciar.UseVisualStyleBackColor = True
        '
        'btnAddP
        '
        Me.btnAddP.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        Me.btnAddP.Location = New System.Drawing.Point(338, 38)
        Me.btnAddP.Name = "btnAddP"
        Me.btnAddP.Size = New System.Drawing.Size(34, 34)
        Me.btnAddP.TabIndex = 37
        Me.ToolTip.SetToolTip(Me.btnAddP, "Adicionar Peça")
        Me.btnAddP.UseVisualStyleBackColor = True
        Me.btnAddP.Visible = False
        '
        'cmbFuncionarios
        '
        Me.cmbFuncionarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFuncionarios.DropDownWidth = 300
        Me.cmbFuncionarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFuncionarios.FormattingEnabled = True
        Me.cmbFuncionarios.Location = New System.Drawing.Point(993, 112)
        Me.cmbFuncionarios.Name = "cmbFuncionarios"
        Me.cmbFuncionarios.Size = New System.Drawing.Size(176, 33)
        Me.cmbFuncionarios.TabIndex = 34
        '
        'lblNomeCliente
        '
        Me.lblNomeCliente.AutoSize = True
        Me.lblNomeCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNomeCliente.Location = New System.Drawing.Point(13, 485)
        Me.lblNomeCliente.Name = "lblNomeCliente"
        Me.lblNomeCliente.Size = New System.Drawing.Size(91, 13)
        Me.lblNomeCliente.TabIndex = 35
        Me.lblNomeCliente.Text = "lblNomeCliente"
        '
        'txtPesqOperacao
        '
        Me.txtPesqOperacao.Location = New System.Drawing.Point(661, 43)
        Me.txtPesqOperacao.Name = "txtPesqOperacao"
        Me.txtPesqOperacao.OcxState = CType(resources.GetObject("txtPesqOperacao.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqOperacao.Size = New System.Drawing.Size(315, 32)
        Me.txtPesqOperacao.TabIndex = 22
        '
        'txtPesqPecas
        '
        Me.txtPesqPecas.Location = New System.Drawing.Point(390, 41)
        Me.txtPesqPecas.Name = "txtPesqPecas"
        Me.txtPesqPecas.OcxState = CType(resources.GetObject("txtPesqPecas.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqPecas.Size = New System.Drawing.Size(214, 32)
        Me.txtPesqPecas.TabIndex = 21
        '
        'txtPesqMolde
        '
        Me.txtPesqMolde.Location = New System.Drawing.Point(172, 41)
        Me.txtPesqMolde.Name = "txtPesqMolde"
        Me.txtPesqMolde.OcxState = CType(resources.GetObject("txtPesqMolde.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqMolde.Size = New System.Drawing.Size(157, 32)
        Me.txtPesqMolde.TabIndex = 20
        '
        'txtPesqPosto
        '
        Me.txtPesqPosto.Location = New System.Drawing.Point(12, 41)
        Me.txtPesqPosto.Name = "txtPesqPosto"
        Me.txtPesqPosto.OcxState = CType(resources.GetObject("txtPesqPosto.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqPosto.Size = New System.Drawing.Size(154, 32)
        Me.txtPesqPosto.TabIndex = 19
        '
        'FrmPrincipal
        '
        Me.ClientSize = New System.Drawing.Size(1191, 500)
        Me.Controls.Add(Me.btnAddP)
        Me.Controls.Add(Me.btnGestConf)
        Me.Controls.Add(Me.lblNomeCliente)
        Me.Controls.Add(Me.cmbFuncionarios)
        Me.Controls.Add(Me.txtPesqOperacao)
        Me.Controls.Add(Me.txtPesqPecas)
        Me.Controls.Add(Me.txtPesqMolde)
        Me.Controls.Add(Me.txtPesqPosto)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.lblREdicao)
        Me.Controls.Add(Me.lblOperacao)
        Me.Controls.Add(Me.lblPecas)
        Me.Controls.Add(Me.lblMoldes)
        Me.Controls.Add(Me.lblPostos)
        Me.Controls.Add(Me.lstPostos)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.lstOperacao)
        Me.Controls.Add(Me.lstPecas)
        Me.Controls.Add(Me.lstMoldes)
        Me.Controls.Add(Me.btnParar)
        Me.Controls.Add(Me.btnIniciar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(917, 512)
        Me.Name = "FrmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestão Projectos - Registo Ponto"
        CType(Me.txtPesqOperacao, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqPecas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqMolde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqPosto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnIniciar As System.Windows.Forms.Button
    Friend WithEvents btnParar As System.Windows.Forms.Button
    Friend WithEvents lstMoldes As System.Windows.Forms.ListBox
    Friend WithEvents lstPecas As System.Windows.Forms.ListBox
    Friend WithEvents lstOperacao As System.Windows.Forms.ListBox
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents lblPostos As System.Windows.Forms.Label
    Friend WithEvents lblMoldes As System.Windows.Forms.Label
    Friend WithEvents lblPecas As System.Windows.Forms.Label
    Friend WithEvents lblOperacao As System.Windows.Forms.Label
    Friend WithEvents lstPostos As System.Windows.Forms.ListBox
    Friend WithEvents lblREdicao As System.Windows.Forms.Label
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtPesqPosto As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtPesqMolde As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtPesqPecas As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtPesqOperacao As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents cmbFuncionarios As System.Windows.Forms.ComboBox
    Friend WithEvents lblNomeCliente As Label
    Friend WithEvents btnGestConf As Button
    Friend WithEvents btnAddP As Button
End Class
