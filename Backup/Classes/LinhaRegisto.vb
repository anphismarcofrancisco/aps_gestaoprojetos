﻿Public Class LinhaRegisto
    ''' <summary>
    ''' ID do Registo
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_ID_ As String
    Property ID() As String
        Get
            ID = CDU_ID_
        End Get
        Set(ByVal value As String)
            CDU_ID_ = value
        End Set
    End Property


    ''' <summary>
    ''' CDU_IDLinhaInterno
    ''' </summary>
    ''' <remarks></remarks>
    Dim CDU_IDLinhaInterno_ As String
    Property CDU_IDLinhaInterno() As String
        Get
            CDU_IDLinhaInterno = CDU_IDLinhaInterno_
        End Get
        Set(ByVal value As String)
            CDU_IDLinhaInterno_ = value
        End Set
    End Property

    ''' <summary>
    ''' NumLinha
    ''' </summary>
    ''' <remarks></remarks>
    Dim NumLinha_ As Double
    Property NumLinha() As Double
        Get
            NumLinha = NumLinha_
        End Get
        Set(ByVal value As Double)
            NumLinha_ = value
        End Set
    End Property

    ''' <summary>
    ''' TipoArtigo
    ''' </summary>
    ''' <remarks></remarks>
    Dim TipoArtigo_ As String
    Property TipoArtigo() As String
        Get
            TipoArtigo = TipoArtigo_
        End Get
        Set(ByVal value As String)
            TipoArtigo_ = value
        End Set
    End Property

    ''' <summary>
    ''' Artigo
    ''' </summary>
    ''' <remarks></remarks>
    Dim Artigo_ As String
    Property Artigo() As String
        Get
            Artigo = Artigo_
        End Get
        Set(ByVal value As String)
            Artigo_ = value
        End Set
    End Property

    ''' <summary>
    ''' Armazem
    ''' </summary>
    ''' <remarks></remarks>
    Dim Armazem_ As String
    Property Armazem() As String
        Get
            Armazem = Armazem_
        End Get
        Set(ByVal value As String)
            Armazem_ = value
        End Set
    End Property
    ''' <summary>
    ''' Quantidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim Quantidade_ As Double
    Property Quantidade() As Double
        Get
            Quantidade = Quantidade_
        End Get
        Set(ByVal value As Double)
            Quantidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' Projecto
    ''' </summary>
    ''' <remarks></remarks>
    Dim Projecto_ As String
    Property Projecto() As String
        Get
            Projecto = Projecto_
        End Get
        Set(ByVal value As String)
            Projecto_ = value
        End Set
    End Property




End Class
