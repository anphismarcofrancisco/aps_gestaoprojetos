﻿Public Class ConexaoOLEDB
    Inherits Conexao

    Public Sub New(ByVal servidor As String, ByVal baseDados As String, ByVal utilizador As String, ByVal password As String)
        MyBase.New("Provider=sqloledb; Data Source=" + servidor + ";Initial Catalog=" + baseDados + ";User Id=" + utilizador + ";Password=" + password + ";")
    End Sub


    Public Sub New(connectionString As String)
        MyBase.New(connectionString)
    End Sub

End Class