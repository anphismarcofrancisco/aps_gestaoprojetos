﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGestAvaliacaoFornecedores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGestAvaliacaoFornecedores))
        Me.lstAno = New System.Windows.Forms.ComboBox()
        Me.lblAno = New System.Windows.Forms.Label()
        Me.lblFornecedor = New System.Windows.Forms.Label()
        Me.btnForn = New System.Windows.Forms.Button()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.txtNomeEntidade = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtEntidade = New AxXtremeSuiteControls.AxFlatEdit()
        Me.gridRegistos = New AxXtremeReportControl.AxReportControl()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNomeEntidade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEntidade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridRegistos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstAno
        '
        Me.lstAno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.lstAno.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAno.FormattingEnabled = True
        Me.lstAno.Location = New System.Drawing.Point(1168, 19)
        Me.lstAno.Name = "lstAno"
        Me.lstAno.Size = New System.Drawing.Size(153, 37)
        Me.lstAno.TabIndex = 36
        '
        'lblAno
        '
        Me.lblAno.AutoSize = True
        Me.lblAno.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAno.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblAno.Location = New System.Drawing.Point(1108, 25)
        Me.lblAno.Name = "lblAno"
        Me.lblAno.Size = New System.Drawing.Size(54, 25)
        Me.lblAno.TabIndex = 67
        Me.lblAno.Text = "Ano:"
        '
        'lblFornecedor
        '
        Me.lblFornecedor.AutoSize = True
        Me.lblFornecedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFornecedor.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblFornecedor.Location = New System.Drawing.Point(12, 62)
        Me.lblFornecedor.Name = "lblFornecedor"
        Me.lblFornecedor.Size = New System.Drawing.Size(112, 25)
        Me.lblFornecedor.TabIndex = 71
        Me.lblFornecedor.Text = "Fornecedor"
        '
        'btnForn
        '
        Me.btnForn.Image = Global.APS_GestaoProjectos.My.Resources.Resources.ZoomHS
        Me.btnForn.Location = New System.Drawing.Point(169, 89)
        Me.btnForn.Name = "btnForn"
        Me.btnForn.Size = New System.Drawing.Size(40, 35)
        Me.btnForn.TabIndex = 70
        Me.btnForn.UseVisualStyleBackColor = True
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(519, 32)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 85
        '
        'txtNomeEntidade
        '
        Me.txtNomeEntidade.Location = New System.Drawing.Point(215, 89)
        Me.txtNomeEntidade.Name = "txtNomeEntidade"
        Me.txtNomeEntidade.OcxState = CType(resources.GetObject("txtNomeEntidade.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtNomeEntidade.Size = New System.Drawing.Size(1106, 35)
        Me.txtNomeEntidade.TabIndex = 68
        '
        'txtEntidade
        '
        Me.txtEntidade.Location = New System.Drawing.Point(12, 89)
        Me.txtEntidade.Name = "txtEntidade"
        Me.txtEntidade.OcxState = CType(resources.GetObject("txtEntidade.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtEntidade.Size = New System.Drawing.Size(151, 35)
        Me.txtEntidade.TabIndex = 69
        '
        'gridRegistos
        '
        Me.gridRegistos.Location = New System.Drawing.Point(12, 144)
        Me.gridRegistos.Name = "gridRegistos"
        Me.gridRegistos.OcxState = CType(resources.GetObject("gridRegistos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridRegistos.Size = New System.Drawing.Size(1309, 526)
        Me.gridRegistos.TabIndex = 34
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(-2, 1)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(515, 55)
        Me.CommandBarsFrame1.TabIndex = 33
        '
        'FrmGestAvaliacaoFornecedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1341, 695)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.txtNomeEntidade)
        Me.Controls.Add(Me.txtEntidade)
        Me.Controls.Add(Me.lblFornecedor)
        Me.Controls.Add(Me.btnForn)
        Me.Controls.Add(Me.lblAno)
        Me.Controls.Add(Me.lstAno)
        Me.Controls.Add(Me.gridRegistos)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmGestAvaliacaoFornecedores"
        Me.Text = "Avaliação de Fornecedores"
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNomeEntidade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEntidade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridRegistos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gridRegistos As AxXtremeReportControl.AxReportControl
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lstAno As ComboBox
    Friend WithEvents lblAno As Label
    Friend WithEvents txtNomeEntidade As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtEntidade As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lblFornecedor As Label
    Friend WithEvents btnForn As Button
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
End Class
