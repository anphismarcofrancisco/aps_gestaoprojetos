﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports PlatAPSNET
Public Class FrmConfPontos

    Dim ficheiroConfiguracao As Configuracao
    Dim ini As IniFile
    Dim seg As Seguranca
    Dim lic As Licenca

    Const ID_CONFIRMAR As Integer = 1
    Const ID_CANCELAR As Integer = 3

    Private Sub FrmConfPontos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim fileINI As String
        Dim iniFile As String
        iniFile = "CONFIG.ini"
        fileINI = Application.StartupPath + "\\" + iniFile
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)

        ficheiroConfiguracao = Configuracao.GetInstance()
        ini = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao())
        seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

        Dim m As Motor
        m = Motor.GetInstance
        m.AplicacaoInicializada = True

        iniciarToolBox()
        carregarForm()
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, ID_CONFIRMAR, " Gravar", -1, False)
            Control.IconId = 1
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"


            Control = .Add(xtpControlButton, ID_CANCELAR, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub CommandBarsFrame1_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        If e.control.Id = ID_CONFIRMAR Then
            If TabControl1.SelectedIndex = 1 Then gravarFuncionarios()
            If TabControl1.SelectedIndex = 2 Then gravarPostos()
        End If

        If e.control.Id = ID_CANCELAR Then
            Me.Close()
        End If
    End Sub

    Private Sub gravarFuncionarios()
        Dim m As Motor
        Dim funcionario As String
        Dim funcAdmin As String
        Dim funcAddPecas As String

        m = Motor.GetInstance
        m.IniciaTransacao()
        Try

            funcionario = txtFunc.Text
            funcAdmin = IIf(ckbAdmin.Checked, "1", "0")
            funcAddPecas = IIf(ckbAddPecas.Checked, "1", "0")

            If Not ckbAdmin.Checked Then
                m.executarComando("UPDATE Funcionarios set  CDU_Administrador=" + funcAdmin + " WHERE Codigo='" + funcionario + "'")
            End If

            m.executarComando("UPDATE Funcionarios set CDU_Password='" + txtPassword.Text + "', CDU_AdministradorRP=" + funcAdmin + ", CDU_AdicionaPecas=" + funcAddPecas + " WHERE Codigo='" + funcionario + "'")

            m.removePermissoes(funcionario)
            inserePermissoesFuncionarioOperacoes(funcionario)
            inserePermissoesFuncionarioPostos(funcionario)
            inserePermissoesFuncionarioFuncionarios(funcionario)

            m.TerminaTransacao()
            MsgBox("Permissões configurados com sucesso", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            m.DesfazTransacao()
        End Try

    End Sub
    Private Sub gravarPostos()
        Dim m As Motor
        Dim posto As String
        Dim funcAdmin As String

        m = Motor.GetInstance
        m.IniciaTransacao()
        Try

            posto = lstPostos3.SelectedValue


            m.removePermissoesPosto(posto)
            inserePermissoesPostoOperacoes(posto)

            m.TerminaTransacao()
            MsgBox("Permissões configurados com sucesso", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            m.DesfazTransacao()
        End Try

    End Sub


    Public Sub inserePermissoesPostoOperacoes(posto As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In lstOperacao3.SelectedItems
            m.inserePermissoesOperacaoPosto(posto, drview("Codigo"))
        Next

    End Sub
    Public Sub inserePermissoesFuncionarioOperacoes(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In lstOperacao2.SelectedItems
            m.inserePermissoesOperacao(funcionario, drview("Codigo"))
        Next
    End Sub

    Public Sub inserePermissoesFuncionarioPostos(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In lstPostos2.SelectedItems
            m.inserePermissoesPosto(funcionario, drview("CDU_Posto"))
        Next
    End Sub

    Public Sub inserePermissoesFuncionarioFuncionarios(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In lstFuncionariosPerm.SelectedItems
            m.inserePermissoesFuncionario(funcionario, drview("Codigo"))
        Next
    End Sub


    Public Sub carregarForm()
        inicializarPecas()
        inicializarOperacoes()
        inicializarPostos()
        inicializaFuncionarios(txtFunc.Text)
        inicializarOperacoesFunc(txtFunc.Text)
        inicializarPostosFunc(txtFunc.Text)
        inicializarOperacoesPostos("")
    End Sub


    Private Sub inicializarPecas()
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT CDU_ObraN1 as Codigo ,CDU_ObraN1+' | '+ CDU_Descricao as Descricao FROM TDU_ObraN1")
        lstPecas.DataSource = dt
        lstPecas.ValueMember = "Codigo"
        lstPecas.DisplayMember = "Descricao"
        lstPecas.ClearSelected()
    End Sub

    Private Sub inicializarOperacoes()
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT CDU_Descricao as Codigo,CDU_ObraN2+' | '+ CDU_Descricao as Descricao FROM TDU_ObraN2")
        lstOperacao1.DataSource = dt
        lstOperacao1.ValueMember = "Codigo"
        lstOperacao1.DisplayMember = "Descricao"
        lstOperacao1.ClearSelected()
    End Sub

    Private Sub inicializarPostos()
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT *,CDU_Posto+' | '+ Nome+' | '+ Codigo as Display FROM APS_GP_Postos")
        lstPostos1.ValueMember = "CDU_Posto"
        lstPostos1.DisplayMember = "Display"
        lstPostos1.DataSource = dt
        lstPostos1.ClearSelected()


        dt = m.consultaDataTable("SELECT *, CDU_Posto+' | '+ Nome+' | '+ Codigo as Display FROM APS_GP_Postos")
        lstPostos3.ValueMember = "CDU_Posto"
        lstPostos3.DisplayMember = "Display"
        lstPostos3.DataSource = dt
        ' lstPostos1.ClearSelected()

    End Sub

    Private Sub inicializarOperacoesFunc(func As String)
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT *, Codigo+' | '+ Nome as Display FROM APS_GP_Operacoes")
        lstOperacao2.DataSource = dt
        lstOperacao2.ValueMember = "Codigo"
        lstOperacao2.DisplayMember = "Display"

        buscarOPeracoesFuncionario(func)

    End Sub

    Private Sub buscarOPeracoesFuncionario(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        lstOperacao2.ClearSelected()
        For i = 0 To lstOperacao2.Items.Count - 1
            drview = lstOperacao2.Items(i)
            If m.daPermissoesOperacaoesFuncionario(funcionario, drview("Codigo")) Then
                lstOperacao2.SetSelected(i, True)
            End If
        Next
    End Sub


    Private Sub inicializarPostosFunc(func As String)
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT *, Nome+' | '+ Codigo as Display FROM APS_GP_Postos")
        lstPostos2.ValueMember = "CDU_Posto"
        lstPostos2.DisplayMember = "Display"
        lstPostos2.DataSource = dt
        buscarPostosFuncionario(func)

    End Sub

    Private Sub buscarPostosFuncionario(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        lstPostos2.ClearSelected()
        For i = 0 To lstPostos2.Items.Count - 1
            drview = lstPostos2.Items(i)
            If m.daPermissoesPostosFuncionario(funcionario, drview("CDU_Posto")) Then
                lstPostos2.SetSelected(i, True)
            End If
        Next
    End Sub


    Private Sub inicializaFuncionarios(func As String)
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT Codigo,Nome FROM APS_GP_Funcionarios")
        lstFuncionariosPerm.ValueMember = "Codigo"
        lstFuncionariosPerm.DisplayMember = "Nome"
        lstFuncionariosPerm.DataSource = dt
        buscarFuncionariosFuncionario(func)

    End Sub

    Private Sub buscarFuncionariosFuncionario(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        lstFuncionariosPerm.ClearSelected()
        For i = 0 To lstFuncionariosPerm.Items.Count - 1
            drview = lstFuncionariosPerm.Items(i)
            If m.daPermissoesFuncionariosFuncionario(funcionario, drview("Codigo")) Or funcionario = drview("Codigo") Then
                lstFuncionariosPerm.SetSelected(i, True)
            End If
        Next
    End Sub


    Private Sub btnRemovePC_Click(sender As Object, e As EventArgs) Handles btnRemovePC.Click
        Dim m As Motor
        m = Motor.GetInstance
        If MsgBox("Deseja remover a operação seleccionada? ", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If m.removerPeca(getSeleccionadoPeca, getSeleccionadoPecaNome) Then
                inicializarPecas()
            End If
        End If
    End Sub


    Private Sub inicializarOperacoesPostos(posto As String)
        Dim m As Motor
        Dim dt As DataTable
        m = Motor.GetInstance
        dt = m.consultaDataTable("SELECT *,Codigo+' | '+ Nome as Display FROM APS_GP_Operacoes")
        lstOperacao3.DataSource = dt
        lstOperacao3.ValueMember = "Codigo"
        lstOperacao3.DisplayMember = "Display"

        buscarOPeracoesPostos(posto)
    End Sub

    Private Sub buscarOPeracoesPostos(posto As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        lstOperacao3.ClearSelected()
        For i = 0 To lstOperacao3.Items.Count - 1
            drview = lstOperacao3.Items(i)
            If m.daPermissoesOperacaoesPosto(posto, drview("Codigo")) Then
                lstOperacao3.SetSelected(i, True)
            End If
        Next

    End Sub
    Private Sub btnAddPC_Click(sender As Object, e As EventArgs) Handles btnAddPC.Click
        Dim f As FrmAddPecas
        f = New FrmAddPecas
        f.setObjecto(getSeleccionadoPeca())
        f.ShowDialog()
        inicializarPecas()
    End Sub


    Private Sub btnAddOP_Click(sender As Object, e As EventArgs) Handles btnAddOP.Click
        Dim f As FrmAddOPPostos
        f = New FrmAddOPPostos
        f.setObjecto(getSeleccionadoOperacao)
        f.Modo = "O"
        f.ShowDialog()
        inicializarOperacoes()
        inicializarOperacoesFunc(txtFunc.Text)

    End Sub
    Private Function getSeleccionadoOperacao()
        Dim drView As DataRowView
        drView = lstOperacao1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoOperacao = drView("Codigo")
    End Function

    Private Function getSeleccionadoPosto()
        Dim drView As DataRowView
        drView = lstPostos1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoPosto = drView("CDU_Posto")
    End Function

    Private Function getSeleccionadoPostoArtigo()
        Dim drView As DataRowView
        drView = lstPostos1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoPostoArtigo = drView("Codigo")
    End Function



    Private Function getSeleccionadoOperacaoNome()
        Dim drView As DataRowView
        drView = lstOperacao1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoOperacaoNome = drView("Descricao")
    End Function

    Private Function getSeleccionadPostoNome()
        Dim drView As DataRowView
        drView = lstPostos1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadPostoNome = drView("Nome")
    End Function

    Private Function getSeleccionadoPeca()
        Dim drView As DataRowView
        drView = lstPecas.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoPeca = drView("Codigo")
    End Function


    Private Function getSeleccionadoPecaNome()
        Dim drView As DataRowView
        drView = lstPecas.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoPecaNome = drView("Descricao")
    End Function

    Private Sub btnRemoveOP_Click(sender As Object, e As EventArgs) Handles btnRemoveOP.Click
        Dim m As Motor
        m = Motor.GetInstance
        If MsgBox("Deseja remover a operação seleccionada? " + vbCrLf + vbCrLf + "Todos os Postos de Trabalho associados serão eliminados.", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If m.removerOperacao(getSeleccionadoOperacao, getSeleccionadoOperacaoNome) Then
                inicializarOperacoes()
                inicializarOperacoesFunc(txtFunc.Text)
            End If
        End If
    End Sub


    Private Sub btnRemovePostos_Click(sender As Object, e As EventArgs) Handles btnRemovePostos.Click
        Dim m As Motor
        m = Motor.GetInstance
        If MsgBox("Deseja remover o posto seleccionado? " + vbCrLf + vbCrLf + "Todos os registos associados serão eliminados.", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If m.removerPosto(getSeleccionadoPostoArtigo, getSeleccionadoPosto, getSeleccionadPostoNome) Then
                inicializarPostos()
                inicializarPostosFunc(txtFunc.Text)
            End If
        End If
    End Sub

    Private Sub btnAddPostos_Click(sender As Object, e As EventArgs) Handles btnAddPostos.Click
        Dim f As FrmAddOPPostos
        f = New FrmAddOPPostos
        f.setObjecto(getSeleccionadoPosto)
        f.Modo = "P"
        f.ShowDialog()
        inicializarPostos()
        inicializarPostosFunc(txtFunc.Text)
    End Sub

    Private Sub btnFunc_Click(sender As Object, e As EventArgs) Handles btnFunc.Click
        Dim f As FrmLista
        f = New FrmLista
        f.setComando("SELECT Codigo, Nome, CDU_Password, isnull( CDU_AdministradorRP,0) as Adm,isnull( CDU_AdicionaPecas,0) as 'Adiciona Pcs'  from Funcionarios ")
        f.setCaption("Funcionários")
        f.SetOrdem(" ORDER BY Codigo")
        f.ShowFilter(True)
        f.ShowDialog()

        If f.seleccionouOK Then

            txtFunc.Text = f.getValor(0)
            txtNomeFunc.Text = f.getValor(1)
            txtPassword.Text = f.getValor(2)
            ckbAdmin.Checked = f.getValor(3)
            ckbAddPecas.Checked = f.getValor(4)

            inicializaFuncionarios(txtFunc.Text)
            inicializarOperacoesFunc(txtFunc.Text)
            inicializarPostosFunc(txtFunc.Text)
        End If
    End Sub

    Private Sub lstPostos3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstPostos3.SelectedIndexChanged
        inicializarOperacoesPostos(lstPostos3.SelectedValue)
    End Sub

    Private Sub lstPecas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstPecas.SelectedIndexChanged
        If lstPecas.SelectedIndices.Count > 0 Then
            btnAddPC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.edit16
        Else
            btnAddPC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        End If
    End Sub

    Private Sub lstOperacao1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstOperacao1.SelectedIndexChanged
        If lstOperacao1.SelectedIndices.Count > 0 Then
            btnAddOP.Image = Global.APS_GestaoProjectos.My.Resources.Resources.edit16
        Else
            btnAddOP.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        End If
    End Sub

    Private Sub lstPostos1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstPostos1.SelectedIndexChanged
        If lstPostos1.SelectedIndices.Count > 0 Then
            btnAddPostos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.edit16
        Else
            btnAddPostos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        End If
    End Sub
End Class


