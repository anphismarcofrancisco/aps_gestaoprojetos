﻿Imports PlatAPSNET
Imports Interop.GcpBE900
Imports Interop.StdBE900
Imports Interop.ErpBS900
Imports System.IO
Imports XtremeReportControl
Imports Interop.StdPlatBS900

Public Class Motor

    Shared myInstance As Motor
    Dim bso As ErpBS
    Dim PlataformaPrimavera As StdPlatBS
    Dim nomeEmpresa_ As String
    Dim codEmpresa As String
    Dim utilizador As String
    Dim password As String
    Dim tipoPlat As String
    Dim ficheiroAccess_ As String
    Dim file As String
    Dim log As TextBox
    Dim funcionario_ As String
    Dim nomefuncionario_ As String

    Dim funcionarioAdm_ As Boolean

    'Dim modoFuncionamento As String

    Dim empresas_ As String
    Dim apliInicializada As Boolean

    Dim ArmazemDefault As String

    Dim artigoML As String
    Dim objConfApl As StdBSConfApl

    Dim listaDocumentosCompra As String
    Dim listaDocumentosVenda As String
    Dim listaDocumentosStock As String

    Dim campoMolde As String
    Dim campoRequisicao As String
    Dim campoQuantidadeFormula As String
    Dim TEXTOFORMULA As String

    Dim apresentaEncomenda As Boolean
    Dim apresentaProjectos As Boolean
    Dim descontoLinhas As Boolean
    Dim campoArtigoPreco As String
    Dim validaMatricula As Boolean

    Dim campoMoeda As String

    Dim artInsp1 As String
    Dim artInsp2 As String

    '  Dim campoProjecto As String

    Dim viewCompras As String
    Dim viewVendas As String
    Dim viewArtigosFerramentas As String
    Dim viewStocks As String
    Dim documentoFE As String

    Dim numLicenciamentos As Integer
    Dim campos As String
    Dim tamanho As Long
    Dim modulos As String
    Dim licenca As String
    Dim artigoInvalido As String

    Dim validaStockMinimo As Boolean
    Dim documentosEncomenda As String
    Dim ficheiroIni As IniFile

    Private Sub New()
        Try

            '  AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf CurrentDomain_AssemblyResolve

            Dim ficheiroConfiguracao As Configuracao


            apliInicializada = False
            ficheiroConfiguracao = Configuracao.GetInstance
            ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)

            bso = New ErpBS
            PlataformaPrimavera = New StdPlatBS
            Dim seg As Seguranca
            seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

            utilizador = ficheiroIni.GetString("ERP", "UTILIZADOR", "")


            If ficheiroIni.GetString("ERP", "CRYPT", "") = "1" Then
                InputBox("Texto Incriptado", "Texto Incriptado", seg.encriptarStringTripleDES(InputBox("Insira o Texto", "Texto para Incriptar")))
            End If

            If ficheiroIni.GetString("ERP", "DECRYPT", "") = "1" Then
                InputBox("Texto Decriptado", "Texto Decriptado", seg.decifrarStringTripleDES(InputBox("Insira o Texto", "Texto para Decriptar")))
            End If

            '     utilizador = seg.encriptarStringTripleDES(utilizador)
            utilizador = seg.decifrarStringTripleDES(utilizador)

            password = ficheiroIni.GetString("ERP", "PASSWORD", "")
            ' password = seg.encriptarStringTripleDES(password)
            password = seg.decifrarStringTripleDES(password)

            codEmpresa = ficheiroIni.GetString("ERP", "EMPRESA", "")
            empresas_ = "'" + ficheiroIni.GetString("ERP", "EMPRESAS", codEmpresa) + "'"
            tipoPlat = ficheiroIni.GetString("ERP", "TIPOPLAT", 1)
            'modoFuncionamento = ficheiroIni.GetString("ERP", "MODO", "M")

            licenca = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "LICENCA", ""))

            tipoDocP_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCP", "")
            serieP_ = ficheiroIni.GetString("DOCUMENTO", "SERIEP", "")
            tipoEntidadeP_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADEP", "")

            tipoDocPS_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCPS", "")
            seriePS_ = ficheiroIni.GetString("DOCUMENTO", "SERIEPS", "")

            campoMolde = ficheiroIni.GetString("DOCUMENTO", "CAMPOMOLDE", "CDU_VMolde")
            campoRequisicao = ficheiroIni.GetString("DOCUMENTO", "CAMPOREQUISICAO", "CDU_VRequisicao")
            campoQuantidadeFormula = ficheiroIni.GetString("DOCUMENTO", "CAMPOFormula", "")
            TEXTOFORMULA = ficheiroIni.GetString("DOCUMENTO", "TEXTOFORMULA", "")

            listaDocumentosCompra = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSC", "")
            listaDocumentosVenda = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSV", "")
            listaDocumentosStock = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSS", "")

            tipoDocS_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCS", "")
            serieS_ = ficheiroIni.GetString("DOCUMENTO", "SERIES", "")
            tipoEntidadeS_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADES", "")

            tipoDoc_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOC", "")
            serieC_ = ficheiroIni.GetString("DOCUMENTO", "SERIEC", "")
            tipoEntidadeC_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADEC", "")


            serieV_ = ficheiroIni.GetString("DOCUMENTO", "SERIEV", "")
            tipoEntidadeV_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADEV", "")

            artigoML = ficheiroIni.GetString("DOCUMENTO", "ART_MAT_LIM", ficheiroIni.GetString("ERP", "ART_MAT_LIM", ""))

            ArmazemDefault = ficheiroIni.GetString("DOCUMENTO", "ARMAZEMDEFAULT", ficheiroIni.GetString("ERP", "ARMAZEMDEFAULT", "A1"))

            MODULO_STOCKS_ = ficheiroIni.GetString("ERP", "MODULO", "S")

            modulos = ficheiroIni.GetString("ERP", "MODULOS", MODULO_STOCKS_)


            campoArtigoPreco = ficheiroIni.GetString("DOCUMENTO", "CAMPO_ART_PRECO", ficheiroIni.GetString("ERP", "CAMPO_ART_PRECO", ""))

            campoMoeda = ficheiroIni.GetString("ERP", "MOEDA", "EUR")

            apresentaEncomenda = ficheiroIni.GetBoolean("DOCUMENTO", "APRESENTAENCOMENDA", True)

            artInsp1 = ficheiroIni.GetString("DOCUMENTO", "ARTINSP1", "")
            artInsp2 = ficheiroIni.GetString("DOCUMENTO", "ARTINSP2", "")

            apresentaProjectos = ficheiroIni.GetString("DOCUMENTO", "APRESENTAPROJECTOS", "") <> "0"

            descontoLinhas = ficheiroIni.GetString("DOCUMENTO", "DESCONTOLINHAS", "0") <> "0"

            campos = ficheiroIni.GetString("DOCUMENTO", "CAMPOS", "")

            tamanho = ficheiroIni.GetInteger("DOCUMENTO", "TAMANHOGRELHA", 540)


            viewCompras = ficheiroIni.GetString("DOCUMENTO", "VIEWCOMPRAS", "APS_GP_Artigos")
            viewVendas = ficheiroIni.GetString("DOCUMENTO", "VIEWVENDAS", "APS_GP_Artigos")

            viewArtigosFerramentas = ficheiroIni.GetString("DOCUMENTO", "VIEWARTIGOSFERRAMENTAS", "")
            viewStocks = ficheiroIni.GetString("DOCUMENTO", "VIEWSTOCKS", "APS_GP_Artigos")


            documentoFE = ficheiroIni.GetString("DOCUMENTO", "TIPODOCSFE", "")

            validaMatricula = ficheiroIni.GetInteger("DOCUMENTO", "VALIDAMATRICULA", 1) = 1


            artigoInvalido = ficheiroIni.GetString("DOCUMENTO", "ARTIGOINVALIDO", "*")

            validaStockMinimo = ficheiroIni.GetString("DOCUMENTO", "VALIDASTOCKMINIMO", "0") <> "0"
            documentosEncomenda = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSENCOMENDA", "''")


            verificarLicenciamento(ficheiroIni, seg)

            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = utilizador
            objConfApl.PwdUtilizador = password
            objConfApl.LicVersaoMinima = "9.00"


            bso.AbreEmpresaTrabalho(tipoPlat, codEmpresa, utilizador, password, , objConfApl.Instancia)


            'If bso.Comercial.Artigos.Existe("TESTE") Then

            'End If
            PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")


            inicializarAplicacao(codEmpresa)

            'PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "primavera")
            apliInicializada = True
        Catch ex As Exception
            MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub inicializarAplicacao(codEmpresa)
        Dim mAPl As MotorAPL
        mAPl = New MotorAPL(Me)
        mAPl.inicializarAplicacao(codEmpresa)
    End Sub


    Private Sub verificarLicenciamento(ficheiroIni As IniFile, seg As Seguranca)
        Dim StrnumLicenciamentos As String
        StrnumLicenciamentos = ficheiroIni.GetString("ERP", "NUMPOSTOS", "")

        '   MsgBox(seg.decifrarStringTripleDES("rOsbQaIyAGY="))
        If StrnumLicenciamentos = "-1" Then
            StrnumLicenciamentos = "999999"
        Else
            If StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos) Then
                StrnumLicenciamentos = "0"
            Else
                StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos)
            End If
        End If

        If IsNumeric(StrnumLicenciamentos) Then
            numLicenciamentos = CInt(StrnumLicenciamentos)
        Else
            numLicenciamentos = 0
        End If
    End Sub

    Public Function daViewModulo(modulo As String)
        If modulo = "C" Then Return viewCompras
        If modulo = "V" Then Return viewVendas
    End Function

    ''' <summary>
    ''' Funcao que permite retornar a empresa selecionada
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getArmazemDefault() As String
        Return ArmazemDefault
    End Function

    ''' <summary>
    ''' Funcao que permite o modulo do documento a criar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getMOLDULOP() As String
        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        Return ficheiroIni.GetString("DOCUMENTO", "MOLDULOP", "I")
    End Function

    ''' <summary>
    ''' Funcao que permite o verificar se grava o documento ao terminar um ponto
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getGRAVAFINALIZAR() As Boolean
        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        Return ficheiroIni.GetBoolean("DOCUMENTO", "GRAVAFINALIZAR", True)
    End Function

    ''' <summary>
    ''' Funcao que permite retornar a empresa selecionada
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getEmpresa() As String
        Return codEmpresa
    End Function

    ''' <summary>
    ''' Funcao que permite indicar se apresenta o botão de Encomendas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getApresentaEncomendas() As Boolean
        Return apresentaEncomenda
    End Function

    ''' <summary>
    ''' Funcao que permite indicar apresenta a lista de Projectos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getApresentaProjectos() As Boolean
        Return apresentaProjectos
    End Function

    ''' <summary>
    ''' Funcao que permite indicar apresenta os artigos Stock Minimo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getApresentaArtigosStockMinimo() As Boolean
        Return validaStockMinimo
    End Function



    ''' <summary>
    ''' Funcao que permite os artigos Encomenda
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getDocumentosEncomenda() As String
        Return documentosEncomenda
    End Function

    ''' <summary>
    ''' retorna a vista dos artigos de Stocks
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daViewStocks()
        Return viewStocks
    End Function
    Public Sub setEmpresa(ByVal empresa_ As String, Optional forcaAberturaEmpresa As Boolean = False)
        If apliInicializada Then
            Try
                bso.FechaEmpresaTrabalho()
                bso.AbreEmpresaTrabalho(tipoPlat, empresa_, utilizador, password)
                codEmpresa = empresa_
            Catch ex As Exception
                MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            End Try
        End If

    End Sub
    ''' <summary>
    ''' Função que permite verificar que modulo é necessário inicializar
    ''' </summary>
    ''' <remarks></remarks>
    Dim MODULO_STOCKS_ As String
    Property MODULO() As String
        Get
            MODULO = MODULO_STOCKS_
        End Get
        Set(value As String)
            MODULO_STOCKS_ = value
        End Set
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_MAOOBRA() As String
        Get
            MATERIAL_MAOOBRA = "MO"
        End Get
    End Property

    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_LIMPEZA() As String
        Get
            MATERIAL_LIMPEZA = "ML"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_RTJ() As String
        Get
            MATERIAL_RTJ = "MR"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_CLIENTE() As String
        Get
            MATERIAL_CLIENTE = "MC"
        End Get
    End Property

    ''' <summary>
    ''' Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Funcionario() As String
        Get
            Funcionario = funcionario_
        End Get
        Set(ByVal value As String)
            funcionario_ = value
        End Set
    End Property

    ''' <summary>
    ''' Nome Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property NomeFuncionario() As String
        Get
            NomeFuncionario = nomefuncionario_
        End Get
        Set(ByVal value As String)
            nomefuncionario_ = value
        End Set
    End Property


    ''' <summary>
    ''' Permite validar se um funcionario é administrador
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property FuncionarioAdm() As Boolean
        Get
            FuncionarioAdm = funcionarioAdm_
        End Get
        Set(ByVal value As Boolean)
            funcionarioAdm_ = value
        End Set
    End Property


    Dim serieV_ As String = ""
    Property SerieV() As String
        Get
            SerieV = serieV_
        End Get
        Set(ByVal value As String)
            serieV_ = value
        End Set
    End Property


    Dim tipoEntidadeV_ As String = "C"
    Property TipoEntidadeV() As String
        Get
            TipoEntidadeV = tipoEntidadeV_
        End Get
        Set(ByVal value As String)
            tipoEntidadeV_ = value
        End Set
    End Property

    Dim tipoDoc_ As String = ""
    Property TipoDoc() As String
        Get
            TipoDoc = tipoDoc_
        End Get
        Set(ByVal value As String)
            tipoDoc_ = value
        End Set
    End Property


    Dim serieC_ As String = ""
    Property SerieC() As String
        Get
            SerieC = serieC_
        End Get
        Set(ByVal value As String)
            serieC_ = value
        End Set
    End Property


    Dim tipoEntidadeC_ As String = "C"
    Property TipoEntidadeC() As String
        Get
            TipoEntidadeC = tipoEntidadeC_
        End Get
        Set(ByVal value As String)
            tipoEntidadeC_ = value
        End Set
    End Property


    Dim tipoDocS_ As String = ""
    Property TipoDocS() As String
        Get
            TipoDocS = tipoDocS_
        End Get
        Set(ByVal value As String)
            tipoDocS_ = value
        End Set
    End Property

    Dim serieS_ As String = ""
    Property SerieS() As String
        Get
            SerieS = serieS_
        End Get
        Set(ByVal value As String)
            serieS_ = value
        End Set
    End Property


    Dim tipoEntidadeS_ As String = "U"
    Property TipoEntidadeS() As String
        Get
            TipoEntidadeS = tipoEntidadeS_
        End Get
        Set(ByVal value As String)
            tipoEntidadeS_ = value
        End Set
    End Property


    Dim tipoDocP_ As String = ""
    Property TipoDocP() As String
        Get
            TipoDocP = tipoDocP_
        End Get
        Set(ByVal value As String)
            tipoDocP_ = value
        End Set
    End Property

    Dim serieP_ As String = ""
    Property SerieP() As String
        Get
            SerieP = serieP_
        End Get
        Set(ByVal value As String)
            serieP_ = value
        End Set
    End Property

    Dim tipoDocPS_ As String = ""
    Property TipoDocPS() As String
        Get
            TipoDocPS = tipoDocPS_
        End Get
        Set(ByVal value As String)
            tipoDocPS_ = value
        End Set
    End Property

    Dim seriePS_ As String = ""
    Property SeriePS() As String
        Get
            SeriePS = seriePS_
        End Get
        Set(ByVal value As String)
            seriePS_ = value
        End Set
    End Property


    Dim tipoEntidadeP_ As String = "U"
    Property TipoEntidadeP() As String
        Get
            TipoEntidadeP = tipoEntidadeP_
        End Get
        Set(ByVal value As String)
            tipoEntidadeP_ = value
        End Set
    End Property


    ReadOnly Property DocumentoCompra() As String
        Get
            DocumentoCompra = tipoDoc_
        End Get
    End Property

    ReadOnly Property DocumentoStock() As String
        Get
            DocumentoStock = tipoDocS_
        End Get
    End Property
    ReadOnly Property Empresa() As String
        Get
            Empresa = codEmpresa
        End Get

    End Property

    ReadOnly Property EmpresaDescricao() As String
        Get
            EmpresaDescricao = Trim(bso.Contexto.IDNome)
        End Get

    End Property

    ReadOnly Property getCampoMolde() As String
        Get
            getCampoMolde = campoMolde
        End Get
    End Property

    ReadOnly Property getCampoFormulaNumero() As String
        Get
            getCampoFormulaNumero = campoQuantidadeFormula
        End Get
    End Property

    ReadOnly Property getTextoCampoFormula() As String
        Get
            getTextoCampoFormula = TEXTOFORMULA
        End Get
    End Property



    ReadOnly Property getValidaMatricula() As Boolean
        Get
            getValidaMatricula = validaMatricula
        End Get
    End Property



    ReadOnly Property EmpresaAberta() As Boolean
        Get
            EmpresaAberta = bso.Contexto.EmpresaAberta
        End Get
    End Property

    ReadOnly Property getArtigoInsp1() As String
        Get
            getArtigoInsp1 = artInsp1
        End Get
    End Property

    ReadOnly Property getArtigoInsp2() As String
        Get
            getArtigoInsp2 = artInsp2
        End Get
    End Property

    ReadOnly Property getDescricaoArtigo(artigo As String) As String
        Get
            getDescricaoArtigo = bso.Comercial.Artigos.DaValorAtributo(artigo, "Descricao")
        End Get
    End Property



    Property AplicacaoInicializada() As Boolean
        Get
            AplicacaoInicializada = apliInicializada
        End Get
        Set(ByVal value As Boolean)
            apliInicializada = value
        End Set
    End Property



    Public Sub setLog(ByVal log_ As TextBox)
        log = log_
    End Sub

    Public Sub fechaEmpresa()
        bso.FechaEmpresaTrabalho()
        PlataformaPrimavera.FechaPlataforma()
    End Sub

    Public Shared Function GetInstance() As Motor
        If myInstance Is Nothing Then
            myInstance = New Motor
        End If
        Return myInstance
    End Function

    Private Function buscarIva(ByVal taxa As Double) As String
        Return consultaValor("SELECT Iva FROM iva WHERE Taxa=" + taxa.ToString())
    End Function

    Private Function buscarTaxaIva(ByVal codIva As String) As String
        Return consultaValor("SELECT Taxa FROM iva WHERE iva=" + codIva)
    End Function

    ''' <summary>
    ''' Função que permite a execução de um comando 
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function executarComando(ByVal comando As String, Optional mostraMsg As Boolean = True)
        Try
            bso.DSO.Plat.ExecSql.ExecutaXML(comando)
        Catch ex As Exception
            If mostraMsg Then MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

        Return ""
    End Function

    Public Function consulta(ByVal query As String) As StdBELista
        Try
            Return bso.Consulta(query)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return Nothing
        End Try

    End Function

    Public Function consultaDataTable(ByVal query As String) As DataTable
        Try
            Return convertRecordSetToDataTable(bso.Consulta(query).DataSet)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return Nothing
        End Try

    End Function


    Public Function consultaValor(ByVal query As String) As String
        Dim lista As StdBELista

        Try

            lista = bso.Consulta(query)
            If lista.NoFim Then
                Return ""
            Else
                Return lista.Valor(0).ToString
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try

    End Function


    ''' <summary>
    ''' Função que permite validar a password do respectivo funcionario
    ''' </summary>
    ''' <param name="funcionario"></param>
    ''' <param name="password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function validaPassword(ByVal funcionario As String, ByVal password As String) As Boolean
        Try
            Dim numLinhas As String
            numLinhas = consultaValor("SELECT  COUNT(*) FROM APS_GP_Funcionarios WHERE Codigo='" + funcionario + "' and CDU_Password='" + password + "'")
            Return numLinhas = "1"
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Função que permite ir buscar o posto local
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daPosto() As String
        Dim ficheiroConfiguracao As Configuracao
        Dim ficheiroIni As IniFile

        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        daPosto = ficheiroIni.GetString("ERP", "POSTO", "")
        ficheiroIni = Nothing
    End Function


    Public Function convertRecordSetToDataTable(ByVal MyRs As ADODB.Recordset) As DataTable
        'Create and fill the dataset from the recordset and populate grid from Dataset. 
        Dim myDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter()
        Dim myDS As DataSet = New DataSet("MyTable")
        myDA.Fill(myDS, MyRs, "MyTable")
        Return myDS.Tables("MyTable")
    End Function


    Public Function firstDayMonth1(ByVal data As Date) As Date
        Dim firstDay As DateTime
        firstDay = CDate("01-" + CStr(Month(data)) + "-" + CStr(Year(data)))
        Return firstDay
    End Function

    Public Function lastDayMonth1(ByVal data As Date) As Date
        Dim lastDay As Date
        lastDay = CDate(data)
        lastDay = DateAdd(DateInterval.Month, 1, lastDay)
        lastDay = Convert.ToDateTime("1-" & Month(lastDay).ToString() & "-" & Year(lastDay).ToString())
        lastDay = DateAdd(DateInterval.Day, -1, lastDay)
        Return lastDay
    End Function



    Public Function DayStart(ByVal data As Date) As Date
        'Dim firstDay As DateTime
        'firstDay = CDate("01-" + CStr(Month(data)) + "-" + CStr(Year(data)))
        'Return firstDay


        Return CDate(data.AddMonths(-1).ToShortDateString)
    End Function

    Public Function DayEnd(ByVal data As Date) As Date

        Return CDate(data.ToShortDateString)
        'Dim lastDay As Date
        'lastDay = CDate(data)
        'lastDay = DateAdd(DateInterval.Month, 1, lastDay)
        'lastDay = Convert.ToDateTime("1-" & Month(lastDay).ToString() & "-" & Year(lastDay).ToString())
        'lastDay = DateAdd(DateInterval.Day, -1, lastDay)
        'Return lastDay
    End Function


    Public Function daListaRegistos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim restricaoCT As String
        restricaoCT = ""
        If centroTrabalho <> "Todos" Then
            restricaoCT = "AND TDU_RegistoFolhaPonto.CDU_Posto='" + centroTrabalho + "'"
        End If
        If func = "0" Then
            ' InputBox("", "", "Select LinhasInternos.Estado, Funcionarios.Codigo as Codigo, Funcionarios.Nome as Funcionario, TDU_RegistoFolhaPonto.CDU_Posto as [Centro Trabalho],CDU_Molde as Molde,CDU_Peca as Peça,CDU_Operacao as Operação,  CDU_DataFinal as Data, CDU_Tempo as Tempo,TDU_RegistoFolhaPonto.CDU_EstadoTrabalho as EstadoTrabalho, TDU_RegistoFolhaPonto.CDU_Observacoes as Observacoes,CDU_Id, CDU_IdLinha FROM TDU_RegistoFolhaPonto INNER JOIN Funcionarios ON TDU_RegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN LinhasInternos ON TDU_RegistoFolhaPonto.CDU_IdLinha = LinhasInternos.Id WHERE CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT + " order by CDU_DataInicial")
            lista = consulta("Select LinhasInternos.Estado, Funcionarios.Codigo as Codigo, Funcionarios.Nome as Funcionario, TDU_RegistoFolhaPonto.CDU_Posto as [Centro Trabalho],CDU_Molde as Molde,CDU_Peca as Peça,CDU_Operacao as Operação,  CDU_DataFinal as Data, CDU_Tempo as Tempo,TDU_RegistoFolhaPonto.CDU_EstadoTrabalho as EstadoTrabalho, TDU_RegistoFolhaPonto.CDU_Observacoes as Observacoes,CDU_Id, CDU_IdLinha FROM TDU_RegistoFolhaPonto INNER JOIN Funcionarios ON TDU_RegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN LinhasInternos ON TDU_RegistoFolhaPonto.CDU_IdLinha = LinhasInternos.Id WHERE CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT + " order by CDU_DataInicial")
        Else
            lista = consulta("Select LinhasInternos.Estado, Funcionarios.Codigo as Codigo,Funcionarios.Nome as Funcionario, TDU_RegistoFolhaPonto.CDU_Posto as [Centro Trabalho],CDU_Molde as Molde,CDU_Peca as Peça,CDU_Operacao as Operação,  CDU_DataFinal as Data, CDU_Tempo as Tempo,TDU_RegistoFolhaPonto.CDU_EstadoTrabalho as EstadoTrabalho, TDU_RegistoFolhaPonto.CDU_Observacoes as Observacoes,CDU_Id, CDU_IdLinha FROM TDU_RegistoFolhaPonto INNER JOIN Funcionarios ON TDU_RegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN LinhasInternos ON TDU_RegistoFolhaPonto.CDU_IdLinha = LinhasInternos.Id WHERE  CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario='" + func + "' " + restricaoCT + " order by CDU_DataInicial")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daListaRegistosStocks(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String, Optional documento As String = "") As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim tipodoc As String
        Dim TipoDocStk As String
        Dim movimento As String
        If documento = "" Then
            If modulo = "S" Then
                tipodoc = tipoDocS_
            Else
                tipodoc = tipoDocP_
            End If
        Else
            tipodoc = documento
        End If

        TipoDocStk = entradaSaida(tipodoc)
        If TipoDocStk = "S" Then
            movimento = "-1"
        Else
            movimento = "1"
        End If

        If func = "0" Then
            lista = consulta("SELECT CB.ID, CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.Data,LI.Artigo,LI.Descricao," + movimento + "*LI.Quantidade as Quantidade,LI.Estado,COP_Obras.Codigo as Projecto,LI.Armazem,Armazens.Descricao as ArmDesc,LI.* FROM LinhasInternos LI INNER JOIN CabecInternos CB ON LI.IdCabecInternos = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID LEFT OUTER JOIN Armazens ON LI.Armazem = Armazens.Armazem WHERE CB.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipodoc + "' and CB.Entidade IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CB.Data")
        Else
            lista = consulta("SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.Data,LI.Artigo,LI.Descricao," + movimento + "*LI.Quantidade as Quantidade,LI.Estado,COP_Obras.Codigo as Projecto,,LI.Armazem,Armazens.Descricao as ArmDesc,LI.* FROM LinhasInternos LI INNER JOIN CabecInternos CB ON LI.IdCabecInternos = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID LEFT OUTER JOIN Armazens ON LI.Armazem = Armazens.Armazem WHERE  CB.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.Entidade='" + func + "' and CB.TipoDoc='" + tipodoc + "' ORDER BY CB.Data")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Private Function entradaSaida(documento) As String
        Return bso.Comercial.TabInternos.DaValorAtributo(documento, "TipoDocStk")
    End Function

    Public Function daListaRegistosDocumentoVenda(ByVal tipodoc As String, ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByRef query As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        'If func = "0" Then
        '    lista = consulta("SELECT CB.ID, CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id  INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID   WHERE CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipoDocC_ + "' and CB.Utilizador IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT " + IIf(funcionario_ = "", "NULL", "'" + funcionario_ + "'") + ")  ORDER BY CB.DataDoc")
        'Else
        '    lista = consulta("SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.Utilizador='" + func + "' and CB.TipoDoc='" + tipoDocC_ + "' ORDER BY CB.DataDoc")
        'End If

        query = "SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.Data as DataDoc,LI.Artigo,LI.Descricao,LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasDoc LI INNER JOIN CabecDoc CB ON LI.IdCabecDoc = CB.Id INNER JOIN CabecDocStatus CS ON CS.IdCabecDoc = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:00',105) AND CB.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipodoc + "'"
        lista = consulta(query)

        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daListaRegistosDocumentoCompra(ByVal tipodoc As String, ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByRef query As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        'If func = "0" Then
        '    lista = consulta("SELECT CB.ID, CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id  INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID   WHERE CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipoDocC_ + "' and CB.Utilizador IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT " + IIf(funcionario_ = "", "NULL", "'" + funcionario_ + "'") + ")  ORDER BY CB.DataDoc")
        'Else
        '    lista = consulta("SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.Utilizador='" + func + "' and CB.TipoDoc='" + tipoDocC_ + "' ORDER BY CB.DataDoc")
        'End If

        query = "SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:00',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipodoc + "'"
        lista = consulta(query)

        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function


    Public Function daListaRegistosPontos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        If getMOLDULOP() = "I" Then
            If func = "0" Then
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecInternos.Id as IdCabecInternos,CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc, CabecInternos.Serie,TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao,TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao  FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                ' InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario AS Utilizador IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            Else
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome,CabecInternos.Id as IdCabecInternos, CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc,CabecInternos.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                '   InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc,CabecInternos.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            End If
        Else

            If func = "0" Then
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecDoc.Id as IdCabecInternos,CabecDoc.TipoDoc, CabecDoc.NumDoc, CabecDoc.Serie,TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao,TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDocStatus.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao  FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id LEFT OUTER  JOIN CabecdocStatus  ON dbo.CabecDocStatus.IdCabecDoc = dbo.CabecDoc.ID WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                ' InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecDoc.Id as IdCabecInternos,CabecDoc.TipoDoc, CabecDoc.NumDoc, CabecDoc.Serie,TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao,TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDocStatus.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde  FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id INNER JOIN CabecdocStatus  ON dbo.CabecDocStatus.IdCabecDoc = dbo.CabecDoc.ID WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            Else
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome,CabecDoc.Id as IdCabecInternos, CabecDoc.TipoDoc, CabecDoc.NumDoc,CabecDoc.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDocStatus.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id LEFT OUTER JOIN CabecdocStatus  ON dbo.CabecDocStatus.IdCabecDoc = dbo.CabecDoc.ID  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                '   InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome,CabecDoc.Id as IdCabecInternos, CabecDoc.TipoDoc, CabecDoc.NumDoc,CabecDoc.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDoc.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            End If
        End If


        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="func"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As Double

        Dim restricaoCT As String
        restricaoCT = ""
        If centroTrabalho <> "Todos" Then
            restricaoCT = "AND TDU_RegistoFolhaPonto.CDU_Posto='" + centroTrabalho + "'"
        End If

        If func = "0" Then
            daTotalRegistos = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,Funcionarios WHERE TDU_RegistoFolhaPonto.CDU_Funcionario=Funcionarios.Codigo AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT)
        Else
            daTotalRegistos = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,Funcionarios WHERE TDU_RegistoFolhaPonto.CDU_Funcionario=Funcionarios.Codigo AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario ='" + func + "' " + restricaoCT)
        End If
    End Function



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="func"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double

        Dim tipodoc As String
        Dim campo As String
        If modulo = "S" Then
            campo = "CabecInternos.Entidade"
            tipodoc = tipoDocS_
        Else
            tipodoc = tipoDocP_
            campo = "CabecInternos.Utilizador"
        End If

        If func = "0" Then
            daTotalRegistos = consultaValor("Select ISNULL(SUM(-Quantidade),0) as Tempo  FROM CabecInternos,LinhasInternos WHERE CabecInternos.TipoDoc='" + tipodoc + "' and CabecInternos.Id=LinhasInternos.IdCabecInternos AND CabecInternos.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecInternos.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and " + campo + " IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')")
        Else
            daTotalRegistos = consultaValor("Select ISNULL(SUM(-Quantidade),0) as Tempo  FROM CabecInternos,LinhasInternos WHERE CabecInternos.TipoDoc='" + tipodoc + "' and CabecInternos.Id=LinhasInternos.IdCabecInternos AND CabecInternos.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecInternos.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and " + campo + " ='" + func + "' ")
        End If
    End Function

    Public Function daTotalRegistosDocumentoCompra(ByVal tipodoc As String, ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double

        '   Dim tipodoc As String
        ' tipodoc = tipoDocC_
        Return consultaValor("Select ISNULL(SUM(Quantidade),0) as Tempo  FROM CabecCompras,LinhasCompras WHERE CabecCompras.TipoDoc='" + tipodoc + "' and  CabecCompras.Id=LinhasCompras.IdCabecCompras AND CabecCompras.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecCompras.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105)")

    End Function

    Public Function daTotalRegistosDocumentoVenda(ByVal tipodoc As String, ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double

        Return consultaValor("Select ISNULL(SUM(Quantidade),0) as Tempo  FROM CabecDoc,LinhasDoc WHERE CabecDoc.TipoDoc='" + tipodoc + "' and  CabecDoc.Id=LinhasDoc.IdCabecDoc AND CabecDoc.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecDoc.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105)")

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="centroTrabalho"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <param name="modulo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistosCT(ByVal centroTrabalho As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double
        Dim tipodoc As String

        If modulo = "S" Then
            tipodoc = tipoDocS_
        Else
            tipodoc = tipoDocP_
        End If
        If centroTrabalho = "0" Then
            daTotalRegistosCT = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,TDU_Postos WHERE TDU_RegistoFolhaPonto.CDU_Posto=TDU_Postos.CDU_Descricao AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')")
        Else
            daTotalRegistosCT = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,TDU_Postos WHERE TDU_RegistoFolhaPonto.CDU_Posto=TDU_Postos.CDU_Descricao AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and TDU_RegistoFolhaPonto.CDU_Posto ='" + centroTrabalho + "'")
        End If
    End Function

    Public Function daListaFuncionariosPermissoes() As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = consulta("SELECT '0' as Codigo,'Todos' as Nome UNION  SELECT Codigo,Nome FROM Funcionarios WHERE Codigo='" + funcionario_ + "' UNION SELECT cdu_utilizadorPermissao as Codigo,Funcionarios.Nome FROM [TDU_UTILIZADORESPERMISSOES],Funcionarios where TDU_UTILIZADORESPERMISSOES.cdu_utilizadorPermissao=Funcionarios.Codigo and CDU_Utilizador='" + funcionario_ + "'")
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todos os postos ao qual o funcionario tem acesso
    ''' </summary>
    ''' <param name="funcionario"></param>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaPostos(ByVal funcionario As String, ByVal restricao As String, Optional ByVal colocaTodos As Boolean = False) As StdBELista
        Try
            Dim lista As StdBELista
            Dim sqlTodos As String
            Dim ordenacao As String
            sqlTodos = ""
            ordenacao = ""
            If colocaTodos Then
                sqlTodos = "SELECT '0' as Codigo,'Todos' as Nome,'0' as Ordenacao  UNION "
            End If
            ordenacao = "ORDER BY Ordenacao"

            If restricao <> "" Then
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') and Nome LIKE '%" + restricao + "%' " + ordenacao)
            Else
                ' InputBox("", "", sqlTodos + "SELECT Codigo,Nome FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') ORDER BY Nome")
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') " + ordenacao)
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todos os funcionarios ou os funcionarios de um determinado posto
    ''' </summary>
    ''' <param name="postoTrabalho"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaFuncionarios(ByVal restricao As String, Optional ByVal postoTrabalho As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                restricao = " AND Nome LIKE '%" + restricao + "%'"
            End If

            If postoTrabalho <> "" Then
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_Funcionarios WHERE CDU_Posto='" + postoTrabalho + "' " + restricao + "ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_Funcionarios Where 1=1 " + restricao + " ORDER BY Nome")
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaCentrosTrabalho(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_CentrosTrabalho WHERE Nome LIKE '" + restricao + "%' ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_CentrosTrabalho Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaMoldes(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If apresentaProjectos Then
                If restricao <> "" Then
                    lista = consulta("SELECT Codigo,Nome FROM APS_GP_Moldes WHERE Nome LIKE '%" + restricao + "%'")
                Else
                    lista = consulta("SELECT Codigo,Nome FROM APS_GP_Moldes")
                End If
            Else
                If restricao <> "" Then
                    lista = consulta("SELECT Armazem as Codigo, Descricao as Nome FROM Armazens WHERE Descricao LIKE '%" + restricao + "%' ORDER BY Descricao")
                Else
                    lista = consulta("SELECT Armazem Codigo,Descricao as Nome FROM Armazens Order By Descricao")
                End If
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Funcao que permite ir buscar a Lista de Armazens
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaArmazens(Optional artigo As String = "", Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista
            Dim rest As String = ""

            If artigo = "" Then
                If restricao <> "" Then
                    lista = consulta("SELECT Armazem as Codigo, Descricao as Nome FROM  Armazens WHERE Descricao LIKE '" + restricao + "%' ORDER BY Descricao")
                Else
                    lista = consulta("SELECT Armazem Codigo,Descricao as Nome FROM Armazens Order By Descricao")
                End If
            Else
                If restricao <> "" Then
                    lista = consulta("SELECT distinct Armazens.Armazem as Codigo,Armazens.Descricao as Nome FROM ArtigoArmazem,Armazens WHERE ArtigoArmazem.StkActual<>0 AND ArtigoArmazem.Armazem=Armazens.Armazem and Artigo='" + artigo + "' and Descricao LIKE '" + restricao + "%' ORDER BY Descricao")
                Else
                    lista = consulta("SELECT distinct Armazens.Armazem Codigo,Armazens.Descricao as Nome FROM ArtigoArmazem, Armazens WHERE ArtigoArmazem.StkActual<>0 AND  ArtigoArmazem.Armazem=Armazens.Armazem and Artigo='" + artigo + "' Order By Descricao")
                End If

            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaPecas(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT * FROM APS_GP_Pecas WHERE Nome LIKE '" + restricao + "%' Order by Nome")
            Else
                lista = consulta("SELECT * FROM APS_GP_Pecas Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaOperacoes(ByVal funcionario As String, Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            'If restricao <> "" Then
            '    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "') and Nome LIKE '%" + restricao + "%' Order By Nome")
            'Else
            '    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "') Order By Nome")
            'End If

            If restricao <> "" Then
                lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Nome LIKE '" + restricao + "%'")
            Else
                lista = consulta("SELECT * FROM APS_GP_Operacoes")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaProblemas(ByVal funcionario As String, Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista
            If restricao <> "" Then
                lista = consulta("SELECT * FROM APS_GP_Problemas WHERE Nome LIKE '%" + restricao + "%' Order By Nome")
            Else
                lista = consulta("SELECT * FROM APS_GP_Problemas Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    '''  Ir buscar a lista de estados de trabalho
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaEstadosTrabalho() As StdBELista
        Try
            Dim lista As StdBELista
            lista = consulta("SELECT CDU_Estado as Codigo,CDU_Descricao as Descricao FROM TDU_EstadosTrabalho Order By CDU_Descricao")
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    '''  Ir buscar a lista de empresas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaEmpresas(empresas As String) As StdBELista
        Try
            Dim lista As StdBELista
            empresas = daCodigosEmpresas(empresas)
            If empresas <> "" Then
                '     InputBox("", "", "SELECT Codigo, IDNome as Nome FROM PRIEMPRE..EMPRESAS WHERE Codigo IN ('" + empresas_ + "')")
                lista = consulta("SELECT Codigo, IDNome as Nome FROM PRIEMPRE..EMPRESAS WHERE Codigo IN (" + empresas + ")")
            Else
                lista = Nothing
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Private Function daCodigosEmpresas(empresas As String) As String
        Dim strempresas() As String
        Dim codEmpresas As String
        codEmpresas = ""
        strempresas = empresas_.Split(",")


        For i = 0 To strempresas.Length - 1
            If InStr(empresas, strempresas(i)) > 0 Then
                If codEmpresas <> "" Then
                    codEmpresas = codEmpresas + ","
                End If
                codEmpresas = codEmpresas + strempresas(i)
            End If
        Next
        Return codEmpresas
    End Function

    ''' <summary>
    ''' Função que permite ir buscar os documentos de compra que é necessário criar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaDocumentos(Optional documentosInterno As Boolean = False) As StdBELista
        Try
            Dim lista As StdBELista
            If documentosInterno = False Then
                lista = consulta("SELECT 'C' as Modulo, Documento as Codigo, Descricao as Nome FROM DocumentosCompra WHERE Documento IN ('" + listaDocumentosCompra + "') UNION SELECT 'V' as Modulo,Documento as Codigo, Descricao as Nome FROM DocumentosVenda WHERE Documento IN ('" + listaDocumentosVenda + "')")
            Else
                lista = consulta("SELECT Documento as Codigo, Descricao as Nome FROM DocumentosInternos WHERE Documento IN ('" + listaDocumentosStock + "')")
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaClientes(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT Codigo,Nome,CDU_Armazem FROM APS_GP_Clientes WHERE Nome LIKE '" + restricao + "%' ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome,CDU_Armazem FROM APS_GP_Clientes Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub removerRegisto(ByVal registo As Registo)
        Dim comando As String
        Dim idCabecDoc As String
        Dim idCabecStk As String
        If registo.ID = "" Then
            registo.ID = consultaValor("SELECT CDU_ID FROM  TDU_APSCabecRegistoFolhaPonto WHERE CDU_Funcionario='" + registo.Funcionario + "' AND CDU_Molde='" + registo.Molde + "' AND CDu_TipoEntidade='" + registo.TipoEntidade + "' AND CDU_Entidade='" + registo.Entidade + "' AND CDU_Operacao='" + registo.Operacao + "' AND CDU_Posto='" + registo.Posto + "' and CDU_Idcabec IS NULL")
        End If

        Try
            If registo.ID <> "" Then
                bso.IniciaTransaccao()
                idCabecDoc = consultaValor("SELECT CDU_IdCabec FROM  TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'")
                idCabecStk = consultaValor("SELECT id from cabecstk where id in (select idcabecorig from linhasstk where id in (select cdu_IdLinhaInternos from TDU_APSLINHASREGISTOFOLHAPONTO where CDU_TipoArtigo='MC' and CDU_IdTDUCabec='" + registo.ID + "'))")
                If getMOLDULOP() = "I" Then
                    removerDocumentoInterno(idCabecDoc)
                Else
                    removerDocumentoVenda(idCabecDoc)
                End If

                removerDocumentoStock(idCabecStk)

                comando = "DELETE FROM  TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'"
                executarComando(comando)
                comando = "DELETE FROM  TDU_APSLinhasRegistoFolhaPonto WHERE CDU_IdTDUCabec='" + registo.ID + "'"
                executarComando(comando)
                bso.TerminaTransaccao()
                MsgBox("Registo removido com sucesso", MsgBoxStyle.Information)
            Else
                MsgBox("Registo Inexistente " + vbCrLf + vbCrLf + "Impossivel remover o registo", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            bso.DesfazTransaccao()
        End Try

    End Sub

    ''' <summary>
    ''' funcão que permite iniciar uma operacao
    ''' </summary>
    ''' <param name="registo"></param>
    ''' <param name="terminouponto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function iniciarOperacao(ByVal registo As Registo, ByVal terminouponto As Boolean) As String
        Dim msg As String
        Dim comando As String
        iniciarOperacao = ""
        '  If idLinha = "" Then
        msg = obraOperacaoJaIniciada(registo.Posto, registo.Operacao, funcionario_, True)
        If msg = "" Then
            Try
                bso.IniciaTransaccao()
                Dim id As String
                id = consultaValor("SELECT NEWID()")
                If terminouponto Then
                    comando = "INSERT INTO TDU_APSCabecRegistoFolhaPonto(CDU_ID,CDU_Funcionario,CDU_TipoEntidade,CDU_Entidade,CDU_Posto,CDU_Operacao,CDU_Molde,CDU_Requisicao,CDU_DataInicial,CDU_DataFinal,CDU_TempoDescontado,CDU_IdCabec,CDU_Tempo,CDU_Observacoes,CDU_Problema) VALUES('" + id + "','" + registo.Funcionario + "','" + registo.TipoEntidade + "','" + registo.Entidade + "','" + registo.Posto + "','" + registo.Operacao + "','" + registo.Molde + "','" + registo.Requisicao + "',Convert(datetime,'" + CStr(registo.DataInicial) + "',105),Convert(datetime,'" + CStr(registo.DataFinal) + "',105),Convert(datetime,'" + CStr(registo.TempoDescontado) + "',105)," + IIf(registo.CDU_IdCabec = "", "NULL", "'" + registo.CDU_IdCabec + "'") + "," + Replace(registo.LinhaRegistoMaoObra.Quantidade.ToString, ",", ".") + ",'" + registo.Observacoes + "','" + registo.Problema + "')"
                Else
                    comando = "INSERT INTO TDU_APSCabecRegistoFolhaPonto(CDU_ID,CDU_Funcionario,CDU_TipoEntidade,CDU_Entidade,CDU_Posto,CDU_Operacao,CDU_Molde,CDU_Requisicao,CDU_DataInicial) VALUES('" + id + "','" + registo.Funcionario + "','" + registo.TipoEntidade + "','" + registo.Entidade + "','" + registo.Posto + "','" + registo.Operacao + "','" + registo.Molde + "','" + registo.Requisicao + "',Convert(datetime,'" + CStr(Now) + "',105))"
                End If
                ' InputBox("", "", comando)
                executarComando(comando)
                registo.ID = id
                inserirArtigos(registo)
                bso.TerminaTransaccao()
                If terminouponto Then
                    Return registo.ID
                Else
                    Return ""
                End If
            Catch ex As Exception
                bso.DesfazTransaccao()
                ' "Motor L-339 : " + ex.Message, MsgBoxStyle.Critical
                iniciarOperacao = "Motor L-339 : " + ex.Message
            End Try
        Else
            Return "O Posto " + registo.Posto + " já se encontra iniciado com o utilizador:" + msg
        End If
        Return ""
    End Function

    Private Sub inserirArtigos(ByVal registo As Registo)
        Dim comando As String
        Dim numlinha As Integer

        Try
            'Apagar todas as linhas do documento
            comando = "DELETE  FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_IdTDUCabec='" + registo.ID + "'"
            executarComando(comando)

            Dim linha As LinhaRegisto

            linha = registo.LinhaRegistoMaoObra

            numlinha = 1
            If linha.Artigo <> "" Then
                If linha.CDU_IDLinhaInterno = "" Then
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_MAOOBRA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "')"
                Else
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_MAOOBRA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + linha.CDU_IDLinhaInterno + "')"
                End If
                ' InputBox("", "", comando)
                executarComando(comando)
                numlinha = numlinha + 1
            End If

            linha = registo.LinhaRegistoMaterialLimpeza


            If Not linha Is Nothing Then
                If linha.Artigo <> "" And linha.Quantidade <> 0 Then
                    If linha.CDU_IDLinhaInterno = "" Then
                        comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_LIMPEZA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "')"
                    Else
                        comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_LIMPEZA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + linha.CDU_IDLinhaInterno + "')"
                    End If
                    ' InputBox("", "", comando)
                    executarComando(comando)
                    numlinha = numlinha + 1
                End If
            End If


            Dim idLinha As String
            Dim lista As List(Of LinhaRegisto)

            lista = registo.LinhasRegistosUMR
            Dim i As Integer
            For i = 0 To lista.Count - 1
                linha = lista(i)
                idLinha = linha.CDU_IDLinhaInterno
                If idLinha = "" Then
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_RTJ + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "')"
                Else
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_RTJ + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + idLinha + "')"
                End If
                executarComando(comando)
                numlinha = numlinha + 1
            Next


            lista = registo.LinhasRegistosUMC

            For i = 0 To lista.Count - 1
                linha = lista(i)
                idLinha = linha.CDU_IDLinhaInterno
                If idLinha = "" Then
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_RTJ + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "')"
                Else
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_CLIENTE + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + idLinha + "')"
                End If
                executarComando(comando)
                numlinha = numlinha + 1
            Next
            '  bso.TerminaTransaccao()
        Catch ex As Exception
            ' bso.DesfazTransaccao()
        End Try
    End Sub
    Public Sub TerminarOperacaoFinal(ByVal registo As Registo, ByVal data As Date)

        Dim comando As String


        If registo.ID = "" Then
            registo.ID = consultaValor("SELECT CDU_ID FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Operacao='" + registo.Operacao + "' and CDU_Posto='" + registo.Posto + "' and CDU_Funcionario='" + funcionario_ + "' and CDU_Entidade='" + registo.ID + "' and CDU_TipoEntidade='" + tipoEntidadeP_ + "'  AND CDU_IdCabec IS NULL")
        End If
        If registo.ID = "" Then
            registo.ID = iniciarOperacao(registo, True)
            inserirArtigos(registo)
        Else
            comando = "UPDATE TDU_APSCabecRegistoFolhaPonto SET CDU_Entidade='" + registo.Entidade + "',CDU_TipoEntidade='" + registo.TipoEntidade + "',  CDU_Molde='" + registo.Molde + "', CDU_Requisicao='" + registo.Requisicao + "',CDU_Operacao='" + registo.Operacao + "', CDU_Posto='" + registo.Posto + "',CDU_DataInicial=Convert(datetime,'" + CStr(registo.DataInicial) + "',105),CDU_DataFinal=Convert(datetime,'" + CStr(registo.DataFinal) + "',105),CDU_TempoDescontado=Convert(datetime,'" + CStr(registo.TempoDescontado) + "',105),CDU_Tempo='" + Replace(registo.LinhaRegistoMaoObra.Quantidade.ToString, ",", ".") + "',CDU_Observacoes='" + registo.Observacoes + "',CDU_Problema='" + registo.Problema + "',CDU_IdCabec=" + IIf(registo.CDU_IdCabec = "", "NULL", "'" + registo.CDU_IdCabec + "'") + " WHERE CDU_Id='" + registo.ID + "'"
            executarComando(comando)
            inserirArtigos(registo)
        End If

    End Sub

    ''' <summary>
    ''' Função que permite validar se um posto já se encontra inicializado
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="operacao"></param>
    ''' <param name="funcionarioPosto"></param>
    ''' <param name="mostraMSG"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obraOperacaoJaIniciada(ByVal posto As String, ByVal operacao As String, ByRef funcionarioPosto As String, Optional ByVal mostraMSG As Boolean = True) As String
        Dim lista As StdBELista
        lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Posto='" + posto + "' and CDU_DataFinal IS NULL")
        obraOperacaoJaIniciada = ""
        If Not lista.NoFim Then
            funcionarioPosto = lista.Valor("CDU_Funcionario")
            obraOperacaoJaIniciada = lista.Valor("CDU_Funcionario")
            If mostraMSG Then
                obraOperacaoJaIniciada = vbCrLf + vbCrLf + consultaValor("Select Nome FROM Funcionarios where Codigo='" + lista.Valor("CDU_Funcionario") + "'")
                obraOperacaoJaIniciada += vbCrLf + vbCrLf + "Cliente: " + consultaValor("Select Nome FROM Clientes where Cliente='" + lista.Valor("CDU_Entidade") + "'")
                obraOperacaoJaIniciada += vbCrLf + "Operação: " + lista.Valor("CDU_Operacao") 'consultaValor("SELECT CDU_Descricao FROM TDU_ObraN2 WHERE CDU_ObraN2='" + lista.Valor("CDU_Operacao") + "'")
            End If
            If obraOperacaoJaIniciada = "" Then
                obraOperacaoJaIniciada = " "
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite validar se um posto já se encontra inicializado
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="entidade"></param>
    ''' <param name="operacao"></param>
    ''' <param name="mostraMSG"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obraPodeSerTerminada(ByVal posto As String, ByVal entidade As String, ByVal operacao As String, Optional ByVal mostraMSG As Boolean = True) As String
        Dim lista As StdBELista
        lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_TipoEntidade='" + tipoEntidadeP_ + "' AND CDU_Entidade='" + entidade + "' AND CDU_Operacao='" + operacao + "' AND CDU_Posto='" + posto + "' and CDU_IdCabec IS NULL")
        obraPodeSerTerminada = ""
        If lista.NoFim Then
            obraPodeSerTerminada = funcionario_
            If mostraMSG Then
                lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Posto='" + posto + "' and CDU_IdCabec IS NULL")
                If Not lista.NoFim Then
                    obraPodeSerTerminada = consultaValor("Select Nome FROM Funcionarios where Codigo='" + lista.Valor("CDU_Funcionario") + "'")
                    obraPodeSerTerminada += vbCrLf + vbCrLf + "Cliente: " + consultaValor("Select Nome FROM Clientes where Cliente='" + lista.Valor("CDU_Entidade") + "'")
                    '  obraPodeSerTerminada += vbCrLf + "Peça: " + lista.Valor("CDU_Peca")
                    obraPodeSerTerminada += vbCrLf + "Operação: " + lista.Valor("CDU_Operacao") 'consultaValor("SELECT CDU_Descricao FROM TDU_ObraN2 WHERE CDU_ObraN2='" + lista.Valor("CDU_Operacao") + "'")
                End If
            End If
        End If
    End Function


    'Public Function TerminarOperacao(ByVal registo As Registo) As Integer
    '    TerminarOperacao = abrirFormTerminar(registo)
    'End Function

    'Public Function abrirFormTerminar(ByVal registo As Registo) As Integer


    '    Dim frmT As FrmTerminar
    '    frmT = New FrmTerminar
    '    frmT.Registo = registo


    '    Try
    '        frmT.ShowDialog()
    '        abrirFormTerminar = frmT.BotaoSeleccionado
    '        frmT.Close()
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '        abrirFormTerminar = 3
    '    End Try

    'End Function
    Public Function NuloToDate(ByVal obj As Object, ByVal def As DateTime) As DateTime
        If IsDBNull(obj) Or IsNothing(obj) Then
            NuloToDate = def
        Else
            If Not IsDate(obj) Then
                NuloToDate = def
            Else
                NuloToDate = obj
            End If
        End If
    End Function

    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Or IsNothing(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                NuloToDouble = 0
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Public Function NuloToString(ByVal obj) As String
        If IsDBNull(obj) Or IsNothing(obj) Then
            NuloToString = ""
        Else
            NuloToString = CStr(obj)
        End If

    End Function

    Public Function NuloToBoolean(ByVal obj) As Double
        If IsDBNull(obj) Or IsNothing(obj) Then
            NuloToBoolean = 0
        Else
            NuloToBoolean = CBool(obj)
        End If

    End Function
    Public Function actualizarDocumentoInterno(ByVal registo As Registo, ByVal data As Date) As String
        Dim docI As GcpBEDocumentoInterno
        Dim linhaI As GcpBELinhaDocumentoInterno
        Dim id As String
        docI = Nothing
        ' Dim dataI As String
        'dataI = getDateDocument(data)
        Dim idCabecdoc As String
        Dim idCabecStk As String
        Dim ultimoNumDoc As Long
        Dim ultimoNumDocBase As Long
        ultimoNumDoc = -1
        ultimoNumDocBase = -1
        If registo.ID <> "" Then
            idCabecdoc = consultaValor("SELECT CDU_IdCabec FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'")
            idCabecStk = consultaValor("SELECT id from cabecstk where id in (select idcabecorig from linhasstk where id in (select cdu_IdLinhaInternos from TDU_APSLINHASREGISTOFOLHAPONTO where CDU_TipoArtigo='MC' and CDU_IdTDUCabec='" + registo.ID + "'))")

            If idCabecdoc <> "" Then
                ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
                id = idCabecdoc
                Try
                    docI = bso.Comercial.Internos.EditaID(id)
                Catch ex As Exception
                    ' MsgBox(ex.Message, MsgBoxStyle.Critical)
                    docI = Nothing
                End Try

                If Not docI Is Nothing Then
                    removerDocumentoInterno(id)
                    ultimoNumDoc = docI.NumDoc - 1
                    docI = Nothing
                End If
            End If

            If idCabecStk <> "" Then
                removerDocumentoStock(idCabecStk)
            End If
        End If

        If docI Is Nothing Then
            docI = New GcpBEDocumentoInterno
            docI.Tipodoc = tipoDocP_
            If serieP_ <> "" Then
                docI.Serie = serieP_
            Else
                docI.Serie = bso.Comercial.Series.DaSerieDefeito("N", tipoDocP_, data)
            End If
        End If
        If registo.Problema <> "" Then
            docI.Observacoes = registo.Problema
        End If

        If registo.Observacoes <> "" Then
            If registo.Problema <> "" Then docI.Observacoes = docI.Observacoes + vbCrLf + vbCrLf
            docI.Observacoes = docI.Observacoes + registo.Observacoes
        End If
        '   docI.Observacoes = registo.Problema + vbCrLf + vbCrLf + registo.Observacoes

        docI.Entidade = registo.Entidade
        docI.TipoEntidade = registo.TipoEntidade
        bso.Comercial.Internos.PreencheDadosRelacionados(docI)

        If registo.Funcionario = "" Then
            docI.Utilizador = funcionario_
        Else
            docI.Utilizador = registo.Funcionario
        End If
        docI.Data = CDate(data)
        docI.DataVencimento = docI.Data
        docI.Referencia = registo.Molde
        'remove todas as linhas
        docI.Linhas.RemoveTodos()

        Dim idProjecto As String
        Dim linha As LinhaRegisto

        idProjecto = daIdProjecto(registo.Posto)

        linha = registo.LinhaRegistoMaoObra

        'Preço Mao de Obra
        If Not linha Is Nothing Then
            If linha.Artigo <> "" Then

                bso.Comercial.Internos.AdicionaLinha(docI, linha.Artigo, linha.Armazem)
                linhaI = docI.Linhas(docI.Linhas.NumItens)
                Dim precunit As Double

                precunit = daValorPrecoArtigo(docI, linhaI.Artigo, linhaI.Unidade)

                If precunit <> -1 Then
                    linhaI.PrecoUnitario = precunit
                End If

                linhaI.Quantidade = linha.Quantidade
                linhaI.ObraID = idProjecto
                linhaI.CamposUtil(campoMolde) = registo.Molde
                linhaI.CamposUtil(campoRequisicao) = registo.Requisicao

                linha.CDU_IDLinhaInterno = linhaI.ID

            Else
                MsgBox("Não existe artigo configurado para o Posto " + registo.Posto, MsgBoxStyle.Critical)
            End If
        End If

        linha = registo.LinhaRegistoMaterialLimpeza
        'Material de Limpeza

        If Not linha Is Nothing Then
            If linha.Quantidade <> 0 Then
                bso.Comercial.Internos.AdicionaLinha(docI, linha.Artigo, linha.Armazem)
                linhaI = docI.Linhas(docI.Linhas.NumItens)
                linhaI.Quantidade = linha.Quantidade
                linhaI.ObraID = idProjecto
                linhaI.CamposUtil(campoMolde) = registo.Molde
                linhaI.CamposUtil(campoRequisicao) = registo.Requisicao
                linha.CDU_IDLinhaInterno = linhaI.ID
            End If
        End If


        Dim lista As List(Of LinhaRegisto)

        lista = registo.LinhasRegistosUMR
        Dim i As Integer
        For i = 0 To lista.Count - 1
            linha = lista(i)
            bso.Comercial.Internos.AdicionaLinha(docI, linha.Artigo, linha.Armazem)
            linhaI = docI.Linhas(docI.Linhas.NumItens)
            linhaI.Quantidade = linha.Quantidade
            linhaI.ObraID = idProjecto
            linhaI.CamposUtil(campoMolde) = registo.Molde
            linhaI.CamposUtil(campoRequisicao) = registo.Requisicao
            linha.CDU_IDLinhaInterno = linhaI.ID
        Next

        actualizarDocumentoMaterialCliente(registo, idProjecto, data)

        Try
            If docI.Linhas.NumItens > 0 Then

                If ultimoNumDoc <> -1 Then
                    ultimoNumDocBase = bso.Comercial.Series.DaValorAtributo("N", tipoDocP_, docI.Serie, "Numerador")
                    bso.Comercial.Series.ActualizaNumerador("N", tipoDocP_, docI.Serie, ultimoNumDoc, data)
                End If
                bso.Comercial.Internos.Actualiza(docI)

                If ultimoNumDoc <> -1 Then
                    bso.Comercial.Series.ActualizaNumerador("N", tipoDocP_, docI.Serie, ultimoNumDocBase, data)
                End If
                Return docI.ID
            Else
                Return ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Private Sub actualizarDocumentoMaterialCliente(ByRef registo As Registo, ByVal idProjecto As String, ByVal data As Date)
        Dim lista As List(Of LinhaRegisto)
        Dim doc As GcpBEDocumentoStock
        Dim linhaS As GcpBELinhaDocumentoStock
        Dim linha As LinhaRegisto
        lista = registo.LinhasRegistosUMC

        doc = New GcpBEDocumentoStock

        If seriePS_ <> "" Then
            doc.Serie = seriePS_
        Else
            doc.Serie = bso.Comercial.Series.DaSerieDefeito("S", tipoDocPS_, data)
        End If
        doc.Tipodoc = tipoDocPS_
        bso.Comercial.Stocks.PreencheDadosRelacionados(doc)
        doc.DataDoc = data
        For i = 0 To lista.Count - 1
            linha = lista(i)
            bso.Comercial.Stocks.AdicionaLinha(doc, linha.Artigo)
            linhaS = doc.Linhas(doc.Linhas.NumItens)
            linhaS.Armazem = linha.Armazem
            linhaS.Quantidade = linha.Quantidade
            linhaS.IDObra = idProjecto
            linha.CDU_IDLinhaInterno = linhaS.IdLinha
        Next

        bso.Comercial.Stocks.Actualiza(doc)

    End Sub

    Public Function actualizarDocumentoVenda(ByVal registo As Registo, ByVal data As Date) As String
        Dim docV As GcpBEDocumentoVenda
        Dim linhaV As GcpBELinhaDocumentoVenda
        Dim id As String
        docV = Nothing
        ' Dim dataI As String
        'dataI = getDateDocument(data)
        Dim idCabecdoc As String
        Dim idCabecStk As String
        Dim ultimoNumDoc As Long
        Dim ultimoNumDocBase As Long
        ultimoNumDoc = -1
        ultimoNumDocBase = -1
        If registo.ID <> "" Then
            idCabecdoc = consultaValor("SELECT CDU_IdCabec FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'")
            idCabecStk = consultaValor("SELECT id from cabecstk where id in (select idcabecorig from linhasstk where id in (select cdu_IdLinhaInternos from TDU_APSLINHASREGISTOFOLHAPONTO where CDU_TipoArtigo='MC' and CDU_IdTDUCabec='" + registo.ID + "'))")

            If idCabecdoc <> "" Then
                ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
                id = idCabecdoc
                Try
                    docV = bso.Comercial.Vendas.EditaID(id)
                Catch ex As Exception
                    ' MsgBox(ex.Message, MsgBoxStyle.Critical)
                    docV = Nothing
                End Try
                If Not docV Is Nothing Then
                    '  removerDocumentoVenda(id)
                    ultimoNumDoc = docV.NumDoc - 1
                    ' docV = Nothing
                End If
            End If

            If idCabecStk <> "" Then
                removerDocumentoStock(idCabecStk)
            End If

        End If

        If docV Is Nothing Then
            docV = New GcpBEDocumentoVenda

            '  bso.Comercial.Vendas.AdicionaLinha(docV, "72110106", 1)
            docV.Tipodoc = tipoDocP_
            If serieP_ <> "" Then
                docV.Serie = serieP_
            Else
                docV.Serie = bso.Comercial.Series.DaSerieDefeito("V", tipoDocP_, data)
            End If
        End If

        If registo.Problema <> "" Then
            docV.Observacoes = registo.Problema
        End If

        If registo.Observacoes <> "" Then
            If registo.Problema <> "" Then docV.Observacoes = docV.Observacoes + vbCrLf + vbCrLf
            docV.Observacoes = docV.Observacoes + registo.Observacoes
        End If
        '   docI.Observacoes = registo.Problema + vbCrLf + vbCrLf + registo.Observacoes

        docV.Entidade = registo.Entidade
        docV.TipoEntidade = registo.TipoEntidade
        If ultimoNumDoc = -1 Then
            bso.Comercial.Vendas.PreencheDadosRelacionados(docV)
        Else
            bso.Comercial.Vendas.PreencheDadosRelacionados(docV, PreencheRelacaoVendas.vdDadosCliente)
        End If
        '  bso.Comercial.Vendas.AdicionaLinha(docV, "72110106", 1)
        If registo.Funcionario = "" Then
            docV.Utilizador = funcionario_
        Else
            docV.Utilizador = registo.Funcionario
        End If
        docV.DataDoc = CDate(FormatDateTime(data, DateFormat.ShortDate))
        docV.DataVenc = docV.DataDoc
        docV.Referencia = registo.Requisicao '+ " / " + registo.Molde
        docV.Requisicao = registo.Requisicao '+ " / " + registo.Molde

        'remove todas as linhas


        docV.Linhas.RemoveTodos()

        Dim idProjecto As String
        Dim linha As LinhaRegisto

        idProjecto = daIdProjecto(registo.Posto)

        linha = registo.LinhaRegistoMaoObra


        'Preço Mao de Obra
        If Not linha Is Nothing Then
            If linha.Artigo <> "" Then

                bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                Dim precunit As Double

                precunit = daValorPrecoArtigo(docV, linhaV.Artigo, linhaV.Unidade)

                If precunit <> -1 Then
                    linhaV.PrecUnit = precunit
                End If

                linhaV.Quantidade = linha.Quantidade
                linhaV.IDObra = idProjecto
                linhaV.CamposUtil(campoMolde) = registo.Molde
                linhaV.CamposUtil(campoRequisicao) = registo.Requisicao
                linha.CDU_IDLinhaInterno = linhaV.IdLinha
            Else
                MsgBox("Não existe artigo configurado para o Posto " + registo.Posto, MsgBoxStyle.Critical)
            End If
        End If

        linha = registo.LinhaRegistoMaterialLimpeza
        'Material de Limpeza

        If Not linha Is Nothing Then
            If linha.Quantidade <> 0 Then
                bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                linhaV.Quantidade = linha.Quantidade
                linhaV.IDObra = idProjecto
                linhaV.CamposUtil(campoMolde) = registo.Molde
                linhaV.CamposUtil(campoRequisicao) = registo.Requisicao
                linha.CDU_IDLinhaInterno = linhaV.IdLinha
            End If
        End If


        Dim lista As List(Of LinhaRegisto)

        lista = registo.LinhasRegistosUMR
        Dim i As Integer
        For i = 0 To lista.Count - 1
            linha = lista(i)
            bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
            linhaV = docV.Linhas(docV.Linhas.NumItens)
            linhaV.Quantidade = linha.Quantidade
            linhaV.IDObra = idProjecto
            linhaV.CamposUtil(campoMolde) = registo.Molde
            linhaV.CamposUtil(campoRequisicao) = registo.Requisicao
            linha.CDU_IDLinhaInterno = linhaV.IdLinha
        Next

        'lista = Registo.LinhasRegistosUMC

        'For i = 0 To lista.Count - 1
        '    linha = lista(i)
        '    bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
        '    linhaV = docV.Linhas(docV.Linhas.NumItens)
        '    linhaV.Quantidade = linha.Quantidade
        '    linhaV.IDObra = idProjecto
        '    linha.CDU_IDLinhaInterno = linhaV.IdLinha
        'Next

        actualizarDocumentoMaterialCliente(registo, idProjecto, data)

        Try
            If docV.Linhas.NumItens > 0 Then

                'If ultimoNumDoc <> -1 Then
                '    ultimoNumDocBase = bso.Comercial.Series.DaValorAtributo("V", tipoDocP_, docV.Serie, "Numerador")
                '    bso.Comercial.Series.ActualizaNumerador("V", tipoDocP_, docV.Serie, ultimoNumDoc, data)
                'End If
                bso.Comercial.Vendas.Actualiza(docV)

                'If ultimoNumDoc <> -1 Then
                '    bso.Comercial.Series.ActualizaNumerador("V", tipoDocP_, docV.Serie, ultimoNumDocBase, data)
                'End If

                Return docV.ID
            Else
                Return ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Private Function daValorPrecoArtigo(ByVal docI As GcpBEDocumentoInterno, ByVal artigo As String, ByVal unidade As String) As Double

        Dim precUnit As Double
        precUnit = -1
        If docI.TipoEntidade = "C" Then
            Dim entidade As GcpBECliente
            Dim preco As GcpBEArtigoMoeda
            preco = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, unidade)
            entidade = bso.Comercial.Clientes.Edita(docI.Entidade)
            Select Case entidade.LinhaPrecos
                Case 0 : precUnit = preco.PVP1
                Case 1 : precUnit = preco.PVP2
                Case 2 : precUnit = preco.PVP3
                Case 3 : precUnit = preco.PVP4
                Case 4 : precUnit = preco.PVP5
                Case 5 : precUnit = preco.PVP6
            End Select
        End If
        Return precUnit

    End Function

    Private Function daValorPrecoArtigo(ByVal docI As GcpBEDocumentoVenda, ByVal artigo As String, ByVal unidade As String) As Double

        Dim precUnit As Double
        precUnit = -1
        If docI.TipoEntidade = "C" Then
            Dim entidade As GcpBECliente
            Dim preco As GcpBEArtigoMoeda
            preco = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, unidade)
            entidade = bso.Comercial.Clientes.Edita(docI.Entidade)
            Select Case entidade.LinhaPrecos
                Case 0 : precUnit = preco.PVP1
                Case 1 : precUnit = preco.PVP2
                Case 2 : precUnit = preco.PVP3
                Case 3 : precUnit = preco.PVP4
                Case 4 : precUnit = preco.PVP5
                Case 5 : precUnit = preco.PVP6
            End Select
        End If
        Return precUnit

    End Function

    Private Function daArtigoProjecto(ByVal projecto As String)
        daArtigoProjecto = ""
        daArtigoProjecto = consultaValor("SELECT CDU_Artigo FROM COP_Obras WHERE Codigo='" + projecto + "'")
    End Function


    Private Function daArtigoMLProjecto(ByVal projecto As String)
        daArtigoMLProjecto = ""
        daArtigoMLProjecto = consultaValor("SELECT CDU_ArtigoML FROM COP_Obras WHERE Codigo='" + projecto + "'")

        If daArtigoMLProjecto = "" Then
            daArtigoMLProjecto = artigoML
        End If
    End Function

    Private Function daIdProjecto(ByVal projecto As String)
        daIdProjecto = ""
        daIdProjecto = consultaValor("SELECT Id FROM COP_Obras WHERE Codigo='" + projecto + "'")
    End Function

    Private Sub removerLinhaDocumentoVenda(ByVal idLinha As String)
        Dim id As String
        Dim docI As GcpBEDocumentoInterno
        Dim linha As GcpBELinhaDocumentoInterno

        id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idLinha + "'")
        If id = "" Then
            Exit Sub
        Else
            docI = bso.Comercial.Internos.EditaID(id)
        End If

        Dim i As Integer
        i = 1
        For Each linha In docI.Linhas
            If linha.ID = idLinha Then
                ' linhaI = linha
                docI.Linhas.Remove(i)
                Exit For
            End If
            i = i + 1
        Next

        Try
            bso.Comercial.Internos.Actualiza(docI)
        Catch ex As Exception

        End Try


    End Sub

    '''' <summary>
    '''' Função que permite criar toda a extrutura da BD exitente
    '''' </summary>
    '''' <remarks></remarks>
    'Private Sub inicializarAplicacao()
    '    Dim lista As StdBELista
    '    lista = consulta("SELECT *  FROM sysobjects WHERE  name='APS_GP_Funcionarios'")
    '    If lista.NoFim Then
    '        executarComando("CREATE VIEW APS_GP_Funcionarios AS SELECT TOP (10000000) dbo.Funcionarios.* FROM Funcionarios")
    '    End If
    'End Sub

    ''' <summary>
    ''' Funcao que permite validar se uma peça existe
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function pecaExiste(ByVal peca As String) As Boolean
        Dim peca1 As String
        peca1 = consultaValor("SELECT CDU_ObraN1 FROm TDU_ObraN1 WHERE CDU_ObraN1='" + peca + "'")
        pecaExiste = peca1 <> ""
    End Function

    Public Function projetoExiste(ByVal projeto As String) As Boolean
        Dim projeto1 As String
        projeto1 = consultaValor("SELECT Codigo FROm COP_Obras WHERE Codigo='" + projeto + "'")
        projetoExiste = projeto1 <> ""
    End Function

    ''' <summary>
    ''' Função que permite inserir uma peça no sistema
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <param name="descricao"></param>
    ''' <remarks></remarks>
    Public Sub inserirPeca(ByVal peca As String, ByVal descricao As String, activa As Boolean)
        Dim strActiva As String
        If activa Then
            strActiva = "0"
        Else
            strActiva = "1"
        End If

        If pecaExiste(peca) Then
            executarComando("UPDATE TDU_ObraN1 set CDU_Descricao='" + descricao + "', CDU_Inactivo=" + strActiva + " WHERE CDU_ObraN1='" + peca + "'")
        Else
            executarComando("INSERT INTO TDU_ObraN1(CDu_ObraN1, CDU_Descricao, CDU_Inactivo) VALUES('" + peca + "','" + descricao + "'," + strActiva + ")")
        End If

    End Sub

    Public Sub inserirProjeto(ByVal projeto As String, ByVal descricao As String)


        If Not projetoExiste(projeto) Then
            Dim proj As GcpBEProjecto
            proj = New GcpBEProjecto()

            proj.Codigo = projeto
            proj.Descricao = descricao
            proj.TipoProjecto = "1"
            proj.Projecto = True
            proj.EstadoProjecto = "ABTO"
            proj.DataPrevisaoInicio = Now
            proj.DataPrevisaoFim = Now
            bso.Comercial.Projectos.Actualiza(proj)
        End If

    End Sub





    'Private Function getDateDocument(ByVal data As Date) As String
    '    Dim dataI As String
    '    dataI = ""
    '    If modoFuncionamento = "D" Then
    '        dataI = CStr(data.Day) + "-" + CStr(data.Month) + "-" + CStr(data.Year)
    '    End If

    '    If modoFuncionamento = "S" Then
    '        data = data.AddDays(-data.DayOfWeek + 1)
    '        dataI = CStr(data.Day) + "-" + CStr(data.Month) + "-" + CStr(data.Year)
    '    End If

    '    If modoFuncionamento = "M" Then
    '        dataI = "01" + "-" + CStr(Month(data)) + "-" + CStr(Year(data))
    '    End If
    '    Return dataI
    'End Function
    'Private Function buscarIdDocumento1(ByVal func As String, ByVal data As Date) As String
    '    Dim dataI As String
    '    dataI = getDateDocument(data)
    '    buscarIdDocumento1 = consultaValor("SELECT Id FROM CabecInternos where Entidade='" + func + "' and TipoDoc='" + tipoDoc_ + "' and Serie='" + serie_ + "' and Data=Convert(Datetime,'" + dataI + "',105)")
    'End Function

    Public Function daListaArtigos(ByVal view As String, ByVal pesquisa As String, ByVal armazem As String, ByVal artigosExluir As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        Dim filtro As String
        filtro = ""
        If pesquisa <> "" Then
            If artigosExluir <> "" Then
                filtro = "Artigo not in (" + artigosExluir + ") and "
            End If

            If armazem <> "" Then
                filtro += " Armazem ='" + armazem + "' AND"
            End If

            lista = consulta("SELECT * FROM " + view + " WHERE " + filtro + " (Artigo Like '%" + pesquisa + "%' OR Descricao Like '%" + pesquisa + "%')")
        Else
            If artigosExluir <> "" Then
                filtro = "WHERE Artigo not in (" + artigosExluir + ")"
            End If

            If armazem <> "" Then
                If filtro <> "" Then
                    filtro += "AND Armazem ='" + armazem + "'"
                Else
                    filtro += "WHERE Armazem ='" + armazem + "'"
                End If
            End If
            ' InputBox("", "", "SELECT * FROM " + view + " " + filtro)
            lista = consulta("SELECT * FROM " + view + " " + filtro)
        End If

        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daListaArtigosDocumento(ByVal idDocumento As String, ByVal modulo As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        If modulo = "I" Then
            Dim view As String
            view = "APS_GP_Artigos"
            If Not apresentaProjectos Then view = "APS_GP_Artigos1"
            lista = consulta("SELECT " + view + ".*,LinhasInternos.* FROM " + view + " INNER JOIN LinhasInternos ON " + view + ".Artigo=LinhasInternos.Artigo WHERE LinhasInternos.IdCabecInternos='" + idDocumento + "' ORDER BY NumLinha")
        End If

        If modulo = "C" Then
            lista = consulta("SELECT  LinhasCompras.*,COP_Obras.Codigo FROM LinhasCompras LEFT JOIN COP_Obras ON LinhasCompras.ObraId=Cop_Obras.ID WHERE LinhasCompras.IdCabecCompras='" + idDocumento + "' and ((LinhasCompras.Artigo<>'" + artInsp1 + "' and LinhasCompras.Artigo<>'" + artInsp2 + "') OR LinhasCompras.Artigo is null) ORDER BY NumLinha")
        End If

        If modulo = "V" Then
            lista = consulta("SELECT  LinhasDoc.*,COP_Obras.Codigo FROM LinhasDoc LEFT JOIN COP_Obras ON LinhasDoc.ObraId=Cop_Obras.ID WHERE LinhasDoc.IdCabecDoc='" + idDocumento + "' and ((LinhasDoc.Artigo<>'" + artInsp1 + "' and LinhasDoc.Artigo<>'" + artInsp2 + "') Or LinhasDoc.Artigo is null ) ORDER BY NumLinha")
        End If

        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function actualizarDocumentoInterno(documento As String, ByVal func As String, ByVal molde As String, ByVal data As Date, ByVal recordsGrid As ReportRecords, Optional ByVal idCabecdoc As String = "") As String

        'ola
        Const COLUMN_ARTIGO As Integer = 0
        Const COLUMN_QUANTIDADE As Integer = 3
        Const COLUMN_QUANTIDADEARM As Integer = 2
        Const COLUMN_ARMAZEM As Integer = 3
        Const COLUMN_PROJECTO As Integer = 3


        Dim docI As GcpBEDocumentoInterno
        Dim linhaI As GcpBELinhaDocumentoInterno

        Dim id As String

        docI = Nothing

        If idCabecdoc <> "" Then
            ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
            id = idCabecdoc
            docI = bso.Comercial.Internos.EditaID(id)
        End If

        If docI Is Nothing Then
            docI = New GcpBEDocumentoInterno
            docI.Tipodoc = documento
            If serieS_ <> "" Then
                docI.Serie = serieS_
            Else
                docI.Serie = bso.Comercial.Series.DaSerieDefeito("N", documento, data)
            End If

            docI.Entidade = func
            docI.TipoEntidade = tipoEntidadeS_
            bso.Comercial.Internos.PreencheDadosRelacionados(docI)
        End If
        docI.Utilizador = utilizador
        docI.Data = CDate(data)
        docI.DataVencimento = docI.Data

        docI.Linhas.RemoveTodos()
        Dim artigo As String
        Dim quantidade As Double
        Dim row As ReportRecord
        Dim unidade As String
        Dim arm As String

        Dim arrCampos As ArrayList
        arrCampos = buscarCampos()

        For Each row In recordsGrid
            artigo = row(COLUMN_ARTIGO).Value
            unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
            'caso seja para apresentar Projectos ou para ser como armazem
            If apresentaProjectos Then
                quantidade = row(COLUMN_QUANTIDADE).Value
                bso.Comercial.Internos.AdicionaLinha(docI, artigo, , , , , , quantidade)
            Else
                quantidade = row(COLUMN_QUANTIDADEARM).Value
                arm = row(COLUMN_ARMAZEM).Tag
                bso.Comercial.Internos.AdicionaLinha(docI, artigo, arm, , , , , quantidade)
            End If
            linhaI = docI.Linhas(docI.Linhas.NumItens)
            linhaI.Quantidade = quantidade
            '   linhaI.PrecoUnitario = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, unidade).PVP1
            'caso seja para apresentar Projectos ou para ser como armazem
            If apresentaProjectos Then
                linhaI.ObraID = bso.Comercial.Projectos.DaValorAtributo(molde, "ID")
            End If

            'campos de Utilizador
            For i = 0 To arrCampos.Count - 1
                If existeColuna("LinhasInternos", arrCampos(i)) And Left(arrCampos(i), 3).ToUpper = "CDU" Then
                    linhaI.CamposUtil(arrCampos(i)).Valor = NuloToString(row(COLUMN_PROJECTO + i + 1).Value)
                Else
                    If arrCampos(i).ToString.ToUpper = "DATAENTREGA" Then
                        If IsDate(row(COLUMN_PROJECTO + i + 1).Value) Then
                            linhaI.DataEntrega = NuloToDate(row(COLUMN_PROJECTO + i + 1).Value, New Date())
                        End If
                    End If
                    End If


            Next

        Next

        Try
            If docI.Linhas.NumItens > 0 Or idCabecdoc <> "" Then
                bso.Comercial.Internos.Actualiza(docI)

                'Actualização para o Kardex
                sincronizarMaquinaKardex(docI, molde)

                Return docI.ID
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Public Function actualizarDocumentoCompra(fornecedor As Entidade, ByVal tipodoc As String, ByVal entidade As String, ByVal func As String, ByVal molde As String, observacoes As String, ByVal data As Date, ByVal recordsGrid As ReportRecords, matricula As String, localCarga As String, localDescarga As String, dataCarga As Date, dataDescarga As Date, horaCarga As String, horaDescarga As String, artigoInsp1 As Boolean, artigoInsp2 As Boolean, Optional ByVal idCabecdoc As String = "") As String

        'ola
        Const COLUMN_ARTIGO As Integer = 0
        Const COLUMN_DESCRICAO As Integer = 1
        Const COLUMN_PRECUNIT As Integer = 2
        Const COLUMN_QUANTIDADE As Integer = 3
        Const COLUMN_DESCONTO As Integer = 4
        Const COLUMN_PROJECTO As Integer = 5


        Dim docC As GcpBEDocumentoCompra
        Dim linhaC As GcpBELinhaDocumentoCompra
        Dim id As String

        Dim arrCampos As ArrayList
        arrCampos = buscarCampos()


        docC = Nothing

        Dim documentoT As String
        documentoT = consultaValor("SELECT BensCirculacao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")



        If idCabecdoc <> "" Then
            ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
            id = idCabecdoc
            docC = bso.Comercial.Compras.EditaID(id)
        End If

        If docC Is Nothing Then
            docC = New GcpBEDocumentoCompra
            docC.Tipodoc = tipodoc
            If serieC_ <> "" Then
                docC.Serie = serieC_
            Else
                docC.Serie = bso.Comercial.Series.DaSerieDefeito("C", tipodoc, data)
            End If

            docC.Entidade = entidade
            docC.TipoEntidade = tipoEntidadeC_
            bso.Comercial.Compras.PreencheDadosRelacionados(docC, PreencheRelacaoCompras.compDadosTodos)
        End If

        If documentoT = "True" And docC.EmModoEdicao Then

            actualizarLinhasDocumento(docC, recordsGrid, arrCampos)
            Return idCabecdoc
        End If

        If Not docC.EmModoEdicao Then
            docC.CamposUtil("CDU_CabVar1").Valor = func
        End If

        docC.Utilizador = func

        docC.DataDoc = CDate(FormatDateTime(data, DateFormat.ShortDate)) 'CDate(data)
        docC.DataVenc = docC.DataDoc
        docC.DataIntroducao = docC.DataDoc

        docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
        docC.Observacoes = observacoes

        docC.Matricula = matricula
        docC.LocalCarga = localCarga
        docC.LocalDescarga = localDescarga
        docC.DataCarga = FormatDateTime(dataCarga, DateFormat.ShortDate)
        docC.HoraCarga = horaCarga

        docC.HoraDescarga = IIf(horaDescarga = "  :", "", horaDescarga)
        docC.DataDescarga = FormatDateTime(dataDescarga, DateFormat.ShortDate)

        docC.TipoEntidadeEntrega = "F"

        docC.Pais = bso.Comercial.Fornecedores.DaValorAtributo(docC.Entidade, "Pais")

        If docC.Pais = "" Then docC.Pais = "PT"


        docC.Nome = fornecedor.Nome
        docC.NumContribuinte = fornecedor.Contribuinte
        docC.Morada = fornecedor.Morada
        docC.Morada2 = fornecedor.Morada2
        docC.Localidade = fornecedor.Localidade
        docC.CodigoPostal = fornecedor.CodPostal
        docC.LocalidadeCodigoPostal = fornecedor.CodPostalLocalidade

        docC.MoradaEntrega = fornecedor.MoradaDescarga
        docC.Morada2Entrega = fornecedor.Morada2Descarga
        docC.LocalidadeEntrega = fornecedor.LocalidadeDescarga
        docC.CodPostalEntrega = fornecedor.CodPostalDescarga
        docC.CodPostalLocalidadeEntrega = fornecedor.CodPostalLocalidadeDescarga



        If documentoT <> "True" Or Not docC.EmModoEdicao Then

            docC.Linhas.RemoveTodos()
            Dim artigo As String
            Dim quantidade As Double
            Dim row As ReportRecord
            Dim unidade As String
            Dim projecto As String
            Dim precunit As Double
            Dim desconto As Double
            Dim descricao As String
            Dim formFormula As FormulaLinha

            For Each row In recordsGrid
                If NuloToString(row(COLUMN_ARTIGO).Value) <> "" Then
                    formFormula = row(COLUMN_ARTIGO).Tag
                    artigo = row(COLUMN_ARTIGO).Value
                    descricao = row(COLUMN_DESCRICAO).Value
                    quantidade = row(COLUMN_QUANTIDADE).Value
                    projecto = NuloToString(row(COLUMN_PROJECTO).Value)
                    unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
                    precunit = row(COLUMN_PRECUNIT).Value
                    desconto = NuloToDouble(row(COLUMN_DESCONTO).Value)
                    bso.Comercial.Compras.AdicionaLinha(docC, artigo, quantidade, , , precunit, desconto)
                    linhaC = docC.Linhas(docC.Linhas.NumItens)
                    linhaC.Quantidade = quantidade
                    linhaC.Descricao = descricao



                    If Not formFormula Is Nothing Then
                        linhaC.VariavelA = formFormula.VarA
                        linhaC.VariavelB = formFormula.VarB
                        linhaC.VariavelC = formFormula.VarC
                        linhaC.QuantFormula = formFormula.QuantidadeFormula
                    End If

                    ' linhaC.PrecUnit = precunit 'bso.Comercial.ArtigosPrecos.Edita(artigo, docC.Moeda, unidade).PVP1
                    If projecto <> "" Then
                        linhaC.IDObra = bso.Comercial.Projectos.DaValorAtributo(projecto, "ID")
                    Else
                        linhaC.IDObra = bso.Comercial.Projectos.DaValorAtributo(molde, "ID")
                    End If

                    If existeColuna("LinhasCompras", campoMolde) Then linhaC.CamposUtil(campoMolde).Valor = projecto
                    If Not formFormula Is Nothing Then
                        If existeColuna("LinhasCompras", campoQuantidadeFormula) Then linhaC.CamposUtil(campoQuantidadeFormula).Valor = formFormula.Numero
                    End If

                    'campos de Utilizador
                    For i = 0 To arrCampos.Count - 1
                        'If existeColuna("LinhasCompras", arrCampos(i)) Then
                        If linhaC.CamposUtil.Existe(arrCampos(i)) Then
                            If linhaC.CamposUtil(arrCampos(i)).Tipo <> EnumTipoCampo.tcDesconhecido Then
                                If linhaC.CamposUtil(arrCampos(i)).Tipo = EnumTipoCampo.tcBit Then
                                    linhaC.CamposUtil(arrCampos(i)).Valor = NuloToBoolean(row(COLUMN_PROJECTO + i + 1).Checked)
                                Else
                                    linhaC.CamposUtil(arrCampos(i)).Valor = NuloToString(row(COLUMN_PROJECTO + i + 1).Value)
                                End If
                            Else
                                linhaC = inserirValorLinha(linhaC, arrCampos(i), NuloToString(row(COLUMN_PROJECTO + i + 1).Value))
                            End If
                        Else
                            linhaC = inserirValorLinha(linhaC, arrCampos(i), NuloToString(row(COLUMN_PROJECTO + i + 1).Value))
                        End If

                        If arrCampos(i) = "CDU_DataRecepcao" Then
                            If Not linhaC.CamposUtil("CDU_DataRecepcao") Is Nothing Then
                                If linhaC.CamposUtil.Existe("CDU_ObraN5") Then
                                    If IsDate(linhaC.CamposUtil("CDU_DataRecepcao").Valor) Then
                                        linhaC.CamposUtil("CDU_ObraN5") = "V"
                                    Else
                                        linhaC.CamposUtil("CDU_ObraN5") = ""
                                    End If

                                End If
                                End If
                        End If

                    Next
                Else
                    bso.Comercial.Compras.AdicionaLinhaEspecial(docC, compTipoLinhaEspecial.compLinha_Comentario)
                    linhaC = docC.Linhas(docC.Linhas.NumItens)
                    linhaC.Descricao = NuloToString(row(COLUMN_DESCRICAO).Value)
                End If
            Next

            If artigoInsp1 Then
                bso.Comercial.Compras.AdicionaLinha(docC, artInsp1)
                linhaC = docC.Linhas(docC.Linhas.NumItens)
                linhaC.Descricao = bso.Comercial.Artigos.DaValorAtributo(artInsp1, "Observacoes")
            End If

            If artigoInsp2 Then
                bso.Comercial.Compras.AdicionaLinha(docC, artInsp2)
                linhaC = docC.Linhas(docC.Linhas.NumItens)
                linhaC.Descricao = bso.Comercial.Artigos.DaValorAtributo(artInsp2, "Observacoes")
            End If

        End If
        Try


            If docC.Linhas.NumItens > 0 Or idCabecdoc <> "" Then

                If validaQuantidadeZero(docC) Then

                    bso.Comercial.Compras.Actualiza(docC)

                    executarComando("UPDATE CabecCompras SET PaisEntrega='" + docC.Pais + "', MoradaCarga='" + fornecedor.MoradaCarga + "',Morada2Carga='" + fornecedor.Morada2Carga + "',LocalidadeCarga='" + fornecedor.LocalidadeCarga + "',CodPostalCarga='" + fornecedor.CodPostalCarga + "',CodPostalLocalidadeCarga='" + fornecedor.CodPostalLocalidadeCarga + "' WHERE Id='" + docC.ID + "'")
                    Dim serie As GcpBESerie
                    Dim erros As String
                    erros = ""
                    serie = bso.Comercial.Series.Edita("C", docC.Tipodoc, docC.Serie)

                    If serie.TipoComunicacao = 2 Then
                        bso.Comercial.Internos.ATComunicaDocumentoId(docC.ID, "C", erros)
                        If erros <> "" Then
                            MsgBox(erros, MsgBoxStyle.Critical)
                        End If
                    End If

                    Return docC.ID
                Else
                    MsgBox("Existe Artigos com Quantidade Zero!", MsgBoxStyle.Critical)
                    Return ""
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Private Sub actualizarLinhasDocumento(ByRef doc As GcpBEDocumentoCompra, recordsGrid As ReportRecords, arrCampos As ArrayList)

        Dim linhaC As GcpBELinhaDocumentoCompra
        Dim row As ReportRecord

        Const COLUMN_ARTIGO As Integer = 0
        Const COLUMN_DESCRICAO As Integer = 1
        Const COLUMN_QUANTIDADE As Integer = 3
        Const COLUMN_PROJECTO As Integer = 4
        Const COLUMN_PRECUNIT As Integer = 2

        Dim dtrow As DataRow


        For Each row In recordsGrid
            dtrow = row.Tag
            linhaC = buscarLinhaById(doc, dtrow("ID").ToString())

            If Not linhaC Is Nothing Then
                linhaC.PrecUnit = row(COLUMN_PRECUNIT).Value
                'campos de Utilizador
                For i = 0 To arrCampos.Count - 1
                    'If existeColuna("LinhasCompras", arrCampos(i)) Then
                    If linhaC.CamposUtil.Existe(arrCampos(i)) Then
                        If linhaC.CamposUtil(arrCampos(i)).Tipo <> EnumTipoCampo.tcDesconhecido Then
                            If linhaC.CamposUtil(arrCampos(i)).Tipo = EnumTipoCampo.tcBit Then
                                linhaC.CamposUtil(arrCampos(i)) = NuloToBoolean(row(COLUMN_PROJECTO + i + 1).Checked)
                            Else
                                linhaC.CamposUtil(arrCampos(i)) = NuloToString(row(COLUMN_PROJECTO + i + 1).Value)
                            End If
                        Else
                            linhaC = inserirValorLinha(linhaC, arrCampos(i), NuloToString(row(COLUMN_PROJECTO + i + 1).Value))
                        End If
                    Else
                        linhaC = inserirValorLinha(linhaC, arrCampos(i), NuloToString(row(COLUMN_PROJECTO + i + 1).Value))
                    End If

                    If arrCampos(i) = "CDU_DataRecepcao" Then
                        If Not linhaC.CamposUtil("CDU_DataRecepcao") Is Nothing Then
                            If linhaC.CamposUtil.Existe("CDU_ObraN5") Then
                                linhaC.CamposUtil("CDU_ObraN5") = "V"
                            End If
                        End If
                    End If

                Next
            End If
        Next
        Try
            bso.Comercial.Compras.Actualiza(doc)
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical, "Erro")
        End Try

    End Sub


    Private Function buscarLinhaById(doc As GcpBEDocumentoCompra, id As String) As GcpBELinhaDocumentoCompra
        Dim linhaC As GcpBELinhaDocumentoCompra
        For Each linhaC In doc.Linhas
            If UCase(linhaC.IdLinha) = "{" + UCase(id) + "}" Then
                Return linhaC
            End If
        Next
        Return Nothing
    End Function
    Private Function inserirValorLinha(linha As GcpBELinhaDocumentoCompra, campo As String, valor As String) As GcpBELinhaDocumentoCompra
        campo = UCase(campo)
        Select Case campo
            Case "DATAENTREGA" : If IsDate(valor) Then linha.DataEntrega = valor
        End Select
        Return linha
    End Function

    Private Function inserirValorLinha(linha As GcpBELinhaDocumentoVenda, campo As String, valor As String) As GcpBELinhaDocumentoVenda
        campo = UCase(campo)
        Select Case campo
            Case "DATAENTREGA" : linha.DataEntrega = valor
        End Select
        Return linha
    End Function

    Private Function existeColuna(tabela As String, campo As String) As Boolean
        Return consultaValor("select * from syscolumns where name like '" + campo + "' and id=OBJECT_ID('" + tabela + "')") <> ""
    End Function
    Public Function actualizarDocumentoVenda1(cliente As Entidade, ByVal tipodoc As String, ByVal entidade As String, ByVal func As String, ByVal molde As String, observacoes As String, ByVal data As Date, ByVal recordsGrid As ReportRecords, matricula As String, localCarga As String, localDescarga As String, dataCarga As Date, dataDescarga As Date, horaCarga As String, horaDescarga As String, artigoInsp1 As Boolean, artigoInsp2 As Boolean, Optional ByVal idCabecdoc As String = "") As String
        Dim docV As GcpBEDocumentoVenda
        docV = Nothing
        ' Dim dataI As String
        'dataI = getDateDocument(data)
        Dim ultimoNumDoc As Long
        Dim ultimoNumDocBase As Long
        ultimoNumDoc = -1
        ultimoNumDocBase = -1


        If docV Is Nothing Then
            docV = New GcpBEDocumentoVenda
            docV.Tipodoc = tipoDocP_
            If serieP_ <> "" Then
                docV.Serie = serieP_
            Else
                docV.Serie = bso.Comercial.Series.DaSerieDefeito("V", tipoDocP_, data)
            End If
        End If

        bso.Comercial.Vendas.AdicionaLinha(docV, "72110106", 1)

    End Function
    Public Function actualizarDocumentoVenda(cliente As Entidade, ByVal tipodoc As String, ByVal entidade As String, ByVal func As String, ByVal molde As String, observacoes As String, ByVal data As Date, ByVal recordsGrid As ReportRecords, matricula As String, localCarga As String, localDescarga As String, dataCarga As Date, dataDescarga As Date, horaCarga As String, horaDescarga As String, artigoInsp1 As Boolean, artigoInsp2 As Boolean, Optional ByVal idCabecdoc As String = "") As String

        'ola
        Const COLUMN_ARTIGO As Integer = 0
        Const COLUMN_DESCRICAO As Integer = 1
        Const COLUMN_QUANTIDADE As Integer = 3
        Const COLUMN_PROJECTO As Integer = 5
        Const COLUMN_DESCONTO As Integer = 4
        Const COLUMN_PRECUNIT As Integer = 2

        Dim docV As GcpBEDocumentoVenda
        Dim linhaV As GcpBELinhaDocumentoVenda

        Dim desconto As Double
        desconto = 0

        Dim id As String

        docV = Nothing


        If idCabecdoc <> "" Then
            ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
            id = idCabecdoc
            docV = bso.Comercial.Vendas.EditaID(id)
        End If

        If docV Is Nothing Then
            docV = New GcpBEDocumentoVenda

            docV.Tipodoc = tipodoc
            If serieC_ <> "" Then
                docV.Serie = serieV_
            Else
                docV.Serie = bso.Comercial.Series.DaSerieDefeito("V", tipodoc, data)
            End If

            docV.Entidade = entidade
            docV.TipoEntidade = tipoEntidadeV_
            'bso.Comercial.Vendas.PreencheDadosRelacionados(docV)
            bso.Comercial.Vendas.PreencheDadosRelacionados(docV, PreencheRelacaoVendas.vdDadosTodos)
        End If


        'docV.CamposUtil("CDU_CabVar1").Valor = func
        docV.DataDoc = CDate(FormatDateTime(data, DateFormat.ShortDate))
        'docV.DataDoc = CDate(data)

        docV.DataVenc = docV.DataDoc

        'docV.Requisicao = docV.Tipodoc + " / " + docV.Serie + " / " + CStr(docV.NumDoc)
        'docV.Referencia = docV.Tipodoc + " / " + docV.Serie + " / " + CStr(docV.NumDoc)
        docV.Observacoes = observacoes
        docV.Matricula = matricula
        docV.LocalCarga = localCarga
        docV.LocalDescarga = localDescarga
        docV.DataCarga = FormatDateTime(dataCarga, DateFormat.ShortDate)
        'docV.DataDescarga = FormatDateTime(dataDescarga, DateFormat.ShortDate)
        docV.HoraCarga = horaCarga
        docV.HoraDescarga = IIf(horaDescarga = "  :", "", horaDescarga)
        docV.DataDescarga = FormatDateTime(dataDescarga, DateFormat.ShortDate)

        docV.Nome = cliente.Nome
        docV.NumContribuinte = cliente.Contribuinte
        docV.Morada = cliente.Morada
        docV.Morada2 = cliente.Morada2
        docV.Localidade = cliente.Localidade
        docV.CodigoPostal = cliente.CodPostal
        docV.LocalidadeCodigoPostal = cliente.CodPostalLocalidade

        docV.NomeFac = cliente.Nome
        docV.NumContribuinteFac = cliente.Contribuinte
        docV.MoradaFac = cliente.Morada
        docV.Morada2Fac = cliente.Morada2
        docV.LocalidadeFac = cliente.Localidade
        docV.CodigoPostalFac = cliente.CodPostal
        docV.LocalidadeCodigoPostalFac = cliente.CodPostalLocalidade

        docV.CamposUtil("CDU_CabVar1").Valor = func


        docV.MoradaEntrega = cliente.MoradaDescarga
        docV.Morada2Entrega = cliente.Morada2Descarga
        docV.LocalidadeEntrega = cliente.LocalidadeDescarga
        docV.CodPostalEntrega = cliente.CodPostalDescarga
        docV.CodPostalLocalidadeEntrega = cliente.CodPostalLocalidadeDescarga

        docV.TipoEntidadeEntrega = "C"

        If docV.Pais = "" Then docV.Pais = "PT"
        If docV.PaisFac = "" Then docV.PaisFac = "PT"


        ' bso.Comercial.Vendas.AdicionaLinha(docV, "72110106", 1)
        Dim documentoT As String
        documentoT = consultaValor("SELECT BensCirculacao FROM DocumentosVenda WHERE Documento='" + tipodoc + "'")

        If descontoLinhas Then
            desconto = docV.DescEntidade
            docV.DescEntidade = 0
            docV.DescFinanceiro = 0
        End If


        If documentoT <> "True" Or Not docV.EmModoEdicao Then

            docV.Linhas.RemoveTodos()
            Dim artigo As String
            Dim quantidade As Double
            Dim row As ReportRecord
            Dim unidade As String
            Dim projecto As String
            Dim precunit As Double
            Dim descricao As String

            For Each row In recordsGrid
                artigo = row(COLUMN_ARTIGO).Value
                descricao = row(COLUMN_DESCRICAO).Value
                quantidade = row(COLUMN_QUANTIDADE).Value
                projecto = NuloToString(row(COLUMN_PROJECTO).Value)
                unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
                precunit = row(COLUMN_PRECUNIT).Value
                bso.Comercial.Vendas.AdicionaLinha(docV, artigo)
                'bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                linhaV.Quantidade = quantidade
                linhaV.Descricao = descricao
                linhaV.PrecUnit = precunit
                linhaV.Desconto1 = NuloToDouble(row(COLUMN_DESCONTO).Value)

                If linhaV.Desconto1 = 0 And Not artigoSemDesconto(linhaV.Artigo) Then
                    linhaV.Desconto1 = desconto
                End If

                'linhaC.PrecUnit = precunit 'bso.Comercial.ArtigosPrecos.Edita(artigo, docC.Moeda, unidade).PVP1
                If projecto <> "" Then
                    linhaV.IDObra = bso.Comercial.Projectos.DaValorAtributo(projecto, "ID")
                Else
                    linhaV.IDObra = bso.Comercial.Projectos.DaValorAtributo(molde, "ID")
                End If

                If existeColuna("LinhasDoc", campoMolde) Then linhaV.CamposUtil(campoMolde).Valor = projecto
                'linhaV.CamposUtil("CDU_Linvar1").Valor = projecto
            Next


            If artigoInsp1 Then
                bso.Comercial.Vendas.AdicionaLinha(docV, artInsp1)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                linhaV.Descricao = bso.Comercial.Artigos.DaValorAtributo(artInsp1, "Observacoes")
            End If

            If artigoInsp2 Then
                bso.Comercial.Vendas.AdicionaLinha(docV, artInsp2)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                linhaV.Descricao = bso.Comercial.Artigos.DaValorAtributo(artInsp2, "Observacoes")
            End If

        End If
        Try
            If docV.Linhas.NumItens > 0 Or idCabecdoc <> "" Then

                If validaQuantidadeZero(docV) Then
                    bso.Comercial.Vendas.Actualiza(docV)
                    executarComando("UPDATE CabecDoc SET MoradaCarga='" + cliente.MoradaCarga + "',Morada2Carga='" + cliente.Morada2Carga + "',LocalidadeCarga='" + cliente.LocalidadeCarga + "',CodPostalCarga='" + cliente.CodPostalCarga + "',CodPostalLocalidadeCarga='" + cliente.CodPostalLocalidadeCarga + "' WHERE Id='" + docV.ID + "'")
                    Dim serie As GcpBESerie
                    Dim erros As String
                    erros = ""
                    serie = bso.Comercial.Series.Edita("V", docV.Tipodoc, docV.Serie)
                    If serie.TipoComunicacao = 2 Then
                        bso.Comercial.Internos.ATComunicaDocumentoId(docV.ID, "V", erros)
                        If erros <> "" Then
                            MsgBox(erros, MsgBoxStyle.Critical)
                        End If
                    End If

                    verificarComposicaoArtigos(ficheiroIni, docV)

                    Return docV.ID

                Else
                    MsgBox("Existem Artigos com Quantidade Zero!", MsgBoxStyle.Critical)
                    Return ""
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function
    Public Function artigoSemDesconto(artigo As String) As Boolean
        artigoSemDesconto = True
        Try
            artigoSemDesconto = bso.Comercial.Artigos.DaValorAtributo(artigo, "CDU_ArtigoSemDesconto")
        Catch ex As Exception
            artigoSemDesconto = False
        End Try

    End Function

    Private Function validaQuantidadeZero(doc As GcpBEDocumentoVenda) As Boolean
        Dim valida As Boolean
        Dim i As Long

        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        valida = ficheiroIni.GetBoolean("DOCUMENTO", "VALIDAZERO", False)

        ' validaQuantidadeZero = True
        If valida Then
            For i = 1 To doc.Linhas.NumItens
                If doc.Linhas(i).Artigo <> "" And doc.Linhas(i).Quantidade = 0 Then
                    Return False
                End If
            Next
            Return True
        Else
            Return True
        End If

    End Function


    Private Function validaQuantidadeZero(doc As GcpBEDocumentoCompra) As Boolean
        Dim valida As Boolean
        Dim i As Long

        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        valida = ficheiroIni.GetBoolean("DOCUMENTO", "VALIDAZERO", False)

        '  validaQuantidadeZero = True
        If valida Then
            For i = 1 To doc.Linhas.NumItens
                If doc.Linhas(i).Artigo <> "" And doc.Linhas(i).Quantidade = 0 Then
                    Return False
                End If
            Next
            Return True
        Else
            Return True
        End If

    End Function

    Public Function artigoValidoEnc(ByVal artigo As String, descricao As String)
        If InStr(descricao, artigoInvalido) > 0 Then
            Return False
        End If
        Return True
    End Function

    Public Function artigoValido(ByVal artigo As String, descricao As String, ByVal moduloStocks As Boolean) As Boolean

        Dim tipoArtigo As String
        Dim intTipoDoc As Integer
        Dim movStock As String

        Dim tipoArtPer As GcpBeTipoArtigoPermissao

        If InStr(descricao, artigoInvalido) > 0 Then
            Return False
        End If

        movStock = bso.Comercial.Artigos.DaValorAtributo(artigo, "MovStock")

        If movStock = "N" Then Return False

        tipoArtigo = bso.Comercial.Artigos.DaValorAtributo(artigo, "TipoArtigo")
        If moduloStocks Then
            intTipoDoc = bso.Comercial.TabInternos.DaValorAtributo(tipoDocS_, "TipoDocumento")
        Else
            intTipoDoc = bso.Comercial.TabInternos.DaValorAtributo(tipoDocP_, "TipoDocumento")
        End If
        tipoArtPer = bso.Comercial.TiposArtigosPermissoes.Edita(tipoArtigo, "N", intTipoDoc)

        Return tipoArtPer.Permitido


    End Function


    Public Function daDataDocumento(ByVal id As String, ByVal dataDefaut As Date, ByVal modulo As String) As Date
        If modulo = "I" Then
            If bso.Comercial.Internos.ExisteID(id) Then
                Return bso.Comercial.Internos.DaValorAtributoID(id, "Data")
            Else
                Return dataDefaut
            End If
        End If

        If modulo = "C" Then
            If bso.Comercial.Compras.ExisteID(id) Then
                Return bso.Comercial.Compras.DaValorAtributoID(id, "DataDoc")
            Else
                Return dataDefaut
            End If
        End If

    End Function

    Public Sub daEntidadeDocumento(ByVal id As String, ByRef entidade As String, ByRef nome As String, ByRef observacoes As String, ByVal modulo As String)
        If modulo = "I" Then
            If bso.Comercial.Internos.ExisteID(id) Then
                entidade = bso.Comercial.Internos.DaValorAtributoID(id, "Entidade")
                nome = bso.Comercial.Internos.DaValorAtributoID(id, "Nome")
                observacoes = bso.Comercial.Internos.DaValorAtributoID(id, "Observacoes")
            End If
        End If
        If modulo = "C" Then
            If bso.Comercial.Compras.ExisteID(id) Then
                entidade = bso.Comercial.Compras.DaValorAtributoID(id, "Entidade")
                nome = bso.Comercial.Compras.DaValorAtributoID(id, "Nome")
                observacoes = bso.Comercial.Compras.DaValorAtributoID(id, "Observacoes")
            End If
        End If

    End Sub

    Public Function daObservacoesDocumento(ByVal id As String, ByVal modulo As String) As String
        If modulo = "I" Then
            If bso.Comercial.Internos.ExisteID(id) Then
                Return bso.Comercial.Internos.DaValorAtributoID(id, "Observacoes")
            End If
        End If
        If modulo = "C" Then
            If bso.Comercial.Compras.ExisteID(id) Then
                Return bso.Comercial.Compras.DaValorAtributoID(id, "Observacoes")
            End If
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Função que permite ir buscar o numero de Kg de uma determinada máquina
    ''' </summary>
    ''' <param name="centroTrabalho"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daNumeroKgCT(ByVal centroTrabalho As String) As String

        Dim kg As String

        kg = bso.Comercial.Projectos.DaValorAtributo(centroTrabalho, "CDU_KgLimpeza")

        If IsNumeric(kg) Then
            Return kg
        Else
            Return "0"
        End If
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daExtruturaDataTableArtigos() As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = consulta("SELECT Artigo,ArmazemSugestao as Armazem,Descricao,0.0 as StkActual,0.0 as Quantidade FROM APS_GP_Artigos WHERE 1=0")
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daStockActual(ByVal view As String, ByVal artigo As String) As String
        Return consultaValor("Select StkActual FROM " + view + " WHERE Artigo='" + artigo + "'")
    End Function

    Private Sub removerDocumentoInterno(ByVal idCabecDoc As String)
        If idCabecDoc <> "" Then
            If bso.Comercial.Internos.ExisteID(idCabecDoc) Then
                executarComando("DELETE FROM  LinhasStk WHERE IdCabecOrig='" + idCabecDoc + "'")
                bso.Comercial.Internos.RemoveID(idCabecDoc)
            End If
        End If

    End Sub

    Public Sub removerDocumentoVenda(ByVal idCabecDoc As String)
        If idCabecDoc <> "" Then
            If bso.Comercial.Vendas.ExisteID(idCabecDoc) Then
                Dim doc As GcpBEDocumentoVenda
                doc = bso.Comercial.Vendas.EditaID(idCabecDoc)
                executarComando("DELETE FROM  LinhasStk WHERE IdCabecOrig='" + idCabecDoc + "'")
                'Alteracao dia 14-10-2015
                doc.Linhas.RemoveTodos()
                bso.Comercial.Vendas.Actualiza(doc)
                'Versão anterior
                'bso.Comercial.Vendas.Remove(doc.Filial, doc.Tipodoc, doc.Serie, doc.NumDoc)
            End If
        End If

    End Sub

    Public Sub removerDocumentoStock(ByVal idCabecStock As String)
        If idCabecStock <> "" Then
            If bso.Comercial.Stocks.ExisteID(idCabecStock) Then
                Dim lista As StdBELista
                lista = bso.Consulta("SELECT * FROM CabecStk Where id='" + idCabecStock + "'")
                'executarComando("DELETE FROM  LinhasStk WHERE IdCabecOrig='" + idCabecDoc + "'")
                bso.Comercial.Stocks.Remove(lista.Valor("Filial"), "S", lista.Valor("Tipodoc"), lista.Valor("Serie"), lista.Valor("NumDoc"))
            End If
        End If

    End Sub



    Public Function daObjectoRegisto(ByVal posto As String, ByVal entidade As String, ByVal operacao As String, ByVal molde As String, ByVal requisicao As String, ByVal numeroKg As Double, ByVal dtUMR As DataTable, ByVal dtUMC As DataTable, Optional ByVal idRegisto As String = "") As Registo
        Dim lista As StdBELista
        Dim registo As Registo

        registo = New Registo

        If idRegisto = "" Then
            lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Posto='" + posto + "' AND CDU_Operacao='" + operacao + "' AND CDU_Entidade='" + entidade + "' and CDU_TipoEntidade='" + tipoEntidadeP_ + "' and CDU_IdCabec IS NULL")
        Else
            lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_ID='" + idRegisto + "'")
        End If

        registo.ArmazemDefault = ArmazemDefault
        registo.ID = idRegisto
        If Not lista.NoFim Then
            registo.ID = lista.Valor("CDU_Id")
            registo.DataInicial = NuloToDate(lista.Valor("CDU_DataInicial"), Now)
            registo.DataFinal = NuloToDate(lista.Valor("CDU_DataFinal"), Now)
            registo.TempoDescontado = NuloToDate(lista.Valor("CDU_TempoDescontado"), CDate(CStr(Date.Today) + " 00:00:00"))
            If CStr(lista.Valor("CDU_Tempo")) <> "" Then
                registo.Tempo = NuloToDouble(lista.Valor("CDU_Tempo"))
            End If
            registo.Funcionario = lista.Valor("CDU_Funcionario")
            registo.Observacoes = lista.Valor("CDU_Observacoes")
            registo.Problema = NuloToString(lista.Valor("CDU_Problema"))
        Else
            registo.Funcionario = funcionario_
        End If

        If registo.DataInicial Is Nothing Then
            registo.DataInicial = DateAdd(DateInterval.Minute, -15, Now)
        End If
        If registo.DataFinal Is Nothing Then
            registo.DataFinal = Now
        End If

        If registo.TempoDescontado Is Nothing Then
            registo.TempoDescontado = CDate(CStr(Date.Today) + " 00:00:00")
        End If


        registo.Entidade = entidade
        registo.TipoEntidade = tipoEntidadeP_
        registo.Posto = posto
        registo.Operacao = operacao
        registo.Molde = molde
        registo.Requisicao = requisicao

        If registo.Tempo Is Nothing Then
            registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(posto), calculaTempoMaoObra(registo.DataInicial, registo.DataFinal))
        Else
            registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(posto), registo.Tempo)
        End If

        registo.adicionaRegistoMaterialLimpeza(daArtigoMLProjecto(posto), numeroKg)

        registo.adicionaLinhasMaterialRTJ(dtUMR)
        registo.adicionaLinhasMaterialCliente(dtUMC)

        Return registo
    End Function

    Public Function calculaTempoMaoObra(ByVal dataInicial As Date, ByVal dataFinal As Date) As Double

        Dim data As Date



        data = CDate(dataFinal)
        Dim minutos As Double

        minutos = DateDiff(DateInterval.Minute, dataInicial, dataFinal)

        'minutos = minutos / 15
        'minutos = minutos + 0.5
        'minutos = Math.Round(minutos, 0)
        'If minutos = 0 Then minutos = 1

        'minutos = minutos * 15

        'calculaTempoMaoObra = (minutos / 60)
        minutos = minutos / 15
        If minutos > Math.Round(minutos) Then
            minutos = Math.Round(minutos) + 1
        Else
            minutos = Math.Round(minutos)
        End If

        minutos = minutos * 15
        calculaTempoMaoObra = (minutos / 60)

    End Function


    Public Function VerficarFuncionarioAdm(ByVal funcionario As String, modulo As String) As Boolean
        Dim lista As StdBELista
        Dim adm As Boolean
        Dim campo As String = ""
        Try
            lista = bso.Consulta("SELECT ISNULL(CDU_Administrador,0) FROM APS_GP_Funcionarios WHERE Codigo='" + funcionario + "'")
            If lista.NoFim Then
                Return False
            Else
                adm = CBool(lista.Valor(0))

                If adm Then
                    Return adm
                Else

                    Select Case modulo
                        Case "RP" : campo = "CDU_AdministradorRP"
                    End Select

                    Select Case modulo
                        Case "LM" : campo = "CDU_AdministradorLM"
                    End Select

                    If campo = "" Then
                        Return False
                    End If

                    lista = bso.Consulta("SELECT ISNULL(" + campo + ",0) FROM APS_GP_Funcionarios WHERE Codigo='" + funcionario + "'")

                    If Not lista.NoFim Then
                        Return CBool(lista.Valor(0))
                    End If
                End If
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function


    ''' <summary>
    ''' Função que permite verificar se o cliente existe
    ''' </summary>
    ''' <param name="cliente"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeCliente(ByVal cliente As String) As Boolean
        Return bso.Comercial.Clientes.Existe(cliente)
    End Function

    ''' <summary>
    ''' Função que permite verificar se o fornecedor existe
    ''' </summary>
    ''' <param name="fornecedor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeFornecedor(ByVal fornecedor As String) As Boolean
        Return bso.Comercial.Fornecedores.Existe(fornecedor)
    End Function

    Public Sub enviarEmailDocumento(ByVal idDocumento As String, ByVal modulo As String)
        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(idDocumento)
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, doc.Moeda, 2)
            Dim str As String
            Dim mapa As Mapa
            mapa = New Mapa
            Dim gcpSerie As GcpBESerie
            Dim tabCompras As GcpBETabCompra
            Dim tempFolder As String
            tempFolder = System.IO.Path.GetTempPath + "GCPMailTemp\Document.pdf"
            If Not Directory.Exists(System.IO.Path.GetTempPath + "GCPMailTemp") Then Directory.CreateDirectory(System.IO.Path.GetTempPath + "GCPMailTemp")
            tabCompras = bso.Comercial.TabCompras.Edita(doc.Tipodoc)
            gcpSerie = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
            mapa.iniciarComponente(gcpSerie.Config, codEmpresa, objConfApl.Instancia, utilizador, password, PlataformaPrimavera)
            mapa.Inicializar()
            str = "{CabecCompras.Filial}='" & doc.Filial & "' And {CabecCompras.Serie}='" & doc.Serie & "' And {CabecCompras.TipoDoc}='" & doc.Tipodoc & "' and {CabecCompras.NumDoc}=" & CStr(doc.NumDoc)
            mapa.selectionFormula(str)
            mapa.setFormulaFieldsByName("DadosEmpresa", rp.getFormula())

            Dim certificado As String
            certificado = bso.Comercial.Compras.DevolveTextoAssinaturaDocID(doc.ID)

            mapa.setFormulaFieldsByName("InicializaParametros", rp.getFormulaIniciarParametros(doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado)))
            mapa.exportarDiscoPDF(tempFolder)
            PlataformaPrimavera.Mail.Inicializa()

            Dim emailDestino As String
            Dim profile As String
            profile = ""
            emailDestino = ""
            If PlataformaPrimavera.PrefUtilStd.EmailMAPI Then
                profile = PlataformaPrimavera.PrefUtilStd.EmailMAPIProfile
            Else
                ' PlataformaPrimavera.Mail.SMTPServer = PlataformaPrimavera.PrefUtilStd.EmailServSMTP
                '  PlataformaPrimavera.Mail.EnderecoLocal = PlataformaPrimavera.PrefUtilStd.EmailEndereco
            End If

            If tabCompras.EmailFixo Then
                emailDestino = tabCompras.EmailTo
            Else
                If tabCompras.EmailTo <> "" Then
                    emailDestino = consultaValor("Select Email FROM LinhasContactoEntidades WHERE TipoEntidade='" + doc.TipoEntidade + "' and Entidade='" + doc.Entidade + "' and TipoContacto='" + tabCompras.EmailTo + "'")
                End If
            End If

            'If tabCompras.EnviaEmail Then
            '  If emailDestino <> "" Then
            Try
                '     PlataformaPrimavera.Mail.MostraJanela = 1
                '    PlataformaPrimavera.Mail.EnviaMail(profile, emailDestino, tabCompras.EmailCC, tabCompras.EmailCC, doc.Tipodoc + " " + CStr(doc.NumDoc) + "/" + doc.Serie, tabCompras.EMailTexto, tempFolder, tabCompras.EmailVisualizar, Not PlataformaPrimavera.PrefUtilStd.EmailMAPI)
                PlataformaPrimavera.Mail.Inicializa()

                PlataformaPrimavera.Mail.EnviaMailEx(emailDestino, tabCompras.EmailCC, PlataformaPrimavera.PrefUtilStd.EmailEndereco, doc.Tipodoc + " " + CStr(doc.NumDoc) + "/" + doc.Serie, tabCompras.EMailTexto, tempFolder, tabCompras.EmailVisualizar)
                MsgBox("Email enviado com sucesso", MsgBoxStyle.Information)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            'End If

            'Else
            ' MsgBox("O Email da entidade " + doc.Entidade + " não se encontra configurado", MsgBoxStyle.Critical)
            '  End If


        End If


    End Sub


    Public Function daNumeracaoDocumentoVenda(ByVal documentoVenda As String) As Integer
        Dim serie As String
        daNumeracaoDocumentoVenda = -1
        If documentoVenda <> "" Then
            If serieV_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("V", documentoVenda)
            Else
                serie = serieV_
            End If
            Return bso.Comercial.Series.ProximoNumero("V", documentoVenda, serie)
        End If
    End Function

    Public Function daValorNumeracaoMinimaDocumentoVenda(ByVal documentoVenda As String) As Integer
        Dim serie As String

        daValorNumeracaoMinimaDocumentoVenda = -1
        If documentoVenda <> "" Then
            If serieV_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("V", documentoVenda)
            Else
                serie = serieV_
            End If
            Return bso.Comercial.Series.DaValorAtributo("V", documentoVenda, serie, "LimiteInferior")
        End If
    End Function

    Public Function daValorNumeracaoMaximaDocumentoVenda(ByVal documentoVenda As String) As Integer
        Dim serie As String

        daValorNumeracaoMaximaDocumentoVenda = -1
        If documentoVenda <> "" Then
            If serieV_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("V", documentoVenda)
            Else
                serie = serieV_
            End If
            Return bso.Comercial.Series.DaValorAtributo("V", documentoVenda, serie, "LimiteSuperior")
        End If
    End Function

    Public Function daNumeracaoDocumentoCompra(ByVal documentoCompra As String) As Integer
        Dim serie As String

        daNumeracaoDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            Return bso.Comercial.Series.ProximoNumero("C", documentoCompra, serie)
        End If
    End Function

    Public Function daValorNumeracaoMinimaDocumentoCompra(ByVal documentoCompra As String) As Integer
        Dim serie As String

        daValorNumeracaoMinimaDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            Return bso.Comercial.Series.DaValorAtributo("C", documentoCompra, serie, "LimiteInferior")
        End If
    End Function

    Public Function daValorNumeracaoMaximaDocumentoCompra(ByVal documentoCompra As String) As Integer
        Dim serie As String
        daValorNumeracaoMaximaDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            Return bso.Comercial.Series.DaValorAtributo("C", documentoCompra, serie, "LimiteSuperior")
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        Dim serie As String

        existeDocumentoCompra = ""
        Dim idProj As String
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            If bso.Comercial.Compras.Existe("000", documentoCompra, serie, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serie, "000", "Id")
                idProj = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serie, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoVenda(ByVal documentoVenda As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        Dim serie As String

        existeDocumentoVenda = ""

        Dim idProj As String
        If documentoVenda <> "" Then
            If serieV_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("v", documentoVenda)
            Else
                serie = serieV_
            End If
            If bso.Comercial.Vendas.Existe("000", documentoVenda, serie, numdoc) Then
                existeDocumentoVenda = bso.Comercial.Vendas.DaValorAtributo("000", documentoVenda, serie, numdoc, "Id")
                idProj = bso.Comercial.Vendas.DaValorAtributo("000", documentoVenda, serie, numdoc, "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function

    Public Sub imprimirDocumento(ByVal idDocumento As String, ByVal modulo As String, ByVal owner As System.Windows.Forms.IWin32Window)
        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(idDocumento)
            imprimir(doc, owner)
            doc = Nothing
        End If
        If modulo = "V" Then
            Dim doc As GcpBEDocumentoVenda
            doc = bso.Comercial.Vendas.EditaID(idDocumento)
            imprimir(doc, owner)
            doc = Nothing
        End If
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoVenda, ByVal owner As System.Windows.Forms.IWin32Window)

        Dim se As GcpBESerie

        se = bso.Comercial.Series.Edita("V", doc.Tipodoc, doc.Serie)
        Dim numVia As String
        For i = 1 To se.NumVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            If se.Config <> "" Then
                Dim certificado As String
                certificado = bso.Comercial.Vendas.DevolveTextoAssinaturaDocID(doc.ID)
                imprimirMapa(se.Config, IIf(se.Previsao, "W", "P"), "{CabecDoc.Filial}='" + doc.Filial + "' AND {CabecDoc.Serie}='" + doc.Serie + "' AND {CabecDoc.TipoDoc}='" + doc.Tipodoc + "' AND {CabecDoc.Numdoc}=" + CStr(doc.NumDoc), numVia, doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado), owner)
            Else
                MsgBox("O Documento " + doc.Tipodoc + " não possui nenhum mapa configurado!", MsgBoxStyle.Exclamation, "Contactar Anphis")
            End If
        Next i
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoCompra, ByVal owner As System.Windows.Forms.IWin32Window)

        Dim se As GcpBESerie

        se = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
        Dim numVia As String
        For i = 1 To se.NumVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            If se.Config <> "" Then
                Dim certificado As String
                certificado = bso.Comercial.Compras.DevolveTextoAssinaturaDocID(doc.ID)
                imprimirMapa(se.Config, IIf(se.Previsao, "W", "P"), "{CabecCompras.Filial}='" + doc.Filial + "' AND {CabecCompras.Serie}='" + doc.Serie + "' AND {CabecCompras.TipoDoc}='" + doc.Tipodoc + "' AND {CabecCompras.Numdoc}=" + CStr(doc.NumDoc), numVia, doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado), owner)
            Else
                MsgBox("O Documento " + doc.Tipodoc + " não possui nenhum mapa configurado!", MsgBoxStyle.Exclamation, "Contactar Anphis")
            End If
        Next i
    End Sub
    Private Sub imprimirMapa(ByVal report As String, ByVal destiny As String, ByVal selectionFormula As String, ByVal numVia As String, moeda As String, textoCertificacao As String, ByVal owner As System.Windows.Forms.IWin32Window)
        Try

            Dim p As New System.Windows.Forms.PrintDialog
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, moeda, 2)
            '  Dim mapa As Mapa

            ' mapa = New Mapa()
            ' mapa.iniciarComponente(report, codEmpresa, objConfApl.Instancia, objConfApl.Utilizador, objConfApl.PwdUtilizador, PlataformaPrimavera)
            'buscar o nome da impressora predefinida
            'setDefaultPrinter(p.PrinterSettings.PrinterName)
            PlataformaPrimavera.Mapas.Inicializar("GCP")
            PlataformaPrimavera.Mapas.SelectionFormula = selectionFormula
            PlataformaPrimavera.Mapas.AddFormula("NumVia", numVia)
            PlataformaPrimavera.Mapas.AddFormula("DadosEmpresa", rp.getFormula)
            PlataformaPrimavera.Mapas.AddFormula("InicializaParametros", rp.getFormulaIniciarParametros(moeda, textoCertificacao))
            PlataformaPrimavera.Mapas.DefinicaoImpressoraEx2(p.PrinterSettings.PrinterName, "winspool", "winspool", "", CRPEOrientacaoFolha.ofPortrait, 1, CRPETipoFolha.tfA4, 23, 23, 1, p.PrinterSettings.Duplex, 1, 1, 0)
            PlataformaPrimavera.Mapas.ImprimeListagem(report, "Mapa", destiny, 1, "S", , , , , , True)
            'PlataformaPrimavera.Mapas.ImprimeListagem(sReport:=report, sTitulo:="Mapa", sDestino:=destiny, iNumCopias:=1, sDocumento:="S", blnModal:=True)
            PlataformaPrimavera.Mapas.TerminaJanelas()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function setDefaultPrinter(ByVal strPrinterName As String) As Boolean
        Dim strOldPrinter As String

        Dim WshNetwork As Object

        Dim pd As New System.Drawing.Printing.PrintDocument

        Try

            strOldPrinter = pd.PrinterSettings.PrinterName

            WshNetwork = Microsoft.VisualBasic.CreateObject("WScript.Network")

            WshNetwork.SetDefaultPrinter(strPrinterName)

            pd.PrinterSettings.PrinterName = strPrinterName

            If pd.PrinterSettings.IsValid Then

                Return True

            Else

                WshNetwork.SetDefaultPrinter(strOldPrinter)

                Return False

            End If

        Catch exptd As Exception

            WshNetwork.SetDefaultPrinter(strOldPrinter)

            Return False

        Finally

            WshNetwork = Nothing

            pd = Nothing

        End Try

    End Function

    Public Sub gravarDocumento(ByVal idRegisto As String)
        Dim reg As Registo
        reg = daObjectoRegisto(idRegisto)
        Dim idCabecDoc As String
        If getMOLDULOP() = "I" Then
            idCabecDoc = actualizarDocumentoInterno(reg, reg.DataInicial)
        Else
            idCabecDoc = actualizarDocumentoVenda(reg, reg.DataInicial)
        End If

        If idCabecDoc <> "" Then
            executarComando("UPDATE TDU_APSCabecRegistoFolhaPonto set CDU_IdCabec='" + idCabecDoc + "' WHERE CDU_ID='" + idRegisto + "' ")

            Dim linhaR As LinhaRegisto
            For Each linhaR In reg.LinhasRegistos
                If Not linhaR Is Nothing Then
                    executarComando("UPDATE TDU_APSLinhasRegistoFolhaPonto set CDU_IdCabecInternos='" + idCabecDoc + "',CDU_IdLinhaInternos='" + linhaR.CDU_IDLinhaInterno + "' WHERE CDU_ID='" + linhaR.ID + "' ")
                End If
            Next
        End If
    End Sub

    Public Function daObjectoRegisto(ByVal idRegisto As String) As Registo
        Dim lista As StdBELista
        Dim registo As Registo

        registo = New Registo

        lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_ID='" + idRegisto + "'")

        registo.ArmazemDefault = ArmazemDefault
        registo.ID = idRegisto
        If Not lista.NoFim Then
            registo.ID = lista.Valor("CDU_Id")
            registo.DataInicial = NuloToDate(lista.Valor("CDU_DataInicial"), Now)
            registo.DataFinal = NuloToDate(lista.Valor("CDU_DataFinal"), Now)
            registo.TempoDescontado = NuloToDate(lista.Valor("CDU_TempoDescontado"), CDate(CStr(Date.Today) + " 00:00:00"))
            If CStr(lista.Valor("CDU_Tempo")) <> "" Then
                registo.Tempo = NuloToDouble(lista.Valor("CDU_Tempo"))
            End If
            registo.Funcionario = lista.Valor("CDU_Funcionario")
            registo.Observacoes = lista.Valor("CDU_Observacoes")
            registo.Problema = lista.Valor("CDU_Problema")
        Else
            registo.Funcionario = funcionario_
        End If

        If registo.DataInicial Is Nothing Then
            registo.DataInicial = DateAdd(DateInterval.Minute, -15, Now)
        End If
        If registo.DataFinal Is Nothing Then
            registo.DataFinal = Now
        End If

        If registo.TempoDescontado Is Nothing Then
            registo.TempoDescontado = CDate(CStr(Date.Today) + " 00:00:00")
        End If


        registo.Entidade = lista.Valor("CDU_Entidade")
        registo.TipoEntidade = lista.Valor("CDU_TipoEntidade")
        registo.Posto = lista.Valor("CDU_Posto")
        registo.Operacao = lista.Valor("CDU_Operacao")
        registo.Molde = lista.Valor("CDU_Molde")

        Dim valor As String

        valor = consultaValor("SELECT CDU_Quantidade FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_MAOOBRA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
        If valor <> "" Then
            Dim id As String
            id = consultaValor("SELECT CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_MAOOBRA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
            registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(registo.Posto), valor, id)
        End If


        'If registo.Tempo Is Nothing Then
        '    registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(registo.Posto), calculaTempoMaoObra(registo.DataInicial, registo.DataFinal))
        'Else
        '    registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(registo.Posto), registo.Tempo)
        'End If

        valor = consultaValor("SELECT CDU_Quantidade FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_LIMPEZA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
        If valor <> "" Then
            Dim id As String
            id = consultaValor("SELECT CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_LIMPEZA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
            registo.adicionaRegistoMaterialLimpeza(daArtigoMLProjecto(registo.Posto), valor, id)
        End If

        registo.adicionaLinhasMaterialRTJ(convertRecordSetToDataTable(consulta("SELECT CDU_Artigo as Artigo,CDU_Armazem as Armazem, CDU_Quantidade as Quantidade,CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_RTJ + "' and CDU_IdTDUCabec='" + idRegisto + "'").DataSet))
        registo.adicionaLinhasMaterialCliente(convertRecordSetToDataTable(consulta("SELECT CDU_Artigo as Artigo,CDU_Armazem as Armazem, CDU_Quantidade as Quantidade,CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_CLIENTE + "' and CDU_IdTDUCabec='" + idRegisto + "'").DataSet))

        Return registo
    End Function

    Public Function buscarPrecoArtigo(artigo As String, entidade As String, tipoentidade As String) As Double
        If campoArtigoPreco <> "" Then
            Return bso.Comercial.Artigos.DaValorAtributo(artigo, campoArtigoPreco)
        Else
            If tipoentidade = "C" Then
                Return bso.Comercial.Artigos.DaValorAtributo(artigo, "PCMedio")
            Else
                Dim tipoPreco As Integer
                Dim unidade As String
                tipoPreco = bso.Comercial.Clientes.DaValorAtributo(entidade, "TipoPrec")
                unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
                Select Case tipoPreco
                    Case 0 : Return bso.Comercial.ArtigosPrecos.DaValorAtributo(artigo, campoMoeda, unidade, "PVP1")
                    Case 1 : Return bso.Comercial.ArtigosPrecos.DaValorAtributo(artigo, campoMoeda, unidade, "PVP2")
                    Case 2 : Return bso.Comercial.ArtigosPrecos.DaValorAtributo(artigo, campoMoeda, unidade, "PVP3")
                    Case 3 : Return bso.Comercial.ArtigosPrecos.DaValorAtributo(artigo, campoMoeda, unidade, "PVP4")
                    Case 4 : Return bso.Comercial.ArtigosPrecos.DaValorAtributo(artigo, campoMoeda, unidade, "PVP5")
                    Case 5 : Return bso.Comercial.ArtigosPrecos.DaValorAtributo(artigo, campoMoeda, unidade, "PVP6")
                    Case Else : Return 0
                End Select
            End If
        End If
        Return 0
    End Function

    Public Function buscarDocumentoCompra(id As String) As GcpBEDocumentoCompra
        If bso.Comercial.Compras.ExisteID(id) Then
            Return bso.Comercial.Compras.EditaID(id)
        Else
            Return Nothing
        End If
    End Function

    Public Function buscarDocumentoVenda(id As String) As GcpBEDocumentoVenda
        If bso.Comercial.Vendas.ExisteID(id) Then
            Return bso.Comercial.Vendas.EditaID(id)
        Else
            Return Nothing
        End If
    End Function

    'Public Function existeArtigo(artigo As String) As Boolean
    '    Dim valor As Integer
    '    valor = consultaValor("SELECT count(*) FROM Artigo WHERE artigo='" + artigo + "'")
    '    Return valor <> 0
    'End Function

    Public Function existeArtigo1(id As String) As Boolean
        Dim valor As Integer
        valor = consultaValor("SELECT count(*) FROM LinhasCompras WHERE artigo='" + artInsp1 + "' and idCabecCompras='" + id + "'")
        Return valor <> 0
    End Function

    Public Function existeArtigo2(id As String) As Boolean
        Dim valor As Integer
        valor = consultaValor("SELECT count(*) FROM LinhasCompras WHERE artigo='" + artInsp2 + "' and idCabecCompras='" + id + "'")
        Return valor <> 0
    End Function

    Public Function TerminarOperacao(ByVal registo As Registo) As Integer
        TerminarOperacao = abrirFormTerminar(registo)
    End Function

    Public Function abrirFormTerminar(ByVal registo As Registo) As Integer

        Dim frmT As FrmTerminar
        frmT = New FrmTerminar
        frmT.Registo = registo


        Try
            frmT.ShowDialog()
            abrirFormTerminar = frmT.BotaoSeleccionado
            frmT.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            abrirFormTerminar = 3
        End Try

    End Function

 

    'Buscar o numero de licenciamentos (postos disponiveis)
    ReadOnly Property getNumeroLicenciamentos()
        Get
            Return numLicenciamentos
        End Get
    End Property

    Public Function validaPostos(posto As String, numPostos As Integer) As Boolean
        Dim numLinhas As Integer
        Dim existePosto As Boolean

       

        numLinhas = CInt(consultaValor("SELECT COUNT(*) FROM TDU_POSTOSTRABALHO"))

        If numLinhas <= numPostos Then

            existePosto = CInt(consultaValor("SELECT COUNT(*) FROM TDU_POSTOSTRABALHO WHERE CDU_POSTO='" + posto + "'")) <> 0

            If Not existePosto Then
                If numLinhas < numPostos Then
                    executarComando("INSERT INTO TDU_POSTOSTRABALHO(CDU_POSTO,CDU_ACTIVO) VALUES ('" + posto + "',1) ")
                Else
                    Return False
                End If
            Else
                actualizarPosto(posto, "1")
            End If

            Return True

        Else
            '  MsgBox("Não existem postos disponiveis, desactive algum dos postos", MsgBoxStyle.Critical)
            Return False
        End If

    End Function

    Public Sub actualizarPosto(posto As String, estado As String)
        executarComando("UPDATE TDU_POSTOSTRABALHO set CDU_ACTIVO=" + estado + "WHERE CDU_POSTO='" + posto + "'")
    End Sub

    Public Function GetMoradas(index As Integer) As String
        Select Case index
            Case 0 : Return bso.Contexto.IDMorada
            Case 1 : Return bso.Contexto.IDLocalidade
            Case 2 : Return bso.Contexto.IDCodPostal
            Case 3 : Return bso.Contexto.IDCodPostalLocal
            Case Else : Return ""
        End Select
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Function buscarCampos(Optional modulo As String = "") As ArrayList
        Dim strCampos As String()
        Dim strcampo As String()
        Dim str As String
        Dim arr As ArrayList


        arr = New ArrayList
        strCampos = campos.Split(";")

        For Each str In strCampos
            strcampo = str.Split(",")
            If strcampo.Length = 2 Then
                If existeCampo(modulo, strcampo(0)) Then
                    arr.Add(strcampo(0))
                End If
            End If
        Next
        Return arr

    End Function



    Public Function buscarNomesCampos(Optional modulo As String = "") As ArrayList
        Dim strCampos As String()
        Dim strcampo As String()
        Dim str As String
        Dim arr As ArrayList
        arr = New ArrayList
        strCampos = campos.Split(";")

        For Each str In strCampos
            strcampo = str.Split(",")
            If strcampo.Length = 2 Then
                If existeCampo(modulo, strcampo(0)) Then
                    arr.Add(strcampo(1))
                End If
            End If
        Next
        Return arr
    End Function


    Private Function existeCampo(modulo As String, campo As String) As Boolean
        Dim tabela As String
        tabela = ""
        If modulo = "" Then Return True
        Select Case modulo
            Case "I" : tabela = "LinhasInternos"
        End Select

        Dim mAPL As MotorAPL
        mAPL = New MotorAPL(Me)
        Return mAPL.existeCampo(tabela, campo)
    End Function

    ''' <summary>
    ''' Função que permite ir buscar o Tamanho para a grelha
    ''' </summary>
    ''' <remarks></remarks>

    ReadOnly Property TAMANHOGRELHA() As Long
        Get
            TAMANHOGRELHA = tamanho
        End Get
    End Property


    Public Function consultaEntradaIniFile(seccao As String, chave As String) As String

        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        Return ficheiroIni.GetString(seccao, chave, "")

    End Function

    Public Function artigoFormula(artigo As String, modulo As String) As String
        Dim artFomula As String

        If modulo = "V" Then
            artFomula = bso.Comercial.Artigos.DaValorAtributo(artigo, "FormulaVendas")
        Else
            artFomula = bso.Comercial.Artigos.DaValorAtributo(artigo, "FormulaCompras")
        End If

        Return artFomula

    End Function

    Public Function daFormula(formula) As GcpBEFormula
        Return bso.Comercial.Formulas.Edita(formula)
    End Function

    Public Function validarLicenca(empresa As String, modulo As String) As Boolean
        Dim licence() As String
        Dim company() As String
        licence = licenca.Split("#")
        If licence.Count = 2 Then
            company = licence(0).Split(",")
            For i = 0 To company.Count() - 1
                If empresa.ToUpper = company(i).ToUpper() And licence(1).ToUpper.IndexOf(modulo) <> -1 Then
                    Return True
                End If
            Next
        Else
            Return False
        End If

    End Function

    Public Function daListaModulos(modulosEmpresa As String) As StdBELista

        Dim modules() As String
        Dim query As String
        '  licence = licenca.Split("#")
        query = ""
        ' If licence.Count = 2 Then
        modules = modulosEmpresa.Split(",")
        For i = 0 To modules.Count() - 1
            If modules(i).ToUpper() = "S" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'S' as Codigo, 'Stocks' as Nome"
            End If

            If modules(i).ToUpper() = "P" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'P' as Codigo, 'Registo Pontos' as Nome"
            End If

            If modules(i).ToUpper() = "E" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'E' as Codigo, 'Docs\Encomendas' as Nome"
            End If

            If modules(i).ToUpper() = "R" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'R' as Codigo, 'Registo Pontos' as Nome"
            End If

            If modules(i).ToUpper() = "M" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'M' as Codigo, 'Lista Materiais' as Nome"
            End If

            If modules(i).ToUpper() = "G" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'G' as Codigo, 'Gestão Encomendas' as Nome"
            End If

            If modules(i).ToUpper() = "A" Then
                If query <> "" Then query = query + " UNION "
                query = query + "SELECT 'A' as Codigo, 'Avaliação Fornecedores' as Nome"
            End If

        Next

        If query <> "" Then
            Return bso.Consulta(query)
        End If

        ' End If
        Return Nothing
    End Function

    Public Function buscarForm(artigo As String) As Integer
        Dim art As String
        buscarForm = 1

        Dim view As String

        view = viewArtigosFerramentas

        If view = "" Then
            Return buscarForm
        End If

        art = consultaValor("SELECT Artigo FROM Artigo WHere artigo in (SELECT artigo from " + view + ") and artigo='" + artigo + "'")

        If art <> "" Then
            buscarForm = 2
        End If
        Return buscarForm
    End Function


    Public Function isDocumentoFE(documento As String) As Boolean
        Return documentoFE.ToUpper = documento.ToUpper
    End Function


    Private Sub sincronizarMaquinaKardex(doc As GcpBEDocumentoInterno, molde As String)
        Dim k As MotorKardex
        k = MotorKardex.GetInstance(bso.Contexto.CodEmp)
        Dim entradaSaida As String
        entradaSaida = bso.Comercial.TabInternos.DaValorAtributo(doc.Tipodoc, "TipoDocStk")
        k.integrarDocumentoStockKardex(doc, molde, buscarCampos(), entradaSaida)
    End Sub

    Public Function existeArtigo(ByRef pesquisa As String) As Boolean
        Dim m As Motor
        Dim artigo As String
        existeArtigo = False
        m = Motor.GetInstance
        existeArtigo = bso.Comercial.Artigos.Existe(pesquisa)
        If Not existeArtigo Then
            artigo = bso.Comercial.Artigos.ExisteCodBarras(pesquisa)
            If artigo <> "" Then
                pesquisa = artigo
                existeArtigo = True
            End If
        End If
    End Function



    ''' <summary>
    '''  Funcao que permite validar se uma operacao existe
    ''' </summary>
    ''' <param name="operacao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function operacaoExiste(ByVal operacao As String) As Boolean
        Dim operacao1 As String
        operacao1 = consultaValor("SELECT CDU_ObraN2 FROm TDU_ObraN2 WHERE CDU_ObraN2='" + operacao + "'")
        operacaoExiste = operacao1 <> ""
    End Function

    ''' <summary>
    ''' Função que permite inserir uma operacao no sistema
    ''' </summary>
    ''' <param name="operacao"></param>
    ''' <param name="descricao"></param>
    ''' <remarks></remarks>
    Public Sub inserirOperacao(ByVal operacao As String, ByVal descricao As String)
        If operacaoExiste(operacao) Then
            executarComando("UPDATE TDU_ObraN2 set CDU_Descricao='" + descricao + "' WHERE CDU_ObraN2='" + operacao + "'")
        Else
            executarComando("INSERT INTO TDU_ObraN2(CDU_ObraN2,CDU_Descricao) VALUES('" + operacao + "','" + descricao + "')")
        End If

    End Sub


    ''' <summary>
    ''' Funcao que permite validar se uma peça existe
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function pecaExiste(ByVal projecto As String, ByVal peca As String) As Boolean
        Dim peca1 As String
        peca1 = consultaValor("SELECT CDU_ObraN1 FROm TDU_ObraN1 WHERE CDU_ObraN1='" + peca + "' and CDU_Projecto='" + projecto + "'")
        pecaExiste = peca1 <> ""
    End Function

    ''' <summary>
    ''' Funcao que permite validar se um posto existe
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function postoExiste(ByVal posto As String) As Boolean
        Dim posto1 As String
        posto1 = consultaValor("SELECT CDU_Posto FROM TDU_Postos WHERE CDU_Posto='" + posto + "'")
        postoExiste = posto1 <> ""
    End Function

    ''' <summary>
    ''' Função que permite inserir um posto no sistema
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="descricao"></param>
    ''' <remarks></remarks>
    Public Sub inserirPosto(ByVal posto As String, ByVal descricao As String, artigo As String)
        If postoExiste(posto) Then
            executarComando("UPDATE TDU_Postos set CDU_Descricao='" + descricao + "',CDU_Artigo='" + artigo + "'  WHERE CDU_Posto='" + posto + "'")
        Else
            executarComando("INSERT INTO TDU_Postos(CDU_Posto,CDU_Descricao,CDU_Artigo) VALUES('" + posto + "','" + descricao + "','" + artigo + "')")
        End If

    End Sub

    Public Function daListaOperacoes() As StdBELista
        Dim lista As StdBELista
        lista = bso.Consulta("SELECT * FROM APS_GP_Operacoes")
        Return lista
    End Function

    Public Function daListaPostos() As StdBELista
        Dim lista As StdBELista
        lista = bso.Consulta("SELECT * FROM APS_GP_Postos")
        Return lista
    End Function

    Public Function daListaPostos(operacao As String) As StdBELista
        Dim lista As StdBELista
        lista = bso.Consulta("SELECT * FROM APS_GP_Postos WHERE CDU_Operacao='" + operacao + "'")
        Return lista
    End Function

    Public Function daListaFuncionarios() As StdBELista
        Dim lista As StdBELista
        lista = bso.Consulta("SELECT * FROM APS_GP_Funcionarios")
        Return lista

    End Function


    Public Function removerPeca(Peca As String, PecaNome As String) As Boolean

        Dim valor As Integer
        Dim motRP As MotorRP
        motRP = MotorRP.GetInstance()

        valor = consultaValor("SELECT COUNT(*) FROM LinhasInternos inner join CabecInternos On CabecInternos.Id=LinhasInternos.idCabecInternos WHERE cdu_ObraN1='" + Peca + "'  and CabecInternos.tipodoc='" + motRP.TipoDoc + "'")

        If valor > 0 Then
            MsgBox("Já existem registos iniciados" + vbCrLf + vbCrLf + "Impossivel Remover Registo", MsgBoxStyle.Exclamation)
            Return False
        Else
            executarComando("DELETE FROM TDU_RegistoFolhaPonto WHERE cdu_peca='" + Peca + "' and CDU_Idlinha is null")
            executarComando("DELETE FROM TDU_ObraN1 WHERE cdu_ObraN1='" + Peca + "'")
            MsgBox("Peça [" + PecaNome + "]  removido com sucesso!", MsgBoxStyle.Information)
            Return True
        End If


    End Function


    Public Function removerOperacao(operacaoID As String, operacao As String, nome As String) As Boolean
        Dim valor As Integer
        valor = consultaValor("SELECT COUNT(*) FROM TDU_RegistoFolhaPonto WHERE CDU_Operacao='" + operacao + "'")

        If valor > 0 Then
            MsgBox("A Operação já se encontra configuração em Projectos!" + vbCrLf + vbCrLf + "Impossivel Remover Registo.", MsgBoxStyle.Exclamation)
            Return False
        Else
            IniciaTransacao()
            executarComando("DELETE FROM TDU_UtilizadoresOperacoes WHERE CDU_Operacao='" + operacao + "'")
            '   executarComando("DELETE FROM TDU_UtilizadoresPostos WHERE CDU_Posto IN (select CDU_Posto FROM TDU_Postos WHERE CDU_Operacao='" + operacao + "')")
            executarComando("DELETE FROM TDU_ObraN2 WHERE cdu_ObraN2='" + operacaoID + "'")
            ' executarComando("DELETE FROM TDU_Postos WHERE CDU_Operacao='" + operacao + "'")
            TerminaTransacao()
            MsgBox("Operação [" + operacao + "] [" + nome + "] removida com sucesso!", MsgBoxStyle.Information)
            Return True
        End If
    End Function

    Public Function removerPosto(artigo As String, posto As String, nome As String) As Boolean
        Dim valor As Integer
        Dim motRP As MotorRP
        motRP = MotorRP.GetInstance()
        valor = consultaValor("SELECT COUNT(*) FROM LinhasInternos inner join CabecInternos On CabecInternos.Id=LinhasInternos.idCabecInternos WHERE Artigo='" + artigo + "' and CabecInternos.tipodoc='" + motRP.TipoDoc + "'")

        If valor > 0 Then
            MsgBox("O Posto já se encontra configurado em Projectos" + vbCrLf + vbCrLf + "Impossivel Remover Registo.", MsgBoxStyle.Exclamation)
            Return False
        Else
            IniciaTransacao()
            executarComando("DELETE FROM TDU_Postos WHERE CDU_Posto='" + posto + "'")
            executarComando("DELETE FROM TDU_PostosOperacoes WHERE CDU_Posto='" + posto + "'")
            executarComando("DELETE FROM TDU_UtilizadoresPostos WHERE CDU_Posto='" + posto + "'")
            TerminaTransacao()
            MsgBox("Posto [" + posto + "] [" + nome + "] removido com sucesso!", MsgBoxStyle.Information)
            Return True
        End If
    End Function



    Public Function removerOperacao(operacao As String, nome As String) As Boolean
        Dim valor As Integer
        Dim motRP As MotorRP
        motRP = MotorRP.GetInstance()

        valor = consultaValor("SELECT COUNT(*) FROM LinhasInternos inner join CabecInternos On CabecInternos.Id=LinhasInternos.idCabecInternos WHERE CDU_ObraN2='" + operacao + "' and CabecInternos.tipodoc='" + motRP.TipoDoc + "'")

        If valor > 0 Then
            MsgBox("A Operação já se encontra configuração em Projectos!" + vbCrLf + vbCrLf + "Impossivel Remover Registo.", MsgBoxStyle.Exclamation)
            Return False
        Else
            IniciaTransacao()
            executarComando("DELETE FROM TDU_PostosOperacoes WHERE CDU_Operacao='" + operacao + "'")
            executarComando("DELETE FROM TDU_UtilizadoresOperacoes WHERE CDU_Operacao='" + operacao + "'")
            executarComando("DELETE FROM TDU_ObraN2 WHERE cdu_ObraN2='" + operacao + "' or CDU_Descricao='" + operacao + "'")
            TerminaTransacao()
            MsgBox("Operação [" + operacao + "] [" + nome + "] removida com sucesso!", MsgBoxStyle.Information)
            Return True
        End If
    End Function

    Public Function daPermissoesOperacaoesFuncionario(func As String, operacao As String) As Boolean
        Dim valor As String
        valor = consultaValor("SELECT Count(*) FROM TDU_UtilizadoresOperacoes WHERE TDU_UtilizadoresOperacoes.CDU_Operacao='" + operacao + "' AND CDU_Utilizador='" + func + "'")
        Return valor <> "0"
    End Function

    Public Function daPermissoesPostosFuncionario(func As String, posto As String) As Boolean
        Dim valor As String
        valor = consultaValor("SELECT Count(*) FROM TDU_UtilizadoresPostos WHERE TDU_UtilizadoresPostos.CDU_Posto='" + posto + "' AND CDU_Utilizador='" + func + "'")
        Return valor <> "0"
    End Function

    Public Function daPermissoesFuncionariosFuncionario(func As String, funcionarioPermissoes As String) As Boolean
        Dim valor As String
        valor = consultaValor("SELECT Count(*) FROM TDU_UtilizadoresPermissoes WHERE CDU_Utilizador='" + func + "' AND CDU_UtilizadorPermissao='" + funcionarioPermissoes + "'")
        Return valor <> "0"
    End Function

    Public Function daPermissoesOperacaoesPosto(posto As String, operacao As String) As Boolean
        Dim valor As String
        valor = consultaValor("SELECT Count(*) FROM TDU_PostosOperacoes WHERE TDU_PostosOperacoes.CDU_Operacao='" + operacao + "' AND CDU_Posto='" + posto + "'")
        Return valor <> "0"
    End Function


    Public Sub IniciaTransacao()
        bso.IniciaTransaccao()
    End Sub

    Public Sub TerminaTransacao()
        bso.TerminaTransaccao()
    End Sub

    Public Sub DesfazTransacao()
        bso.DesfazTransaccao()
    End Sub

    Public Sub removePermissoes(func As String)
        executarComando("DELETE FROM TDU_UtilizadoresOperacoes WHERE  CDU_Utilizador='" + func + "'")
        executarComando("DELETE FROM TDU_UtilizadoresPostos WHERE  CDU_Utilizador='" + func + "'")
        executarComando("DELETE FROM TDU_UtilizadoresPermissoes WHERE  CDU_Utilizador='" + func + "'")
    End Sub
    Public Sub inserePermissoesOperacao(func As String, operacao As String)
        executarComando("Insert Into TDU_UtilizadoresOperacoes (CDU_Utilizador,CDU_Operacao) VALUES('" + func + "','" + operacao + "') ")
    End Sub

    Public Sub inserePermissoesPosto(func As String, posto As String)
        executarComando("Insert Into TDU_UtilizadoresPostos (CDU_Utilizador,CDU_Posto) VALUES('" + func + "','" + posto + "') ")
    End Sub
    Public Sub inserePermissoesFuncionario(func As String, funcionarioPerm As String)
        executarComando("Insert Into TDU_UtilizadoresPermissoes (CDU_Utilizador,CDU_UtilizadorPermissao) VALUES('" + func + "','" + funcionarioPerm + "') ")
    End Sub


    Public Sub removePermissoesPosto(posto As String)
        executarComando("DELETE FROM TDU_PostosOperacoes WHERE  CDU_Posto='" + posto + "'")
    End Sub
    Public Sub inserePermissoesOperacaoPosto(posto As String, operacao As String)
        executarComando("Insert Into TDU_PostosOperacoes (CDU_Posto,CDU_Operacao) VALUES('" + posto + "','" + operacao + "') ")
    End Sub



    Private Function verificarComposicaoArtigos(ini As IniFile, documento As GcpBEDocumentoVenda)

        'Efetuar compisocao stocks
        If ini.GetInteger("COMPOSICAO", "TRATAR", 0) = 1 Then
            Try

                Dim doccomposicao As String

                'Saber qual o documento de composição referente ao documento de venda em causa
                doccomposicao = ini.GetString("COMPOSICAO", "DOCUMENTO", "COM")
                doccomposicao = consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosVenda WHERE Documento='" + documento.Tipodoc + "'")
                '  doccomposicao = Equivalencias.daEquivalenciaDocComposicao(ini.GetString("BASEDADOS", "INSTANCIA", ""), ini.GetString("BASEDADOS", "UTILIZADOR", ""), seg.decifrarStringTripleDES(ini.GetString("BASEDADOS", "PASSWORD", "")), codigoempresa_, Documento.TipoDoc, Documento.Serie)


                efectuarComposicaoStocks(documento, doccomposicao, ini.GetString("ERP", "UTILIZADOR", ""))
                'efectuarComposicaoStocks(documento, ini.GetString("COMPOSICAO", "TipoDoc", ""), ini.GetString("ERP", "UTILIZADOR", ""))


            Catch ex As Exception
                MsgBox("AdicionarDocumento: Efetuar Composições de Stocks: " + ex.Message)
                ' escreverFicheiroLog("AdicionarDocumento: Efetuar Composições de Stocks: " + ex.Message, True, True)
                'Logger.log("AdicionarDocumento: Efetuar Composições de Stocks: " + ex.Message)
            End Try
        End If
    End Function

    Public Sub efectuarComposicaoStocks(ByVal docVenda As GcpBEDocumentoVenda, ByVal tipoDocComp As String, ByVal funcionario As String)
        Dim docStocks As GcpBEDocumentoStock
        ' Dim docVenda As GcpBEDocumentoVenda
        Dim linhadocStocks As GcpBELinhaDocumentoStock
        Dim artigo As GcpBEArtigo
        Dim linha As GcpBELinhaDocumentoVenda
        Dim dv As GcpBETabVenda
        Dim tipoDoc As String
        Dim idLinha As String

        dv = New GcpBETabVenda
        dv = bso.Comercial.TabVendas.Edita(docVenda.Tipodoc)

        Dim ht As Hashtable
        ht = New Hashtable()


        'verificar  se o tipo de documento existe
        If Not dv Is Nothing Then


            'verificar se o campo de utilziador existe
            tipoDoc = tipoDocComp
            docStocks = New GcpBEDocumentoStock
            docStocks.Tipodoc = tipoDoc

            bso.Comercial.Stocks.PreencheDadosRelacionados(docStocks)
            docStocks.Utilizador = funcionario
            Dim numerador As Long
            'actualizar o numerador da serie
            numerador = docVenda.NumDoc - 1 'CLng(documento.NumDocOrig)
            docStocks.Serie = docVenda.Serie
            executarComando("UPDATE SeriesStocks SET Numerador=" + CStr(numerador) + " WHERE TipoDoc='" + tipoDoc + "' AND Serie='" + docStocks.Serie + "'")

            docStocks.Filial = docVenda.Filial

            docStocks.NumDoc = docVenda.NumDoc
            'colocar a data de doc com a certa
            docStocks.DataDoc = docVenda.DataDoc + DateTime.Now.TimeOfDay


            Try

                carregarArtigosCompostos(ht, docVenda)

                docStocks = carregarComposicaoLinhas(ht, docStocks)

                docStocks.DataDoc = buscarMenorData(docStocks)

                '   bso.Comercial.Vendas.Actualiza(docVenda)

                'apaga o doc de stocks se existir
                If bso.Comercial.Stocks.Existe(docStocks.Filial, docStocks.Tipodoc, docStocks.Serie, docStocks.NumDoc) Then
                    bso.Comercial.Stocks.Remove(docStocks.Filial, docStocks.Modulo, docStocks.Tipodoc, docStocks.Serie, docStocks.NumDoc)
                End If

                ''caso tenha linhas a inserir
                If docStocks.Linhas.NumItens > 0 Then
                    'Actualizar o documento de Stocks

                    Try
                        bso.Comercial.Stocks.Actualiza(docStocks)
                    Catch ex As Exception
                        MsgBox("efectuarComposicaoStocks: Erro ao inserir o documento de stock: " + ex.Message.ToString)
                        '  escreverFicheiroLog("efectuarComposicaoStocks: Erro ao inserir o documento de stock: " + ex.Message.ToString, True, True)
                        '   Logger.log("efectuarComposicaoStocks: Erro ao inserir o documento de stock: " + ex.Message.ToString)
                    End Try


                End If

            Catch ex As Exception
                MsgBox("efectuarComposicaoStocks: Erro ao Efetuar composição de stock com o numero " + docStocks.Tipodoc.ToString + " " + docStocks.Serie.ToString + "\" + docStocks.NumDoc.ToString + ": " + ex.Message)

                'escreverFicheiroLog("efectuarComposicaoStocks: Erro ao Efetuar composição de stock com o numero " + docStocks.Tipodoc.ToString + " " + docStocks.Serie.ToString + "\" + docStocks.NumDoc.ToString + ": " + ex.Message, True, True)
                '  Logger.log()
            End Try

        End If


    End Sub

    Private Function carregarComposicaoLinhas(ht As Hashtable, docStocks As GcpBEDocumentoStock) As GcpBEDocumentoStock

        'para cada linha do documento do venda
        Dim artigo As GcpBEArtigo
        Dim nivel As Integer
        Dim linhadocStocks As GcpBELinhaDocumentoStock
        Dim lstKeys As List(Of Integer)
        lstKeys = New List(Of Integer)


        For Each nivel In ht.Keys
            lstKeys.Add(nivel)
        Next


        Dim tipoDoc As String
        tipoDoc = consultaValor("SELECT TipoDocumento FROM DocumentosStk WHERE Documento='" + docStocks.Tipodoc + "'")
        lstKeys.Sort()

        If tipoDoc = "4" Then
            lstKeys.Reverse()
        End If

        Dim index As Integer
        index = lstKeys.Count

        Dim lst As List(Of ArtigoComposicao)
        For Each nivel In lstKeys
            lst = ht(nivel)

            For Each ac In lst

                artigo = bso.Comercial.Artigos.Edita(ac.Artigo)
                bso.Comercial.Artigos.PreencheDadosRelacionados(artigo)
                'adicionar a linha ao documento de stock
                docStocks = bso.Comercial.Stocks.AdicionaLinha(docStocks, ac.Artigo, "E", ac.Quantidade)

                'editar a linha
                linhadocStocks = docStocks.Linhas.Edita(docStocks.Linhas.NumItens)
                'artigo composto
                linhadocStocks.Descricao = ac.Descricao
                linhadocStocks.Armazem = ac.Armazem

                If ac.Lote <> "" Then linhadocStocks.Lote = ac.Lote
                linhadocStocks.Localizacao = ac.Localizacao
                linhadocStocks.Unidade = ac.Unidade
                linhadocStocks.Quantidade = ArredondaCDQuantidade(ac.Quantidade)
                ac.DataStock = ac.DataStock.AddSeconds(-index)
                linhadocStocks.DataStock = ac.DataStock
                processarLinhaArtigoCompostoGenerico(linhadocStocks, ac)
                '  processarLinhaArtigoCompostoEspecifico(docStocks, linhadocStocks, linha)
                'linha.PCM = ArredondaCDPCM(linhadocStocks.PrecUnit)


            Next
            index -= 1
        Next


        Return docStocks
    End Function


    'Função genérica que vai actualizar as linhas dos componentes
    Private Sub processarLinhaArtigoCompostoGenerico(ByRef linhadocStocks As GcpBELinhaDocumentoStock, ByVal ac As ArtigoComposicao)
        Dim linhasComp As GcpBEArtigoComponentes
        Dim i As Long
        linhasComp = bso.Comercial.ArtigosComponentes.ListaArtigosComponentes(ac.Artigo)
        If Not linhasComp Is Nothing Then
            For i = 1 To linhasComp.NumItens
                ' linhadocStocks.Linhas(i).Armazem = linhadocStocks.Armazem
                ' linhadocStocks.Linhas(i).Localizacao = linhadocStocks.Localizacao
                'linhadocStocks.Linhas(i).Lote = ""
                linhadocStocks.Linhas(i).Quantidade = ArredondaCDQuantidade(linhasComp(i).Quantidade)
                linhadocStocks.Linhas(i).DataStock = ac.DataStock
            Next
        End If
    End Sub


    Private Function linhaJaMovimentouStocks(ByVal id_Linha As String) As Boolean

        ' Dim linhas As GcpBELinhasRastreabilidade

        '  linhas = New GcpBELinhasRastreabilidade

        '  empresa.Comercial.Vendas.ProcuraLinhasPosteriores(id_Linha, linhas)

        Dim rs As StdBELista
        Dim query As String

        query = "SELECT Id, IdCabecOrig, IdLinhaOrig FROM LinhasSTK WHERE IdLinhaOrig='" + id_Linha + "'"

        rs = bso.Consulta(query)

        If Not rs.NoFim Then
            linhaJaMovimentouStocks = True
        Else
            linhaJaMovimentouStocks = False
        End If


    End Function



    Public Function ArredondaCDQuantidade(ByVal valor As Double)
        Dim p As GcpBEParametros
        p = bso.Comercial.Parametros.Edita
        ArredondaCDQuantidade = Math.Round(valor, p.CasasDecimaisQnt)
        p = Nothing
    End Function

    Public Function ArredondaCDPCM(ByVal valor As Double)
        Dim p As GcpBEParametros
        p = bso.Comercial.Parametros.Edita
        ArredondaCDPCM = Math.Round(valor, p.CasasDecimaisPCM)
        p = Nothing
    End Function

    'Funcao que vai buscar a menor Data das linhas de documento de stock
    Private Function buscarMenorData(ByVal docStock As GcpBEDocumentoStock) As Date

        Dim i As Long
        Dim data As Date
        data = docStock.DataDoc

        For i = 1 To docStock.Linhas.NumItens

            If (data > docStock.Linhas(i).DataStock) Then
                data = docStock.Linhas(i).DataStock
            End If

        Next i

        buscarMenorData = data

    End Function



    Private Sub carregarArtigosCompostos(ByRef ht As Hashtable, docVenda As GcpBEDocumentoVenda)
        Dim lst As List(Of ArtigoComposicao) = New List(Of ArtigoComposicao)
        Dim artigo As GcpBEArtigo
        Dim artComp As ArtigoComposicao
        Dim linha As GcpBELinhaDocumentoVenda
        Dim nivel As Integer = 0
        Dim idLinha As String
        For Each linha In docVenda.Linhas

            'verificar se o artigo existe
            If bso.Comercial.Artigos.Existe(linha.Artigo) Then
                artigo = bso.Comercial.Artigos.Edita(linha.Artigo)
                'verificar se o artigo é um artigo composto
                If artigo.Classe = "2" Then

                    idLinha = ""

                    'caso a linha tenha sido transformada
                    If linha.IDLinhaOriginal <> "" Then
                        idLinha = linha.IDLinhaOriginal
                    Else
                        idLinha = linha.IdLinha
                    End If

                    If Not linhaJaMovimentouStocks(idLinha) Or linha.IDLinhaOriginal = "" Then
                        artComp = New ArtigoComposicao
                        artComp.Artigo = artigo.Artigo
                        artComp.Armazem = linha.Armazem
                        artComp.Descricao = linha.Descricao
                        artComp.Localizacao = linha.Localizacao
                        artComp.Quantidade = linha.Quantidade
                        artComp.Unidade = linha.Unidade
                        artComp.DataStock = linha.DataStock '.AddSeconds(-1)
                        lst.Add(artComp)
                        carregarArtigosCompostos(ht, nivel + 1, artigo, artComp)
                    End If
                End If
            End If
        Next
        If lst.Count > 0 Then
            ht.Add(nivel, lst)
        End If
    End Sub


    Private Sub carregarArtigosCompostos(ByRef ht As Hashtable, nivel As Integer, art As GcpBEArtigo, ac As ArtigoComposicao)
        Dim lst As List(Of ArtigoComposicao) = New List(Of ArtigoComposicao)
        Dim artigo As GcpBEArtigo
        Dim linhasComp As GcpBEArtigoComponentes
        Dim artComp As ArtigoComposicao
        Dim i As Long

        If nivel > 500 Then
            Exit Sub
        End If

        linhasComp = bso.Comercial.ArtigosComponentes.ListaArtigosComponentes(art.Artigo)
        If Not linhasComp Is Nothing Then

            For i = 1 To linhasComp.NumItens
                artigo = bso.Comercial.Artigos.Edita(linhasComp(i).Componente)
                'verificar se o artigo é um artigo composto
                If artigo.Classe = "2" Then
                    artComp = New ArtigoComposicao
                    artComp.Artigo = artigo.Artigo
                    artComp.Descricao = artigo.Descricao
                    artComp.Armazem = linhasComp(i).Armazem ' ac.Armazem
                    artComp.Localizacao = linhasComp(i).Localizacao
                    artComp.Quantidade = linhasComp(i).Quantidade * ac.Quantidade
                    artComp.Unidade = artigo.UnidadeBase
                    artComp.DataStock = ac.DataStock '.AddSeconds(-1)
                    lst.Add(artComp)
                    carregarArtigosCompostos(ht, nivel + 1, artigo, artComp)
                End If
            Next
        End If
        If lst.Count > 0 Then
            ht.Add(nivel, lst)
        End If


    End Sub

End Class
