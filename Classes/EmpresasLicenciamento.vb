﻿Public Class EmpresasLicenciamento
    Dim empresa_ As String
    Property Empresa() As String
        Get
            Empresa = empresa_
        End Get
        Set(ByVal value As String)
            empresa_ = value
        End Set
    End Property

    Dim modulos_ As String
    Property Modulos() As String
        Get
            Modulos = modulos_
        End Get
        Set(ByVal value As String)
            modulos_ = value
        End Set
    End Property


    Dim dataLimite_ As String
    Property DataLimite() As String
        Get
            DataLimite = dataLimite_
        End Get
        Set(ByVal value As String)
            dataLimite_ = value
        End Set
    End Property

    Dim numPostos_ As Integer
    Property NumPostos() As Integer
        Get
            NumPostos = numPostos_
        End Get
        Set(ByVal value As Integer)
            numPostos_ = value
        End Set
    End Property

    Public Function validarModulo(modulo As String) As String

        If modulos_.ToUpper.IndexOf(modulo) <> -1 Then
            If IsDate(dataLimite_) Then
                If CDate(dataLimite_) > Date.Now.Date Or dataLimite_ = "" Then
                    Return ""
                Else
                    Return "Licença Expirada em " + dataLimite_ + ". Por favor contacte a Anphis."

                End If
            Else
                Return ""
            End If
        Else
            Return "A empresa " + empresa_ + " não se encontra licenciada para o módulo " + getDescricaoModulo(modulo)
        End If

    End Function

    Public Function getDescricaoModulo(modulo As String) As String

        ';S- Stocks
        ';P- Pontos
        ';E- Encomendas
        ';R-Registo Pontos
        ';M- Lista Materiais

        Select Case modulo.ToUpper()
            Case "S" : Return "Stocks"
            Case "P" : Return "Pontos"
            Case "E" : Return "Encomendas"
            Case "R" : Return "Registo Pontos"
            Case "M" : Return "Lista Materiais"
            Case Else : Return modulo
        End Select

    End Function



End Class
