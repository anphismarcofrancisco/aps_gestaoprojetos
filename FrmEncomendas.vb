﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900
Imports Interop.GcpBE900
Imports Interop.StdPlatBS900

Imports PlatAPSNET

Public Class FrmEncomendas

    Const COLUMN_ARTIGO As Integer = 0
    ' Const COLUMN_ARMAZEM As Integer = 2
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTSTOCK_PRECO As Integer = 2
    Const COLUMN_QUANTIDADE As Integer = 3
    Const COLUMN_DESCONTO As Integer = 4
    Const COLUMN_PROJECTO As Integer = 5
    Dim aplConfigurada As Boolean
    Dim modulo As String
    Dim arrCampo As ArrayList
    Dim arrNome As ArrayList



    Private Sub FrmEncomendas_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim m As Motor
        m = Motor.GetInstance
        If m.getApresentaArtigosStockMinimo() Then
            validarArtigosStockMinimo()
        End If
    End Sub


    Private Sub FrmEncomendas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            Me.WindowState = FormWindowState.Maximized

            Dim fileINI As String
            Dim ficheiroConfiguracao As Configuracao
            fileINI = Application.StartupPath + "\\CONFIG.ini"
            ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
            Dim m As Motor
            m = Motor.GetInstance

            lblText.Text = m.EmpresaDescricao
            Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario

            gridArtigosStock.Tag = ""
            '
            ' txtNumDoc.Value = -1

            iniciarToolBox()

            inicializarMoldes()

            gridArtigos.AutoColumnSizing = False
            gridArtigos.AutoColumnSizing = False

            gridArtigosStock.AutoColumnSizing = False
            gridArtigosStock.AutoColumnSizing = False

            arrCampo = m.buscarCampos
            arrNome = m.buscarNomesCampos

            inicializarDocumentos()

            inserirColunasRegistos(1, gridArtigos, m.getApresentaProjectos)
            inserirColunasRegistos(2, gridArtigosStock, m.getApresentaProjectos)

            '   inicializarMatriculas()

            ' buscarArtigos(gridArtigos, txtArtigo.Text)
            gridArtigosStock.AllowEdit = True
            gridArtigosStock.EditOnClick = True

            If modulo = "C" Then
                txtNumDoc.Minimum = 0 ' m.daValorNumeracaoMinimaDocumentoCompra(lstDocumentos.SelectedValue)
                txtNumDoc.Maximum = 999999999 'm.daValorNumeracaoMaximaDocumentoCompra(lstDocumentos.SelectedValue)
                txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
            Else
                txtNumDoc.Minimum = 0 'm.daValorNumeracaoMinimaDocumentoVenda(lstDocumentos.SelectedValue)
                txtNumDoc.Maximum = 999999999 ' m.daValorNumeracaoMaximaDocumentoVenda(lstDocumentos.SelectedValue)
                txtNumDoc.Value = m.daNumeracaoDocumentoVenda(lstDocumentos.SelectedValue)
            End If


            ckbInsp1.Visible = m.getArtigoInsp1 <> ""
            ckbInsp2.Visible = m.getArtigoInsp2 <> ""

            lblInsp1.Text = m.getDescricaoArtigo(m.getArtigoInsp1)
            lblInsp2.Text = m.getDescricaoArtigo(m.getArtigoInsp2)


            lblMoldes.Visible = m.getApresentaProjectos
            txtPesqMolde.Visible = m.getApresentaProjectos
            lstMoldes.Visible = m.getApresentaProjectos

            'If Not m.getApresentaProjectos Then
            '    lstDocumentos.Location = New Point(469, 8)
            '    lblText.Location = New Point(310, 60)
            '    btnImprimir.Location = New Point(863, 8)
            '    btnEnviar.Location = New Point(974, 8)
            '    Me.Width = btnEnviar.Location.X + btnEnviar.Width + 40
            'End If

            'configurarPosicoes()



            '     txtObservacoes.WordWrap = False
            If txtNumDoc.Value = -1 Then
                MsgBox("Documento de Encomenda não configurado", MsgBoxStyle.Critical)
                Me.Close()
            End If

            txtArtigo.Focus()
            SendKeys.Send("{RIGHT}")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    'Private Sub inicializarMatriculas()
    '    Dim m As Motor
    '    m = Motor.GetInstance
    '    Dim dt As DataTable

    '    dt = m.consultaDataTable("SELECT Matricula as Codigo, Matricula as Descricao FROM Viaturas order by Matricula")
    '    txtMatricula.DropDownStyle = ComboBoxStyle.DropDown
    '    txtMatricula.DataSource = dt
    '    txtMatricula.DisplayMember = "Descricao"
    '    txtMatricula.AutoCompleteSource = AutoCompleteSource.ListItems
    '    txtMatricula.AutoCompleteMode = AutoCompleteMode.SuggestAppend
    '    txtMatricula.DisplayMember = "Descricao"
    '    txtMatricula.DropDownWidth = 100
    '    txtMatricula.ValueMember = "Codigo"

    '    limparMatricula()

    'End Sub



    Public Sub limparMatricula()
        txtMatricula.Text = ""
    End Sub
    'Private Sub InitializeTextBox()

    '    Dim allowedStatorTypes As New AutoCompleteStringCollection()
    '    Dim allstatortypes = StatorTypeDAL.LoadList(Of List(Of StatorType))().OrderBy(Function(x) x.Name).[Select](Function(x) x.Name).Distinct().ToList()

    '    If allstatortypes IsNot Nothing AndAlso allstatortypes.Count > 0 Then
    '        For Each item As String In allstatortypes
    '            allowedStatorTypes.Add(item)
    '        Next
    '    End If

    '    txtLocalDescarga.AutoCompleteMode = AutoCompleteMode.Suggest
    '    txtLocalDescarga.AutoCompleteSource = AutoCompleteSource.CustomSource
    '    txtLocalDescarga.AutoCompleteCustomSource = allowedStatorTypes
    'End Sub


    Private Sub configurarPosicoes()
        Dim m As Motor
        m = Motor.GetInstance

        gridArtigosStock.Width = m.TAMANHOGRELHA

        lstMoldes.Left = gridArtigosStock.Left + gridArtigosStock.Width + 10
        txtPesqMolde.Left = lstMoldes.Left
        lblMoldes.Left = lstMoldes.Left

        lblMoldes.Visible = m.getApresentaProjectos
        txtPesqMolde.Visible = m.getApresentaProjectos
        lstMoldes.Visible = m.getApresentaProjectos
        btnImportarLista.Visible = m.getApresentaProjectos

        'If m.getApresentaProjectos Then
        '    Me.MaximumSize = New Point(lstMoldes.Left + lstMoldes.Width + 30, Me.Height)
        '    Me.Width = lstMoldes.Left + lstMoldes.Width + 30
        'Else
        '    Me.MaximumSize = New Point(gridArtigosStock.Left + gridArtigosStock.Width + 30, Me.Height)
        '    Me.Width = gridArtigosStock.Left + gridArtigosStock.Width + 30
        'End If

        btnEnviar.Left = Me.Width - btnEnviar.Width - 30
        btnImprimir.Left = btnEnviar.Left - btnImprimir.Width - 10
        lstDocumentos.Left = btnImprimir.Left - lstDocumentos.Width - 10

        txtObservacoes.Left = gridArtigosStock.Left
        txtObservacoes.Width = gridArtigosStock.Width


        'If Not m.getApresentaProjectos Then
        '    lstDocumentos.Location = New Point(469, 8)
        '    lblText.Location = New Point(310, 60)
        '    btnImprimir.Location = New Point(863, 8)
        '    btnEnviar.Location = New Point(974, 8)
        '    Me.Width = btnEnviar.Location.X + btnEnviar.Width + 40
        'End If
    End Sub

    Private Sub inicializarMoldes()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstMoldes.SelectedIndex = -1
        lista = m.daListaMoldes(txtPesqMolde.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstMoldes.DataSource = dt
        lstMoldes.ValueMember = "Codigo"
        lstMoldes.DisplayMember = "Nome"
        lstMoldes.ClearSelected()
    End Sub


    Private Sub inicializarDocumentos()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstDocumentos.SelectedIndex = -1
        lista = m.daListaDocumentos()
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstDocumentos.ValueMember = "Codigo"
        lstDocumentos.DisplayMember = "Nome"
        lstDocumentos.DataSource = dt
        lstDocumentos.SelectedValue = m.DocumentoCompra
        m.AplicacaoInicializada = True

    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Novo", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Novo"

            Control = .Add(xtpControlButton, 2, " Gravar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Gravar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 7, " Duplicar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Gravar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 6, " Avaliacção Forn.", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Avaliacção Forn."
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False


            Control = .Add(xtpControlButton, 3, " Enviar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Enviar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False

            Control = .Add(xtpControlButton, 4, " Registos", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Registos"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 5, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow




        End With
    End Sub

    Private Sub inserirColunasRegistos(ByVal indexGrid As Integer, ByVal grid As AxXtremeReportControl.AxReportControl, apresentaProjectos As Boolean)

        Dim m As Motor
        Dim mAPL As MotorAPL
        m = Motor.GetInstance
        mAPL = New MotorAPL(m)
        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", 100, False, True)
        '  Column = inserirColuna(grid, COLUMN_ARMAZEM, "Armazém", 100, False, True)
        Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", IIf(indexGrid = 2, 250, 250), False, True)
        If indexGrid = 2 Then
            Column.EditOptions.AllowEdit = True
        End If

        '  If Not apresentaProjectos And indexGrid = 2 Then Column.Width = Column.Width + 60

        Column = inserirColuna(grid, COLUMN_QUANTSTOCK_PRECO, IIf(indexGrid = 2, "Preço", "Quant Stk"), IIf(indexGrid = 2, 60, 60), False, True)
        If indexGrid = 2 Then
            Column.EditOptions.AllowEdit = True
        End If
        Column.Alignment = xtpAlignmentRight

        If indexGrid = 2 Then
            Column = inserirColuna(grid, COLUMN_QUANTIDADE, "Quant.", 60, False, True)
            Column.Alignment = xtpAlignmentRight
            Column = inserirColuna(grid, COLUMN_DESCONTO, "Desc.", 60, True, True)
            Column.Alignment = xtpAlignmentRight
            Column.EditOptions.AllowEdit = True
            Column = inserirColuna(grid, COLUMN_PROJECTO, "Projecto", 60, False, True)
            Column.Alignment = xtpAlignmentRight
            If Not apresentaProjectos Then
                Column.EditOptions.AllowEdit = True
            End If
            '   Column.Visible = apresentaProjectos

            Dim tabela As String
            tabela = daTabelaModulo()
            'alteracao para colocar colunas consuante o descriminado no config 21-01-2013
            For i = 0 To arrCampo.Count - 1

                If mAPL.existeCampo(tabela, arrCampo(i)) Then
                    Column = inserirColuna(grid, COLUMN_PROJECTO + i + 1, arrNome(i), 70, False, True)
                    Column.Tag = m.consultaEntradaIniFile("DOCUMENTO", arrCampo(i))
                    Column.EditOptions.AllowEdit = True
                    Column.Alignment = xtpAlignmentRight
                    If mAPL.daTipoCampo(tabela, arrCampo(i)) = "bit" Then
                        Column.Width = 20
                    End If
                End If
            Next

        End If


    End Sub

    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Sub txtPesqMolde_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqMolde.Change
        inicializarMoldes()
    End Sub

    Private Sub buscarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim listaExclusao As String
        Dim m As Motor

        Me.Cursor = Cursors.WaitCursor
        'If documento <> "" Then
        '    lblEmModoEdicao.Text = "Documento " + documento + " em modo de edição"
        '    lblEmModoEdicao.Visible = True
        'Else
        '    lblEmModoEdicao.Visible = False
        'End If
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        grid.Records.DeleteAll()
        listaExclusao = "" 'daListaArtigosExcluidos()
        dtRegistos = m.daListaArtigos(m.daViewModulo(modulo), pesquisa, "", "")
        inserirLinhasRegistos(dtRegistos, grid)
        grid.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable, ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow, grid)
        Next
    End Sub

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional ByVal valorStk As Double = -1, Optional precUnit As Double = -1, Optional desconto As Double = -1, Optional varA As Double = -1, Optional varB As Double = -1, Optional varC As Double = -1, Optional quantidadeFormula As Double = -1, Optional numero As String = "")
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim m As Motor
        Dim formulLinha As FormulaLinha
        Dim motAPL As MotorAPL
        m = Motor.GetInstance
        motAPL = New MotorAPL(m)

        formulLinha = New FormulaLinha
        formulLinha.Numero = numero
        formulLinha.Quantidade = valorStk
        formulLinha.QuantidadeFormula = quantidadeFormula
        formulLinha.VarA = varA
        formulLinha.VarB = varB
        formulLinha.VarC = varC
        record = grid.Records.Add()


        '  record.PreviewText = "asd"
        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo"))
        Item.Tag = formulLinha
        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = dtRow("Descricao")
        If quantidadeFormula <> -1 Then
            Item.Tag = m.consultaValor("Select Descricao FROM Artigo WHERE Artigo='" + dtRow("Artigo") + "'")
            Item.Value = calcularDescricao(Item.Tag, varA, varB, varC, quantidadeFormula, numero)
            '  Item.Caption = Item.Value
        End If

        If grid.Name = "gridArtigosStock" Then
            Item.Editable = True
        End If
        ' Item.Tag = 1

        If grid.Name <> "gridArtigosStock" Then
            Item = record.AddItem(dtRow("StkActual"))
        Else
            If precUnit = -1 Then
                Item = record.AddItem(buscarPrecoArtigo(dtRow("Artigo"), txtEntidade.Text, modulo))
            Else
                Item = record.AddItem(precUnit)
            End If
            Item.Editable = True
            If valorStk < 0 Then valorStk = valorStk * -1
            Item = record.AddItem(valorStk)

            If desconto <> -1 Then
                Item = record.AddItem(desconto)
            Else
                Item = record.AddItem(0)
            End If


            If m.getApresentaProjectos Then
                If dtRow.Table.Columns.IndexOf("Codigo") <> -1 Then
                    Item = record.AddItem(m.NuloToString(dtRow("Codigo")))
                Else
                    Item = record.AddItem(lstMoldes.SelectedValue)
                End If
            Else
                If dtRow.Table.Columns.IndexOf(m.getCampoMolde) <> -1 Then
                    Item = record.AddItem(m.NuloToString(dtRow(m.getCampoMolde)))
                Else
                    Item = record.AddItem("")
                End If

            End If

            Dim tabela As String
            tabela = daTabelaModulo()

            For i = 0 To arrCampo.Count - 1
                If dtRow.Table.Columns.IndexOf(arrCampo(i)) <> -1 Then
                    If motAPL.daTipoCampo(tabela, arrCampo(i)) = "bit" Then
                        Item = record.AddItem("")
                        Item.HasCheckbox = True
                        Item.Checked = m.NuloToBoolean(dtRow(arrCampo(i)))
                    Else
                        Item = record.AddItem(m.NuloToString(dtRow(arrCampo(i))))
                    End If
                Else
                    Item = record.AddItem("")
                    If motAPL.daTipoCampo(tabela, arrCampo(i)) = "bit" Then
                        Item.HasCheckbox = True
                        Item.Checked = False
                    End If
                End If
            Next
        End If


    End Sub

    Private Function calcularDescricao(descricao As String, varA As Double, varB As Double, varC As Double, quantidadeFormula As Double, numero As String) As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim texto = m.getTextoCampoFormula
        texto = Replace(texto, "VAR_A", CStr(varA))
        texto = Replace(texto, "VAR_B", CStr(varB))
        texto = Replace(texto, "VAR_C", CStr(varC))
        texto = Replace(texto, "VAR_NUMERO", numero)

        Return descricao + " " + texto

    End Function
    Private Function buscarPrecoArtigo(artigo As String, entidade As String, tipoentidade As String) As Double
        Dim m As Motor
        m = Motor.GetInstance
        Return m.buscarPrecoArtigo(artigo, entidade, tipoentidade)
    End Function
    'Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

    '    buscarArtigos(gridArtigos, txtArtigo.Text)

    'End Sub

    Private Sub btnDPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDPA.Click
        enviarArtigoParaSaida()
    End Sub

    Private Function enviarArtigoParaSaida() As Boolean
        Dim dtrow As DataRow
        Dim m As Motor
        m = Motor.GetInstance
        Dim formula As String
        Dim botaoSeleccionado As Integer
        Dim valor As Double
        Dim varA, varB, varC, quantidadeFormula As Double
        Dim numero As String
        numero = ""
        varA = -1
        varB = -1
        varC = -1
        quantidadeFormula = -1
        enviarArtigoParaSaida = True

        If gridArtigos.SelectedRows.Count > 0 Then
            Dim i As Integer
            Dim index As Integer
            i = 0

            If Not m.artigoValidoEnc(gridArtigos.SelectedRows(i).Record.Item(COLUMN_ARTIGO).Value, gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Value) Then
                MsgBox("O Artigo selecionado não é está configurado para ser utilizado neste documento", MsgBoxStyle.Information, "Anphis")
                enviarArtigoParaSaida = False
                Exit Function
            End If
            '  For i = 0 To gridArtigos.SelectedRows.Count - 1

            index = gridArtigos.SelectedRows(i).Record.Index
            dtrow = gridArtigos.SelectedRows(i).Record.Tag


            valor = 0
            formula = m.artigoFormula(dtrow("Artigo"), modulo)

            If formula = "" Then
                Dim f As FrmQt
                f = New FrmQt
                f.Valor = 1
                f.ShowDialog()
                botaoSeleccionado = f.BotaoSeleccionado
                valor = f.Valor
            Else
                Dim f As FrmQtFormula
                f = New FrmQtFormula
                f.QuantidadeFormula = 1
                f.Formula = formula
                f.Artigo = dtrow("Artigo")
                f.Descricao = dtrow("Descricao")
                f.VarA = 0
                f.VarB = 0
                f.VarC = 0
                f.ShowDialog()
                botaoSeleccionado = f.BotaoSeleccionado
                valor = f.QuantidadeFormula
                varA = f.VarA
                varB = f.VarB
                varC = f.VarC
                quantidadeFormula = f.QuantidadeFormula
                valor = f.Quantidade
                numero = f.Numero
            End If

            If botaoSeleccionado = 1 Then
                inserirLinhaRegisto(dtrow, gridArtigosStock, valor, , , varA, varB, varC, quantidadeFormula, numero)
                gridArtigosStock.Populate()
                gridArtigos.Populate()
                gridArtigosStock.SelectedRows.DeleteAll()
                gridArtigosStock.SelectedRows.Add(gridArtigosStock.Rows(gridArtigosStock.Rows.Count - 1))
                If index < gridArtigos.Rows.Count Then
                    gridArtigos.SelectedRows.Add(gridArtigos.Rows(index))
                    gridArtigos.Navigator.MoveToRow(index)
                End If
            End If

            lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
            lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
            Me.Activate()
        Else
            enviarArtigoParaSaida = False
        End If
    End Function

    Private Sub btnEPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPA.Click
        retirarArtigoSaida()
    End Sub

    Private Sub retirarArtigoSaida()
        If gridArtigosStock.SelectedRows.Count > 0 Then
            Dim index As Integer
            '  Dim index1 As Integer
            Dim record As ReportRecord
            Dim i As Integer
            i = 0
            ' For i = 0 To gridArtigosStock.SelectedRows.Count - 1
            'ir buscar o index dos artigos a movimentar stock
            index = gridArtigosStock.SelectedRows(i).Record.Index
            'ir buscar o index da  grelha dos artigos 
            'buscar a linha
            record = gridArtigosStock.SelectedRows(i).Record
            gridArtigosStock.Records.RemoveAt(index)
            ' Next i
            gridArtigosStock.Populate()
            '   buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub
    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case 1 : actualizar(True)
            Case 2 : gravarDocumento()
            Case 3 : enviarEmailDocumento()
            Case 4 : abrirEditorRegistos()
            Case 5 : If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then Me.Close()
            Case 6 : imprimirAvaliacaoFornecedor() 'imprimirDoc()
            Case 7 : duplicar()
        End Select
    End Sub
    Private Sub gravarDocumento()
        Dim idDoc As String
        idDoc = ""
        If modulo = "C" Then idDoc = gravarDocumentoCompra(True, gridArtigosStock.Tag, lstDocumentos.SelectedValue)
        If modulo = "V" Then idDoc = gravarDocumentoVenda(True, gridArtigosStock.Tag, lstDocumentos.SelectedValue)
        If idDoc <> "" Then
            If modulo = "C" Then ApresentarPerguntaCompra(idDoc, lstDocumentos.SelectedValue)
            If modulo = "V" Then ApresentarPerguntaVenda(idDoc, lstDocumentos.SelectedValue)
            actualizar()
        End If
    End Sub

    Private Sub actualizar(Optional ByVal pergunta As Boolean = False)

        If pergunta Then
            If MsgBox("Deseja realmente actualizar e limpar a informação existente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
        End If

        dtpData.Value = Now
        txtPesqMolde.Text = ""
        '  txtArtigo.Text = ""
        gridArtigosStock.Tag = ""
        ' gridArtigos.Records.DeleteAll()
        gridArtigosStock.Records.DeleteAll()
        gridArtigosStock.Populate()
        inicializarMoldes()


        ckbInsp1.Checked = False
        ckbInsp2.Checked = False
        ' buscarArtigos(gridArtigos, txtArtigo.Text)
        Dim m As Motor
        m = Motor.GetInstance

        txtEntidade.Text = ""
        txtNomeEntidade.Text = ""

        txtNumContrib.Text = ""
        txtMorada1.Text = ""
        txtMorada2.Text = ""
        txtLocalidade.Text = ""
        txtCodPostal.Text = ""
        txtLocalidadeCodPostal.Text = ""

        txtMorada1Carga.Text = ""
        txtMorada2Carga.Text = ""
        txtLocalidadeCarga.Text = ""
        txtCodPostalCarga.Text = ""
        txtLocalidadeCodPostalCarga.Text = ""

        txtMorada1Descarga.Text = ""
        txtMorada2Descarga.Text = ""
        txtLocalidadeDescarga.Text = ""
        txtCodPostalDescarga.Text = ""
        txtLocalidadeCodPostalDescarga.Text = ""

        actualizarDadosCarga()

        txtObservacoes.Text = ""
        If modulo = "C" Then
            txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
        Else
            txtNumDoc.Value = m.daNumeracaoDocumentoVenda(lstDocumentos.SelectedValue)
        End If

        aplicarTotais(0, 0)


    End Sub

    Private Sub duplicar()
        Dim m As Motor
        m = Motor.GetInstance

        If MsgBox("Deseja realmente duplicar o documento?", MsgBoxStyle.Question + vbYesNo, "Deseja duplicar?") = MsgBoxResult.No Then
            Exit Sub
        End If

        If modulo = "C" Then
            txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
        Else
            txtNumDoc.Value = m.daNumeracaoDocumentoVenda(lstDocumentos.SelectedValue)
        End If
        actualizarDadosCarga()

        dtpData.Value = Now
        gridArtigosStock.Tag = ""

        MsgBox("Documento duplicado com sucesso!", MsgBoxStyle.Information)

    End Sub

    Private Sub actualizarDadosCarga()
        Dim motor As Motor
        motor = Motor.GetInstance
        limparMatricula()
        txtLocalCarga.Text = "N/ Morada"
        txtLocalDescarga.Text = "V/ Morada"
        txtHoraCarga.Text = ""
        txtHoraCarga.Text = FormatDateTime(Now, DateFormat.ShortTime)
        txtHoraCarga.Tag = ""
        txtHoraDescarga.Text = ""
        DtpDataCarga.Value = Now.ToShortDateString
        DtpDataDescarga.Value = Now.ToShortDateString

        txtMorada1Carga.Text = motor.GetMoradas(0)
        txtMorada2Carga.Text = ""
        txtLocalidadeCarga.Text = motor.GetMoradas(1)
        txtCodPostalCarga.Text = motor.GetMoradas(2)
        txtLocalidadeCodPostalCarga.Text = motor.GetMoradas(3)

    End Sub

    Private Function daListaArtigosExcluidos() As String
        daListaArtigosExcluidos = ""
        Dim row As ReportRecord
        '  If gridArtigosStock.Records.Count > 0 Then
        For Each row In gridArtigosStock.Records
            If daListaArtigosExcluidos = "" Then
                daListaArtigosExcluidos = "'" + row.Item(COLUMN_ARTIGO).Value + "'"
            Else
                daListaArtigosExcluidos = daListaArtigosExcluidos + ",'" + row.Item(COLUMN_ARTIGO).Value + "'"
            End If
        Next
    End Function


    Private Function gravarDocumentoCompra(ByVal apresentaPergunta As Boolean, idCabecDoc As String, Optional tipodoc As String = "") As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim descricaoDocumento As String
        Dim segundoDoc As Boolean
        segundoDoc = False
        If tipodoc = "" Then
            tipodoc = lstDocumentos.SelectedValue
        Else
            segundoDoc = True
        End If

        descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")

        If apresentaPergunta Then


            If MsgBox("Deseja realmente gravar a " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        If Not m.existeFornecedor(txtEntidade.Text) Then
            MsgBox("O Fornecedor '" + txtEntidade.Text + "' não existe!", MsgBoxStyle.Critical)
            Return ""
        End If

        Dim documentoT As String
        documentoT = m.consultaValor("SELECT BensCirculacao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")



        If documentoT = "True" And Trim(Replace(txtMatricula.Text, "  -  -", "")) = "" And m.getValidaMatricula Then
            MsgBox("A Matricula não se encontra preenchida!", MsgBoxStyle.Critical)
            Return ""
        End If

        If Not validarGravacao() Then
            Return ""
        End If
        Me.Cursor = Cursors.WaitCursor

        If Not m.getValidaMatricula And Trim(Replace(txtMatricula.Text, "  -  -", "")) = "" Then
            txtMatricula.Text = ""
        End If

        If lstMoldes.SelectedValue = Nothing And segundoDoc = False And m.getApresentaProjectos Then
            If MsgBox("Atenção! Não tem nenhum molde Selecionado!" + vbCrLf + "Deseja Continuar?", MsgBoxStyle.Exclamation + vbYesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        Me.Cursor = Cursors.WaitCursor

        ' Dim idCabecDoc As String
        ' idCabecDoc = gridArtigosStock.Tag
        Dim forn As Entidade
        forn = carregarDadosEntidade("F")

        If idCabecDoc = "" Then
            If txtHoraCarga.Tag = "" Then
                txtHoraCarga.Text = FormatDateTime(DateAdd(DateInterval.Minute, 5, Now), DateFormat.ShortTime)
            End If
        End If


        idCabecDoc = m.actualizarDocumentoCompra(forn, tipodoc, txtEntidade.Text, m.Funcionario, lstMoldes.SelectedValue, txtObservacoes.Text, dtpData.Value, gridArtigosStock.Records, txtMatricula.Text, txtLocalCarga.Text, txtLocalDescarga.Text, DtpDataCarga.Value, DtpDataDescarga.Value, txtHoraCarga.Text, txtHoraDescarga.Text, ckbInsp1.Checked, ckbInsp2.Checked, idCabecDoc)

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" And apresentaPergunta And segundoDoc = False Then
            MsgBox(descricaoDocumento + " gravada realidada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Me.Cursor = Cursors.Default

        Return idCabecDoc
    End Function


    Private Function validarGravacao() As Boolean
        Dim m As Motor
        m = Motor.GetInstance
        validarGravacao = False

        Dim row As ReportRecord
        Dim i As Integer
        Dim valida As String

        For Each row In gridArtigosStock.Records
            For i = 0 To arrCampo.Count - 1
                valida = m.consultaEntradaIniFile("DOCUMENTO", modulo + "_" + lstDocumentos.SelectedValue + "_" + arrCampo(i))
                If valida = "1" And m.NuloToString(row(COLUMN_PROJECTO + i + 1).Value) = "" And m.NuloToString(row(COLUMN_ARTIGO).Value) <> "" Then
                    MsgBox("O campo " + arrNome(i) + " não se encontra preenchido", MsgBoxStyle.Critical)
                    Exit Function
                End If
            Next
        Next

        Return True

    End Function

    Private Function gravarDocumentoVenda(ByVal apresentaPergunta As Boolean, idCabecDoc As String, Optional tipodoc As String = "") As String
        Dim m As Motor
        m = Motor.GetInstance
        Dim descricaoDocumento As String
        Dim segundoDoc As Boolean
        segundoDoc = False
        If tipodoc = "" Then
            tipodoc = lstDocumentos.SelectedValue
        Else
            segundoDoc = True
        End If

        descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosVenda WHERE Documento='" + tipodoc + "'")

        If apresentaPergunta Then
            If MsgBox("Deseja realmente gravar a " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        If Not m.existeCliente(txtEntidade.Text) Then
            MsgBox("O Cliente '" + txtEntidade.Text + "' não existe!", MsgBoxStyle.Critical)
            Return ""
        End If

        Dim documentoT As String
        documentoT = m.consultaValor("SELECT BensCirculacao FROM DocumentosVenda WHERE Documento='" + tipodoc + "'")

        If documentoT = "True" And Trim(Replace(txtMatricula.Text, "  -  -", "")) = "" And m.getValidaMatricula Then
            MsgBox("A Matricula não se encontra preenchida!", MsgBoxStyle.Critical)
            Return ""
        End If

        If Not validarGravacao() Then
            Return ""
        End If

        Me.Cursor = Cursors.WaitCursor

        If Not m.getValidaMatricula Then
            txtMatricula.Text = ""
        End If

        If (lstMoldes.SelectedValue = Nothing) And m.getApresentaProjectos() Then
            If MsgBox("Atenção! Não tem nenhum molde Selecionado!" + vbCrLf + "Deseja Continuar?", MsgBoxStyle.Exclamation + vbYesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        Me.Cursor = Cursors.WaitCursor

        ' Dim idCabecDoc As String
        ' idCabecDoc = gridArtigosStock.Tag
        Dim cli As Entidade
        cli = carregarDadosEntidade("C")

        Dim registo As New Registo
        registo.Entidade = txtEntidade.Text
        registo.TipoEntidade = "C"
        '   idCabecDoc = m.actualizarDocumentoVenda(registo, Now)

        If idCabecDoc = "" Then
            If txtHoraCarga.Tag = "" Then
                txtHoraCarga.Text = FormatDateTime(DateAdd(DateInterval.Minute, 5, Now), DateFormat.ShortTime)
            End If
        End If

        idCabecDoc = m.actualizarDocumentoVenda(cli, tipodoc, txtEntidade.Text, m.Funcionario, lstMoldes.SelectedValue, txtObservacoes.Text, dtpData.Value, gridArtigosStock.Records, txtMatricula.Text, txtLocalCarga.Text, txtLocalDescarga.Text, DtpDataCarga.Value, DtpDataDescarga.Value, txtHoraCarga.Text, txtHoraDescarga.Text, ckbInsp1.Checked, ckbInsp2.Checked, idCabecDoc)

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" And apresentaPergunta And segundoDoc = False Then
            MsgBox(descricaoDocumento + " gravada realidada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Me.Cursor = Cursors.Default

        Return idCabecDoc
    End Function
    Private Function carregarDadosEntidade(tipoentidade As String) As Entidade
        Dim entidade As Entidade
        entidade = New Entidade
        entidade.TipoEntidade = tipoentidade
        entidade.Entidade = txtEntidade.Text
        entidade.Nome = txtNomeEntidade.Text
        entidade.Contribuinte = txtNumContrib.Text
        entidade.Morada = txtMorada1.Text
        entidade.Morada2 = txtMorada2.Text
        entidade.Localidade = txtLocalidade.Text
        entidade.CodPostal = txtCodPostal.Text
        entidade.CodPostalLocalidade = txtLocalidadeCodPostal.Text

        entidade.MoradaCarga = txtMorada1Carga.Text
        entidade.Morada2Carga = txtMorada2Carga.Text
        entidade.LocalidadeCarga = txtLocalidadeCarga.Text
        entidade.CodPostalCarga = txtCodPostalCarga.Text
        entidade.CodPostalLocalidadeCarga = txtLocalidadeCodPostalCarga.Text

        entidade.MoradaDescarga = txtMorada1Descarga.Text
        entidade.Morada2Descarga = txtMorada2Descarga.Text
        entidade.LocalidadeDescarga = txtLocalidadeDescarga.Text
        entidade.CodPostalDescarga = txtCodPostalDescarga.Text
        entidade.CodPostalLocalidadeDescarga = txtLocalidadeCodPostalDescarga.Text

        Return entidade
    End Function

    Private Sub gridArtigos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigos.RowDblClick
        ' enviarArtigoParaSaida()
    End Sub

    Private Sub gridArtigosStock_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigosStock.RowDblClick
        ' retirarArtigoSaida()     
        Dim formula As String
        Dim m As Motor
        m = Motor.GetInstance
        If CStr(e.row.Record.Item(COLUMN_QUANTIDADE).Value) <> "" Then

            formula = m.artigoFormula(e.row.Record.Item(COLUMN_ARTIGO).Value, modulo)
            If formula = "" Then
                Dim f As FrmQt
                f = New FrmQt
                f.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
                f.ShowDialog()
                If f.BotaoSeleccionado = 1 Then
                    e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Valor
                End If
                f = Nothing
            Else
                Dim formuLinha As FormulaLinha
                Dim f As FrmQtFormula
                formuLinha = e.row.Record.Item(COLUMN_ARTIGO).Tag
                f = New FrmQtFormula
                f.Artigo = e.row.Record.Item(COLUMN_ARTIGO).Value
                f.Descricao = e.row.Record.Item(COLUMN_DESCRICAO).Tag
                f.QuantidadeFormula = formuLinha.QuantidadeFormula
                '  f.Quantidade = formuLinha.Quantidade
                f.Formula = formula
                f.Numero = formuLinha.Numero
                f.VarA = formuLinha.VarA
                f.VarB = formuLinha.VarB
                f.VarC = formuLinha.VarC
                f.ShowDialog()
                If f.BotaoSeleccionado = 1 Then
                    e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Quantidade
                    e.row.Record.Item(COLUMN_DESCRICAO).Value = calcularDescricao(f.Descricao, f.VarA, f.VarB, f.VarC, f.QuantidadeFormula, f.Numero)
                    '   e.row.Record.Item(COLUMN_DESCRICAO).Caption = e.row.Record.Item(COLUMN_DESCRICAO).Value
                    formuLinha.Numero = f.Numero
                    formuLinha.VarA = f.VarA
                    formuLinha.VarB = f.VarB
                    formuLinha.VarC = f.VarC
                    formuLinha.Quantidade = f.Quantidade
                    formuLinha.QuantidadeFormula = f.QuantidadeFormula

                End If
                f = Nothing
            End If
            Me.Activate()
            gridArtigosStock.Populate()
            actualizarTotaisDocumento(gridArtigosStock.Tag, gridArtigosStock)
        End If
    End Sub


    Private Sub abrirEditorRegistos()
        Dim f As FrmEditarEncomendas
        f = New FrmEditarEncomendas
        f.TipoDocumento = lstDocumentos.SelectedValue
        f.Modulo = modulo
        f.ShowDialog()
        If f.clicouConfirmar Then
            Dim idLinha As String
            idLinha = f.getIdLinha()
            If idLinha <> "" Then actualizarFormulario(idLinha, f.getValorLinha(0), f.getValorLinha(6))
        End If
    End Sub

    Dim emModoEdicao As Boolean
    Private Sub actualizarFormulario(ByVal id As String, ByVal documento As String, ByVal projecto As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosStock.Tag = id
        Dim m As Motor
        m = Motor.GetInstance
        Dim doc As Object

        Dim entidade As String
        Dim nome As String
        Dim observacoes As String
        Dim str As String()
        entidade = ""
        nome = ""
        observacoes = ""

        If modulo = "C" Then
            doc = m.buscarDocumentoCompra(id)
        Else
            doc = m.buscarDocumentoVenda(id)
        End If

        'm.daEntidadeDocumento(id, entidade, nome, observacoes, "C")
        txtEntidade.Text = doc.Entidade '  entidade
        txtNomeEntidade.Text = doc.Nome  'nome
        dtpData.Value = doc.DataDoc


        txtMorada1.Text = doc.Morada
        txtMorada2.Text = doc.Morada2
        txtNumContrib.Text = doc.NumContribuinte
        txtLocalidade.Text = doc.Localidade
        txtCodPostal.Text = doc.CodigoPostal
        txtLocalidadeCodPostal.Text = doc.LocalidadeCodigoPostal

        Dim lista As StdBELista
        If modulo = "C" Then
            lista = m.consulta("SELECT MoradaCarga,Morada2Carga,LocalidadeCarga,CodPostalCarga,CodPostalLocalidadeCarga FROM CabecCompras WHERE id='" + doc.Id + "'")
        Else
            lista = m.consulta("SELECT MoradaCarga,Morada2Carga,LocalidadeCarga,CodPostalCarga,CodPostalLocalidadeCarga FROM CabecDoc WHERE id='" + doc.Id + "'")
        End If

        If Not lista.NoFim Then
            txtMorada1Carga.Text = lista.Valor("MoradaCarga")
            txtMorada2Carga.Text = lista.Valor("Morada2Carga")
            txtLocalidadeCarga.Text = lista.Valor("LocalidadeCarga")
            txtCodPostalCarga.Text = lista.Valor("CodPostalCarga")
            txtLocalidadeCodPostalCarga.Text = lista.Valor("CodPostalLocalidadeCarga")
        End If

        txtHoraCarga.Text = doc.HoraCarga
        txtLocalCarga.Text = doc.LocalCarga

        txtMorada1Descarga.Text = doc.MoradaEntrega
        txtMorada2Descarga.Text = doc.Morada2Entrega
        txtLocalidadeDescarga.Text = doc.LocalidadeEntrega
        txtCodPostalDescarga.Text = doc.CodPostalEntrega
        txtLocalidadeCodPostalDescarga.Text = doc.CodPostalLocalidadeEntrega

        txtMatricula.Text = doc.Matricula
        txtHoraDescarga.Text = doc.HoraDescarga
        txtLocalDescarga.Text = doc.LocalDescarga

        If IsDate(doc.DataCarga) Then
            DtpDataCarga.Value = doc.DataCarga
        Else
            DtpDataCarga.Value = CDate(Now)
        End If

        If IsDate(doc.DataDescarga) Then
            DtpDataDescarga.Value = doc.DataDescarga
        Else
            DtpDataDescarga.Value = CDate(Now)
        End If


        txtNumDoc.Value = doc.NumDoc

        'str = Split(observacoes, vbLf)
        str = Split(doc.Observacoes, vbLf)
        txtObservacoes.Lines = str
        Dim dtRegistos As DataTable

        If m.existeArtigo1(id) Then
            ckbInsp1.Checked = True
        End If

        If m.existeArtigo2(id) Then
            ckbInsp2.Checked = True
        End If

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(id, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(id, "V")

        ' dt = m.daDataDocumento(id, dtpData.Value, "C")

        Dim dtrow As DataRow
        gridArtigosStock.Records.DeleteAll()
        emModoEdicao = True
        Dim numero As String
        Dim formula As String
        For Each dtrow In dtRegistos.Rows
            numero = ""
            If lstMoldes.SelectedValue Is Nothing Then
                If m.NuloToString(dtrow("Codigo")) <> "" Then lstMoldes.SelectedValue = m.NuloToString(dtrow("Codigo"))
            End If

            If dtRegistos.Columns.IndexOf(m.getCampoFormulaNumero) <> -1 Then
                numero = IIf(IsDBNull(dtrow(m.getCampoFormulaNumero)), "", dtrow(m.getCampoFormulaNumero))
            End If
            formula = m.artigoFormula(m.NuloToString(dtrow("Artigo")), modulo)
            inserirLinhaRegisto(dtrow, gridArtigosStock, dtrow("Quantidade"), IIf(dtrow("PrecUnit") < 0, dtrow("PrecUnit") * -1, dtrow("PrecUnit")), dtrow("Desconto1"), dtrow("VariavelA"), dtrow("VariavelB"), dtrow("VariavelC"), IIf(formula = "", -1, dtrow("QntFormula")), numero)
        Next
        emModoEdicao = False
        txtArtigo.Text = ""

        'buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        gridArtigosStock.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"

        actualizarTotaisDocumento(id, gridArtigosStock)

        Me.Cursor = Cursors.Default
    End Sub


    Private Sub enviarEmailDocumento()
        Dim idDocumento As String
        Dim m As Motor
        m = Motor.GetInstance
        idDocumento = ""
        Me.Cursor = Cursors.WaitCursor
        If modulo = "C" Then idDocumento = gravarDocumentoCompra(False, gridArtigosStock.Tag)
        If modulo = "V" Then idDocumento = gravarDocumentoVenda(False, gridArtigosStock.Tag)
        If idDocumento <> "" Then
            m.enviarEmailDocumento(idDocumento, modulo)
            If modulo = "C" Then ApresentarPerguntaCompra(idDocumento, lstDocumentos.SelectedValue)
            If modulo = "V" Then ApresentarPerguntaCompra(idDocumento, lstDocumentos.SelectedValue)
            actualizar()
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub imprimirAvaliacaoFornecedor()


        Try
            imprimirMapa("TJ_AVFOR", "W", "TJM")
            'imprimirDetalhes("TJ_AVFOR", "W", Motor.GetInstance.getEmpresa)
        Catch ex As Exception
            MsgBox("Erro ao imprimir o mapa de Avaliação: " + ex.Message, MsgBoxStyle.Exclamation)
        End Try


    End Sub


    Private Sub imprimirMapa(ByVal report As String, ByVal destiny As String, codEmpresa As String)
        Try
            Dim PlataformaPrimavera As StdPlatBS
            Dim fileINI As String
            Dim ficheiroConfiguracao As Configuracao
            Dim objConfApl As StdBSConfApl

            PlataformaPrimavera = New StdPlatBS

            Dim ficheiroIni As IniFile
            Dim seg As Seguranca = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

            ficheiroConfiguracao = Configuracao.GetInstance
            ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)


            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "UTILIZADOR", ""))
            objConfApl.PwdUtilizador = seg.decifrarStringTripleDES(ficheiroIni.GetString("ERP", "PASSWORD", ""))
            objConfApl.LicVersaoMinima = "9.00"

            PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")

            Dim p As New System.Windows.Forms.PrintDialog
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, 2)

            PlataformaPrimavera.Mapas.Inicializar("ERP")

            PlataformaPrimavera.Mapas.DefinicaoImpressoraEx2(p.PrinterSettings.PrinterName, "winspool", "winspool", "", CRPEOrientacaoFolha.ofPortrait, 1, CRPETipoFolha.tfA4, 23, 23, 1, p.PrinterSettings.Duplex, 1, 1, 0)

            PlataformaPrimavera.Mapas.ImprimeListagem(report, "Mapa", destiny, 1, "S", , , , , , True)

            'PlataformaPrimavera.Mapas.ImprimeListagem(sReport:=report, sTitulo:="Mapa", sDestino:=destiny, iNumCopias:=1, sDocumento:="S", blnModal:=True)
            PlataformaPrimavera.Mapas.TerminaJanelas()

            PlataformaPrimavera.FechaPlataformaEmpresa()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub imprimirDoc()
        Dim idDocumento As String
        Dim m As Motor
        m = Motor.GetInstance
        idDocumento = ""
        Me.Cursor = Cursors.WaitCursor
        If modulo = "C" Then idDocumento = gravarDocumentoCompra(False, gridArtigosStock.Tag)
        If modulo = "V" Then idDocumento = gravarDocumentoVenda(False, gridArtigosStock.Tag)
        If idDocumento <> "" Then
            m.imprimirDocumento(idDocumento, modulo, Me)
            If modulo = "C" Then ApresentarPerguntaCompra(idDocumento, lstDocumentos.SelectedValue)
            If modulo = "V" Then ApresentarPerguntaVenda(idDocumento, lstDocumentos.SelectedValue)
            actualizar()
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnForn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnForn.Click

        abrirFormLista()

    End Sub



    Private Sub abrirFormLista()
        Dim frmLista As FrmLista
        frmLista = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(4)
        If modulo = "C" Then
            frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
            frmLista.setCaption("Fornecedores")
        Else
            frmLista.setComando("SELECT TOP 100 PERCENT Cliente,Nome, Fac_Mor as Morada,Fac_Tel as 'Telefone', Fac_Fax FROM Clientes WHERE ClienteAnulado=0 ORDER BY Cliente")
            frmLista.setCaption("Clientes")
        End If
        ' frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
        ' frmLista.setCaption("Fornecedores")
        frmLista.ShowFilter(True)
        frmLista.setalinhamentoColunas(alinhamentoColunas)
        frmLista.ShowDialog()

        If frmLista.seleccionouOK Then
            txtEntidade.Text = frmLista.getValor(0)
            '  actualizarDadosFornecedor(frmLista.getValor(0))
        End If
    End Sub

    Private Sub actualizarDadosFornecedor(fornecedor As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.consulta("SELECT Nome,NumContrib from fornecedores where fornecedor='" + fornecedor + "'")
        If Not lista.NoFim Then
            txtNomeEntidade.Text = lista.Valor("Nome")
            txtNumContrib.Text = lista.Valor("NumContrib")
            carregarMoradasFornecedor(fornecedor)
        End If
    End Sub

    Private Sub actualizarDadosCliente(cliente As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.consulta("SELECT Nome,NumContrib from Clientes where cliente='" + cliente + "'")
        If Not lista.NoFim Then
            txtNomeEntidade.Text = lista.Valor("Nome")
            txtNumContrib.Text = lista.Valor("NumContrib")
            carregarMoradasCliente(cliente)
        End If
    End Sub


    Private Sub carregarMoradasFornecedor(fornecedor As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstMoradas.SelectedIndex = -1
        lista = m.consulta("SELECT 'Principal' as MoradaAlternativa UNION SELECT MoradaAlternativa FROM MoradasAlternativasFornecedores where fornecedor='" + fornecedor + "'")
        Dim dt As DataTable
        'MsgBox(lista.NumLinhas)
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        '  Dim row As DataRow
        ' row = dt.NewRow()
        ''  row("MoradaAlternativa") = "Principal"
        ' dt.Rows.Add(row)
        lstMoradas.ValueMember = "MoradaAlternativa"
        lstMoradas.DisplayMember = "MoradaAlternativa"
        lstMoradas.DataSource = dt
        lstMoradas.SelectedValue = "Principal"

    End Sub

    Private Sub carregarMoradasCliente(cliente As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstMoradas.SelectedIndex = -1
        lista = m.consulta("SELECT 'Principal' as MoradaAlternativa UNION SELECT MoradaAlternativa FROM MoradasAlternativasClientes where cliente='" + cliente + "'")
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstMoradas.ValueMember = "MoradaAlternativa"
        lstMoradas.DisplayMember = "MoradaAlternativa"
        lstMoradas.DataSource = dt
        lstMoradas.SelectedValue = "Principal"

    End Sub



    Private Sub actualizarMoradasFornecedor(fornecedor As String, moradaAlternativa As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        If moradaAlternativa = "Principal" Then
            lista = m.consulta("SELECT  Morada,Morada2,Local,Cp,cpLoc from fornecedores where fornecedor='" + fornecedor + "'")
        Else
            lista = m.consulta("SELECT  Morada,Morada2,Localidade as Local,Cp,cpLocalidade as cpLoc  from MoradasAlternativasFornecedores where fornecedor='" + fornecedor + "' and MoradaAlternativa='" + moradaAlternativa + "'")
        End If
        txtMorada1.Text = ""
        txtMorada2.Text = ""
        txtLocalidade.Text = ""
        txtCodPostal.Text = ""
        txtLocalidadeCodPostal.Text = ""

        txtMorada1Descarga.Text = ""
        txtMorada2Descarga.Text = ""
        txtLocalidadeDescarga.Text = ""
        txtCodPostalDescarga.Text = ""
        txtLocalidadeCodPostalDescarga.Text = ""

        If Not lista.NoFim Then
            txtMorada1.Text = lista.Valor("Morada")
            txtMorada2.Text = lista.Valor("Morada2")
            txtLocalidade.Text = lista.Valor("Local")
            txtCodPostal.Text = lista.Valor("Cp")
            txtLocalidadeCodPostal.Text = lista.Valor("CpLoc")

            txtMorada1Descarga.Text = lista.Valor("Morada")
            txtMorada2Descarga.Text = lista.Valor("Morada2")
            txtLocalidadeDescarga.Text = lista.Valor("Local")
            txtCodPostalDescarga.Text = lista.Valor("Cp")
            txtLocalidadeCodPostalDescarga.Text = lista.Valor("CpLoc")

        End If
    End Sub


    Private Sub actualizarMoradasCliente(cliente As String, moradaAlternativa As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        If moradaAlternativa = "Principal" Then
            lista = m.consulta("SELECT  Fac_Mor as Morada,Fac_Mor2 as Morada2,fac_Local as Local,Fac_Cp as Cp,Fac_cpLoc as CpLoc from Clientes where cliente='" + cliente + "'")
        Else
            lista = m.consulta("SELECT  Morada,Morada2,Localidade as Local,Cp,cpLocalidade as cpLoc  from MoradasAlternativasClientes where cliente='" + cliente + "' and MoradaAlternativa='" + moradaAlternativa + "'")
        End If

        txtMorada1.Text = ""
        txtMorada2.Text = ""
        txtLocalidade.Text = ""
        txtCodPostal.Text = ""
        txtLocalidadeCodPostal.Text = ""

        txtMorada1Descarga.Text = ""
        txtMorada2Descarga.Text = ""
        txtLocalidadeDescarga.Text = ""
        txtCodPostalDescarga.Text = ""
        txtLocalidadeCodPostalDescarga.Text = ""

        If Not lista.NoFim Then
            txtMorada1.Text = lista.Valor("Morada")
            txtMorada2.Text = lista.Valor("Morada2")
            txtLocalidade.Text = lista.Valor("Local")
            txtCodPostal.Text = lista.Valor("Cp")
            txtLocalidadeCodPostal.Text = lista.Valor("CpLoc")

            txtMorada1Descarga.Text = lista.Valor("Morada")
            txtMorada2Descarga.Text = lista.Valor("Morada2")
            txtLocalidadeDescarga.Text = lista.Valor("Local")
            txtCodPostalDescarga.Text = lista.Valor("Cp")
            txtLocalidadeCodPostalDescarga.Text = lista.Valor("CpLoc")


        End If
    End Sub

    Private Sub txtFornecedor_KeyUpEvent(ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DFlatEditEvents_KeyUpEvent) Handles txtEntidade.KeyUpEvent
        If e.keyCode = Keys.F4 Then abrirFormLista()
    End Sub

    Private Sub txtNumDoc_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumDoc.Leave
        Dim m As Motor
        m = Motor.GetInstance
        Dim projecto As String
        Dim idCabecDoc As String
        projecto = ""
        If modulo = "C" Then
            idCabecDoc = m.existeDocumentoCompra(lstDocumentos.SelectedValue, txtNumDoc.Value, projecto)
        Else
            idCabecDoc = m.existeDocumentoVenda(lstDocumentos.SelectedValue, txtNumDoc.Value, projecto)
        End If

        If idCabecDoc <> "" Then
            actualizarFormulario(idCabecDoc, "", projecto)
        Else
            actualizar(False)
        End If
    End Sub



    Private Sub lstDocumentos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstDocumentos.SelectedIndexChanged
        Try

            Dim m As Motor
            Dim modAux As String
            m = Motor.GetInstance
            Dim dtRow As DataRowView = lstDocumentos.SelectedItem
            If dtRow Is Nothing Then Exit Sub
            modAux = modulo
            modulo = dtRow("Modulo")
            If dtRow("Modulo") = "C" Then
                lblFornecedor.Text = "Fornecedor"
                txtNumDoc.Minimum = 0 'm.daValorNumeracaoMinimaDocumentoCompra(lstDocumentos.SelectedValue)
                txtNumDoc.Maximum = 999999999 'm.daValorNumeracaoMaximaDocumentoCompra(lstDocumentos.SelectedValue)
                txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
            Else
                lblFornecedor.Text = "Cliente"
                txtNumDoc.Minimum = 0 'm.daValorNumeracaoMinimaDocumentoVenda(lstDocumentos.SelectedValue)
                txtNumDoc.Maximum = 999999999 ' m.daValorNumeracaoMaximaDocumentoVenda(lstDocumentos.SelectedValue)
                txtNumDoc.Value = m.daNumeracaoDocumentoVenda(lstDocumentos.SelectedValue)
            End If
            actualizar(False)
            If modAux <> modulo Then buscarArtigos(gridArtigos, txtArtigo.Text)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub txtArtigo_KeyDownEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyDownEvent)
        If e.keyCode = Keys.Enter Then
            filtrarArtigos(gridArtigos, txtArtigo.Text) '  buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub

    'Private Sub txtArtigo_Change(sender As Object, e As EventArgs) Handles txtArtigo.Change
    '    If txtArtigo.Text = "" Then
    '        buscarArtigos(gridArtigos, txtArtigo.Text)
    '    End If
    'End Sub
    Private Sub filtrarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim row As ReportRecord
        pesquisa = Trim(pesquisa)
        Me.Cursor = Cursors.WaitCursor

        Dim m As Motor
        m = Motor.GetInstance

        '   If Not m.existeArtigo(pesquisa) Then
        For Each row In grid.Records
            row.Visible = True
            If pesquisa <> "" Then
                If InStr(LCase(row(COLUMN_ARTIGO).Value), LCase(pesquisa)) = 0 And InStr(LCase(row(COLUMN_DESCRICAO).Value), LCase(pesquisa)) = 0 Then
                    row.Visible = False
                End If
            End If
        Next
        grid.Populate()


        gridArtigos.SelectedRows.DeleteAll()

        If gridArtigos.Rows.Count > 0 Then
            gridArtigos.SelectedRows.Add(gridArtigos.Rows(0))
        End If

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
        'Else
        'enviarArtigoParaSaida()
        'txtArtigo.Text = ""
        'aplicarFiltro()

        'End If

        If gridArtigos.Rows.Count > 0 Then
            If LCase(gridArtigos.Rows(0).Record(COLUMN_ARTIGO).Value) = LCase(pesquisa) Then
                If enviarArtigoParaSaida() Then
                    txtArtigo.Text = ""
                    aplicarFiltro()
                End If
            End If
        End If

    End Sub


    Private Sub aplicarFiltro()
        filtrarArtigos(gridArtigos, Replace(txtArtigo.Text, "$", " "))
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click
        aplicarFiltro()
    End Sub

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        enviarEmailDocumento()
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        imprimirDoc()
    End Sub

    Private Sub ckbInsp2_1_CheckedChanged(sender As Object, e As EventArgs) Handles ckbInsp1.CheckedChanged
        If ckbInsp1.Checked Then
            ckbInsp1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checked1
        Else
            ckbInsp1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        End If
    End Sub


    Private Sub ckbInsp3_1_CheckedChanged(sender As Object, e As EventArgs) Handles ckbInsp2.CheckedChanged
        If ckbInsp2.Checked Then
            ckbInsp2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checked1
        Else
            ckbInsp2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        End If
    End Sub

    Private Sub lstMoldes_SelectedValueChanged(sender As Object, e As EventArgs) Handles lstMoldes.SelectedValueChanged
        If gridArtigosStock.SelectedRows.Count > 0 And Not emModoEdicao And Not lstMoldes.SelectedValue Is Nothing Then
            Dim row As ReportRow
            Dim index As Integer
            row = gridArtigosStock.SelectedRows(0)
            index = row.Index
            row.Record(COLUMN_PROJECTO).Value = lstMoldes.SelectedValue
            row.Record(COLUMN_PROJECTO).Caption = lstMoldes.SelectedValue
            gridArtigosStock.Populate()
            gridArtigosStock.SelectedRows.DeleteAll()
            gridArtigosStock.SelectedRows.Add(gridArtigosStock.Rows(index))
        End If
        '   Item = record.AddItem(lstMoldes.SelectedValue)

    End Sub

    Private Sub ApresentarPerguntaCompra(idDocOrigem As String, tipoDocOrig As String)
        Dim tipodocDest As String
        Dim descricaotipodocDest As String
        Dim idDocumento As String
        Dim m As Motor
        m = Motor.GetInstance
        idDocumento = ""
        Dim refOrig As String
        tipodocDest = m.consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosCompra Where Documento='" + tipoDocOrig + "'")

        If tipodocDest <> "" Then
            descricaotipodocDest = m.consultaValor("SELECT Descricao FROM DocumentosCompra Where Documento='" + tipodocDest + "'")
            Dim frmP As FrmPergunta
            frmP = New FrmPergunta
            frmP.setTexto("Deseja criar o documento " + descricaotipodocDest + "?")
            frmP.ShowDialog()

            refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie +'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocOrigem + "'")

            idDocumento = m.consultaValor("SELECT id FROM CabecCompras WHERE NumDocExterno='" + refOrig + "'")

            ''SOMENTE GRAVAR
            If frmP.getBotaoSeleccionado() = 0 Then
                idDocumento = gravarDocumentoCompra(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
                End If
            End If

            'IMPRIMIR
            If frmP.getBotaoSeleccionado() = 1 Then
                idDocumento = gravarDocumentoCompra(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
                    m.imprimirDocumento(idDocumento, "C", Me)
                End If
            End If

            'ENVIAR
            If frmP.getBotaoSeleccionado() = 2 Then
                idDocumento = gravarDocumentoCompra(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
                    m.enviarEmailDocumento(idDocumento, "C")
                End If
            End If
        End If

    End Sub

    Private Sub ApresentarPerguntaVenda(idDocOrigem As String, tipoDocOrig As String)
        Dim tipodocDest As String
        Dim descricaotipodocDest As String
        Dim idDocumento As String
        Dim m As Motor
        m = Motor.GetInstance
        idDocumento = ""
        Dim refOrig As String
        tipodocDest = m.consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosVenda Where Documento='" + tipoDocOrig + "'")

        If tipodocDest <> "" Then
            descricaotipodocDest = m.consultaValor("SELECT Descricao FROM DocumentosVenda Where Documento='" + tipodocDest + "'")
            Dim frmP As FrmPergunta
            frmP = New FrmPergunta
            frmP.setTexto("Deseja criar o documento " + descricaotipodocDest + "?")
            frmP.ShowDialog()

            refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie +'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocOrigem + "'")

            idDocumento = m.consultaValor("SELECT id FROM CabecDoc WHERE requisicao='" + refOrig + "'")

            ''SOMENTE GRAVAR
            If frmP.getBotaoSeleccionado() = 0 Then
                idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
                End If
            End If

            'IMPRIMIR
            If frmP.getBotaoSeleccionado() = 1 Then
                idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
                    m.imprimirDocumento(idDocumento, "V", Me)
                End If
            End If

            'ENVIAR
            If frmP.getBotaoSeleccionado() = 2 Then
                idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
                    m.enviarEmailDocumento(idDocumento, "V")
                End If
            End If
        End If

    End Sub

    Private Sub actualizarReferenciasDocumentoCompra(idDocOrigem As String, idDocumento As String)
        Dim refOrig As String
        Dim refDest As String
        Dim m As Motor
        m = Motor.GetInstance

        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocOrigem + "'")
        refDest = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocumento + "'")

        m.executarComando("UPDATE CabecCompras set NumDocExterno='" + refDest + "' WHERE Id='" + idDocOrigem + "'")
        m.executarComando("UPDATE CabecCompras set NumDocExterno='" + refOrig + "' WHERE Id='" + idDocumento + "'")

    End Sub

    Private Sub actualizarReferenciasDocumentoVenda(idDocOrigem As String, idDocumento As String)
        Dim refOrig As String
        Dim refDest As String
        Dim m As Motor
        m = Motor.GetInstance

        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocOrigem + "'")
        refDest = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocumento + "'")

        m.executarComando("UPDATE CabecDoc set requisicao='" + refDest + "' WHERE Id='" + idDocOrigem + "'")
        m.executarComando("UPDATE CabecDoc set requisicao='" + refOrig + "' WHERE Id='" + idDocumento + "'")

    End Sub


    Private Sub txtFornecedor_Change(sender As Object, e As EventArgs) Handles txtEntidade.Change
        Dim nome As String
        Dim m As Motor
        m = Motor.GetInstance
        nome = ""
        If modulo = "C" Then nome = m.consultaValor("SELECT nome from Fornecedores where fornecedor like '" + txtEntidade.Text + "'")
        If modulo = "V" Then nome = m.consultaValor("SELECT nome from Clientes where cliente like '" + txtEntidade.Text + "'")

        txtNomeEntidade.Text = nome
        If nome <> "" Then
            If modulo = "C" Then actualizarDadosFornecedor(txtEntidade.Text)
            If modulo = "V" Then actualizarDadosCliente(txtEntidade.Text)
        End If
    End Sub


    Private Sub lstMoradas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstMoradas.SelectedIndexChanged
        If modulo = "C" Then actualizarMoradasFornecedor(txtEntidade.Text, lstMoradas.SelectedValue)
        If modulo = "V" Then actualizarMoradasCliente(txtEntidade.Text, lstMoradas.SelectedValue)
    End Sub





    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        If TabControl1.SelectedIndex = 3 Then
            txtMatricula.SelectionStart = 0
            txtMatricula.Focus()
            txtMatricula.SelectionStart = 0
        End If
    End Sub

    Private Sub Label18_Click(sender As Object, e As EventArgs) Handles Label18.Click

    End Sub

    Private Sub txtHoraCarga_TextChanged(sender As Object, e As EventArgs)
        txtHoraCarga.Tag = "1"
    End Sub

    Private Sub txtHoraCarga_TextChanged_1(sender As Object, e As EventArgs) Handles txtHoraCarga.TextChanged
        txtHoraCarga.Tag = "1"
    End Sub

    Private Sub gridArtigosStock_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles gridArtigosStock.PreviewKeyDown
        If e.KeyCode = Keys.F4 Then
            If gridArtigosStock.FocusedColumn Is Nothing Then Exit Sub
            If gridArtigosStock.FocusedColumn.Tag = "" Then Exit Sub
            Dim frmLista As FrmLista
            frmLista = New FrmLista
            ' Dim alinhamentoColunas() As Integer
            ' ReDim alinhamentoColunas(4)
            frmLista.setComando(gridArtigosStock.FocusedColumn.Tag)
            frmLista.setCaption(gridArtigosStock.FocusedColumn.Caption)
            ' frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
            ' frmLista.setCaption("Fornecedores")
            frmLista.ShowFilter(True)
            '   FrmLista.setalinhamentoColunas(alinhamentoColunas)
            frmLista.ShowDialog()

            If frmLista.seleccionouOK Then
                gridArtigosStock.SelectedRows(0).Record(gridArtigosStock.FocusedColumn.Index).Value = frmLista.getValor(0)
                gridArtigosStock.SelectedRows(0).Record(gridArtigosStock.FocusedColumn.Index).Caption = frmLista.getValor(0)
                'actualizarDadosFornecedor(frmLista.getValor(0))
            End If

        End If
    End Sub


    Private Sub txtArtigo_KeyDown(sender As Object, e As KeyEventArgs) Handles txtArtigo.KeyDown
        If e.KeyCode = Keys.Enter Then
            aplicarFiltro()
        End If
    End Sub

    Private Sub validarArtigosStockMinimo()
        Dim dtRegistos As DataTable
        Dim m As Motor = Motor.GetInstance()
        Dim listaExclusao As String
        Dim consulta As String = ""

        consulta = "SELECT TOP 100 PERCENT Artigo,Descricao as 'Descrição', StkMinimo  as StkMinimo ,StkActual as StkActual,"
        consulta += " ISNULL((select -sum(LinhasComprasStatus.Quantidade-LinhasComprasStatus.QuantTrans)  from LinhasCompras inner join CabecCompras on linhascompras.IdCabecCompras=cabeccompras.id inner join LinhasComprasStatus on linhascompras.Id=LinhasComprasStatus.IdLinhasCompras WHERE TipoDoc in (" + m.getDocumentosEncomenda + ") and Serie>='2015'  and LinhasComprasStatus.EstadoTrans='P' and LinhasComprasStatus.Fechado=0 and Linhascompras.Artigo=VISTA.Artigo),0) as 'Qtd. Enc.'"
        consulta += " FROM " + m.daViewModulo("C") + " as VISTA"
        consulta += " WHERE StkMinimo>StkActual+ISNULL((select -sum(LinhasComprasStatus.Quantidade-LinhasComprasStatus.QuantTrans)  from LinhasCompras inner join CabecCompras on linhascompras.IdCabecCompras=cabeccompras.id inner join LinhasComprasStatus on linhascompras.Id=LinhasComprasStatus.IdLinhasCompras inner join DocumentosCompra ON CabecCompras.Tipodoc=DocumentosCompra.Documento WHERE (DocumentosCompra.TipoDocumento=2 or TipoDoc in (" + m.getDocumentosEncomenda + ")) and Serie>='2015'  and LinhasComprasStatus.EstadoTrans='P' and LinhasComprasStatus.Fechado=0 and Linhascompras.Artigo=VISTA.Artigo),0) "
        consulta += " AND ArtigoAnulado=0 ORDER BY ARTIGO"


        listaExclusao = "" 'daListaArtigosExcluidos()
        dtRegistos = m.consultaDataTable(consulta)   'm.daListaArtigos(m.daViewModulo(modulo), pesquisa, "", "")"

        If dtRegistos.Rows.Count > 0 Then
            Dim f As FrmLista
            f = New FrmLista
            Dim alinhamentoColunas() As Integer
            ReDim alinhamentoColunas(4)
            f.setComando(consulta)
            f.setCaption("Artigos Abaixo Stock Minimo")
            f.ShowFilter(True)
            f.setalinhamentoColunas(alinhamentoColunas)
            f.ShowDialog()
        End If
    End Sub


    Private Sub FrmEncomendas_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        lstMoldes.Location = New Point(Me.Width - lstMoldes.Width - 30, lstMoldes.Location.Y)

        txtPesqMolde.Location = New Point(lstMoldes.Location.X, txtPesqMolde.Location.Y)
        btnEnviar.Location = New Point(Me.Width - btnEnviar.Width - 30, btnEnviar.Location.Y)
        btnImprimir.Location = New Point(btnEnviar.Location.X - btnImprimir.Width, btnEnviar.Location.Y)

        lblMoldes.Location = New Point(lstMoldes.Location.X, lblMoldes.Location.Y)
        lstDocumentos.Location = New Point(btnImprimir.Location.X - lstDocumentos.Size.Width - 10, btnImprimir.Location.Y)
        '  dtpData.Location = New Point(lstDocumentos.Location.X + lstDocumentos.Width - dtpData.Width, dtpData.Location.Y)
        ' lblData.Left = dtpData.Location.X - lblData.Width - 5
        Dim m As Motor
        m = Motor.GetInstance

        If m.getApresentaProjectos Then
            gridArtigosStock.Width = lstMoldes.Location.X - gridArtigosStock.Location.X - 15
        Else
            gridArtigosStock.Width = Me.Width - gridArtigosStock.Left - 25
        End If

        gridArtigos.Height = Me.Height - gridArtigos.Location.Y - 60
        gridArtigosStock.Height = Me.Height - gridArtigosStock.Location.Y - 60
        lstMoldes.Height = Me.Height - lstMoldes.Location.Y - 60
        lblRegisto1.Top = Me.Height - 60
        lblRegisto2.Top = Me.Height - 60
        lblRegisto1.Left = gridArtigos.Location.X
        lblRegisto2.Left = gridArtigosStock.Location.X

        txtObservacoes.Width = gridArtigosStock.Width
        txtObservacoes.Left = gridArtigosStock.Left
        btnImportarLista.Left = txtObservacoes.Left + txtObservacoes.Width - btnImportarLista.Width
        btnRepartirCustos.Left = btnImportarLista.Left - btnRepartirCustos.Width - 10

        btnAplicarTodos.Left = txtObservacoes.Left + txtObservacoes.Width - btnAplicarTodos.Width


        GroupBox1.Width = lstMoldes.Width
        GroupBox1.Left = lstMoldes.Left

        btnImportarLista.Left = btnImprimir.Left - btnImportarLista.Width - 10
        btnRepartirCustos.Left = btnImportarLista.Left - btnRepartirCustos.Width - 10
    End Sub

    Private Sub gridArtigosStock_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigosStock.ValueChanged

        Dim m As MotorAPL
        m = New MotorAPL(Motor.GetInstance())
        Dim type As String
        Dim tabela As String
        tabela = daTabelaModulo()

        If e.column.Index > COLUMN_PROJECTO Then
            If eCampoData(tabela, arrCampo(e.column.Index - COLUMN_PROJECTO - 1)) Then
                If Not IsDate(e.item.Value) Then
                    MsgBox("Data Inválida", MsgBoxStyle.Critical)
                    e.item.Value = ""
                End If
            End If
        End If

        If e.column.Index = COLUMN_DESCONTO Then
            Dim desc As Double

            desc = m.NuloToDouble(e.item.Value)

            If desc < 0 Or desc > 100 Then
                e.item.Value = 0
            End If


        End If

        e.item.Caption = e.item.Value

        actualizarTotaisDocumento(gridArtigosStock.Tag, gridArtigosStock)


    End Sub

    Private Function eCampoData(tabela As String, campo As String) As Boolean
        Dim type As String
        Dim m As MotorAPL
        m = New MotorAPL(Motor.GetInstance())
        type = m.daTipoCampo(tabela, campo)
        Return type = "datetime" Or type = "smalldatetime"
    End Function


    Private Function daTabelaModulo() As String
        Dim tabela As String
        tabela = ""
        Select Case modulo
            Case "V" : tabela = "LinhasDoc"
            Case "C" : tabela = "LinhasCompras"
        End Select
        Return tabela
    End Function

    Private Sub txtMatricula_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMatricula.KeyPress
        e.KeyChar = UCase(e.KeyChar)
    End Sub

    Private Sub abrirFormListaMatricula()
        Dim frmLista As FrmLista
        frmLista = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(4)

        frmLista.setComando("SELECT TOP 100 PERCENT Matricula,Descricao FROM Viaturas order by Matricula")
        frmLista.setCaption("Viaturas")
        ' frmLista.setCaption("Fornecedores")
        frmLista.ShowFilter(True)
        frmLista.setalinhamentoColunas(alinhamentoColunas)
        frmLista.ShowDialog()

        If frmLista.seleccionouOK Then
            txtMatricula.Text = frmLista.getValor(0)
            '  actualizarDadosFornecedor(frmLista.getValor(0))
        End If
    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Label7.Click
        abrirFormListaMatricula()
    End Sub

    Private Sub Label7_MouseHover(sender As Object, e As EventArgs) Handles Label7.MouseHover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub Label7_MouseLeave(sender As Object, e As EventArgs) Handles Label7.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub dtPicker_Change(sender As Object, e As EventArgs) Handles dtPicker.Change

    End Sub

    Private Sub dtPicker_Leave(sender As Object, e As EventArgs) Handles dtPicker.Leave

    End Sub

    Private Sub dtPicker_CloseUp(sender As Object, e As EventArgs) Handles dtPicker.CloseUp
        Dim str() As String

        str = dtPicker.Tag.ToString.Split(",")
        If str.Length = 2 Then
            actualizarValorData(gridArtigosStock, CInt(str(1)))
        End If

    End Sub

    Private Sub actualizarValorData(report As AxXtremeReportControl.AxReportControl, indexColuna As Integer)
        report.FocusedRow.Record(indexColuna).Value = CDate(dtPicker.Value).ToShortDateString
        report.Populate()
        MDtPicker.HideDatePicker(dtPicker)
    End Sub

    Private Sub gridArtigosENC_BeginEdit(sender As Object, e As AxXtremeReportControl._DReportControlEvents_BeginEditEvent) Handles gridArtigosStock.BeginEdit
        dtPicker.Visible = False
        If e.column.Index > COLUMN_PROJECTO Then
            If eCampoData(daTabelaModulo(), arrCampo(e.column.Index - COLUMN_PROJECTO - 1)) And e.column.Editable Then
                dtPicker.Tag = gridArtigosStock.Name + "," + CStr(e.column.Index)
                MDtPicker.ShowDatePicker(gridArtigosStock, dtPicker, e.row, e.column, e.item)
            End If
        End If
    End Sub

    Private Sub gridArtigosStock_FocusChanging(sender As Object, e As AxXtremeReportControl._DReportControlEvents_FocusChangingEvent) Handles gridArtigosStock.FocusChanging
        MDtPicker.HideDatePicker(dtPicker)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnImportarLista.Click

        If Not lstMoldes.SelectedValue Is Nothing Then
            importarArtigosListaMateriais(lstMoldes.SelectedValue)
        Else
            MsgBox("Por favor, indique um molde!", vbExclamation)
        End If

    End Sub

    Private Sub importarArtigosListaMateriais(projecto As String)

        If gridArtigos.SelectedRows.Count = 0 Then Exit Sub

        Dim frmLista As FrmLista
        Dim colInv(7) As Integer
        Dim seleccionouOK As Boolean
        frmLista = New FrmLista
        frmLista.setComando("SELECT LinhasCompras.CDU_ObraN1 as Peça, LinhasCompras.Descricao as Descrição,LinhasCompras.Quantidade, LinhasCompras.CDU_ObraN3 as Dimensões,LinhasCompras.CDU_ObraN4 as Material, Artigo, LinhasCompras.Id,LinhasCompras.Descricao,LinhasCompras.CDU_ObraN1,LinhasCompras.CDU_ObraN3, LinhasCompras.CDU_ObraN4, LinhasCompras.NumLinha
                            FROM LinhasCompras Inner join CabecCompras on LinhasCompras.IdCabecCompras=CabecCompras.Id inner join COP_Obras ON CabecCompras.ObraId=COP_Obras.Id WHERE CabecCompras.TipoDoc='LST' and COP_Obras.Codigo='" + projecto + "'")
        frmLista.setShowCheckBox(True)


        colInv(0) = 5
        colInv(1) = 6
        colInv(2) = 7
        colInv(3) = 8
        colInv(4) = 9
        colInv(5) = 10
        colInv(6) = 11
        frmLista.setcolunasInvisiveis(colInv)
        frmLista.ShowFilter(True)
        frmLista.SetOrdem(" ORDER BY NumLinha")
        frmLista.ShowDialog()
        seleccionouOK = frmLista.seleccionouOK

        Dim dt As DataTable
        dt = frmLista.getItensSeleccionados()

        If seleccionouOK Then

            If Not dt Is Nothing Then


                For Each row In dt.Rows

                    row("Artigo") = gridArtigos.SelectedRows(0).Record.Item(COLUMN_ARTIGO).Value
                    row("Descricao") = gridArtigos.SelectedRows(0).Record.Item(COLUMN_DESCRICAO).Value + " | " + row("Descricao").ToString
                    inserirLinhaRegisto(row, gridArtigosStock,,, 0)
                Next
            End If


            gridArtigosStock.Populate()
            gridArtigos.Populate()
            gridArtigosStock.SelectedRows.DeleteAll()
            gridArtigosStock.SelectedRows.Add(gridArtigosStock.Rows(gridArtigosStock.Rows.Count - 1))
        End If

        'If frmLista.seleccionouOK Then
        '    Dim 

        '    inserirLinhaRegisto()
        'End If

    End Sub

    Private Sub btnAplicarTodos_Click(sender As Object, e As EventArgs) Handles btnAplicarTodos.Click
        Dim col As Integer
        Dim row As Integer
        Dim i As Integer
        Dim record As ReportRecord
        If Not gridArtigosStock.FocusedColumn Is Nothing And Not gridArtigosStock.FocusedRow Is Nothing Then
            col = gridArtigosStock.FocusedColumn.Index
            row = gridArtigosStock.FocusedRow.Index
            For i = row To gridArtigosStock.Records.Count - 1

                record = gridArtigosStock.Records(i)
                record(col).Value = gridArtigosStock.Records(row)(col).Value

                If record(col).HasCheckbox Then
                    record(col).Checked = gridArtigosStock.Records(row)(col).Checked
                End If
            Next
            gridArtigosStock.Populate()
        End If


    End Sub

    Private Sub btnRepartirCustos_Click(sender As Object, e As EventArgs) Handles btnRepartirCustos.Click
        Dim frmCustos As FrmReparticaoValores
        frmCustos = New FrmReparticaoValores
        frmCustos.ShowDialog()

        If frmCustos.seleccionouOK Then
            repartirCustos(frmCustos.getValor())
        End If



    End Sub

    Private Sub repartirCustos(total As Double)
        Dim quantidadeTotal As Double
        Dim custoIndividual As Double
        quantidadeTotal = daQuantidadeTotalDocumento()

        If quantidadeTotal <> 0 Then
            custoIndividual = total / quantidadeTotal
        End If
        actualizarPrecUnitLinhas(custoIndividual)
    End Sub

    Private Function daQuantidadeTotalDocumento() As Double
        Dim total As Double
        Dim row As ReportRow
        For Each row In gridArtigosStock.Rows
            If IsNumeric(row.Record(COLUMN_QUANTIDADE).Value) Then
                total += CDbl(row.Record(COLUMN_QUANTIDADE).Value)
            End If
        Next
        Return total
    End Function

    Private Sub actualizarPrecUnitLinhas(precUnit As Double)

        Dim m As Motor
        m = Motor.GetInstance

        For Each row In gridArtigosStock.Rows
            If IsNumeric(row.Record(COLUMN_QUANTIDADE).Value) Then
                If CDbl(row.Record(COLUMN_QUANTIDADE).Value) > 0 Then
                    row.Record(COLUMN_QUANTSTOCK_PRECO).value = Math.Round(precUnit, 2)
                    row.Record(COLUMN_QUANTSTOCK_PRECO).Caption = FormatNumber(row.Record(COLUMN_QUANTSTOCK_PRECO).value, 2)

                End If

            End If
        Next
        gridArtigosStock.Populate()
        actualizarTotaisDocumento(gridArtigosStock.Tag, gridArtigosStock)
    End Sub


    Private Sub actualizarTotaisDocumento(id As String, grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim index As Integer
        Dim indexQT As Integer
        Dim indexDesc As Integer
        Dim record As ReportRecord
        Dim totalMerc As Double = 0
        Dim totalDesc As Double = 0
        Dim totalLinha As Double


        index = COLUMN_QUANTSTOCK_PRECO
        indexQT = COLUMN_QUANTIDADE
        indexDesc = COLUMN_DESCONTO

        If index <> -1 Then
            For Each record In grid.Records
                totalLinha = 0
                If IsNumeric(record(index).Value) Then
                    totalLinha = (record(index).Value * record(indexQT).Value)
                    totalMerc += totalLinha
                End If

                If IsNumeric(record(indexDesc).Value) Then
                    totalDesc += totalLinha * (record(indexDesc).Value / 100)
                End If

            Next


        Else
            totalMerc = 0
            totalDesc = 0
        End If

        aplicarTotais(totalMerc, totalDesc)
    End Sub


    Private Sub aplicarTotais(totalMerc As Double, totalDesc As Double)

        lblTotalMerc.Text = FormatNumber(totalMerc, 2)
        lblTotalDesc.Text = FormatNumber(totalDesc, 2)
        lblTotalDoc.Text = FormatNumber(totalMerc - totalDesc, 2)
    End Sub

End Class
