﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle

Public Class FrmAddOPPostos

    Const MSGERRO1 As String = "A CD não se encontra preenchida"
    Const MSGERRO2 As String = "A Descrição da CD se encontra preenchida"
    Const MSGERRO3 As String = "A CD ""CODIGO"" já existe!"
    Const MSGERRO4 As String = "A CD ""CODIGO"" inserido/actualizado com sucesso!"
    Const MSGERRO5 As String = "O Artigo não se encontra preenchido"


    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Dim objecto As String
    Public Sub setObjecto(obj As String)
        objecto = obj
    End Sub

    ''' <summary>
    ''' Indicação ao modo ao qual se refere a aplicação
    ''' </summary>
    ''' <remarks></remarks>
    Dim modo_ As String
    Public Property Modo() As String
        Get
            Modo = modo_
        End Get
        Set(ByVal value As String)
            modo_ = value
        End Set

    End Property
    Private Sub FrmAddPecas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        inicializarForm()
        botaoSeleccionado_ = 3

        If modo_ = "O" Then
            lblCodigo.Text = "Operação"
            Me.Text = "Adicionar Operações"
            Me.Height = 201
        Else
            lblCodigo.Text = "Posto"
            Me.Text = "Adicionar Postos"
            Me.Height = 291
        End If

        inicializarObjectos()
    End Sub

    Private Sub inicializarObjectos()
        Dim dt As DataTable
        Dim m As Motor
        m = Motor.GetInstance


        If modo_ = "O" Then
            dt = m.consultaDataTable("SELECT * FROM TDU_ObraN2 where CDU_ObraN2='" + objecto + "' or CDU_Descricao='" + objecto + "'")

            If dt.Rows.Count > 0 Then
                txtCodigo.Text = m.NuloToString(dt.Rows(0)("CDU_ObraN2"))
                txtDescricao.Text = m.NuloToString(dt.Rows(0)("CDU_Descricao"))
            End If
        Else
            dt = m.consultaDataTable("SELECT * FROM APS_GP_Postos where CDU_Posto='" + objecto + "'")
            If dt.Rows.Count > 0 Then
                txtCodigo.Text = m.NuloToString(dt.Rows(0)("CDU_Posto"))
                txtDescricao.Text = m.NuloToString(dt.Rows(0)("Nome"))
                txtArtigo.Text = m.NuloToString(dt.Rows(0)("Codigo"))
                txtDescricaoArt.Text = m.consultaValor("SELECT Descricao FROM Artigo WHERE Artigo='" + txtArtigo.Text + "'")
            End If
        End If
    End Sub

    Private Sub inicializarForm()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"

            'Control = .Add(xtpControlButton, 2, " Remover", -1, False)
            'Control.BeginGroup = True
            'Control.DescriptionText = "Remover"
            'Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        If e.control.Index = 1 Then
            adicionarOPeracaoPosto()
        End If
        If e.control.Index = 2 Then
            botaoSeleccionado_ = 2
            Me.Close()
        End If
    End Sub

    Private Sub adicionarOPeracaoPosto()
        Dim codigo As String
        Dim descricao As String

        codigo = txtCodigo.Text.Trim
        descricao = txtDescricao.Text.Trim
        Dim msg As String

        If codigo = "" Then
            MsgBox(MSGERRO1.Replace("CD", IIf(modo_ = "O", "operação", "posto")), MsgBoxStyle.Critical)
            txtCodigo.Focus()
            Exit Sub
        End If

        If descricao = "" Then
            MsgBox(MSGERRO2.Replace("CD", IIf(modo_ = "O", "operação", "posto")), MsgBoxStyle.Critical)
            txtDescricao.Focus()
            Exit Sub
        End If

        If modo_ = "P" Then
            If txtArtigo.Text = "" Then
                MsgBox(MSGERRO5.Replace("CD", IIf(modo_ = "O", "operação", "posto")), MsgBoxStyle.Critical)
                txtArtigo.Focus()
                Exit Sub
            End If
        End If

        Dim m As Motor
        m = Motor.GetInstance()


        If modo_ = "O" Then
            '   If m.operacaoExiste(codigo) Then
            ' msg = MSGERRO3.Replace("CODIGO", codigo)
            'msg = msg.Replace("CD", "Operação")
            ' m.inserirOperacao(codigo, descricao)
            ' MsgBox(msg, MsgBoxStyle.Critical)
            '  txtCodigo.Focus()
            ' Else
            msg = MSGERRO4.Replace("CODIGO", codigo)
                msg = msg.Replace("CD", "Operação")
                m.inserirOperacao(codigo, descricao)
                MsgBox(msg, MsgBoxStyle.Information)
                Me.Close()
                botaoSeleccionado_ = 1
            '  End If
        Else
            'If m.postoExiste(codigo) Then
            '    msg = MSGERRO3.Replace("CODIGO", codigo)
            '    msg = msg.Replace("CD", "posto")
            '    MsgBox(msg, MsgBoxStyle.Critical)
            '    txtCodigo.Focus()
            'Else
            msg = MSGERRO4.Replace("CODIGO", codigo)
                msg = msg.Replace("CD", "posto")
                m.inserirPosto(codigo, descricao, txtArtigo.Text)
                MsgBox(msg, MsgBoxStyle.Information)
                Me.Close()
                botaoSeleccionado_ = 1
            '  End If
        End If

    End Sub

    Private Sub btnPesquisar_Click(sender As Object, e As EventArgs)
        Dim f As FrmLista

        Dim alinhamento(5) As Integer
        f = New FrmLista
        f.setCaption("Cliente - ")
        alinhamento(0) = 0
        alinhamento(1) = 0
        alinhamento(2) = 0
        alinhamento(3) = 0
        alinhamento(4) = 0
        f.setdescricaoMapaCabecalho("Artigos Peças")
        f.setalinhamentoColunas(alinhamento)
        f.Height = 700
        f.Width = 800
        f.ShowGroup(False)
        f.ShowFilter(True)
        f.setComando("Select * FROM APS_GP_PecasArtigos")
        f.ShowDialog()
        If f.seleccionouOK Then
            txtCodigo.Text = f.getValor(0)
            txtDescricao.Text = f.getValor(1)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs)

    End Sub


    Private Sub txtQuantidade_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim allowedChars As String = "0123456789, "




        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 8 AndAlso allowedChars.IndexOf(e.KeyChar) = -1 Then
            '   MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If

    End Sub


    Private Sub btnPesquisar_Click_1(sender As Object, e As EventArgs) Handles btnPesquisar.Click
        Dim f As FrmLista

        Dim alinhamento(5) As Integer
        f = New FrmLista
        f.setCaption("Artigos")
        alinhamento(0) = 0
        alinhamento(1) = 0
        alinhamento(2) = 0
        alinhamento(3) = 0
        alinhamento(4) = 0
        f.setdescricaoMapaCabecalho("Artigos")
        f.setalinhamentoColunas(alinhamento)
        f.Height = 700
        f.Width = 800
        f.ShowGroup(False)
        f.ShowFilter(True)
        f.setComando("Select Artigo, Descricao as 'Descrição' FROM Artigo")
            f.ShowDialog()
        If f.seleccionouOK Then
            txtArtigo.Text = f.getValor(0)
            txtDescricaoArt.Text = f.getValor(1)
        End If
    End Sub

    Private Sub btnLimpar_Click(sender As Object, e As EventArgs) Handles btnLimpar.Click
        txtArtigo.Text = ""
        txtDescricaoArt.Text = ""
    End Sub
End Class
