﻿Imports Interop.StdBE900

Public Class FrmApl

    Dim newRowNeeded As Boolean
    Private Sub FrmApl_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        actualizar()
    End Sub

    Private Sub actualizar()
        Dim m As MotorLM
        Dim lista As StdBELista
        Dim dt As DataTable
        m = MotorLM.GetInstance


        lista = m.consulta("SELECT * FROM TDU_EQUIV_FORMATO_ARTIGO")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        DataGridView1.DataSource = dt
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect

        lista = m.consulta("SELECT * FROM TDU_EQUIV_LSTM_ARTIGO")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        DataGridView2.DataSource = dt
        DataGridView2.Columns(0).Width = 0
        DataGridView2.Columns(0).Visible = False
        '  DataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        DataGridView2.EditMode = DataGridViewEditMode.EditOnEnter
    End Sub


    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count > 0 Then
            TextBox1.Text = DataGridView1.SelectedRows.Item(0).Cells(2).Value
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim m As MotorLM
        m = MotorLM.GetInstance

        If DataGridView1.SelectedRows.Count > 0 Then
            m.executarComando("UPDATE TDU_EQUIV_FORMATO_ARTIGO set CDU_CodigoVB='" + TextBox1.Text + "' WHERE CDU_Id=" + DataGridView1.SelectedRows.Item(0).Cells(0).Value.ToString() + "")
        End If
    End Sub

    Private Sub DataGridView2_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellValueChanged

        Dim m As MotorLM
        m = MotorLM.GetInstance

        '  If DataGridView2.SelectedRows.Count > 0 Then
        If Not IsDBNull(DataGridView2.Rows(e.RowIndex).Cells(0).Value) Then
            m.executarComando("UPDATE TDU_EQUIV_LSTM_ARTIGO set CDU_MARCALSTM=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(1).Value) + ",CDU_DESCRICAOLSTM=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(2).Value) + ", CDU_REFERENCIALSTM=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(3).Value) + ",CDU_MARCAARTIGO=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(4).Value) + ",CDU_DESCRICAOARTIGO=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(5).Value) + ",CDU_REFERENCIAARTIGO=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(6).Value) + ",CDU_FORMATOARTIGO=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(7).Value) + ",CDU_FORMATACAOARTIGO=" + formatarValorDouble(DataGridView2.Rows(e.RowIndex).Cells(8).Value) + ",CDU_ARTIGO=" + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(9).Value) + " WHERE CDU_Id='" + CStr(DataGridView2.Rows(e.RowIndex).Cells(0).Value.ToString) + "'")
        Else
            Dim id As String
            id = m.consultaValor("SELECT NEWID()")
            m.executarComando("INSERT INTO TDU_EQUIV_LSTM_ARTIGO (CDU_ID,CDU_MARCALSTM,CDU_DESCRICAOLSTM,CDU_REFERENCIALSTM,CDU_MARCAARTIGO,CDU_DESCRICAOARTIGO,CDU_REFERENCIAARTIGO,CDU_FORMATOARTIGO,CDU_FORMATACAOARTIGO,CDU_ARTIGO) VALUES('" + id + "'," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(1).Value) + "," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(2).Value) + ", " + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(3).Value) + "," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(4).Value) + "," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(5).Value) + "," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(6).Value) + "," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(7).Value) + "," + formatarValorDouble(DataGridView2.Rows(e.RowIndex).Cells(8).Value) + "," + formatarValorString(DataGridView2.Rows(e.RowIndex).Cells(9).Value) + ")")
            DataGridView2.Rows(e.RowIndex).Cells(0).Value = id
        End If ' End If



    End Sub

    Private Function formatarValorString(valor As Object) As String

        If IsDBNull(valor) Or IsNothing(valor) Then
            Return "NULL"
        Else
            Return "'" + CStr(valor) + "'"
        End If

    End Function


    Private Function formatarValorDouble(valor As Object) As String

        If IsDBNull(valor) Then
            Return "NULL"
        Else
            If IsNumeric(valor) Then
                Return Replace(CStr(valor), ",", ".")
            Else
                Return "NULL"
            End If

        End If

    End Function




    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim m As MotorLM
        m = MotorLM.GetInstance

        If MsgBox("Deseja realmente apagar a linha?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) Then
            If DataGridView2.SelectedRows.Count > 0 Then

                If Not IsDBNull(DataGridView2.SelectedRows(0).Cells(0).Value) Then
                    m.executarComando("DELETE FROM TDU_EQUIV_LSTM_ARTIGO WHERE CDU_Id='" + CStr(DataGridView2.SelectedRows(0).Cells(0).Value.ToString) + "'")
                    actualizar()
                    MsgBox("Registo removido com sucesso", MsgBoxStyle.Information)
                End If
            End If

        End If
    End Sub

    Private Sub CalcularArtigo_Click(sender As Object, e As EventArgs) Handles CalcularArtigo.Click
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim dt As DataTable
        Dim row As DataRow
        dt = New DataTable
        dt.Columns.Add("Material")
        dt.Columns.Add("Descricao")
        dt.Columns.Add("Peca")
        dt.Columns.Add("Dimensoes")
        dt.Columns.Add("Artigo")
        row = dt.NewRow()
        row("Material") = txtMarca.Text
        row("Descricao") = txtDescricao.Text
        row("Peca") = ""
        row("Dimensoes") = txtReferencia.Text
        row("Artigo") = ""
        txtArtigo.Text = m.daArtigoEquivalente(row)
        If Not m.artigoExiste(txtArtigo.Text) Then
            lblArtigo.Text = "Equivalencia não realizada"
            lblArtigo.ForeColor = Color.Red
        Else
            lblArtigo.Text = "Equivalencia efectuada com sucesso"
            lblArtigo.ForeColor = Color.Green
        End If
    End Sub
End Class