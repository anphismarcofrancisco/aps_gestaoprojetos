﻿Imports Interop.StdBE900
Imports PlatAPSNET
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment


Public Class FrmGestPontos
    Dim formInicializado As Boolean
    Const COLUMN_ARTIGO As Integer = 0
    ' Const COLUMN_ARMAZEM As Integer = 2
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTIDADE As Integer = 2

    Const VIEW_ARTIGOMATERIALRTJ As String = "APS_GP_ArtigosMateriais"
    Const VIEW_ARTIGOMATERIALCLIENTE As String = "APS_GP_ArtigosMateriaisCliente"

    Const MSGBOX1 = "Tem de seleccionar um Centro Trabalho"
    Const MSGBOX2 = "Tem de seleccionar um Cliente"
    Const MSGBOX3 = "Tem de seleccionar uma Peça"
    Const MSGBOX4 = "Tem de seleccionar uma Operação"
    Const MSGBOX5 = "Tem de indicar o V\Molde"
    Const MSGBOX6 = "Atenção V\Requisicao não se encontra preenchida."

    Dim idRegisto As String
    Dim aactualizar As Boolean

    Private Sub FrmGestPontos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fileINI As String
        Dim ficheiroConfiguracao As Configuracao
        fileINI = Application.StartupPath + "\\CONFIG.ini"
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
        inicializarForm()
        formInicializado = True
        aactualizar = False
        Dim m As Motor
        m = Motor.GetInstance
        Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario
        Label1.Text = "Utiliza Mat. " + m.Empresa

    End Sub

    Private Sub ckbUML_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbUML.CheckedChanged


        If ckbUML.Checked Then
            Dim quantidade As String
            ckbUML.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checked1
            'caso esteja  aactualizar o registo nao abre o formQuantidade
            If aactualizar Then Exit Sub
            quantidade = abrirFormularioQuantidade(txtNumeroKG.Text)
            If quantidade <> "" Then
                txtNumeroKG.Text = quantidade
            End If
        Else
            ckbUML.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        End If
    End Sub


    Private Sub ckbUMR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbUMR.CheckedChanged
        If ckbUMR.Checked Then
            ckbUMR.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checked1
            'caso esteja  aactualizar o registo nao abre o formQuantidade
            If aactualizar Then Exit Sub
            Dim m As Motor
            m = Motor.GetInstance
            abrirFormArtigos(gridAMRTJ, VIEW_ARTIGOMATERIALRTJ, m.getArmazemDefault())
        Else
            ckbUMR.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        End If
        '  gridAMRTJ.Enabled = ckbUMR.Checked
    End Sub

    Private Sub ckbUMC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbUMC.CheckedChanged
        If ckbUMC.Checked Then
            ckbUMC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checked1
            'caso esteja  aactualizar o registo nao abre o formQuantidade
            If aactualizar Then Exit Sub

            Dim armazem As String
            Dim dt As DataRowView
            dt = lstCliente.SelectedItem
            If Not dt Is Nothing Then
                armazem = Motor.GetInstance().NuloToString(dt("CDU_Armazem"))
                If armazem = "" Then armazem = "-1"
                abrirFormArtigos(gridAMC, VIEW_ARTIGOMATERIALCLIENTE, armazem)
            Else
                MsgBox("Indicar um Cliente", MsgBoxStyle.Information)
            End If
        Else
            ckbUMC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        End If

        '  gridAMC.Enabled = ckbUMC.Checked
    End Sub

    Private Sub inicializarForm()

        idRegisto = ""

        inicializarCentroTrabalho()

        inicializarClientes()

        inicializarOperacoes()


        inserirColunasRegistos(1, gridAMRTJ)
        inserirColunasRegistos(2, gridAMC)

        Dim dt As DataTable
        Dim m As Motor
        m = Motor.GetInstance

        dt = m.daExtruturaDataTableArtigos()
        inserirLinhasGrelhas(gridAMRTJ, dt)
        dt = m.daExtruturaDataTableArtigos()
        inserirLinhasGrelhas(gridAMC, dt)
        'ToolTip.SetToolTip(btnEditar, " ")
        'ToolTipIniciar.SetToolTip(btnIniciar, " ")
        'ToolTipTerminar.SetToolTip(btnParar, " ")
        'ToolTipSair.SetToolTip(btnSair, " ")
        'ToolTipAdd.SetToolTip(btnAdd, " ")
        'If lstCentroTrab.Items.Count > 0 Then
        '    lstCentroTrab.SelectedIndex = 0
        '    actualizaValorKg()
        'End If
    End Sub

    Private Sub inserirColunasRegistos(ByVal indexGrid As Integer, ByVal grid As AxXtremeReportControl.AxReportControl)

        grid.Columns.DeleteAll()
        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", 150, False, True)
        '  Column = inserirColuna(grid, COLUMN_ARMAZEM, "Armazém", 100, False, True)
        Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", 200, False, True)
        Column = inserirColuna(grid, COLUMN_QUANTIDADE, "Quant.", 80, False, True)
        Column.Alignment = xtpAlignmentRight

    End Sub

    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function

    Private Sub inicializarCentroTrabalho()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstCentroTrab.SelectedIndex = -1
        lista = m.daListaCentrosTrabalho(txtPesqCT.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstCentroTrab.DataSource = dt
        lstCentroTrab.ValueMember = "Codigo"
        lstCentroTrab.DisplayMember = "Nome"
        lstCentroTrab.SelectedIndex = -1
    End Sub

    Private Sub inicializarClientes()

        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstCliente.SelectedIndex = -1
        lista = m.daListaClientes(txtPesqClientes.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstCliente.DataSource = dt
        lstCliente.ValueMember = "Codigo"
        lstCliente.DisplayMember = "Nome"
        lstCliente.SelectedIndex = -1
    End Sub
    Private Sub inicializarOperacoes()
        Dim m As Motor
        Dim lista As StdBELista
        lstOperacao.SelectedIndex = -1
        m = Motor.GetInstance()
        lista = m.daListaOperacoes(m.Funcionario, txtPesqOperacao.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstOperacao.DataSource = dt
        lstOperacao.ValueMember = "Codigo"
        lstOperacao.DisplayMember = "Nome"
    End Sub

    Private Sub txtPesqCT_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqCT.Change
        inicializarCentroTrabalho()
    End Sub

    Private Sub txtPesqClientes_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqClientes.Change
        inicializarClientes()
    End Sub

    Private Sub txtPesqOperacao_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqOperacao.Change
        inicializarOperacoes()
    End Sub

  

    Private Sub actualizaValorKg()
        Dim m As Motor
        m = Motor.GetInstance
        txtNumeroKG.Text = m.daNumeroKgCT(lstCentroTrab.SelectedValue())
        '  End If
    End Sub


    Private Function abrirFormularioQuantidade(ByVal quantidade As String) As String
        If IsNumeric(quantidade) Then
            Dim f As FrmQt
            f = New FrmQt
            f.Valor = CDbl(quantidade)
            f.ShowDialog()
            If f.BotaoSeleccionado = 1 Then
                quantidade = f.Valor.ToString
            Else
                quantidade = ""
            End If
        Else
            quantidade = ""
        End If
        Return quantidade
    End Function

    Private Sub inserirLinhasGrelhas(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal dt As DataTable)
        Dim dtrow As DataRow
        grid.Records.DeleteAll()
        grid.Tag = dt
        For Each dtrow In dt.Rows
            inserirLinhaRegisto(dtrow, grid, dtrow("Quantidade"))
        Next

        grid.Populate()
    End Sub

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional ByVal valorStk As Double = -1)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        record = grid.Records.Add()

        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo"))
        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = 1
        If valorStk < 0 Then valorStk = valorStk * -1
        Item = record.AddItem(valorStk)

    End Sub


    Private Sub abrirFormArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal view As String, ByVal armazem As String)

        Dim dt As DataTable
        dt = grid.Tag
        Dim f As FrmArtigo
        f = New FrmArtigo
        f.TabelaArtigos = dt
        f.ViewArtigo = view
        f.ArmazemCliente = armazem
        f.ShowDialog()

        If f.BotaoSeleccionado = 1 Then
            dt = f.TabelaArtigos
            inserirLinhasGrelhas(grid, dt)
        End If
        Me.Show()
    End Sub

    Private Sub btnIniciar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIniciar.Click
        If lstCentroTrab.SelectedIndex = -1 Then
            MsgBox(MSGBOX1, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstCliente.SelectedIndex = -1 Then
            MsgBox(MSGBOX2, MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        If lstOperacao.SelectedIndex = -1 Then
            MsgBox(MSGBOX4, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If txtVMolde.Text = "" Then
            MsgBox(MSGBOX5, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If txtVRequisicao.Text = "" Then
            MsgBox(MSGBOX6, MsgBoxStyle.Information)
            '  Exit Sub
        End If



        Dim m As Motor
        m = Motor.GetInstance
        Dim erro As String
        Dim rowCT As DataRowView
        Dim rowCliente As DataRowView
        Dim rowOperacao As DataRowView
        Dim dtUMC As DataTable
        Dim dtUMR As DataTable
        rowCT = lstCentroTrab.SelectedItem
        rowCliente = lstCliente.SelectedItem
        rowOperacao = lstOperacao.SelectedItem

        dtUMC = gridAMC.Tag
        dtUMR = gridAMRTJ.Tag
        Dim numeroKG As Double
        numeroKG = txtNumeroKG.Text

        If Not ckbUML.Checked Then numeroKG = 0

        Dim objectoRegisto As Registo
        objectoRegisto = m.daObjectoRegisto(rowCT("Codigo"), rowCliente("Codigo"), rowOperacao("Codigo"), txtVMolde.Text, txtVRequisicao.Text, numeroKG, dtUMR, dtUMC, idRegisto)

        erro = m.iniciarOperacao(objectoRegisto, False)
        If erro <> "" Then
            MsgBox("sdad" + erro, MsgBoxStyle.Exclamation)
        Else
            MsgBox("Obra Inicializada com sucesso", MsgBoxStyle.Information, "Anphis GP")
        End If

    End Sub

    Private Sub lstCentroTrab_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lstCentroTrab.DrawItem
        If e.Index = -1 Then Exit Sub
        e.DrawBackground()
        Dim myBrush As Brush
        Dim myBackColor As Color = Color.White
        Dim myForeColor As Color = Color.Black
        Dim myFont As Font = e.Font

        Dim m As Motor
        m = Motor.GetInstance

        Dim row As DataRowView
        row = CType(sender, ListBox).Items(e.Index)

        If m.obraOperacaoJaIniciada(row("Codigo"), "", "", False) <> "" Then
            myForeColor = Color.Green
        Else
            myForeColor = Color.Black
        End If

        If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
            myBackColor = e.BackColor
            If myForeColor = Color.Black Then
                myForeColor = Color.White
            Else
                myForeColor = Color.Lime
            End If
        End If

        myBrush = New SolidBrush(myBackColor)
        e.Graphics.FillRectangle(myBrush, e.Bounds)
        myBrush = New SolidBrush(myForeColor)

        Dim drv As DataRowView
        drv = lstCentroTrab.Items(e.Index)

        e.Graphics.DrawString(row("Codigo"), e.Font, myBrush, e.Bounds, StringFormat.GenericDefault)


        e.DrawFocusRectangle()



    End Sub

    Private Sub lstCentroTrab_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstCentroTrab.SelectedIndexChanged
        If formInicializado Then actualizaValorKg()
    End Sub


    Private Sub btnParar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParar.Click
        If lstCentroTrab.SelectedIndex = -1 Then
            MsgBox(MSGBOX1, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstCliente.SelectedIndex = -1 Then
            MsgBox(MSGBOX2, MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        If lstOperacao.SelectedIndex = -1 Then
            MsgBox(MSGBOX4, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If txtVMolde.Text = "" Then
            MsgBox(MSGBOX5, MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        If txtVRequisicao.Text = "" Then
            MsgBox(MSGBOX6, MsgBoxStyle.Information)
            ' Exit Sub
        End If

        Dim m As Motor
        m = Motor.GetInstance
        Dim erro As String
        Dim rowCT As DataRowView

        Dim rowCliente As DataRowView
        Dim rowOperacao As DataRowView
        rowCT = lstCentroTrab.SelectedItem
        rowCliente = lstCliente.SelectedItem
        rowOperacao = lstOperacao.SelectedItem
        Dim operador As String
        Dim msg As String
        operador = ""

        If idRegisto = "" Then
            msg = m.obraOperacaoJaIniciada(rowCT("Codigo"), rowOperacao("Codigo"), operador, True)

            Dim podeTerminarObra As Boolean
            podeTerminarObra = True

            If msg <> "" Then
                If operador = m.Funcionario Then
                    erro = m.obraPodeSerTerminada(rowCT("Codigo"), rowCliente("Codigo"), rowOperacao("Codigo"), True)
                    If erro <> "" Then
                        MsgBox("O Posto " + rowCT("Nome") + " já se encontra iniciado com o utilizador " + msg, MsgBoxStyle.Exclamation)
                        Exit Sub
                    Else
                        podeTerminarObra = True
                    End If
                Else
                    MsgBox("O Posto " + rowCT("Nome") + " já se encontra iniciado com o utilizador " + msg, MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
            End If
        End If

        Dim dtUMC As DataTable
        Dim dtUMR As DataTable
        dtUMC = gridAMC.Tag
        dtUMR = gridAMRTJ.Tag
        Dim numeroKG As Double
        numeroKG = txtNumeroKG.Text

        If Not ckbUML.Checked Then numeroKG = 0

        Dim botaoSeleccionado As Integer
        Dim objectoRegisto As Registo

        objectoRegisto = m.daObjectoRegisto(rowCT("Codigo"), rowCliente("Codigo"), rowOperacao("Codigo"), txtVMolde.Text, txtVRequisicao.Text, numeroKG, dtUMR, dtUMC, idRegisto)

        botaoSeleccionado = m.TerminarOperacao(objectoRegisto)

        If botaoSeleccionado <> 3 Then

            btnIniciar.Enabled = True

            If botaoSeleccionado = 1 Then
                If MsgBox("Registo Ponto terminado com sucesso" + vbCrLf + "Deseja inserir mais pontos?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Me.Close()
                Else
                    reiniciarForm()
                    inicializarForm()
                End If
            Else
                reiniciarForm()
                inicializarForm()
            End If
        End If

    End Sub

    Private Sub limparSistema()
        txtPesqClientes.Text = ""
        txtPesqOperacao.Text = ""
        txtPesqCT.Text = ""
        Dim dt As DataTable
        Dim m As Motor
        m = Motor.GetInstance()
        dt = m.daExtruturaDataTableArtigos()
        inserirLinhasGrelhas(gridAMRTJ, dt)
        dt = m.daExtruturaDataTableArtigos()
        inserirLinhasGrelhas(gridAMC, dt)

        If lstCliente.Items.Count > 0 Then
            lstCentroTrab.SelectedIndex = 0
        End If

        If lstCentroTrab.Items.Count > 0 Then
            lstCentroTrab.SelectedIndex = 0
        End If

        If lstOperacao.Items.Count > 0 Then
            lstCentroTrab.SelectedIndex = 0
        End If
    End Sub

    Private Sub btnFechar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechar.Click

        If MsgBox("Deseja realmente fechar a aplicação?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Gestão Produção") = MsgBoxResult.Yes Then
            Me.Close()
        End If
    End Sub

  
    Private Sub btnRegistos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistos.Click
        Dim frmEditar As FrmEditarRegistos
        frmEditar = New FrmEditarRegistos
        frmEditar.ShowDialog()
        Dim id As String

        If frmEditar.clicouConfirmar Then
            id = frmEditar.getIdLinha()
            If id <> "" Then
                preencherForm(id)
            End If
        End If
    End Sub


    Private Sub preencherForm(ByVal cdu_id As String)
        Dim lista As StdBELista
        Dim m As Motor
        Dim dt As DataTable
        reiniciarForm()
        aactualizar = True
        m = Motor.GetInstance()
        idRegisto = ""
        lista = m.consulta("Select * from TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + cdu_id + "' ")
        If Not lista.NoFim Then
            Dim id As String
            id = lista.Valor("CDU_ID")
            idRegisto = id
            lstCentroTrab.SelectedValue = lista.Valor("CDU_Posto")
            lstCliente.SelectedValue = lista.Valor("CDU_Entidade")
            lstOperacao.SelectedValue = lista.Valor("CDU_Operacao")
            txtVMolde.Text = lista.Valor("CDU_Molde")
            txtVRequisicao.Text = lista.Valor("CDU_Requisicao")

            lista = m.consulta("Select * from TDU_APSLinhasRegistoFolhaPonto WHERE CDU_IdTDUCabec='" + id + "' and CDU_TipoArtigo='" + m.MATERIAL_LIMPEZA + "' ORDER BY CDU_NumLinha")

            ckbUML.Checked = False

            If Not lista.NoFim Then
                ckbUML.Checked = True
                txtNumeroKG.Text = lista.Valor("CDu_Quantidade")
            End If

            lista = m.consulta("Select Artigo.Artigo, CDU_Armazem as Armazem,Descricao,ArtigoArmazem.StkActual as StkActual, CDU_Quantidade as Quantidade from TDU_APSLinhasRegistoFolhaPonto,Artigo,ArtigoArmazem   WHERE TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo=Artigo.Artigo and artigoarmazem.Artigo=TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo and TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem=ArtigoArmazem.Armazem  And CDU_IdTDUCabec='" + id + "' and CDU_TipoArtigo='" + m.MATERIAL_RTJ + "' ORDER BY CDU_NumLinha")
            ckbUMR.Checked = False
            If Not lista.NoFim Then
                dt = m.convertRecordSetToDataTable(lista.DataSet)
                ckbUMR.Checked = True
                inserirLinhasGrelhas(gridAMRTJ, dt)
            End If

            lista = m.consulta("Select Artigo.Artigo, CDU_Armazem as Armazem,Descricao,ArtigoArmazem.StkActual as StkActual, CDU_Quantidade as Quantidade from TDU_APSLinhasRegistoFolhaPonto,Artigo,ArtigoArmazem   WHERE TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo=Artigo.Artigo and artigoarmazem.Artigo=TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo and TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem=ArtigoArmazem.Armazem   And CDU_IdTDUCabec='" + id + "' and CDU_TipoArtigo='" + m.MATERIAL_CLIENTE + "' ORDER BY CDU_NumLinha")
            ckbUMC.Checked = False
            If Not lista.NoFim Then
                dt = m.convertRecordSetToDataTable(lista.DataSet)
                ckbUMC.Checked = True
                inserirLinhasGrelhas(gridAMC, dt)
            End If

        End If

        aactualizar = False
    End Sub

    Private Sub reiniciarForm()
        aactualizar = True
        txtVMolde.Text = ""
        txtVRequisicao.Text = ""
        txtPesqClientes.Text = ""
        txtPesqCT.Text = ""
        txtPesqOperacao.Text = ""
        txtNumeroKG.Text = "0"
        gridAMC.Records.DeleteAll()
        gridAMRTJ.Records.DeleteAll()
        gridAMC.Populate()
        gridAMRTJ.Populate()
        ckbUMC.Checked = False
        ckbUMR.Checked = False
        ckbUML.Checked = False
        idRegisto = ""
        aactualizar = False
    End Sub

    Private Sub lstCliente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstCliente.SelectedIndexChanged
        If idRegisto <> "" Then

            Dim m As Motor
            Dim entidade As String
            m = Motor.GetInstance
            entidade = m.consultaValor("Select CDu_Entidade FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + idRegisto + "'")

            If lstCliente.SelectedValue <> entidade Then
                If MsgBox("Está a alterar a entidade do registo!" + vbCrLf + vbCrLf + "O Material do Cliente será eliminado!" + vbCrLf + vbCrLf + "Deseja continuar?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    lstCliente.SelectedValue = entidade
                Else
                    Dim dt As DataTable
                    dt = m.daExtruturaDataTableArtigos()
                    inserirLinhasGrelhas(gridAMC, dt)
                    ckbUMC.Checked = False
                End If
            End If

        End If
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub
End Class
