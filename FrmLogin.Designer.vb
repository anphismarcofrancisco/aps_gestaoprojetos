﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLogin))
        Me.lstFuncionarios = New System.Windows.Forms.ListBox()
        Me.imgLst = New System.Windows.Forms.ImageList(Me.components)
        Me.txtPassword = New System.Windows.Forms.MaskedTextBox()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btnC = New System.Windows.Forms.Button()
        Me.btn0 = New System.Windows.Forms.Button()
        Me.txtFilter = New System.Windows.Forms.TextBox()
        Me.cmbEmpresas = New System.Windows.Forms.ComboBox()
        Me.cmbListaModulos = New System.Windows.Forms.ComboBox()
        Me.btnEnter1 = New System.Windows.Forms.Button()
        Me.btnEnter3 = New System.Windows.Forms.Button()
        Me.btnEnter2 = New System.Windows.Forms.Button()
        Me.btnDw = New System.Windows.Forms.Button()
        Me.btnUP = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lstFuncionarios
        '
        Me.lstFuncionarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstFuncionarios.FormattingEnabled = True
        Me.lstFuncionarios.ItemHeight = 25
        Me.lstFuncionarios.Location = New System.Drawing.Point(12, 56)
        Me.lstFuncionarios.Name = "lstFuncionarios"
        Me.lstFuncionarios.Size = New System.Drawing.Size(331, 229)
        Me.lstFuncionarios.TabIndex = 0
        '
        'imgLst
        '
        Me.imgLst.ImageStream = CType(resources.GetObject("imgLst.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgLst.TransparentColor = System.Drawing.Color.Transparent
        Me.imgLst.Images.SetKeyName(0, "SetaUp.ico")
        Me.imgLst.Images.SetKeyName(1, "SetaDown.ico")
        '
        'txtPassword
        '
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Info
        Me.txtPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(12, 291)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(331, 26)
        Me.txtPassword.TabIndex = 3
        '
        'btn1
        '
        Me.btn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.Location = New System.Drawing.Point(53, 326)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(58, 60)
        Me.btn1.TabIndex = 4
        Me.btn1.Tag = "1"
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.Location = New System.Drawing.Point(119, 326)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(58, 60)
        Me.btn2.TabIndex = 5
        Me.btn2.Tag = "2"
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn3.Location = New System.Drawing.Point(185, 326)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(58, 60)
        Me.btn3.TabIndex = 6
        Me.btn3.Tag = "3"
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn4.Location = New System.Drawing.Point(53, 392)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(58, 60)
        Me.btn4.TabIndex = 7
        Me.btn4.Tag = "4"
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn5
        '
        Me.btn5.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn5.Location = New System.Drawing.Point(119, 392)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(58, 60)
        Me.btn5.TabIndex = 8
        Me.btn5.Tag = "5"
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = True
        '
        'btn6
        '
        Me.btn6.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn6.Location = New System.Drawing.Point(185, 392)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(58, 60)
        Me.btn6.TabIndex = 9
        Me.btn6.Tag = "6"
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = True
        '
        'btn7
        '
        Me.btn7.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn7.Location = New System.Drawing.Point(53, 458)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(58, 60)
        Me.btn7.TabIndex = 10
        Me.btn7.Tag = "7"
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = True
        '
        'btn8
        '
        Me.btn8.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn8.Location = New System.Drawing.Point(119, 458)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(58, 60)
        Me.btn8.TabIndex = 11
        Me.btn8.Tag = "8"
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = True
        '
        'btn9
        '
        Me.btn9.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn9.Location = New System.Drawing.Point(185, 458)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(58, 60)
        Me.btn9.TabIndex = 12
        Me.btn9.Tag = "9"
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = True
        '
        'btnC
        '
        Me.btnC.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnC.Location = New System.Drawing.Point(53, 524)
        Me.btnC.Name = "btnC"
        Me.btnC.Size = New System.Drawing.Size(58, 60)
        Me.btnC.TabIndex = 13
        Me.btnC.Tag = "-1"
        Me.btnC.Text = "C"
        Me.btnC.UseVisualStyleBackColor = True
        '
        'btn0
        '
        Me.btn0.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn0.Location = New System.Drawing.Point(119, 524)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(58, 60)
        Me.btn0.TabIndex = 14
        Me.btn0.Tag = "0"
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = True
        '
        'txtFilter
        '
        Me.txtFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilter.Location = New System.Drawing.Point(14, 15)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(328, 30)
        Me.txtFilter.TabIndex = 0
        '
        'cmbEmpresas
        '
        Me.cmbEmpresas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEmpresas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEmpresas.FormattingEnabled = True
        Me.cmbEmpresas.Location = New System.Drawing.Point(15, 602)
        Me.cmbEmpresas.Name = "cmbEmpresas"
        Me.cmbEmpresas.Size = New System.Drawing.Size(383, 33)
        Me.cmbEmpresas.TabIndex = 38
        '
        'cmbListaModulos
        '
        Me.cmbListaModulos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbListaModulos.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbListaModulos.FormattingEnabled = True
        Me.cmbListaModulos.Location = New System.Drawing.Point(14, 188)
        Me.cmbListaModulos.Name = "cmbListaModulos"
        Me.cmbListaModulos.Size = New System.Drawing.Size(383, 33)
        Me.cmbListaModulos.TabIndex = 39
        Me.cmbListaModulos.Visible = False
        '
        'btnEnter1
        '
        Me.btnEnter1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnter1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checklist64
        Me.btnEnter1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEnter1.Location = New System.Drawing.Point(275, 332)
        Me.btnEnter1.Name = "btnEnter1"
        Me.btnEnter1.Size = New System.Drawing.Size(122, 80)
        Me.btnEnter1.TabIndex = 42
        Me.btnEnter1.Text = "Lista Materiais"
        Me.btnEnter1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEnter1.UseVisualStyleBackColor = True
        '
        'btnEnter3
        '
        Me.btnEnter3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnter3.Image = Global.APS_GestaoProjectos.My.Resources.Resources.order_history48
        Me.btnEnter3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEnter3.Location = New System.Drawing.Point(275, 507)
        Me.btnEnter3.Name = "btnEnter3"
        Me.btnEnter3.Size = New System.Drawing.Size(122, 77)
        Me.btnEnter3.TabIndex = 41
        Me.btnEnter3.Text = "Doc\ Encomendas"
        Me.btnEnter3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEnter3.UseVisualStyleBackColor = True
        '
        'btnEnter2
        '
        Me.btnEnter2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnter2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Add_Male_User48
        Me.btnEnter2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEnter2.Location = New System.Drawing.Point(275, 418)
        Me.btnEnter2.Name = "btnEnter2"
        Me.btnEnter2.Size = New System.Drawing.Size(122, 77)
        Me.btnEnter2.TabIndex = 40
        Me.btnEnter2.Text = "Registo Pontos"
        Me.btnEnter2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEnter2.UseVisualStyleBackColor = True
        '
        'btnDw
        '
        Me.btnDw.ImageKey = "SetaDown.ico"
        Me.btnDw.ImageList = Me.imgLst
        Me.btnDw.Location = New System.Drawing.Point(350, 149)
        Me.btnDw.Name = "btnDw"
        Me.btnDw.Size = New System.Drawing.Size(48, 136)
        Me.btnDw.TabIndex = 2
        Me.btnDw.UseVisualStyleBackColor = True
        '
        'btnUP
        '
        Me.btnUP.ImageKey = "SetaUp.ico"
        Me.btnUP.ImageList = Me.imgLst
        Me.btnUP.Location = New System.Drawing.Point(349, 12)
        Me.btnUP.Name = "btnUP"
        Me.btnUP.Size = New System.Drawing.Size(48, 136)
        Me.btnUP.TabIndex = 1
        Me.btnUP.UseVisualStyleBackColor = True
        '
        'FrmLogin
        '
        Me.ClientSize = New System.Drawing.Size(410, 644)
        Me.Controls.Add(Me.btnEnter1)
        Me.Controls.Add(Me.btnEnter3)
        Me.Controls.Add(Me.btnEnter2)
        Me.Controls.Add(Me.cmbListaModulos)
        Me.Controls.Add(Me.cmbEmpresas)
        Me.Controls.Add(Me.txtFilter)
        Me.Controls.Add(Me.btn0)
        Me.Controls.Add(Me.btnC)
        Me.Controls.Add(Me.btn9)
        Me.Controls.Add(Me.btn8)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.btn6)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.btnDw)
        Me.Controls.Add(Me.btnUP)
        Me.Controls.Add(Me.lstFuncionarios)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(426, 800)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(426, 634)
        Me.Name = "FrmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestão Projectos - V 2.111"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub



    Friend WithEvents lstFuncionarios As System.Windows.Forms.ListBox
    Friend WithEvents btnUP As System.Windows.Forms.Button
    Friend WithEvents btnDw As System.Windows.Forms.Button
    Friend WithEvents imgLst As System.Windows.Forms.ImageList
    Friend WithEvents txtPassword As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btnC As System.Windows.Forms.Button
    Friend WithEvents btn0 As System.Windows.Forms.Button
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
    Friend WithEvents cmbEmpresas As System.Windows.Forms.ComboBox
    Friend WithEvents cmbListaModulos As System.Windows.Forms.ComboBox
    Friend WithEvents btnEnter2 As Button
    Friend WithEvents btnEnter3 As Button
    Friend WithEvents btnEnter1 As Button
End Class
