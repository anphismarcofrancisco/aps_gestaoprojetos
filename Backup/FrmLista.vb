﻿Imports AxXtremeReportControl
Imports StdBE800
Imports XtremeReportControl
Imports XtremeReportControl.XTPReportFixedRowsDividerStyle
Imports XtremeReportControl.XTPColumnAlignment
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Public Class FrmLista

    Private Sub FrmLista_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  Dim data As Date
        ' data = Now
        If Not NaoprimeiraVez Then
            iniciarForm()
            NaoprimeiraVez = True
        End If

        ' MsgBox(DateDiff(DateInterval.Second, Now, data))
    End Sub


    Dim consulta As String
    Dim clicouOK As Boolean
    Dim bEnterPressed As Boolean
    Dim colunasInvisiveis() As Integer

    Const ID_CONFIRMAR As Integer = 1
    Const ID_IMPRIMIR As Integer = 2
    Const ID_CANCELAR As Integer = 3
    ' Dim rs As ADODB.Recordset

    Dim NaoprimeiraVez As Boolean

    Dim apresentarGrupo As Boolean
    Dim apresentarFiltro As Boolean
    Dim alinhamentoColunas() As Integer
    Dim descricaoMapaCabecalho As String
    Dim formatoCasasDec As String

    Public Sub setComando(ByVal comando As String)
        consulta = comando
    End Sub
    Public Sub setcolunasInvisiveis(ByVal colunasInvisiveis_() As Integer)
        colunasInvisiveis = colunasInvisiveis_
    End Sub

    '0-Esquerda
    '1-Centrado
    '2-Direita
    Public Sub setalinhamentoColunas(ByVal alinhamentoColunas_() As Integer)
        alinhamentoColunas = alinhamentoColunas_
    End Sub

    'descricao a aparecer a quando da impressão do mapa
    Public Sub setdescricaoMapaCabecalho(ByVal valor As String)
        descricaoMapaCabecalho = valor
    End Sub

    Public Sub setCaption(ByVal caption As String)
        Me.Text = caption
    End Sub

    Public Function seleccionouOK() As Boolean
        seleccionouOK = clicouOK
    End Function

    Public Function getValor(ByVal index As Long) As String
        getValor = CStr(grid.SelectedRows(0).Record(index).Value)
    End Function

    Public Sub ShowGroup(ByVal valor As Boolean)
        apresentarGrupo = valor
    End Sub

    Public Sub ShowFilter(ByVal valor As Boolean)
        apresentarFiltro = valor
    End Sub

    Private Sub confirmar()
        clicouOK = True
        Me.Hide()
    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal control As XtremeCommandBars.ICommandBarControl)
        Select Case control.Id
            Case ID_CONFIRMAR : confirmar()
            Case ID_IMPRIMIR : imprimir()
            Case ID_CANCELAR : Me.Close()
        End Select
    End Sub

   

    Private Sub aplicarFiltro()
        Dim lista As StdBELista
        Dim restricao As String
        Dim index As Integer
        Dim Item As ReportRecordItem
        Dim consulta As String
        Dim m As Motor
        m = Motor.GetInstance()
        consulta = grid.Tag
        restricao = " WHERE "

        If grid.SelectedRows.Count > 0 Then
            For index = 0 To grid.Columns.Count - 1
                Item = grid.HeaderRecords(0).Item(index)
                'Set Item = grid.SelectedRows(0).record(index)
                If Item.Value <> "" Then
                    restricao = restricao & "[" & grid.Columns(index).Caption & "]" & " LIKE '%" & Item.Value & "%' AND "
                End If
            Next index

            If restricao <> " WHERE " Then
                restricao = Mid(restricao, 1, Len(restricao) - 5)
            Else
                restricao = ""
            End If

            lista = m.consulta("SELECT * FROM (" & consulta & ") tmpQuery" & restricao)

            inserirDados(lista)
        End If
        bEnterPressed = False
    End Sub

  

    Private Sub UserForm_Activate()
        If Not NaoprimeiraVez Then
            iniciarForm()
            NaoprimeiraVez = True
        End If
    End Sub

    Private Sub iniciarForm()
        Dim lista As StdBELista
        Dim m As Motor
        m = Motor.GetInstance
        clicouOK = False
        grid.Width = Me.Width - 30
        grid.Height = Me.Height - grid.Top - 40
        'Set colunasInvisiveis = Nothing
        'Set lista = BSO.consulta("Select 1")
        lista = New StdBELista
        lista = m.consulta(consulta)
        ' rs = New ADODB.Recordset
        'rs.Open consulta, lista.DataSet.ActiveConnection
        formatoCasasDec = formatoCasasDecimais(2)
        inserirColunas(lista)
        If apresentarFiltro Then inserirCabecalhoPesquisa(lista)
        inserirDados(lista)
        grid.Tag = consulta
        grid.AllowEdit = True
        grid.HeaderRowsAllowEdit = True
        'grid.ShowHeaderRows = apresentarFiltro
        grid.PaintManager.HeaderRowsDividerStyle = xtpReportFixedRowsDividerBold
        grid.PaintManager.VerticalGridStyle = XTPReportGridStyle.xtpGridSolid
        grid.ShowHeaderRows = True
        ' grid.PaintManager.SelectedRowBackColor = Color.Highlight
        ' grid.PaintManager.SelectedRowForeColor = Color.White
        grid.AllowColumnRemove = False
        grid.Focus()
        grid.ShowGroupBox = False 'apresentarGrupo
        criarToolBox()
    End Sub
    Private Sub criarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim control As CommandBarControl

        CommandBarsFrame1.Icons = ImageManager.Icons

        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop) '
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        ' CommandBarsFrame1.Options.LargeIcons = True
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            control = .Add(xtpControlButton, ID_CONFIRMAR, " Confirmar", -1, False)
            control.Style = xtpButtonIconAndCaptionBelow
            control.DescriptionText = "Gravar o Serviço Especial"
            control = .Add(xtpControlButton, ID_IMPRIMIR, " Imprimir", -1, False)
            control.BeginGroup = True
            control.DescriptionText = "Imprimir"
            control.Style = xtpButtonIconAndCaptionBelow
            control = .Add(xtpControlButton, ID_CANCELAR, " Cancelar", -1, False)
            control.BeginGroup = True
            control.DescriptionText = "Cancelar"
            control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub inserirColunas(ByVal lista As StdBELista)
        Dim col As Integer
        Dim Column As ReportColumn
        Dim Item As ReportRecordItem
        For col = 0 To lista.NumColunas - 1
            Column = grid.Columns.Add(col, lista.Nome(col), 10, True)
            Column.Visible = colunaVisivel(col)
            Column.EditOptions.SelectTextOnEdit = True
            Column.Alignment = buscarAlinhamento(col)
            Column.HeaderAlignment = xtpAlignmentCenter
        Next col
    End Sub

    Private Function buscarAlinhamento(ByVal col As Integer) As Integer

        On Error GoTo erro
        If UBound(alinhamentoColunas) > col Then
            Select Case alinhamentoColunas(col)
                Case 0 : buscarAlinhamento = 0
                Case 1 : buscarAlinhamento = 1
                Case 2 : buscarAlinhamento = 2
                Case 3 : buscarAlinhamento = 2
                Case Else : buscarAlinhamento = 0
            End Select
            Exit Function
        Else
            buscarAlinhamento = 0
            Exit Function
        End If
        Exit Function
erro:
        buscarAlinhamento = 0
        Exit Function
    End Function


    Private Function buscarValorAlinhamentoReal(ByVal col As Integer) As Integer

        On Error GoTo erro
        If UBound(alinhamentoColunas) > col Then
            Select Case alinhamentoColunas(col)
                Case 0 : buscarValorAlinhamentoReal = 0
                Case 1 : buscarValorAlinhamentoReal = 1
                Case 2 : buscarValorAlinhamentoReal = 2
                Case 3 : buscarValorAlinhamentoReal = 3
                Case Else : buscarValorAlinhamentoReal = 0
            End Select
            Exit Function
        Else
            buscarValorAlinhamentoReal = 0
            Exit Function
        End If
        Exit Function
erro:
        buscarValorAlinhamentoReal = 0
        Exit Function
    End Function

    Private Sub inserirCabecalhoPesquisa(ByVal lista As StdBELista)
        Dim col As Integer
        Dim Item As ReportRecordItem
        Dim record As ReportRecord
        record = grid.HeaderRecords.Add
        For col = 0 To lista.NumColunas - 1
            Item = record.AddItem("")
        Next col
    End Sub

    Private Function colunaVisivel(ByVal col As Integer) As Boolean
        Dim index As Integer
        colunaVisivel = True
        ' Len coluna
        On Error GoTo erro
        If UBound(colunasInvisiveis) = 0 Then
            colunaVisivel = True
            Exit Function
        End If

        For index = 0 To UBound(colunasInvisiveis) - 1
            If colunasInvisiveis(index) = col Then
                colunaVisivel = False
                Exit Function
            End If
        Next index
        Exit Function
erro:
        colunaVisivel = True
    End Function

    Private Sub inserirDados(ByVal lista As StdBELista)
        Dim col As Integer
        Dim record As ReportRecord
        Dim Item As ReportRecordItem

        removerTodos()

        While Not lista.NoFim
            record = grid.Records.Add()
            For col = 0 To lista.NumColunas - 1
                Item = record.AddItem(lista.valor(col))
                If buscarValorAlinhamentoReal(col) = 3 Then
                    Item.Caption = String.Format(Item.Value, formatoCasasDec)
                    Item.Caption = Item.Caption + "  "
                Else
                    Item.Caption = CStr(Item.Value) + "  "
                End If

                'Item.EditOptions.AllowEdit = False
                Item.Editable = False
            Next col
            lista.Seguinte()
        End While
        grid.Populate()
    End Sub

    Private Sub removerTodos()
        grid.Records.DeleteAll()
    End Sub

    Private Sub imprimir()
        grid.PrintOptions.Header.FormatString = descricaoMapaCabecalho
        grid.PrintOptions.Header.Font.Size = 15
        grid.PrintOptions.BlackWhitePrinting = True
        grid.PrintOptions.Landscape = False
        grid.PrintPreview(True)
        'grid.PrintReport2 True
    End Sub

    Public Function formatoCasasDecimais(ByVal numeroCasasDecimais As Integer)
        Dim casasDecimais As String
        formatoCasasDecimais = ""
        For i = 0 To numeroCasasDecimais - 1
            casasDecimais = casasDecimais + "0"
        Next

        If numeroCasasDecimais >= 0 Then
            formatoCasasDecimais = "###,##0." + casasDecimais
        End If
    End Function

  

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case ID_CONFIRMAR : confirmar()
            Case ID_IMPRIMIR : imprimir()
            Case ID_CANCELAR : Me.Close()
        End Select
    End Sub

    Private Sub grid_PreviewKeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles grid.PreviewKeyDown
       
        If e.KeyCode = 13 And e.Shift = 0 Then
            bEnterPressed = True
        Else
            bEnterPressed = False
        End If
    End Sub

    Private Sub grid_KeyDownEvent(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_KeyDownEvent) Handles grid.KeyDownEvent
        If e.keyCode = Keys.F4 And grid.Navigator.CurrentFocusInFootersRows = False And grid.Navigator.CurrentFocusInHeadersRows = False Then
            clicouOK = True
            Me.Hide()
        End If
    End Sub


    Private Sub grid_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles grid.RowDblClick
        clicouOK = True
        Me.Hide()
    End Sub

    Private Sub grid_ValueChanged(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles grid.ValueChanged
        If bEnterPressed Then
            aplicarFiltro()
        End If
    End Sub
End Class