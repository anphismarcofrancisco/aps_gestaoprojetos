﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAddProjetos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAddProjetos))
        Me.lblDescrição = New System.Windows.Forms.Label()
        Me.txtDescricao = New System.Windows.Forms.TextBox()
        Me.lblProjeto = New System.Windows.Forms.Label()
        Me.txtProjeto = New System.Windows.Forms.TextBox()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDescrição
        '
        Me.lblDescrição.AutoSize = True
        Me.lblDescrição.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescrição.Location = New System.Drawing.Point(14, 140)
        Me.lblDescrição.Name = "lblDescrição"
        Me.lblDescrição.Size = New System.Drawing.Size(99, 25)
        Me.lblDescrição.TabIndex = 43
        Me.lblDescrição.Text = "Descrição"
        '
        'txtDescricao
        '
        Me.txtDescricao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescricao.Location = New System.Drawing.Point(14, 169)
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.Size = New System.Drawing.Size(245, 30)
        Me.txtDescricao.TabIndex = 42
        '
        'lblProjeto
        '
        Me.lblProjeto.AutoSize = True
        Me.lblProjeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjeto.Location = New System.Drawing.Point(14, 78)
        Me.lblProjeto.Name = "lblProjeto"
        Me.lblProjeto.Size = New System.Drawing.Size(73, 25)
        Me.lblProjeto.TabIndex = 41
        Me.lblProjeto.Text = "Projeto"
        '
        'txtProjeto
        '
        Me.txtProjeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProjeto.Location = New System.Drawing.Point(14, 107)
        Me.txtProjeto.Name = "txtProjeto"
        Me.txtProjeto.Size = New System.Drawing.Size(245, 30)
        Me.txtProjeto.TabIndex = 40
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(235, 79)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 39
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(-1, 20)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(287, 55)
        Me.CommandBarsFrame1.TabIndex = 38
        '
        'FrmAddProjetos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 226)
        Me.Controls.Add(Me.lblDescrição)
        Me.Controls.Add(Me.txtDescricao)
        Me.Controls.Add(Me.lblProjeto)
        Me.Controls.Add(Me.txtProjeto)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmAddProjetos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FrmAddProjetos"
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblDescrição As Label
    Friend WithEvents txtDescricao As TextBox
    Friend WithEvents lblProjeto As Label
    Friend WithEvents txtProjeto As TextBox
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
End Class
