﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfPontos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfPontos))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lstPecas = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstPostos1 = New System.Windows.Forms.ListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lstOperacao1 = New System.Windows.Forms.ListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lblPermissoes = New System.Windows.Forms.Label()
        Me.lstFuncionariosPerm = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblPass = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.ckbAdmin = New System.Windows.Forms.CheckBox()
        Me.txtFunc = New System.Windows.Forms.TextBox()
        Me.btnFunc = New System.Windows.Forms.Button()
        Me.txtNomeFunc = New System.Windows.Forms.TextBox()
        Me.lstPostos2 = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lstOperacao2 = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lstPostos3 = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lstOperacao3 = New System.Windows.Forms.ListBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        Me.ckbAddPecas = New System.Windows.Forms.CheckBox()
        Me.btnRemovePC = New System.Windows.Forms.Button()
        Me.btnAddPC = New System.Windows.Forms.Button()
        Me.btnRemoveOP = New System.Windows.Forms.Button()
        Me.btnRemovePostos = New System.Windows.Forms.Button()
        Me.btnAddOP = New System.Windows.Forms.Button()
        Me.btnAddPostos = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(7, 61)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1066, 699)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lstPecas)
        Me.TabPage1.Controls.Add(Me.btnRemovePC)
        Me.TabPage1.Controls.Add(Me.btnAddPC)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.btnRemoveOP)
        Me.TabPage1.Controls.Add(Me.btnRemovePostos)
        Me.TabPage1.Controls.Add(Me.btnAddOP)
        Me.TabPage1.Controls.Add(Me.lstPostos1)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.btnAddPostos)
        Me.TabPage1.Controls.Add(Me.lstOperacao1)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Location = New System.Drawing.Point(4, 33)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1058, 662)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Tabelas Mestre"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'lstPecas
        '
        Me.lstPecas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPecas.FormattingEnabled = True
        Me.lstPecas.ItemHeight = 25
        Me.lstPecas.Location = New System.Drawing.Point(52, 41)
        Me.lstPecas.Name = "lstPecas"
        Me.lstPecas.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstPecas.Size = New System.Drawing.Size(233, 579)
        Me.lstPecas.TabIndex = 53
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(47, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 25)
        Me.Label1.TabIndex = 50
        Me.Label1.Text = "Peças"
        '
        'lstPostos1
        '
        Me.lstPostos1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPostos1.FormattingEnabled = True
        Me.lstPostos1.ItemHeight = 25
        Me.lstPostos1.Location = New System.Drawing.Point(719, 41)
        Me.lstPostos1.Name = "lstPostos1"
        Me.lstPostos1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstPostos1.Size = New System.Drawing.Size(321, 579)
        Me.lstPostos1.TabIndex = 42
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(331, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(99, 25)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Operação"
        '
        'lstOperacao1
        '
        Me.lstOperacao1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOperacao1.FormattingEnabled = True
        Me.lstOperacao1.ItemHeight = 25
        Me.lstOperacao1.Location = New System.Drawing.Point(336, 41)
        Me.lstOperacao1.Name = "lstOperacao1"
        Me.lstOperacao1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstOperacao1.Size = New System.Drawing.Size(321, 579)
        Me.lstOperacao1.TabIndex = 41
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label5.Location = New System.Drawing.Point(714, 15)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 25)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Postos"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lblPermissoes)
        Me.TabPage2.Controls.Add(Me.lstFuncionariosPerm)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.lstPostos2)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.lstOperacao2)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 33)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1058, 662)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Utilizadores"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lblPermissoes
        '
        Me.lblPermissoes.AutoSize = True
        Me.lblPermissoes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermissoes.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblPermissoes.Location = New System.Drawing.Point(688, 129)
        Me.lblPermissoes.Name = "lblPermissoes"
        Me.lblPermissoes.Size = New System.Drawing.Size(114, 25)
        Me.lblPermissoes.TabIndex = 51
        Me.lblPermissoes.Text = "Permissões"
        '
        'lstFuncionariosPerm
        '
        Me.lstFuncionariosPerm.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstFuncionariosPerm.FormattingEnabled = True
        Me.lstFuncionariosPerm.ItemHeight = 25
        Me.lstFuncionariosPerm.Location = New System.Drawing.Point(693, 157)
        Me.lstFuncionariosPerm.Name = "lstFuncionariosPerm"
        Me.lstFuncionariosPerm.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstFuncionariosPerm.Size = New System.Drawing.Size(318, 479)
        Me.lstFuncionariosPerm.TabIndex = 50
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ckbAddPecas)
        Me.GroupBox1.Controls.Add(Me.lblPass)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.ckbAdmin)
        Me.GroupBox1.Controls.Add(Me.txtFunc)
        Me.GroupBox1.Controls.Add(Me.btnFunc)
        Me.GroupBox1.Controls.Add(Me.txtNomeFunc)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(853, 120)
        Me.GroupBox1.TabIndex = 49
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Utilizador"
        '
        'lblPass
        '
        Me.lblPass.AutoSize = True
        Me.lblPass.Location = New System.Drawing.Point(89, 79)
        Me.lblPass.Name = "lblPass"
        Me.lblPass.Size = New System.Drawing.Size(92, 24)
        Me.lblPass.TabIndex = 40
        Me.lblPass.Text = "Password"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(187, 79)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(143, 29)
        Me.txtPassword.TabIndex = 39
        '
        'ckbAdmin
        '
        Me.ckbAdmin.AutoSize = True
        Me.ckbAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbAdmin.Location = New System.Drawing.Point(345, 80)
        Me.ckbAdmin.Name = "ckbAdmin"
        Me.ckbAdmin.Size = New System.Drawing.Size(151, 29)
        Me.ckbAdmin.TabIndex = 38
        Me.ckbAdmin.Text = "Administrador"
        Me.ckbAdmin.UseVisualStyleBackColor = True
        '
        'txtFunc
        '
        Me.txtFunc.Location = New System.Drawing.Point(19, 32)
        Me.txtFunc.Name = "txtFunc"
        Me.txtFunc.ReadOnly = True
        Me.txtFunc.Size = New System.Drawing.Size(100, 29)
        Me.txtFunc.TabIndex = 1
        '
        'btnFunc
        '
        Me.btnFunc.Location = New System.Drawing.Point(135, 32)
        Me.btnFunc.Name = "btnFunc"
        Me.btnFunc.Size = New System.Drawing.Size(46, 29)
        Me.btnFunc.TabIndex = 0
        Me.btnFunc.Text = "..."
        Me.btnFunc.UseVisualStyleBackColor = True
        '
        'txtNomeFunc
        '
        Me.txtNomeFunc.Location = New System.Drawing.Point(187, 32)
        Me.txtNomeFunc.Name = "txtNomeFunc"
        Me.txtNomeFunc.ReadOnly = True
        Me.txtNomeFunc.Size = New System.Drawing.Size(623, 29)
        Me.txtNomeFunc.TabIndex = 2
        '
        'lstPostos2
        '
        Me.lstPostos2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPostos2.FormattingEnabled = True
        Me.lstPostos2.ItemHeight = 25
        Me.lstPostos2.Location = New System.Drawing.Point(335, 157)
        Me.lstPostos2.Name = "lstPostos2"
        Me.lstPostos2.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstPostos2.Size = New System.Drawing.Size(318, 479)
        Me.lstPostos2.TabIndex = 46
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(10, 129)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 25)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Operação"
        '
        'lstOperacao2
        '
        Me.lstOperacao2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOperacao2.FormattingEnabled = True
        Me.lstOperacao2.ItemHeight = 25
        Me.lstOperacao2.Location = New System.Drawing.Point(15, 157)
        Me.lstOperacao2.Name = "lstOperacao2"
        Me.lstOperacao2.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstOperacao2.Size = New System.Drawing.Size(279, 479)
        Me.lstOperacao2.TabIndex = 45
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label3.Location = New System.Drawing.Point(330, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 25)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Postos"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lstPostos3)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.lstOperacao3)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Location = New System.Drawing.Point(4, 33)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1058, 662)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Postos"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lstPostos3
        '
        Me.lstPostos3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPostos3.FormattingEnabled = True
        Me.lstPostos3.ItemHeight = 25
        Me.lstPostos3.Location = New System.Drawing.Point(42, 40)
        Me.lstPostos3.Name = "lstPostos3"
        Me.lstPostos3.Size = New System.Drawing.Size(318, 604)
        Me.lstPostos3.TabIndex = 50
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(401, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 25)
        Me.Label4.TabIndex = 51
        Me.Label4.Text = "Operação"
        '
        'lstOperacao3
        '
        Me.lstOperacao3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOperacao3.FormattingEnabled = True
        Me.lstOperacao3.ItemHeight = 25
        Me.lstOperacao3.Location = New System.Drawing.Point(406, 40)
        Me.lstOperacao3.Name = "lstOperacao3"
        Me.lstOperacao3.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstOperacao3.Size = New System.Drawing.Size(342, 604)
        Me.lstOperacao3.TabIndex = 49
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(37, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 25)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Postos"
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(308, 12)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 34
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(1, 0)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(211, 55)
        Me.CommandBarsFrame1.TabIndex = 33
        '
        'ckbAddPecas
        '
        Me.ckbAddPecas.AutoSize = True
        Me.ckbAddPecas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbAddPecas.Location = New System.Drawing.Point(502, 80)
        Me.ckbAddPecas.Name = "ckbAddPecas"
        Me.ckbAddPecas.Size = New System.Drawing.Size(167, 29)
        Me.ckbAddPecas.TabIndex = 41
        Me.ckbAddPecas.Text = "Adiciona Peças"
        Me.ckbAddPecas.UseVisualStyleBackColor = True
        '
        'btnRemovePC
        '
        Me.btnRemovePC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.close16
        Me.btnRemovePC.Location = New System.Drawing.Point(11, 84)
        Me.btnRemovePC.Name = "btnRemovePC"
        Me.btnRemovePC.Size = New System.Drawing.Size(35, 37)
        Me.btnRemovePC.TabIndex = 52
        Me.btnRemovePC.UseVisualStyleBackColor = True
        '
        'btnAddPC
        '
        Me.btnAddPC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        Me.btnAddPC.Location = New System.Drawing.Point(11, 41)
        Me.btnAddPC.Name = "btnAddPC"
        Me.btnAddPC.Size = New System.Drawing.Size(35, 37)
        Me.btnAddPC.TabIndex = 51
        Me.btnAddPC.UseVisualStyleBackColor = True
        '
        'btnRemoveOP
        '
        Me.btnRemoveOP.Image = Global.APS_GestaoProjectos.My.Resources.Resources.close16
        Me.btnRemoveOP.Location = New System.Drawing.Point(295, 84)
        Me.btnRemoveOP.Name = "btnRemoveOP"
        Me.btnRemoveOP.Size = New System.Drawing.Size(35, 37)
        Me.btnRemoveOP.TabIndex = 46
        Me.btnRemoveOP.UseVisualStyleBackColor = True
        '
        'btnRemovePostos
        '
        Me.btnRemovePostos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.close16
        Me.btnRemovePostos.Location = New System.Drawing.Point(678, 84)
        Me.btnRemovePostos.Name = "btnRemovePostos"
        Me.btnRemovePostos.Size = New System.Drawing.Size(35, 37)
        Me.btnRemovePostos.TabIndex = 48
        Me.btnRemovePostos.UseVisualStyleBackColor = True
        '
        'btnAddOP
        '
        Me.btnAddOP.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        Me.btnAddOP.Location = New System.Drawing.Point(295, 41)
        Me.btnAddOP.Name = "btnAddOP"
        Me.btnAddOP.Size = New System.Drawing.Size(35, 37)
        Me.btnAddOP.TabIndex = 45
        Me.btnAddOP.UseVisualStyleBackColor = True
        '
        'btnAddPostos
        '
        Me.btnAddPostos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.add116
        Me.btnAddPostos.Location = New System.Drawing.Point(678, 41)
        Me.btnAddPostos.Name = "btnAddPostos"
        Me.btnAddPostos.Size = New System.Drawing.Size(35, 37)
        Me.btnAddPostos.TabIndex = 47
        Me.btnAddPostos.UseVisualStyleBackColor = True
        '
        'FrmConfPontos
        '
        Me.ClientSize = New System.Drawing.Size(1078, 792)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1094, 831)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1094, 831)
        Me.Name = "FrmConfPontos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Menu Opções"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnRemovePC As Button
    Friend WithEvents btnAddPC As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents btnRemoveOP As Button
    Friend WithEvents btnRemovePostos As Button
    Friend WithEvents btnAddOP As Button
    Friend WithEvents lstPostos1 As ListBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btnAddPostos As Button
    Friend WithEvents lstOperacao1 As ListBox
    Friend WithEvents Label5 As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lstPecas As ListBox
    Friend WithEvents txtNomeFunc As TextBox
    Friend WithEvents txtFunc As TextBox
    Friend WithEvents btnFunc As Button
    Friend WithEvents lstPostos2 As ListBox
    Friend WithEvents Label2 As Label
    Friend WithEvents lstOperacao2 As ListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblPass As Label
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents ckbAdmin As CheckBox
    Friend WithEvents lblPermissoes As Label
    Friend WithEvents lstFuncionariosPerm As ListBox
    Friend WithEvents lstPostos3 As ListBox
    Friend WithEvents Label4 As Label
    Friend WithEvents lstOperacao3 As ListBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ckbAddPecas As CheckBox
End Class
