﻿
Public NotInheritable Class MDtPicker

    Private Sub New()

    End Sub

    Public Const Name As String = "test"

    Public Shared i As Integer = 10

    Public Shared Sub HideDatePicker(ByRef DTPicker As AxXtremeSuiteControls.AxDateTimePicker)
        With DTPicker
            DTPicker.Location = New Point(-1000, -10000)
            .Visible = False
        End With
    End Sub


    Public Shared Sub ShowDatePicker(ByVal rc As AxXtremeReportControl.AxReportControl, ByVal DTPicker As AxXtremeSuiteControls.AxDateTimePicker, ByVal Row As XtremeReportControl.IReportRow, ByVal Column As XtremeReportControl.IReportColumn, ByVal Item As XtremeReportControl.IReportRecordItem)
        Dim l As Long, t As Long, r As Long, b As Long, x As Long, y As Long

        rc.Rows(Row.Index).GetItemRect(Item, l, t, r, b)
        l = l '* x
        t = t '* y
        r = r '* x
        b = b '* y

        With DTPicker
            ' MsgBox(New Point((rc.Left + x) + 1, (rc.Top + y) + t).ToString)
            DTPicker.Location = New Point((rc.Left + x) + 1, (rc.Top + y) + t)
            DTPicker.Location = New Point(rc.Left + l, rc.Top + t + 1)
            DTPicker.Size = New Size(r - l + 2, b - t - 1)
            .Visible = True
            If Not IsDate(Item.Value) Then
                .Value = Now.ToShortDateString
            Else
                .Value = Item.Value
            End If

        End With
    End Sub



End Class

