﻿Imports PlatAPSNET
Imports Interop.GcpBE900
Imports Interop.StdBE900
Imports Interop.ErpBS900
Imports System.IO
Imports XtremeReportControl
Imports Interop.StdPlatBS900

Public Class MotorGE

    Shared myInstance As MotorGE
    Dim bso As ErpBS
    Dim PlataformaPrimavera As StdPlatBS
    Dim nomeEmpresa_ As String
    Dim codEmpresa As String
    Dim utilizador As String
    Dim password As String
    Dim tipoPlat As String
    Dim ficheiroAccess_ As String
    Dim file As String
    Dim log As TextBox
    Dim funcionario_ As String
    Dim nomefuncionario_ As String

    Dim funcionarioAdm_ As Boolean

    ' Dim modoFuncionamento As String

    Dim empresas_ As String
    Dim apliInicializada As Boolean

    ' Dim ArmazemDefault As String

    Dim objConfApl As StdBSConfApl

    Dim entidadeDefault As String

    Dim infModLM As InfModLstMaterial
    Dim emailDestino As String

    Dim visualizarEmail As String

    Dim numLicenciamentos As Integer


    Dim apresentaFormAPL As Boolean
    Dim serieC_ As String


    Const COLUMN_ARTIGO As Integer = 0
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTIDADE As Integer = 2
    Const COLUMN_STKACTUAL As Integer = 3
    Const COLUMN_ULTPRECO As Integer = 4
    Const COLUMN_FORNPRECO As Integer = 5
    Const COLUMN_DATARECEPCAO As Integer = 6
    Const COLUMN_CHECKBOX As Integer = 7

    Const ENCCOLUMN_ARTIGO As Integer = 0
    Const ENCCOLUMN_DESCRICAO As Integer = 1
    Const ENCCOLUMN_ULTPRECO As Integer = 2
    Const ENCCOLUMN_STKACTUAL As Integer = 3
    Const ENCCOLUMN_QUANTIDADE As Integer = 4
    Const ENCCOLUMN_QUANTIDADEENT As Integer = 5
    Const ENCCOLUMN_QUANTIDADEFALTA As Integer = 6
    Const ENCCOLUMN_FORNPRECO As Integer = 7
    Const ENCCOLUMN_LINHADATA As Integer = 8
    Const ENCCOLUMN_CHECKBOX As Integer = 9


    Private Sub New()
        Try

            '  AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf CurrentDomain_AssemblyResolve

            Dim ficheiroConfiguracao As Configuracao
            Dim ficheiroIni As IniFile

            apliInicializada = False
            ficheiroConfiguracao = Configuracao.GetInstance
            ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)

            bso = New ErpBS
            PlataformaPrimavera = New StdPlatBS
            Dim seg As Seguranca
            seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

            utilizador = ficheiroIni.GetString("ERP", "UTILIZADOR", "")

            utilizador = seg.decifrarStringTripleDES(utilizador)

            password = ficheiroIni.GetString("ERP", "PASSWORD", "")

            password = seg.decifrarStringTripleDES(password)

            codEmpresa = ficheiroIni.GetString("ERP", "EMPRESA", "")
            empresas_ = ficheiroIni.GetString("ERP", "EMPRESAS", "'" + codEmpresa + "'")
            tipoPlat = ficheiroIni.GetString("ERP", "TIPOPLAT", 1)


            serieC_ = ficheiroIni.GetString("LISTAENCOMENDAS", "SERIE", "")

            tipoEntidadeC_ = ficheiroIni.GetString("LISTAENCOMENDAS", "TIPOENTIDADEC", "F")

            entidadeDefault = ficheiroIni.GetString("LISTAENCOMENDAS", "ENTIDADEDEFAULT", "FVD")

            infModLM = New InfModLstMaterial
            infModLM.TIPODOCLM = ficheiroIni.GetString("LISTAENCOMENDAS", "TIPODOCLM", "")
            infModLM.SERIELM = ficheiroIni.GetString("LISTAENCOMENDAS", "SERIELM", "")
            infModLM.MAPALM = ficheiroIni.GetString("LISTAENCOMENDAS", "MAPALM", "")
            infModLM.TIPODOCCOT = ficheiroIni.GetString("LISTAENCOMENDAS", "TIPODOCCOT", "")
            infModLM.SERIECOT = ficheiroIni.GetString("LISTAENCOMENDAS", "SERIECOT", "")
            infModLM.MAPACOT = ficheiroIni.GetString("LISTAENCOMENDAS", "MAPACOT", "")
            infModLM.TIPODOCECF = ficheiroIni.GetString("LISTAENCOMENDAS", "TIPODOCECF", "")
            infModLM.SERIEECF = ficheiroIni.GetString("LISTAENCOMENDAS", "SERIEECF", "")
            infModLM.MAPAECF = ficheiroIni.GetString("LISTAENCOMENDAS", "MAPAECF", "")
            infModLM.TIPODOCSTK = ficheiroIni.GetString("LISTAENCOMENDAS", "TIPODOCSTK", "")
            infModLM.SERIESTK = ficheiroIni.GetString("LISTAENCOMENDAS", "SERIESTK", "")
            infModLM.ARTIGO = ficheiroIni.GetString("LISTAENCOMENDAS", "ARTIGO", "")
            infModLM.EMAILECF = ficheiroIni.GetString("LISTAENCOMENDAS", "EMAIL", "")

            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = utilizador
            objConfApl.PwdUtilizador = password
            objConfApl.LicVersaoMinima = "9.00"

            visualizarEmail = ficheiroIni.GetString("LISTAENCOMENDAS", "VISUALIZAR_EMAIL", "1")

            verificarLicenciamento(ficheiroIni, seg)

            bso.AbreEmpresaTrabalho(tipoPlat, codEmpresa, utilizador, password, , objConfApl.Instancia)

            PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")

            apresentaFormAPL = ficheiroIni.GetString("ERP", "CONFIGARTIGO", "") = "1"

        Catch ex As Exception
            MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub verificarLicenciamento(ficheiroIni As IniFile, seg As Seguranca)
        Dim StrnumLicenciamentos As String
        StrnumLicenciamentos = ficheiroIni.GetString("ERP", "NUMPOSTOS", "-1")

        If StrnumLicenciamentos = "-1" Then
            StrnumLicenciamentos = "999999"
        Else
            If StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos) Then
                StrnumLicenciamentos = "0"
            Else
                StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos)
            End If
        End If

        If IsNumeric(StrnumLicenciamentos) Then
            numLicenciamentos = CInt(StrnumLicenciamentos)
        Else
            numLicenciamentos = 0
        End If
    End Sub




    Public Function getViewFormAPL() As Boolean
        Return apresentaFormAPL
    End Function

    ''' <summary>
    ''' Funcao que permite retornar a entidae por defeito
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getEntidadeDefault() As String
        Return entidadeDefault
    End Function


    Public Sub setEmpresa(ByVal empresa_ As String)
        If codEmpresa <> empresa_ Then
            Try
                bso.FechaEmpresaTrabalho()
                bso.AbreEmpresaTrabalho(tipoPlat, empresa_, utilizador, password)
                codEmpresa = empresa_
            Catch ex As Exception
                MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            End Try
        End If

    End Sub



    ''' <summary>
    ''' Função que permite ir buscar a Informacao do Modulo de Lista de Materiais.
    ''' </summary>
    ''' <remarks></remarks>

    ReadOnly Property InformModLstMaterial() As InfModLstMaterial
        Get
            InformModLstMaterial = infModLM
        End Get
    End Property


    ''' <summary>
    ''' Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Funcionario() As String
        Get
            Funcionario = funcionario_
        End Get
        Set(ByVal value As String)
            funcionario_ = value
        End Set
    End Property

    ''' <summary>
    ''' Nome Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property NomeFuncionario() As String
        Get
            NomeFuncionario = nomefuncionario_
        End Get
        Set(ByVal value As String)
            nomefuncionario_ = value
        End Set
    End Property


    ''' <summary>
    ''' Permite validar se um funcionario é administrador
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property FuncionarioAdm() As Boolean
        Get
            FuncionarioAdm = funcionarioAdm_
        End Get
        Set(ByVal value As Boolean)
            funcionarioAdm_ = value
        End Set
    End Property



    Dim tipoEntidadeC_ As String = "C"
    Property TipoEntidadeC() As String
        Get
            TipoEntidadeC = tipoEntidadeC_
        End Get
        Set(ByVal value As String)
            tipoEntidadeC_ = value
        End Set
    End Property



    ReadOnly Property Empresa() As String
        Get
            Empresa = codEmpresa
        End Get

    End Property

    ReadOnly Property EmpresaDescricao() As String
        Get
            EmpresaDescricao = Trim(bso.Contexto.IDNome)
        End Get

    End Property

    ReadOnly Property EmpresaAberta() As Boolean
        Get
            EmpresaAberta = bso.Contexto.EmpresaAberta
        End Get
    End Property

    Property AplicacaoInicializada() As Boolean
        Get
            AplicacaoInicializada = apliInicializada
        End Get
        Set(ByVal value As Boolean)
            apliInicializada = value
        End Set
    End Property

    Public Sub setLog(ByVal log_ As TextBox)
        log = log_
    End Sub

    Public Sub fechaEmpresa()
        bso.FechaEmpresaTrabalho()
        PlataformaPrimavera.FechaPlataforma()
    End Sub

    Public Shared Function GetInstance() As MotorGE
        If myInstance Is Nothing Then
            myInstance = New MotorGE
        End If
        Return myInstance
    End Function


    ''' <summary>
    ''' Função que permite a execução de um comando 
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function executarComando(ByVal comando As String)
        Try
            bso.DSO.Plat.ExecSql.ExecutaXML(comando)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return ""
    End Function


    Public Function consulta(ByVal query As String, Optional apresentaMsgErro As Boolean = True) As StdBELista
        Try
            Return bso.Consulta(query)
        Catch ex As Exception
            If apresentaMsgErro Then
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End If
            Return Nothing
        End Try

    End Function

    Public Function consultaValor(ByVal query As String) As String
        Dim lista As StdBELista        '
        lista = bso.Consulta(query)
        If lista.NoFim Then
            Return ""
        Else
            Return lista.Valor(0).ToString
        End If
    End Function

    Public Function convertRecordSetToDataTable(ByVal MyRs As ADODB.Recordset) As DataTable
        'Create and fill the dataset from the recordset and populate grid from Dataset. 
        Dim myDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter()
        Dim myDS As DataSet = New DataSet("MyTable")
        myDA.Fill(myDS, MyRs, "MyTable")
        Return myDS.Tables("MyTable")
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Fornecedores inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaFornecedores(ByVal restricao As String, listaFornecedores As String) As StdBELista
        Try
            Dim lista As StdBELista
            If listaFornecedores = "" Then
                If restricao <> "" Then
                    lista = consulta("SELECT Fornecedor as Codigo,Nome FROM Fornecedores WHERE FornecedorAnulado=0 and Nome LIKE '%" + restricao + "%' ORDER BY Nome")
                Else
                    lista = consulta("SELECT Fornecedor as Codigo,Nome FROM Fornecedores WHERE FornecedorAnulado=0 Order By Nome")
                End If
            Else
                lista = consulta("SELECT Fornecedor as Codigo,Nome FROM Fornecedores WHERE Fornecedor in (" + listaFornecedores + ") Order By Nome")
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function NuloToDate(ByVal obj As Object, ByVal def As DateTime) As DateTime
        If IsDBNull(obj) Then
            NuloToDate = def
        Else
            If Not IsDate(obj) Then
                NuloToDate = def
            Else
                NuloToDate = obj
            End If
        End If
    End Function

    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                NuloToDouble = 0
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Public Function NuloToString(ByVal obj) As String
        If IsDBNull(obj) Then
            NuloToString = ""
        Else
            NuloToString = CStr(obj)
        End If
    End Function

    Public Function daListaArtigosDocumento(ByVal idDocumento As String, ByVal modulo As String) As DataTable
        Dim lista As StdBELista = Nothing
        Dim dt As DataTable = Nothing
        If modulo = "I" Then
            lista = consulta("SELECT APS_GP_Artigos.*,LinhasInternos.Quantidade FROM APS_GP_Artigos INNER JOIN LinhasInternos ON APS_GP_Artigos.Artigo=LinhasInternos.Artigo WHERE LinhasInternos.IdCabecInternos='" + idDocumento + "' ORDER BY Numlinha")
        End If

        If modulo = "C" Then
            lista = consulta("SELECT  LinhasCompras.*,CDU_ObraN5 as Estado FROM LinhasCompras WHERE LinhasCompras.IdCabecCompras='" + idDocumento + "' ORDER BY NUMLINHA")
        End If

        If modulo = "V" Then
            lista = consulta("SELECT  LinhasDoc.* FROM LinhasDoc WHERE LinhasDoc.IdCabecDoc='" + idDocumento + "' ORDER BY NUMLINHA")
        End If

        If Not lista Is Nothing Then
            dt = convertRecordSetToDataTable(lista.DataSet)
        End If

        Return dt
    End Function

    Public Function actualizarDocumentoCompra(fornecedor As Fornecedor, ByVal tipodoc As String, ByVal func As String, observacoes As String, idCabecDocOrigem As String, ByRef recordsGrid As Object, dataRecepcao As String, Optional ByVal idCabecdoc As String = "", Optional botao As Integer = 0, Optional indexTabSelel As Integer = 0, Optional conferido As Boolean = False) As String

        Dim docC As GcpBEDocumentoCompra
        Dim linhaC As GcpBELinhaDocumentoCompra

        Dim id As String

        docC = Nothing

        Dim data As Date
        data = Now
        If idCabecdoc <> "" Then
            id = idCabecdoc
            docC = bso.Comercial.Compras.EditaID(id)
            data = docC.DataDoc
        End If


        If docC Is Nothing Then
            docC = New GcpBEDocumentoCompra
            docC.Tipodoc = tipodoc
            If serieC_ <> "" Then
                docC.Serie = serieC_
            Else
                docC.Serie = bso.Comercial.Series.DaSerieDefeito("C", tipodoc, data)
            End If

            docC.Entidade = fornecedor.Fornecedor
            docC.TipoEntidade = tipoEntidadeC_
            bso.Comercial.Compras.PreencheDadosRelacionados(docC)

            If docC.CamposUtil.Existe("CDU_criadopor") Then
                docC.CamposUtil("CDU_criadopor").Valor = func
            End If
        End If

        docC.CamposUtil("CDU_CabVar1").Valor = func
        docC.CamposUtil("CDU_CabVar2").Valor = dataRecepcao
        docC.CamposUtil("CDU_Conferido").Valor = conferido

        docC.IdDocOrigem = idCabecDocOrigem
        docC.ModuloOrigem = "C"

        docC.Utilizador = func
        docC.DataDoc = CDate(data)
        docC.DataVenc = docC.DataDoc
        docC.DataIntroducao = docC.DataDoc
        docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
        docC.Observacoes = observacoes

        Dim documentoT As String
        documentoT = consultaValor("SELECT BensCirculacao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")

        Dim index As Integer
        index = 1
        If documentoT <> "True" Or Not docC.EmModoEdicao Then

            '   docC.Linhas.RemoveTodos()
            Dim artigo As String
            Dim quantidade As Double
            Dim quantidadeOrig As Double
            Dim row As ReportRecord
            Dim unidade As String
            '   Dim projecto As String
            Dim precunit As Double
            Dim descricao As String
            Dim dtrow As DataRow

            removerLinhasEliminadas(docC, recordsGrid)
            Dim adicionarNovaLinha As Boolean

            For i = 0 To recordsGrid.Count - 1
                adicionarNovaLinha = False
                row = recordsGrid(i)
                dtrow = row.Tag
                linhaC = buscarLinha(dtrow("Id").ToString(), docC)

                artigo = buscarArtigo(row(COLUMN_ARTIGO).Value, infModLM.ARTIGO)

                descricao = row(COLUMN_DESCRICAO).Value
                If botao = 2 And indexTabSelel = 2 Then
                    quantidade = row(ENCCOLUMN_QUANTIDADE).Value
                    precunit = IIf(IsNumeric(row(ENCCOLUMN_FORNPRECO).Value), row(ENCCOLUMN_FORNPRECO).Value, 0)
                Else
                    quantidade = row(COLUMN_QUANTIDADE).Value
                    precunit = IIf(IsNumeric(row(COLUMN_FORNPRECO).Value), row(COLUMN_FORNPRECO).Value, 0)
                End If
                unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")

                If linhaC Is Nothing Then
                    bso.Comercial.Compras.AdicionaLinha(docC, artigo, quantidade)
                    linhaC = docC.Linhas(docC.Linhas.NumItens)
                    If botao = 0 Then
                        dtrow("Id") = Replace(Replace(linhaC.IdLinha, "{", ""), "}", "")
                    End If
                End If


                quantidadeOrig = linhaC.Quantidade
                linhaC.Quantidade = quantidade
                linhaC.Descricao = descricao
                linhaC.PrecUnit = precunit

                If botao <> 0 Then

                    If indexTabSelel <> 2 Then
                        actualizarEstadoLinha("C", dtrow("Id").ToString, IIf(botao = 1, "C", "E"))
                        '  actualizarEstadoLinha("C", BuscarIdCopiaLinha(dtrow("Id").ToString), IIf(botao = 1, "C", "E"))
                    Else
                        If row(9).Checked Then
                            If row(ENCCOLUMN_QUANTIDADEENT).Value + row(ENCCOLUMN_QUANTIDADEFALTA).Value >= row(ENCCOLUMN_QUANTIDADE).Value Then
                                actualizarEstadoLinha("C", dtrow("Id").ToString, "V")
                                linhaC.CamposUtil("CDU_ObraN5") = "V"
                                linhaC.CamposUtil("CDU_DataRecepcao") = row(8).Value
                            End If
                        End If
                    End If
                    If botao <> indexTabSelel Then
                        linhaC.IdLinhaOrigemCopia = dtrow("Id").ToString
                        linhaC.ModuloOrigemCopia = "C"
                    End If

                End If

                index = index + 1

                If conferido Then
                    actualizarEstadoLinha("C", dtrow("Id").ToString, "F")
                End If
            Next
        End If
        Try
            If docC.Linhas.NumItens > 0 Or idCabecdoc <> "" Then
                bso.Comercial.Compras.Actualiza(docC)
                If indexTabSelel = 2 Then actualizarEntradasStocks(docC, recordsGrid)
                actualizarValoresPCUForn(docC)

                Return docC.ID
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function



    ''' <summary>
    ''' Permite ir buscar o artigo a ser utilizado na criação do documento
    ''' </summary>
    ''' <param name="artigo"></param>
    ''' <param name="artigoDefault"></param>
    ''' <returns></returns>
    Private Function buscarArtigo(artigo As String, artigoDefault As String)
        If artigo Is Nothing Then
            Return artigoDefault
        End If
        If artigo.Trim() = "" Or artigo = artigoDefault Then
            Return artigoDefault
        End If

        If bso.Comercial.Artigos.Existe(artigo) Then
            Return artigo
        Else
            Return artigoDefault
        End If
    End Function

    Private Function buscarLinha(idLinha As String, doc As GcpBEDocumentoCompra) As GcpBELinhaDocumentoCompra

        Dim i As Integer
        For i = 1 To doc.Linhas.NumItens
            If doc.Linhas(i).IdLinha = UCase("{" + idLinha + "}") Then
                Return doc.Linhas(i)
            End If
        Next
        Return Nothing
    End Function

    Private Sub removerLinhasEliminadas(ByRef doc As GcpBEDocumentoCompra, reportRecord As Object)

        Dim i As Integer
        Dim j As Integer
        Dim existe As Boolean
        Dim row As DataRow
        For i = doc.Linhas.NumItens To 1 Step -1
            existe = False
            For j = reportRecord.count - 1 To 0 Step -1
                row = reportRecord(j).tag
                If doc.Linhas(i).IdLinha = UCase("{" + row("Id").ToString() + "}") Then
                    existe = True
                    Exit For
                End If
            Next
            If Not existe Then
                doc.Linhas.Remove(i)
            End If
        Next

    End Sub


    'Public Function VerficarFuncionarioAdm(ByVal funcionario As String) As Boolean
    '    Dim lista As StdBELista
    '    Try
    '        lista = bso.Consulta("SELECT ISNULL(CDU_Administrador,0) FROM APS_GP_Funcionarios WHERE Codigo='" + funcionario + "'")
    '        If lista.NoFim Then
    '            Return False
    '        Else
    '            Return CBool(lista.Valor(0))
    '        End If
    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function




    ''' <summary>
    ''' Função que permite verificar se um projecto existe
    ''' </summary>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeProjecto(ByVal projecto As String) As Boolean
        Return bso.Comercial.Projectos.Existe(projecto)
    End Function

    Public Sub enviarEmailDocumento(ByVal idDocumento As String, ByVal modulo As String, Optional destino As String = "")
        Cursor.Current = Cursors.WaitCursor
        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(idDocumento)
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, doc.Moeda, 2)
            Dim str As String
            Dim mapa As Mapa
            mapa = New Mapa
            Dim gcpSerie As GcpBESerie
            Dim tabCompras As GcpBETabCompra
            Dim tempFolder As String
            tempFolder = System.IO.Path.GetTempPath + "GCPMailTemp\Document.pdf"
            If Not Directory.Exists(System.IO.Path.GetTempPath + "GCPMailTemp") Then Directory.CreateDirectory(System.IO.Path.GetTempPath + "GCPMailTemp")
            tabCompras = bso.Comercial.TabCompras.Edita(doc.Tipodoc)
            gcpSerie = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
            mapa.iniciarComponente(buscarMapaDefault(doc.Tipodoc, gcpSerie.Config), codEmpresa, objConfApl.Instancia, utilizador, password, PlataformaPrimavera)
            mapa.Inicializar()
            str = "{CabecCompras.Filial}='" & doc.Filial & "' And {CabecCompras.Serie}='" & doc.Serie & "' And {CabecCompras.TipoDoc}='" & doc.Tipodoc & "' and {CabecCompras.NumDoc}=" & CStr(doc.NumDoc)
            mapa.selectionFormula(str)
            mapa.setFormulaFieldsByName("DadosEmpresa", rp.getFormula())

            Dim certificado As String
            certificado = bso.Comercial.Compras.DevolveTextoAssinaturaDocID(doc.ID)
            mapa.setFormulaFieldsByName("NumVia", "'Original'")
            mapa.setFormulaFieldsByName("InicializaParametros", rp.getFormulaIniciarParametros(doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado)))
            mapa.exportarDiscoPDF(tempFolder)
            PlataformaPrimavera.Mail.Inicializa()

            Dim emailDestino As String
            Dim profile As String
            profile = ""
            emailDestino = ""
            If PlataformaPrimavera.PrefUtilStd.EmailMAPI Then
                profile = PlataformaPrimavera.PrefUtilStd.EmailMAPIProfile
            Else
                'PlataformaPrimavera.Mail.SMTPServer = PlataformaPrimavera.PrefUtilStd.EmailServSMTP
                'PlataformaPrimavera.Mail.EnderecoLocal = PlataformaPrimavera.PrefUtilStd.EmailEndereco
            End If

            If tabCompras.EmailFixo Then
                emailDestino = tabCompras.EmailTo
            Else
                If tabCompras.EmailTo <> "" Then
                    Dim listaEmails As StdBELista
                    listaEmails = consulta("Select Email FROM LinhasContactoEntidades WHERE TipoEntidade='" + doc.TipoEntidade + "' and Entidade='" + doc.Entidade + "' and TipoContacto='" + tabCompras.EmailTo + "'")
                    While Not listaEmails.NoFim
                        If emailDestino <> "" Then emailDestino = emailDestino + ";"
                        emailDestino = emailDestino + listaEmails.Valor("Email")
                        listaEmails.Seguinte()
                    End While
                    ' emailDestino = consultaValor("Select Email FROM LinhasContactoEntidades WHERE TipoEntidade='" + doc.TipoEntidade + "' and Entidade='" + doc.Entidade + "' and TipoContacto='" + tabCompras.EmailTo + "'")
                End If
            End If


            Try
                Dim previsualizar As Boolean
                If visualizarEmail = "1" Then
                    previsualizar = True
                Else
                    previsualizar = tabCompras.EmailVisualizar
                End If

                If destino <> "" Then
                    emailDestino = destino
                    previsualizar = False
                End If

                PlataformaPrimavera.Mail.EnviaMailEx(emailDestino, tabCompras.EmailCC, tabCompras.EmailBCC, doc.Tipodoc + " " + CStr(doc.NumDoc) + "/" + doc.Serie, tabCompras.EMailTexto, tempFolder, previsualizar)
                '   If destino = "" Then MsgBox("Email enviado com sucesso", MsgBoxStyle.Information)
            Catch ex As Exception
                Cursor.Current = Cursors.Default
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try

        End If
        Cursor.Current = Cursors.Default

    End Sub

    ''' <summary>
    ''' Existe documento de compra
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function existeDocumentoCompra(ByVal id As String) As String

        If id = "" Then
            Return ""
        End If

        If bso.Comercial.Compras.ExisteID(id) Then
            Return id
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoVenda(ByVal id As String) As String
        existeDocumentoVenda = ""
        If bso.Comercial.Vendas.ExisteID(id) Then
            Return id
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' IdCabecDocOriginal
    ''' </summary>
    ''' <param name="documentoCompra"></param>
    ''' <param name="idCabecDoc"></param>
    ''' <param name="verTodos"></param>
    ''' <returns></returns>
    Public Function existeDocumentosCompraRelacionado(ByVal documentoCompra As String, idCabecDoc As String, verTodos As Boolean) As StdBELista
        existeDocumentosCompraRelacionado = Nothing
        Dim idProj As String
        Dim lista As StdBELista
        If documentoCompra <> "" And idCabecDoc <> "" Then
            Dim restricaoAbertoFechado As String = ""
            If Not verTodos Then
                restricaoAbertoFechado = " AND (CabecComprasStatus.Anulado=0)"
            End If

            lista = bso.Consulta("SELECT CabecCompras.*,CabecComprasStatus.Anulado as StatusAnulado, CabecComprasStatus.Fechado as StatusFechado from CabecCompras inner join CabecComprasStatus ON CabecCompras.ID=CabecComprasStatus.IDCabecCompras  where CabecCompras.Tipodoc='" + documentoCompra + "' and CabecCompras.IdDocOrigem='" + idCabecDoc + "' " + restricaoAbertoFechado + " order by numdoc desc")
            existeDocumentosCompraRelacionado = lista
        End If
    End Function


    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, serie As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        existeDocumentoCompra = ""
        Dim idProj As String
        If documentoCompra <> "" Then
            If bso.Comercial.Compras.Existe("000", documentoCompra, serie, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serie, "000", "Id")
                idProj = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serie, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        existeDocumentoCompra = ""
        Dim idProj As String
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            End If
            If bso.Comercial.Compras.Existe("000", documentoCompra, serieC_, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "Id")
                idProj = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="documentoCompra"></param>
    ''' <param name="numdoc"></param>
    ''' <returns></returns>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, ByVal numdoc As Integer) As String
        existeDocumentoCompra = ""
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            End If
            If bso.Comercial.Compras.Existe("000", documentoCompra, serieC_, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "Id")
            End If
        End If
    End Function

    Public Sub imprimirDocumento(ByVal idDocumento As String, ByVal modulo As String, ByVal owner As System.Windows.Forms.IWin32Window)
        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(idDocumento)
            imprimir(doc, owner)
            doc = Nothing
        End If
        If modulo = "V" Then
            Dim doc As GcpBEDocumentoVenda
            doc = bso.Comercial.Vendas.EditaID(idDocumento)
            imprimir(doc, owner)
            doc = Nothing
        End If
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoVenda, ByVal owner As System.Windows.Forms.IWin32Window)

        Dim se As GcpBESerie

        se = bso.Comercial.Series.Edita("V", doc.Tipodoc, doc.Serie)
        Dim numVia As String
        For i = 1 To se.NumVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            If se.Config <> "" Then
                Dim certificado As String
                certificado = bso.Comercial.Vendas.DevolveTextoAssinaturaDocID(doc.ID)
                imprimirMapa(se.Config, IIf(se.Previsao, "W", "P"), "{CabecDoc.Filial}='" + doc.Filial + "' AND {CabecDoc.Serie}='" + doc.Serie + "' AND {CabecDoc.TipoDoc}='" + doc.Tipodoc + "' AND {CabecDoc.Numdoc}=" + CStr(doc.NumDoc), numVia, doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado), owner)
            Else
                MsgBox("O Documento " + doc.Tipodoc + " não possui nenhum mapa configurado!", MsgBoxStyle.Exclamation, "Contactar Anphis")
            End If
        Next i
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoCompra, ByVal owner As System.Windows.Forms.IWin32Window)

        Dim se As GcpBESerie

        se = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
        Dim numVia As String
        For i = 1 To se.NumVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            If se.Config <> "" Then
                Dim certificado As String
                certificado = bso.Comercial.Compras.DevolveTextoAssinaturaDocID(doc.ID)
                imprimirMapa(buscarMapaDefault(doc.Tipodoc, se.Config), IIf(se.Previsao, "W", "P"), "{CabecCompras.Filial}='" + doc.Filial + "' AND {CabecCompras.Serie}='" + doc.Serie + "' AND {CabecCompras.TipoDoc}='" + doc.Tipodoc + "' AND {CabecCompras.Numdoc}=" + CStr(doc.NumDoc), numVia, doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado), owner)
            Else
                MsgBox("O Documento " + doc.Tipodoc + " não possui nenhum mapa configurado!", MsgBoxStyle.Exclamation, "Contactar Anphis")
            End If
        Next i
    End Sub

    Private Function buscarMapaDefault(tipodoc As String, mapaBase As String) As String
        Dim mapa As String
        mapa = ""
        Select Case tipodoc
            Case infModLM.TIPODOCLM : mapa = infModLM.MAPALM
            Case infModLM.TIPODOCCOT : mapa = infModLM.MAPACOT
            Case infModLM.TIPODOCECF : mapa = infModLM.MAPAECF
        End Select

        If mapa <> "" Then
            Return mapa
        Else
            Return mapaBase
        End If

    End Function
    Private Sub imprimirMapa(ByVal report As String, ByVal destiny As String, ByVal selectionFormula As String, ByVal numVia As String, moeda As String, textoCertificacao As String, ByVal owner As System.Windows.Forms.IWin32Window)
        Try

            Dim p As New System.Windows.Forms.PrintDialog
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, moeda, 2)
            '  Dim mapa As Mapa

            ' mapa = New Mapa()
            ' mapa.iniciarComponente(report, codEmpresa, objConfApl.Instancia, objConfApl.Utilizador, objConfApl.PwdUtilizador, PlataformaPrimavera)
            'buscar o nome da impressora predefinida
            'setDefaultPrinter(p.PrinterSettings.PrinterName)


            PlataformaPrimavera.Mapas.Inicializar("GCP")
            PlataformaPrimavera.Mapas.SelectionFormula = selectionFormula
            PlataformaPrimavera.Mapas.AddFormula("NumVia", numVia)
            PlataformaPrimavera.Mapas.AddFormula("DadosEmpresa", rp.getFormula)
            PlataformaPrimavera.Mapas.AddFormula("InicializaParametros", rp.getFormulaIniciarParametros(moeda, textoCertificacao))
            PlataformaPrimavera.Mapas.DefinicaoImpressoraEx2(p.PrinterSettings.PrinterName, "winspool", "winspool", "", CRPEOrientacaoFolha.ofPortrait, 1, CRPETipoFolha.tfA4, 23, 23, 1, p.PrinterSettings.Duplex, 1, 1, 0)
            PlataformaPrimavera.Mapas.ImprimeListagem(report, "Mapa", destiny, 1, "S", , , , , , True)
            '  PlataformaPrimavera.Mapas.ImprimeListagem(sReport:=report, sTitulo:="Mapa", sDestino:=destiny, iNumCopias:=1, sDocumento:="S", blnModal:=True)
            PlataformaPrimavera.Mapas.TerminaJanelas()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function setDefaultPrinter(ByVal strPrinterName As String) As Boolean
        Dim strOldPrinter As String

        Dim WshNetwork As Object

        Dim pd As New System.Drawing.Printing.PrintDocument

        Try

            strOldPrinter = pd.PrinterSettings.PrinterName

            WshNetwork = Microsoft.VisualBasic.CreateObject("WScript.Network")

            WshNetwork.SetDefaultPrinter(strPrinterName)

            pd.PrinterSettings.PrinterName = strPrinterName

            If pd.PrinterSettings.IsValid Then

                Return True

            Else

                WshNetwork.SetDefaultPrinter(strOldPrinter)

                Return False

            End If

        Catch exptd As Exception

            WshNetwork.SetDefaultPrinter(strOldPrinter)

            Return False

        Finally

            WshNetwork = Nothing

            pd = Nothing

        End Try

    End Function



    Public Function buscarDocumentoCompra(id As String) As GcpBEDocumentoCompra
        If bso.Comercial.Compras.ExisteID(id) Then
            Return bso.Comercial.Compras.EditaID(id)
        Else
            Return Nothing
        End If
    End Function

    Public Function buscarDocumentoVenda(id As String) As GcpBEDocumentoVenda
        If bso.Comercial.Vendas.ExisteID(id) Then
            Return bso.Comercial.Vendas.EditaID(id)
        Else
            Return Nothing
        End If
    End Function




    Public Function daProjectoCodigo(id As String) As String
        daProjectoCodigo = ""
        If bso.Comercial.Projectos.ExisteID(id) Then
            Return bso.Comercial.Projectos.DaValorAtributoID(id, "Codigo")
        End If
    End Function


    Public Function daProjectoNome(id As String) As String
        daProjectoNome = ""
        If bso.Comercial.Projectos.ExisteID(id) Then
            Return bso.Comercial.Projectos.DaValorAtributoID(id, "Descricao")
        End If
    End Function




    Private Function BuscarIdCopiaLinha(idOrigemCopia As String) As String
        Dim m As Motor
        Dim id As String
        If idOrigemCopia = "" Then Return ""
        m = Motor.GetInstance
        id = m.consultaValor("SELECT idLinhaOrigemCopia FROM LinhasCompras where id='" + idOrigemCopia + "'")
        Return id
    End Function



    Public Function VerificarEstadoDocumento(id As String, estadoAberto As Boolean) As Boolean
        Dim count As String
        If estadoAberto Then
            count = consultaValor("SELECT Count(*) FROM CabecComprasStatus WHERE IdCabecCompras='" + id + "' AND (Anulado=0)")
        Else
            count = consultaValor("SELECT Count(*) FROM CabecComprasStatus WHERE IdCabecCompras='" + id + "' AND (Anulado=1)")
        End If
        Return count <> "0"
    End Function

    Public Function artigoExiste(artigo As String) As Boolean
        Return bso.Comercial.Artigos.Existe(artigo)
    End Function




    Public Function buscarEntidadesLinha(idLinha As String) As String
        Return buscarEntidadeLinha(idLinha, "")
    End Function


    Private Function buscarEntidadeLinha(idLinha As String, ent As String) As String
        Dim dt As DataTable
        Dim dtECF As DataTable
        Dim lista As StdBELista
        Dim i As Integer
        Dim entidadeCOT As String = ""
        Dim entidadeECF As String = ""
        lista = consulta("Select  CabecCompras.Tipodoc, CabecCompras.Entidade,CabecCompras.Nome,LinhasCompras.Id  FROM CabecCompras INNER JOIN LinhasCompras on CabecCompras.id=LinhasCompras.IdCabecCompras WHERE IdLinhaOrigemCopia ='" + idLinha + "'")

        If lista Is Nothing Then Return ""

        dt = convertRecordSetToDataTable(lista.DataSet)
        If dt.Rows.Count > 0 Then
            ent = ""
            For i = 0 To dt.Rows.Count - 1


                If dt.Rows(i)("Tipodoc") = infModLM.TIPODOCECF Then
                    entidadeECF = entidadeECF + dt.Rows(i)("Nome") + " ; "
                Else
                    entidadeCOT = entidadeCOT + dt.Rows(i)("Nome") + " ; "
                End If
                idLinha = dt.Rows(i)("Id").ToString
                lista = consulta("Select  CabecCompras.Tipodoc, CabecCompras.Entidade,CabecCompras.Nome,LinhasCompras.Id  FROM CabecCompras INNER JOIN LinhasCompras on CabecCompras.id=LinhasCompras.IdCabecCompras WHERE IdLinhaOrigemCopia ='" + idLinha + "'")

                If Not lista Is Nothing Then
                    dtECF = convertRecordSetToDataTable(lista.DataSet)
                    If dtECF.Rows.Count > 0 Then
                        If dtECF.Rows(0)("Tipodoc") = infModLM.TIPODOCECF Then
                            entidadeECF = entidadeECF + dtECF.Rows(0)("Nome") + " ; "
                        Else
                            entidadeCOT = entidadeCOT + dtECF.Rows(0)("Nome") + " ; "
                        End If
                    End If
                End If

            Next
        End If

        If entidadeECF <> "" Then
            ent = entidadeECF
        Else
            ent = entidadeCOT
        End If

        If ent <> "" Then
            ent = Mid(ent, 1, Len(ent) - 2)
        End If

        Return ent

    End Function



    Public Function daNumeracaoDocumentoCompra(ByVal documentoCompra As String) As Integer
        Dim serie As String

        daNumeracaoDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            Return bso.Comercial.Series.ProximoNumero("C", documentoCompra, serie)
        End If
    End Function

    Public Function daValorNumeracaoMinimaDocumentoCompra(ByVal documentoCompra As String) As Integer
        Dim serie As String

        daValorNumeracaoMinimaDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            Return bso.Comercial.Series.DaValorAtributo("C", documentoCompra, serie, "LimiteInferior")
        End If
    End Function

    Public Function daValorNumeracaoMaximaDocumentoCompra(ByVal documentoCompra As String) As Integer
        Dim serie As String
        daValorNumeracaoMaximaDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serie = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            Else
                serie = serieC_
            End If
            Return bso.Comercial.Series.DaValorAtributo("C", documentoCompra, serie, "LimiteSuperior")
        End If
    End Function


    Public Function BuscarUltimosDadosArtigo(artigo As String, idLinha As String) As StdBELista

        Try
            If Trim(idLinha) = "" Then
                Return consulta("SELECT FORNECEDORES.Nome as UltimoFornecedor,PCUltimo,StkActual, 0 as QuantidadeEnt FROM ARTIGO LEFT OUTER JOIN FORNECEDORES ON ARTIGO.UltimoFornecedor=FORNECEDORES.Fornecedor  WHERE Artigo='" + artigo + "'")
            Else
                Return consulta("SELECT FORNECEDORES.Nome as UltimoFornecedor,PCUltimo,StkActual, (SELECT isnull(SUM(Quantidade),0) FROM LinhasStk WHERE IdLinhaOrigemCopia='" + idLinha + "') as QuantidadeEnt FROM ARTIGO LEFT OUTER JOIN FORNECEDORES ON ARTIGO.UltimoFornecedor=FORNECEDORES.Fornecedor  WHERE Artigo='" + artigo + "'")
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    ''' <summary>
    ''' Função que permite actualizar
    ''' </summary>
    ''' <param name="doc"></param>
    Private Sub actualizarValoresPCUForn(doc As GcpBEDocumentoCompra)


        If doc.Tipodoc <> infModLM.TIPODOCECF Then Exit Sub

        Dim artigo As String
        Dim precunit As Double
        Dim unidade As String
        Dim desconto As Double

        For i = 1 To doc.Linhas.NumItens
            artigo = doc.Linhas(i).Artigo
            precunit = doc.Linhas(i).PrecUnit

            If precunit < 0 Then precunit = precunit * -1

            unidade = doc.Linhas(i).Unidade
            desconto = doc.Linhas(i).Desconto1

            If bso.Comercial.Artigos.Existe(artigo) Then
                If precunit <> 0 Then
                    bso.Comercial.Artigos.ActualizaPCUltimo(artigo, precunit, unidade, desconto, "000")
                    bso.Comercial.Artigos.ActualizaValorAtributo(artigo, "UltimoFornecedor", doc.Entidade)
                    bso.Comercial.Artigos.ActualizaValorAtributo(artigo, "UltimoNumDoc", doc.NumDoc)
                    bso.Comercial.Artigos.ActualizaValorAtributo(artigo, "UltimoTipoDoc", doc.Tipodoc)
                    bso.Comercial.Artigos.ActualizaValorAtributo(artigo, "UltimaSerieDoc", doc.Serie)
                End If
                'bso.Comercial.Artigos.ActualizaUltFornecedor(artigo, doc.Tipodoc, doc.Serie, doc.NumDoc, doc.Entidade)

            End If
        Next
    End Sub


    Public Function daListaRegistos(ByVal tipodoc As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = consulta("Select C.ID, C.Tipodoc, C.Serie, C.NumDoc,C.DataDoc,L.Artigo,L.Descricao,L.Quantidade, L.CDU_ObraN5 as Estado, A.StkActual, A.PCUltimo, A.UltimoFornecedor FROM CabecCompras C INNER JOIN LinhasCompras L ON C.Id=L.IdCabecCompras INNER JOIN ARTIGO A ON L.Artigo=A.Artigo WHERE  C.TipoDoc='" + tipodoc + "' AND C.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND C.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) order by C.DataDoc, C.Tipodoc,C.NumDoc,L.NumLinha")
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="modulo"></param>
    ''' <param name="id"></param>
    ''' <param name="estado"></param>
    ''' <param name="tudo"></param>
    Public Sub actualizarEstadoLinha(modulo As String, id As String, estado As String, Optional tudo As Boolean = True)

        If id = "" Then Exit Sub
        If modulo = "C" Then
            executarComando("UPDATE LINHASCOMPRAS SET CDU_ObraN5='" + estado + "' WHERE id='" + id + "'")
            If tudo = False Then
                Exit Sub
            Else
                Dim lista As StdBELista
                lista = bso.Consulta("SELECT IdLinhaOrigemCopia  from LINHASCOMPRAS WHERE  id='" + id + "' and ModuloOrigemCopia='" + modulo + "' and id<>IdLinhaOrigemCopia")
                While Not lista.NoFim
                    If Not IsDBNull(lista.Valor("IdLinhaOrigemCopia")) Then
                        actualizarEstadoLinha(modulo, lista.Valor("IdLinhaOrigemCopia"), estado, tudo)
                    End If
                    lista.Seguinte()
                End While
            End If
        End If

    End Sub


    Private Sub actualizarEntradasStocks(doc As GcpBEDocumentoCompra, recordsGrid As Object)


        If doc.Tipodoc <> infModLM.TIPODOCECF Then Exit Sub

        Dim row As ReportRecord
        Dim dtrow As DataRow
        Dim artigo As String
        Dim descricao As String
        Dim quantidade As Double
        Dim precunit As Double
        Dim unidade As String
        Dim linha As GcpBELinhaDocumentoStock
        Dim movStock As String

        Dim docStk As GcpBEDocumentoStock
        docStk = buscarDocumentoStock(doc.ID)

        If docStk Is Nothing Then
            docStk = New GcpBEDocumentoStock
            docStk.Tipodoc = infModLM.TIPODOCSTK
            docStk.Serie = doc.Serie
            docStk.DataDoc = doc.DataDoc
            bso.Comercial.Stocks.PreencheDadosRelacionados(docStk, PreencheRelacaoStk.stkDadosTodos)
        End If

        '   docStk.Linhas.RemoveTodos()

        For i = 0 To recordsGrid.Count - 1
            row = recordsGrid(i)
            dtrow = row.Tag
            artigo = buscarArtigo(row(ENCCOLUMN_ARTIGO).Value, infModLM.ARTIGO)
            descricao = row(COLUMN_DESCRICAO).Value
            quantidade = row(6).Value
            precunit = IIf(IsNumeric(row(ENCCOLUMN_FORNPRECO).Value), row(ENCCOLUMN_FORNPRECO).Value, 0)
            unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
            movStock = bso.Comercial.Artigos.DaValorAtributo(artigo, "MovStock")
            If row(9).Checked And UCase(movStock) = "S" And quantidade <> 0 Then
                bso.Comercial.Stocks.AdicionaLinha(docStk, artigo,, quantidade,, precunit)
                linha = docStk.Linhas(docStk.Linhas.NumItens)
                linha.IdLinhaOrigemCopia = dtrow("ID").ToString
                linha.ModuloOrigemCopia = "C"
                linha.DataStock = row(8).Value
            End If
        Next

        docStk.CamposUtil("CDU_IDDocOrigem") = doc.ID
        If docStk.Linhas.NumItens > 0 Then
            bso.Comercial.Stocks.Actualiza(docStk)
        End If



    End Sub

    ''' <summary>
    ''' Função que permite ir 
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Private Function buscarDocumentoStock(id As String) As GcpBEDocumentoStock
        Dim lista As StdBELista

        lista = consulta("SELECT * FROM CabecStk WHERE CDU_IDDocOrigem='" + id + "'")

        If Not lista.NoFim Then
            If lista.NumLinhas > 0 Then
                Return bso.Comercial.Stocks.Edita(lista.Valor("Filial"), "S", lista.Valor("TipoDoc"), lista.Valor("Serie"), lista.Valor("NumDoc"))
            End If
        End If
        Return Nothing
    End Function


    Public Function inicializarAplicacao() As Boolean
        Dim msg As String
        msg = ""

        If Not existeTipoDocumentoCompra(infModLM.TIPODOCLM) Then
            msg = "O Documento de Compra da Lista de Materais '" + infModLM.TIPODOCLM + "' não existe!"
        End If

        If Not existeTipoDocumentoCompra(infModLM.TIPODOCCOT) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Documento de Compra do Pedido de cotação '" + infModLM.TIPODOCCOT + "' não existe!"
        End If

        If Not existeTipoDocumentoCompra(infModLM.TIPODOCECF) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Documento de Compra de encomenda '" + infModLM.TIPODOCECF + "' não existe!"
        End If

        If msg <> "" Then
            MsgBox(msg, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erro")
            Return False
        Else
            Return True
        End If

    End Function

    Private Function existeTipoDocumentoCompra(documento As String) As Boolean
        Return bso.Comercial.TabCompras.Existe(documento)
    End Function

    ''' <summary>
    ''' Função que permite a actualização dos movimentos de stocks
    ''' </summary>
    ''' <param name="Records"></param>
    Public Sub gravarDocumentoStocks(Records As ReportRecords)

        Dim doc As GcpBEDocumentoStock
        Dim record As ReportRecord
        Dim i As Integer
        Dim id As String
        Dim lista As StdBELista
        Dim linha As GcpBELinhaDocumentoStock
        Try

            For Each record In Records
                id = record.Tag
                id = id.ToUpper
                lista = consulta("SELECT Modulo, FIlial,Tipodoc,Serie,Numdoc,IdCabecOrig FROM LinhasStk WHERE id='" + id + "'")
                If Not lista.NoFim Then
                    If bso.Comercial.Stocks.ExisteID(lista.Valor("IdCabecOrig")) Then
                        doc = bso.Comercial.Stocks.Edita(lista.Valor("Filial"), lista.Valor("Modulo"), lista.Valor("TipoDoc"), lista.Valor("Serie"), lista.Valor("Numdoc"))

                        For Each linha In doc.Linhas
                            If linha.IdLinha.ToUpper = id Then
                                linha.Quantidade = record(2).Value
                                linha.DataStock = record(3).Value
                                Exit For
                            End If
                        Next
                        bso.Comercial.Stocks.Actualiza(doc)
                    End If
                End If
            Next
        Catch ex As Exception
            MsgBox("Erro na actualização dos movimentos de stocks --> gravarDocumentoStocks", MsgBoxStyle.Critical)
        End Try
    End Sub
End Class
