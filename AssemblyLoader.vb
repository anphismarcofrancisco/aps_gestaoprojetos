﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Reflection
Imports System.IO


Namespace Primavera.Integration2
    Public NotInheritable Class AssemblyLoader
        Private Sub New()
        End Sub
        Public Shared Function AssemblyResolver800(sender As Object, args As ResolveEventArgs) As System.Reflection.Assembly
            Dim outAssembly As Assembly = Nothing
            Dim strTempAssemblyPath As String = String.Empty


            Dim commonProgramFilesPath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFilesX86)

            If commonProgramFilesPath.Length = 0 Then
                commonProgramFilesPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles)
            End If

            Dim objExeAssembly As Assembly = Assembly.GetExecutingAssembly()
            Dim arrRefAssemblyNames As AssemblyName() = objExeAssembly.GetReferencedAssemblies()

            Dim strArgToLoad As String = args.Name.Substring(0, args.Name.IndexOf(","))

            For Each strAName As System.Reflection.AssemblyName In arrRefAssemblyNames
                If strAName.FullName.Substring(0, strAName.FullName.IndexOf(",")).Equals(strArgToLoad) Then
                    strTempAssemblyPath = Path.Combine(Path.Combine(commonProgramFilesPath, "PRIMAVERA\SG800"), strArgToLoad & Convert.ToString(".dll"))
                    Exit For
                End If
            Next

            ' Valida nome do assembly
            If String.IsNullOrEmpty(strTempAssemblyPath) Then
                outAssembly = Nothing
            Else
                outAssembly = Assembly.LoadFrom(strTempAssemblyPath)
            End If

            Return outAssembly
        End Function


        Public Shared Function AssemblyResolver900(sender As Object, args As ResolveEventArgs) As System.Reflection.Assembly
            Dim outAssembly As Assembly = Nothing
            Dim strTempAssemblyPath As String = String.Empty


            Dim commonProgramFilesPath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFilesX86)

            If commonProgramFilesPath.Length = 0 Then
                commonProgramFilesPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles)
            End If

            Dim objExeAssembly As Assembly = Assembly.GetExecutingAssembly()
            Dim arrRefAssemblyNames As AssemblyName() = objExeAssembly.GetReferencedAssemblies()

            Dim strArgToLoad As String = args.Name.Substring(0, args.Name.IndexOf(","))

            For Each strAName As System.Reflection.AssemblyName In arrRefAssemblyNames
                If strAName.FullName.Substring(0, strAName.FullName.IndexOf(",")).Equals(strArgToLoad) Then
                    strTempAssemblyPath = Path.Combine(Path.Combine(commonProgramFilesPath, "PRIMAVERA\SG900"), strArgToLoad & Convert.ToString(".dll"))
                    Exit For
                End If
            Next

            ' Valida nome do assembly
            If String.IsNullOrEmpty(strTempAssemblyPath) Then
                outAssembly = Nothing
            Else
                outAssembly = Assembly.LoadFrom(strTempAssemblyPath)
            End If

            Return outAssembly
        End Function
    End Class

End Namespace