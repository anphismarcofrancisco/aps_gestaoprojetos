﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports Interop.StdBE900

Public Class FrmQtFormula

    Const MSGERRO1 As String = "Valores Inválidos"
    Const MSGPERGUNTA1 As String = "Deseja realmente remover o registo?"
    Dim limpar As Boolean
    Dim txtActivo As TextBox

    Dim quantidade_ As Double = 0
    Property QuantidadeFormula() As Double
        Get
            If IsNumeric(txtQuant.Text) Then
                QuantidadeFormula = CDbl(txtQuant.Text)
            Else
                QuantidadeFormula = 1
            End If
        End Get
        Set(ByVal value As Double)
            quantidade_ = value
        End Set
    End Property

    Dim formula_ As String
    Property Formula() As String
        Get
            Formula = formula_
        End Get
        Set(ByVal value As String)
            formula_ = value
        End Set
    End Property


    Dim artigo_ As String
    Property Artigo() As String
        Get
            Artigo = artigo_
        End Get
        Set(ByVal value As String)
            artigo_ = value
        End Set
    End Property

    Dim descricao_ As String
    Property Descricao() As String
        Get
            Descricao = descricao_
        End Get
        Set(ByVal value As String)
            descricao_ = value
        End Set
    End Property


    Dim varA_ As Double = 0
    Property VarA() As Double
        Get
            If IsNumeric(txtQtA.Text) Then
                VarA = CDbl(txtQtA.Text)
            Else
                VarA = 0
            End If
        End Get
        Set(ByVal value As Double)
            varA_ = value
        End Set
    End Property

    Dim varB_ As Double = 0
    Property VarB() As Double
        Get
            If IsNumeric(txtQtB.Text) Then
                VarB = CDbl(txtQtB.Text)
            Else
                VarB = 0
            End If
        End Get
        Set(ByVal value As Double)
            varB_ = value
        End Set
    End Property

    Dim varC_ As Double = 0
    Property VarC() As Double
        Get
            If IsNumeric(txtQtC.Text) Then
                VarC = CDbl(txtQtC.Text)
            Else
                VarC = 0
            End If
        End Get
        Set(ByVal value As Double)
            varC_ = value
        End Set
    End Property

    Dim numero_ As String = ""
    Property Numero() As String
        Get
            numero_ = txtNumero.Text
            Numero = numero_
        End Get
        Set(ByVal value As String)
            numero_ = value
        End Set
    End Property


    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Private Sub FrmTerminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        botaoSeleccionado_ = 3
        limpar = True
        iniciarForm()
        txtQtA.Text = varA_
        txtQtB.Text = varB_
        txtQtC.Text = varC_
        txtQuant.Text = quantidade_
        txtNumero.Text = numero_
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Confirmar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Confirmar"

            Control = .Add(xtpControlButton, 2, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    Private Sub iniciarForm()
        iniciarToolBox()
        txtQuant.Text = 1
       
        txtQtA.Focus()
        txtActivo = txtQtA

        'txtQtA.Enabled = False
        'txtQtB.Enabled = False
        'txtQtC.Enabled = False

        Dim m As Motor
        Dim formu As Interop.GcpBE900.GcpBEFormula
        m = Motor.GetInstance

        formu = m.daFormula(formula_)

        lblQtA.Text = formu.DescricaoA
        lblQtB.Text = formu.DescricaoB
        lblQtC.Text = formu.DescricaoC

        txtQtA.Enabled = formu.NumVariaveis >= 1
        txtQtB.Enabled = formu.NumVariaveis >= 2
        txtQtC.Enabled = formu.NumVariaveis >= 3

        lblFormula.Text = formu.TextoFormula
        lblArtigo1.Text = descricao_
        GroupBox1.Text = artigo_ + " - " + formula_

        If Not txtQtA.Enabled Then txtQtA.Text = ""
        If Not txtQtB.Enabled Then txtQtB.Text = ""
        If Not txtQtC.Enabled Then txtQtC.Text = ""



    End Sub


    Private Sub colocarQuantidadeStk(ByVal valor As String)
        Dim txt As TextBox
        txt = txtActivo
        If limpar Then
            txt.Text = ""
            limpar = False
        End If
        If valor = "" Then
            txt.Text = ""
        Else
            If IsNumeric(txt.Text + valor) Then txt.Text = txt.Text + valor
        End If
    End Sub

    Private Function TextBoxActiva() As TextBox

        If txtNumero.Focused Then
            Return txtNumero
        End If

        If txtQtA.Focused Then
            Return txtQtA
        End If

        If txtQtB.Focused Then
            Return txtQtB
        End If

        If txtQtC.Focused Then
            Return txtQtC
        End If

        If txtQuant.Focused Then
            Return txtQuant
        End If

    End Function

    Private Sub habilitarBotoes(ByVal b As Boolean)
        btn1.Enabled = b
        btn2.Enabled = b
        btn3.Enabled = b
        btn4.Enabled = b
        If b = True Then btn5.Enabled = b
        btn6.Enabled = b
        btn7.Enabled = b
        btn8.Enabled = b
        btn9.Enabled = b
        btn0.Enabled = b
        btnP.Enabled = b
    End Sub

    Private Sub btnP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnP.Click
        '  habilitarBotoes(False)
        colocarQuantidadeStk(",")
    End Sub

    Private Sub btnC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnC.Click
        colocarQuantidadeStk("")
        ' habilitarBotoes(True)
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        colocarQuantidadeStk(btn1.Tag)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        colocarQuantidadeStk(btn2.Tag)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        colocarQuantidadeStk(btn3.Tag)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        colocarQuantidadeStk(btn4.Tag)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        colocarQuantidadeStk(btn5.Tag)
        'If btn0.Enabled = False Then
        '    btn5.Enabled = False
        'End If
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        colocarQuantidadeStk(btn6.Tag)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        colocarQuantidadeStk(btn7.Tag)
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        colocarQuantidadeStk(btn8.Tag)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        colocarQuantidadeStk(btn9.Tag)
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        colocarQuantidadeStk(btn0.Tag)
    End Sub


    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        botaoSeleccionado_ = e.control.Index

        ' terminar o registo
        If e.control.Index = 1 Then
            If Not IsNumeric(txtQtA.Text) Then
                MsgBox(MSGERRO1, MsgBoxStyle.Critical)
                Exit Sub
            End If

            If Not IsNumeric(txtQtB.Text) Then
                MsgBox(MSGERRO1, MsgBoxStyle.Critical)
                Exit Sub
            End If

            If Not IsNumeric(txtQtC.Text) Then
                MsgBox(MSGERRO1, MsgBoxStyle.Critical)
                Exit Sub
            End If

            If IsNumeric(txtQuant.Text) Then
                Me.Hide()
            Else
                MsgBox(MSGERRO1, MsgBoxStyle.Critical)
            End If

        End If


        'fechar o serviço
        If e.control.Index = 2 Then
            Me.Close()
        End If
    End Sub

    Private Sub txtQtA_Enter(sender As Object, e As EventArgs) Handles txtQtA.Enter
        txtActivo = txtQtA
        txtQtA.SelectionStart = 1
        txtQtA.SelectAll()
        txtQtA.SelectionLength = txtQtA.TextLength
        limpar = True
    End Sub

    Private Sub txtQtB_Enter(sender As Object, e As EventArgs) Handles txtQtB.Enter
        txtActivo = txtQtB
        txtQtB.SelectAll()
        limpar = True
    End Sub

    Private Sub txtQtC_Enter(sender As Object, e As EventArgs) Handles txtQtC.Enter
        txtActivo = txtQtC
        txtQtC.SelectAll()
        limpar = True
    End Sub

    Private Sub txtQuant_Enter(sender As Object, e As EventArgs) Handles txtQuant.Enter
        txtActivo = txtQuant
        txtQuant.SelectAll()
        limpar = True
    End Sub

    ReadOnly Property Quantidade() As Double
        Get
            Dim formul As String
            formul = lblFormula.Text
            formul = Replace(formul, "A", txtQtA.Text)
            formul = Replace(formul, "B", txtQtB.Text)
            formul = Replace(formul, "C", txtQtC.Text)
            formul = Evaluer(formul)
            If IsNumeric(txtQuant.Text) Then
                Quantidade = CDbl(formul) * CDbl(txtQuant.Text)
            Else
                Quantidade = CDbl(formul)
            End If
        End Get
    End Property
End Class