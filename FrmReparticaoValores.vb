﻿Imports AxXtremeReportControl
Imports Interop.StdBE900
Imports XtremeReportControl
Imports XtremeReportControl.XTPReportFixedRowsDividerStyle
Imports XtremeReportControl.XTPColumnAlignment
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType

Public Class FrmReparticaoValores

    Dim clicouOK As Boolean
    Const ID_CONFIRMAR As Integer = 1
    Const ID_IMPRIMIR As Integer = 2
    Const ID_CANCELAR As Integer = 3
    Dim valor As Double
    Private Sub FrmReparticaoValores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        clicouOK = False
        criarToolBox()
        txtValor.Focus()
    End Sub

    Private Sub criarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim control As CommandBarControl

        CommandBarsFrame1.Icons = ImageManager.Icons

        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop) '
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        ' CommandBarsFrame1.Options.LargeIcons = True
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            control = .Add(xtpControlButton, ID_CONFIRMAR, " Confirmar", -1, False)
            control.Style = XTPButtonStyle.xtpButtonIconAndCaptionBelow
            control.DescriptionText = "Confirmar"
            control = .Add(xtpControlButton, ID_CANCELAR, " Cancelar", -1, False)
            control.BeginGroup = True
            control.DescriptionText = "Cancelar"
            control.Style = XTPButtonStyle.xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub CommandBarsFrame1_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case ID_CONFIRMAR : confirmar()
            Case ID_IMPRIMIR : imprimir()
            Case ID_CANCELAR : Me.Close()
        End Select
    End Sub

    Public Function seleccionouOK() As Boolean
        seleccionouOK = clicouOK
    End Function
    Private Sub imprimir()

    End Sub

    Private Sub confirmar()
        If MsgBox("Deseja realmente repartir os valores pelo documento?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

            If IsNumeric(txtValor.Text) Then
                valor = CDbl(txtValor.Text)
            Else
                valor = 0
            End If

            clicouOK = True
            Me.Hide()
        End If
    End Sub

    Public Function getValor() As Double
        Return valor
    End Function

    Private Sub txtValor_KeyPressEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyPressEvent) Handles txtValor.KeyPressEvent

    End Sub

    Private Sub txtValor_KeyUpEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyUpEvent) Handles txtValor.KeyUpEvent
        If e.keyCode = Keys.Enter Then
            confirmar()
        End If
    End Sub
End Class