﻿Public Class Entidade

    Public Sub New()

    End Sub

    ''' <summary>
    ''' Tipo Entidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim tipoentidade_ As String
    Property TipoEntidade() As String
        Get
            TipoEntidade = tipoentidade_
        End Get
        Set(ByVal value As String)
            tipoentidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' Entidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim entidade_ As String
    Property Entidade() As String
        Get
            Entidade = entidade_
        End Get
        Set(ByVal value As String)
            entidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' Nome Fornecedor
    ''' </summary>
    ''' <remarks></remarks>
    Dim nome_ As String
    Property Nome As String
        Get
            Nome = nome_
        End Get
        Set(ByVal value As String)
            nome_ = value
        End Set
    End Property

    ''' <summary>
    ''' Contribuinte
    ''' </summary>
    ''' <remarks></remarks>
    Dim contribuinte_ As String
    Property Contribuinte As String
        Get
            Contribuinte = contribuinte_
        End Get
        Set(ByVal value As String)
            contribuinte_ = value
        End Set
    End Property

    ''' <summary>
    ''' Morada
    ''' </summary>
    ''' <remarks></remarks>
    Dim morada_ As String
    Property Morada As String
        Get
            Morada = morada_
        End Get
        Set(ByVal value As String)
            morada_ = value
        End Set
    End Property

    ''' <summary>
    ''' Morada 2
    ''' </summary>
    ''' <remarks></remarks>
    Dim morada2_ As String
    Property Morada2 As String
        Get
            Morada2 = morada2_
        End Get
        Set(ByVal value As String)
            morada2_ = value
        End Set
    End Property

    ''' <summary>
    ''' Localidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim localidade_ As String
    Property Localidade As String
        Get
            Localidade = localidade_
        End Get
        Set(ByVal value As String)
            localidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal
    ''' </summary>
    ''' <remarks></remarks>
    Dim codPostal_ As String
    Property CodPostal As String
        Get
            CodPostal = codPostal_
        End Get
        Set(ByVal value As String)
            codPostal_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal Localidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim CodPostalLocalidade_ As String
    Property CodPostalLocalidade As String
        Get
            CodPostalLocalidade = CodPostalLocalidade_
        End Get
        Set(ByVal value As String)
            CodPostalLocalidade_ = value
        End Set
    End Property


    'CARGA

    ''' <summary>
    ''' Morada Carga
    ''' </summary>
    ''' <remarks></remarks>
    Dim moradaCarga_ As String
    Property MoradaCarga As String
        Get
            MoradaCarga = moradaCarga_
        End Get
        Set(ByVal value As String)
            moradaCarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' Morada 2
    ''' </summary>
    ''' <remarks></remarks>
    Dim morada2Carga_ As String
    Property Morada2Carga As String
        Get
            Morada2Carga = morada2Carga_
        End Get
        Set(ByVal value As String)
            morada2Carga_ = value
        End Set
    End Property

    ''' <summary>
    ''' Localidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim localidadeCarga_ As String
    Property LocalidadeCarga As String
        Get
            LocalidadeCarga = localidadeCarga_
        End Get
        Set(ByVal value As String)
            localidadeCarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal
    ''' </summary>
    ''' <remarks></remarks>
    Dim codPostalCarga_ As String
    Property CodPostalCarga As String
        Get
            CodPostalCarga = codPostalCarga_
        End Get
        Set(ByVal value As String)
            codPostalCarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal Localidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim CodPostalLocalidadeCarga_ As String
    Property CodPostalLocalidadeCarga As String
        Get
            CodPostalLocalidadeCarga = CodPostalLocalidadeCarga_
        End Get
        Set(ByVal value As String)
            CodPostalLocalidadeCarga_ = value
        End Set
    End Property

    'DESCARGA
    ''' <summary>
    ''' Morada Descarga
    ''' </summary>
    ''' <remarks></remarks>
    Dim moradaDescarga_ As String
    Property MoradaDescarga As String
        Get
            MoradaDescarga = moradaDescarga_
        End Get
        Set(ByVal value As String)
            moradaDescarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' Morada 2 Descarga
    ''' </summary>
    ''' <remarks></remarks>
    Dim morada2Descarga_ As String
    Property Morada2Descarga As String
        Get
            Morada2Descarga = morada2Descarga_
        End Get
        Set(ByVal value As String)
            morada2Descarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' Localidade Descarga
    ''' </summary>
    ''' <remarks></remarks>
    Dim localidadeDescarga_ As String
    Property LocalidadeDescarga As String
        Get
            LocalidadeDescarga = localidadeDescarga_
        End Get
        Set(ByVal value As String)
            localidadeDescarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal Descarga
    ''' </summary>
    ''' <remarks></remarks>
    Dim codPostalDescarga_ As String
    Property CodPostalDescarga As String
        Get
            CodPostalDescarga = codPostalDescarga_
        End Get
        Set(ByVal value As String)
            codPostalDescarga_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal Localidade Descarga
    ''' </summary>
    ''' <remarks></remarks>
    Dim CodPostalLocalidadeDescarga_ As String
    Property CodPostalLocalidadeDescarga As String
        Get
            CodPostalLocalidadeDescarga = CodPostalLocalidadeDescarga_
        End Get
        Set(ByVal value As String)
            CodPostalLocalidadeDescarga_ = value
        End Set
    End Property
End Class
