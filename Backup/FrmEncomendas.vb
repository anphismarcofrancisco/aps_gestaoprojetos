﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports StdBE800
Imports PlatAPSNET

Public Class FrmEncomendas

    Const COLUMN_ARTIGO As Integer = 0
    ' Const COLUMN_ARMAZEM As Integer = 2
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTSTOCK As Integer = 2
    Const COLUMN_QUANTIDADE As Integer = 3

    Private Sub FrmEncomendas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fileINI As String
        Dim ficheiroConfiguracao As Configuracao
        fileINI = Application.StartupPath + "\\CONFIG.ini"
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
        Dim m As Motor
        m = Motor.GetInstance

        lblText.Text = m.EmpresaDescricao
        Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario

        gridArtigosStock.Tag = ""

        iniciarToolBox()
        inicializarMoldes()

        inicializarDocumentos()

        gridArtigos.AutoColumnSizing = False
        gridArtigos.AutoColumnSizing = False
        '  gridArtigos.ShowGroupBox = True
        ' gridArtigos.ShowItemsInGroups = False

        gridArtigosStock.AutoColumnSizing = False
        gridArtigosStock.AutoColumnSizing = False
        ' gridArtigosStock.ShowGroupBox = True
        ' gridArtigosStock.ShowItemsInGroups = False

        inserirColunasRegistos(1, gridArtigos)
        inserirColunasRegistos(2, gridArtigosStock)

        buscarArtigos(gridArtigos, txtArtigo.Text)

        txtNumDoc.Minimum = m.daValorNumeracaoMinimaDocumentoCompra(lstDocumentos.SelectedValue)
        txtNumDoc.Maximum = m.daValorNumeracaoMaximaDocumentoCompra(lstDocumentos.SelectedValue)
        txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)

        If txtNumDoc.Value = -1 Then
            MsgBox("Documento de Encomenda não configurado", MsgBoxStyle.Critical)
            Me.Close()
        End If
    End Sub

    Private Sub inicializarMoldes()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstMoldes.SelectedIndex = -1
        lista = m.daListaMoldes(txtPesqMolde.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstMoldes.DataSource = dt
        lstMoldes.ValueMember = "Codigo"
        lstMoldes.DisplayMember = "Nome"
    End Sub


    Private Sub inicializarDocumentos()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstDocumentos.SelectedIndex = -1
        lista = m.daListaDocumentos()
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstDocumentos.ValueMember = "Codigo"
        lstDocumentos.DisplayMember = "Nome"
        lstDocumentos.DataSource = dt
        lstDocumentos.SelectedValue = m.DocumentoCompra
        m.AplicacaoInicializada = True
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Novo", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Novo"

            Control = .Add(xtpControlButton, 2, " Gravar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Gravar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 6, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True


            Control = .Add(xtpControlButton, 3, " Enviar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Enviar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 4, " Registos", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Registos"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 5, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub inserirColunasRegistos(ByVal indexGrid As Integer, ByVal grid As AxXtremeReportControl.AxReportControl)

        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", 150, False, True)
        '  Column = inserirColuna(grid, COLUMN_ARMAZEM, "Armazém", 100, False, True)
        Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", 200, False, True)
        Column = inserirColuna(grid, COLUMN_QUANTSTOCK, "Quant Stk", 80, False, True)
        Column.Alignment = xtpAlignmentRight
        If indexGrid = 2 Then
            Column = inserirColuna(grid, COLUMN_QUANTIDADE, "Quant.", 80, False, True)
            Column.Alignment = xtpAlignmentRight
        End If


    End Sub

    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Sub txtPesqMolde_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqMolde.Change
        inicializarMoldes()
    End Sub

    Private Sub buscarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim listaExclusao As String
        Dim m As Motor

        'If documento <> "" Then
        '    lblEmModoEdicao.Text = "Documento " + documento + " em modo de edição"
        '    lblEmModoEdicao.Visible = True
        'Else
        '    lblEmModoEdicao.Visible = False
        'End If
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        grid.Records.DeleteAll()
        listaExclusao = "" 'daListaArtigosExcluidos()
        dtRegistos = m.daListaArtigos("APS_GP_Artigos", pesquisa, "", "")
        inserirLinhasRegistos(dtRegistos, grid)
        grid.Populate()
    End Sub

    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable, ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow, grid)
        Next
    End Sub

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional ByVal valorStk As Double = -1)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem

        record = grid.Records.Add()


        '  record.PreviewText = "asd"
        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo"))

        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = 1
        Item = record.AddItem(dtRow("StkActual"))

        If grid.Name = "gridArtigosStock" Then
            If valorStk < 0 Then valorStk = valorStk * -1
            Item = record.AddItem(valorStk)
        End If

    End Sub

    Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

        buscarArtigos(gridArtigos, txtArtigo.Text)

    End Sub

    Private Sub btnDPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDPA.Click
        enviarArtigoParaSaida()
    End Sub

    Private Sub enviarArtigoParaSaida()
        Dim dtrow As DataRow
        Dim m As Motor
        m = Motor.GetInstance
        If gridArtigos.SelectedRows.Count > 0 Then
            Dim i As Integer
            Dim index As Integer
            i = 0
            If Not m.artigoValido(gridArtigos.SelectedRows(i).Record.Item(COLUMN_ARTIGO).Value, True) Then
                MsgBox("O Artigo selecionado não é está configurado para movimentar Stock.", MsgBoxStyle.Information, "Anphis")
                Exit Sub
            End If
            '  For i = 0 To gridArtigos.SelectedRows.Count - 1
            index = gridArtigos.SelectedRows(i).Record.Index
            dtrow = gridArtigos.SelectedRows(i).Record.Tag
            Dim f As FrmQt
            f = New FrmQt
            f.Valor = 1
            f.ShowDialog()
            If f.BotaoSeleccionado = 1 Then
                inserirLinhaRegisto(dtrow, gridArtigosStock, f.Valor)

                ' gridArtigos.SelectedRows(i).Record.Visible = False
                ' gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Tag = 0
                ' Next i
                gridArtigosStock.Populate()
                gridArtigos.Populate()
            End If
            ' Me.Show()
            Me.Activate()
        End If
    End Sub

    Private Sub btnEPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPA.Click
        retirarArtigoSaida()
    End Sub

    Private Sub retirarArtigoSaida()
        If gridArtigosStock.SelectedRows.Count > 0 Then
            Dim index As Integer
            '  Dim index1 As Integer
            Dim record As ReportRecord
            Dim i As Integer
            i = 0
            ' For i = 0 To gridArtigosStock.SelectedRows.Count - 1
            'ir buscar o index dos artigos a movimentar stock
            index = gridArtigosStock.SelectedRows(i).Record.Index
            'ir buscar o index da  grelha dos artigos 
            'buscar a linha
            record = gridArtigosStock.SelectedRows(i).Record
            gridArtigosStock.Records.RemoveAt(index)
            ' Next i
            gridArtigosStock.Populate()
            buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub
    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case 1 : actualizar(True)
            Case 2 : gravarDocumento(True)
            Case 3 : enviarEmailDocumento()
            Case 4 : abrirEditorRegistos()
            Case 5 : If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then Me.Close()
            Case 6 : imprimirDoc()

        End Select
    End Sub

    Private Sub actualizar(Optional ByVal pergunta As Boolean = False)

        If pergunta Then
            If MsgBox("Deseja realmente actualizar e limpar a informação existente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
        End If

        dtpData.Value = Now
        txtPesqMolde.Text = ""
        txtArtigo.Text = ""
        gridArtigosStock.Tag = ""
        gridArtigos.Records.DeleteAll()
        gridArtigosStock.Records.DeleteAll()
        gridArtigosStock.Populate()
        inicializarMoldes()
        buscarArtigos(gridArtigos, txtArtigo.Text)
        Dim m As Motor
        m = Motor.GetInstance
        txtFornecedor.Text = ""
        txtNomeForn.Text = ""
        txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
    End Sub

    Private Function daListaArtigosExcluidos() As String
        daListaArtigosExcluidos = ""
        Dim row As ReportRecord
        '  If gridArtigosStock.Records.Count > 0 Then
        For Each row In gridArtigosStock.Records
            If daListaArtigosExcluidos = "" Then
                daListaArtigosExcluidos = "'" + row.Item(COLUMN_ARTIGO).Value + "'"
            Else
                daListaArtigosExcluidos = daListaArtigosExcluidos + ",'" + row.Item(COLUMN_ARTIGO).Value + "'"
            End If
        Next
    End Function

   
    Private Function gravarDocumento(ByVal apresentaPergunta As Boolean) As String
        Dim m As Motor
        m = Motor.GetInstance

        If apresentaPergunta Then
            If MsgBox("Deseja realmente gravar a " + lstDocumentos.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        If Not m.existeFornecedor(txtFornecedor.Text) Then
            MsgBox("O Fornecedor '" + txtFornecedor.Text + "' não existe!", MsgBoxStyle.Critical)
            Return ""
        End If

        'If lstMoldes.SelectedValue = Nothing Then
        '    MsgBox("Tem de seleccionar um Molde", MsgBoxStyle.Critical)
        '    Return ""
        'End If

        Me.Cursor = Cursors.WaitCursor

        Dim idCabecDoc As String
        idCabecDoc = gridArtigosStock.Tag

        idCabecDoc = m.actualizarDocumentoCompra(lstDocumentos.SelectedValue, txtFornecedor.Text, m.Funcionario, lstMoldes.SelectedValue, dtpData.Value, gridArtigosStock.Records, idCabecDoc)

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" And apresentaPergunta Then
            MsgBox(lstDocumentos.Text + " gravada realidada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Return idCabecDoc
    End Function

    Private Sub gridArtigos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigos.RowDblClick
        ' enviarArtigoParaSaida()
    End Sub

    Private Sub gridArtigosStock_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigosStock.RowDblClick
        ' retirarArtigoSaida()
        Dim f As FrmQt
        f = New FrmQt
        f.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
        f.ShowDialog()
        If f.BotaoSeleccionado = 1 Then
            e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Valor
        End If
        f = Nothing
        Me.Activate()
        gridArtigosStock.Populate()
    End Sub


    Private Sub abrirEditorRegistos()
        Dim f As FrmEditarEncomendas
        f = New FrmEditarEncomendas
        f.TipoDocumento = lstDocumentos.SelectedValue
        f.ShowDialog()
        If f.clicouConfirmar Then
            actualizarFormulario(f.getIdLinha, f.getValorLinha(0), f.getValorLinha(6))
        End If
    End Sub

    Private Sub actualizarFormulario(ByVal id As String, ByVal documento As String, ByVal projecto As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosStock.Tag = id
        Dim m As Motor
        m = Motor.GetInstance

        Dim entidade As String
        Dim nome As String
        entidade = ""
        nome = ""
        m.daEntidadeDocumento(id, entidade, nome, "C")

        txtFornecedor.Text = entidade
        txtNomeForn.Text = nome

        Dim dtRegistos As DataTable
        dtRegistos = m.daListaArtigosDocumento(id, "C")
        Dim dt As Date
        lstMoldes.SelectedValue = projecto
        dt = m.daDataDocumento(id, dtpData.Value, "C")
        dtpData.Value = dt
        Dim dtrow As DataRow
        gridArtigosStock.Records.DeleteAll()
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow, gridArtigosStock, dtrow("Quantidade"))
        Next
        txtArtigo.Text = ""
        buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        gridArtigosStock.Populate()
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub enviarEmailDocumento()
        Dim idDocumento As String
        Dim m As Motor
        m = Motor.GetInstance
        idDocumento = gravarDocumento(False)
        If idDocumento <> "" Then
            m.enviarEmailDocumento(idDocumento, "C")
            actualizar()
        End If
    End Sub


    Private Sub imprimirDoc()
        Dim idDocumento As String
        Dim m As Motor
        m = Motor.GetInstance
        idDocumento = gravarDocumento(False)
        If idDocumento <> "" Then
            m.imprimirDocumento(idDocumento, "C")
            actualizar()
        End If
    End Sub

    Private Sub btnForn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnForn.Click

        abrirFormLista()

    End Sub



    Private Sub abrirFormLista()
        Dim frmLista As FrmLista
        frmLista = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(4)
        frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
        frmLista.setCaption("Fornecedores")
        frmLista.ShowFilter(True)
        frmLista.setalinhamentoColunas(alinhamentoColunas)
        frmLista.ShowDialog()

        If frmLista.seleccionouOK Then
            txtFornecedor.Text = frmLista.getValor(0)
            txtNomeForn.Text = frmLista.getValor(1)
        End If
    End Sub

    Private Sub txtFornecedor_KeyUpEvent(ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DFlatEditEvents_KeyUpEvent) Handles txtFornecedor.KeyUpEvent
        If e.keyCode = Keys.F4 Then abrirFormLista()
    End Sub

    Private Sub txtNumDoc_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumDoc.Leave
        Dim m As Motor
        m = Motor.GetInstance
        Dim projecto As String
        Dim idCabecDoc As String
        projecto = ""
        idCabecDoc = m.existeDocumentoCompra(lstDocumentos.SelectedValue, txtNumDoc.Value, projecto)
        If idCabecDoc <> "" Then
            actualizarFormulario(idCabecDoc, "", projecto)
        Else
            actualizar(False)
        End If
    End Sub

    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        gravarDocumento(True)
    End Sub

    Private Sub lstDocumentos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstDocumentos.SelectedIndexChanged
        Dim m As Motor
        m = Motor.GetInstance
        txtNumDoc.Minimum = m.daValorNumeracaoMinimaDocumentoCompra(lstDocumentos.SelectedValue)
        txtNumDoc.Maximum = m.daValorNumeracaoMaximaDocumentoCompra(lstDocumentos.SelectedValue)
        txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
        actualizar(False)
    End Sub

   
End Class