﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle

Public Class FrmAddPecas

    Const MSGERRO1 As String = "A Peça não se encontra preenchida"
    Const MSGERRO2 As String = "A Descrição da peça se encontra preenchida"
    Const MSGERRO3 As String = "A peça ""PECA"" já existe!"
    Const MSGERRO4 As String = "A peça ""PECA"" inserira com sucesso!"

    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Private Sub FrmAddPecas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        inicializarForm()
        botaoSeleccionado_ = 3
    End Sub

    Private Sub inicializarForm()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"

            'Control = .Add(xtpControlButton, 2, " Remover", -1, False)
            'Control.BeginGroup = True
            'Control.DescriptionText = "Remover"
            'Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        If e.control.Index = 1 Then
            adicionarPeca()
        End If
        If e.control.Index = 2 Then
            botaoSeleccionado_ = 2
            Me.Close()
        End If
    End Sub

    Private Sub adicionarPeca()
        Dim peca As String
        Dim descricao As String

        peca = txtPeca.Text.Trim
        descricao = txtDescricao.Text.Trim

        If peca = "" Then
            MsgBox(MSGERRO1, MsgBoxStyle.Critical)
            txtPeca.Focus()
            Exit Sub
        End If

        If descricao = "" Then
            MsgBox(MSGERRO2, MsgBoxStyle.Critical)
            txtDescricao.Focus()
            Exit Sub
        End If

        Dim m As Motor
        m = Motor.GetInstance()

        If m.pecaExiste(peca) Then
            MsgBox(MSGERRO3.Replace("PECA", peca), MsgBoxStyle.Critical)
            txtPeca.Focus()
        Else
            m.inserirPeca(peca, descricao)
            MsgBox(MSGERRO4.Replace("PECA", peca), MsgBoxStyle.Information)
            Me.Close()
            botaoSeleccionado_ = 1
        End If

    End Sub
End Class