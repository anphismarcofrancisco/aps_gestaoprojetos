﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTerminar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTerminar))
        Me.dtpData = New System.Windows.Forms.DateTimePicker
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager
        Me.lblHoraInicial = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtpTempoI = New System.Windows.Forms.DateTimePicker
        Me.lstProblemas = New System.Windows.Forms.ListBox
        Me.dtpTempoF = New System.Windows.Forms.DateTimePicker
        Me.lblHoraFinal = New System.Windows.Forms.Label
        Me.dtpTempoDescontar = New System.Windows.Forms.DateTimePicker
        Me.lblTempoDescontar = New System.Windows.Forms.Label
        Me.txtObservacoes = New System.Windows.Forms.TextBox
        Me.lblProblemas = New System.Windows.Forms.Label
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtpData
        '
        Me.dtpData.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpData.Location = New System.Drawing.Point(35, 95)
        Me.dtpData.Name = "dtpData"
        Me.dtpData.Size = New System.Drawing.Size(153, 35)
        Me.dtpData.TabIndex = 1
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(1, 1)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(263, 55)
        Me.CommandBarsFrame1.TabIndex = 28
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(181, 65)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 29
        '
        'lblHoraInicial
        '
        Me.lblHoraInicial.AutoSize = True
        Me.lblHoraInicial.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHoraInicial.Location = New System.Drawing.Point(52, 149)
        Me.lblHoraInicial.Name = "lblHoraInicial"
        Me.lblHoraInicial.Size = New System.Drawing.Size(108, 25)
        Me.lblHoraInicial.TabIndex = 30
        Me.lblHoraInicial.Text = "Hora Inicial"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(85, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 25)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Data"
        '
        'dtpTempoI
        '
        Me.dtpTempoI.CustomFormat = "HH:mm"
        Me.dtpTempoI.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTempoI.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTempoI.Location = New System.Drawing.Point(35, 177)
        Me.dtpTempoI.Name = "dtpTempoI"
        Me.dtpTempoI.ShowUpDown = True
        Me.dtpTempoI.Size = New System.Drawing.Size(153, 35)
        Me.dtpTempoI.TabIndex = 33
        '
        'lstProblemas
        '
        Me.lstProblemas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstProblemas.ForeColor = System.Drawing.Color.Black
        Me.lstProblemas.FormattingEnabled = True
        Me.lstProblemas.ItemHeight = 25
        Me.lstProblemas.Location = New System.Drawing.Point(220, 78)
        Me.lstProblemas.Name = "lstProblemas"
        Me.lstProblemas.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstProblemas.Size = New System.Drawing.Size(281, 279)
        Me.lstProblemas.TabIndex = 34
        '
        'dtpTempoF
        '
        Me.dtpTempoF.CustomFormat = "HH:mm"
        Me.dtpTempoF.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTempoF.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTempoF.Location = New System.Drawing.Point(35, 243)
        Me.dtpTempoF.Name = "dtpTempoF"
        Me.dtpTempoF.ShowUpDown = True
        Me.dtpTempoF.Size = New System.Drawing.Size(153, 35)
        Me.dtpTempoF.TabIndex = 36
        '
        'lblHoraFinal
        '
        Me.lblHoraFinal.AutoSize = True
        Me.lblHoraFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHoraFinal.Location = New System.Drawing.Point(59, 215)
        Me.lblHoraFinal.Name = "lblHoraFinal"
        Me.lblHoraFinal.Size = New System.Drawing.Size(101, 25)
        Me.lblHoraFinal.TabIndex = 35
        Me.lblHoraFinal.Text = "Hora Final"
        '
        'dtpTempoDescontar
        '
        Me.dtpTempoDescontar.CustomFormat = "HH:mm"
        Me.dtpTempoDescontar.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTempoDescontar.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTempoDescontar.Location = New System.Drawing.Point(35, 326)
        Me.dtpTempoDescontar.Name = "dtpTempoDescontar"
        Me.dtpTempoDescontar.ShowUpDown = True
        Me.dtpTempoDescontar.Size = New System.Drawing.Size(153, 35)
        Me.dtpTempoDescontar.TabIndex = 38
        '
        'lblTempoDescontar
        '
        Me.lblTempoDescontar.AutoSize = True
        Me.lblTempoDescontar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTempoDescontar.Location = New System.Drawing.Point(30, 298)
        Me.lblTempoDescontar.Name = "lblTempoDescontar"
        Me.lblTempoDescontar.Size = New System.Drawing.Size(168, 25)
        Me.lblTempoDescontar.TabIndex = 37
        Me.lblTempoDescontar.Text = "Tempo Descontar"
        '
        'txtObservacoes
        '
        Me.txtObservacoes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoes.Location = New System.Drawing.Point(20, 378)
        Me.txtObservacoes.Multiline = True
        Me.txtObservacoes.Name = "txtObservacoes"
        Me.txtObservacoes.Size = New System.Drawing.Size(480, 114)
        Me.txtObservacoes.TabIndex = 39
        '
        'lblProblemas
        '
        Me.lblProblemas.AutoSize = True
        Me.lblProblemas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProblemas.Location = New System.Drawing.Point(215, 50)
        Me.lblProblemas.Name = "lblProblemas"
        Me.lblProblemas.Size = New System.Drawing.Size(105, 25)
        Me.lblProblemas.TabIndex = 40
        Me.lblProblemas.Text = "Problemas"
        '
        'FrmTerminar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        ' Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 504)
        Me.Controls.Add(Me.lblProblemas)
        Me.Controls.Add(Me.txtObservacoes)
        Me.Controls.Add(Me.dtpTempoDescontar)
        Me.Controls.Add(Me.lblTempoDescontar)
        Me.Controls.Add(Me.dtpTempoF)
        Me.Controls.Add(Me.lblHoraFinal)
        Me.Controls.Add(Me.lstProblemas)
        Me.Controls.Add(Me.dtpTempoI)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblHoraInicial)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.dtpData)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1000, 1000)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(392, 305)
        Me.Name = "FrmTerminar"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestão Projectos - Terminar"
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpData As System.Windows.Forms.DateTimePicker
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents lblHoraInicial As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpTempoI As System.Windows.Forms.DateTimePicker
    Friend WithEvents lstProblemas As System.Windows.Forms.ListBox
    Friend WithEvents dtpTempoF As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblHoraFinal As System.Windows.Forms.Label
    Friend WithEvents dtpTempoDescontar As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTempoDescontar As System.Windows.Forms.Label
    Friend WithEvents txtObservacoes As System.Windows.Forms.TextBox
    Friend WithEvents lblProblemas As System.Windows.Forms.Label
End Class
