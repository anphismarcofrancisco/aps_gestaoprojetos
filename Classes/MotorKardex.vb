﻿Imports Interop.GcpBE900
Imports PlatAPSNET

Public Class MotorKardex

    Dim comunicaKardex As Boolean
    Dim Oconn As ADODB.Connection
    Dim OconnPRIEMPRE As ADODB.Connection

    Dim servidor As String
    Dim baseDados As String
    Dim utilizador As String
    Dim password As String
    Dim strCnxn As String

    Dim statusE As String
    Dim statusS As String
    Dim statusR As String


    Dim typeE As String
    Dim typeS As String

    Private Sub New(empresa As String)

        Try


            Dim ini As IniFile


            Dim seg As New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")
            Dim conf As Configuracao
            Dim fileINI As String

            conf = Configuracao.GetInstance()

            fileINI = Application.StartupPath + "\\" + "KARDEX.ini"


            If System.IO.File.Exists(fileINI) Then


                ini = New IniFile(fileINI)


                servidor = ini.GetString("ERP", "SQLSERVER", "")
                baseDados = ini.GetString("ERP", "ERPBD", "")
                utilizador = ini.GetString("ERP", "SQLUSER", "")
                password = seg.buscarStringDesincriptada(ini.GetString("ERP", "SQLPASS", ""))

                statusE = ini.GetString("ERP", "STATUSE", "1")
                statusS = ini.GetString("ERP", "STATUSS", "0")
                statusR = ini.GetString("ERP", "STATUSR", "2")



                typeE = ini.GetString("ERP", "TYPEE", "1")
                typeS = ini.GetString("ERP", "TYPES", "2")

                ' provider de conexao
                strCnxn = "Provider=sqloledb;Data Source=" & servidor & ";Initial Catalog=" & baseDados & ";User Id=" & utilizador & ";Password=" & password & "; "

                comunicaKardex = ini.GetString("ERP", UCase(empresa), "0") = "1"
            Else
                comunicaKardex = False
            End If


            'Oconn = New ADODB.Connection
            'Oconn.CursorLocation = ADODB.CursorLocationEnum.adUseClient
            'Oconn.Open(strCnxn)

        Catch ex As Exception

        End Try
    End Sub


    Shared myInstance As MotorKardex
    Public Shared Function GetInstance(empresa As String) As MotorKardex
        If myInstance Is Nothing Then
            myInstance = New MotorKardex(empresa)
        End If
        Return myInstance
    End Function


    Public Sub integrarDocumentoStockKardex(doc As GcpBEDocumentoInterno, molde As String, arryCampos As ArrayList, Optional entradaSaida As String = "S")

        If Not comunicaKardex Then Exit Sub


        Dim conn As ConexaoOLEDB = Nothing
        Dim conexao As OleDb.OleDbConnection = Nothing
        Dim trans As OleDb.OleDbTransaction = Nothing
        Dim comando As OleDb.OleDbCommand = Nothing
        Dim status As String
        Dim type As String
        Dim quantidade As Double

        Dim m As Motor
        m = Motor.GetInstance


        conn = New ConexaoOLEDB(servidor, baseDados, utilizador, password)


        conexao = conn.buscarConexao()

        status = IIf(entradaSaida.ToUpper() = "S", statusS, statusE)

        type = IIf(entradaSaida.ToUpper() = "S", typeS, typeE)

        Try

            trans = conexao.BeginTransaction()

            comando = New OleDb.OleDbCommand()
            comando.Connection = conexao
            comando.Transaction = trans

            comando.CommandText = "UPDATE IMPORTORDERS SET STATUS='" + statusR + "' WHERE cast(ORDERID as varchar(max))  ='" + doc.ID + "'"
            conn.executarComando(comando)

            For i = 1 To doc.Linhas.NumItens

                comando = New OleDb.OleDbCommand()
                comando.Connection = conexao
                comando.Transaction = trans

                quantidade = doc.Linhas(i).Quantidade
                If quantidade < 0 Then quantidade = quantidade * -1

                comando.CommandText = "INSERT INTO IMPORTORDERS ([STATUS], ORDER_TYP,ORDERID,ORDERINFO1,ORDERINFO2,ORDERINFO3,ORDERINFO4,ORDERINFO5,MATERIAL,MATERIALDESCRIPTION,QUANTITY,ORDERLINEINFO1,ORDERLINEINFO2,ORDERLINEINFO3,ORDERLINEINFO4,ORDERLINEINFO5)"
                comando.CommandText += "VALUES"
                comando.CommandText += "('" + status + "',"
                comando.CommandText += "'" + type + "',"
                comando.CommandText += "'" + doc.ID + "',"
                comando.CommandText += "'" + doc.Filial + "',"
                comando.CommandText += "'" + doc.Tipodoc + "',"
                comando.CommandText += "'" + doc.Serie + "',"
                comando.CommandText += "'" + doc.NumDoc.ToString + "',"
                comando.CommandText += "'" + doc.Data.ToShortDateString + "',"
                comando.CommandText += "'" + doc.Linhas(i).Artigo + "',"
                comando.CommandText += "'" + doc.Linhas(i).Descricao + "',"
                comando.CommandText += "" + quantidade.ToString + ","
                comando.CommandText += "'" + molde + "',"
                comando.CommandText += buscarValoresCampos(doc.Linhas(i), arryCampos)
                comando.CommandText += ")"
                comando.Connection = conexao
                conn.executarComando(comando)
            Next

            trans.Commit()
        Catch ex As Exception
            MsgBox(ex.Message)
            If Not trans Is Nothing Then trans.Rollback()
        End Try
    End Sub

    Private Function buscarValoresCampos(linha As GcpBELinhaDocumentoInterno, arryCampos As ArrayList)
        Dim valoresCampos As String = ""

        For j = 0 To arryCampos.Count - 1
            If j < 4 Then
                valoresCampos += "'" + linha.CamposUtil(arryCampos(j)).Valor + "'"
                If j <= 2 And j < arryCampos.Count - 1 Then
                    valoresCampos += ","
                End If

            End If
        Next

        For j = 0 To 4 - arryCampos.Count - 1
            valoresCampos += ",NULL"
        Next
        Return valoresCampos
    End Function

End Class
