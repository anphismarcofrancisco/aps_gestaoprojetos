﻿Imports Interop.StdBE900
Public Class ReportConfiguracao

    Dim dsEmpresa As DataSet
    'Dim dsIniciarParametros As New DataSet
    Dim conexao As SqlClient.SqlConnection
    Dim tipoDocumento As Integer

    Public Sub New(ByVal nomeEmpresa_ As String, moeda As String, Optional ByVal tipoDocumento_ As Integer = 0)
        dsEmpresa = New DataSet
        iniciarEmpresa(nomeEmpresa_)
        buscarFormulaInicializaParametros(moeda)
        Me.tipoDocumento = tipoDocumento_
    End Sub

    Private Sub iniciarEmpresa(ByVal nome)
        buscarEmpresa(nome)
    End Sub

    Private Sub buscarEmpresa(ByVal nomeEmpresa As String)
        executarQuery("empresa", "SELECT IDNome,IDMorada,IDNumPorta,IDLocalidade,IDCodPostal,IDCodPostalLocal,IDIndicativoTelefone,IDTelefone,IDIndicativoFax,IDFax,IFNIF,ICCapitalSocial,ICConservatoria,ICMatricula,ICMoedaCapSocial,MoedaBase,MoedaAlternativa,IFMotivoIsencao FROM PRIEMPRE..Empresas WHERE CODIGO LIKE '" & nomeEmpresa & "'", conexao)
    End Sub

    Private Sub buscarFormulaInicializaParametros(ByVal moeda As String)
        '       executarQuery("inicializaParametros", "SELECT TipoDesc,DecQde,DecPrecUnit FROM ParametrosGCP,Moedas WHERE Moedas.Moeda='" & dsEmpresa.Tables("empresa").Rows(0).Item("MoedaBase") & "'", conexao)
        executarQuery("inicializaParametros", "SELECT TipoDesc,DecQde,DecPrecUnit,DecArredonda FROM ParametrosGCP,Moedas WHERE Moedas.Moeda='" & moeda & "'", conexao)
    End Sub

    Private Function executarQuery(ByVal srctable As String, ByVal comando As String, ByVal cn As SqlClient.SqlConnection) As Boolean

        Try
            Dim m As Motor
            Dim lista As StdBELista
            Dim dt As DataTable

            m = Motor.GetInstance
            lista = m.consulta(comando)
            dt = m.convertRecordSetToDataTable(lista.DataSet)
            dt.TableName = srctable
            dsEmpresa.Tables.Add(dt.Copy)
        Catch ex As Exception
            dsEmpresa = Nothing
            Return False
        End Try
        Return True
    End Function

    Public ReadOnly Property getNomeEmpresa() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDNome")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getMorada() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDMorada") & ", " & dsEmpresa.Tables(0).Rows(0).Item("IDNumPorta")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getLocalidade() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDLocalidade")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property


    Public ReadOnly Property getNumCodigoPostal() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDCodPostal")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getNumCodigoPostalLocal() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDCodPostalLocal")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getNumeroTelefone() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDIndicativoTelefone") & dsEmpresa.Tables(0).Rows(0).Item("IDTelefone")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getFax() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IDIndicativoFax") & dsEmpresa.Tables(0).Rows(0).Item("IDFax")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getNumContribuinte() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("IFNIF")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property



    Public ReadOnly Property getCapitalSocial() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("ICCapitalSocial")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property getConservatoria() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("ICConservatoria")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getMatricula() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("ICMatricula")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getMoedaCapitalSocial() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("ICMoedaCapSocial")
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get

    End Property

    Public ReadOnly Property getMoedaBase() As String
        Get
            'Return dataset.Tables(0).Rows(GridUtilizador.FocusCell.Row).Item(0)

            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    Return dsEmpresa.Tables(0).Rows(0).Item("MoedaBase")
                Else
                    Return "EUR"
                End If
            Else
                Return "EUR"
            End If
        End Get

    End Property

    Public ReadOnly Property getFormula() As String
        Get
            Dim strFormula As String

            strFormula = "StringVar Nome; StringVar Morada;StringVar Localidade; StringVar CodPostal; StringVar Telefone; StringVar Fax; StringVar Contribuinte; StringVar CapitalSocial; StringVar Conservatoria; StringVar Matricula;StringVar MoedaCapitalSocial;"
            If tipoDocumento = 2 Then
                strFormula = strFormula & "StringVar DecArrMoedaBase;StringVar DecQtd; StringVar DecPrecMoedaBase;"
            End If
            strFormula = strFormula & "Nome:='" & getNomeEmpresa & "'"
            strFormula = strFormula & ";Morada:='" & getMorada & "'"
            strFormula = strFormula & ";Localidade:='" & getLocalidade & "'"
            strFormula = strFormula & ";CodPostal:='" & getNumCodigoPostal & " " & getNumCodigoPostalLocal & "'"
            strFormula = strFormula & ";Telefone:='" & getNumeroTelefone & "'"
            strFormula = strFormula & ";Fax:='" & getFax & "'"
            strFormula = strFormula & ";Contribuinte:='" & getNumContribuinte & "'"
            strFormula = strFormula & ";CapitalSocial:='" & getCapitalSocial & "'"
            strFormula = strFormula & ";Conservatoria:='" & getConservatoria & "'"
            strFormula = strFormula & ";Matricula:='" & getMatricula & "'"
            strFormula = strFormula & ";MoedaCapitalSocial:='" & getMoedaCapitalSocial & "'"


            If tipoDocumento = 2 Then

                If dsEmpresa.Tables("inicializaParametros").Rows.Count > 0 Then
                    strFormula = strFormula & ";DecArrMoedaBase:='" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecArredonda") & "'"
                    strFormula = strFormula & ";DecQtd:='" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecQde") & "'"
                    strFormula = strFormula & ";DecPrecMoedaBase:='" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecPrecUnit") & "'"
                Else
                    strFormula = strFormula & ";DecArrMoedaBase:='" & 2 & "'"
                    strFormula = strFormula & ";DecQtd:='" & 2 & "'"
                    strFormula = strFormula & ";DecPrecMoedaBase:='" & 2 & "'"
                End If
            End If

            strFormula = strFormula & ";"

            Return strFormula
        End Get
    End Property




    Public Function getFormulaIniciarParametros(ByVal moeda As String, ByVal textoCertificacao As String) As String
        ' buscarFormulaInicializaParametros(moeda)
        If dsEmpresa.Tables("inicializaParametros").Rows.Count > 0 Then
            If textoCertificacao <> "" Then
                Return "NumberVar TipoDesc;NumberVar DecQde;NumberVar DecPrecUnit;StringVar MotivoIsencao;BooleanVar UltimaPag; StringVar PRI_TextoCertificacao; TipoDesc:= " & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("TipoDesc") & "; DecQde:=" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecQde") & "; DecPrecUnit:=" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecPrecUnit") & "; MotivoIsencao:='" & dsEmpresa.Tables("empresa").Rows(0).Item("IFMotivoIsencao") & "'; UltimaPag := False; PRI_TextoCertificacao:='" + textoCertificacao + "'"
            Else
                Return "NumberVar TipoDesc;NumberVar DecQde;NumberVar DecPrecUnit;StringVar MotivoIsencao;BooleanVar UltimaPag; TipoDesc:= " & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("TipoDesc") & "; DecQde:=" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecQde") & "; DecPrecUnit:=" & dsEmpresa.Tables("inicializaParametros").Rows(0).Item("DecPrecUnit") & "; MotivoIsencao:='" & dsEmpresa.Tables("empresa").Rows(0).Item("IFMotivoIsencao") & "'; UltimaPag := False"
            End If
        Else
            If textoCertificacao <> "" Then
                Return "NumberVar TipoDesc;NumberVar DecQde;NumberVar DecPrecUnit;StringVar MotivoIsencao;BooleanVar UltimaPag; TipoDesc:= 0; DecQde:=2; DecPrecUnit:=2; MotivoIsencao:=' '; UltimaPag := False; PRI_TextoCertificacao:='" + textoCertificacao + "'"
            Else
                Return "NumberVar TipoDesc;NumberVar DecQde;NumberVar DecPrecUnit;StringVar MotivoIsencao;BooleanVar UltimaPag;StringVar PRI_TextoCertificacao; TipoDesc:= 0; DecQde:=2; DecPrecUnit:=2; MotivoIsencao:=' '; UltimaPag := False"
            End If

        End If
    End Function

End Class
