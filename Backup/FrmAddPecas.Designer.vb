﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAddPecas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAddPecas))
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame
        Me.lblPeca = New System.Windows.Forms.Label
        Me.txtPeca = New System.Windows.Forms.TextBox
        Me.lblDescrição = New System.Windows.Forms.Label
        Me.txtDescricao = New System.Windows.Forms.TextBox
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(233, 74)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 31
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(-3, 0)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(287, 55)
        Me.CommandBarsFrame1.TabIndex = 30
        '
        'lblPeca
        '
        Me.lblPeca.AutoSize = True
        Me.lblPeca.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeca.Location = New System.Drawing.Point(12, 73)
        Me.lblPeca.Name = "lblPeca"
        Me.lblPeca.Size = New System.Drawing.Size(57, 25)
        Me.lblPeca.TabIndex = 33
        Me.lblPeca.Text = "Peça"
        '
        'txtPeca
        '
        Me.txtPeca.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeca.Location = New System.Drawing.Point(12, 102)
        Me.txtPeca.Name = "txtPeca"
        Me.txtPeca.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtPeca.Size = New System.Drawing.Size(245, 30)
        Me.txtPeca.TabIndex = 32
        '
        'lblDescrição
        '
        Me.lblDescrição.AutoSize = True
        Me.lblDescrição.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescrição.Location = New System.Drawing.Point(12, 149)
        Me.lblDescrição.Name = "lblDescrição"
        Me.lblDescrição.Size = New System.Drawing.Size(99, 25)
        Me.lblDescrição.TabIndex = 35
        Me.lblDescrição.Text = "Descrição"
        '
        'txtDescricao
        '
        Me.txtDescricao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescricao.Location = New System.Drawing.Point(12, 178)
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtDescricao.Size = New System.Drawing.Size(245, 30)
        Me.txtDescricao.TabIndex = 34
        '
        'FrmAddPecas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(271, 233)
        Me.Controls.Add(Me.lblDescrição)
        Me.Controls.Add(Me.txtDescricao)
        Me.Controls.Add(Me.lblPeca)
        Me.Controls.Add(Me.txtPeca)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(287, 271)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(287, 271)
        Me.Name = "FrmAddPecas"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Adicionar Peças"
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lblPeca As System.Windows.Forms.Label
    Friend WithEvents txtPeca As System.Windows.Forms.TextBox
    Friend WithEvents lblDescrição As System.Windows.Forms.Label
    Friend WithEvents txtDescricao As System.Windows.Forms.TextBox
End Class
