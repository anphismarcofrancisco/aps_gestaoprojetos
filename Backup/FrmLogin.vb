﻿Imports StdBE800
Imports PlatAPSNET

Public Class FrmLogin

    Private Sub FrmLogin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Dim m As Motor
            m = Motor.GetInstance
            m.fechaEmpresa()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub FrmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fileINI As String
        Dim ficheiroConfiguracao As Configuracao
        fileINI = Application.StartupPath + "\\CONFIG.ini"
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
        inicializarForm()
        inicializarEmpresas()
        txtFilter.Focus()
    End Sub

    Private Sub inicializarForm()
        Dim m As Motor
        Dim posto As String
        Dim lista As StdBELista
        m = Motor.GetInstance()
        posto = m.daPosto()
        lista = m.daListaFuncionarios(txtFilter.Text, posto)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstFuncionarios.DataSource = dt
        lstFuncionarios.ValueMember = "Codigo"
        lstFuncionarios.DisplayMember = "Nome"

        'lstFuncionarios.
        'While Not lista.NoFim
        '    lstFuncionarios.Items.Add(lista.Valor("Nome"))
        '    lista.Seguinte()
        'End While

        lstFuncionarios.SelectedItems.Clear()

        'If lstFuncionarios.Items.Count > 0 Then
        '    lstFuncionarios.SelectedIndex = 0
        'End If

    End Sub

   
    Private Sub btnUP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUP.Click
        If lstFuncionarios.SelectedIndex = -1 Or lstFuncionarios.SelectedIndex = 0 Then
            lstFuncionarios.SelectedIndex = 0
        Else
            lstFuncionarios.SelectedIndex = lstFuncionarios.SelectedIndex - 1
        End If
    End Sub

    Private Sub btnDw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDw.Click
        If lstFuncionarios.SelectedIndex = -1 Or lstFuncionarios.SelectedIndex = lstFuncionarios.Items.Count - 1 Then
            lstFuncionarios.SelectedIndex = lstFuncionarios.Items.Count - 1
        Else
            lstFuncionarios.SelectedIndex = lstFuncionarios.SelectedIndex + 1
        End If
    End Sub

    Private Sub actualizarTextBoxPassword(ByVal valor As String)
        If valor <> "-1" Then
            txtPassword.Text = txtPassword.Text + valor
        Else
            txtPassword.Text = ""
        End If
    End Sub

    Private Sub btnC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnC.Click
        actualizarTextBoxPassword(btnC.Tag)
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        actualizarTextBoxPassword(btn0.Tag)
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        actualizarTextBoxPassword(btn1.Tag)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        actualizarTextBoxPassword(btn2.Tag)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        actualizarTextBoxPassword(btn3.Tag)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        actualizarTextBoxPassword(btn4.Tag)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        actualizarTextBoxPassword(btn5.Tag)
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        actualizarTextBoxPassword(btn6.Tag)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        actualizarTextBoxPassword(btn7.Tag)
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        actualizarTextBoxPassword(btn8.Tag)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        actualizarTextBoxPassword(btn9.Tag)
    End Sub

    Private Sub btnEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        Dim m As Motor
        m = Motor.GetInstance()
        Dim funcionario As String
        Dim password As String
        If lstFuncionarios.SelectedValue <> "" Then
            funcionario = lstFuncionarios.SelectedValue
            password = txtPassword.Text
            If m.validaPassword(funcionario, password) Then

                If m.MODULO_STOCKS Then
                    Dim fs As FrmStocks
                    fs = New FrmStocks
                    m.Funcionario = funcionario
                    Dim drView As DataRowView
                    drView = lstFuncionarios.SelectedItem
                    m.NomeFuncionario = drView("Nome")
                    m.FuncionarioAdm = m.VerficarFuncionarioAdm(funcionario)
                    Me.Hide()
                    fs.ShowDialog()

                Else
                    Dim f As FrmGestPontos
                    f = New FrmGestPontos
                    '  f.idLinha = "" 
                    m.Funcionario = funcionario
                    Dim drView As DataRowView
                    drView = lstFuncionarios.SelectedItem
                    m.NomeFuncionario = drView("Nome")
                    m.FuncionarioAdm = m.VerficarFuncionarioAdm(funcionario)
                    Me.Hide()
                    f.ShowDialog()
                End If

                txtPassword.Text = ""
                Me.Show()
            Else
                MsgBox("Password Errada", MsgBoxStyle.Exclamation, "Erro")
                txtPassword.Text = ""
            End If
        Else
            MsgBox("Não existe nenhum funcionário selecionado", MsgBoxStyle.Exclamation, "Erro")
        End If
    End Sub

    Private Sub txtFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        inicializarForm()
    End Sub

    Private Sub inicializarEmpresas()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        cmbEmpresas.SelectedIndex = -1
        lista = m.daListaEmpresas()
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        cmbEmpresas.ValueMember = "Codigo"
        cmbEmpresas.DisplayMember = "Nome"
        cmbEmpresas.DataSource = dt
        
        cmbEmpresas.SelectedValue = m.Empresa
        m.AplicacaoInicializada = True
    End Sub

    Private Sub cmbEmpresas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEmpresas.SelectedIndexChanged
        Dim m As Motor
        m = Motor.GetInstance()
        '  Dim rowEmpresa As DataRowView
        '  rowEmpresa = cmbEmpresas.SelectedValue

        If m.AplicacaoInicializada Then
            m.setEmpresa(cmbEmpresas.SelectedValue)
            inicializarForm()
        End If

    End Sub
End Class