﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmGestEncomendas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGestEncomendas))
        Me.lblFornecedor = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblText = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblRegisto1 = New System.Windows.Forms.Label()
        Me.txtObservacoesLST = New System.Windows.Forms.TextBox()
        Me.txtNumDoc = New System.Windows.Forms.NumericUpDown()
        Me.dtpData = New System.Windows.Forms.DateTimePicker()
        Me.lblData = New System.Windows.Forms.Label()
        Me.tabCtlMaterial = New System.Windows.Forms.TabControl()
        Me.TabListaArtigos = New System.Windows.Forms.TabPage()
        Me.ckbVerTodos = New System.Windows.Forms.CheckBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lstFornecedor = New System.Windows.Forms.CheckedListBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lstFornPC = New System.Windows.Forms.ListBox()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.TabPedidoCotacao = New System.Windows.Forms.TabPage()
        Me.cbTodosCOT = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtObservacoesCOT = New System.Windows.Forms.TextBox()
        Me.lblRegisto2 = New System.Windows.Forms.Label()
        Me.tabEncomendas = New System.Windows.Forms.TabPage()
        Me.lblMatEntrega = New System.Windows.Forms.Label()
        Me.txtObservacoesENC = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpDataFatura = New System.Windows.Forms.DateTimePicker()
        Me.cbTodosENC = New System.Windows.Forms.CheckBox()
        Me.lblRegisto3 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Group1 = New System.Windows.Forms.GroupBox()
        Me.rb3 = New System.Windows.Forms.RadioButton()
        Me.rb2 = New System.Windows.Forms.RadioButton()
        Me.rb1 = New System.Windows.Forms.RadioButton()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.Group2 = New System.Windows.Forms.GroupBox()
        Me.rb6 = New System.Windows.Forms.RadioButton()
        Me.rb5 = New System.Windows.Forms.RadioButton()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.rb4 = New System.Windows.Forms.RadioButton()
        Me.Group3 = New System.Windows.Forms.GroupBox()
        Me.rb9 = New System.Windows.Forms.RadioButton()
        Me.rb8 = New System.Windows.Forms.RadioButton()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.rb7 = New System.Windows.Forms.RadioButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsAberto = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsPCotacao = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsEncomendado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsInterno = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsProblema = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsVerificado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsConferido = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtArtigo = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtFornecedor = New AxXtremeSuiteControls.AxFlatEdit()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.AxImageManager1 = New AxXtremeCommandBars.AxImageManager()
        Me.gridArtigos = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosCOT = New AxXtremeReportControl.AxReportControl()
        Me.gridDocsCOT = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosENT = New AxXtremeReportControl.AxReportControl()
        Me.gridDocsENC = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosENC = New AxXtremeReportControl.AxReportControl()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        CType(Me.txtNumDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCtlMaterial.SuspendLayout()
        Me.TabListaArtigos.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPedidoCotacao.SuspendLayout()
        Me.tabEncomendas.SuspendLayout()
        Me.Group1.SuspendLayout()
        Me.Group2.SuspendLayout()
        Me.Group3.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFornecedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxImageManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosCOT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridDocsCOT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosENT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridDocsENC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosENC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFornecedor
        '
        Me.lblFornecedor.AutoSize = True
        Me.lblFornecedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFornecedor.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblFornecedor.Location = New System.Drawing.Point(921, 5)
        Me.lblFornecedor.Name = "lblFornecedor"
        Me.lblFornecedor.Size = New System.Drawing.Size(112, 25)
        Me.lblFornecedor.TabIndex = 44
        Me.lblFornecedor.Text = "Fornecedor"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(6, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 25)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Artigos"
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.Location = New System.Drawing.Point(52, 60)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(152, 29)
        Me.lblText.TabIndex = 54
        Me.lblText.Text = "TJ Aços, Lda"
        Me.lblText.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label3.Location = New System.Drawing.Point(920, 431)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 25)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "Observações"
        '
        'lblRegisto1
        '
        Me.lblRegisto1.AutoSize = True
        Me.lblRegisto1.Location = New System.Drawing.Point(87, 17)
        Me.lblRegisto1.Name = "lblRegisto1"
        Me.lblRegisto1.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto1.TabIndex = 59
        Me.lblRegisto1.Text = "0 Registos"
        '
        'txtObservacoesLST
        '
        Me.txtObservacoesLST.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoesLST.Location = New System.Drawing.Point(926, 459)
        Me.txtObservacoesLST.Multiline = True
        Me.txtObservacoesLST.Name = "txtObservacoesLST"
        Me.txtObservacoesLST.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoesLST.Size = New System.Drawing.Size(322, 66)
        Me.txtObservacoesLST.TabIndex = 7
        Me.txtObservacoesLST.WordWrap = False
        '
        'txtNumDoc
        '
        Me.txtNumDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDoc.Location = New System.Drawing.Point(336, 33)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(140, 35)
        Me.txtNumDoc.TabIndex = 71
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtpData
        '
        Me.dtpData.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpData.Location = New System.Drawing.Point(491, 33)
        Me.dtpData.Name = "dtpData"
        Me.dtpData.Size = New System.Drawing.Size(153, 35)
        Me.dtpData.TabIndex = 72
        '
        'lblData
        '
        Me.lblData.AutoSize = True
        Me.lblData.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblData.Location = New System.Drawing.Point(498, 12)
        Me.lblData.Name = "lblData"
        Me.lblData.Size = New System.Drawing.Size(44, 20)
        Me.lblData.TabIndex = 73
        Me.lblData.Text = "Data"
        '
        'tabCtlMaterial
        '
        Me.tabCtlMaterial.Controls.Add(Me.TabListaArtigos)
        Me.tabCtlMaterial.Controls.Add(Me.TabPedidoCotacao)
        Me.tabCtlMaterial.Controls.Add(Me.tabEncomendas)
        Me.tabCtlMaterial.Location = New System.Drawing.Point(15, 97)
        Me.tabCtlMaterial.Name = "tabCtlMaterial"
        Me.tabCtlMaterial.SelectedIndex = 0
        Me.tabCtlMaterial.Size = New System.Drawing.Size(1262, 566)
        Me.tabCtlMaterial.TabIndex = 71
        '
        'TabListaArtigos
        '
        Me.TabListaArtigos.Controls.Add(Me.txtArtigo)
        Me.TabListaArtigos.Controls.Add(Me.txtFornecedor)
        Me.TabListaArtigos.Controls.Add(Me.lblData)
        Me.TabListaArtigos.Controls.Add(Me.dtpData)
        Me.TabListaArtigos.Controls.Add(Me.gridArtigos)
        Me.TabListaArtigos.Controls.Add(Me.ckbVerTodos)
        Me.TabListaArtigos.Controls.Add(Me.txtNumDoc)
        Me.TabListaArtigos.Controls.Add(Me.Button2)
        Me.TabListaArtigos.Controls.Add(Me.Button1)
        Me.TabListaArtigos.Controls.Add(Me.TabControl1)
        Me.TabListaArtigos.Controls.Add(Me.Label1)
        Me.TabListaArtigos.Controls.Add(Me.btnAplicar)
        Me.TabListaArtigos.Controls.Add(Me.lblRegisto1)
        Me.TabListaArtigos.Controls.Add(Me.Label3)
        Me.TabListaArtigos.Controls.Add(Me.txtObservacoesLST)
        Me.TabListaArtigos.Controls.Add(Me.lblFornecedor)
        Me.TabListaArtigos.Location = New System.Drawing.Point(4, 22)
        Me.TabListaArtigos.Name = "TabListaArtigos"
        Me.TabListaArtigos.Padding = New System.Windows.Forms.Padding(3)
        Me.TabListaArtigos.Size = New System.Drawing.Size(1254, 540)
        Me.TabListaArtigos.TabIndex = 0
        Me.TabListaArtigos.Text = "Lista Artigos"
        Me.TabListaArtigos.UseVisualStyleBackColor = True
        '
        'ckbVerTodos
        '
        Me.ckbVerTodos.AutoSize = True
        Me.ckbVerTodos.Location = New System.Drawing.Point(1140, 9)
        Me.ckbVerTodos.Name = "ckbVerTodos"
        Me.ckbVerTodos.Size = New System.Drawing.Size(109, 17)
        Me.ckbVerTodos.TabIndex = 64
        Me.ckbVerTodos.Text = "Ver Selecionados"
        Me.ckbVerTodos.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.delete_object
        Me.Button2.Location = New System.Drawing.Point(874, 33)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(40, 35)
        Me.Button2.TabIndex = 67
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.insert_object
        Me.Button1.Location = New System.Drawing.Point(828, 33)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 35)
        Me.Button1.TabIndex = 66
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(926, 70)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(322, 364)
        Me.TabControl1.TabIndex = 65
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lstFornecedor)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(314, 338)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "Fornecedores"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lstFornecedor
        '
        Me.lstFornecedor.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstFornecedor.FormattingEnabled = True
        Me.lstFornecedor.Location = New System.Drawing.Point(7, 6)
        Me.lstFornecedor.Name = "lstFornecedor"
        Me.lstFornecedor.Size = New System.Drawing.Size(301, 327)
        Me.lstFornecedor.TabIndex = 63
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lstFornPC)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(314, 338)
        Me.TabPage3.TabIndex = 1
        Me.TabPage3.Text = "Fornecedores P. Cotação"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lstFornPC
        '
        Me.lstFornPC.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.lstFornPC.FormattingEnabled = True
        Me.lstFornPC.ItemHeight = 16
        Me.lstFornPC.Location = New System.Drawing.Point(7, 6)
        Me.lstFornPC.Name = "lstFornPC"
        Me.lstFornPC.Size = New System.Drawing.Size(301, 244)
        Me.lstFornPC.TabIndex = 66
        '
        'btnAplicar
        '
        Me.btnAplicar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.search
        Me.btnAplicar.Location = New System.Drawing.Point(290, 33)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(40, 35)
        Me.btnAplicar.TabIndex = 61
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'TabPedidoCotacao
        '
        Me.TabPedidoCotacao.Controls.Add(Me.cbTodosCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.Label2)
        Me.TabPedidoCotacao.Controls.Add(Me.txtObservacoesCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.lblRegisto2)
        Me.TabPedidoCotacao.Controls.Add(Me.gridArtigosCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.gridDocsCOT)
        Me.TabPedidoCotacao.Location = New System.Drawing.Point(4, 22)
        Me.TabPedidoCotacao.Name = "TabPedidoCotacao"
        Me.TabPedidoCotacao.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPedidoCotacao.Size = New System.Drawing.Size(1254, 540)
        Me.TabPedidoCotacao.TabIndex = 1
        Me.TabPedidoCotacao.Text = "Pedido Cotação"
        Me.TabPedidoCotacao.UseVisualStyleBackColor = True
        '
        'cbTodosCOT
        '
        Me.cbTodosCOT.AutoSize = True
        Me.cbTodosCOT.Location = New System.Drawing.Point(245, 3)
        Me.cbTodosCOT.Name = "cbTodosCOT"
        Me.cbTodosCOT.Size = New System.Drawing.Size(94, 17)
        Me.cbTodosCOT.TabIndex = 66
        Me.cbTodosCOT.Text = "Mostrar Todos"
        Me.cbTodosCOT.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(343, 434)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(129, 25)
        Me.Label2.TabIndex = 63
        Me.Label2.Text = "Observações"
        '
        'txtObservacoesCOT
        '
        Me.txtObservacoesCOT.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoesCOT.Location = New System.Drawing.Point(344, 463)
        Me.txtObservacoesCOT.Multiline = True
        Me.txtObservacoesCOT.Name = "txtObservacoesCOT"
        Me.txtObservacoesCOT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoesCOT.Size = New System.Drawing.Size(895, 71)
        Me.txtObservacoesCOT.TabIndex = 62
        Me.txtObservacoesCOT.WordWrap = False
        '
        'lblRegisto2
        '
        Me.lblRegisto2.AutoSize = True
        Me.lblRegisto2.Location = New System.Drawing.Point(6, 7)
        Me.lblRegisto2.Name = "lblRegisto2"
        Me.lblRegisto2.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto2.TabIndex = 60
        Me.lblRegisto2.Text = "0 Registos"
        '
        'tabEncomendas
        '
        Me.tabEncomendas.Controls.Add(Me.lblMatEntrega)
        Me.tabEncomendas.Controls.Add(Me.txtObservacoesENC)
        Me.tabEncomendas.Controls.Add(Me.Label4)
        Me.tabEncomendas.Controls.Add(Me.Label5)
        Me.tabEncomendas.Controls.Add(Me.dtpDataFatura)
        Me.tabEncomendas.Controls.Add(Me.cbTodosENC)
        Me.tabEncomendas.Controls.Add(Me.lblRegisto3)
        Me.tabEncomendas.Controls.Add(Me.gridArtigosENT)
        Me.tabEncomendas.Controls.Add(Me.gridDocsENC)
        Me.tabEncomendas.Controls.Add(Me.gridArtigosENC)
        Me.tabEncomendas.Location = New System.Drawing.Point(4, 22)
        Me.tabEncomendas.Name = "tabEncomendas"
        Me.tabEncomendas.Padding = New System.Windows.Forms.Padding(3)
        Me.tabEncomendas.Size = New System.Drawing.Size(1254, 540)
        Me.tabEncomendas.TabIndex = 2
        Me.tabEncomendas.Text = "Encomendas"
        Me.tabEncomendas.UseVisualStyleBackColor = True
        '
        'lblMatEntrega
        '
        Me.lblMatEntrega.AutoSize = True
        Me.lblMatEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMatEntrega.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblMatEntrega.Location = New System.Drawing.Point(776, 314)
        Me.lblMatEntrega.Name = "lblMatEntrega"
        Me.lblMatEntrega.Size = New System.Drawing.Size(165, 25)
        Me.lblMatEntrega.TabIndex = 74
        Me.lblMatEntrega.Text = "Material Entregue"
        '
        'txtObservacoesENC
        '
        Me.txtObservacoesENC.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoesENC.Location = New System.Drawing.Point(369, 342)
        Me.txtObservacoesENC.Multiline = True
        Me.txtObservacoesENC.Name = "txtObservacoesENC"
        Me.txtObservacoesENC.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoesENC.Size = New System.Drawing.Size(406, 192)
        Me.txtObservacoesENC.TabIndex = 65
        Me.txtObservacoesENC.WordWrap = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(369, 314)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(129, 25)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Observações"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label5.Location = New System.Drawing.Point(1041, 314)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 16)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Data Fatura"
        '
        'dtpDataFatura
        '
        Me.dtpDataFatura.CustomFormat = ""
        Me.dtpDataFatura.Location = New System.Drawing.Point(1146, 311)
        Me.dtpDataFatura.Name = "dtpDataFatura"
        Me.dtpDataFatura.ShowCheckBox = True
        Me.dtpDataFatura.Size = New System.Drawing.Size(96, 20)
        Me.dtpDataFatura.TabIndex = 67
        '
        'cbTodosENC
        '
        Me.cbTodosENC.AutoSize = True
        Me.cbTodosENC.Location = New System.Drawing.Point(272, 3)
        Me.cbTodosENC.Name = "cbTodosENC"
        Me.cbTodosENC.Size = New System.Drawing.Size(94, 17)
        Me.cbTodosENC.TabIndex = 71
        Me.cbTodosENC.Text = "Mostrar Todos"
        Me.cbTodosENC.UseVisualStyleBackColor = True
        '
        'lblRegisto3
        '
        Me.lblRegisto3.AutoSize = True
        Me.lblRegisto3.Location = New System.Drawing.Point(6, 7)
        Me.lblRegisto3.Name = "lblRegisto3"
        Me.lblRegisto3.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto3.TabIndex = 64
        Me.lblRegisto3.Text = "0 Registos"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Todos os Ficheiros Excel 2007|*.xlsx|Todos os Ficheiros Excel 2003 V1|*.xls|Todos" &
    " os Ficheiros Excel 2003|*.xls|Todos os OpenOffice|*.ods"
        Me.OpenFileDialog1.FilterIndex = 2
        '
        'Group1
        '
        Me.Group1.Controls.Add(Me.rb3)
        Me.Group1.Controls.Add(Me.rb2)
        Me.Group1.Controls.Add(Me.rb1)
        Me.Group1.Controls.Add(Me.btn1)
        Me.Group1.Location = New System.Drawing.Point(660, 12)
        Me.Group1.Name = "Group1"
        Me.Group1.Size = New System.Drawing.Size(200, 96)
        Me.Group1.TabIndex = 78
        Me.Group1.TabStop = False
        Me.Group1.Text = "Lista Material"
        '
        'rb3
        '
        Me.rb3.AutoSize = True
        Me.rb3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb3.Location = New System.Drawing.Point(120, 68)
        Me.rb3.Name = "rb3"
        Me.rb3.Size = New System.Drawing.Size(66, 21)
        Me.rb3.TabIndex = 79
        Me.rb3.TabStop = True
        Me.rb3.Text = "Enviar"
        Me.rb3.UseVisualStyleBackColor = True
        '
        'rb2
        '
        Me.rb2.AutoSize = True
        Me.rb2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb2.Location = New System.Drawing.Point(120, 45)
        Me.rb2.Name = "rb2"
        Me.rb2.Size = New System.Drawing.Size(75, 21)
        Me.rb2.TabIndex = 75
        Me.rb2.TabStop = True
        Me.rb2.Text = "Imprimir"
        Me.rb2.UseVisualStyleBackColor = True
        '
        'rb1
        '
        Me.rb1.AutoSize = True
        Me.rb1.Checked = True
        Me.rb1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb1.Location = New System.Drawing.Point(120, 22)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(70, 21)
        Me.rb1.TabIndex = 74
        Me.rb1.TabStop = True
        Me.rb1.Text = "Gravar"
        Me.rb1.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.btn1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Cotação
        Me.btn1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn1.Location = New System.Drawing.Point(6, 25)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(108, 59)
        Me.btn1.TabIndex = 73
        Me.btn1.Text = "Lista Material"
        Me.btn1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn1.UseVisualStyleBackColor = True
        '
        'Group2
        '
        Me.Group2.Controls.Add(Me.rb6)
        Me.Group2.Controls.Add(Me.rb5)
        Me.Group2.Controls.Add(Me.btn2)
        Me.Group2.Controls.Add(Me.rb4)
        Me.Group2.Location = New System.Drawing.Point(866, 12)
        Me.Group2.Name = "Group2"
        Me.Group2.Size = New System.Drawing.Size(200, 96)
        Me.Group2.TabIndex = 80
        Me.Group2.TabStop = False
        Me.Group2.Text = "Pedido Cotação"
        '
        'rb6
        '
        Me.rb6.AutoSize = True
        Me.rb6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb6.Location = New System.Drawing.Point(120, 68)
        Me.rb6.Name = "rb6"
        Me.rb6.Size = New System.Drawing.Size(66, 21)
        Me.rb6.TabIndex = 79
        Me.rb6.TabStop = True
        Me.rb6.Text = "Enviar"
        Me.rb6.UseVisualStyleBackColor = True
        '
        'rb5
        '
        Me.rb5.AutoSize = True
        Me.rb5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb5.Location = New System.Drawing.Point(120, 45)
        Me.rb5.Name = "rb5"
        Me.rb5.Size = New System.Drawing.Size(75, 21)
        Me.rb5.TabIndex = 75
        Me.rb5.TabStop = True
        Me.rb5.Text = "Imprimir"
        Me.rb5.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PCotação
        Me.btn2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn2.Location = New System.Drawing.Point(6, 25)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(108, 59)
        Me.btn2.TabIndex = 12
        Me.btn2.Text = " P. Cotação"
        Me.btn2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn2.UseVisualStyleBackColor = True
        '
        'rb4
        '
        Me.rb4.AutoSize = True
        Me.rb4.Checked = True
        Me.rb4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb4.Location = New System.Drawing.Point(120, 22)
        Me.rb4.Name = "rb4"
        Me.rb4.Size = New System.Drawing.Size(70, 21)
        Me.rb4.TabIndex = 74
        Me.rb4.TabStop = True
        Me.rb4.Text = "Gravar"
        Me.rb4.UseVisualStyleBackColor = True
        '
        'Group3
        '
        Me.Group3.Controls.Add(Me.rb9)
        Me.Group3.Controls.Add(Me.rb8)
        Me.Group3.Controls.Add(Me.btn3)
        Me.Group3.Controls.Add(Me.rb7)
        Me.Group3.Location = New System.Drawing.Point(1072, 12)
        Me.Group3.Name = "Group3"
        Me.Group3.Size = New System.Drawing.Size(200, 96)
        Me.Group3.TabIndex = 81
        Me.Group3.TabStop = False
        Me.Group3.Text = "Encomenda"
        '
        'rb9
        '
        Me.rb9.AutoSize = True
        Me.rb9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb9.Location = New System.Drawing.Point(120, 68)
        Me.rb9.Name = "rb9"
        Me.rb9.Size = New System.Drawing.Size(66, 21)
        Me.rb9.TabIndex = 79
        Me.rb9.TabStop = True
        Me.rb9.Text = "Enviar"
        Me.rb9.UseVisualStyleBackColor = True
        '
        'rb8
        '
        Me.rb8.AutoSize = True
        Me.rb8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb8.Location = New System.Drawing.Point(120, 45)
        Me.rb8.Name = "rb8"
        Me.rb8.Size = New System.Drawing.Size(75, 21)
        Me.rb8.TabIndex = 75
        Me.rb8.TabStop = True
        Me.rb8.Text = "Imprimir"
        Me.rb8.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn3.Image = Global.APS_GestaoProjectos.My.Resources.Resources.encomendar
        Me.btn3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn3.Location = New System.Drawing.Point(6, 25)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(108, 59)
        Me.btn3.TabIndex = 12
        Me.btn3.Text = "Encomendar"
        Me.btn3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn3.UseVisualStyleBackColor = True
        '
        'rb7
        '
        Me.rb7.AutoSize = True
        Me.rb7.Checked = True
        Me.rb7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb7.Location = New System.Drawing.Point(120, 22)
        Me.rb7.Name = "rb7"
        Me.rb7.Size = New System.Drawing.Size(70, 21)
        Me.rb7.TabIndex = 74
        Me.rb7.TabStop = True
        Me.rb7.Text = "Gravar"
        Me.rb7.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsAberto, Me.tsPCotacao, Me.tsEncomendado, Me.tsInterno, Me.tsProblema, Me.tsVerificado, Me.tsConferido, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 665)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1289, 22)
        Me.StatusStrip1.TabIndex = 82
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsAberto
        '
        Me.tsAberto.Name = "tsAberto"
        Me.tsAberto.Size = New System.Drawing.Size(77, 17)
        Me.tsAberto.Text = "     Normal     "
        '
        'tsPCotacao
        '
        Me.tsPCotacao.Name = "tsPCotacao"
        Me.tsPCotacao.Size = New System.Drawing.Size(94, 17)
        Me.tsPCotacao.Text = "     P. Cotação     "
        '
        'tsEncomendado
        '
        Me.tsEncomendado.Name = "tsEncomendado"
        Me.tsEncomendado.Size = New System.Drawing.Size(114, 17)
        Me.tsEncomendado.Text = "     Encomendado     "
        '
        'tsInterno
        '
        Me.tsInterno.Name = "tsInterno"
        Me.tsInterno.Size = New System.Drawing.Size(75, 17)
        Me.tsInterno.Text = "     Interno     "
        '
        'tsProblema
        '
        Me.tsProblema.Name = "tsProblema"
        Me.tsProblema.Size = New System.Drawing.Size(88, 17)
        Me.tsProblema.Text = "     Problema     "
        '
        'tsVerificado
        '
        Me.tsVerificado.Name = "tsVerificado"
        Me.tsVerificado.Size = New System.Drawing.Size(89, 17)
        Me.tsVerificado.Text = "     Verificado     "
        '
        'tsConferido
        '
        Me.tsConferido.Name = "tsConferido"
        Me.tsConferido.Size = New System.Drawing.Size(90, 17)
        Me.tsConferido.Text = "     Conferido     "
        Me.tsConferido.ToolTipText = "     Conferido     "
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(320, 17)
        Me.ToolStripStatusLabel1.Text = "Anphis - Novas Tec. em Inf. e Tel . Lda @2015 -  Licenciado: "
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtArtigo
        '
        Me.txtArtigo.Location = New System.Drawing.Point(6, 36)
        Me.txtArtigo.Name = "txtArtigo"
        Me.txtArtigo.OcxState = CType(resources.GetObject("txtArtigo.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtArtigo.Size = New System.Drawing.Size(278, 32)
        Me.txtArtigo.TabIndex = 70
        '
        'txtFornecedor
        '
        Me.txtFornecedor.Location = New System.Drawing.Point(921, 32)
        Me.txtFornecedor.Name = "txtFornecedor"
        Me.txtFornecedor.OcxState = CType(resources.GetObject("txtFornecedor.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtFornecedor.Size = New System.Drawing.Size(328, 32)
        Me.txtFornecedor.TabIndex = 69
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(436, 19)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 84
        '
        'AxImageManager1
        '
        Me.AxImageManager1.Enabled = True
        Me.AxImageManager1.Location = New System.Drawing.Point(466, 19)
        Me.AxImageManager1.Name = "AxImageManager1"
        Me.AxImageManager1.OcxState = CType(resources.GetObject("AxImageManager1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxImageManager1.Size = New System.Drawing.Size(24, 24)
        Me.AxImageManager1.TabIndex = 83
        '
        'gridArtigos
        '
        Me.gridArtigos.Location = New System.Drawing.Point(9, 74)
        Me.gridArtigos.Name = "gridArtigos"
        Me.gridArtigos.OcxState = CType(resources.GetObject("gridArtigos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigos.Size = New System.Drawing.Size(905, 455)
        Me.gridArtigos.TabIndex = 68
        Me.gridArtigos.Tag = "0"
        '
        'gridArtigosCOT
        '
        Me.gridArtigosCOT.Location = New System.Drawing.Point(344, 23)
        Me.gridArtigosCOT.Name = "gridArtigosCOT"
        Me.gridArtigosCOT.OcxState = CType(resources.GetObject("gridArtigosCOT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosCOT.Size = New System.Drawing.Size(895, 408)
        Me.gridArtigosCOT.TabIndex = 65
        Me.gridArtigosCOT.Tag = "0"
        '
        'gridDocsCOT
        '
        Me.gridDocsCOT.Location = New System.Drawing.Point(7, 23)
        Me.gridDocsCOT.Name = "gridDocsCOT"
        Me.gridDocsCOT.OcxState = CType(resources.GetObject("gridDocsCOT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridDocsCOT.Size = New System.Drawing.Size(332, 511)
        Me.gridDocsCOT.TabIndex = 64
        Me.gridDocsCOT.Tag = "COT"
        '
        'gridArtigosENT
        '
        Me.gridArtigosENT.Location = New System.Drawing.Point(781, 342)
        Me.gridArtigosENT.Name = "gridArtigosENT"
        Me.gridArtigosENT.OcxState = CType(resources.GetObject("gridArtigosENT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosENT.Size = New System.Drawing.Size(462, 192)
        Me.gridArtigosENT.TabIndex = 73
        Me.gridArtigosENT.Tag = "0"
        '
        'gridDocsENC
        '
        Me.gridDocsENC.Location = New System.Drawing.Point(7, 23)
        Me.gridDocsENC.Name = "gridDocsENC"
        Me.gridDocsENC.OcxState = CType(resources.GetObject("gridDocsENC.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridDocsENC.Size = New System.Drawing.Size(356, 511)
        Me.gridDocsENC.TabIndex = 69
        Me.gridDocsENC.Tag = "ENC"
        '
        'gridArtigosENC
        '
        Me.gridArtigosENC.Location = New System.Drawing.Point(369, 23)
        Me.gridArtigosENC.Name = "gridArtigosENC"
        Me.gridArtigosENC.OcxState = CType(resources.GetObject("gridArtigosENC.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosENC.Size = New System.Drawing.Size(879, 285)
        Me.gridArtigosENC.TabIndex = 70
        Me.gridArtigosENC.Tag = "0"
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(12, 2)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(373, 55)
        Me.CommandBarsFrame1.TabIndex = 35
        '
        'FrmGestEncomendas
        '
        Me.ClientSize = New System.Drawing.Size(1289, 687)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.AxImageManager1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Group3)
        Me.Controls.Add(Me.Group2)
        Me.Controls.Add(Me.Group1)
        Me.Controls.Add(Me.tabCtlMaterial)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(1308, 726)
        Me.MinimumSize = New System.Drawing.Size(1024, 678)
        Me.Name = "FrmGestEncomendas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anphis - Gestão Encomendas - "
        CType(Me.txtNumDoc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCtlMaterial.ResumeLayout(False)
        Me.TabListaArtigos.ResumeLayout(False)
        Me.TabListaArtigos.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPedidoCotacao.ResumeLayout(False)
        Me.TabPedidoCotacao.PerformLayout()
        Me.tabEncomendas.ResumeLayout(False)
        Me.tabEncomendas.PerformLayout()
        Me.Group1.ResumeLayout(False)
        Me.Group1.PerformLayout()
        Me.Group2.ResumeLayout(False)
        Me.Group2.PerformLayout()
        Me.Group3.ResumeLayout(False)
        Me.Group3.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFornecedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxImageManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosCOT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridDocsCOT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosENT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridDocsENC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosENC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lblFornecedor As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblRegisto1 As System.Windows.Forms.Label
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents txtObservacoesLST As System.Windows.Forms.TextBox
    Friend WithEvents tabCtlMaterial As System.Windows.Forms.TabControl
    Friend WithEvents TabListaArtigos As System.Windows.Forms.TabPage
    Friend WithEvents TabPedidoCotacao As System.Windows.Forms.TabPage
    Friend WithEvents lblRegisto2 As System.Windows.Forms.Label
    Friend WithEvents lstFornecedor As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabEncomendas As System.Windows.Forms.TabPage
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents Group1 As System.Windows.Forms.GroupBox
    Friend WithEvents rb3 As System.Windows.Forms.RadioButton
    Friend WithEvents rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents Group2 As System.Windows.Forms.GroupBox
    Friend WithEvents rb6 As System.Windows.Forms.RadioButton
    Friend WithEvents rb5 As System.Windows.Forms.RadioButton
    Friend WithEvents rb4 As System.Windows.Forms.RadioButton
    Friend WithEvents lblRegisto3 As System.Windows.Forms.Label
    Friend WithEvents Group3 As System.Windows.Forms.GroupBox
    Friend WithEvents rb9 As System.Windows.Forms.RadioButton
    Friend WithEvents rb8 As System.Windows.Forms.RadioButton
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents rb7 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtObservacoesCOT As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtObservacoesENC As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents lstFornPC As System.Windows.Forms.ListBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsAberto As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsPCotacao As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsEncomendado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsInterno As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsProblema As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ckbVerTodos As System.Windows.Forms.CheckBox
    Friend WithEvents dtpDataFatura As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tsVerificado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtArtigo As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtFornecedor As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents gridArtigos As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigosCOT As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridDocsCOT As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigosENC As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridDocsENC As AxXtremeReportControl.AxReportControl
    Friend WithEvents AxImageManager1 As AxXtremeCommandBars.AxImageManager
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents tsConferido As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbTodosCOT As System.Windows.Forms.CheckBox
    Friend WithEvents cbTodosENC As System.Windows.Forms.CheckBox
    Friend WithEvents txtNumDoc As NumericUpDown
    Friend WithEvents dtpData As DateTimePicker
    Friend WithEvents lblData As Label
    Friend WithEvents gridArtigosENT As AxXtremeReportControl.AxReportControl
    Friend WithEvents lblMatEntrega As Label
End Class
