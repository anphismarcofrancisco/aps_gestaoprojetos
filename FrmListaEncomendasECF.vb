﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900
Imports Interop.GcpBE900
Imports PlatAPSNET
Public Class FrmListaEncomendasECF

    Dim mgrid As MotorGrelhas
    Dim linhas_ As ArrayList
    Dim hastable As Hashtable
    Dim forn_ As Fornecedor

    Dim okSelected As Boolean
    Dim tipodoc As String
    Dim modulo As String
    Dim doc As Objeto

    Private Sub FrmListaEncomendasECF_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        okSelected = False

        modulo = "C"

        iniciarToolBox()
        Dim m As MotorLM
        m = MotorLM.GetInstance()

        lblNomeFornecedor.Text = forn_.Nome

        gridArtigos.AutoColumnSizing = True
        gridArtigos.Icons = AxImageManager1.Icons


        hastable = New Hashtable

        inserirColunasRegistos(tipodoc, gridArtigos.Name, gridArtigos, True, mgrid)


        gridArtigos.AllowEdit = True
        gridArtigos.EditOnClick = True

        inserirLinhas()


        actualizarTotaisDocumento(gridArtigos, mgrid, TotalDocENC)

    End Sub

    Private Sub inserirColunasRegistos(documento As String, nomeGrid As String, ByVal grid As AxXtremeReportControl.AxReportControl, mostraFornecedores As Boolean, ByRef mgrid As MotorGrelhas)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = m.consulta("SELECT * FROM TDU_GP_CONFIGGRID WHERE CDU_APL='M' and CDU_FORM='" + Me.Name + "' and CDU_GRID='" + nomeGrid + "' ORDER BY CDU_ORDEM ASC")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        mgrid = New MotorGrelhas("C", dt, m.FuncionarioAdm, mostraFornecedores, documento)
        mgrid.inserirColunas(grid, True)
    End Sub


    Public Sub setRecordsGrid(linhas As ArrayList)
        linhas_ = linhas
    End Sub
    Public Sub setFornecedor(fornecedor As Fornecedor)
        forn_ = fornecedor
    End Sub

    Public Function setdocumentoOrigem(tipoDocOrig As String)
        tipodoc = tipoDocOrig
    End Function

    Public Function setDocOrigem(docOrig As Objeto)
        doc = docOrig
    End Function

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlButton, 2, " Gravar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Gravar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True
        End With
    End Sub

    Private Sub inserirLinhas()
        Dim row As ReportRecord

        hastable = New Hashtable

        For Each row In linhas_
            mgrid.inserirLinha(gridArtigos, row, hastable, doc.Projeto)
        Next

        gridArtigos.Populate()
    End Sub

    Private Sub btnRemoveRowLST_Click(sender As Object, e As EventArgs) Handles btnRemoveRowLST.Click
        apagarLinhaRegisto(hastable, gridArtigos)
    End Sub

    Const MSG_APAGAR_LINHA = "Deseja realmente apagar a linha(s) seleccionada(s) ?"
    Const MSG_APAGAR_LINHA_ERRO = "Existem registos que não foram eliminados!" + vbCrLf + " Já foram feitos Pedidos Cotação/Encomendas/Recepção Encomenda"

    Private Sub apagarLinhaRegisto(ByRef ht As Hashtable, grid As AxXtremeReportControl.AxReportControl)

        If grid.FocusedRow Is Nothing Then
            Exit Sub
        End If

        If MsgBox(MSG_APAGAR_LINHA, vbQuestion + vbYesNo) = vbYes Then
            Dim m As MotorLM

            Dim index As Integer
            Dim estado As String
            Dim row As ReportRow
            Dim dtrow As DataRow
            Dim apagouLinhas As Boolean = False
            Dim msg As String = ""

            Dim erroEliminacao As Boolean = False
            Dim i As Integer

            m = MotorLM.GetInstance()


            row = grid.FocusedRow

            grid.RemoveRowEx(row)


            If erroEliminacao Then
                MsgBox(MSG_APAGAR_LINHA_ERRO, MsgBoxStyle.Critical)
            End If

            recalcularIds(ht, grid)
        End If

        actualizarTotaisDocumento(gridArtigos, mgrid, TotalDocENC)
    End Sub


    Private Sub recalcularIds(ByRef ht As Hashtable, grid As AxXtremeReportControl.AxReportControl)
        Dim r As ReportRecord
        Dim id As String
        ht = New Hashtable()
        Dim row As DataRow


        For Each r In grid.Records
            row = r.Tag
            '   MsgBox(row("CDU_Obran1").ToString)
            id = row("ID").ToString
            id = mgrid.formatarIdsLinha(id)
            ht.Add(id, r.Index)
        Next


    End Sub

    Private Sub btnAddRowLST_Click(sender As Object, e As EventArgs) Handles btnAddRowLST.Click
        inserirNovaLinhaRegisto(True, gridArtigos, mgrid, hastable, -1)
    End Sub


    Private Sub inserirNovaLinhaRegisto(validaChecked As Boolean, grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas, ByRef ht As Hashtable, index As Integer)
        Dim m As MotorLM
        Dim id As String


        m = MotorLM.GetInstance


        Dim lista As StdBELista
        Dim dt As DataTable

        '  lista = m.consulta("select  '' as ID,' ' as Peca, '' as Descricao, '' as Dimensoes, '' as Material ,1 as Quantidade,'" + m.InformModLstMaterial.ARTIGO + "' as Artigo,'' as Estado,'' as Fornecedor,'' as CDU_DataRecepcao")
        lista = m.consulta("SELECT '' as ID,' ' as Peca, '' as Descricao, '' as Dimensoes, '' as Material ,1 as Quantidade,'" + m.InformModLstMaterial.ARTIGO + "' as Artigo,'' as Estado,'' as Fornecedor,0 as NumAlt,* FROM LINHASCOMPRAS WHERE 1=0")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        dt.Rows.Add()

        id = mgrid.formatarIdsLinha(m.consultaValor("SELECT NEWID()"))

        'dt(0)("ID") = " "
        dt(0)("ID") = id
        dt(0)("Peca") = " "
        dt(0)("Quantidade") = 1 * mgrid.getMultiplicador
        dt(0)("Artigo") = m.InformModLstMaterial.ARTIGO
        inserirLinhaRegisto(validaChecked, ht, dt(0), grid, mgrid, index, True)
        grid.Populate()
        recalcularIds(ht, grid)
        If index = -1 Then grid.Navigator.MoveLastRow()

        actualizarTotaisDocumento(gridArtigos, mgrid, TotalDocENC)
    End Sub


    Const COR_BRANCO As UInteger = 4294967295
    Private Sub inserirLinhaRegisto(validaChecked As Boolean, hashTable As Hashtable, ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas, Optional indexLinha As Integer = -1, Optional nova As Boolean = False)
        ' Dim m As MotorLM
        'm = MotorLM.GetInstance
        Dim cor As UInteger
        If dtRow("CDu_ObraN1").ToString() = "" And dtRow("Id").ToString() = "" Then Exit Sub
        cor = COR_BRANCO
        Dim datasVazias As Boolean
        datasVazias = grid.Name = "gridArtigos"

        mgrid.inserirLinha(grid, dtRow, dtRow("Estado").ToString(), cor, indexLinha, hashTable, nova, validaChecked, datasVazias, doc)
    End Sub



    Private Sub actualizarTotaisDocumento(grid As AxXtremeReportControl.AxReportControl, mGrid As MotorGrelhas, lblTotal As Label)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim index As Integer
        Dim indexQT As Integer
        Dim record As ReportRecord
        Dim total As Double = 0

        index = mGrid.buscarIndexCampo("PRECUNIT")
        indexQT = mGrid.buscarIndexCampo("QUANTIDADE")

        ' If index <> -1 Then
        For Each record In grid.Records
            If IsNumeric(record(index).Value) Then
                total = total + (record(index).Value * record(indexQT).Value)
            End If
        Next

        lblTotal.Text = "Total: " + FormatNumber(total, 2)

    End Sub

    Private Sub gridArtigos_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigos.ValueChanged
        validarValorCampo(mgrid, e)
        actualizarTotaisDocumento(gridArtigos, mgrid, TotalDocENC)
    End Sub

    Private Function validarValorCampo(mgrid As MotorGrelhas, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent)
        Dim result As String
        result = mgrid.validarCampo(e.column.Tag, e.item.Value)

        If result = "" Then
            e.item.Value = mgrid.formatarValorCampo(e.column.Tag, e.item.Value, False)
            e.item.Caption = e.item.Value
            If e.column.Tag <> "ARTIGO" Then
                e.item.Tag = e.item.Value
            End If
        Else
            MsgBox(result, MsgBoxStyle.Critical)
            e.item.Value = e.item.Tag
            e.item.Caption = mgrid.formatarValorCampo(e.column.Tag, e.item.Tag, False)
        End If
    End Function


    Public Function buscarLinhasArtigos() As Object
        Dim lista As ArrayList
        lista = New ArrayList

        Dim grid As AxXtremeReportControl.AxReportControl

        Dim row As ReportRecord

        For Each row In gridArtigos.Records
            ' If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then
            lista.Add(row)
            '  End If
        Next

        Return lista



    End Function

    Private Sub CommandBarsFrame1_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        okSelected = True
        Me.Close()
    End Sub


    Public Function seleccionouOk() As Boolean
        Return okSelected
    End Function

    Public Function getMotorGrid() As MotorGrelhas
        Return mgrid
    End Function


    Private Sub abrirFormLista(grelha As AxXtremeReportControl.AxReportControl, linha As Integer, coluna As Integer)
        Dim frm As FrmLista
        frm = New FrmLista
        Dim query As String
        Dim m As MotorLM
        Dim lista As StdBELista
        m = MotorLM.GetInstance


        query = mgrid.buscarSubQuery(grelha.Columns(coluna).Tag)
        If query = "" Then Exit Sub

        lista = m.consulta("select * from (" + query + ") as Query WHERE 1=0")
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(lista.NumColunas)
        frm.setalinhamentoColunas(alinhamentoColunas)
        frm.setComando(query)
        frm.ShowFilter(True)
        frm.monstrarAddPecas(grelha.Columns(coluna).Tag = "CDU_OBRAN1")
        frm.ShowDialog()

        If frm.seleccionouOK Then
            grelha.Rows(linha).Record(coluna).Value = frm.getValor(0)
            grelha.Rows(linha).Record(coluna).Caption = frm.getValor(0)

            If gridArtigos.Columns(coluna).Caption.ToUpper() = "PEÇA" Then
                coluna = buscarIndexCampo(gridArtigos, "DESCRICAO")
                If coluna <> 0 Then
                    grelha.Rows(linha).Record(coluna).Value = frm.getValor(1)
                    grelha.Rows(linha).Record(coluna).Caption = frm.getValor(1)
                End If
            End If
        End If
    End Sub

    Private Sub gridArtigos_InplaceButtonDown(sender As Object, e As AxXtremeReportControl._DReportControlEvents_InplaceButtonDownEvent) Handles gridArtigos.InplaceButtonDown
        abrirFormLista(gridArtigos, e.button.Row.Index, e.button.Column.Index)
    End Sub

    Private Function buscarIndexCampo(grid As AxXtremeReportControl.AxReportControl, campo As String) As Integer
        Select Case grid.Name
            Case "gridArtigos" : Return mgrid.buscarIndexCampo(campo)
        End Select


    End Function




    Private Sub actualizarValorData(report As AxXtremeReportControl.AxReportControl, indexColuna As Integer)
        report.FocusedRow.Record(indexColuna).Value = CDate(dtPicker.Value).ToShortDateString
        report.Populate()
        MDtPicker.HideDatePicker(dtPicker)
    End Sub

    Private Sub gridArtigos_BeginEdit(sender As Object, e As AxXtremeReportControl._DReportControlEvents_BeginEditEvent) Handles gridArtigos.BeginEdit
        dtPicker.Visible = False
        If eCampoData(daTabelaModulo(), e.column.Tag) And e.column.Editable Then
            dtPicker.Tag = gridArtigos.Name + "," + CStr(e.column.Index)
            MDtPicker.ShowDatePicker(gridArtigos, dtPicker, e.row, e.column, e.item)
        End If

    End Sub

    Private Sub gridArtigos_FocusChanging(sender As Object, e As AxXtremeReportControl._DReportControlEvents_FocusChangingEvent) Handles gridArtigos.FocusChanging
        MDtPicker.HideDatePicker(dtPicker)
    End Sub

    Private Function eCampoData(tabela As String, campo As String) As Boolean
        Dim type As String
        Dim m As MotorAPL
        m = New MotorAPL(Motor.GetInstance())
        type = m.daTipoCampo(tabela, campo)
        Return type = "datetime" Or type = "smalldatetime"
    End Function


    Private Function daTabelaModulo() As String
        Dim tabela As String
        tabela = ""
        Select Case modulo
            Case "V" : tabela = "LinhasDoc"
            Case "C" : tabela = "LinhasCompras"
        End Select
        Return tabela
    End Function

    Private Sub btnRepartirCustos_Click(sender As Object, e As EventArgs) Handles btnRepartirCustos.Click
        Dim frmCustos As FrmReparticaoValores
        frmCustos = New FrmReparticaoValores
        frmCustos.ShowDialog()

        If frmCustos.seleccionouOK Then
            repartirCustos(frmCustos.getValor(), gridArtigos, mgrid)
        End If
        actualizarTotaisDocumento(gridArtigos, mgrid, TotalDocENC)
    End Sub

    Private Sub repartirCustos(total As Double, grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas)
        Dim quantidadeTotal As Double
        Dim custoIndividual As Double
        quantidadeTotal = daQuantidadeTotalDocumento(grid, mgrid)

        If quantidadeTotal <> 0 Then
            custoIndividual = total / quantidadeTotal
        End If
        actualizarPrecUnitLinhas(custoIndividual, grid, mgrid)
    End Sub

    Private Sub btnAplicarTodos_Click(sender As Object, e As EventArgs) Handles btnAplicarTodos.Click
        aplicarTodos(gridArtigos)
    End Sub

    Private Function daQuantidadeTotalDocumento(grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas) As Double
        Dim total As Double
        Dim row As ReportRow
        Dim index As Integer

        For Each row In grid.Rows
            index = mgrid.buscarIndexCampo("QUANTIDADE")
            If index <> -1 Then
                If IsNumeric(row.Record(index).Value) Then
                    total += CDbl(row.Record(index).Value)
                End If
            End If
        Next
        Return total
    End Function


    Private Sub actualizarPrecUnitLinhas(precUnit As Double, grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas)
        Dim indexQ As Integer
        Dim indexP As Integer
        indexQ = mgrid.buscarIndexCampo("QUANTIDADE")
        indexP = mgrid.buscarIndexCampo("PRECUNIT")

        If indexQ <> -1 And indexP <> -1 Then
            For Each row In grid.Rows
                If IsNumeric(row.Record(indexQ).Value) Then
                    If CDbl(row.Record(indexQ).Value) > 0 Then
                        row.Record(indexP).value = Math.Round(precUnit, 2)
                        row.Record(indexP).Caption = mgrid.formatarValorCampo("PRECUNIT", row.Record(indexP).value, False)
                    End If

                End If
            Next
        End If
        grid.Populate()

    End Sub

    Private Sub aplicarTodos(grid As AxXtremeReportControl.AxReportControl)
        Dim col As Integer
        Dim row As Integer
        Dim i As Integer
        Dim record As ReportRecord
        If Not grid.FocusedColumn Is Nothing And Not grid.FocusedRow Is Nothing Then
            col = grid.FocusedColumn.Index
            row = grid.FocusedRow.Index
            For i = row To grid.Records.Count - 1

                record = grid.Records(i)
                record(col).Value = grid.Records(row)(col).Value

                If record(col).HasCheckbox Then
                    record(col).Checked = grid.Records(row)(col).Checked
                End If
            Next
            grid.Populate()
        End If
    End Sub

    Private Sub dtPicker_CloseUp(sender As Object, e As EventArgs) Handles dtPicker.CloseUp
        Dim str() As String
        str = dtPicker.Tag.ToString.Split(",")
        If str.Length = 2 Then
            actualizarValorData(gridArtigos, CInt(str(1)))
        End If

    End Sub
End Class