﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900
Imports Interop.GcpBE900
Imports PlatAPSNET

Public Class FrmGestEncomendas


    Const COLUMN_ARTIGO As Integer = 0
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTIDADE As Integer = 2
    Const COLUMN_STKACTUAL As Integer = 3
    Const COLUMN_ULTPRECO As Integer = 4
    Const COLUMN_FORNPRECO As Integer = 5
    Const COLUMN_LINHADATA As Integer = 6
    Const COLUMN_CHECKBOX As Integer = 7



    Const ENCCOLUMN_ARTIGO As Integer = 0
    Const ENCCOLUMN_DESCRICAO As Integer = 1
    Const ENCCOLUMN_ULTPRECO As Integer = 2
    Const ENCCOLUMN_STKACTUAL As Integer = 3
    Const ENCCOLUMN_QUANTIDADE As Integer = 4
    Const ENCCOLUMN_QUANTIDADEENT As Integer = 5
    Const ENCCOLUMN_QUANTIDADEFALTA As Integer = 6
    Const ENCCOLUMN_FORNPRECO As Integer = 7
    Const ENCCOLUMN_LINHADATA As Integer = 8
    Const ENCCOLUMN_CHECKBOX As Integer = 9


    Const ENTCOLUMN_ARTIGO As Integer = 0
    Const ENTCOLUMN_DESCRICAO As Integer = 1
    Const ENTCOLUMN_QUANTIDADE As Integer = 2
    Const ENTCOLUMN_LINHADATA As Integer = 3


    Const COLUMN_ENTIDADE As Integer = 0
    Const COLUMN_NOME As Integer = 1
    Const COLUMN_DATA As Integer = 2
    ' Const COLUMN_SERIE As Integer = 3
    Const COLUMN_NUMDOC As Integer = 3

    Dim aplConfigurada As Boolean
    Dim modulo As String
    Dim doc As GcpBEDocumentoCompra
    Dim listaFornecedores As ArrayList

    Const COR_BRANCO As UInteger = 4294967295
    Const COR_COT As UInteger = 12432256
    Const COR_ECF As String = "6495ed"
    Const COR_VERMELHO As String = "605DCF"
    Const COR_INTERNO As String = "4682b4"
    Const COR_VERIFICADO As String = "48d1cc"
    Const COR_CONFERIDO As String = "bebebe"
    Const COR_ARTIGODEFAULT As String = "87CEEB"
    ' Const COR_ARTIGODEFAULT As String = "9D7D81"
    'Const COR_ARTIGODEFAULT As String = "5EC664"
    'cor = 12432256

    Const ESTADO_NORMAL = ""
    Const ESTADO_INTERNO = "I"
    Const ESTADO_PROBLEMAS = "P"
    Const ESTADO_COT = "C"
    Const ESTADO_ECF = "E"
    Const ESTADO_VER = "V"
    Const ESTADO_CONF = "F"

    Private Function getIdCabecdoc() As String
        If gridArtigos.Tag = "0" Then
            Return ""
        Else
            Return gridArtigos.Tag
        End If

    End Function

    Private Sub FrmGestEncomendas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim m As Motor
        m = Motor.GetInstance
        If m.getApresentaArtigosStockMinimo() Then
            validarArtigosStockMinimo()
        End If
    End Sub

    Private Sub FrmEncomendas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fileINI As String
        Dim ficheiroConfiguracao As Configuracao
        fileINI = Application.StartupPath + "\\CONFIG.ini"
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        listaFornecedores = New ArrayList

        dtpDataFatura.Format = DateTimePickerFormat.Short
        dtpDataFatura.Value = Now
        dtpDataFatura.Checked = False
        modulo = "C"

        lblText.Text = m.EmpresaDescricao
        Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario
        gridArtigos.Icons = AxImageManager1.Icons
        gridArtigosCOT.Icons = AxImageManager1.Icons
        gridArtigosENC.Icons = AxImageManager1.Icons
        gridDocsENC.Icons = AxImageManager1.Icons

        gridArtigosCOT.Tag = ""

        emModoEdicao = False

        iniciarToolBox()
        inicializarFornecedores()
        carregarFornecedoresLinha("")

        gridArtigos.AutoColumnSizing = False

        gridArtigosCOT.AutoColumnSizing = False
        gridDocsCOT.AutoColumnSizing = False

        gridArtigosENC.AutoColumnSizing = False
        gridDocsENC.AutoColumnSizing = False

        inserirColunasRegistos(gridArtigos)
        inserirColunasRegistos(gridArtigosCOT)
        inserirColunasRegistosDocs(gridDocsCOT)

        inserirColunasRegistosENC(gridArtigosENC)
        inserirColunasRegistosDocs(gridDocsENC)

        inserirColunasRegistosMATENT(gridArtigosENT)

        '  inserirColunasRegistos(2, gridArtigosStock, m.getApresentaProjectos)


        gridArtigos.AllowEdit = True
        gridArtigos.EditOnClick = False

        gridArtigosCOT.AllowEdit = True
        gridArtigosCOT.EditOnClick = True

        gridArtigosENC.AllowEdit = True
        gridArtigosENC.EditOnClick = True


        lstFornecedor.CheckOnClick = True

        gridArtigosENT.AllowEdit = True
        gridArtigosENT.EditOnClick = True


        tabCtlMaterial.TabPages(0).Tag = m.InformModLstMaterial.TIPODOCLM + ";" + m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + m.InformModLstMaterial.TIPODOCLM + "'")
        tabCtlMaterial.TabPages(1).Tag = m.InformModLstMaterial.TIPODOCCOT + ";" + m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + m.InformModLstMaterial.TIPODOCCOT + "'")
        tabCtlMaterial.TabPages(2).Tag = m.InformModLstMaterial.TIPODOCECF + ";" + m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + m.InformModLstMaterial.TIPODOCECF + "'")

        gridArtigos.SelectionEnable = False
        gridArtigosCOT.SelectionEnable = False
        gridArtigosENC.SelectionEnable = True

        If modulo = "C" Then
            txtNumDoc.Minimum = 0 ' m.daValorNumeracaoMinimaDocumentoCompra(lstDocumentos.SelectedValue)
            txtNumDoc.Maximum = 999999999 'm.daValorNumeracaoMaximaDocumentoCompra(lstDocumentos.SelectedValue)
            txtNumDoc.Value = m.daNumeracaoDocumentoCompra(m.InformModLstMaterial.TIPODOCLM)
            'Else
            '    txtNumDoc.Minimum = 0 'm.daValorNumeracaoMinimaDocumentoVenda(lstDocumentos.SelectedValue)
            '    txtNumDoc.Maximum = 999999999 ' m.daValorNumeracaoMaximaDocumentoVenda(lstDocumentos.SelectedValue)
            '    txtNumDoc.Value = m.daNumeracaoDocumentoVenda(m.InformModLstMaterial.TIPODOCLM)
        End If

        'aplConfigurada = True
        configurarAplicacao()
        Group1.Text = "Lista Material"

        actualizarToolbar()
    End Sub

    Private Sub actualizarToolbar()
        tsAberto.BackColor = Color.White
        tsEncomendado.BackColor = ColorTranslator.FromHtml("#" + "0d9864")
        tsPCotacao.BackColor = ColorTranslator.FromHtml("#80b3bd")
        tsInterno.BackColor = ColorTranslator.FromHtml("#" + "B48446")
        tsProblema.BackColor = ColorTranslator.FromHtml("#" + "cf5d60") 'Color.FromName(COR_VERMELHO)
        tsVerificado.BackColor = ColorTranslator.FromHtml("#" + "ECD34A")
        tsConferido.BackColor = ColorTranslator.FromHtml("#" + "DEE0E0")
    End Sub

    Private Sub inicializarFornecedores()
        Dim m As MotorGE
        Dim lista As StdBELista
        m = MotorGE.GetInstance()
        lstFornecedor.SelectedIndex = -1
        '   lstFornecedor.Items.Clear()
        lista = m.daListaFornecedores(txtFornecedor.Text, buscarListaFornecedores)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstFornecedor.DataSource = Nothing
        lstFornecedor.DataSource = dt
        lstFornecedor.ValueMember = "Codigo"
        lstFornecedor.DisplayMember = "Nome"
        lstFornecedor.ClearSelected()
    End Sub

    Private Function buscarListaFornecedores() As String
        Dim strForn As String
        Dim row As String
        strForn = ""
        If ckbVerTodos.Checked Then
            For Each row In listaFornecedores
                If strForn <> "" Then
                    strForn = strForn + ","
                End If
                strForn = strForn + "'" + row + "'"
            Next
            If strForn = "" Then
                strForn = "'1=0'"
            End If
            Return strForn
        Else
            Return ""
        End If
    End Function

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Novo", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Novo"

            Control = .Add(xtpControlButton, 2, " Gravar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Gravar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False

            Control = .Add(xtpControlButton, 6, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False


            Control = .Add(xtpControlButton, 3, " Enviar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Enviar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False

            Control = .Add(xtpControlButton, 4, " Registos", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Registos"
            Control.Style = xtpButtonIconAndCaptionBelow
            '   Control.Visible = False

            Control = .Add(xtpControlButton, 5, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub inserirColunasRegistos(ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim Column As ReportColumn

        Column = inserirColuna(grid, COLUMN_ARTIGO, nomeColuna(COLUMN_ARTIGO, grid), tamanhoColuna(COLUMN_ARTIGO, grid), False, True)
        Column.EditOptions.AllowEdit = grelhaPai(grid)
        Column.EditOptions.AddExpandButton()
        Column = inserirColuna(grid, COLUMN_DESCRICAO, nomeColuna(COLUMN_DESCRICAO, grid), tamanhoColuna(COLUMN_DESCRICAO, grid), False, True)
        ' If indexGrid = 2 Then
        Column.EditOptions.AllowEdit = True
        Column = inserirColuna(grid, COLUMN_QUANTIDADE, nomeColuna(COLUMN_QUANTIDADE, grid), tamanhoColuna(COLUMN_QUANTIDADE, grid), False, True)
        Column.Alignment = xtpAlignmentRight
        Column.EditOptions.AllowEdit = True

        Column = inserirColuna(grid, COLUMN_STKACTUAL, nomeColuna(COLUMN_STKACTUAL, grid), tamanhoColuna(COLUMN_STKACTUAL, grid), False, True)
        Column.EditOptions.AllowEdit = False
        Column.Alignment = xtpAlignmentRight

        Column = inserirColuna(grid, COLUMN_ULTPRECO, nomeColuna(COLUMN_ULTPRECO, grid), tamanhoColuna(COLUMN_ULTPRECO, grid), False, True)
        Column.EditOptions.AllowEdit = False
        Column.Alignment = xtpAlignmentRight


        Column = inserirColuna(grid, COLUMN_FORNPRECO, nomeColuna(COLUMN_FORNPRECO, grid), tamanhoColuna(COLUMN_FORNPRECO, grid), False, True)
        Column.EditOptions.AllowEdit = Not grelhaPai(grid)
        Column.Alignment = xtpAlignmentRight

        Column = inserirColuna(grid, COLUMN_LINHADATA, nomeColuna(COLUMN_LINHADATA, grid), tamanhoColuna(COLUMN_LINHADATA, grid), False, True)
        Column.EditOptions.AllowEdit = grelhaENC(grid)
        Column.Visible = Column.EditOptions.AllowEdit
        Column.Alignment = xtpAlignmentCenter

        If m.FuncionarioAdm Then
            Column = inserirColuna(grid, COLUMN_CHECKBOX, "", 20, False, True)
            Column.EditOptions.AllowEdit = True
            Column.Alignment = xtpAlignmentRight
            Column.Icon = 1
        End If

    End Sub


    Private Sub inserirColunasRegistosMATENT(ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim Column As ReportColumn

        Column = inserirColuna(grid, ENTCOLUMN_ARTIGO, "Artigo", 80, False, True)
        Column.EditOptions.AllowEdit = False
        Column = inserirColuna(grid, ENTCOLUMN_DESCRICAO, "Descricao", 100, False, True)
        Column.EditOptions.AllowEdit = False
        Column = inserirColuna(grid, ENTCOLUMN_QUANTIDADE, "Quantidade", 60, False, True)
        Column.EditOptions.AllowEdit = True
        Column.Alignment = XTPColumnAlignment.xtpAlignmentRight
        Column = inserirColuna(grid, ENTCOLUMN_LINHADATA, "Data Entrega", 80, False, True)
        Column.EditOptions.AllowEdit = True
        Column.Alignment = XTPColumnAlignment.xtpAlignmentRight


    End Sub

    Private Sub inserirColunasRegistosENC(ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim Column As ReportColumn

        Column = inserirColuna(grid, ENCCOLUMN_ARTIGO, nomeColunaEnc(ENCCOLUMN_ARTIGO, grid), tamanhoColuna(ENCCOLUMN_ARTIGO, grid), False, True)
        ' Column.EditOptions.AllowEdit = grelhaPai(grid)
        '  Column.EditOptions.AddExpandButton()
        Column = inserirColuna(grid, ENCCOLUMN_DESCRICAO, nomeColunaEnc(ENCCOLUMN_DESCRICAO, grid), tamanhoColuna(ENCCOLUMN_DESCRICAO, grid), False, True)
        Column.EditOptions.AllowEdit = True

        Column = inserirColuna(grid, ENCCOLUMN_ULTPRECO, nomeColunaEnc(ENCCOLUMN_ULTPRECO, grid), tamanhoColuna(ENCCOLUMN_ULTPRECO, grid), False, True)
        Column.EditOptions.AllowEdit = False
        Column.Alignment = xtpAlignmentRight

        Column = inserirColuna(grid, ENCCOLUMN_STKACTUAL, nomeColunaEnc(ENCCOLUMN_STKACTUAL, grid), tamanhoColuna(ENCCOLUMN_STKACTUAL, grid), False, True)
        Column.EditOptions.AllowEdit = False
        Column.Alignment = xtpAlignmentRight


        Column = inserirColuna(grid, ENCCOLUMN_QUANTIDADE, nomeColunaEnc(ENCCOLUMN_QUANTIDADE, grid), tamanhoColuna(ENCCOLUMN_QUANTIDADE, grid), False, True)
        Column.Alignment = xtpAlignmentRight
        Column.EditOptions.AllowEdit = False


        Column = inserirColuna(grid, ENCCOLUMN_QUANTIDADEENT, nomeColunaEnc(ENCCOLUMN_QUANTIDADEENT, grid), tamanhoColuna(ENCCOLUMN_QUANTIDADEENT, grid), False, True)
        Column.Alignment = xtpAlignmentRight
        Column.EditOptions.AllowEdit = False

        Column = inserirColuna(grid, ENCCOLUMN_QUANTIDADEFALTA, nomeColunaEnc(ENCCOLUMN_QUANTIDADEFALTA, grid), tamanhoColuna(ENCCOLUMN_QUANTIDADEFALTA, grid), False, True)
        Column.Alignment = xtpAlignmentRight
        Column.EditOptions.AllowEdit = True

        Column = inserirColuna(grid, ENCCOLUMN_FORNPRECO, nomeColunaEnc(ENCCOLUMN_FORNPRECO, grid), tamanhoColuna(ENCCOLUMN_FORNPRECO, grid), False, True)
        Column.EditOptions.AllowEdit = Not grelhaPai(grid)
        Column.Alignment = xtpAlignmentRight

        Column = inserirColuna(grid, ENCCOLUMN_LINHADATA, nomeColunaEnc(ENCCOLUMN_LINHADATA, grid), tamanhoColuna(ENCCOLUMN_LINHADATA, grid), False, True)
        Column.EditOptions.AllowEdit = grelhaENC(grid)
        Column.Visible = Column.EditOptions.AllowEdit
        Column.Alignment = xtpAlignmentCenter

        If m.FuncionarioAdm Then
            Column = inserirColuna(grid, ENCCOLUMN_CHECKBOX, "", 20, False, True)
            Column.EditOptions.AllowEdit = True
            Column.Alignment = xtpAlignmentRight
            Column.Icon = 1
        End If

    End Sub

    Private Function nomeColuna(index As Integer, grid As AxXtremeReportControl.AxReportControl)
        Select Case index
            Case COLUMN_ARTIGO : Return "Artigo"
            Case COLUMN_DESCRICAO : Return "Descricao"
            Case COLUMN_QUANTIDADE : Return "Quant."
            Case COLUMN_FORNPRECO : Return IIf(grelhaPai(grid), "Ult. Forn.", "Preço")
            Case COLUMN_ULTPRECO : Return "Ult. Preço"
            Case COLUMN_STKACTUAL : Return "Stock"
            Case COLUMN_LINHADATA : Return "Dt. Recep."
        End Select
    End Function

    Private Function nomeColunaEnc(index As Integer, grid As AxXtremeReportControl.AxReportControl)
        Select Case index
            Case ENCCOLUMN_ARTIGO : Return "Artigo"
            Case ENCCOLUMN_DESCRICAO : Return "Descricao"
            Case ENCCOLUMN_ULTPRECO : Return "Ult. Preço"
            Case ENCCOLUMN_STKACTUAL : Return "Stock"
            Case ENCCOLUMN_QUANTIDADE : Return "Quant. Enc."
            Case ENCCOLUMN_QUANTIDADEENT : Return "Quant. Ent."
            Case ENCCOLUMN_QUANTIDADEFALTA : Return "Quant."
            Case ENCCOLUMN_FORNPRECO : Return IIf(grelhaPai(grid), "Ult. Forn.", "Preço")
            Case ENCCOLUMN_LINHADATA : Return "Dt. Recep."
        End Select
    End Function


    Private Sub inserirColunasRegistosDocs(ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ENTIDADE, "Entidade", 50, False, True)
        Column = inserirColuna(grid, COLUMN_NOME, "Nome", 120, False, True)
        Column = inserirColuna(grid, COLUMN_DATA, "Data", 70, False, True)
        Column.Alignment = xtpAlignmentCenter
        Column = inserirColuna(grid, COLUMN_NUMDOC, "Num. Doc.", 70, False, True)
        Column.Alignment = xtpAlignmentRight
        If m.FuncionarioAdm And grid.Tag = "ENC" Then
            Column = inserirColuna(grid, 4, "", 20, False, True)
            Column.EditOptions.AllowEdit = True
            Column.Alignment = xtpAlignmentRight
            Column.Icon = 1
        End If

    End Sub


    Private Function tamanhoColuna(index As Integer, grid As AxXtremeReportControl.AxReportControl) As Integer
        Dim m As MotorGE
        Dim tamanho As Integer
        Dim tamanhoForn As Integer
        Dim tamanhoData As Integer
        m = MotorGE.GetInstance
        tamanho = 0
        tamanhoForn = 0

        If Not grelhaPai(grid) Then
            tamanhoForn = 80
        End If

        If grelhaENC(grid) Then
            tamanhoData = 80
        End If

        If m.FuncionarioAdm Then
            If Not grelhaENC(grid) Then
                Select Case index
                    Case COLUMN_ARTIGO : Return 100
                    Case COLUMN_DESCRICAO : Return 250 + tamanhoForn + tamanho - tamanhoData
                    Case COLUMN_QUANTIDADE : Return 60 + tamanho
                    Case COLUMN_STKACTUAL : Return 60 + tamanho
                    Case COLUMN_FORNPRECO : Return 150 - tamanhoForn + tamanho
                    Case COLUMN_ULTPRECO : Return 70 + tamanho
                    Case COLUMN_LINHADATA : Return 80 + tamanho
                End Select
            Else
                Select Case index
                    Case ENCCOLUMN_ARTIGO : Return 100
                    Case ENCCOLUMN_DESCRICAO : Return 250 + tamanhoForn + tamanho - tamanhoData
                    Case ENCCOLUMN_ULTPRECO : Return 70 + tamanho
                    Case ENCCOLUMN_STKACTUAL : Return 60 + tamanho
                    Case ENCCOLUMN_QUANTIDADE : Return 70 + tamanho
                    Case ENCCOLUMN_QUANTIDADEENT : Return 70 + tamanho
                    Case ENCCOLUMN_QUANTIDADEFALTA : Return 70 + tamanho
                    Case ENCCOLUMN_FORNPRECO : Return 150 - tamanhoForn + tamanho
                    Case ENCCOLUMN_LINHADATA : Return 80 + tamanho
                End Select
            End If
        Else
            Select Case index
                Case COLUMN_ARTIGO : Return 137
                Case COLUMN_DESCRICAO : Return 337
                Case COLUMN_QUANTIDADE : Return 109
                Case COLUMN_STKACTUAL : Return 150
                Case COLUMN_FORNPRECO : Return 220
                Case COLUMN_ULTPRECO : Return 154
            End Select
        End If

    End Function


    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)
        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Function buscarCor(estado As String) As UInteger
        Dim cor As UInteger
        cor = COR_BRANCO

        Select Case estado
            Case "" : cor = COR_BRANCO
            Case "I" : cor = System.Convert.ToUInt32(HexToDecimal(COR_INTERNO))
            Case "P" : cor = System.Convert.ToUInt32(HexToDecimal(COR_VERMELHO))
            Case "C" : cor = COR_COT
            Case "E" : cor = System.Convert.ToUInt32(HexToDecimal(COR_ECF)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "V" : cor = System.Convert.ToUInt32(HexToDecimal(COR_VERIFICADO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "F" : cor = System.Convert.ToUInt32(HexToDecimal(COR_CONFERIDO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))

        End Select
        Return cor
    End Function

    Private Function buscarCor(cor As UInteger, artigo As String, artigodefault As String) As UInteger
        If cor = COR_BRANCO Then
            If artigo = artigodefault Then
                cor = System.Convert.ToUInt32(HexToDecimal(COR_ARTIGODEFAULT))
            End If
        End If
        Return cor
    End Function
    Private Function GetHexColor(colorObj As System.Drawing.Color) As String
        Return "#" & Hex(colorObj.R) & Hex(colorObj.G) & Hex(colorObj.B)
    End Function


    Public Function HexToDecimal(ByVal HexString As String) As Integer
        Dim HexColor As Char() = HexString.ToCharArray()
        Dim DecimalColor As Integer = 0
        Dim iLength As Integer = HexColor.Length - 1
        Dim iDecimalNumber As Integer

        Dim cHexValue As Char
        For Each cHexValue In HexColor
            If Char.IsNumber(cHexValue) Then
                iDecimalNumber = Integer.Parse(cHexValue.ToString())
            Else
                iDecimalNumber = Convert.ToInt32(cHexValue) - 55
            End If

            DecimalColor += iDecimalNumber * (Convert.ToInt32(Math.Pow(16, iLength)))
            iLength -= 1
        Next cHexValue
        Return DecimalColor
    End Function

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional inserirArigoVazio As Boolean = False)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim editavel As Boolean
        editavel = True
        Dim cor As UInteger


        If dtRow("Artigo").ToString() = "" And dtRow("Id").ToString() = "" Then Exit Sub

        If Not m.artigoExiste(dtRow("Artigo").ToString()) And Not inserirArigoVazio Then
            Exit Sub
        End If

        cor = buscarCor(dtRow("Estado").ToString())

        record = grid.Records.Add()

        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo").ToString())
        Item.BackColor = cor
        Item.Editable = editavel
        '  If descricao = "" Then
        Item = record.AddItem(dtRow("Descricao").ToString())
        ' Else
        'Item = record.AddItem(descricao)
        'End If
        Item.Editable = editavel
        Item.BackColor = cor
        Dim valor As Double
        Dim checked As Boolean

        'If Not IsDBNull(dtRow("Quantidade")) And Not IsNumeric((dtRow("Quantidade"))) Then
        '    Item.Value = Item.Value + " (" + dtRow("Quantidade").ToString() + ")"
        '    Item.Caption = Item.Value '+ " (" + dtRow("Quantidade").ToString() + ")"
        'End If

        valor = NuloToDouble(dtRow("Quantidade"))
        If valor < 0 Then valor = valor * -1

        Item = record.AddItem(valor)
        Item.Editable = editavel
        Item.BackColor = cor

        Dim lista As StdBELista
        Dim valoresVazio As Boolean

        lista = m.BuscarUltimosDadosArtigo(dtRow("Artigo").ToString(), dtRow("ID").ToString())

        If Not lista Is Nothing Then
            valoresVazio = lista.NumLinhas = 0
        End If

        If Not valoresVazio Then

            valor = NuloToDouble(lista.Valor("StkActual"))
            If valor < 0 Then valor = valor * -1

            Item = record.AddItem(valor)

            Item.Editable = editavel
            Item.BackColor = cor

            valor = NuloToDouble(lista.Valor("PCUltimo"))
            If valor < 0 Then valor = valor * -1
            Item = record.AddItem(valor)
            Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor

            If Not grelhaPai(grid) Then
                valor = NuloToDouble(dtRow("PrecUnit"))
                If valor < 0 Then valor = valor * -1
            End If

            Item = record.AddItem(IIf(grelhaPai(grid), NuloToString(lista.Valor("UltimoFornecedor")), valor))
            If Not grelhaPai(grid) Then Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            checked = Not IsDBNull(dtRow("CDU_DataRecepcao"))
            Item = record.AddItem(IIf(grelhaENC(grid), NuloToDate(dtRow("CDU_DataRecepcao")), ""))
            Item.Editable = editavel
            Item.BackColor = cor

        Else
            Item = record.AddItem("0")
            '   Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            Item = record.AddItem("0")
            Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            Item = record.AddItem(IIf(grelhaPai(grid), "", "0"))
            If Not grelhaPai(grid) Then Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            Item = record.AddItem("")
            Item.Editable = editavel
            Item.BackColor = cor

        End If
        Item.Tag = dtRow("Estado").ToString()
        If m.FuncionarioAdm Then
            Item = record.AddItem("")
            Item.HasCheckbox = True
            Item.Checked = checked
            Item.BackColor = cor
        End If




    End Sub



    Private Sub inserirLinhaRegistoEnc(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional inserirArigoVazio As Boolean = False)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim editavel As Boolean
        editavel = True
        Dim cor As UInteger


        If dtRow("Artigo").ToString() = "" And dtRow("Id").ToString() = "" Then Exit Sub

        If Not m.artigoExiste(dtRow("Artigo").ToString()) And Not inserirArigoVazio Then
            Exit Sub
        End If

        cor = buscarCor(dtRow("Estado").ToString())

        record = grid.Records.Add()

        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo").ToString())
        Item.BackColor = cor
        Item.Editable = editavel
        Item = record.AddItem(dtRow("Descricao").ToString())

        Item.Editable = editavel
        Item.BackColor = cor
        Dim valor As Double
        Dim valor1 As Double
        Dim checked As Boolean

        Dim lista As StdBELista
        Dim valoresVazio As Boolean

        lista = m.BuscarUltimosDadosArtigo(dtRow("Artigo").ToString(), dtRow("ID").ToString())

        If Not lista Is Nothing Then
            valoresVazio = lista.NumLinhas = 0
        End If

        If Not valoresVazio Then

            valor = NuloToDouble(lista.Valor("PCUltimo"))
            If valor < 0 Then valor = valor * -1
            Item = record.AddItem(valor)
            Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor

            valor = NuloToDouble(lista.Valor("StkActual"))
            '  If valor < 0 Then valor = valor * -1
            Item = record.AddItem(valor)
            Item.Editable = editavel
            Item.BackColor = cor


            Item.BackColor = cor
            valor = NuloToDouble(dtRow("Quantidade"))
            If valor < 0 Then valor = valor * -1
            Item = record.AddItem(valor)
            Item.Editable = editavel
            Item.BackColor = cor


            valor1 = NuloToDouble(lista.Valor("QuantidadeEnt"))
            If valor1 < 0 Then valor = valor * -1
            Item = record.AddItem(valor1)
            Item.BackColor = cor
            Item.Editable = editavel


            valor = valor - valor1
            If valor < 0 Then valor = 0
            Item = record.AddItem(valor)
            Item.BackColor = cor
            Item.Editable = editavel


            valor = NuloToDouble(dtRow("PrecUnit"))
            If valor < 0 Then valor = valor * -1

            Item = record.AddItem(valor)
            Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor

            checked = Not IsDBNull(dtRow("CDU_DataRecepcao"))
            Item = record.AddItem(IIf(grelhaENC(grid), NuloToDate(dtRow("CDU_DataRecepcao")), ""))
            Item.Editable = editavel
            Item.BackColor = cor

        Else
            Item = record.AddItem("0")
            '   Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            Item = record.AddItem("0")
            Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            Item = record.AddItem(IIf(grelhaPai(grid), "", "0"))
            If Not grelhaPai(grid) Then Item.Caption = Formatar(Item.Value)
            Item.Editable = editavel
            Item.BackColor = cor
            Item = record.AddItem("")
            Item.Editable = editavel
            Item.BackColor = cor

        End If
        Item.Tag = dtRow("Estado").ToString()
        If m.FuncionarioAdm Then
            Item = record.AddItem("")
            Item.HasCheckbox = True
            Item.Checked = False
            Item.BackColor = cor
        End If




    End Sub

    Private Function grelhaPai(grid As AxXtremeReportControl.AxReportControl) As Boolean
        Return grid.Name = "gridArtigos"
    End Function

    Private Function grelhaENC(grid As AxXtremeReportControl.AxReportControl) As Boolean
        Return grid.Name = "gridArtigosENC"
    End Function

    Public Function NuloToString(ByVal obj) As String
        Dim strOjb As String
        strOjb = obj.ToString()
        If IsDBNull(obj) Then
            NuloToString = ""
        Else
            If strOjb = "NULL" Then
                NuloToString = ""
            Else
                NuloToString = CStr(obj)
            End If
        End If
    End Function


    Public Function NuloToDate(ByVal obj) As Date
        If IsDBNull(obj) Then
            NuloToDate = CDate(Now.Date.ToShortDateString)
        Else
            If Not IsDate(obj) Then
                NuloToDate = CDate(Now.Date.ToShortDateString)
            Else
                NuloToDate = CDate(CDate(obj).ToShortDateString)
            End If
        End If
    End Function



    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                Try
                    obj = Evaluer(obj)
                    If IsNumeric(obj) Then
                        NuloToDouble = CDbl(obj)
                    Else
                        NuloToDouble = 0
                    End If
                Catch ex As Exception
                    NuloToDouble = 0
                End Try
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Function Evaluer(ByVal Txt As String) As String
        Dim i As Integer, oNB As Integer, fNB As Integer
        Dim P1 As Integer, P2 As Integer
        Dim Buff As String
        Dim T As String
        'Pour les calculs y faut un point à la place de la virgule
        Txt = Replace(Txt, ",", ".")
        'Voir s'il y a des (
        For i = 1 To Len(Txt)
            If Mid(Txt, i, 1) = "(" Then oNB = oNB + 1
        Next i
        'S'il y a des ( (ouvrantes), voir si elle sont validée par  des ) (fermantes)
        If oNB > 0 Then
            For i = 1 To Len(Txt)
                If Mid(Txt, i, 1) = ")" Then fNB = fNB + 1
            Next i
        Else
            'Pas de parenthèse, Evalue directement le calcul
            Evaluer = EvalueExpression(Txt)
            Exit Function
        End If
        If oNB <> fNB Then
            Evaluer = ""
            'Les parenthèses ne sont pas concordantes, mettre  message erreur parenthèse
            Exit Function
        End If

        While oNB > 0
            'recherche la dernière parenthèse ouvrante
            P1 = InStrRev(Txt, "(")
            'Recherche la parenthèse fermante de l'expression
            P2 = InStr(Mid(Txt, P1 + 1), ")")
            'Evalue l'expression qui est entre parenthèses
            Buff = EvalueExpression(Mid(Txt, P1 + 1, P2 - 1))
            'Remplacer l'expression par le résultat et supprimer les parenthèses
            Txt = Mid(Txt, P1 - 1) & Buff & Mid(Txt, P1 + P2 + 1)
            oNB = oNB - 1
        End While
        'plus de parenthèse, évaluer la dernière expression
        Evaluer = EvalueExpression(Txt)

    End Function
    Function EvalueExpression(a As String) As String
        Dim T As Integer, S As Integer
        Dim B As String, i As Integer, C As Boolean
        Dim c1 As Double, c2 As Double, Signe As Integer
        Dim R As String, Fin As Boolean, z As Integer

        'enlever les espace
        a = Replace(a, " ", "")

        While Not Fin
            For i = 1 To Len(a)
                T = Asc(Mid(a, i, 1))
                If T < 48 And T <> 46 Or i = Len(a) Then
                    If C Then 'évalue
                        If i = Len(a) Then
                            c2 = Val(Mid(a, S))
                        Else
                            c2 = Val(Mid(a, S, i - S))
                        End If
                        R = Str(CalculSimple(c1, c2, Signe))
                        If i = Len(a) Then
                            Fin = True
                        Else
                            a = Trim(R & Mid(a, i))
                            C = False
                        End If
                        Exit For
                    Else 'sépare le 1er chiffre
                        c1 = Val(Mid(a, i - 1))
                        Signe = T
                        S = i + 1
                        C = True
                    End If
                End If
            Next i
        End While
        'remplacer l'expression par le résultat
        EvalueExpression = Trim(R)
    End Function

    Function CalculSimple(n1 As Double, n2 As Double, Signe As Integer) As Double
        Select Case Signe
            Case 43 ' +
                CalculSimple = n1 + n2
            Case 45 ' -
                CalculSimple = n1 - n2
            Case 42 ' *
                CalculSimple = n1 * n2
            Case 47 ' /
                CalculSimple = n1 / n2
                'Ici, ajouter d'autre calcul...
        End Select
    End Function


    Private Sub inserirLinhaRegisto(Id As String, entidade As String, nome As String, Data As Date, serie As String, numdoc As Long, ByVal grid As AxXtremeReportControl.AxReportControl, Optional descricao As String = "")
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim count As String
        Dim cor As String
        Dim conf As String

        cor = buscarCor(ESTADO_NORMAL)
        If grid.Tag = "ENC" Then
            conf = LCase(m.consultaValor("SELECT CDU_Conferido FROM CabecCompras where id='" + Id + "'"))

            count = m.consultaValor("SELECT Count(*) FROM LinhasCompras WHERE IdCabecCompras='" + Id + "' and ISNULL(CDU_ObraN5,'')<>'" + ESTADO_VER + "'")
            If count = "0" Then cor = buscarCor(ESTADO_VER)
            If conf = "true" Then cor = buscarCor(ESTADO_CONF)
        End If

        If grid.Tag = "COT" Then
            count = m.consultaValor("SELECT Count(*) FROM LinhasCompras WHERE IdCabecCompras='" + Id + "' and ISNULL(CDU_ObraN5,'')<>'" + ESTADO_ECF + "'")
            If count = "0" Then cor = buscarCor(ESTADO_ECF)
        End If

        count = m.consultaValor("SELECT Count(*) FROM CabecComprasStatus WHERE IdCabecCompras='" + Id + "' AND (Anulado=1)")
        If count <> "0" Then cor = buscarCor(ESTADO_PROBLEMAS)

        record = grid.Records.Add()
        record.Tag = Id
        Item = record.AddItem(entidade)
        Item.BackColor = cor
        Item = record.AddItem(nome)
        Item.BackColor = cor
        Item = record.AddItem(Data.ToString("dd-MM-yyyy"))
        Item.BackColor = cor

        Item = record.AddItem(numdoc)
        Item.BackColor = cor

        If grid.Tag = "ENC" Then
            Item = record.AddItem("")
            Item.HasCheckbox = True
            Item.Checked = conf = "true"
            ' Item.Editable = conf <> "true"
            Item.BackColor = cor
        End If
    End Sub

    Private Function isEditable(m As MotorGE, dtRow As DataRow) As Boolean
        If dtRow("ID").ToString = "" Then Return True
        Return m.consultaValor("SELECT ID from LinhasCompras where idLinhaOrigemCopia='" + dtRow("ID").ToString + "'") = "" And m.FuncionarioAdm
    End Function

    'Private Function buscarPrecoArtigo(artigo As String) As Double
    '    Dim m As MotorGE
    '    m = MotorGE.GetInstance
    '    Return m.buscarPrecoArtigo(artigo)
    'End Function
    'Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

    '    buscarArtigos(gridArtigos, txtArtigo.Text)

    'End Sub



    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case 1 : actualizar(True, True)
            'Case 2 : gravarDocumento(0)
            '    '   Case 3 : enviarEmailDocumento()
            Case 4 : abrirEditorRegistos()
            Case 5 : If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then Me.Close()
            Case ID_INSERIRLINHA : inserirLinhaRegisto()
            Case ID_APAGARLINHA : apagarLinhaRegisto()
            Case ID_ARTIGOINTERNO : actualizarEstadoLinha(ESTADO_INTERNO)
            Case ID_ARTIGOPROBLEMA : actualizarEstadoLinha(ESTADO_PROBLEMAS)
            Case ID_ARTIGONORMAL : actualizarEstadoLinha(ESTADO_NORMAL)
            Case ID_ARTIGOENCOMENDADO : actualizarEstadoLinha(ESTADO_ECF)
            Case ID_REABRIR : actualizarEstadoDocumento(True)
            Case ID_ANULAR : actualizarEstadoDocumento(False)
        End Select
    End Sub
    Private Sub gravarDocumento(apresentaPergunta As Boolean, botao As Integer, Optional fornecedor As String = "", Optional eparaactualizar As Boolean = True)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim idDoc As String
        idDoc = ""
        Dim docOrig As String
        docOrig = buscarIdDocumento(tabCtlMaterial.SelectedIndex, botao)



        If modulo = "C" Then idDoc = gravarDocumentoCompra(apresentaPergunta, docOrig, tabCtlMaterial.SelectedIndex, botao, buscarTipoDocumento(tabCtlMaterial.SelectedIndex, botao), fornecedor)
        If modulo = "V" Then idDoc = gravarDocumentoVenda(apresentaPergunta, docOrig)

        If botao = 2 And idDoc <> "" And docOrig = "" And m.InformModLstMaterial.EMAILECF.Trim() <> "" Then
            '  If MsgBox("Deseja Enviar email ás Compras?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            m.enviarEmailDocumento(idDoc, "C", m.InformModLstMaterial.EMAILECF)
            Me.Cursor = Cursors.Default
            'End If
        End If

        If botao = 0 And idDoc <> "" Then
            gridArtigos.Tag = idDoc
        End If
    End Sub

    Private Function buscarIdDocumento(index As Integer, botao As Integer) As String
        buscarIdDocumento = ""

        If botao = index Then
            Select Case index
                Case 0 : buscarIdDocumento = gridArtigos.Tag
                Case 1 : buscarIdDocumento = gridArtigosCOT.Tag
                Case 2 : buscarIdDocumento = gridArtigosENC.Tag
            End Select
        End If

    End Function


    Private Sub actualizar(Optional ByVal pergunta As Boolean = False, Optional limpaProjecto As Boolean = False, Optional limpatudo As Boolean = True)
        Dim m As MotorGE
        m = MotorGE.GetInstance

        listaFornecedores = New ArrayList
        If Not limpatudo Then
            inicializarFornecedores()
            selecionarTodos(False, gridArtigosCOT)
            selecionarTodos(False, gridArtigosENC)
            selecionarTodos(False, gridArtigos)
            gridArtigosCOT.Populate()
            gridArtigosENC.Populate()
            gridArtigos.Populate()
            Exit Sub
        End If

        If pergunta Then
            If MsgBox("Deseja realmente actualizar e limpar a informação existente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        End If

        If limpaProjecto Then
            dtpData.Value = Now
            If modulo = "C" Then
                txtNumDoc.Value = m.daNumeracaoDocumentoCompra(m.InformModLstMaterial.TIPODOCLM)
                'Else
                '    txtNumDoc.Value = m.daNumeracaoDocumentoVenda(m.InformModLstMaterial.TIPODOCLM)
            End If
        End If

        doc = Nothing


        txtFornecedor.Text = ""
        gridArtigosCOT.Tag = ""
        gridArtigosENC.Tag = ""
        gridArtigos.Tag = ""

        ' gridArtigos.Records.DeleteAll()
        gridArtigosCOT.Records.DeleteAll()
        gridArtigosCOT.Populate()
        gridDocsCOT.Records.DeleteAll()
        gridDocsCOT.Populate()

        gridArtigosENC.Records.DeleteAll()
        gridArtigosENC.Populate()
        gridDocsENC.Records.DeleteAll()
        gridDocsENC.Populate()

        gridArtigos.Records.DeleteAll()
        gridArtigos.Populate()

        inicializarFornecedores()

        txtObservacoesLST.Text = ""
        txtObservacoesCOT.Text = ""
        txtObservacoesENC.Text = ""

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"


    End Sub



    Private Function gravarDocumentoCompra(ByVal apresentaPergunta As Boolean, idCabecDoc As String, index As Integer, botao As Integer, Optional tipodoc As String = "", Optional fornecedor As String = "") As String
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim descricaoDocumento As String = ""
        Dim segundoDoc As Boolean
        segundoDoc = False
        If tipodoc = "" Then
            Exit Function
            ' Else
            '   segundoDoc = True
        End If

        If apresentaPergunta Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")
            If MsgBox("Deseja realmente gravar a " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        Me.Cursor = Cursors.WaitCursor

        Dim forn As Fornecedor
        forn = carregarDadosFornecedor(fornecedor)

        Dim txt As String
        txt = ""
        Select Case tabCtlMaterial.SelectedIndex
            Case 0 : txt = txtObservacoesLST.Text
            Case 1 : txt = txtObservacoesCOT.Text
            Case 2 : txt = txtObservacoesENC.Text
        End Select

        If botao = 0 Then
            txt = ""
        End If


        Dim conferido As Boolean
        Dim dataRecepcao As String
        If tabCtlMaterial.SelectedIndex = 2 Then
            conferido = gridDocsENC.SelectedRows(0).Record(4).Checked
            dataRecepcao = IIf(dtpDataFatura.Checked, dtpDataFatura.Value, "")
        Else
            conferido = False
            dataRecepcao = ""
        End If

        'idCabecDoc = m.actualizarDocumentoCompra(forn, tipodoc, m.Funcionario, txtProjecto.Text, txt, buscarLinhasArtigosSeleccionados(index, botao), IIf(dtpDataRecepcao.Checked, dtpDataRecepcao.Value, ""), idCabecDoc, botao, index, IIf(tabCtlMaterial.SelectedIndex = 2, gridDocsENC.SelectedRows(0).Record(4).Checked, False))
        idCabecDoc = m.actualizarDocumentoCompra(forn, tipodoc, m.Funcionario, txt, getIdCabecdoc(), buscarLinhasArtigosSeleccionados(index, botao), dataRecepcao, idCabecDoc, botao, index, conferido)
        'idCabecDoc = m.actualizarDocumentoCompra(forn, tipodoc, m.Funcionario, txtProjecto.Text, txt, buscarLinhasArtigosSeleccionados(index, botao), IIf(dtpDataRecepcao.Checked, dtpDataRecepcao.Value, ""), idCabecDoc, botao, index, False)

        'If tabCtlMaterial.SelectedIndex = 2 And idCabecDoc <> "" Then
        '    Dim count As String
        '    count = m.consultaValor("SELECT Count(*) FROM LinhasCompras WHERE IdCabecCompras='" + idCabecDoc + "' and ISNULL(CDU_ObraN5,'')<>'" + ESTADO_VER + "'")
        '    If count = "0" Then
        '        m.executarComando("UPDATE CabecCompras set CDU_CabVar2='" + FormatDateTime(Now, DateFormat.ShortDate) + "'where CDU_CabVar2 is null and id='" + idCabecDoc + "'")
        '    End If
        'End If

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" And apresentaPergunta And segundoDoc = False Then
            MsgBox(descricaoDocumento + " gravada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Me.Cursor = Cursors.Default

        Return idCabecDoc
    End Function

    Private Function buscarLinhasArtigosSeleccionados(index As Integer, botao As Integer) As Object
        Dim lista As ArrayList
        lista = New ArrayList

        Dim grid As AxXtremeReportControl.AxReportControl

        Select Case index
            ' Lista material
            Case 0 : grid = gridArtigos
            ' pedido cotacao
            Case 1 : grid = gridArtigosCOT
            ' encomenda
            Case 2 : grid = gridArtigosENC

        End Select


        If botao = 0 Then Return grid.Records

        If index = botao Then Return grid.Records
        Dim row As ReportRecord

        For Each row In grid.Records
            If row(COLUMN_CHECKBOX).Checked Then
                lista.Add(row)
            End If
        Next

        Return lista


    End Function

    Private Function carregarDadosFornecedor(Optional fornecedor As String = "") As Fornecedor
        Dim f As Fornecedor
        Dim m As MotorGE
        m = MotorGE.GetInstance

        f = New Fornecedor

        If fornecedor = "" Then
            f.Fornecedor = m.getEntidadeDefault
        Else
            f.Fornecedor = fornecedor
        End If

        Return f
    End Function

    Private Function gravarDocumentoVenda(ByVal apresentaPergunta As Boolean, idCabecDoc As String, Optional tipodoc As String = "") As String
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim descricaoDocumento As String
        Dim segundoDoc As Boolean
        segundoDoc = False
        If tipodoc = "" Then
            'tipodoc = lstDocumentos.SelectedValue
        Else
            segundoDoc = True
        End If

        descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosVenda WHERE Documento='" + tipodoc + "'")

        If apresentaPergunta Then
            If MsgBox("Deseja realmente gravar a " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return ""
            End If
        End If

        Me.Cursor = Cursors.WaitCursor

        Dim registo As New Registo

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" And apresentaPergunta And segundoDoc = False Then
            MsgBox(descricaoDocumento + " gravada realidada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Me.Cursor = Cursors.Default

        Return idCabecDoc
    End Function


    Private Sub abrirEditorRegistos()
        Dim f As FrmGestEncomendasRegistos
        f = New FrmGestEncomendasRegistos
        Dim m As MotorGE
        m = MotorGE.GetInstance
        f.TipoDocumento = m.InformModLstMaterial.TIPODOCLM
        f.ShowDialog()
        If f.clicouConfirmar Then
            emModoEdicao = False
            actualizarFormulario(f.getIdLinha)
            'actualizarFormularioLM(f.getIdLinha)
            emModoEdicao = False
        End If
    End Sub



    Dim emModoEdicao As Boolean
    Private Sub actualizarFormularioLM(ByVal id As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigos.Tag = id
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim entidade As String
        Dim nome As String
        Dim observacoes As String
        Dim str As String()
        entidade = ""
        nome = ""
        observacoes = ""

        If modulo = "C" Then
            doc = m.buscarDocumentoCompra(id)
        Else
            doc = m.buscarDocumentoVenda(id)
        End If

        ' txtProjecto.Text = m.daProjectoCodigo(doc.IDObra)
        '  txtNomeProjecto.Text = m.daProjectoNome(doc.IDObra)

        str = Split(doc.Observacoes, vbLf)
        txtObservacoesLST.Lines = str
        Dim dtRegistos As DataTable
        dtRegistos = Nothing

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(id, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(id, "V")

        Dim dtrow As DataRow
        gridArtigos.Records.DeleteAll()
        emModoEdicao = True

        txtNumDoc.Value = doc.NumDoc
        dtpData.Value = doc.DataDoc

        For Each dtrow In dtRegistos.Rows
            'If lstFornecedor.SelectedValue Is Nothing Then
            '    If m.NuloToString(dtrow("Codigo")) <> "" Then lstFornecedor.SelectedValue = m.NuloToString(dtrow("Codigo"))
            'End If
            inserirLinhaRegisto(dtrow, gridArtigos)
        Next
        emModoEdicao = False
        txtArtigo.Text = ""

        gridArtigos.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub actualizarFormularioCOT(ByVal id As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosCOT.Tag = id
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim entidade As String
        Dim nome As String
        Dim observacoes As String
        Dim str As String()
        entidade = ""
        nome = ""
        observacoes = ""

        If modulo = "C" Then
            doc = m.buscarDocumentoCompra(id)
        Else
            doc = m.buscarDocumentoVenda(id)
        End If

        'm.daEntidadeDocumento(id, entidade, nome, observacoes, "C")
        ' txtProjecto.Text = m.daProjectoCodigo(doc.IDObra)
        ' txtNomeProjecto.Text = m.daProjectoNome(doc.IDObra)

        'str = Split(observacoes, vbLf)
        str = Split(doc.Observacoes, vbLf)
        txtObservacoesCOT.Lines = str
        Dim dtRegistos As DataTable
        dtRegistos = Nothing

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(id, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(id, "V")

        ' dt = m.daDataDocumento(id, dtpData.Value, "C")

        Dim dtrow As DataRow
        gridArtigosCOT.Records.DeleteAll()
        gridArtigos.Records.DeleteAll()
        emModoEdicao = True
        For Each dtrow In dtRegistos.Rows
            If lstFornecedor.SelectedValue Is Nothing Then
                If m.NuloToString(dtrow("Codigo")) <> "" Then lstFornecedor.SelectedValue = m.NuloToString(dtrow("Codigo"))
            End If
            inserirLinhaRegisto(dtrow, gridArtigos)
        Next
        emModoEdicao = False
        txtArtigo.Text = ""

        'buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        gridArtigos.Populate()
        gridArtigosCOT.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"

        Me.Cursor = Cursors.Default
        '   MsgBox(buscarNomeDocumento(tabCtlMaterial.SelectedIndex, 0) + "em modo de edição", MsgBoxStyle.Information)
    End Sub


    Private Sub actualizarFormularioECF(ByVal id As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosENC.Tag = id
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim entidade As String
        Dim nome As String
        Dim observacoes As String
        Dim str As String()
        entidade = ""
        nome = ""
        observacoes = ""

        If modulo = "C" Then
            doc = m.buscarDocumentoCompra(id)
        Else
            doc = m.buscarDocumentoVenda(id)
        End If

        'm.daEntidadeDocumento(id, entidade, nome, observacoes, "C")
        'txtProjecto.Text = m.daProjectoCodigo(doc.IDObra)
        'txtNomeProjecto.Text = m.daProjectoNome(doc.IDObra)

        'str = Split(observacoes, vbLf)
        str = Split(doc.Observacoes, vbLf)
        txtObservacoesENC.Lines = str
        Dim dtRegistos As DataTable
        dtRegistos = Nothing

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(id, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(id, "V")

        ' dt = m.daDataDocumento(id, dtpData.Value, "C")

        Dim dtrow As DataRow
        gridArtigosCOT.Records.DeleteAll()
        gridArtigos.Records.DeleteAll()
        emModoEdicao = True
        For Each dtrow In dtRegistos.Rows
            If lstFornecedor.SelectedValue Is Nothing Then
                If m.NuloToString(dtrow("Codigo")) <> "" Then lstFornecedor.SelectedValue = m.NuloToString(dtrow("Codigo"))
            End If
            inserirLinhaRegisto(dtrow, gridArtigos)
        Next
        emModoEdicao = False
        txtArtigo.Text = ""

        'buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        gridArtigos.Populate()
        gridArtigosCOT.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"

        Me.Cursor = Cursors.Default

    End Sub

    Private Function buscarTipoDocumento(index As Integer) As String
        Dim strDoc As String
        strDoc = tabCtlMaterial.TabPages(index).Tag
        Return strDoc.Split(";")(0)
    End Function

    Private Function buscarTipoDocumento(index As Integer, botao As Integer) As String
        Dim strDoc As String
        strDoc = tabCtlMaterial.TabPages(botao).Tag
        Return strDoc.Split(";")(0)
    End Function

    Private Function buscarNomeDocumento(index As Integer, botao As Integer) As String
        Dim strDoc As String
        strDoc = tabCtlMaterial.TabPages(botao).Tag
        Return strDoc.Split(";")(1)
    End Function

    Private Sub enviarEmailDocumento(index As Integer, botao As Integer, Optional fornecedor As String = "")
        Dim idDocumento As String
        Dim m As MotorGE
        m = MotorGE.GetInstance
        idDocumento = ""
        Dim idDocOrig As String
        idDocOrig = buscarIdDocumento(index, botao)


        Me.Cursor = Cursors.WaitCursor
        If modulo = "C" Then idDocumento = gravarDocumentoCompra(False, idDocOrig, index, botao, buscarTipoDocumento(index, botao), fornecedor) 'gravarDocumentoCompra(False, buscarIdDocumento(index, botao), buscarTipoDocumento(index, botao), fornecedor)
        If modulo = "V" Then idDocumento = gravarDocumentoVenda(False, idDocOrig)
        If idDocumento <> "" Then
            m.enviarEmailDocumento(idDocumento, "C")
            If botao = 2 And idDocOrig = "" And m.InformModLstMaterial.EMAILECF <> "" Then
                '  If MsgBox("Deseja Enviar email ás Compras?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                m.enviarEmailDocumento(idDocumento, "C", m.InformModLstMaterial.EMAILECF)
                'End If
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub imprimirDoc(index As Integer, botao As Integer, Optional fornecedor As String = "")
        Dim idDocumento As String
        Dim m As MotorGE
        m = MotorGE.GetInstance
        idDocumento = ""
        Dim idDocOrig As String
        idDocOrig = buscarIdDocumento(index, botao)
        Me.Cursor = Cursors.WaitCursor
        If modulo = "C" Then idDocumento = gravarDocumentoCompra(False, idDocOrig, index, botao, buscarTipoDocumento(index, botao), fornecedor)
        If modulo = "V" Then idDocumento = gravarDocumentoVenda(False, idDocOrig, fornecedor)
        If idDocumento <> "" Then
            m.imprimirDocumento(idDocumento, "C", Me)


            If botao = 2 And idDocOrig = "" And m.InformModLstMaterial.EMAILECF <> "" Then
                ' If MsgBox("Deseja Enviar email ás Compras?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                m.enviarEmailDocumento(idDocumento, "C", m.InformModLstMaterial.EMAILECF)
                'End If
            End If

        End If
        Me.Cursor = Cursors.Default
    End Sub


    'Private Sub lstDocumentos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim m As MotorGE
    '    m = MotorGE.GetInstance
    '    Dim dtRow As DataRowView = lstDocumentos.SelectedItem
    '    If dtRow Is Nothing Then Exit Sub
    '    modulo = dtRow("Modulo")
    '    If dtRow("Modulo") = "C" Then
    '        lblFornecedor.Text = "Fornecedor"
    '        txtNumDoc.Minimum = m.daValorNumeracaoMinimaDocumentoCompra(lstDocumentos.SelectedValue)
    '        txtNumDoc.Maximum = m.daValorNumeracaoMaximaDocumentoCompra(lstDocumentos.SelectedValue)
    '        txtNumDoc.Value = m.daNumeracaoDocumentoCompra(lstDocumentos.SelectedValue)
    '    Else
    '        lblFornecedor.Text = "Cliente"
    '        txtNumDoc.Minimum = m.daValorNumeracaoMinimaDocumentoVenda(lstDocumentos.SelectedValue)
    '        txtNumDoc.Maximum = m.daValorNumeracaoMaximaDocumentoVenda(lstDocumentos.SelectedValue)
    '        txtNumDoc.Value = m.daNumeracaoDocumentoVenda(lstDocumentos.SelectedValue)
    '    End If
    '    actualizar(False)
    'End Sub


    Private Sub txtArtigo_KeyDownEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyDownEvent) Handles txtArtigo.KeyDownEvent
        If e.keyCode = Keys.Enter Then
            filtrarArtigos(gridArtigos, txtArtigo.Text) '  buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub

    'Private Sub txtArtigo_Change(sender As Object, e As EventArgs) Handles txtArtigo.Change
    '    If txtArtigo.Text = "" Then
    '        buscarArtigos(gridArtigos, txtArtigo.Text)
    '    End If
    'End Sub
    Private Sub filtrarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim row As ReportRecord
        pesquisa = Trim(pesquisa)
        Me.Cursor = Cursors.WaitCursor
        For Each row In grid.Records
            row.Visible = True
            If pesquisa <> "" Then
                If InStr(LCase(row(COLUMN_ARTIGO).Value), LCase(pesquisa)) = 0 And InStr(LCase(row(COLUMN_DESCRICAO).Value), LCase(pesquisa)) = 0 And InStr(LCase(row(COLUMN_FORNPRECO).Value), LCase(pesquisa)) = 0 And InStr(LCase(row(COLUMN_ULTPRECO).Value), LCase(pesquisa)) = 0 Then
                    row.Visible = False
                End If
            End If
        Next
        grid.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub aplicarFiltro()
        filtrarArtigos(gridArtigos, txtArtigo.Text)
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click

        aplicarFiltro()
    End Sub

    'Private Sub btnEnviar_Click(sender As Object, e As EventArgs)
    '    Dim row As DataRowView
    '    If tabCtlMaterial.SelectedIndex = 0 Then
    '        For Each row In lstFornecedor.CheckedItems
    '            enviarEmailDocumento(row("Codigo"))
    '        Next
    '    End If
    '    actualizar(False, True)
    '    'enviarEmailDocumento()
    'End Sub

    Private Function buscarListaFornecedores(index As Integer) As ArrayList
        Dim lista As ArrayList
        lista = New ArrayList

        If index = 0 Then
            For Each row In lstFornecedor.CheckedItems
                lista.Add(row("Codigo"))
            Next
        Else
            If gridDocsCOT.SelectedRows.Count > 0 Then
                lista.Add(gridDocsCOT.SelectedRows(0).Record(0).Value)
            End If
        End If

        Return lista
    End Function


    Private Function apresentarPergunta(tipo As Integer, index As Integer, botao As Integer) As Boolean
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim descricaoDocumento As String
        If tipo = 0 Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + buscarTipoDocumento(index, botao) + "'")
            If MsgBox("Deseja realmente gravar o documento " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            End If
            Return True
        End If

        If tipo = 1 Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + buscarTipoDocumento(index, botao) + "'")
            If MsgBox("Deseja realmente imprimir o documento " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            End If
            Return True
        End If

        If tipo = 2 Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + buscarTipoDocumento(index, botao) + "'")
            If MsgBox("Deseja realmente enviar  o documento " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            End If
            Return True
        End If

        Return True

    End Function


    Private Function validarGravacao(botao As Integer) As Boolean
        Dim m As MotorGE
        m = MotorGE.GetInstance
        'If Not m.existeDocumentoCompra(buscarIdDocumento(tabCtlMaterial.SelectedIndex, botao)) Then
        '    MsgBox("Tem de indicar um Projecto", MsgBoxStyle.Critical)
        '    Return False
        'End If

        If botao = 1 Then
            If Not existeArtigosSeleccionados(tabCtlMaterial.SelectedIndex) Then
                MsgBox("Não existem artigos seleccionados!", MsgBoxStyle.Critical)
                Return False
            End If
        End If

        If tabCtlMaterial.SelectedIndex = 0 And botao = 1 Then
            If Not existeFornecedoresSeleccionados() Then
                MsgBox("Não existem fornecedores seleccionados!", MsgBoxStyle.Critical)
                Return False
            End If
        End If

        If botao <> 0 Then
            If existeArtigosSeleccionadosemGravacao(tabCtlMaterial.SelectedIndex) Then
                MsgBox("Não existem artigos seleccionados que ainda não foram gravados! Favor de gravar primeiro o documento!", MsgBoxStyle.Critical)
                Return False
            End If
        End If



        Return True

    End Function

    Private Function existeArtigosSeleccionados(indexTAB As Integer) As Boolean
        Dim grid As AxXtremeReportControl.AxReportControl
        grid = Nothing

        If indexTAB = 0 Then
            grid = gridArtigos
        End If

        If indexTAB = 1 Then
            grid = gridArtigosCOT
        End If

        Dim b As Boolean
        Dim row As ReportRecord
        b = False
        For Each row In grid.Records
            If row(COLUMN_CHECKBOX).Checked Then
                b = True
                Exit For
            End If
        Next
        Return b
    End Function


    Private Function existeArtigosSeleccionadosemGravacao(indexTAB As Integer) As Boolean
        Dim grid As AxXtremeReportControl.AxReportControl
        grid = Nothing

        If indexTAB = 0 Then
            grid = gridArtigos
        End If

        If indexTAB = 1 Then
            grid = gridArtigosCOT
        End If

        Dim b As Boolean
        Dim row As ReportRecord
        b = False
        For Each row In grid.Records
            Dim dt As DataRow
            dt = row.Tag
            If row(COLUMN_CHECKBOX).Checked And dt("Id").ToString() = "" Then
                b = True
                Exit For
            End If
        Next
        Return b
    End Function

    Private Function existeFornecedoresSeleccionados() As Boolean
        Return lstFornecedor.CheckedIndices.Count <> 0
    End Function
    Private Function buscarDescricaoDocumentos() As String
        Select Case tabCtlMaterial.SelectedIndex
            Case 0 : Return "o(s) pedido(s) de Cotação?"
            Case 1 : Return "a(s) encomendas(s) a fornecedor?"
        End Select

    End Function


    Private Sub ApresentarPerguntaCompra(idDocOrigem As String, tipoDocOrig As String)
        Dim tipodocDest As String
        Dim descricaotipodocDest As String
        Dim idDocumento As String
        Dim m As MotorGE
        m = MotorGE.GetInstance
        idDocumento = ""
        Dim refOrig As String
        tipodocDest = m.consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosCompra Where Documento='" + tipoDocOrig + "'")

        If tipodocDest <> "" Then
            descricaotipodocDest = m.consultaValor("SELECT Descricao FROM DocumentosCompra Where Documento='" + tipodocDest + "'")
            Dim frmP As FrmPergunta
            frmP = New FrmPergunta
            frmP.setTexto("Deseja criar o documento " + descricaotipodocDest + "?")
            frmP.ShowDialog()

            refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie +'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocOrigem + "'")

            idDocumento = m.consultaValor("SELECT id FROM CabecCompras WHERE refDocOrig='" + refOrig + "'")

            ''SOMENTE GRAVAR
            If frmP.getBotaoSeleccionado() = 0 Then
                idDocumento = gravarDocumentoCompra(False, idDocumento, 0, 0, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
                End If
            End If

            'IMPRIMIR
            If frmP.getBotaoSeleccionado() = 1 Then
                idDocumento = gravarDocumentoCompra(False, idDocumento, 0, 0, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
                    m.imprimirDocumento(idDocumento, "C", Me)
                End If
            End If

            'ENVIAR
            If frmP.getBotaoSeleccionado() = 2 Then
                idDocumento = gravarDocumentoCompra(False, idDocumento, 0, 0, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
                    m.enviarEmailDocumento(idDocumento, "C")
                End If
            End If
        End If

    End Sub

    Private Sub ApresentarPerguntaVenda(idDocOrigem As String, tipoDocOrig As String)
        Dim tipodocDest As String
        Dim descricaotipodocDest As String
        Dim idDocumento As String
        Dim m As MotorGE
        m = MotorGE.GetInstance
        idDocumento = ""
        Dim refOrig As String
        tipodocDest = m.consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosVenda Where Documento='" + tipoDocOrig + "'")

        If tipodocDest <> "" Then
            descricaotipodocDest = m.consultaValor("SELECT Descricao FROM DocumentosVenda Where Documento='" + tipodocDest + "'")
            Dim frmP As FrmPergunta
            frmP = New FrmPergunta
            frmP.setTexto("Deseja criar o documento " + descricaotipodocDest + "?")
            frmP.ShowDialog()

            refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie +'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocOrigem + "'")

            idDocumento = m.consultaValor("SELECT id FROM CabecDoc WHERE refDocOrig='" + refOrig + "'")

            ''SOMENTE GRAVAR
            If frmP.getBotaoSeleccionado() = 0 Then
                idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
                End If
            End If

            'IMPRIMIR
            If frmP.getBotaoSeleccionado() = 1 Then
                idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
                    m.imprimirDocumento(idDocumento, "V", Me)
                End If
            End If

            'ENVIAR
            If frmP.getBotaoSeleccionado() = 2 Then
                idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
                If idDocumento <> "" Then
                    actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
                    m.enviarEmailDocumento(idDocumento, "V")
                End If
            End If
        End If

    End Sub

    Private Sub actualizarReferenciasDocumentoCompra(idDocOrigem As String, idDocumento As String)
        Dim refOrig As String
        Dim refDest As String
        Dim m As MotorGE
        m = MotorGE.GetInstance

        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocOrigem + "'")
        refDest = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocumento + "'")

        m.executarComando("UPDATE CabecCompras set RefDocOrig='" + refDest + "' WHERE Id='" + idDocOrigem + "'")
        m.executarComando("UPDATE CabecCompras set RefDocOrig='" + refOrig + "' WHERE Id='" + idDocumento + "'")

    End Sub

    Private Sub actualizarReferenciasDocumentoVenda(idDocOrigem As String, idDocumento As String)
        Dim refOrig As String
        Dim refDest As String
        Dim m As MotorGE
        m = MotorGE.GetInstance

        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocOrigem + "'")
        refDest = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocumento + "'")

        m.executarComando("UPDATE CabecDoc set RefDocOrig='" + refDest + "' WHERE Id='" + idDocOrigem + "'")
        m.executarComando("UPDATE CabecDoc set RefDocOrig='" + refOrig + "' WHERE Id='" + idDocumento + "'")

    End Sub


    'Private Sub txtFornecedor_Change(sender As Object, e As EventArgs) Handles txtFornecedor.Change
    '    Dim nome As String
    '    Dim m As MotorGE
    '    m = MotorGE.GetInstance
    '    nome = ""
    '    If modulo = "C" Then nome = m.consultaValor("SELECT nome from Fornecedores where fornecedor like '" + txtFornecedor.Text + "'")
    '    If modulo = "V" Then nome = m.consultaValor("SELECT nome from Clientes where cliente like '" + txtFornecedor.Text + "'")

    '    txtNomeForn.Text = nome
    '    If nome <> "" Then
    '        If modulo = "C" Then actualizarDadosFornecedor(txtFornecedor.Text)
    '        If modulo = "V" Then actualizarDadosCliente(txtFornecedor.Text)
    '    End If
    'End Sub


    Private Sub btnListaArtigos_Click(sender As Object, e As EventArgs)
        abrirFormListaArtigos()
    End Sub

    Private Sub abrirFormListaArtigos()
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim frmLista As FrmLista
        frmLista = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(4)

        frmLista.setComando("SELECT TOP 100 PERCENT Codigo as 'Código',Descricao as 'Descrição' FROM COP_Obras WHERE ESTADO='ABTO' ORDER BY Codigo")
        frmLista.setCaption("Projectos")

        ' frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
        ' frmLista.setCaption("Fornecedores")
        frmLista.ShowFilter(True)
        frmLista.setalinhamentoColunas(alinhamentoColunas)
        frmLista.ShowDialog()

        If frmLista.seleccionouOK Then
            Dim id As String
            id = ""
            id = m.consultaValor("SELECT id from CabecCompras where Id '" + frmLista.getValor(0) + "'")

            If id <> "" Then
                actualizarDados()
                carregarDadosProjectoLM(frmLista.getValor(0))
                If m.FuncionarioAdm Then
                    carregarDadosProjectoCOT(frmLista.getValor(0))
                    carregarDadosProjectoECF(frmLista.getValor(0))
                End If
                MsgBox(buscarNomeDocumento(tabCtlMaterial.SelectedIndex, 0) + " em modo de edição", MsgBoxStyle.Information)
            Else
                actualizar()
            End If
        End If
    End Sub

    Private Sub actualizarDados()

    End Sub



    'Private Sub txtProjecto_Change(sender As Object, e As EventArgs)

    '    If emModoEdicao Then Exit Sub
    '    Dim nome As String
    '    Dim m As MotorGE
    '    m = MotorGE.GetInstance
    '    nome = ""
    '    nome = m.consultaValor("SELECT Descricao from COP_Obras where codigo like '" + txtProjecto.Text + "'")
    '    txtNomeProjecto.Text = nome
    '    If nome <> "" Then
    '        carregarDadosProjectoLM(txtProjecto.Text)
    '        If m.FuncionarioAdm Then
    '            carregarDadosProjectoCOT(txtProjecto.Text)
    '            carregarDadosProjectoECF(txtProjecto.Text)
    '        End If
    '        MsgBox(buscarNomeDocumento(tabCtlMaterial.SelectedIndex, 0) + " do Projecto " + txtProjecto.Text + " em modo de edição", MsgBoxStyle.Information)
    '    Else
    '        actualizar()
    '    End If

    'End Sub

    Private Sub carregarDadosProjectoLM(idDoc As String)
        Dim m As MotorGE

        carregarFornecedoresLinha("")
        gridArtigos.Tag = ""
        m = MotorGE.GetInstance

        Dim idCabecDoc As String
        idCabecDoc = idDoc
        'If modulo = "C" Then
        '    idCabecDoc = m.existeDocumentoCompra(idDoc)
        'Else
        '    idCabecDoc = m.existeDocumentoVenda(idDoc)
        'End If

        '  gridArtigos.Tag = idCabecDoc
        '   If idCabecDoc <> "" Then
        actualizarFormularioLM(idCabecDoc)
        '  Else
        'actualizar(False)
        ' End If

    End Sub

    Private Sub carregarDadosProjectoCOT(idCabecDoc As String)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim listaDocs As StdBELista
        gridDocsCOT.Records.DeleteAll()
        listaDocs = m.existeDocumentosCompraRelacionado(buscarTipoDocumento(1), idCabecDoc, cbTodosCOT.Checked)
        While Not listaDocs.NoFim
            inserirLinhaRegisto(listaDocs.Valor("ID"), listaDocs.Valor("Entidade"), listaDocs.Valor("Nome"), listaDocs.Valor("DataDoc"), listaDocs.Valor("Serie"), listaDocs.Valor("Numdoc"), gridDocsCOT)
            listaDocs.Seguinte()
        End While
        gridDocsCOT.Populate()
        If gridDocsCOT.Rows.Count > 0 Then
            gridArtigosCOT.Tag = gridDocsCOT.Rows(0).Record.Tag
            actualizarGrelhaLinhasArtigos(gridDocsCOT.Rows(0).Record.Tag, gridArtigosCOT, lblRegisto2)
            txtObservacoesCOT.Text = m.consultaValor("SELECT Observacoes FROM CabecCompras where id='" + gridArtigosCOT.Tag + "'")
        End If
    End Sub

    Private Sub carregarDadosProjectoECF(idCabecDoc As String)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim listaDocs As StdBELista
        gridDocsENC.Records.DeleteAll()
        gridDocsENC.Populate()
        listaDocs = m.existeDocumentosCompraRelacionado(buscarTipoDocumento(2), idCabecDoc, cbTodosENC.Checked)
        If listaDocs Is Nothing Then Exit Sub
        While Not listaDocs.NoFim
            inserirLinhaRegisto(listaDocs.Valor("ID"), listaDocs.Valor("Entidade"), listaDocs.Valor("Nome"), listaDocs.Valor("DataDoc"), listaDocs.Valor("Serie"), listaDocs.Valor("Numdoc"), gridDocsENC)
            listaDocs.Seguinte()
        End While
        gridDocsENC.Populate()
        If gridDocsENC.Rows.Count > 0 Then
            gridArtigosENC.Tag = gridDocsENC.Rows(0).Record.Tag
            actualizarGrelhaLinhasArtigos(gridDocsENC.Rows(0).Record.Tag, gridArtigosENC, lblRegisto3)
            txtObservacoesENC.Text = m.consultaValor("SELECT Observacoes FROM CabecCompras where id='" + gridArtigosENC.Tag + "'")
            actualizarDataRecepcao(gridArtigosENC.Tag)
            actualizarGrelhaStocks()
        End If

    End Sub


    Private Sub actualizarDataRecepcao(id As String)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim datarecepcao As String
        datarecepcao = m.consultaValor("SELECT CDU_CabVar2 FROM CabecCompras where id='" + id + "'")

        dtpDataFatura.Value = Now
        dtpDataFatura.Checked = False
        If datarecepcao <> "" Then
            dtpDataFatura.Value = datarecepcao
            dtpDataFatura.Checked = True
        End If
    End Sub




    Private Sub txtPesqFornecedor_Change(sender As Object, e As EventArgs) Handles txtFornecedor.Change
        inicializarFornecedores()
        VerificarChecked()
    End Sub
    Private Sub VerificarChecked()
        Dim row As DataRowView
        Dim index As Integer
        Dim codigo1 As String
        Dim codigo2 As String
        Dim rowC As ArrayList
        rowC = New ArrayList


        For Each row In lstFornecedor.Items
            rowC.Add(LCase(row("Codigo")))
        Next


        ' index = index + 1
        For Each codigo1 In rowC
            For Each codigo2 In listaFornecedores
                If codigo1 = LCase(codigo2) Then
                    lstFornecedor.SetItemCheckState(index, CheckState.Checked)
                End If
            Next
            index = index + 1
        Next


    End Sub

    Private Sub txtProjecto_Leave(sender As Object, e As EventArgs)
        'Dim nome As String
        'Dim m As MotorGE
        'm = MotorGE.GetInstance
        'nome = ""
        'nome = m.consultaValor("SELECT Descricao from COP_Obras where codigo like '" + txtProjecto.Text + "'")
        'txtNomeProjecto.Text = nome
        'carregarDadosProjectoLM(txtProjecto.Text)
    End Sub




    Private Sub removerLinhasAte(index As Integer)
        Dim i As Integer

        For i = gridArtigos.Records.Count To index Step -1
            gridArtigos.Records.RemoveAt(i)
        Next
    End Sub


    Private Function podeComecar(comecar As Boolean, campo As String, index As Integer, inicioExcel As String) As Boolean
        If comecar = True Then
            Return comecar
        End If

        Dim campoComecar As String
        Select Case index

            Case 2 : campoComecar = "part nr."
            Case 3 : campoComecar = "*"
            Case Else : campoComecar = inicioExcel.ToLower
        End Select


        Return campo.ToLower = campoComecar.ToLower ' IIf(versaoV1, "part nr.", inicioExcel.ToLower)

    End Function


    Private Function actualizarCabecalhosDocumentos(datatable As DataTable) As DataTable
        datatable.Columns(0).ColumnName = "ID"
        datatable.Columns(1).ColumnName = "Peca"
        datatable.Columns(2).ColumnName = "Descricao"
        datatable.Columns(3).ColumnName = "Quantidade"
        datatable.Columns(4).ColumnName = "Dimensoes"
        datatable.Columns(5).ColumnName = "Material"

        Return datatable
    End Function

    Const ID_INSERIRLINHA = 10
    Const ID_APAGARLINHA = 11
    Const ID_ARTIGOINTERNO = 12
    Const ID_ARTIGOPROBLEMA = 13
    Const ID_ARTIGONORMAL = 14
    Const ID_ARTIGOENCOMENDADO = 15

    Const ID_REABRIR = 16
    Const ID_ANULAR = 17

    Private Sub gridArtigos_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridArtigos.MouseUpEvent

        Dim m As MotorGE
        m = MotorGE.GetInstance
        If Not m.FuncionarioAdm Then Exit Sub

        If e.button = 1 Then
            selecionarTodos(sender, e, gridArtigos)
            'actualizarFornecedores(sender, e, gridArtigos)
        Else
            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_INSERIRLINHA, "&Inserir Linha", -1, False)
                Control.IconId = 30
                Control.BeginGroup = True
                Control = .Add(xtpControlButton, ID_APAGARLINHA, "&Apagar Linha", -1, False)
                Control.IconId = 31
                Control.Enabled = gridArtigos.Records.Count > 0
                '   Control.BeginGroup = True
                Control = .Add(xtpControlButton, ID_ARTIGOENCOMENDADO, "&Artigo Encomendado", -1, False)
                Control.IconId = 22
                Control.BeginGroup = True
                'Control = .Add(xtpControlButton, ID_ARTIGOINTERNO, "&Artigo Interno", -1, False)
                'Control.IconId = 23
                'Control = .Add(xtpControlButton, ID_ARTIGOPROBLEMA, "&Artigo Problema", -1, False)
                'Control.IconId = 24
                Control = .Add(xtpControlButton, ID_ARTIGONORMAL, "&Artigo Normal", -1, False)
                Control.IconId = 20
                '  Control.BeginGroup = True
            End With
            popup.ShowPopup()
        End If
    End Sub


    Private Sub selecionarTodos(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent, grid As AxXtremeReportControl.AxReportControl)
        If Not grid.HitTest(e.x, e.y).Column Is Nothing Then
            If grid.HitTest(e.x, e.y).ht = XTPReportHitTestInfoCode.xtpHitTestHeader And grid.HitTest(e.x, e.y).Column.Index = COLUMN_CHECKBOX Then
                Dim check As Boolean
                check = grid.Columns(COLUMN_CHECKBOX).Tag
                check = Not check
                grid.Columns(COLUMN_CHECKBOX).Tag = check
                selecionarTodos(check, grid)
            End If
        End If
    End Sub

    Private Sub selecionarTodos(seleccionar As Boolean, grid As AxXtremeReportControl.AxReportControl)
        Dim row1 As ReportRow
        For Each row1 In grid.Rows
            row1.Record(COLUMN_CHECKBOX).Checked = seleccionar
        Next
        grid.Populate()
    End Sub

    Private Sub lstFornecedor_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstFornecedor.ItemCheck
        If lstFornecedor.SelectedValue Is Nothing Then Exit Sub
        If e.CurrentValue = CheckState.Unchecked Then
            listaFornecedores.Add(lstFornecedor.SelectedValue)
        Else
            listaFornecedores.Remove(lstFornecedor.SelectedValue)
        End If
    End Sub



    Private Sub tabCtlMaterial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabCtlMaterial.SelectedIndexChanged
        Select Case tabCtlMaterial.SelectedIndex
            Case 0 : configurarLM()
            Case 1 : configurarPC()
            Case 2 : configurarENC()
        End Select
    End Sub

    Private Sub configurarLM()
        ' btn1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Cotação
        ' btn2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PCotação
        ' Group1.Text = "Lista Material"
        ' Group2.Text = "Pedido Cotação"
        Group1.Visible = True
        Group2.Visible = True
        Group3.Visible = True
        '    Group1.Location = New Point(581, 2)
        'rb3.Visible = False
        'Group2.Visible = btn2.Visible
        'btn2.Visible = btn2.Visible
        'rb4.Visible = btn2.Visible
        'rb5.Visible = btn2.Visible
        'rb6.Visible = btn2.Visible
        '  btn1.Text = Group1.Text
        ' btn2.Text = Group2.Text
        'btn1.Location = New Point(6, 25)
        'btn2.Location = New Point(6, 25)
    End Sub

    Private Sub configurarPC()
        '    btn1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PCotação
        ' btn2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.encomendar
        ' Group1.Text = "Pedido Cotação"
        ' Group2.Text = "Encomenda"
        Group1.Visible = False
        Group2.Visible = True
        Group3.Visible = True

        '  Group1.Location = New Point(581, 2)
        ' Group2.Visible = btn2.Visible
        ' btn2.Visible = btn2.Visible
        ' rb4.Visible = btn2.Visible
        '  rb5.Visible = btn2.Visible
        '  rb6.Visible = btn2.Visible
        '  btn1.Text = Group1.Text
        'btn2.Text = Group2.Text
        '   btn1.Location = New Point(6, 25)
        '   btn2.Location = New Point(6, 25)
    End Sub

    Private Sub configurarENC()
        ' btn1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.encomendar
        '  btn2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.encomendar
        ' Group1.Text = "Encomenda"
        Group1.Visible = False
        Group2.Visible = False
        Group3.Visible = True
        ' Group1.Location = Group2.Location
        ' Group2.Visible = btn2.Visible
        ' btn2.Visible = btn2.Visible
        ' rb4.Visible = btn2.Visible
        ' rb5.Visible = btn2.Visible
        ' rb6.Visible = btn2.Visible
        ' btn1.Text = Group1.Text
        ' btn2.Text = Group2.Text
        ' btn1.Location = New Point(793, 27)
        ' btn2.Location = New Point(587, 27)
    End Sub

    'New Point(587, 27)
    'New Point(793, 27)

    Private Sub configurarAplicacao()
        Dim m As MotorGE
        m = MotorGE.GetInstance
        txtFornecedor.Visible = m.FuncionarioAdm
        lstFornecedor.Visible = m.FuncionarioAdm
        lblFornecedor.Visible = m.FuncionarioAdm

        rb1.Checked = True
        rb2.Checked = False
        rb3.Checked = False
        rb4.Checked = True
        rb5.Checked = False
        rb6.Checked = False
        rb7.Checked = True
        rb8.Checked = False
        rb9.Checked = False

        If Not m.FuncionarioAdm Then
            tabCtlMaterial.TabPages.RemoveAt(2)
            tabCtlMaterial.TabPages.RemoveAt(1)
            '  gridArtigos.Width = TabControl2.Width
            ' lstCarregarListaMaterial.Left = Me.Width - 100
            Group2.Visible = False
            Group1.Location = Group2.Location
            TabControl1.Visible = False
            ' Group2.Visible = btn2.Visible
            ' btn2.Visible = btn2.Visible
            ' rb4.Visible = btn2.Visible
            ' rb5.Visible = btn2.Visible
            ' rb6.Visible = btn2.Visible
        End If
    End Sub

    Private Sub gravarDocumentoStocks()
        Dim m As MotorGE
        m = MotorGE.GetInstance
        m.gravarDocumentoStocks(gridArtigosENT.Records)
    End Sub

    Private Sub gravarDocumento(botao As Integer)

        If Not validarGravacao(0) Then
            Exit Sub
        End If

        If getRbChecked(0, botao).Checked Then
            If Not apresentarPergunta(0, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub
            gravarDocumento(False, botao)
            actualizar(True, , False)
            MsgBox("Documento gravado com sucesso", MsgBoxStyle.Information)
        End If

        If getRbChecked(1, botao).Checked Then
            If Not apresentarPergunta(1, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub
            imprimirDoc(tabCtlMaterial.SelectedIndex, botao)

            actualizar(True, , False)
            MsgBox("Documento imprimido com sucesso", MsgBoxStyle.Information)

        End If


        If getRbChecked(2, botao).Checked Then
            If Not apresentarPergunta(2, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub
            enviarEmailDocumento(tabCtlMaterial.SelectedIndex, botao)
            actualizar(True, , False)
        End If
        '   MsgBox("Documento enviado com sucesso", MsgBoxStyle.Information)


    End Sub


    'Private Function getRbChecked(index As Integer, botao As Integer) As RadioButton
    '    Dim rb As RadioButton

    '    Select Case index
    '        Case 0 :
    '              If botao = 1 Then
    '                rb = rb4
    '            Else
    '                rb = rb7
    '            End If
    '        Case 1 : If botao = 1 Then
    '                rb = rb5
    '            Else
    '                rb = rb8
    '            End If
    '        Case 2 : If botao = 1 Then
    '                rb = rb6
    '            Else
    '                rb = rb9
    '            End If
    '    End Select
    '    Return rb
    'End Function

    Private Function getRbChecked(index As Integer, botao As Integer) As RadioButton
        Dim rb As RadioButton
        rb = Nothing
        Select Case botao
            Case 0 : Select Case index
                    Case 0 : rb = rb1
                    Case 1 : rb = rb2
                    Case 2 : rb = rb3
                End Select
            Case 1 : Select Case index
                    Case 0 : rb = rb4
                    Case 1 : rb = rb5
                    Case 2 : rb = rb6
                End Select
            Case 2 : Select Case index
                    Case 0 : rb = rb7
                    Case 1 : rb = rb8
                    Case 2 : rb = rb9
                End Select
        End Select
        Return rb
    End Function

    Private Sub transformarDocumento(botao As Integer)
        Dim m As MotorGE
        Dim lista As ArrayList
        Dim codigo As String
        m = MotorGE.GetInstance

        If Not validarGravacao(1) Then
            Exit Sub
        End If

        lista = buscarListaFornecedores(tabCtlMaterial.SelectedIndex)

        If getRbChecked(0, botao).Checked Then
            If Not apresentarPergunta(0, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub
            ' gravarDocumento(False, 0, , False)
            For Each codigo In lista
                gravarDocumento(False, botao, codigo)
            Next

            carregarDadosProjectoLM(getIdCabecdoc())
            carregarDadosProjectoCOT(getIdCabecdoc())
            carregarDadosProjectoECF(getIdCabecdoc())



            'If tabCtlMaterial.SelectedIndex = 0 Then
            '    aplicarCorLinha(gridArtigos, COR_COT)
            'Else
            '    aplicarCorLinha(gridArtigosCOT, COR_COT)
            'End If
            actualizar(True, , False)
            MsgBox("Documento gravado com sucesso", MsgBoxStyle.Information)
        End If

        If getRbChecked(1, botao).Checked Then
            If Not apresentarPergunta(1, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub
            '   gravarDocumento(False, 0, , False)
            For Each codigo In lista
                imprimirDoc(tabCtlMaterial.SelectedIndex, botao, codigo)
            Next
            carregarDadosProjectoLM(getIdCabecdoc())
            carregarDadosProjectoCOT(getIdCabecdoc())
            carregarDadosProjectoECF(getIdCabecdoc())
            'If tabCtlMaterial.SelectedIndex = 0 Then
            '    aplicarCorLinha(gridArtigos, COR_COT)
            'Else
            '    aplicarCorLinha(gridArtigosCOT, COR_COT)
            'End If
            actualizar(True, , False)
            MsgBox("Documento imprimido com sucesso", MsgBoxStyle.Information)
        End If

        If getRbChecked(2, botao).Checked Then
            If Not apresentarPergunta(2, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub
            ' gravarDocumento(False, 0, , False)
            For Each codigo In lista
                enviarEmailDocumento(tabCtlMaterial.SelectedIndex, botao, codigo)
            Next
            carregarDadosProjectoLM(getIdCabecdoc())
            carregarDadosProjectoCOT(getIdCabecdoc())
            carregarDadosProjectoECF(getIdCabecdoc())

            'If tabCtlMaterial.SelectedIndex = 0 Then
            '    aplicarCorLinha(gridArtigos, COR_COT)
            'Else
            '    aplicarCorLinha(gridArtigosCOT, COR_COT)
            'End If

            actualizar(True, , False)
            MsgBox("Documento enviado com sucesso", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub aplicarCorLinha(linha As ReportRecord, cor As UInteger)
        linha(COLUMN_ARTIGO).BackColor = cor
        linha(COLUMN_DESCRICAO).BackColor = cor
        linha(COLUMN_QUANTIDADE).BackColor = cor
        linha(COLUMN_FORNPRECO).BackColor = cor
        linha(COLUMN_ULTPRECO).BackColor = cor
        linha(COLUMN_CHECKBOX).BackColor = cor
    End Sub
    Private Sub aplicarCorLinha(grid As AxXtremeReportControl.AxReportControl, cor As UInteger)
        For i = 0 To grid.Rows.Count - 1
            If grid.Rows(i).Record(COLUMN_CHECKBOX).Checked Then
                aplicarCorLinha(grid.Rows(i).Record, cor)
            End If
        Next
    End Sub
    Private Sub actualizarGrelhaLinhasArtigos(id As String, grid As AxXtremeReportControl.AxReportControl, label As Label)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim dtRegistos As DataTable
        dtRegistos = Nothing

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(id, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(id, "V")

        ' dt = m.daDataDocumento(id, dtpData.Value, "C")

        Dim dtrow As DataRow
        grid.Records.DeleteAll()
        grid.Records.DeleteAll()
        emModoEdicao = True
        For Each dtrow In dtRegistos.Rows
            If Not grelhaENC(grid) Then
                inserirLinhaRegisto(dtrow, grid)
            Else
                inserirLinhaRegistoEnc(dtrow, grid)
            End If
        Next
        ' grid.SelectedRows.DeleteAll()
        'buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        grid.Populate()
        label.Text = CStr(grid.Rows.Count) + " Registos"
        emModoEdicao = False
        actualizarGrelhaStocks()
    End Sub


    Private Sub gridDocsCOT_SelectionChanged(sender As Object, e As EventArgs) Handles gridDocsCOT.SelectionChanged
        gridArtigosCOT.Tag = gridDocsCOT.SelectedRows(0).Record.Tag
        actualizarGrelhaLinhasArtigos(gridDocsCOT.SelectedRows(0).Record.Tag, gridArtigosCOT, lblRegisto2)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        txtObservacoesCOT.Text = m.consultaValor("SELECT Observacoes FROM CabecCompras where id='" + gridArtigosCOT.Tag + "'")
    End Sub

    Private Sub gridArtigosCOT_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridArtigosCOT.MouseUpEvent
        selecionarTodos(sender, e, gridArtigosCOT)
    End Sub

    Private Sub gridDocsENC_SelectionChanged(sender As Object, e As EventArgs) Handles gridDocsENC.SelectionChanged
        gridArtigosENC.Tag = gridDocsENC.SelectedRows(0).Record.Tag
        actualizarGrelhaLinhasArtigos(gridDocsENC.SelectedRows(0).Record.Tag, gridArtigosENC, lblRegisto3)
        Dim m As MotorGE
        m = MotorGE.GetInstance
        txtObservacoesENC.Text = m.consultaValor("SELECT Observacoes FROM CabecCompras where id='" + gridArtigosENC.Tag + "'")
        actualizarDataRecepcao(gridArtigosENC.Tag)
    End Sub


    Private Sub btnAdicionaLinha_Click(sender As Object, e As EventArgs)
        Dim dtRow As DataRow

        dtRow = Nothing
        inserirLinhaRegisto(dtRow, gridArtigos)
    End Sub


    Private Sub gridArtigos_InplaceButtonDown(sender As Object, e As AxXtremeReportControl._DReportControlEvents_InplaceButtonDownEvent) Handles gridArtigos.InplaceButtonDown
        Dim frm As FrmLista
        frm = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(3)
        frm.setalinhamentoColunas(alinhamentoColunas)
        frm.setComando("SELECT APS_GP_Artigos.Artigo, APS_GP_Artigos.Descricao as 'Descrição', APS_GP_Artigos.StkActual, APS_GP_Artigos.PCUltimo, Fornecedores.Nome as UltimoFornecedor FROM APS_GP_Artigos LEFT OUTER JOIN Fornecedores ON APS_GP_Artigos.UltimoFornecedor=Fornecedores.Fornecedor")
        frm.ShowFilter(True)
        frm.ShowDialog()
        If frm.seleccionouOK Then
            gridArtigos.Rows(e.button.Row.Index).Record(e.button.Column.Index).Value = frm.getValor(0)
            gridArtigos.Rows(e.button.Row.Index).Record(e.button.Column.Index).Caption = frm.getValor(0)
            actualizarArtigoLinha(frm.getValor(0), e.button.Row.Index, gridArtigos)
        End If

    End Sub


    Private Sub inserirLinhaRegisto()
        Dim m As MotorGE
        m = MotorGE.GetInstance
        'If m.existeDocumentoCompra(getIdCabecdoc()) = "" Then
        '    MsgBox("Tem de Indicar um documento", MsgBoxStyle.Critical)
        '    Exit Sub
        'End If

        Dim lista As StdBELista
        Dim dt As DataTable

        '  lista = m.consulta("select '' as ID,'' as Descricao, 1 as Quantidade,'' as Artigo,'' as Estado")
        lista = m.consulta("select ' ' as ID,'' as Descricao ,1 as Quantidade,'' as Artigo,'' as Estado,0 as StkActual,0 as PCUltimo,'' as UltimoFornecedor")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        inserirLinhaRegisto(dt(0), gridArtigos, True)
        gridArtigos.Populate()
        gridArtigos.Navigator.MoveLastRow()
    End Sub

    Const MSG_APAGAR_LINHA = "Deseja realmente apagar a linha seleccionada ?"

    Private Sub apagarLinhaRegisto()
        If MsgBox(MSG_APAGAR_LINHA, vbQuestion + vbYesNo) = vbYes Then
            If Not gridArtigos.FocusedRow Is Nothing Then
                gridArtigos.RemoveRowEx(gridArtigos.FocusedRow)
            End If
        End If
    End Sub


    Private Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
        gravarDocumento(0)
    End Sub


    Private Sub btn2_Click(sender As Object, e As EventArgs) Handles btn2.Click
        If tabCtlMaterial.SelectedIndex <> 1 Then
            gravarDocumento(False, tabCtlMaterial.SelectedIndex)
            transformarDocumento(1)
        Else
            gravarDocumento(1)
        End If
    End Sub


    Private Sub btn3_Click(sender As Object, e As EventArgs) Handles btn3.Click
        If tabCtlMaterial.SelectedIndex <> 2 Then
            gravarDocumento(False, tabCtlMaterial.SelectedIndex)
            transformarDocumento(2)
        Else
            gravarDocumentoStocks()
            gravarDocumento(2)
            carregarDadosProjectoLM(getIdCabecdoc())
            carregarDadosProjectoCOT(getIdCabecdoc())
            carregarDadosProjectoECF(getIdCabecdoc())
        End If
    End Sub

    Private Sub actualizarEstadoLinha(estado As String)
        Dim m As MotorGE
        Dim dtRow As DataRow

        m = MotorGE.GetInstance
        If Not gridArtigos.FocusedRow Is Nothing Then
            Dim estadoLinha As String
            dtRow = gridArtigos.FocusedRow.Record.Tag
            estadoLinha = gridArtigos.FocusedRow.Record(COLUMN_ULTPRECO).Tag
            If estadoLinha <> ESTADO_ECF Then
                gridArtigos.FocusedRow.Record(COLUMN_CHECKBOX).Tag = estado
                m.actualizarEstadoLinha(modulo, dtRow("Id").ToString, estado, False)
                aplicarCorLinha(gridArtigos.FocusedRow.Record, buscarCor(estado))
                gridArtigos.Populate()
            End If
        End If
    End Sub


    Private Sub actualizarFornecedores(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent, grid As AxXtremeReportControl.AxReportControl)
        If Not grid.HitTest(e.x, e.y).Column Is Nothing Then
            If grid.HitTest(e.x, e.y).ht = XTPReportHitTestInfoCode.xtpHitTestReportArea Then
                Dim dtRow As DataRow
                dtRow = grid.HitTest(e.x, e.y).Row.Record.Tag
                carregarFornecedoresLinha(dtRow("Id").ToString)
            End If
        End If
    End Sub

    Private Sub carregarFornecedoresLinha(idlinha As String)
        Dim m As MotorGE
        Dim lista As StdBELista
        Dim dt As DataTable
        dt = Nothing
        m = MotorGE.GetInstance
        If idlinha <> "" Then
            lista = m.consulta("SELECT Distinct Nome from CabecCompras WHERE id in (select idcabecCompras FROM LinhasCompras WHERE IdLinhaOrigemCopia='" + idlinha + "')")
            dt = m.convertRecordSetToDataTable(lista.DataSet)
        End If
        lstFornPC.DataSource = Nothing
        lstFornPC.DataSource = dt
        lstFornPC.ValueMember = "Codigo"
        lstFornPC.DisplayMember = "Nome"
        lstFornPC.ClearSelected()
    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        apagarLinhaRegisto()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        inserirLinhaRegisto()
    End Sub

    Private Sub ckbVerTodos_CheckedChanged(sender As Object, e As EventArgs) Handles ckbVerTodos.CheckedChanged
        inicializarFornecedores()
        VerificarChecked()
    End Sub

    Private Sub gridArtigosENC_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridArtigosENC.MouseUpEvent
        Dim m As MotorGE
        m = MotorGE.GetInstance
        If Not m.FuncionarioAdm Then Exit Sub

        If e.button = 1 Then
            selecionarTodos(sender, e, gridArtigosENC)
        End If
    End Sub



    Private Sub cbTodosENC_CheckedChanged(sender As Object, e As EventArgs) Handles cbTodosENC.CheckedChanged
        '   carregarDadosProjectoCOT(txtProjecto.Text)
        carregarDadosProjectoECF(getIdCabecdoc())
    End Sub

    Private Sub cbTodosCOT_CheckedChanged(sender As Object, e As EventArgs) Handles cbTodosCOT.CheckedChanged
        carregarDadosProjectoCOT(getIdCabecdoc())
    End Sub

    Private Sub gridDocsCOT_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridDocsCOT.MouseUpEvent

        If e.button = 2 Then
            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_REABRIR, "&Reabrir Documento", -1, False)
                Control.IconId = 41
                Control.BeginGroup = True
                Control.Enabled = botaoActivo(gridDocsCOT, False)
                Control = .Add(xtpControlButton, ID_ANULAR, "&Anular Documento", -1, False)
                Control.IconId = 40
                Control.Enabled = botaoActivo(gridDocsCOT, True)

            End With
            popup.ShowPopup()
        End If
    End Sub

    Private Sub gridDocsENC_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridDocsENC.MouseUpEvent

        If e.button = 2 Then


            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_REABRIR, "&Reabrir Documento", -1, False)
                Control.IconId = 41
                Control.BeginGroup = True

                Control.Enabled = botaoActivo(gridDocsENC, False)
                Control = .Add(xtpControlButton, ID_ANULAR, "&Anular Documento", -1, False)
                Control.IconId = 40
                Control.Enabled = botaoActivo(gridDocsENC, True)

            End With
            popup.ShowPopup()
        End If
    End Sub

    Private Function botaoActivo(grid As AxXtremeReportControl.AxReportControl, aberto As Boolean) As Boolean

        If grid.SelectedRows.Count > 0 Then
            Dim m As MotorGE
            m = MotorGE.GetInstance()
            Return m.VerificarEstadoDocumento(grid.SelectedRows(0).Record.Tag.ToString, aberto)
        Else
            Return False
        End If

    End Function

    Private Sub actualizarEstadoDocumento(abrir As Boolean)
        Dim m As MotorGE
        m = MotorGE.GetInstance()

        Dim id As String
        If tabCtlMaterial.SelectedIndex = 1 Then
            id = gridDocsCOT.SelectedRows(0).Record.Tag.ToString
        Else
            id = gridDocsENC.SelectedRows(0).Record.Tag.ToString
        End If
        'grid.SelectedRows(0).Record.Tag.ToString

        If abrir Then
            m.executarComando("UPDATE CabecComprasStatus SET Fechado=0, Anulado=0 WHERE idCabecCompras='" + id + "'")
        Else
            m.executarComando("UPDATE CabecComprasStatus SET Anulado=1 WHERE idCabecCompras='" + id + "'")
        End If

        'actualizar o valor da grelha
        If tabCtlMaterial.SelectedIndex = 1 Then
            carregarDadosProjectoCOT(getIdCabecdoc())
        Else
            carregarDadosProjectoECF(getIdCabecdoc())
        End If

    End Sub






    Private Function buscarEntidade(estado As String, idLinha As String) As String
        If estado = "E" Or estado = "C" Then
            Dim m As MotorGE
            m = MotorGE.GetInstance()
            Return m.buscarEntidadesLinha(idLinha)
        End If
        Return ""
    End Function

    Private Sub txtNumDoc_Leave(sender As Object, e As EventArgs) Handles txtNumDoc.Leave

    End Sub

    Private Sub actualizarFormulario(idCabecDoc As String)
        If emModoEdicao Then Exit Sub
        Dim m As MotorGE
        m = MotorGE.GetInstance
        carregarDadosProjectoLM(idCabecDoc)
        If m.FuncionarioAdm Then
            carregarDadosProjectoCOT(idCabecDoc)
            carregarDadosProjectoECF(idCabecDoc)
        End If
        MsgBox(buscarNomeDocumento(tabCtlMaterial.SelectedIndex, 0) + " Nº " + CStr(txtNumDoc.Value) + " em modo de edição", MsgBoxStyle.Information)
    End Sub

    Private Sub gridArtigosCOT_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigosCOT.ValueChanged
        e.item.Caption = Formatar(e.item.Value)
    End Sub

    Private Sub gridArtigosENC_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigosENC.ValueChanged
        If e.column.Index <> ENCCOLUMN_LINHADATA Then
            e.item.Caption = Formatar(e.item.Value)
            If e.column.Index = ENCCOLUMN_QUANTIDADEFALTA Then
                e.row.Record.Item(ENCCOLUMN_CHECKBOX).Checked = True
            End If

        End If

    End Sub

    Public Function Formatar(item As String) As String
        Return FormatNumber(item, 2)
    End Function


    Private Sub txtNumDoc_ValueChanged(sender As Object, e As EventArgs) Handles txtNumDoc.ValueChanged

        If emModoEdicao Then Exit Sub
        Dim m As MotorGE
        m = MotorGE.GetInstance
        Dim projecto As String
        Dim idCabecDoc As String = ""
        projecto = ""
        If modulo = "C" Then
            idCabecDoc = m.existeDocumentoCompra(m.InformModLstMaterial.TIPODOCLM, txtNumDoc.Value)
        End If

        If idCabecDoc <> "" Then
            actualizarFormulario(idCabecDoc)
        Else
            actualizar(False)
        End If
    End Sub


    Private Sub actualizarArtigoLinha(artigo As String, index As Integer, grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorGE
        Dim lista As StdBELista
        m = MotorGE.GetInstance
        Dim artigoExiste As Boolean

        lista = m.consulta("SELECT APS_GP_Artigos.Artigo, APS_GP_Artigos.Descricao, APS_GP_Artigos.StkActual, APS_GP_Artigos.PCUltimo, Fornecedores.Nome as UltimoFornecedor FROM APS_GP_Artigos LEFT OUTER JOIN Fornecedores ON APS_GP_Artigos.UltimoFornecedor=Fornecedores.Fornecedor WHERE Artigo='" + artigo + "'")

        If Not lista Is Nothing Then
            If Not lista.NoFim Then
                grid.Records.Record(index).Item(COLUMN_ARTIGO).Value = lista.Valor("Artigo")
                grid.Records.Record(index).Item(COLUMN_DESCRICAO).Value = lista.Valor("Descricao")
                grid.Records.Record(index).Item(COLUMN_STKACTUAL).Value = lista.Valor("StkActual")
                '  grid.Records.Record(index).Item(COLUMN_STKACTUAL).Caption = Formatar(grid.Records.Record(index).Item(COLUMN_STKACTUAL).Value)
                grid.Records.Record(index).Item(COLUMN_ULTPRECO).Value = lista.Valor("PCUltimo")
                grid.Records.Record(index).Item(COLUMN_ULTPRECO).Caption = Formatar(grid.Records.Record(index).Item(COLUMN_ULTPRECO).Value)
                grid.Records.Record(index).Item(COLUMN_FORNPRECO).Value = lista.Valor("UltimoFornecedor")
                artigoExiste = True
            End If
        End If



        If Not artigoExiste Then
            grid.Records.Record(index).Item(COLUMN_ARTIGO).Value = ""
            grid.Records.Record(index).Item(COLUMN_DESCRICAO).Value = ""
            grid.Records.Record(index).Item(COLUMN_STKACTUAL).Value = 0
            '  grid.Records.Record(index).Item(COLUMN_STKACTUAL).Caption = Formatar(grid.Records.Record(index).Item(COLUMN_STKACTUAL).Value)
            grid.Records.Record(index).Item(COLUMN_ULTPRECO).Value = 0
            grid.Records.Record(index).Item(COLUMN_ULTPRECO).Caption = Formatar(grid.Records.Record(index).Item(COLUMN_ULTPRECO).Value)
            grid.Records.Record(index).Item(COLUMN_FORNPRECO).Value = ""
            MsgBox("O Artigo " + artigo + " não existe", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub gridArtigos_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigos.ValueChanged

        If e.column.Index = COLUMN_ARTIGO Then
            actualizarArtigoLinha(e.item.Value, e.row.Index, gridArtigos)
        End If

    End Sub

    Private Sub gridArtigosENC_SelectionChanged(sender As Object, e As EventArgs) Handles gridArtigosENC.SelectionChanged

        actualizarGrelhaStocks()
    End Sub
    Private Sub actualizarGrelhaStocks()
        gridArtigosENT.Records.DeleteAll()
        If gridArtigosENC.SelectedRows.Count > 0 Then
            Dim dt As DataRow
            dt = gridArtigosENC.SelectedRows(0).Record.Tag
            actualizarGrelhaStocks(dt("ID").ToString)
        End If
        gridArtigosENT.Populate()
    End Sub

    Private Sub actualizarGrelhaStocks(idLinha As String)
        Dim lista As StdBELista
        Dim m As MotorGE
        Dim record As ReportRecord
        Dim item As ReportRecordItem

        m = MotorGE.GetInstance
        lista = m.consulta("SELECT id,artigo,Descricao,Quantidade,Data FROM LinhasStk WHERE IdLinhaOrigemCopia='" + idLinha + "' ORDER BY NUmlinha", False)

        While Not lista.NoFim
            record = gridArtigosENT.Records.Add()

            record.Tag = lista.Valor("ID")
            item = record.AddItem(lista.Valor("Artigo"))
            '  Item.BackColor = cor
            ' item.Editable = editavel
            '  If descricao = "" Then
            item = record.AddItem(lista.Valor("Descricao"))
            item = record.AddItem(lista.Valor("Quantidade"))
            item.Caption = Formatar(item.Value)
            item = record.AddItem(lista.Valor("Data"))
            lista.Seguinte()
        End While

    End Sub


    Private Sub validarArtigosStockMinimo()
        Dim dtRegistos As DataTable
        Dim m As Motor = Motor.GetInstance()
        Dim listaExclusao As String
        Dim consulta As String = ""

        consulta = "SELECT TOP 100 PERCENT Artigo,Descricao as 'Descrição', StkMinimo  as StkMinimo ,StkActual as StkActual,"
        consulta += " ISNULL((select -sum(LinhasComprasStatus.Quantidade-LinhasComprasStatus.QuantTrans)  from LinhasCompras inner join CabecCompras on linhascompras.IdCabecCompras=cabeccompras.id inner join LinhasComprasStatus on linhascompras.Id=LinhasComprasStatus.IdLinhasCompras WHERE (LinhasCompras.CDU_ObraN5 is NULL or (LinhasCompras.CDU_ObraN5<>'V' and LinhasCompras.CDU_ObraN5<>'C')) and TipoDoc in (" + m.getDocumentosEncomenda + ") and Serie>='2015'  and LinhasComprasStatus.EstadoTrans='P' and LinhasComprasStatus.Fechado=0 and Linhascompras.Artigo=VISTA.Artigo),0) as 'Qtd. Enc.'"
        consulta += " FROM " + m.daViewModulo("C") + " as VISTA"
        consulta += " WHERE StkMinimo>StkActual+ISNULL((select -sum(LinhasComprasStatus.Quantidade-LinhasComprasStatus.QuantTrans)  from LinhasCompras inner join CabecCompras on linhascompras.IdCabecCompras=cabeccompras.id inner join LinhasComprasStatus on linhascompras.Id=LinhasComprasStatus.IdLinhasCompras inner join DocumentosCompra ON CabecCompras.Tipodoc=DocumentosCompra.Documento WHERE (LinhasCompras.CDU_ObraN5 is NULL or (LinhasCompras.CDU_ObraN5<>'V' and LinhasCompras.CDU_ObraN5<>'C'))  and (DocumentosCompra.TipoDocumento=2 or TipoDoc in (" + m.getDocumentosEncomenda + ")) and Serie>='2015'  and LinhasComprasStatus.EstadoTrans='P' and LinhasComprasStatus.Fechado=0 and Linhascompras.Artigo=VISTA.Artigo),0) "
        consulta += " AND ArtigoAnulado=0 ORDER BY ARTIGO"

        'InputBox("", "", consulta)
        listaExclusao = "" 'daListaArtigosExcluidos()
        dtRegistos = m.consultaDataTable(consulta)   'm.daListaArtigos(m.daViewModulo(modulo), pesquisa, "", "")"

        If dtRegistos.Rows.Count > 0 Then
            Dim f As FrmLista
            f = New FrmLista
            Dim alinhamentoColunas() As Integer
            ReDim alinhamentoColunas(4)
            f.setComando(consulta)
            f.setCaption("Artigos Abaixo Stock Minimo")
            f.ShowFilter(True)
            f.setalinhamentoColunas(alinhamentoColunas)
            f.ShowDialog()
        End If
    End Sub

    Private Sub gridArtigosENT_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigosENT.ValueChanged
        If e.column.Index <> 4 Then
            e.item.Caption = Formatar(e.item.Value)
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        gridArtigos.PrintPreview(True)
    End Sub
End Class



