﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900
Imports Interop.GcpBE900
Imports PlatAPSNET
Imports Ionic.Zip
Imports System.Xml
Imports System.IO
Imports AxXtremeReportControl

Public Class FrmListaEncomendas


    Const COLUMN_PECA As Integer = 0
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTIDADE As Integer = 2
    Const COLUMN_DIMENSOES As Integer = 3
    Const COLUMN_MATERIAL As Integer = 4
    Const COLUMN_FORNECEDOR As Integer = 5
    Const COLUMN_ARTIGO As Integer = 6
    Const COLUMN_LINHADATA As Integer = 7
    Const COLUMN_CHECKBOX As Integer = 8

    Const COLUMN_ENTIDADE As Integer = 0
    Const COLUMN_NOME As Integer = 1
    Const COLUMN_DATA As Integer = 2
    ' Const COLUMN_SERIE As Integer = 3
    Const COLUMN_NUMDOC As Integer = 3
    Const COLUMN_DATARECEPCAO As Integer = 4
    Const COLUMN_CHECKBOX_DOC As Integer = 5

    Dim aplConfigurada As Boolean
    Dim modulo As String
    ' Dim doc As GcpBEDocumentoCompra
    Dim listaFornecedores As ArrayList

    Const COR_BRANCO As UInteger = 4294967295
    Const COR_COT As UInteger = 12432256

    Const COR_PROBLEMA As String = "605DCF"
    Const COR_INTERNO As String = "4682b4"

    Const COR_CONFERIDO As String = "bebebe"
    Const COR_ARTIGODEFAULT As String = "87CEEB"
    Const COR_ANULADO As String = "0045FF"
    Const COR_LINHAMATERIALCLIENTE As String = "00AAFF"
    Const COR_LINHAALTERACAO As String = "C390D4"
    Const COR_LINHAINSERCAO As String = "F2840F"
    Const COR_LINHASUBCONTRATADO As String = "7f7171"
    Const COR_LINHABLOQUEADO As String = "COCOCO"
    Const COR_LINHAREMOVIDA As String = "0000FF"



    Dim COR_ECF As String = "6495ed"
    Dim COR_VERIFICADO As String = "48d1cc"

    Const ESTADO_NORMAL = ""
    Const ESTADO_INTERNO = "I"
    Const ESTADO_PROBLEMAS = "P"
    Const ESTADO_COT = "C"
    Const ESTADO_ECF = "E"
    Const ESTADO_VER = "V"
    Const ESTADO_CONF = "F"
    Const ESTADO_A = "A"
    Const ESTADO_CLIENTE = "L"
    Const ESTADO_SUB = "S"
    Const ESTADO_BLOQ = "B"

    Dim mGridLST As MotorGrelhas
    Dim mGridLSTAlt As MotorGrelhas
    Dim mGridCOT As MotorGrelhas
    Dim mGridENC As MotorGrelhas
    Dim mGridENT As MotorGrelhas

    Dim hashLST As Hashtable
    Dim hashCOT As Hashtable
    Dim hashENC As Hashtable
    Dim hashLSTPecas As Hashtable



    Dim htLinhaModificadas As Hashtable

    Dim carregouLista As Boolean


    Private Sub FrmEncomendas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try


            Dim fileINI As String
            Dim ficheiroConfiguracao As Configuracao
            fileINI = Application.StartupPath + "\\CONFIG.ini"
            ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
            Dim m As MotorLM
            m = MotorLM.GetInstance
            listaFornecedores = New ArrayList

            dtpDataFatura.Format = DateTimePickerFormat.Short
            dtpDataFatura.Value = Now
            dtpDataFatura.Checked = False
            modulo = "C"

            lblText.Text = m.EmpresaDescricao
            Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario
            gridArtigos.Icons = AxImageManager1.Icons
            gridArtigosAlt.Icons = AxImageManager1.Icons
            gridArtigosCOT.Icons = AxImageManager1.Icons
            gridArtigosENC.Icons = AxImageManager1.Icons
            gridDocsENC.Icons = AxImageManager1.Icons

            dtPicker.Tag = ""
            ssRodape.Tag = ""

            Dim doc As Objeto
            doc = New Objeto
            doc.BaseDados = m.Empresa

            gridArtigos.Tag = doc
            gridArtigosCOT.Tag = doc
            gridArtigosENC.Tag = doc
            gridArtigosENT.Tag = ""

            'HASTABLES COM AS  LINHAS dos IDS das Linhas

            hashLST = New Hashtable
            hashCOT = New Hashtable
            hashENC = New Hashtable
            hashLSTPecas = New Hashtable

            htLinhaModificadas = New Hashtable

            emModoEdicao = False

            iniciarToolBox()
            inicializarFornecedores()
            carregarFornecedoresLinha("")

            gridArtigos.AutoColumnSizing = True
            gridArtigosAlt.AutoColumnSizing = True

            gridArtigosCOT.AutoColumnSizing = True
            gridDocsCOT.AutoColumnSizing = True

            gridArtigosENC.AutoColumnSizing = True
            gridDocsENC.AutoColumnSizing = True

            'inserirColunasRegistos(gridArtigos)
            inserirColunasRegistos(m.InformModLstMaterial.TIPODOCLM, gridArtigos.Name, gridArtigos, True, mGridLST)

            inserirColunasRegistosAlt(m.InformModLstMaterial.TIPODOCLM, gridArtigos.Name, gridArtigosAlt, True, mGridLSTAlt)

            'inserirColunasRegistos(gridArtigosAlt, True, mGridLST)
            'inserirColunasRegistos(gridArtigosCOT)
            inserirColunasRegistos(m.InformModLstMaterial.TIPODOCCOT, gridArtigosCOT.Name, gridArtigosCOT, True, mGridCOT)
            'inserirColunasRegistos(gridDocsCOT, True, mGridECF)
            inserirColunasRegistosDocs(gridDocsCOT)

            'inserirColunasRegistos(gridArtigosENC)
            inserirColunasRegistos(m.InformModLstMaterial.TIPODOCECF, gridArtigosENC.Name, gridArtigosENC, True, mGridENC)
            inserirColunasRegistosDocs(gridDocsENC)

            inserirColunasRegistos(m.InformModLstMaterial.TIPODOCSTK, gridArtigosENT.Name, gridArtigosENT, True, mGridENT)


            '  inserirColunasRegistos(2, gridArtigosStock, m.getApresentaProjectos)

            gridArtigosCOT.AllowEdit = True
            gridArtigosCOT.EditOnClick = True

            lstFornecedor.CheckOnClick = True

            gridArtigos.AllowEdit = True
            gridArtigos.EditOnClick = True



            gridArtigosAlt.AllowEdit = False
            gridArtigosAlt.EditOnClick = False

            gridArtigosENC.AllowEdit = True
            gridArtigosENC.EditOnClick = True

            gridArtigosENT.AllowEdit = True
            gridArtigosENT.EditOnClick = True


            tabCtlMaterial.TabPages(0).Tag = m.InformModLstMaterial.TIPODOCLM + ";" + m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + m.InformModLstMaterial.TIPODOCLM + "'")
            tabCtlMaterial.TabPages(1).Tag = m.InformModLstMaterial.TIPODOCCOT + ";" + m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + m.InformModLstMaterial.TIPODOCCOT + "'")
            tabCtlMaterial.TabPages(2).Tag = m.InformModLstMaterial.TIPODOCECF + ";" + m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + m.InformModLstMaterial.TIPODOCECF + "'")

            gridArtigos.SelectionEnable = False
            ' gridArtigosCOT.SelectionEnable = False
            'gridArtigosENC.SelectionEnable = False
            gridArtigosAlt.SelectionEnable = False

            Group1.Text = "Lista Material"

            actualizarToolbar()

            configurarAplicacao()

            resizeAPL()

            lblUtilActualizacaoLST.Text = ""
            lblUtilGravacaoLST.Text = ""

            lblUtilActualizacaoCOT.Text = ""
            lblUtilGravacaoCOT.Text = ""

            lblUtilActualizacaoENC.Text = ""
            lblUtilGravacaoENC.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub actualizarToolbar()


        Dim m As MotorLM
        m = MotorLM.GetInstance

        tsAberto.BackColor = Color.White
        tsPCotacao.BackColor = ColorTranslator.FromHtml("#80b3bd")
        tsInterno.BackColor = ColorTranslator.FromHtml("#" + "B48446")
        tsProblema.BackColor = ColorTranslator.FromHtml("#" + "cf5d60") 'Color.FromName(COR_VERMELHO)

        tsConferido.BackColor = ColorTranslator.FromHtml("#" + "DEE0E0")
        tsNovo.BackColor = ColorTranslator.FromHtml("#" + "0085ff")
        tsAtualizado.BackColor = ColorTranslator.FromHtml("#" + "E25F9E")
        tsMaterialCliente.BackColor = ColorTranslator.FromHtml("#" + "FF8000")
        tssubcontratado.BackColor = ColorTranslator.FromHtml("#" + "71719f")

        If m.InformModLstMaterial.corInvertida Then
            tsEncomendado.BackColor = ColorTranslator.FromHtml("#" + "ECD34A")
            tsVerificado.BackColor = ColorTranslator.FromHtml("#" + "0d9864")
            COR_ECF = "48d1cc"
            COR_VERIFICADO = "6495ed"

        Else
            tsEncomendado.BackColor = ColorTranslator.FromHtml("#" + "0d9864")
            tsVerificado.BackColor = ColorTranslator.FromHtml("#" + "ECD34A")
            COR_ECF = "6495ed"
            COR_VERIFICADO = "48d1cc"
        End If



    End Sub

    Private Sub inicializarFornecedores()
        Dim m As MotorLM
        Dim lista As StdBELista
        m = MotorLM.GetInstance()
        lstFornecedor.SelectedIndex = -1
        '   lstFornecedor.Items.Clear()
        lista = m.daListaFornecedores(txtFornecedor.Text, buscarListaFornecedores)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstFornecedor.DataSource = Nothing
        lstFornecedor.DataSource = dt
        lstFornecedor.ValueMember = "Codigo"
        lstFornecedor.DisplayMember = "Nome"
        lstFornecedor.ClearSelected()


    End Sub

    Private Function buscarListaFornecedores() As String
        Dim strForn As String
        Dim row As String
        strForn = ""
        If ckbVerTodos.Checked Then
            For Each row In listaFornecedores
                If strForn <> "" Then
                    strForn = strForn + ","
                End If
                strForn = strForn + "'" + row + "'"
            Next
            If strForn = "" Then
                strForn = "'1=0'"
            End If
            Return strForn
        Else
            Return ""
        End If
    End Function

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Novo", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Novo"

            Control = .Add(xtpControlButton, 2, " Gravar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Gravar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False

            Control = .Add(xtpControlButton, 6, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False


            Control = .Add(xtpControlButton, 3, " Enviar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Enviar"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False

            Control = .Add(xtpControlButton, 4, " Registos", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Registos"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = False

            Control = .Add(xtpControlButton, 5, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub


    Private Sub inserirColunasRegistos(documento As String, nomeGrid As String, ByVal grid As AxXtremeReportControl.AxReportControl, mostraFornecedores As Boolean, ByRef mgrid As MotorGrelhas)
        Dim m As MotorLM

        m = MotorLM.GetInstance
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = m.consulta("SELECT * FROM TDU_GP_CONFIGGRID WHERE CDU_APL='M' and CDU_FORM='" + Me.Name + "' and CDU_GRID='" + nomeGrid + "' ORDER BY CDU_ORDEM ASC")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        mgrid = New MotorGrelhas("C", dt, m.FuncionarioAdm, mostraFornecedores, documento)
        mgrid.inserirColunas(grid, True)

    End Sub

    Private Sub inserirColunasRegistosAlt(documento As String, nomeGrid As String, ByVal grid As AxXtremeReportControl.AxReportControl, mostraFornecedores As Boolean, ByRef mgrid As MotorGrelhas)
        Dim m As MotorLM

        m = MotorLM.GetInstance
        Dim lista As StdBELista
        Dim dt As DataTable
        ' lista = m.consulta("SELECT * FROM TDU_GP_CONFIGGRID WHERE CDU_APL='M' and CDU_FORM='" + Me.Name + "' and CDU_GRID='" + nomeGrid + "' ORDER BY CDU_ORDEM ASC")
        lista = m.consulta("SELECT * FROM TDU_GP_CONFIGGRID WHERE CDU_APL='M' and CDU_FORM='" + Me.Name + "' and CDU_GRID='" + nomeGrid + "' and CDU_Campo<>'DataEntrega'  and CDU_Campo<>'CDU_DataRecepcao' UNION Select newid(),'M','FrmListaEncomendas','gridArtigos','CDU_FuncAlteracao','Funcionário',100,NULL,10, NULL,NULL UNION Select newid(),'M','FrmListaEncomendas','gridArtigos','DataEntrega','Data Ult. Mod.',100,NULL,(select count(*) FROM TDU_GP_CONFIGGRID  WHERE CDU_APL='M' and CDU_FORM='" + Me.Name + "' and CDU_GRID='" + nomeGrid + "'), NULL,NULL ORDER BY CDU_ORDEM ASC")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        mgrid = New MotorGrelhas("C", dt, m.FuncionarioAdm, mostraFornecedores, documento)
        mgrid.inserirColunas(grid, False)

    End Sub


    Private Sub inserirColunasRegistos(ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim Column As ReportColumn

        Column = inserirColuna(grid, COLUMN_PECA, "Peça", tamanhoColuna(COLUMN_PECA, grid), False, True)
        Column.EditOptions.AllowEdit = True
        Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", tamanhoColuna(COLUMN_DESCRICAO, grid), False, True)
        Column.EditOptions.AllowEdit = True
        Column = inserirColuna(grid, COLUMN_QUANTIDADE, "Quant.", tamanhoColuna(COLUMN_QUANTIDADE, grid), False, True)
        Column.Alignment = xtpAlignmentRight
        Column.EditOptions.AllowEdit = True
        Column = inserirColuna(grid, COLUMN_DIMENSOES, "Dimensões", tamanhoColuna(COLUMN_DIMENSOES, grid), False, True)
        Column.EditOptions.AllowEdit = True
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(grid, COLUMN_MATERIAL, "Material", tamanhoColuna(COLUMN_MATERIAL, grid), False, True)
        Column.EditOptions.AllowEdit = True
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(grid, COLUMN_FORNECEDOR, "Fornecedor", tamanhoColuna(COLUMN_FORNECEDOR, grid), False, True)
        Column.EditOptions.AllowEdit = False
        Column.Visible = m.getApresentaColunaFornecedor And grid.Name = "gridArtigos"

        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", tamanhoColuna(COLUMN_ARTIGO, grid), False, True)
        Column.EditOptions.AllowEdit = True
        Column.EditOptions.AddExpandButton()
        Column.Visible = m.getApresentaColunaArtigo

        Column = inserirColuna(grid, COLUMN_LINHADATA, "Dt. Recep.", tamanhoColuna(COLUMN_LINHADATA, grid), False, True)
        Column.EditOptions.AllowEdit = grelhaENC(grid)
        Column.Visible = Column.EditOptions.AllowEdit
        Column.Alignment = xtpAlignmentCenter


        If m.FuncionarioAdm Then
            Column = inserirColuna(grid, COLUMN_CHECKBOX, "", 20, False, True)
            Column.EditOptions.AllowEdit = True
            Column.Alignment = xtpAlignmentRight
            Column.Icon = 1
        End If

    End Sub


    Private Function grelhaPai(grid As AxXtremeReportControl.AxReportControl)
        Return grid.Name = "gridArtigos"
    End Function

    Private Function grelhaENC(grid As AxXtremeReportControl.AxReportControl)
        Return grid.Name = "gridArtigosENC"
    End Function

    Private Sub inserirColunasRegistosDocs(ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ENTIDADE, "Entidade", 50, False, True)
        Column = inserirColuna(grid, COLUMN_NOME, "Nome", 120, False, True)
        Column = inserirColuna(grid, COLUMN_DATA, "Data", 70, False, True)
        Column.Alignment = xtpAlignmentCenter
        Column = inserirColuna(grid, COLUMN_NUMDOC, "Num. Doc.", 70, False, True)
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(grid, COLUMN_DATARECEPCAO, "Data .Rec.", 70, False, True)
        Column.Alignment = xtpAlignmentCenter
        Column.Visible = grid.Tag = "ENC"
        If m.FuncionarioAdm And grid.Tag = "ENC" Then
            Column = inserirColuna(grid, COLUMN_CHECKBOX_DOC, "", 20, False, True)
            Column.EditOptions.AllowEdit = True
            Column.Alignment = xtpAlignmentRight
            Column.Icon = 1
        End If

    End Sub


    Private Function tamanhoColuna(index As Integer, grid As AxXtremeReportControl.AxReportControl) As Integer
        Dim m As MotorLM
        Dim tamanho As Integer
        Dim tamanhoData As Integer
        m = MotorLM.GetInstance
        tamanho = 0

        If (Not m.getApresentaColunaArtigo And Not m.getApresentaColunaFornecedor) Or Not grelhaPai(grid) Then
            tamanho = 20
        End If

        If grelhaENC(grid) Then
            tamanhoData = 80
        End If


        If m.FuncionarioAdm Then
            Select Case index
                Case COLUMN_ARTIGO : Return 100
                Case COLUMN_FORNECEDOR : Return 100
                Case COLUMN_PECA : Return 69 + tamanho
                Case COLUMN_DESCRICAO : Return 230 + tamanho - tamanhoData
                Case COLUMN_QUANTIDADE : Return 60 + tamanho
                Case COLUMN_DIMENSOES : Return 155 + tamanho - IIf(grelhaENC(grid), 30, 0)
                Case COLUMN_MATERIAL : Return 69 + tamanho
                Case COLUMN_LINHADATA : Return 80 + tamanho
            End Select
        Else
            Select Case index
                Case COLUMN_PECA : Return 137
                Case COLUMN_DESCRICAO : Return 337
                Case COLUMN_QUANTIDADE : Return 109
                Case COLUMN_DIMENSOES : Return 220
                Case COLUMN_MATERIAL : Return 154
            End Select
        End If

    End Function


    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)
        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Function buscarCor(estado As String) As UInteger
        Dim cor As UInteger
        cor = COR_BRANCO

        Select Case estado
            Case "" : cor = COR_BRANCO
            Case "I" : cor = System.Convert.ToUInt32(HexToDecimal(COR_INTERNO))
            Case "P" : cor = System.Convert.ToUInt32(HexToDecimal(COR_PROBLEMA))
            Case "C" : cor = COR_COT
            Case "E" : cor = System.Convert.ToUInt32(HexToDecimal(COR_ECF)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "V" : cor = System.Convert.ToUInt32(HexToDecimal(COR_VERIFICADO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "F" : cor = System.Convert.ToUInt32(HexToDecimal(COR_CONFERIDO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "A" : cor = System.Convert.ToUInt32(HexToDecimal(COR_ANULADO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "M" : cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHAALTERACAO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "N" : cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHAINSERCAO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "L" : cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHAMATERIALCLIENTE)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "S" : cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHASUBCONTRATADO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "B" : cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHABLOQUEADO)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))
            Case "R" : cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHAREMOVIDA)) 'System.Convert.ToUInt32(HexToDecimal("6495ed"))

        End Select
        Return cor
    End Function


    Private Function GetHexColor(colorObj As System.Drawing.Color) As String
        Return "#" & Hex(colorObj.R) & Hex(colorObj.G) & Hex(colorObj.B)
    End Function


    Public Function HexToDecimal(ByVal HexString As String) As Integer
        Dim HexColor As Char() = HexString.ToCharArray()
        Dim DecimalColor As Integer = 0
        Dim iLength As Integer = HexColor.Length - 1
        Dim iDecimalNumber As Integer

        Dim cHexValue As Char
        For Each cHexValue In HexColor
            If Char.IsNumber(cHexValue) Then
                iDecimalNumber = Integer.Parse(cHexValue.ToString())
            Else
                iDecimalNumber = Convert.ToInt32(cHexValue) - 55
            End If

            DecimalColor += iDecimalNumber * (Convert.ToInt32(Math.Pow(16, iLength)))
            iLength -= 1
        Next cHexValue
        Return DecimalColor
    End Function


    Private Sub inserirLinhaRegisto(validaChecked As Boolean, hashTable As Hashtable, ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas, Optional indexLinha As Integer = -1, Optional nova As Boolean = False)
        ' Dim m As MotorLM
        'm = MotorLM.GetInstance
        Dim cor As UInteger
        If dtRow("CDu_ObraN1").ToString() = "" And dtRow("Id").ToString() = "" Then Exit Sub
        cor = buscarCor(dtRow("Estado").ToString())
        Dim datasVazias As Boolean
        datasVazias = grid.Name = "gridArtigos"
        Dim doc As Objeto
        If grid.Name <> "gridArtigosENT" Then
            doc = grid.Tag
        Else
            doc = gridArtigosENC.Tag
        End If
        mgrid.inserirLinha(grid, dtRow, dtRow("Estado").ToString(), cor, indexLinha, hashTable, nova, validaChecked, datasVazias, doc)
    End Sub


    Private Sub inserirLinhaRegistoAlt(ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas, dtrowBase As DataRow)
        mgrid.inserirLinhaAlt(grid, dtRow, dtrowBase, txtProjecto.Text)
    End Sub


    Public Function NuloToDate(ByVal obj) As Date
        If IsDBNull(obj) Then
            NuloToDate = CDate(Now.Date.ToShortDateString)
        Else
            If Not IsDate(obj) Then
                NuloToDate = CDate(Now.Date.ToShortDateString)
            Else
                NuloToDate = CDate(CDate(obj).ToShortDateString)
            End If
        End If

    End Function
    Public Function NuloToString(ByVal obj) As String
        Dim strOjb As String
        strOjb = obj.ToString()
        If IsDBNull(obj) Then
            NuloToString = ""
        Else
            If strOjb = "NULL" Then
                NuloToString = ""
            Else
                NuloToString = CStr(obj)
            End If
        End If
    End Function


    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                If CStr(obj).Trim() = "" Then
                    NuloToDouble = 0
                Else
                    Try
                        obj = Evaluer(obj)
                        If IsNumeric(obj) Then
                            NuloToDouble = CDbl(obj)
                        Else
                            NuloToDouble = 0
                        End If
                    Catch ex As Exception
                        NuloToDouble = 0
                    End Try
                End If
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Function Evaluer(ByVal Txt As String) As String
        Dim i As Integer, oNB As Integer, fNB As Integer
        Dim P1 As Integer, P2 As Integer
        Dim Buff As String
        Dim T As String
        'Pour les calculs y faut un point à la place de la virgule
        Txt = Replace(Txt, ",", ".")
        'Voir s'il y a des (
        For i = 1 To Len(Txt)
            If Mid(Txt, i, 1) = "(" Then oNB = oNB + 1
        Next i
        'S'il y a des ( (ouvrantes), voir si elle sont validée par  des ) (fermantes)
        If oNB > 0 Then
            For i = 1 To Len(Txt)
                If Mid(Txt, i, 1) = ")" Then fNB = fNB + 1
            Next i
        Else
            'Pas de parenthèse, Evalue directement le calcul
            Evaluer = EvalueExpression(Txt)
            Exit Function
        End If
        If oNB <> fNB Then
            Evaluer = ""
            'Les parenthèses ne sont pas concordantes, mettre  message erreur parenthèse
            Exit Function
        End If

        While oNB > 0
            'recherche la dernière parenthèse ouvrante
            P1 = InStrRev(Txt, "(")
            'Recherche la parenthèse fermante de l'expression
            P2 = InStr(Mid(Txt, P1 + 1), ")")
            'Evalue l'expression qui est entre parenthèses
            Buff = EvalueExpression(Mid(Txt, P1 + 1, P2 - 1))
            'Remplacer l'expression par le résultat et supprimer les parenthèses
            Txt = Mid(Txt, P1 - 1) & Buff & Mid(Txt, P1 + P2 + 1)
            oNB = oNB - 1
        End While
        'plus de parenthèse, évaluer la dernière expression
        Evaluer = EvalueExpression(Txt)

    End Function
    Function EvalueExpression(a As String) As String
        Dim T As Integer, S As Integer
        Dim B As String, i As Integer, C As Boolean
        Dim c1 As Double, c2 As Double, Signe As Integer
        Dim R As String, Fin As Boolean, z As Integer

        'enlever les espace
        a = Replace(a, " ", "")

        While Not Fin
            For i = 1 To Len(a)
                T = Asc(Mid(a, i, 1))
                If T < 48 And T <> 46 Or i = Len(a) Then
                    If C Then 'évalue
                        If i = Len(a) Then
                            c2 = Val(Mid(a, S))
                        Else
                            c2 = Val(Mid(a, S, i - S))
                        End If
                        R = Str(CalculSimple(c1, c2, Signe))
                        If i = Len(a) Then
                            Fin = True
                        Else
                            a = Trim(R & Mid(a, i))
                            C = False
                        End If
                        Exit For
                    Else 'sépare le 1er chiffre
                        c1 = Val(Mid(a, i - 1))
                        Signe = T
                        S = i + 1
                        C = True
                    End If
                End If
            Next i
        End While
        'remplacer l'expression par le résultat
        EvalueExpression = Trim(R)
    End Function

    Function CalculSimple(n1 As Double, n2 As Double, Signe As Integer) As Double
        Select Case Signe
            Case 43 ' +
                CalculSimple = n1 + n2
            Case 45 ' -
                CalculSimple = n1 - n2
            Case 42 ' *
                CalculSimple = n1 * n2
            Case 47 ' /
                CalculSimple = n1 / n2
                'Ici, ajouter d'autre calcul...
        End Select
    End Function


    Private Sub inserirLinhaDocsRegisto(baseDados As String, projeto As String, Id As String, entidade As String, nome As String, Data As Date, serie As String, numdoc As Long, dataRecepcao As String, ByVal grid As AxXtremeReportControl.AxReportControl, Optional descricao As String = "")
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim cor As String
        Dim cor2 As String = ""
        Dim conf As String

        Dim o As Objeto
        o = New Objeto

        o.Id = Id
        o.BaseDados = baseDados
        o.Projeto = projeto


        cor = buscarCorDocumento(grid, o, cor2)

        record = grid.Records.Add()

        record.Tag = o

        Item = record.AddItem(entidade)
        Item.BackColor = cor2
        Item = record.AddItem(nome)
        Item.BackColor = cor
        Item = record.AddItem(Data.ToString("dd-MM-yyyy"))
        Item.BackColor = cor

        Item = record.AddItem(numdoc)
        Item.BackColor = cor

        If IsDate(dataRecepcao) Then
            Item = record.AddItem(CDate(dataRecepcao))
        Else
            Item = record.AddItem(dataRecepcao)
        End If

        Item.BackColor = cor

        If grid.Tag = "ENC" Then
            Item = record.AddItem("")
            Item.HasCheckbox = True
            Item.Checked = conf = "true"
            ' Item.Editable = conf <> "true"
            Item.BackColor = cor
        End If
    End Sub

    Private Function isEditable(m As MotorLM, dtRow As DataRow) As Boolean
        If dtRow("ID").ToString = "" Then Return True
        Return m.consultaValor("SELECT ID from LinhasCompras where idLinhaOrigemCopia='" + dtRow("ID").ToString + "'") = "" And m.FuncionarioAdm
    End Function

    'Private Function buscarPrecoArtigo(artigo As String) As Double
    '    Dim m As MotorLM
    '    m = MotorLM.GetInstance
    '    Return m.buscarPrecoArtigo(artigo)
    'End Function
    'Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

    '    buscarArtigos(gridArtigos, txtArtigo.Text)

    'End Sub

    Private Function buscarIndexLinhaSeleccionada(grid As AxXtremeReportControl.AxReportControl) As Integer
        If grid.FocusedRow Is Nothing Then
            Return -1
        Else
            Return grid.FocusedRow.Index
        End If
    End Function

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Id
            Case 1 : actualizar(True, True)
            'Case 2 : gravarDocumento(0)
            '    '   Case 3 : enviarEmailDocumento()
            Case 4 : abrirEditorRegistos()
            Case 5 : If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then Me.Close()
            Case ID_INSERIRLINHA : inserirNovaLinhaRegisto(False, gridArtigos, mGridLST, hashLST, buscarIndexLinhaSeleccionada(gridArtigos))
            Case ID_APAGARLINHA : apagarLinhaRegisto(hashLST, gridArtigos)
            Case ID_INSERIRLINHACOT : inserirNovaLinhaRegisto(False, gridArtigosCOT, mGridCOT, hashCOT, buscarIndexLinhaSeleccionada(gridArtigosCOT))
            Case ID_APAGARLINHACOT : apagarLinhaRegisto(hashCOT, gridArtigosCOT)
            Case ID_INSERIRLINHAENC : inserirNovaLinhaRegisto(True, gridArtigosENC, mGridENC, hashENC, buscarIndexLinhaSeleccionada(gridArtigosENC))
            Case ID_APAGARLINHAENC : apagarLinhaRegisto(hashENC, gridArtigosENC)
            Case ID_ARTIGOINTERNO : actualizarEstadoLinha(ESTADO_INTERNO)
            Case ID_ARTIGOPROBLEMA : actualizarEstadoLinha(ESTADO_PROBLEMAS)
            Case ID_ARTIGONORMAL : actualizarEstadoLinha(ESTADO_NORMAL)
            Case ID_ARTIGOCLIENTE : actualizarEstadoLinha(ESTADO_CLIENTE)
            Case ID_ARTIGOENCOMENDADO : actualizarEstadoLinha(ESTADO_ECF)
            Case ID_ARTIGOPEDIDOCOTACAO : actualizarEstadoLinha(ESTADO_COT)
            Case ID_ARTIGOVERIFICADO : actualizarEstadoLinha(ESTADO_VER)
            Case ID_ARTIGOSUBCONTRATADO : actualizarEstadoLinha(ESTADO_SUB)
            Case ID_ARTIGOBLOQUEADO : actualizarEstadoLinha(ESTADO_BLOQ)
            Case ID_REABRIR : actualizarEstadoDocumento(True, "Anulado", "reaberto")
            Case ID_ANULAR : actualizarEstadoDocumento(False, "Anulado", "anulado")
            Case ID_FECHAR : actualizarEstadoDocumento(False, "Fechado", "fechado")
            Case ID_ACTIVAR : actualizarEstadoDocumento(True, "Fechado", "activado")
        End Select
    End Sub
    Private Function gravarDocumento(apresentaPergunta As Boolean, botao As Integer, Optional fornecedor As String = "", Optional eparaactualizar As Boolean = True) As Boolean
        Dim m As MotorLM
        m = MotorLM.GetInstance

        Dim docOrig As New Objeto
        Dim doc As New Objeto
        docOrig = buscarObjetoDocumento(tabCtlMaterial.SelectedIndex, botao)

        If modulo = "C" Then doc = gravarDocumentoCompra(apresentaPergunta, docOrig, tabCtlMaterial.SelectedIndex, botao, buscarTipoDocumento(tabCtlMaterial.SelectedIndex, botao), fornecedor, txtDocRecepcao.Text)
        If modulo = "V" Then doc = gravarDocumentoVenda(apresentaPergunta, docOrig)

        enviarEmailInterno(botao, doc, docOrig)

        If botao = 0 Then
            gridArtigos.Tag = doc
        End If
        Return doc.Id <> ""

    End Function

    Private Function buscarObjetoDocumento(index As Integer, botao As Integer) As Objeto

        If botao = index Then
            Select Case index
                Case 0 : Return gridArtigos.Tag
                Case 1 : Return gridArtigosCOT.Tag
                Case 2 : Return gridArtigosENC.Tag
            End Select
        Else
            Dim o As Objeto
            Dim doc As Objeto
            Select Case index
                Case 0 : doc = gridArtigos.Tag
                Case 1 : doc = gridArtigosCOT.Tag
                Case 2 : doc = gridArtigosENC.Tag
            End Select

            o = New Objeto
            o.BaseDados = doc.Empresa
            o.Projeto = doc.Projeto
            Return o
        End If

    End Function


    Private Sub actualizar(Optional ByVal pergunta As Boolean = False, Optional limpaProjecto As Boolean = False, Optional limpatudo As Boolean = True)

        listaFornecedores = New ArrayList
        txtDocRecepcao.Text = ""

        Dim m As MotorLM
        m = MotorLM.GetInstance


        If Not limpatudo Then
            inicializarFornecedores()
            selecionarTodos(False, gridArtigosCOT)
            selecionarTodos(False, gridArtigosENC)
            selecionarTodos(False, gridArtigos)
            gridArtigosCOT.Populate()
            gridArtigosENC.Populate()
            gridArtigos.Populate()
            Exit Sub
        End If

        If pergunta Then
            If MsgBox("Deseja realmente actualizar e limpar a informação existente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        End If

        If limpaProjecto Then
            txtProjecto.Text = ""
            txtNomeProjecto.Text = ""
        End If

        'doc = Nothing
        Dim doc As Objeto
        doc = New Objeto
        doc.BaseDados = m.Empresa

        txtFornecedor.Text = ""
        gridArtigosCOT.Tag = doc
        gridArtigosENC.Tag = doc
        gridArtigos.Tag = doc
        gridArtigosENT.Tag = ""

        ' gridArtigos.Records.DeleteAll()
        gridArtigosCOT.Records.DeleteAll()
        gridArtigosCOT.Populate()
        gridDocsCOT.Records.DeleteAll()
        gridDocsCOT.Populate()

        gridArtigosENC.Records.DeleteAll()
        gridArtigosENC.Populate()
        gridDocsENC.Records.DeleteAll()
        gridDocsENC.Populate()

        gridArtigos.Records.DeleteAll()
        gridArtigos.Populate()

        gridArtigosAlt.Records.DeleteAll()
        gridArtigosAlt.Populate()


        inicializarFornecedores()




        txtObservacoesLST.Text = ""
        txtObservacoesCOT.Text = ""
        txtObservacoesENC.Text = ""

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"


    End Sub


    Private Function gravarDocumentoCompra(ByVal apresentaPergunta As Boolean, doc As Objeto, index As Integer, botao As Integer, Optional tipodoc As String = "", Optional fornecedor As String = "", Optional docRecep As String = "") As Objeto



        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim descricaoDocumento As String = ""
        Dim segundoDoc As Boolean
        segundoDoc = False
        If tipodoc = "" Then
            Exit Function
            ' Else
            '   segundoDoc = True
        End If

        If apresentaPergunta Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")
            If MsgBox("Deseja realmente gravar a " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                doc.Id = ""
                Return doc
            End If
        End If

        Me.Cursor = Cursors.WaitCursor

        Dim forn As Fornecedor
        forn = carregarDadosFornecedor(fornecedor)

        Dim txt As String
        txt = ""
        Select Case tabCtlMaterial.SelectedIndex
            Case 0 : txt = txtObservacoesLST.Text
            Case 1 : txt = txtObservacoesCOT.Text
            Case 2 : txt = txtObservacoesENC.Text
        End Select

        If botao = 0 Then
            txt = ""
        End If

        Dim conferido As Boolean
        Dim datafatura As String
        If tabCtlMaterial.SelectedIndex = 2 Then
            conferido = gridDocsENC.SelectedRows(0).Record(COLUMN_CHECKBOX_DOC).Checked
            datafatura = IIf(dtpDataFatura.Checked, dtpDataFatura.Value, "")
        Else
            conferido = False
            datafatura = ""
        End If

        Dim mgrid As MotorGrelhas

        Dim responsavel As String = ""
        htLinhaModificadas = New Hashtable

        mgrid = buscarObjectoGrelhas(index, botao)

        Dim recordsGrid As Object

        Dim empresa As String
        Dim projeto As String
        empresa = ""
        projeto = ""

        recordsGrid = buscarLinhasArtigosSeleccionados(index, botao, empresa, projeto)

        If botao = 2 And index <> 2 Then
            Dim f As FrmListaEncomendasECF
            f = New FrmListaEncomendasECF
            f.setRecordsGrid(recordsGrid)
            f.setFornecedor(forn)
            f.setdocumentoOrigem(IIf(index = 0, m.InformModLstMaterial.TIPODOCLM, m.InformModLstMaterial.TIPODOCECF))
            f.setDocOrigem(doc)
            f.ShowDialog()

            If f.seleccionouOk Then
                recordsGrid = f.buscarLinhasArtigos()
                mgrid = f.getMotorGrid
                responsavel = f.txtResponsavel.Text
            Else
                Me.Cursor = Cursors.Default
                doc.Id = ""
                Return doc
            End If

        End If

        If Not doc Is Nothing And empresa <> "" And projeto <> "" Then
            doc.BaseDados = empresa
            doc.Projeto = projeto
        End If

        'If doc.BaseDados = "" Then
        '    doc.BaseDados = empresa
        'End If



        doc.Id = m.actualizarDocumentoCompra(htLinhaModificadas, mgrid, forn, tipodoc, m.Funcionario, txtProjecto.Text, txt, recordsGrid, datafatura, doc, botao, index, conferido, responsavel, carregouLista, docRecep)

        Me.Cursor = Cursors.Default

        If doc.Id <> "" And apresentaPergunta And segundoDoc = False Then
            MsgBox(descricaoDocumento + " gravada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Me.Cursor = Cursors.Default

        Return doc
    End Function


    Private Function buscarIndexCampo(grid As AxXtremeReportControl.AxReportControl, campo As String) As Integer
        Select Case grid.Name
            Case "gridArtigos" : Return mGridLST.buscarIndexCampo(campo)
            Case "gridArtigosCOT" : Return mGridCOT.buscarIndexCampo(campo)
            Case "gridArtigosENC" : Return mGridENC.buscarIndexCampo(campo)
        End Select

    End Function

    Private Function buscarObjectoGrelhas(index As Integer, botao As Integer) As MotorGrelhas
        Select Case index
            ' Lista material
            Case 0 : Return mGridLST
            ' pedido cotacao
            Case 1 : Return mGridCOT
            ' encomenda
            Case 2 : Return mGridENC

        End Select

    End Function

    Private Function buscarLinhasArtigosSeleccionados(index As Integer, botao As Integer, ByRef empresa As String, ByRef projeto As String) As Object
        Dim lista As ArrayList
        lista = New ArrayList

        Dim grid As AxXtremeReportControl.AxReportControl

        Select Case index
            ' Lista material
            Case 0 : grid = gridArtigos
            ' pedido cotacao
            Case 1 : grid = gridArtigosCOT
            ' encomenda
            Case 2 : grid = gridArtigosENC

        End Select

        empresa = ""
        If botao = 0 Then Return grid.Records

        If index = botao Then Return grid.Records
        Dim row As ReportRecord

        For Each row In grid.Records
            If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then

                index = buscarIndexCampo(grid, "DATAENTREGA")
                If index <> -1 Then
                    row(index).Value = Now
                End If

                lista.Add(row)


                If empresa = "" Then
                    index = buscarIndexCampo(grid, "CDU_EMPRESA")
                    If index <> -1 Then
                        empresa = row(index).Value
                    End If

                End If

                If projeto = "" Then
                    index = buscarIndexCampo(grid, "CDU_PROJETOASSOCIADO")
                    If index <> -1 Then
                        projeto = row(index).Value
                    End If

                End If


            End If
        Next

        Return lista


    End Function

    Private Function carregarDadosFornecedor(Optional fornecedor As String = "") As Fornecedor
        Dim f As Fornecedor
        Dim m As MotorLM
        m = MotorLM.GetInstance

        f = New Fornecedor

        If fornecedor = "" Then
            f.Fornecedor = m.getEntidadeDefault
        Else
            f.Fornecedor = fornecedor
        End If

        f.Nome = m.consultaValor("SELECT Nome from Fornecedores WHERE Fornecedor='" + f.Fornecedor + "'")

        Return f
    End Function

    Private Function gravarDocumentoVenda(ByVal apresentaPergunta As Boolean, o As Objeto, Optional tipodoc As String = "") As Objeto
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim descricaoDocumento As String
        Dim segundoDoc As Boolean
        segundoDoc = False
        If tipodoc = "" Then
            'tipodoc = lstDocumentos.SelectedValue
        Else
            segundoDoc = True
        End If

        descricaoDocumento = m.consultaValor("SELECT Descricao FROM " + o.BaseDados + "..DocumentosVenda WHERE Documento='" + tipodoc + "'")

        If apresentaPergunta Then
            If MsgBox("Deseja realmente gravar a " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                o.Id = ""
                Return o
            End If
        End If



        Me.Cursor = Cursors.WaitCursor

        Dim registo As New Registo
        ' registo.Entidade = txtFornecedor.Text
        ' registo.TipoEntidade = "C"
        '   idCabecDoc = m.actualizarDocumentoVenda(registo, Now)

        '   idCabecDoc = m.actualizarDocumentoVenda(cli, tipodoc, txtFornecedor.Text, m.Funcionario, lstMoldes.SelectedValue, txtObservacoes.Text, dtpData.Value, gridArtigosStock.Records, txtMatricula.Text, txtLocalCarga.Text, txtLocalDescarga.Text, DtpDataCarga.Value, DtpDataDescarga.Value, txtHoraCarga.Text, txtHoraDescarga.Text, idCabecDoc)

        Me.Cursor = Cursors.Default

        If o.Id <> "" And apresentaPergunta And segundoDoc = False Then
            MsgBox(descricaoDocumento + " gravada realidada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If

        Me.Cursor = Cursors.Default

        Return o
    End Function

    Private Sub gridArtigos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigos.RowDblClick
        ' enviarArtigoParaSaida()
    End Sub

    Private Sub gridArtigosStock_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent)
        ' retirarArtigoSaida()
        Dim f As FrmQtDescr
        f = New FrmQtDescr
        f.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
        f.ValorNome = e.row.Record.Item(COLUMN_DESCRICAO).Value
        f.ShowDialog()
        If f.BotaoSeleccionado = 1 Then
            e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Valor
            e.row.Record.Item(COLUMN_DESCRICAO).Value = f.ValorNome
        End If
        f = Nothing
        Me.Activate()
        gridArtigosCOT.Populate()
    End Sub


    Private Sub abrirEditorRegistos()
        Dim f As FrmEditarEncomendas
        f = New FrmEditarEncomendas
        f.TipoDocumento = tabCtlMaterial.SelectedTab.Tag
        f.ShowDialog()
        If f.clicouConfirmar Then
            emModoEdicao = True
            actualizarFormularioLM(f.getIdLinha, f.getValorLinha(0), f.getValorLinha(6))
            emModoEdicao = False
        End If
    End Sub

    Dim emModoEdicao As Boolean
    Private Sub actualizarFormularioLM(ByVal id As String, ByVal documento As String, ByVal projecto As String)
        Me.Cursor = Cursors.WaitCursor
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim entidade As String
        Dim nome As String
        Dim observacoes As String
        Dim str As String()

        entidade = ""
        nome = ""
        observacoes = ""
        Dim obs As String

        obs = m.buscarDocumentoCompraValorAtributo(id, "Observacoes")

        Dim dataListaMat As String
        dataListaMat = m.buscarDocumentoCompraValorAtributo(id, "CDU_DataListaMat")



        lblDataUltCarregamentoLista.Text = ""
        str = Split(obs, vbLf)
        txtObservacoesLST.Lines = str
        If dataListaMat <> "" Then
            If IsDate(dataListaMat) Then
                lblDataUltCarregamentoLista.Text = "Data Lista : " + CDate(dataListaMat).ToShortDateString
            End If
        Else
            lblDataUltCarregamentoLista.Text = "Data Lista : " + obs = m.buscarDocumentoCompraValorAtributo(id, "DataIntroducao") 'doc.DataIntroducao.ToShortDateString
        End If


        Dim dtRegistos As DataTable
        dtRegistos = Nothing


        Dim o As Objeto
        o = New Objeto

        o.Id = id
        o.BaseDados = m.Empresa
        o.Projeto = projecto

        gridArtigos.Tag = o

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(o, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(o, "V")

        Dim dtrow As DataRow
        gridArtigos.Records.DeleteAll()

        hashLST = New Hashtable
        hashLSTPecas = New Hashtable()

        emModoEdicao = True
        ProgressBar1.Visible = True
        ProgressBar1.Value = 0
        ProgressBar1.Maximum = dtRegistos.Rows.Count

        For Each dtrow In dtRegistos.Rows
            If lstFornecedor.SelectedValue Is Nothing Then
                If m.NuloToString(dtrow("Codigo")) <> "" Then lstFornecedor.SelectedValue = m.NuloToString(dtrow("Codigo"))
            End If

            inserirLinhaRegisto(False, hashLST, dtrow, gridArtigos, mGridLST)
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next

        If dtRegistos.Rows.Count > 0 Then actualizarGrelhaListaAlteracoes(dtRegistos.Rows(0)("ID").ToString(), dtRegistos.Rows(0), True)
        emModoEdicao = False
        ProgressBar1.Visible = False
        txtArtigo.Text = ""

        gridArtigos.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"

        actualizarDadosUtilizadores(o, lblUtilGravacaoLST, lblUtilActualizacaoLST)
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub actualizarDadosUtilizadores(o As Objeto, lblGrav As Label, lblActu As Label)

        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim nome As String

        Dim utilizador As String

        utilizador = m.consultaValor("SELECT Utilizador FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")

        lblActu.Text = "Util. Act. "

        nome = m.consultaValor("SELECT NomeAbreviado as Nome  FROM " + o.BaseDados + "..APS_GP_Funcionarios WHERE Codigo='" + utilizador + "'")
        If nome = "" Then
            nome = m.consultaValor("SELECT * FROM PRIEMPRE..Utilizadores WHERE Codigo='" + utilizador + "'")
            If nome = "" Then
                nome = utilizador
                lblActu.Text = "Responsável "
            End If
        End If


        lblActu.Text += nome

        lblGrav.Text = "Util. Grav. "

        utilizador = m.consultaValor("SELECT CDU_CabVar1 FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")
        nome = m.consultaValor("SELECT NomeAbreviado as Nome FROM " + o.BaseDados + "..APS_GP_Funcionarios WHERE Codigo='" + utilizador + "'")
        If nome = "" Then
            nome = m.consultaValor("SELECT Codigo FROM PRIEMPRE..Utilizadores WHERE Codigo='" + utilizador + "'")
            If nome = "" Then
                nome = utilizador
                lblGrav.Text = "Responsável "
            End If
        End If

        lblGrav.Text += nome

    End Sub


    Private Sub actualizarGrelhaListaAlteracoes(idLinha As String, dtrowBase As DataRow, apresentaLinhaActiva As Boolean)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim dtRegistos As DataTable
        dtRegistos = Nothing

        gridArtigosAlt.Records.DeleteAll()
        '  apresentaLinhaActiva = False

        If Trim(idLinha) <> "" Then
            dtRegistos = m.daListaArtigosLinhaOriginal(idLinha, "C")
            If Not dtRegistos Is Nothing Then
                For Each dtrow In dtRegistos.Rows
                    If apresentaLinhaActiva Then
                        inserirLinhaRegistoAlt(dtrow, gridArtigosAlt, mGridLSTAlt, dtrowBase)
                    End If
                    apresentaLinhaActiva = True
                Next
            End If
        End If
        gridArtigosAlt.Populate()

    End Sub

    Private Sub actualizarGrelhaListaAlteracoes(grid As AxXtremeReportControl.AxReportControl, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent)

        If Not grid.HitTest(e.x, e.y).Column Is Nothing Then
            If grid.HitTest(e.x, e.y).ht = XTPReportHitTestInfoCode.xtpHitTestReportArea Then
                Dim dtRow As DataRow
                dtRow = grid.HitTest(e.x, e.y).Row.Record.Tag
                actualizarGrelhaListaAlteracoes(dtRow("Id").ToString, dtRow, True)
            End If
        End If

    End Sub



    Private Function buscarTipoDocumento(index As Integer) As String
        Dim strDoc As String
        strDoc = tabCtlMaterial.TabPages(index).Tag
        Return strDoc.Split(";")(0)
    End Function

    Private Function buscarTipoDocumento(index As Integer, botao As Integer) As String
        Dim strDoc As String
        strDoc = tabCtlMaterial.TabPages(botao).Tag
        Return strDoc.Split(";")(0)
    End Function

    Private Function buscarNomeDocumento(index As Integer, botao As Integer) As String
        Dim strDoc As String
        strDoc = tabCtlMaterial.TabPages(botao).Tag
        Return strDoc.Split(";")(1)
    End Function

    Private Function enviarEmailDocumento(index As Integer, botao As Integer, Optional fornecedor As String = "") As Boolean
        Dim objDoc As Objeto = New Objeto
        Dim objDocOrig As Objeto = New Objeto
        Dim m As MotorLM
        m = MotorLM.GetInstance

        objDocOrig = buscarObjetoDocumento(index, botao)

        Me.Cursor = Cursors.WaitCursor
        If modulo = "C" Then objDoc = gravarDocumentoCompra(False, objDocOrig, index, botao, buscarTipoDocumento(index, botao), fornecedor) 'gravarDocumentoCompra(False, buscarIdDocumento(index, botao), buscarTipoDocumento(index, botao), fornecedor)
        If modulo = "V" Then objDoc = gravarDocumentoVenda(False, objDocOrig)
        If objDoc.Id <> "" Then
            m.enviarEmailDocumento(objDoc, "C", botao)
            enviarEmailInterno(botao, objDoc, objDocOrig)
            If m.InformModLstMaterial.IMPRIMIEAOENVIAR = "1" And botao = 2 Then
                m.imprimirDocumento(objDoc, "C", Me, 1)
            End If
        End If
        Me.Cursor = Cursors.Default
        Return objDoc.Id <> ""
    End Function

    Private Function imprimirDoc(index As Integer, botao As Integer, Optional fornecedor As String = "") As Boolean
        Dim documento As New Objeto
        Dim docOriginal As New Objeto
        Dim m As MotorLM
        m = MotorLM.GetInstance


        docOriginal = buscarObjetoDocumento(index, botao)
        Me.Cursor = Cursors.WaitCursor
        If modulo = "C" Then documento = gravarDocumentoCompra(False, docOriginal, index, botao, buscarTipoDocumento(index, botao), fornecedor)
        If modulo = "V" Then documento = gravarDocumentoVenda(False, docOriginal, fornecedor)
        If documento.Id <> "" Then
            m.imprimirDocumento(documento, "C", Me)
            enviarEmailInterno(botao, documento, docOriginal)
        End If
        Me.Cursor = Cursors.Default
        Return documento.Id <> ""
    End Function

    Private Sub txtArtigo_KeyDownEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyDownEvent) Handles txtArtigo.KeyDownEvent
        If e.keyCode = Keys.Enter Then
            filtrarArtigos(gridArtigos, txtArtigo.Text) '  buscarArtigos(gridArtigos, txtArtigo.Text)
        End If
    End Sub


    Private Sub filtrarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim row As ReportRecord
        Dim i As Integer
        Dim encontrou As Boolean
        pesquisa = LCase(Trim(pesquisa))
        Me.Cursor = Cursors.WaitCursor
        For Each row In grid.Records
            row.Visible = True
            encontrou = False
            If pesquisa <> "" Then
                For i = 0 To grid.Columns.Count - 1
                    If InStr(LCase(row(i).Value), pesquisa) <> 0 Then
                        encontrou = True
                    End If
                Next
                row.Visible = encontrou
            End If
        Next
        grid.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosCOT.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub aplicarFiltro()
        filtrarArtigos(gridArtigos, txtArtigo.Text)
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click

        aplicarFiltro()
    End Sub

    'Private Sub btnEnviar_Click(sender As Object, e As EventArgs)
    '    Dim row As DataRowView
    '    If tabCtlMaterial.SelectedIndex = 0 Then
    '        For Each row In lstFornecedor.CheckedItems
    '            enviarEmailDocumento(row("Codigo"))
    '        Next
    '    End If
    '    actualizar(False, True)
    '    'enviarEmailDocumento()
    'End Sub

    Private Function buscarListaFornecedores(index As Integer) As ArrayList
        Dim lista As ArrayList
        lista = New ArrayList

        If index = 0 Then
            For Each row In lstFornecedor.CheckedItems
                lista.Add(row("Codigo"))
            Next
        Else
            If gridDocsCOT.SelectedRows.Count > 0 Then
                lista.Add(gridDocsCOT.SelectedRows(0).Record(0).Value)
            End If
        End If

        Return lista
    End Function


    Private Function apresentarPergunta(tipo As Integer, index As Integer, botao As Integer) As Boolean
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim descricaoDocumento As String
        If tipo = 0 Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + buscarTipoDocumento(index, botao) + "'")
            If MsgBox("Deseja realmente gravar o documento " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            End If
            Return True
        End If

        If tipo = 1 Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + buscarTipoDocumento(index, botao) + "'")
            If MsgBox("Deseja realmente imprimir o documento " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            End If
            Return True
        End If

        If tipo = 2 Then
            descricaoDocumento = m.consultaValor("SELECT Descricao FROM DocumentosCompra WHERE Documento='" + buscarTipoDocumento(index, botao) + "'")
            If MsgBox("Deseja realmente enviar  o documento " + descricaoDocumento + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            End If
            Return True
        End If

        Return True

    End Function

    Private Function validarGravacao(botao As Integer) As Boolean
        Dim m As MotorLM
        m = MotorLM.GetInstance
        If Not m.existeProjecto(txtProjecto.Text) Then
            MsgBox("Tem de indicar um Projecto", MsgBoxStyle.Critical)
            Return False
        End If

        If botao = 1 Then
            If Not existeArtigosSeleccionados(tabCtlMaterial.SelectedIndex) Then
                MsgBox("Não existem artigos selecionados!", MsgBoxStyle.Critical)
                Return False
            End If

            If existeArtigosBloqueados(tabCtlMaterial.SelectedIndex) Then
                MsgBox("Existem artigos Bloqueados que se estão selecionados!.", MsgBoxStyle.Critical)
                Return False
            End If

            If existeArtigosDiferentesBDProjetos(tabCtlMaterial.SelectedIndex) Then
                MsgBox("Existem artigos seleccionados para Empresas/Projetos diferentes!.", MsgBoxStyle.Critical)
                Return False
            End If

        End If

        If tabCtlMaterial.SelectedIndex = 0 And botao = 1 Then
            If Not existeFornecedoresSeleccionados() Then
                MsgBox("Não existem fornecedores selecionados!", MsgBoxStyle.Critical)
                Return False
            End If
        End If

        If botao <> 0 Then
            If existeArtigosSeleccionadosemGravacao(tabCtlMaterial.SelectedIndex) Then
                MsgBox("Não existem artigos selecionados que ainda não foram gravados! Favor de gravar primeiro o documento!", MsgBoxStyle.Critical)
                Return False
            End If
        End If



        Return True

    End Function

    Private Function existeArtigosSeleccionados(indexTAB As Integer) As Boolean
        Dim grid As AxXtremeReportControl.AxReportControl
        grid = Nothing

        If indexTAB = 0 Then
            grid = gridArtigos
        End If

        If indexTAB = 1 Then
            grid = gridArtigosCOT
        End If

        Dim b As Boolean
        Dim row As ReportRecord
        b = False
        For Each row In grid.Records
            If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then
                b = True
                Exit For
            End If
        Next
        Return b
    End Function

    Private Function existeArtigosDiferentesBDProjetos(indexTAB As Integer) As Boolean
        Dim grid As AxXtremeReportControl.AxReportControl
        Dim BD As String = "-1"
        Dim projeto As String = "-1"

        grid = Nothing

        If indexTAB = 0 Then
            grid = gridArtigos
        End If

        If indexTAB = 1 Then
            grid = gridArtigosCOT
        End If

        Dim index As Integer
        Dim b As Boolean
        Dim row As ReportRecord
        b = False
        For Each row In grid.Records
            If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then

                index = buscarIndexCampo(grid, "CDU_EMPRESA")

                If index <> -1 Then

                    If BD = "-1" Then
                        BD = row(index).Value
                    End If

                    If row(index).Value <> BD Then
                        b = True
                        Exit For
                    End If
                End If

                index = buscarIndexCampo(grid, "CDU_PROJETOASSOCIADO")
                If index <> -1 Then
                    If projeto = "-1" Then
                        projeto = row(index).Value
                    End If

                    If row(index).Value <> projeto Then
                        b = True
                        Exit For
                    End If
                End If
            End If
        Next
        Return b
    End Function


    Private Function existeArtigosBloqueados(indexTAB As Integer) As Boolean
        Dim grid As AxXtremeReportControl.AxReportControl
        grid = Nothing

        If indexTAB = 0 Then
            grid = gridArtigos
        End If

        If indexTAB = 1 Then
            grid = gridArtigosCOT
        End If

        Dim b As Boolean
        Dim row As ReportRecord
        b = False


        Dim i As Integer
        For Each row In grid.Records
            i = buscarIndexCampo(grid, "CHECKBOX")
            If row(i).Tag = "B" And row(i).Checked Then
                b = True
                Exit For
            End If
        Next
        Return b
    End Function



    Private Function existeArtigosSeleccionadosemGravacao(indexTAB As Integer) As Boolean
        Dim grid As AxXtremeReportControl.AxReportControl
        grid = Nothing

        If indexTAB = 0 Then
            grid = gridArtigos
        End If

        If indexTAB = 1 Then
            grid = gridArtigosCOT
        End If

        Dim b As Boolean
        Dim row As ReportRecord
        b = False
        For Each row In grid.Records
            Dim dt As DataRow
            dt = row.Tag
            If row(buscarIndexCampo(grid, "CHECKBOX")).Checked And dt("Id").ToString() = "" Then
                b = True
                Exit For
            End If
        Next
        Return b
    End Function

    Private Function existeFornecedoresSeleccionados() As Boolean
        Return lstFornecedor.CheckedIndices.Count <> 0
    End Function
    Private Function buscarDescricaoDocumentos() As String
        Select Case tabCtlMaterial.SelectedIndex
            Case 0 : Return "o(s) pedido(s) de Cotação?"
            Case 1 : Return "a(s) encomendas(s) a fornecedor?"
        End Select

    End Function


    'Private Sub ApresentarPerguntaCompra(idDocOrigem As String, tipoDocOrig As String)
    '    Dim tipodocDest As String
    '    Dim descricaotipodocDest As String
    '    Dim idDocumento As String
    '    Dim m As MotorLM
    '    m = MotorLM.GetInstance
    '    idDocumento = ""
    '    Dim refOrig As String
    '    tipodocDest = m.consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosCompra Where Documento='" + tipoDocOrig + "'")

    '    If tipodocDest <> "" Then
    '        descricaotipodocDest = m.consultaValor("SELECT Descricao FROM DocumentosCompra Where Documento='" + tipodocDest + "'")
    '        Dim frmP As FrmPergunta
    '        frmP = New FrmPergunta
    '        frmP.setTexto("Deseja criar o documento " + descricaotipodocDest + "?")
    '        frmP.ShowDialog()

    '        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie +'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocOrigem + "'")

    '        idDocumento = m.consultaValor("SELECT id FROM CabecCompras WHERE refDocOrig='" + refOrig + "'")

    '        ''SOMENTE GRAVAR
    '        If frmP.getBotaoSeleccionado() = 0 Then
    '            idDocumento = gravarDocumentoCompra(False, idDocumento, 0, 0, tipodocDest)
    '            If idDocumento <> "" Then
    '                actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
    '            End If
    '        End If

    '        'IMPRIMIR
    '        If frmP.getBotaoSeleccionado() = 1 Then
    '            idDocumento = gravarDocumentoCompra(False, idDocumento, 0, 0, tipodocDest)
    '            If idDocumento <> "" Then
    '                actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
    '                m.imprimirDocumento(idDocumento, "C", Me)
    '            End If
    '        End If

    '        'ENVIAR
    '        If frmP.getBotaoSeleccionado() = 2 Then
    '            idDocumento = gravarDocumentoCompra(False, idDocumento, 0, 0, tipodocDest)
    '            If idDocumento <> "" Then
    '                actualizarReferenciasDocumentoCompra(idDocOrigem, idDocumento)
    '                m.enviarEmailDocumento(idDocumento, "C", 0)
    '            End If
    '        End If
    '    End If

    'End Sub

    'Private Sub ApresentarPerguntaVenda(idDocOrigem As String, tipoDocOrig As String)
    '    Dim tipodocDest As String
    '    Dim descricaotipodocDest As String
    '    Dim idDocumento As String
    '    Dim m As MotorLM
    '    m = MotorLM.GetInstance
    '    idDocumento = ""
    '    Dim refOrig As String
    '    tipodocDest = m.consultaValor("SELECT CDU_TipoDocGerar FROM DocumentosVenda Where Documento='" + tipoDocOrig + "'")

    '    If tipodocDest <> "" Then
    '        descricaotipodocDest = m.consultaValor("SELECT Descricao FROM DocumentosVenda Where Documento='" + tipodocDest + "'")
    '        Dim frmP As FrmPergunta
    '        frmP = New FrmPergunta
    '        frmP.setTexto("Deseja criar o documento " + descricaotipodocDest + "?")
    '        frmP.ShowDialog()

    '        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie +'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocOrigem + "'")

    '        idDocumento = m.consultaValor("SELECT id FROM CabecDoc WHERE refDocOrig='" + refOrig + "'")

    '        ''SOMENTE GRAVAR
    '        If frmP.getBotaoSeleccionado() = 0 Then
    '            idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
    '            If idDocumento <> "" Then
    '                actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
    '            End If
    '        End If

    '        'IMPRIMIR
    '        If frmP.getBotaoSeleccionado() = 1 Then
    '            idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
    '            If idDocumento <> "" Then
    '                actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
    '                m.imprimirDocumento(idDocumento, "V", Me)
    '            End If
    '        End If

    '        'ENVIAR
    '        If frmP.getBotaoSeleccionado() = 2 Then
    '            idDocumento = gravarDocumentoVenda(False, idDocumento, tipodocDest)
    '            If idDocumento <> "" Then
    '                actualizarReferenciasDocumentoVenda(idDocOrigem, idDocumento)
    '                m.enviarEmailDocumento(idDocumento, "V", 0)
    '            End If
    '        End If
    '    End If

    'End Sub

    Private Sub actualizarReferenciasDocumentoCompra(idDocOrigem As String, idDocumento As String)
        Dim refOrig As String
        Dim refDest As String
        Dim m As MotorLM
        m = MotorLM.GetInstance

        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocOrigem + "'")
        refDest = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecCompras Where Id='" + idDocumento + "'")

        m.executarComando("UPDATE CabecCompras set RefDocOrig='" + refDest + "' WHERE Id='" + idDocOrigem + "'")
        m.executarComando("UPDATE CabecCompras set RefDocOrig='" + refOrig + "' WHERE Id='" + idDocumento + "'")

    End Sub

    Private Sub actualizarReferenciasDocumentoVenda(idDocOrigem As String, idDocumento As String)
        Dim refOrig As String
        Dim refDest As String
        Dim m As MotorLM
        m = MotorLM.GetInstance

        refOrig = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocOrigem + "'")
        refDest = m.consultaValor("SELECT Filial+'/'+TipoDoc+'/'+Serie+'/'+cast(numdoc as Nvarchar) as DocOrig FROM CabecDoc Where Id='" + idDocumento + "'")

        m.executarComando("UPDATE CabecDoc set RefDocOrig='" + refDest + "' WHERE Id='" + idDocOrigem + "'")
        m.executarComando("UPDATE CabecDoc set RefDocOrig='" + refOrig + "' WHERE Id='" + idDocumento + "'")

    End Sub


    'Private Sub txtFornecedor_Change(sender As Object, e As EventArgs) Handles txtFornecedor.Change
    '    Dim nome As String
    '    Dim m As MotorLM
    '    m = MotorLM.GetInstance
    '    nome = ""
    '    If modulo = "C" Then nome = m.consultaValor("SELECT nome from Fornecedores where fornecedor like '" + txtFornecedor.Text + "'")
    '    If modulo = "V" Then nome = m.consultaValor("SELECT nome from Clientes where cliente like '" + txtFornecedor.Text + "'")

    '    txtNomeForn.Text = nome
    '    If nome <> "" Then
    '        If modulo = "C" Then actualizarDadosFornecedor(txtFornecedor.Text)
    '        If modulo = "V" Then actualizarDadosCliente(txtFornecedor.Text)
    '    End If
    'End Sub


    Private Sub btnProjecto_Click(sender As Object, e As EventArgs) Handles btnProjecto.Click
        abrirFormListaProjecto()
    End Sub

    Private Sub abrirFormListaProjecto()
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim frmLista As FrmLista
        frmLista = New FrmLista
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(4)

        frmLista.setComando("SELECT TOP 100 PERCENT Codigo as 'Código',Descricao as 'Descrição' FROM COP_Obras WHERE ESTADO='ABTO' ORDER BY Codigo")
        frmLista.setCaption("Projectos")

        ' frmLista.setComando("SELECT TOP 100 PERCENT Fornecedor,Nome, Morada,Tel as 'Telefone', Fax FROM Fornecedores WHERE FornecedorAnulado=0 ORDER BY Fornecedor")
        ' frmLista.setCaption("Fornecedores")
        frmLista.ShowFilter(True)
        frmLista.setalinhamentoColunas(alinhamentoColunas)
        frmLista.ShowDialog()

        If frmLista.seleccionouOK Then
            emModoEdicao = False
            txtProjecto.Text = frmLista.getValor(0)
            'carregarDadosProjectoLM(txtProjecto.Text)
            'If m.FuncionarioAdm Then
            '    carregarDadosProjectoCOT(txtProjecto.Text)
            '    carregarDadosProjectoECF(txtProjecto.Text)
            'End If
            '' carregarDadosProjectoECF(txtProjecto.Text)
            ''  actualizarDadosFornecedor(frmLista.getValor(0))
            'MsgBox(buscarNomeDocumento(tabCtlMaterial.SelectedIndex, 0) + " do Projecto " + txtProjecto.Text + " em modo de edição", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub txtProjecto_Change(sender As Object, e As EventArgs) Handles txtProjecto.Change

        If emModoEdicao Then Exit Sub
        emModoEdicao = True
        Dim nome As String
        Dim m As MotorLM
        m = MotorLM.GetInstance
        carregouLista = False
        nome = ""
        nome = m.consultaValor("SELECT Descricao from COP_Obras where codigo like '" + txtProjecto.Text + "'")
        txtNomeProjecto.Text = nome
        If nome <> "" Then
            carregarDadosProjectoLM(txtProjecto.Text)
            If m.FuncionarioAdm Then
                carregarDadosProjectoCOT(txtProjecto.Text)
                carregarDadosProjectoECF(txtProjecto.Text)
            End If
            txtArtigo.Focus()
            MsgBox(buscarNomeDocumento(tabCtlMaterial.SelectedIndex, 0) + " do Projecto " + txtProjecto.Text + " em modo de edição", MsgBoxStyle.Information)
        Else
            actualizar()
        End If
        emModoEdicao = False
    End Sub

    Private Sub carregarDadosProjectoLM(projecto As String)
        Dim m As MotorLM
        Dim doc As Objeto
        m = MotorLM.GetInstance


        doc = New Objeto
        doc.BaseDados = m.Empresa
        doc.Projeto = projecto
        gridArtigos.Tag = doc

        carregarFornecedoresLinha("")

        hashLST = New Hashtable()
        hashLSTPecas = New Hashtable()


        Dim idCabecDoc As String

        If modulo = "C" Then
            idCabecDoc = m.existeDocumentoCompra(buscarTipoDocumento(0), projecto)
        Else
            idCabecDoc = m.existeDocumentoVenda(buscarTipoDocumento(0), projecto)
        End If

        'Dim doc As Objeto
        'doc = New Objeto
        'doc.Id = idCabecDoc
        'doc.BaseDados = m.Empresa

        'gridArtigos.Tag = doc

        If idCabecDoc <> "" Then

            actualizarFormularioLM(idCabecDoc, "", projecto)
        Else
            actualizar(False)
        End If

    End Sub

    Private Sub carregarDadosProjectoCOT(projecto As String)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim listaDocs As StdBELista

        gridDocsCOT.Records.DeleteAll()

        hashCOT = New Hashtable()

        listaDocs = m.existeDocumentosCompra(buscarTipoDocumento(1), projecto, cbTodosCOT.Checked)

        While Not listaDocs.NoFim
            inserirLinhaDocsRegisto(listaDocs.Valor("BD"), listaDocs.Valor("PROJETO"), listaDocs.Valor("ID"), listaDocs.Valor("Entidade"), listaDocs.Valor("Nome"), listaDocs.Valor("DataDoc"), listaDocs.Valor("Serie"), listaDocs.Valor("Numdoc"), "", gridDocsCOT)
            listaDocs.Seguinte()
        End While


        listaDocs = m.existeDocumentosCompraGrupo(buscarTipoDocumento(1), projecto, cbTodosCOT.Checked)

        If Not listaDocs Is Nothing Then
            While Not listaDocs.NoFim
                inserirLinhaDocsRegisto(listaDocs.Valor("BD"), listaDocs.Valor("PROJETO"), listaDocs.Valor("ID"), listaDocs.Valor("Entidade"), listaDocs.Valor("Nome"), listaDocs.Valor("DataDoc"), listaDocs.Valor("Serie"), listaDocs.Valor("Numdoc"), "", gridDocsCOT)
                listaDocs.Seguinte()
            End While
        End If

        gridDocsCOT.Populate()
        '  gridDocsCOT.SelectedRows.DeleteAll()

        If gridDocsCOT.Rows.Count > 0 Then
            gridArtigosCOT.Tag = gridDocsCOT.Rows(0).Record.Tag
            Dim o As Objeto
            o = gridArtigosCOT.Tag
            actualizarGrelhaLinhasArtigos(True, hashCOT, gridDocsCOT.Rows(0).Record.Tag, gridArtigosCOT, lblRegisto2, mGridCOT, TotalDocCOT)
            txtObservacoesCOT.Text = m.consultaValor("SELECT Observacoes FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")
            actualizarDadosUtilizadores(gridArtigosCOT.Tag, lblUtilGravacaoCOT, lblUtilActualizacaoCOT)
        End If

    End Sub

    Private Sub carregarDadosProjectoECF(projecto As String)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim listaDocs As StdBELista

        gridDocsENC.Records.DeleteAll()
        gridDocsENC.Populate()

        hashENC = New Hashtable

        listaDocs = m.existeDocumentosCompra(buscarTipoDocumento(2), projecto, cbTodosENC.Checked)

        If listaDocs Is Nothing Then Exit Sub

        If Not listaDocs Is Nothing Then
            While Not listaDocs.NoFim
                inserirLinhaDocsRegisto(listaDocs.Valor("BD"), listaDocs.Valor("PROJETO"), listaDocs.Valor("ID"), listaDocs.Valor("Entidade"), listaDocs.Valor("Nome"), listaDocs.Valor("DataDoc"), listaDocs.Valor("Serie"), listaDocs.Valor("Numdoc"), listaDocs.Valor("DataEntrega"), gridDocsENC)
                listaDocs.Seguinte()
            End While
        End If

        listaDocs = m.existeDocumentosCompraGrupo(buscarTipoDocumento(2), projecto, cbTodosCOT.Checked)

        If Not listaDocs Is Nothing Then
            While Not listaDocs.NoFim
                inserirLinhaDocsRegisto(listaDocs.Valor("BD"), listaDocs.Valor("PROJETO"), listaDocs.Valor("ID"), listaDocs.Valor("Entidade"), listaDocs.Valor("Nome"), listaDocs.Valor("DataDoc"), listaDocs.Valor("Serie"), listaDocs.Valor("Numdoc"), listaDocs.Valor("DataEntrega"), gridDocsENC)
                listaDocs.Seguinte()
            End While
        End If

        gridDocsENC.Populate()
        ' gridDocsENC.SelectedRows.DeleteAll()
        If gridDocsENC.Rows.Count > 0 Then
            gridArtigosENC.Tag = gridDocsENC.Rows(0).Record.Tag

            Dim o As Objeto
            o = gridArtigosENC.Tag
            actualizarGrelhaLinhasArtigos(False, hashENC, gridDocsENC.Rows(0).Record.Tag, gridArtigosENC, lblRegisto3, mGridENC, TotalDocENC)
            txtObservacoesENC.Text = m.consultaValor("SELECT Observacoes FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")
            actualizarDataRecepcao(gridArtigosENC.Tag)
            actualizarDadosUtilizadores(gridArtigosENC.Tag, lblUtilGravacaoENC, lblUtilActualizacaoENC)
        End If

    End Sub


    Private Sub actualizarDataRecepcao(o As Objeto)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim datarecepcao As String
        datarecepcao = m.consultaValor("SELECT CDU_CabVar2 FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")

        dtpDataFatura.Value = Now
        dtpDataFatura.Checked = False
        If datarecepcao <> "" Then
            dtpDataFatura.Value = datarecepcao
            dtpDataFatura.Checked = True
        End If
    End Sub




    Private Sub txtPesqFornecedor_Change(sender As Object, e As EventArgs) Handles txtFornecedor.Change
        inicializarFornecedores()
        VerificarChecked()
    End Sub
    Private Sub VerificarChecked()
        Dim row As DataRowView
        Dim index As Integer
        Dim codigo1 As String
        Dim codigo2 As String
        Dim rowC As ArrayList
        rowC = New ArrayList


        For Each row In lstFornecedor.Items
            rowC.Add(LCase(row("Codigo")))
        Next


        ' index = index + 1
        For Each codigo1 In rowC
            For Each codigo2 In listaFornecedores
                If codigo1 = LCase(codigo2) Then
                    lstFornecedor.SetItemCheckState(index, CheckState.Checked)
                End If
            Next
            index = index + 1
        Next


    End Sub

    Private Sub txtProjecto_Leave(sender As Object, e As EventArgs)
        'Dim nome As String
        'Dim m As MotorLM
        'm = MotorLM.GetInstance
        'nome = ""
        'nome = m.consultaValor("SELECT Descricao from COP_Obras where codigo like '" + txtProjecto.Text + "'")
        'txtNomeProjecto.Text = nome
        'carregarDadosProjectoLM(txtProjecto.Text)
    End Sub


    Private Sub lstCarregarListaMaterial_Click(sender As Object, e As EventArgs) Handles lstCarregarListaMaterial.Click

        Dim m As MotorLM
        m = MotorLM.GetInstance
        If Not m.existeProjecto(txtProjecto.Text) Then
            MsgBox("Tem de Indicar um projecto", MsgBoxStyle.Critical)
            Exit Sub
        End If

        OpenFileDialog1.FilterIndex = m.getIndexExcel()
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        If System.IO.File.Exists(OpenFileDialog1.FileName) Then

            Select Case UCase(System.IO.Path.GetExtension(OpenFileDialog1.FileName))
                Case ".XLSX" : carregarGrelhaArtigos(XlsxToDataSet(OpenFileDialog1.FileName), OpenFileDialog1.FilterIndex)
                Case ".XLS" : carregarGrelhaArtigos(XlsToDataSet(OpenFileDialog1.FileName), OpenFileDialog1.FilterIndex)
                Case ".XLSM" : carregarGrelhaArtigos(XlsToDataSet(OpenFileDialog1.FileName), OpenFileDialog1.FilterIndex)
                Case ".CSV" : carregarGrelhaArtigos(CSVToDataSet(OpenFileDialog1.FileName), OpenFileDialog1.FilterIndex)
                Case ".ODS" : carregarGrelhaArtigos(ODSToDataSet(OpenFileDialog1.FileName, m.getExcelFolha, m.InformModLstMaterial.EXCEL_INICIO), OpenFileDialog1.FilterIndex)
            End Select
        End If

        MsgBox("Importação Finalizada", vbInformation)
        Me.Cursor = Cursors.Default
    End Sub


    Private Function XlsToDataSet(ficheiroExcel As String) As DataTable

        Try


            Dim conn1 As ADODB.Connection
            conn1 = New ADODB.Connection()

            Dim conn As OleDb.OleDbConnection
            Dim adapter As OleDb.OleDbDataAdapter
            Dim comando As OleDb.OleDbCommand
            Dim myTable As DataTable = New DataTable()
            comando = New OleDb.OleDbCommand
            conn = New OleDb.OleDbConnection
            Dim m As MotorLM
            m = MotorLM.GetInstance
            Dim folha As String

            'Dim connString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ficheiroExcel & ";Extended Properties=""Excel 8.0; HDR=YES; IMEX=1;"""
            Dim connString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ficheiroExcel & ";Extended Properties=""Excel 8.0; HDR=YES; IMEX=1;"""

            folha = GetExcelSheetNames(ficheiroExcel, connString)

            conn.ConnectionString = connString
            conn.Open()


            ' folha = m.getExcelFolha

            comando.CommandText = "SELECT * FROM [" & folha & "]"
            comando.Connection = conn
            adapter = New OleDb.OleDbDataAdapter(comando)
            adapter.Fill(myTable)
            desligarConexao(conn)

            Return myTable

        Catch ex As Exception
            MsgBox("Impossivel carregar  a lista" + vbCrLf + vbCrLf + ex.Message, vbCritical)
            Return Nothing
        End Try
    End Function



    Private Function CSVToDataSet(ficheiroExcel As String, Optional indexLinhaColuna As Integer = 2, Optional separador As String = ",") As DataTable


        Dim m As MotorLM
        m = MotorLM.GetInstance()
        Dim dt As New System.Data.DataTable
        Dim firstLine As Boolean = True
        Dim indexLinha As Integer
        indexLinha = 0
        If IO.File.Exists(ficheiroExcel) Then
            Using sr As New IO.StreamReader(ficheiroExcel, System.Text.Encoding.Default)
                While Not sr.EndOfStream
                    indexLinha = indexLinha + 1
                    If firstLine And indexLinha = indexLinhaColuna Then
                        firstLine = False
                        Dim cols = sr.ReadLine.Split(separador)
                        For Each col In cols
                            dt.Columns.Add(New DataColumn(col, GetType(String)))
                        Next

                        cols(0) = m.InformModLstMaterial.EXCEL_INICIO
                        dt.Rows.Add(cols.ToArray)
                    Else
                        Dim data() As String
                        Dim strData As String
                        strData = sr.ReadLine

                        data = ParseLine(strData)
                        data = formatarLinha(data)
                        If Not firstLine Then
                            dt.Rows.Add(data.ToArray)
                        End If

                    End If
                End While
            End Using
        End If
        Return dt


    End Function




    Private Shared namespaces As String(,) = New String(,) {{"table", "urn:oasis:names:tc:opendocument:xmlns:table:1.0"}, {"office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0"}, {"style", "urn:oasis:names:tc:opendocument:xmlns:style:1.0"}, {"text", "urn:oasis:names:tc:opendocument:xmlns:text:1.0"}, {"draw", "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"}, {"fo", "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"},
    {"dc", "http://purl.org/dc/elements/1.1/"}, {"meta", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0"}, {"number", "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"}, {"presentation", "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"}, {"svg", "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"}, {"chart", "urn:oasis:names:tc:opendocument:xmlns:chart:1.0"},
    {"dr3d", "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"}, {"math", "http://www.w3.org/1998/Math/MathML"}, {"form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0"}, {"script", "urn:oasis:names:tc:opendocument:xmlns:script:1.0"}, {"ooo", "http://openoffice.org/2004/office"}, {"ooow", "http://openoffice.org/2004/writer"},
    {"oooc", "http://openoffice.org/2004/calc"}, {"dom", "http://www.w3.org/2001/xml-events"}, {"xforms", "http://www.w3.org/2002/xforms"}, {"xsd", "http://www.w3.org/2001/XMLSchema"}, {"xsi", "http://www.w3.org/2001/XMLSchema-instance"}, {"rpt", "http://openoffice.org/2005/report"},
    {"of", "urn:oasis:names:tc:opendocument:xmlns:of:1.2"}, {"rdfa", "http://docs.oasis-open.org/opendocument/meta/rdfa#"}, {"config", "urn:oasis:names:tc:opendocument:xmlns:config:1.0"}}

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================


    Private Function GetZipFile(inputFilePath As String) As ZipFile
        Return ZipFile.Read(inputFilePath)
    End Function


    Private Function GetContentXmlFile(zipFile As ZipFile) As XmlDocument

        ' Get file(in zip archive) that contains data ("content.xml").
        Dim contentZipEntry As ZipEntry = zipFile("content.xml")

        ' Extract that file to MemoryStream.
        Dim contentStream As Stream = New MemoryStream()
        contentZipEntry.Extract(contentStream)
        contentStream.Seek(0, SeekOrigin.Begin)

        'Create XmlDocument from MemoryStream (MemoryStream contains content.xml).
        Dim contentXml As XmlDocument = New XmlDocument()
        contentXml.Load(contentStream)

        Return contentXml
    End Function


    Private Function InitializeXmlNamespaceManager(xmlDocument As XmlDocument) As XmlNamespaceManager
        Dim nmsManager As New XmlNamespaceManager(xmlDocument.NameTable)

        For i As Integer = 0 To namespaces.GetLength(0) - 1
            nmsManager.AddNamespace(namespaces(i, 0), namespaces(i, 1))
        Next

        Return nmsManager
    End Function


    Private Function GetSheet(tableNode As XmlNode, nmsManager As XmlNamespaceManager) As DataTable
        Dim sheet As New DataTable(tableNode.Attributes("table:name").Value)

        Dim rowNodes As XmlNodeList = tableNode.SelectNodes("table:table-row", nmsManager)

        Dim rowIndex As Integer = 0
        For Each rowNode As XmlNode In rowNodes
            Me.GetRow(rowNode, sheet, nmsManager, rowIndex)
        Next

        Return sheet
    End Function


    Private Function ReadCellValue(cell As XmlNode) As String
        Dim cellVal As XmlAttribute = cell.Attributes("office:value")

        If cellVal Is Nothing Then
            Return If([String].IsNullOrEmpty(cell.InnerText), Nothing, cell.InnerText)
        Else
            Return cellVal.Value
        End If
    End Function

    Private Sub GetCell(cellNode As XmlNode, row As DataRow, nmsManager As XmlNamespaceManager, ByRef cellIndex As Integer)
        Dim cellRepeated As XmlAttribute = cellNode.Attributes("table:number-columns-repeated")

        If cellRepeated Is Nothing Then
            Dim sheet As DataTable = row.Table

            While sheet.Columns.Count <= cellIndex
                sheet.Columns.Add()
            End While

            row(cellIndex) = ReadCellValue(cellNode)

            cellIndex += 1
        Else
            cellIndex += Convert.ToInt32(cellRepeated.Value, System.Globalization.CultureInfo.InvariantCulture)
        End If
    End Sub


    Private Sub GetRow(rowNode As XmlNode, sheet As DataTable, nmsManager As XmlNamespaceManager, ByRef rowIndex As Integer)
        Dim rowsRepeated As XmlAttribute = rowNode.Attributes("table:number-rows-repeated")
        If rowsRepeated Is Nothing OrElse Convert.ToInt32(rowsRepeated.Value, System.Globalization.CultureInfo.InvariantCulture) = 1 Then
            While sheet.Rows.Count < rowIndex
                sheet.Rows.Add(sheet.NewRow())
            End While

            Dim row As DataRow = sheet.NewRow()

            Dim cellNodes As XmlNodeList = rowNode.SelectNodes("table:table-cell", nmsManager)

            Dim cellIndex As Integer = 0
            For Each cellNode As XmlNode In cellNodes
                GetCell(cellNode, row, nmsManager, cellIndex)
            Next

            'Somente as linhas com peças
            If sheet.Columns.Count >= 1 Then
                If Not IsDBNull(row(1)) Then
                    sheet.Rows.Add(row)
                    rowIndex += 1
                End If
            End If

            'sheet.Rows.Add(row)

            ' Else
            ' rowIndex += Convert.ToInt32(rowsRepeated.Value, System.Globalization.CultureInfo.InvariantCulture)
        End If

        ' sheet must have at least one cell
        If sheet.Rows.Count = 0 Then
            sheet.Rows.Add(sheet.NewRow())
            sheet.Columns.Add()
        End If
    End Sub

    Private Function GetTableNodes(contentXmlDocument As XmlDocument, nmsManager As XmlNamespaceManager) As XmlNodeList
        Return contentXmlDocument.SelectNodes("/office:document-content/office:body/office:spreadsheet/table:table", nmsManager)
    End Function

    Private Function ODSToDataSet(inputFilePath As String, celula As String, valorInicio As String) As DataTable


        celula = celula.Replace("$", "")

        Dim odsZipFile As ZipFile = Me.GetZipFile(inputFilePath)

        ' Get content.xml file
        Dim contentXml As XmlDocument = GetContentXmlFile(odsZipFile)

        ' Initialize XmlNamespaceManager
        Dim nmsManager As XmlNamespaceManager = InitializeXmlNamespaceManager(contentXml)

        Dim odsFile As DataSet = New DataSet(Path.GetFileName(inputFilePath))

        Dim tableNode As XmlNode

        For Each tableNode In GetTableNodes(contentXml, nmsManager)
            If UCase(tableNode.Attributes("table:name").Value) = UCase(celula) Then
                odsFile.Tables.Add(GetSheet(tableNode, nmsManager))
            End If
        Next


        Dim dt As DataTable
        dt = odsFile.Tables(celula)

        Dim row As DataRow
        row = dt.NewRow()
        row(0) = valorInicio
        dt.Rows.InsertAt(row, 0)
        Return dt

    End Function




    Private Function formatarLinha(data() As String) As String()
        Dim i As Integer

        For i = 0 To data.Length - 1
            data(i) = Replace(data(i), """", "")
        Next

        Return data
    End Function
    Private Function ParseLine(ByVal oneLine As String) As String()
        ' Returns an array containing the values of the comma-separated fields.
        ' This pattern actually recognizes the correct commas.
        ' The Regex.Split() command later gets text between the commas.
        Dim pattern As String = ",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))"
        Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(pattern)
        Return r.Split(oneLine)
    End Function


    Private Sub carregarFicheiroODS(ficheiroExcel As String)
        MsgBox("Não implementado ainda", MsgBoxStyle.Critical, "Anphis")
    End Sub

    Private Function GetExcelSheetNames(ByVal fileName As String, ConnectionString As String) As String

        Dim m As MotorLM
        m = MotorLM.GetInstance

        Dim conn As New OleDb.OleDbConnection(ConnectionString)

        conn.Open()

        Dim dtSheets As DataTable = conn.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, Nothing)

        conn.Close()

        If dtSheets.Rows.Count > 0 Then
            For Each row In dtSheets.Rows
                If UCase(row("TABLE_NAME").ToString()) = UCase(m.getExcelFolha) Then
                    Return m.getExcelFolha
                End If
            Next
            Return dtSheets.Rows(0)("TABLE_NAME").ToString()
        End If
        Return ""


    End Function

    Public Shared Function XlsxToDataSet(sFile As String) As DataTable
        Try
            Dim ds As New DataSet()
            Dim m As MotorLM
            m = MotorLM.GetInstance

            Dim MyCommand As OleDb.OleDbDataAdapter
            Dim MyConnection As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFile & ";Extended Properties=""Excel 12.0; HDR=YES; IMEX=1;""")

            MyCommand = New System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + m.getExcelFolha + "]", MyConnection)
            MyCommand.TableMappings.Add("Table", "Table")
            MyCommand.Fill(ds)
            MyConnection.Close()

            Return ds.Tables(0)
        Catch ex As Exception
            MsgBox("Impossivel carregar  a lista" + vbCrLf + vbCrLf + ex.Message, vbCritical)
            Return Nothing
        End Try

    End Function


    Public Sub desligarConexao(ByVal conn As OleDb.OleDbConnection)
        Try
            If conn Is Nothing Then Exit Sub
            conn.Close()
            conn = Nothing
        Catch ex As Exception
        End Try
    End Sub

    Private Sub carregarGrelhaArtigos(datatable As DataTable, index_versao As Integer)

        Dim row As DataRow
        Dim comecar As Boolean
        Dim m As MotorLM


        If datatable Is Nothing Then Exit Sub

        hashLSTPecas = New Hashtable()

        Dim doc As Objeto
        doc = gridArtigos.Tag

        If doc.Id = "" Then
            gridArtigos.Records.DeleteAll()
            gridArtigos.Populate()
            hashLST = New Hashtable()
        End If

        m = MotorLM.GetInstance
        comecar = False

        datatable = actualizarCabecalhosDocumentos(datatable, 0, m.InformModLstMaterial.indexCampoInicial)

        Dim lista As StdBELista
        Dim dtBase As DataTable

        lista = m.consulta("SELECT newid() as ID,' ' as Peca, '' as Descricao, '' as Dimensoes, '' as Material ,1 as Quantidade,'" + m.InformModLstMaterial.ARTIGO + "' as Artigo,'' as Estado,'' as Fornecedor,0 as NumAlt,* FROM LINHASCOMPRAS WHERE 1=0")
        dtBase = m.convertRecordSetToDataTable(lista.DataSet)

        Dim indexPeca As Integer
        Dim indexDescricao As Integer
        Dim indexDimensoes As Integer
        Dim indexMaterial As Integer
        Dim indexTemperamento As Integer
        Dim indexcampo7 As Integer
        Dim indexcampo8 As Integer
        Dim indexcampo9 As Integer

        indexDescricao = mGridLST.buscarIndexCampo("DESCRICAO")

        indexPeca = mGridLST.buscarIndexCampo("CDU_OBRAN1")
        indexDimensoes = mGridLST.buscarIndexCampo("CDU_OBRAN3")
        indexMaterial = mGridLST.buscarIndexCampo("CDU_OBRAN4")
        indexTemperamento = mGridLST.buscarIndexCampo("CDU_OBRAN6")
        indexcampo7 = mGridLST.buscarIndexCampo("CDU_OBRAN7")
        indexcampo8 = mGridLST.buscarIndexCampo("CDU_OBRAN8")
        ' indexcampo9 = mGridLST.buscarIndexCampo("CDU_OBRAN9")
        ' indexcampo9 = mGridLST.buscarIndexCampo("CDU_OBRAN10")


        Dim index As Integer
        index = 0
        ProgressBar1.Visible = True
        ProgressBar1.Value = 0
        ProgressBar1.Maximum = datatable.Rows.Count

        'indicação a importação é a 1ª ou não. Para colocar as linhas novas em branco ou em azul 
        Dim nova As Boolean
        nova = True
        'nova = gridArtigos.Records.Count = 0

        '  If numeroLinhasDocumento > 0 Then comecar = True
        For Each row In datatable.Rows


            If comecar Then
                ' MsgBox(index)
                row("ID") = ""
                processarLinhaExcel(dtBase, row, indexPeca, indexDescricao, indexDimensoes, indexMaterial, nova)
            Else
                comecar = podeComecar(comecar, row(buscarCampoExcel(index_versao)).ToString, index_versao, m.InformModLstMaterial.EXCEL_INICIO)
            End If
            index = index + 1
            ProgressBar1.Value = index

        Next

        'caso nao tenha encontrado o inicio, vai verificar mais á frente os valores
        If comecar = False Then
            Dim indexComecar As Integer
            indexComecar = 1
            datatable = actualizarCabecalhosDocumentos(datatable, indexComecar)
            index = 0
            For Each row In datatable.Rows
                If comecar Then
                    row("ID") = ""
                    processarLinhaExcel(dtBase, row, indexPeca, indexDescricao, indexDimensoes, indexMaterial, nova)
                Else
                    comecar = podeComecar(comecar, row(indexComecar).ToString, index_versao, m.InformModLstMaterial.EXCEL_INICIO)
                End If
                index = index + 1
            Next
        End If
        If comecar Then
            carregouLista = True
            lblDataUltCarregamentoLista.Text = "Data Lista : " + Now.ToShortDateString
        End If


        ProgressBar1.Visible = False

        If m.InformModLstMaterial.validaLinhasRemovidas Then

            actualizarLinhasRemovidas()
        End If


        gridArtigos.Populate()

        recalcularIds(hashLST, gridArtigos)

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"

    End Sub

    Private Sub actualizarLinhasRemovidas()


        'Next
        Dim dtRow As DataRow
        Dim coluna As Integer
        Dim cor As String
        Dim record As ReportRecord
        Dim peca As String


        coluna = mGridLST.buscarIndexCampo("CDU_OBRAN1")
        For Each record In gridArtigos.Records
            If coluna <> -1 Then
                peca = UCase(record(coluna).Value)

                If Not hashLSTPecas.ContainsKey(peca) Then
                    dtRow = record.Tag
                    dtRow("Estado") = "R"
                    dtRow("CDU_ObraN5") = "R"
                    cor = buscarCor("R")
                    For i = 0 To gridArtigos.Columns.Count - 1
                        gridArtigos.Records(record.Index).Item(i).BackColor = cor
                    Next
                End If
            End If

        Next
    End Sub
    Private Sub recalcularIds(ByRef ht As Hashtable, grid As AxXtremeReportControl.AxReportControl)
        Dim r As ReportRecord
        Dim id As String
        ht = New Hashtable()
        Dim row As DataRow


        For Each r In grid.Records
            row = r.Tag
            '   MsgBox(row("CDU_Obran1").ToString)
            id = row("ID").ToString
            id = mGridLST.formatarIdsLinha(id)
            ht.Add(id, r.Index)
        Next

    End Sub

    Private Function buscarCampoExcel(index As Integer) As Object

        If index = 2 Or index = 3 Then
            Return "CDU_ObraN1"
        Else
            Return 0
        End If

    End Function



    Private Function podeComecar(comecar As Boolean, campo As String, index As Integer, inicioExcel As String) As Boolean
        If comecar = True Then
            Return comecar
        End If

        Dim campoComecar As String
        Select Case index
            Case 2 : campoComecar = "part nr."
                '   Case 3 : campoComecar = "*"
            Case Else : campoComecar = inicioExcel.ToLower
        End Select


        Return campo.ToLower = campoComecar.ToLower 'IIf(versaoV1, "part nr.", inicioExcel.ToLower)

    End Function


    Private Function actualizarCabecalhosDocumentos(datatable As DataTable, indexComecar As Integer, Optional indexCampoInicial As Integer = 1) As DataTable

        If indexCampoInicial = 0 Then
            If Not datatable.Columns.Contains("0") Then
                datatable.Columns.Add("0", GetType(String)).SetOrdinal(0)
            End If
        End If



        datatable.Columns(0).ColumnName = "0"
        datatable.Columns(1).ColumnName = "1"
        datatable.Columns(2).ColumnName = "2"
        datatable.Columns(3).ColumnName = "3"
        datatable.Columns(4).ColumnName = "4"
        datatable.Columns(5).ColumnName = "5"
        If datatable.Columns.Count > 6 Then datatable.Columns(6).ColumnName = "6"
        If datatable.Columns.Count > 7 Then datatable.Columns(7).ColumnName = "7"
        'A Coluna 8 Esta Reservada ao Fornecedor
        If datatable.Columns.Count > 8 Then datatable.Columns(8).ColumnName = "9"
        If datatable.Columns.Count > 9 Then datatable.Columns(9).ColumnName = "10"

        datatable.Columns(0 + indexComecar).ColumnName = "ID"
        datatable.Columns(1 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(1) ' "CDU_ObraN1"
        datatable.Columns(2 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(2) ' "Descricao"
        datatable.Columns(3 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(3) ' "Quantidade"
        datatable.Columns(4 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(4) ' "CDU_ObraN3"
        datatable.Columns(5 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(5) ' "CDU_ObraN4"

        If datatable.Columns.Count > 6 + indexComecar Then datatable.Columns(6 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(6) '  "CDU_ObraN6"
        If datatable.Columns.Count > 7 + indexComecar Then datatable.Columns(7 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(7) ' "CDU_ObraN7"
        If datatable.Columns.Count > 8 + indexComecar Then datatable.Columns(8 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(8) ' "CDU_ObraN9"
        If datatable.Columns.Count > 9 + indexComecar Then datatable.Columns(9 + indexComecar).ColumnName = mGridLST.buscarCampoIndex(9) ' "CDU_ObraN10"

        Return datatable

        'datatable.Columns(0).ColumnName = "0"
        'datatable.Columns(1).ColumnName = "1"
        'datatable.Columns(2).ColumnName = "2"
        'datatable.Columns(3).ColumnName = "3"
        'datatable.Columns(4).ColumnName = "4"
        'datatable.Columns(5).ColumnName = "5"
        'If datatable.Columns.Count > 6 Then datatable.Columns(6).ColumnName = "6"
        'If datatable.Columns.Count > 7 Then datatable.Columns(7).ColumnName = "7"
        ''A Coluna 8 Esta Reservada ao Fornecedor
        'If datatable.Columns.Count > 8 Then datatable.Columns(8).ColumnName = "9"
        'If datatable.Columns.Count > 9 Then datatable.Columns(9).ColumnName = "10"


        'datatable.Columns(0 + indexComecar).ColumnName = "ID"
        'datatable.Columns(1 + indexComecar).ColumnName = "CDU_ObraN1"
        'datatable.Columns(2 + indexComecar).ColumnName = "Descricao"
        'datatable.Columns(3 + indexComecar).ColumnName = "Quantidade"
        'datatable.Columns(4 + indexComecar).ColumnName = "CDU_ObraN3"
        'datatable.Columns(5 + indexComecar).ColumnName = "CDU_ObraN4"

        'If datatable.Columns.Count > 6 + indexComecar Then datatable.Columns(6 + indexComecar).ColumnName = "CDU_ObraN6"
        'If datatable.Columns.Count > 7 + indexComecar Then datatable.Columns(7 + indexComecar).ColumnName = "CDU_ObraN7"
        'If datatable.Columns.Count > 8 + indexComecar Then datatable.Columns(8 + indexComecar).ColumnName = "CDU_ObraN9"
        'If datatable.Columns.Count > 9 + indexComecar Then datatable.Columns(9 + indexComecar).ColumnName = "CDU_ObraN10"

        'Return datatable




    End Function

    Const ID_INSERIRLINHA = 10
    Const ID_APAGARLINHA = 11
    Const ID_ARTIGOINTERNO = 12
    Const ID_ARTIGOPROBLEMA = 13
    Const ID_ARTIGONORMAL = 14
    Const ID_ARTIGOENCOMENDADO = 15
    Const ID_ARTIGOPEDIDOCOTACAO = 16
    Const ID_ARTIGOCLIENTE = 18
    Const ID_ARTIGOVERIFICADO = 19
    Const ID_ARTIGOSUBCONTRATADO = 20
    Const ID_ARTIGOBLOQUEADO = 21

    Const ID_REABRIR = 50
    Const ID_ANULAR = 51

    Const ID_INSERIRLINHACOT = 30
    Const ID_APAGARLINHACOT = 31


    Const ID_INSERIRLINHAENC = 40
    Const ID_APAGARLINHAENC = 41

    Const ID_FECHAR = 52
    Const ID_ACTIVAR = 53




    ''' <summary>
    ''' Função que permite inserir ou actualizar uma linha proviniente do ficheiro de excel
    ''' </summary>
    ''' <param name="dtBase"></param>
    ''' <param name="row"></param>
    ''' <param name="indexPeca"></param>
    Private Sub processarLinhaExcel(dtBase As DataTable, row As DataRow, indexPeca As Integer, indexDescricao As Integer, indexDimensoes As Integer, indexMaterial As Integer, nova As Boolean)
        Dim rowLinha As DataRow
        Dim indexLinhaPeca As Integer
        Dim estado As String
        Dim m As MotorLM
        Dim peca As String
        m = MotorLM.GetInstance()

        Dim doc As Objeto
        doc = gridArtigos.Tag

        rowLinha = formatarLinhaRegisto(dtBase, row)
        peca = UCase(m.NuloToString(rowLinha("CDU_ObraN1")))
        'indexLinhaPeca = m.existePeca(hashLSTPecas, row)  ' m.existePeca(gridArtigos, row, indexPeca, indexDescricao, indexDimensoes, indexMaterial)
        indexLinhaPeca = m.existePeca(gridArtigos, row, indexPeca, indexDescricao, indexDimensoes, indexMaterial)
        If indexLinhaPeca = -1 Then
            'LINHA NOVA
            If peca <> "" Then
                rowLinha("ID") = mGridLST.formatarIdsLinha(m.consultaValor("SELECT NEWID()"))
                If Not nova Then
                    rowLinha("Estado") = "N"
                    rowLinha("CDU_ObraN5") = "N"
                End If
                inserirLinhaRegisto(False, hashLST, rowLinha, gridArtigos, mGridLST)
            End If
        Else
            Dim rowGrelha As DataRow
            rowGrelha = gridArtigos.Records(indexLinhaPeca).Tag
            estado = m.NuloToString(rowGrelha("CDU_ObraN5"))

            'If estado <> "" And estado <> "C" And estado <> "E" And estado <> "V" Then
            If estado <> "" And estado <> "M" And estado <> "N" Then
                If m.existeAlteracoes(rowGrelha, rowLinha, mGridLST, gridArtigos, indexLinhaPeca) Then

                    rowGrelha = gridArtigos.Records(indexLinhaPeca).Tag
                    estado = m.NuloToString(rowGrelha("CDU_ObraN5"))

                    'LINHA NOVA MAS JÀ EXISTENTE
                    If estado <> "" And estado <> "M" And estado <> "N" Then
                        rowLinha("CDU_ObraN5") = "N"
                        rowLinha("Estado") = "N"
                        inserirLinhaRegisto(False, hashLST, rowLinha, gridArtigos, mGridLST, indexLinhaPeca, True)
                    Else     'LINHA ACTULIZADA
                        rowLinha("CDU_ObraN5") = "M"
                        rowLinha("Estado") = "M"
                        'estado = m.NuloToString(rowGrelha("CDU_ObraN5"))
                        mGridLST.actualizarLinhaRegisto(gridArtigos, indexLinhaPeca, rowLinha, buscarCor(rowLinha("CDU_ObraN5")), doc)
                    End If
                End If
            Else



                'rowLinha("CDU_ObraN5") = "M"
                'rowLinha("Estado") = "M"
                'estado = m.NuloToString(rowGrelha("CDU_ObraN5"))
                mGridLST.actualizarLinhaRegisto(gridArtigos, indexLinhaPeca, rowLinha, buscarCor(estado), doc)
            End If




        End If



        If peca <> "" Then
            If Not hashLSTPecas.ContainsKey(peca) Then
                hashLSTPecas.Add(peca, peca)
            End If
        End If

    End Sub

    Private Sub actulizarIndexGrid()

    End Sub
    Private Sub gridArtigos_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridArtigos.MouseUpEvent

        Dim m As MotorLM
        m = MotorLM.GetInstance
        '    If Not m.FuncionarioAdm Then Exit Sub

        If e.button = 1 Then
            selecionarTodos(sender, e, gridArtigos)
            actualizarFornecedores(sender, e, gridArtigos)
            actualizarGrelhaListaAlteracoes(gridArtigos, e)
        Else
            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_INSERIRLINHA, "&Inserir Linha", -1, False)
                Control.IconId = 30
                Control.BeginGroup = True
                Control.Visible = m.FuncionarioAdm
                Control = .Add(xtpControlButton, ID_APAGARLINHA, "&Apagar Linha", -1, False)
                Control.IconId = 31
                Control.Enabled = gridArtigos.Records.Count > 0
                Control.Visible = m.FuncionarioAdm
                Control = .Add(xtpControlButton, ID_ARTIGOVERIFICADO, "&Artigo Verificado", -1, False)
                Control.IconId = IIf(m.InformModLstMaterial.corInvertida, 22, 27)
                Control.BeginGroup = True
                'Control.BeginGroup = True
                Control = .Add(xtpControlButton, ID_ARTIGOENCOMENDADO, "&Artigo Encomendado", -1, False)
                Control.IconId = IIf(m.InformModLstMaterial.corInvertida, 27, 22)
                Control.Visible = m.FuncionarioAdm
                Control = .Add(xtpControlButton, ID_ARTIGOPEDIDOCOTACAO, "&Artigo P. Cotação", -1, False)
                Control.IconId = 26
                Control.Visible = m.FuncionarioAdm
                Control = .Add(xtpControlButton, ID_ARTIGOINTERNO, "&Artigo Interno", -1, False)
                Control.IconId = 23
                Control = .Add(xtpControlButton, ID_ARTIGOPROBLEMA, "&Artigo Problema", -1, False)
                Control.IconId = 24
                Control = .Add(xtpControlButton, ID_ARTIGOCLIENTE, "&Material Cliente", -1, False)
                Control.IconId = 25
                Control = .Add(xtpControlButton, ID_ARTIGOSUBCONTRATADO, "&Subcontratado", -1, False)
                Control.IconId = 28
                Control = .Add(xtpControlButton, ID_ARTIGOBLOQUEADO, "&Bloquear Peça", -1, False)
                Control.IconId = 40
                Control = .Add(xtpControlButton, ID_ARTIGONORMAL, "&Artigo Normal", -1, False)
                Control.IconId = 20

                'Control.BeginGroup = True
            End With
            popup.ShowPopup()
        End If
    End Sub


    Private Sub selecionarTodos(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent, grid As AxXtremeReportControl.AxReportControl)
        Dim indexCheckbox As Integer
        indexCheckbox = buscarIndexCampo(grid, "CHECKBOX")
        If Not grid.HitTest(e.x, e.y).Column Is Nothing Then
            If grid.HitTest(e.x, e.y).ht = XTPReportHitTestInfoCode.xtpHitTestHeader And grid.HitTest(e.x, e.y).Column.Index = indexCheckbox Then
                Dim check As Boolean
                check = grid.Columns(indexCheckbox).Tag
                check = Not check
                grid.Columns(indexCheckbox).Tag = check
                selecionarTodos(check, grid)
            End If
        End If
    End Sub

    Private Sub selecionarTodos(seleccionar As Boolean, grid As AxXtremeReportControl.AxReportControl)
        Dim row1 As ReportRow
        Dim index As Integer
        index = buscarIndexCampo(grid, "CHECKBOX")
        If index = -1 Then Exit Sub
        For Each row1 In grid.Rows
            row1.Record(index).Checked = seleccionar
        Next
        grid.Populate()
    End Sub

    Private Sub lstFornecedor_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstFornecedor.ItemCheck
        If lstFornecedor.SelectedValue Is Nothing Then Exit Sub
        If e.CurrentValue = CheckState.Unchecked Then
            listaFornecedores.Add(lstFornecedor.SelectedValue)
        Else
            listaFornecedores.Remove(lstFornecedor.SelectedValue)
        End If
    End Sub



    Private Sub tabCtlMaterial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabCtlMaterial.SelectedIndexChanged
        Select Case tabCtlMaterial.SelectedIndex
            Case 0 : configurarLM()
            Case 1 : configurarPC()
            Case 2 : configurarENC()
        End Select
    End Sub

    Private Sub configurarLM()
        ' btn1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Cotação
        ' btn2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PCotação
        ' Group1.Text = "Lista Material"
        ' Group2.Text = "Pedido Cotação"
        Group1.Visible = True
        Group2.Visible = True
        Group3.Visible = True
        '    Group1.Location = New Point(581, 2)
        'rb3.Visible = False
        'Group2.Visible = btn2.Visible
        'btn2.Visible = btn2.Visible
        'rb4.Visible = btn2.Visible
        'rb5.Visible = btn2.Visible
        'rb6.Visible = btn2.Visible
        '  btn1.Text = Group1.Text
        ' btn2.Text = Group2.Text
        'btn1.Location = New Point(6, 25)
        'btn2.Location = New Point(6, 25)
    End Sub

    Private Sub configurarPC()
        Group1.Visible = False
        Group2.Visible = True
        Group3.Visible = True
    End Sub

    Private Sub configurarENC()
        Group1.Visible = False
        Group2.Visible = False
        Group3.Visible = True
    End Sub

    Private Sub configurarAplicacao()
        Dim m As MotorLM
        m = MotorLM.GetInstance
        txtFornecedor.Visible = m.FuncionarioAdm
        lstFornecedor.Visible = m.FuncionarioAdm
        lblFornecedor.Visible = m.FuncionarioAdm

        rb1.Checked = True
        rb2.Checked = False
        rb3.Checked = False
        rb4.Checked = True
        rb5.Checked = False
        rb6.Checked = False
        rb7.Checked = True
        rb8.Checked = False
        rb9.Checked = False

        If Not m.FuncionarioAdm Then

            tabCtlMaterial.TabPages.RemoveAt(2)
            tabCtlMaterial.TabPages.RemoveAt(1)
            gridArtigos.Width = tbcMoldes.Width
            lstCarregarListaMaterial.Left = Me.Width - 100
            Group2.Visible = False
            Group3.Visible = False

            tbcFornecedores.Visible = False
            ckbVerTodos.Visible = False
            btnRemoveRowLST.Visible = False
            btnAddRowLST.Visible = False
            lstCarregarListaMaterial.Visible = False

            lblObsLST.Text = "Obs:"

        End If
    End Sub



    Private Sub gravarDocumento(botao As Integer)

        If Not validarGravacao(0) Then
            Exit Sub
        End If

        Dim tipo As Integer = 0
        Dim msg As String = ""

        If getRbChecked(0, botao).Checked Then
            tipo = 0
            msg = "Documento gravado com sucesso"
        End If

        If getRbChecked(1, botao).Checked Then
            tipo = 1
            msg = "Documento imprimido com sucesso"
        End If

        If getRbChecked(2, botao).Checked Then
            tipo = 2
            msg = "Documento enviado com sucesso"
        End If


        If Not apresentarPergunta(tipo, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub

        'permite a gravação dos documentumentos de stocks
        If botao = 2 Then
            gravarDocumentoStocks()
        End If

        Dim ok As Boolean
        Select Case tipo
            Case 0 : ok = gravarDocumento(False, botao)
            Case 1 : ok = imprimirDoc(tabCtlMaterial.SelectedIndex, botao)
            Case 2 : ok = enviarEmailDocumento(tabCtlMaterial.SelectedIndex, botao)
        End Select

        'actualizarEstadoGrelha(gridArtigos, mGridLST)
        actualizar(True, , False)
        If ok Then MsgBox(msg, MsgBoxStyle.Information)

    End Sub


    'Private Function getRbChecked(index As Integer, botao As Integer) As RadioButton
    '    Dim rb As RadioButton

    '    Select Case index
    '        Case 0 :
    '              If botao = 1 Then
    '                rb = rb4
    '            Else
    '                rb = rb7
    '            End If
    '        Case 1 : If botao = 1 Then
    '                rb = rb5
    '            Else
    '                rb = rb8
    '            End If
    '        Case 2 : If botao = 1 Then
    '                rb = rb6
    '            Else
    '                rb = rb9
    '            End If
    '    End Select
    '    Return rb
    'End Function

    Private Function getRbChecked(index As Integer, botao As Integer) As RadioButton
        Dim rb As RadioButton
        rb = Nothing
        Select Case botao
            Case 0 : Select Case index
                    Case 0 : rb = rb1
                    Case 1 : rb = rb2
                    Case 2 : rb = rb3
                End Select
            Case 1 : Select Case index
                    Case 0 : rb = rb4
                    Case 1 : rb = rb5
                    Case 2 : rb = rb6
                End Select
            Case 2 : Select Case index
                    Case 0 : rb = rb7
                    Case 1 : rb = rb8
                    Case 2 : rb = rb9
                End Select
        End Select
        Return rb
    End Function

    Private Sub transformarDocumento(botao As Integer)
        Dim m As MotorLM
        Dim lista As ArrayList
        Dim codigo As String
        m = MotorLM.GetInstance

        If Not validarGravacao(1) Then
            Exit Sub
        End If

        lista = buscarListaFornecedores(tabCtlMaterial.SelectedIndex)

        Dim tipo As Integer
        Dim msg As String



        If getRbChecked(0, botao).Checked Then
            tipo = 0
            msg = "Documento gravado com sucesso"
        End If

        If getRbChecked(1, botao).Checked Then
            tipo = 1
            msg = "Documento imprimido com sucesso"
        End If

        If getRbChecked(2, botao).Checked Then
            tipo = 2
            msg = "Documento enviado com sucesso"
        End If


        If Not apresentarPergunta(0, tabCtlMaterial.SelectedIndex, botao) Then Exit Sub

        gravarDocumento(False, tabCtlMaterial.SelectedIndex, , False)

        Dim ok As Boolean
        For Each codigo In lista
            Select Case tipo
                Case 0 : ok = gravarDocumento(False, botao, codigo)
                Case 1 : ok = imprimirDoc(tabCtlMaterial.SelectedIndex, botao, codigo)
                Case 2 : ok = enviarEmailDocumento(tabCtlMaterial.SelectedIndex, botao, codigo)
            End Select

        Next

        actualizarEstadoGrelha(hashLST, gridArtigos, mGridLST)
        carregarDadosProjectoCOT(txtProjecto.Text)
        carregarDadosProjectoECF(txtProjecto.Text)

        actualizar(True, , False)

        If ok Then MsgBox(msg, MsgBoxStyle.Information)



    End Sub

    Private Function itemEditavel(gridName As String, item As ReportRecordItem, estado As String) As Boolean


        Return True

        If gridName <> "gridArtigos" Then
            Return True
        End If


        Select Case estado
            Case ESTADO_NORMAL : Return True
            Case ESTADO_INTERNO : Return False
            Case ESTADO_PROBLEMAS : Return False
            Case ESTADO_COT : Return True
            Case ESTADO_ECF : Return False
            Case ESTADO_VER : Return False
            Case ESTADO_CONF : Return False
            Case ESTADO_A : Return False
            Case ESTADO_SUB : Return True
            Case ESTADO_BLOQ : Return False
            Case ESTADO_CLIENTE : Return False
        End Select

        Return item.Editable

    End Function

    Private Sub aplicarEstadoLinha(grid As AxXtremeReportControl.AxReportControl, linha As ReportRecord, estado As String)
        Dim i As Integer
        For i = 0 To grid.Columns.Count - 1
            linha(i).Editable = itemEditavel(grid.Name, linha(i), estado)
        Next
    End Sub
    Private Sub aplicarCorLinha(grid As AxXtremeReportControl.AxReportControl, linha As ReportRecord, cor As UInteger)
        Dim i As Integer
        For i = 0 To grid.Columns.Count - 1
            linha(i).BackColor = cor

        Next
    End Sub
    'Private Sub aplicarCorLinha(grid As AxXtremeReportControl.AxReportControl, cor As UInteger)
    '    For i = 0 To grid.Rows.Count - 1
    '        If grid.Rows(i).Record(buscarIndexCampo(grid, "CHECKBOX")).Checked Then
    '            aplicarCorLinha(grid, grid.Rows(i).Record, cor)
    '        End If
    '    Next
    'End Sub
    Private Sub actualizarGrelhaLinhasArtigos(validaChecked As Boolean, ht As Hashtable, o As Objeto, grid As AxXtremeReportControl.AxReportControl, label As Label, mGrid As MotorGrelhas, lblTotal As Label)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim dtRegistos As DataTable
        dtRegistos = Nothing

        If modulo = "C" Then dtRegistos = m.daListaArtigosDocumento(o, "C")
        If modulo = "V" Then dtRegistos = m.daListaArtigosDocumento(o, "V")

        ' dt = m.daDataDocumento(id, dtpData.Value, "C")

        Dim dtrow As DataRow
        grid.Records.DeleteAll()



        emModoEdicao = True
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(validaChecked, ht, dtrow, grid, mGrid)
            'inserirLinhaRegisto(dtrow, grid)
        Next



        grid.Populate()
        grid.SelectedRows.DeleteAll()
        actualizarTotaisDocumento(o, grid, mGrid, lblTotal)

        label.Text = CStr(grid.Rows.Count) + " Registos"
        emModoEdicao = False

        actualizarGrelhaStocks()
    End Sub


    Private Sub actualizarGrelhaStocks()

        gridArtigosENT.Records.DeleteAll()
        If gridArtigosENC.SelectedRows.Count > 0 Then
            Dim dt As DataRow
            Dim doc As Objeto
            doc = gridArtigosENC.Tag
            dt = gridArtigosENC.SelectedRows(0).Record.Tag
            'actualizarGrelhaStocks(dt("ID").ToString)
            actualizarGrelhaStocks(doc, dt("ID").ToString)
        End If
        gridArtigosENT.Populate()

    End Sub

    Private Sub actualizarGrelhaStocks(o As Objeto, idLinha As String)
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim m As MotorLM
        Dim dtrow As DataRow


        Dim record As ReportRecord
        Dim item As ReportRecordItem

        m = MotorLM.GetInstance
        lista = m.consulta("SELECT *,CDU_ObraN5 as Estado FROM " + o.BaseDados + "..LinhasCompras WHERE IdLinhaOrigemCopia='" + idLinha + "' ORDER BY NUmlinha", False)

        If lista Is Nothing Then
            Exit Sub
        Else
            dt = m.convertRecordSetToDataTable(lista.DataSet)
        End If

        For Each dtrow In dt.Rows
            inserirLinhaRegisto(False, New Hashtable, dtrow, gridArtigosENT, mGridENT)
            gridArtigosENT.Tag = dtrow("IdCabecCompras").ToString()
        Next

    End Sub

    Private Sub actualizarTotaisDocumento(doc As Objeto, grid As AxXtremeReportControl.AxReportControl, mGrid As MotorGrelhas, lblTotal As Label)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim index As Integer
        Dim indexQT As Integer
        Dim record As ReportRecord
        Dim total As Double = 0

        index = mGrid.buscarIndexCampo("PRECUNIT")
        indexQT = mGrid.buscarIndexCampo("QUANTIDADE")

        If index <> -1 Then
            For Each record In grid.Records
                If IsNumeric(record(index).Value) Then
                    total = total + (record(index).Value * record(indexQT).Value)
                End If
            Next

            lblTotal.Text = "Total: " + FormatNumber(total, 2)
        Else
            lblTotal.Text = "Total: " + m.daTotalDocumento(doc, modulo)
        End If


    End Sub



    Private Sub gridDocsCOT_SelectionChanged(sender As Object, e As EventArgs) Handles gridDocsCOT.SelectionChanged
        hashCOT = New Hashtable()
        gridArtigosCOT.Tag = gridDocsCOT.SelectedRows(0).Record.Tag
        Dim o As Objeto
        o = gridArtigosCOT.Tag
        actualizarGrelhaLinhasArtigos(False, hashCOT, o, gridArtigosCOT, lblRegisto2, mGridCOT, TotalDocCOT)
        Dim m As MotorLM
        m = MotorLM.GetInstance
        txtObservacoesCOT.Text = m.consultaValor("SELECT Observacoes FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")
        actualizarDadosUtilizadores(o, lblUtilGravacaoCOT, lblUtilActualizacaoCOT)
    End Sub

    Private Sub gridArtigosCOT_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridArtigosCOT.MouseUpEvent
        Dim m As MotorLM
        m = MotorLM.GetInstance
        If Not m.FuncionarioAdm Then Exit Sub

        If e.button = 1 Then
            selecionarTodos(sender, e, gridArtigosCOT)
        Else

            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_INSERIRLINHACOT, "&Inserir Linha", -1, False)
                Control.IconId = 30
                Control.BeginGroup = True
                Control = .Add(xtpControlButton, ID_APAGARLINHACOT, "&Apagar Linha", -1, False)
                Control.IconId = 31
                Control.Enabled = gridArtigosCOT.Records.Count > 0
            End With
            popup.ShowPopup()
        End If
    End Sub

    Private Sub gridDocsENC_SelectionChanged(sender As Object, e As EventArgs) Handles gridDocsENC.SelectionChanged
        gridDocsENC_SelectionChanged()
    End Sub

    Private Function gridDocsENC_SelectionChanged()
        hashENC = New Hashtable()
        gridArtigosENC.Tag = gridDocsENC.SelectedRows(0).Record.Tag
        Dim o As Objeto
        o = gridArtigosENC.Tag

        actualizarGrelhaLinhasArtigos(True, hashENC, o, gridArtigosENC, lblRegisto3, mGridENC, TotalDocENC)
        Dim m As MotorLM
        m = MotorLM.GetInstance

        txtObservacoesENC.Text = m.consultaValor("SELECT Observacoes FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'")
        actualizarDataRecepcao(gridArtigosENC.Tag)
        actualizarDadosUtilizadores(gridArtigosENC.Tag, lblUtilGravacaoENC, lblUtilActualizacaoENC)
    End Function

    Private Sub inserirNovaLinhaRegisto(validaChecked As Boolean, grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas, ByRef ht As Hashtable, index As Integer)
        Dim m As MotorLM
        Dim id As String


        m = MotorLM.GetInstance
        If Not m.existeProjecto(txtProjecto.Text) Then
            MsgBox("Tem de Indicar um projecto", MsgBoxStyle.Critical)
            Exit Sub
        End If

        Dim lista As StdBELista
        Dim dt As DataTable

        '  lista = m.consulta("select  '' as ID,' ' as Peca, '' as Descricao, '' as Dimensoes, '' as Material ,1 as Quantidade,'" + m.InformModLstMaterial.ARTIGO + "' as Artigo,'' as Estado,'' as Fornecedor,'' as CDU_DataRecepcao")
        lista = m.consulta("SELECT '' as ID,' ' as Peca, '' as Descricao, '' as Dimensoes, '' as Material ,1.00 as Quantidade,'" + m.InformModLstMaterial.ARTIGO + "' as Artigo,'' as Estado,'' as Fornecedor,0 as NumAlt,* FROM LINHASCOMPRAS WHERE 1=0")
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        dt.Rows.Add()

        id = mgrid.formatarIdsLinha(m.consultaValor("SELECT NEWID()"))

        'dt(0)("ID") = " "
        dt(0)("ID") = id
        dt(0)("Peca") = " "
        dt(0)("Quantidade") = 1 * mgrid.getMultiplicador
        dt(0)("Artigo") = m.InformModLstMaterial.ARTIGO
        inserirLinhaRegisto(validaChecked, ht, dt(0), grid, mgrid, index, True)
        grid.Populate()
        recalcularIds(ht, grid)
        If index = -1 Then grid.Navigator.MoveLastRow()
    End Sub


    Private Function formatarLinhaRegisto(dtbase As DataTable, row As DataRow) As DataRow
        Dim numeroLinhas As Integer
        numeroLinhas = 0
        Try
            Dim m As MotorLM

            m = MotorLM.GetInstance
            dtbase.Rows.Add()
            numeroLinhas = dtbase.Rows.Count - 1
            dtbase(numeroLinhas)("ID") = mGridLST.formatarIdsLinha(m.consultaValor("SELECT NEWID()"))
            dtbase(numeroLinhas)("CDU_ObraN1") = row("CDU_ObraN1")
            dtbase(numeroLinhas)("Descricao") = row("Descricao")

            If Not IsNumeric(row("Quantidade")) Then
                row("Quantidade") = row("Quantidade")
            End If
            'permite colocar na descricao a quantidade 
            If Not IsDBNull(row("Quantidade")) And Not IsNumeric((row("Quantidade"))) Then
                dtbase(numeroLinhas)("Descricao") = row("Descricao") + " (" + row("Quantidade").ToString() + ")"
            End If
            dtbase(numeroLinhas)("Quantidade") = NuloToDouble(row("Quantidade")) * mGridLST.getMultiplicador()
            dtbase(numeroLinhas)("CDU_ObraN3") = row("CDU_ObraN3")
            dtbase(numeroLinhas)("CDU_ObraN4") = row("CDU_ObraN4")
            If dtbase.Columns.IndexOf("CDU_ObraN6") <> -1 And row.Table.Columns.IndexOf("CDU_ObraN6") <> -1 Then dtbase(numeroLinhas)("CDU_ObraN6") = row("CDU_ObraN6")
            If dtbase.Columns.IndexOf("CDU_ObraN7") <> -1 And row.Table.Columns.IndexOf("CDU_ObraN7") <> -1 Then dtbase(numeroLinhas)("CDU_ObraN7") = row("CDU_ObraN7")
            If dtbase.Columns.IndexOf("CDU_ObraN8") <> -1 And row.Table.Columns.IndexOf("CDU_ObraN8") <> -1 Then dtbase(numeroLinhas)("CDU_ObraN8") = row("CDU_ObraN8")
            If dtbase.Columns.IndexOf("CDU_ObraN9") <> -1 And row.Table.Columns.IndexOf("CDU_ObraN9") <> -1 Then dtbase(numeroLinhas)("CDU_ObraN9") = row("CDU_ObraN9")
            If dtbase.Columns.IndexOf("CDU_ObraN10") <> -1 And row.Table.Columns.IndexOf("CDU_ObraN10") <> -1 Then dtbase(numeroLinhas)("CDU_ObraN10") = row("CDU_ObraN10")


            dtbase(numeroLinhas)("PrecUnit") = 0
            dtbase(numeroLinhas)("Artigo") = m.daArtigoEquivalente(m.InformModLstMaterial.ARTIGO, dtbase(numeroLinhas))
            Return dtbase(numeroLinhas)

            If numeroLinhas = 600 Then
                numeroLinhas = numeroLinhas
            End If
        Catch ex As Exception
            Return dtbase(numeroLinhas)
        End Try
    End Function

    Const MSG_APAGAR_LINHA = "Deseja realmente apagar a linha(s) seleccionada(s) ?"
    Const MSG_APAGAR_LINHA_ERRO = "Existem registos que não foram eliminados!" + vbCrLf + " Já foram feitos Pedidos Cotação/Encomendas/Recepção Encomenda"

    Private Sub apagarLinhaRegisto(ByRef ht As Hashtable, grid As AxXtremeReportControl.AxReportControl)
        If MsgBox(MSG_APAGAR_LINHA, vbQuestion + vbYesNo) = vbYes Then
            Dim m As MotorLM

            Dim index As Integer
            Dim estado As String
            Dim row As ReportRow
            Dim dtrow As DataRow
            Dim apagouLinhas As Boolean = False
            Dim msg As String = ""

            Dim erroEliminacao As Boolean = False
            Dim i As Integer

            m = MotorLM.GetInstance()

            index = buscarIndexCampo(grid, "CHECKBOX")
            If index <> -1 Then

                For i = grid.Rows.Count - 1 To 0 Step -1
                    row = grid.Rows(i)
                    estado = row.Record.Item(index).Tag
                    If row.Record(index).Checked Then
                        apagouLinhas = True
                        '   dtrow = row.Record.Tag
                        '  m.buscarUltimoTipoDocDocumentoTransformado("C", dtrow("Id").ToString(), msg)
                        If estado <> "C" Or estado <> "V" Or estado <> "E" Then
                            grid.RemoveRowEx(row)
                        Else
                            erroEliminacao = True
                        End If
                    End If
                Next

                ''' 
                If Not apagouLinhas Then
                    row = grid.FocusedRow
                    estado = row.Record.Item(index).Tag
                    If Not row Is Nothing Then
                        If estado <> "C" Or estado <> "V" Or estado <> "E" Then
                            grid.RemoveRowEx(row)
                        Else
                            erroEliminacao = True
                        End If
                    End If
                End If
            End If

            If erroEliminacao Then
                MsgBox(MSG_APAGAR_LINHA_ERRO, MsgBoxStyle.Critical)
            End If

            recalcularIds(ht, grid)
        End If


    End Sub



    Private Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
        gravarDocumento(0)
    End Sub


    Private Sub btn2_Click(sender As Object, e As EventArgs) Handles btn2.Click
        If tabCtlMaterial.SelectedIndex <> 1 Then
            transformarDocumento(1)
        Else
            gravarDocumento(1)
        End If
    End Sub


    Private Sub btn3_Click(sender As Object, e As EventArgs) Handles btn3.Click
        If tabCtlMaterial.SelectedIndex <> 2 Then
            transformarDocumento(2)
        Else
            gravarDocumento(2)
            actualizarEstadoGrelha(hashLST, gridArtigos, mGridLST)
            actualizarEstadoGrelha(hashCOT, gridArtigosCOT, mGridCOT)
            actualizarEstadoGrelha(hashENC, gridArtigosENC, mGridENC)
            buscarCorDocumentoDocs(gridDocsCOT)
            buscarCorDocumentoDocs(gridDocsENC)
            actualizarGrelhaStocks()
            gridDocsENC_SelectionChanged()
            'carregarDadosProjectoLM(txtProjecto.Text)
            'carregarDadosProjectoCOT(txtProjecto.Text)
            'carregarDadosProjectoECF(txtProjecto.Text)
        End If
    End Sub

    Private Sub gravarDocumentoStocks()
        Dim m As MotorLM
        m = MotorLM.GetInstance
        m.gravarDocumentoStocks(gridArtigosENC.Tag, gridArtigosENT.Tag, gridArtigosENT.Records, mGridENT)
    End Sub


    Private Sub actualizarEstadoLinha(estado As String)
        Dim m As MotorLM
        Dim dtRow As DataRow
        Dim msg As String = ""
        Dim ok As Boolean = False

        htLinhaModificadas = New Hashtable
        m = MotorLM.GetInstance

        Dim o As Objeto

        o = gridArtigos.Tag

        If Not gridArtigos.FocusedRow Is Nothing Then

            Dim estadoLinha As String
            dtRow = gridArtigos.FocusedRow.Record.Tag
            estadoLinha = gridArtigos.FocusedRow.Record(buscarIndexCampo(gridArtigos, "CHECKBOX")).Tag

            m.buscarUltimoTipoDocDocumentoTransformado("C", dtRow("Id").ToString, msg)


            If msg <> "" Then
                If MsgBox("Atenção a linha seleccionada já se encontra convertidas nos seguintes documentos!" + vbCrLf + vbCrLf + msg + vbCrLf + vbCrLf + "Deseja realmente modificar o estado da linha?", MsgBoxStyle.Question + vbYesNo) = MsgBoxResult.Yes Then
                    ok = True
                End If
            Else
                ok = True
            End If

            If ok Then
                gridArtigos.FocusedRow.Record(buscarIndexCampo(gridArtigos, "CHECKBOX")).Tag = estado
                dtRow("CDU_ObraN5") = estado
                m.actualizarEstadoLinha(o.BaseDados, "PRI" + m.Empresa, txtProjecto.Text, htLinhaModificadas, modulo, dtRow("Id").ToString, estado)
                aplicarCorLinha(gridArtigos, gridArtigos.FocusedRow.Record, buscarCor(estado))
                aplicarEstadoLinha(gridArtigos, gridArtigos.FocusedRow.Record, estado)
                gridArtigos.Populate()
            End If
        End If
    End Sub


    Private Sub actualizarFornecedores(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent, grid As AxXtremeReportControl.AxReportControl)
        If Not grid.HitTest(e.x, e.y).Column Is Nothing Then
            If grid.HitTest(e.x, e.y).ht = XTPReportHitTestInfoCode.xtpHitTestReportArea Then
                Dim dtRow As DataRow
                dtRow = grid.HitTest(e.x, e.y).Row.Record.Tag
                carregarFornecedoresLinha(dtRow("Id").ToString)
            End If
        End If
    End Sub

    Private Sub carregarFornecedoresLinha(idlinha As String)
        Dim m As MotorLM
        Dim lista As StdBELista
        Dim dt As DataTable
        dt = Nothing
        m = MotorLM.GetInstance
        If Trim(idlinha) <> "" Then
            lista = m.consulta("SELECT Distinct Nome from CabecCompras WHERE id in (select idcabecCompras FROM LinhasCompras WHERE IdLinhaOrigemCopia='" + idlinha + "')")
            dt = m.convertRecordSetToDataTable(lista.DataSet)
        End If
        lstFornPC.DataSource = Nothing
        lstFornPC.DataSource = dt
        lstFornPC.ValueMember = "Codigo"
        lstFornPC.DisplayMember = "Nome"
        lstFornPC.ClearSelected()
    End Sub





    Private Sub ckbVerTodos_CheckedChanged(sender As Object, e As EventArgs) Handles ckbVerTodos.CheckedChanged
        inicializarFornecedores()
        VerificarChecked()
    End Sub



    Private Sub gridArtigosENC_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridArtigosENC.MouseUpEvent

        Dim m As MotorLM
        m = MotorLM.GetInstance
        If Not m.FuncionarioAdm Then Exit Sub

        If e.button = 1 Then
            selecionarTodos(sender, e, gridArtigosENC)
        Else
            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_INSERIRLINHAENC, "&Inserir Linha", -1, False)
                Control.IconId = 30
                Control.BeginGroup = True
                Control = .Add(xtpControlButton, ID_APAGARLINHAENC, "&Apagar Linha", -1, False)
                Control.IconId = 31
                Control.Enabled = gridArtigosENC.Records.Count > 0
            End With
            popup.ShowPopup()
        End If
    End Sub



    Private Sub cbTodosENC_CheckedChanged(sender As Object, e As EventArgs) Handles cbTodosENC.CheckedChanged
        '   carregarDadosProjectoCOT(txtProjecto.Text)
        carregarDadosProjectoECF(txtProjecto.Text)
    End Sub

    Private Sub cbTodosCOT_CheckedChanged(sender As Object, e As EventArgs) Handles cbTodosCOT.CheckedChanged
        carregarDadosProjectoCOT(txtProjecto.Text)
    End Sub

    Private Sub gridDocsCOT_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridDocsCOT.MouseUpEvent

        If e.button = 2 Then
            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                Control = .Add(xtpControlButton, ID_REABRIR, "&Reabrir Documento", -1, False)
                Control.IconId = 41
                Control.BeginGroup = True
                Control.Enabled = botaoActivo(gridDocsCOT, False, "Anulado")
                Control = .Add(xtpControlButton, ID_ANULAR, "&Anular Documento", -1, False)
                Control.IconId = 40
                Control.Enabled = botaoActivo(gridDocsCOT, True, "Anulado")

                Control = .Add(xtpControlButton, ID_FECHAR, "&Fechar Documento", -1, False)
                Control.IconId = 41
                Control.BeginGroup = True
                Control.Enabled = botaoActivo(gridDocsCOT, True, "Fechado")
                Control = .Add(xtpControlButton, ID_ACTIVAR, "&Ativar Documento", -1, False)
                Control.IconId = 40
                Control.Enabled = botaoActivo(gridDocsCOT, False, "Fechado")


            End With
            popup.ShowPopup()
        End If
    End Sub

    Private Sub gridDocsENC_MouseUpEvent(sender As Object, e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles gridDocsENC.MouseUpEvent

        If e.button = 2 Then


            Dim popup As CommandBar
            Dim Control As CommandBarControl
            popup = CommandBarsFrame1.Add("POP", xtpBarPopup)
            With popup.Controls
                'Control = .Add(xtpControlButton, ID_REABRIR, "&Reabrir Documento", -1, False)
                'Control.IconId = 41
                'Control.BeginGroup = True
                'Control.Enabled = botaoActivo(gridDocsENC, False, "Anulado")
                'Control = .Add(xtpControlButton, ID_ANULAR, "&Anular Documento", -1, False)
                'Control.IconId = 40
                'Control.Enabled = botaoActivo(gridDocsENC, True, "Anulado")

                Control = .Add(xtpControlButton, ID_FECHAR, "&Fechar Documento", -1, False)
                Control.IconId = 41
                Control.BeginGroup = True
                Control.Enabled = botaoActivo(gridDocsENC, True, "Fechado")
                Control = .Add(xtpControlButton, ID_ACTIVAR, "&Ativar Documento", -1, False)
                Control.IconId = 40
                Control.Enabled = botaoActivo(gridDocsENC, False, "Fechado")

            End With
            popup.ShowPopup()
        End If
    End Sub

    Private Function botaoActivo(grid As AxXtremeReportControl.AxReportControl, aberto As Boolean, campo As String) As Boolean

        If grid.SelectedRows.Count > 0 Then
            Dim m As MotorLM
            m = MotorLM.GetInstance()
            Dim doc As Objeto
            doc = grid.SelectedRows(0).Record.Tag
            Return m.VerificarEstadoDocumento(doc, aberto, campo)
        Else
            Return False
        End If

    End Function


    Private Sub actualizarEstadoDocumento(abrir As Boolean, campo As String, texto As String)
        Dim m As MotorLM
        Dim sair As Boolean

        m = MotorLM.GetInstance()

        Dim doc As Objeto



        Dim id As String
        If tabCtlMaterial.SelectedIndex = 1 Then
            doc = gridDocsCOT.SelectedRows(0).Record.Tag
            id = doc.Id
        Else
            doc = gridDocsENC.SelectedRows(0).Record.Tag
            id = doc.Id
        End If


        If campo = "Anulado" Then
            sair = m.DocumentoJaConvertido(doc)
        End If


        If sair Then
            MsgBox("Imposivel actualizar o estado do documento, este já se encontra convertido!", vbCritical)
            Exit Sub
        End If

        'grid.SelectedRows(0).Record.Tag.ToString

        If abrir Then
            '  m.executarComando("UPDATE CabecComprasStatus SET Fechado=0, Anulado=0 WHERE idCabecCompras='" + id + "'")
            m.executarComando("UPDATE " + doc.BaseDados + "..CabecComprasStatus SET " + campo + "=0 WHERE idCabecCompras='" + id + "'")
        Else
            m.executarComando("UPDATE " + doc.BaseDados + "..CabecComprasStatus SET " + campo + "=1 WHERE idCabecCompras='" + id + "'")
        End If



        If campo = "Anulado" Then
            'actualizar o valor da grelha
            If tabCtlMaterial.SelectedIndex = 1 Then
                actualizarCoresListaMateriais(doc, id, m.InformModLstMaterial.TIPODOCCOT)
            Else
                actualizarCoresListaMateriais(doc, id, m.InformModLstMaterial.TIPODOCECF)
            End If
        End If
        carregarDadosProjectoCOT(txtProjecto.Text)
        carregarDadosProjectoECF(txtProjecto.Text)


        MsgBox("Documento " + texto + " com sucesso!", vbInformation)

    End Sub

    Private Function pesquisarArtigo(artigo As String, row As DataRow) As String
        Dim m As MotorLM
        m = MotorLM.GetInstance()
        Return m.daArtigoEquivalente(artigo, row)
    End Function

    Private Sub gridArtigos_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles gridArtigos.PreviewKeyDown

        If e.KeyCode = Keys.F4 Then
            If gridArtigos.FocusedColumn Is Nothing Then Exit Sub
            If gridArtigos.FocusedColumn.Tag = "" Then Exit Sub
            abrirFormLista(gridArtigos, gridArtigos.FocusedRow.Index, gridArtigos.FocusedColumn.Index, mGridLST)
        End If

        If e.KeyCode = Keys.Space Then
            If gridArtigos.FocusedColumn Is Nothing Then
                Dim indexCb As Integer
                Dim indexRow As Integer
                If gridArtigos.FocusedRow Is Nothing Then Exit Sub
                indexCb = mGridLST.buscarIndexCampo("CHECKBOX")
                indexRow = gridArtigos.FocusedRow.Index
                If indexCb <> -1 Then
                    gridArtigos.Records.Record(indexRow).Item(indexCb).Checked = Not gridArtigos.Records.Record(indexRow).Item(indexCb).Checked
                End If
            End If
        End If
    End Sub

    Private Sub gridArtigos_InplaceButtonDown(sender As Object, e As AxXtremeReportControl._DReportControlEvents_InplaceButtonDownEvent) Handles gridArtigos.InplaceButtonDown

        abrirFormLista(gridArtigos, e.button.Row.Index, e.button.Column.Index, mGridLST)
    End Sub


    Private Sub gridArtigosCOT_InplaceButtonDown(sender As Object, e As AxXtremeReportControl._DReportControlEvents_InplaceButtonDownEvent) Handles gridArtigosCOT.InplaceButtonDown
        abrirFormLista(gridArtigosCOT, e.button.Row.Index, e.button.Column.Index, mGridCOT)
    End Sub


    Private Sub gridArtigosENC_InplaceButtonDown(sender As Object, e As AxXtremeReportControl._DReportControlEvents_InplaceButtonDownEvent) Handles gridArtigosENC.InplaceButtonDown
        abrirFormLista(gridArtigosENC, e.button.Row.Index, e.button.Column.Index, mGridENC)
    End Sub

    Private Function formatarQuery(grelha As AxXtremeReportControl.AxReportControl, query As String, linha As Integer) As String
        Dim col As ReportColumn
        query = UCase(query)
        For Each col In grelha.Columns
            query = Replace(query, "@@" + col.Tag + "@@", grelha.Records(linha)(col.Index).Value)
        Next
        Return query
    End Function
    Private Sub abrirFormLista(grelha As AxXtremeReportControl.AxReportControl, linha As Integer, coluna As Integer, mgrid As MotorGrelhas)
        Dim frm As FrmLista
        frm = New FrmLista
        Dim query As String
        Dim m As MotorLM
        Dim lista As StdBELista
        m = MotorLM.GetInstance


        query = mgrid.buscarSubQuery(grelha.Columns(coluna).Tag)

        If InStr(query, "@@") <> 0 Then
            query = formatarQuery(grelha, query, linha)
        End If


        If query = "" Then Exit Sub

        lista = m.consulta("select * from (" + query + ") as Query WHERE 1=0")

        If lista Is Nothing Then
            Exit Sub
        End If
        Dim alinhamentoColunas() As Integer
        ReDim alinhamentoColunas(lista.NumColunas)
        frm.setalinhamentoColunas(alinhamentoColunas)
        frm.setComando(query)
        frm.ShowFilter(True)
        frm.monstrarAddPecas(grelha.Columns(coluna).Tag = "CDU_OBRAN1")
        frm.ShowDialog()

        If frm.seleccionouOK Then

            If grelha.Columns(coluna).Tag <> "FORNECEDOR" Then
                grelha.Rows(linha).Record(coluna).Value = frm.getValor(0)
                grelha.Rows(linha).Record(coluna).Caption = frm.getValor(0)
            End If

            If gridArtigos.Columns(coluna).Caption.ToUpper() = "PEÇA" Then
                coluna = buscarIndexCampo(gridArtigos, "DESCRICAO")
                If coluna <> 0 Then
                    grelha.Rows(linha).Record(coluna).Value = frm.getValor(1)
                    grelha.Rows(linha).Record(coluna).Caption = frm.getValor(1)
                End If
            End If

            If grelha.Columns(coluna).Tag = "FORNECEDOR" Then

                If grelha.Rows(linha).Record(coluna).Value <> "" Then
                    grelha.Rows(linha).Record(coluna).Value += ";"
                End If
                grelha.Rows(linha).Record(coluna).Value += frm.getValor(1)
                grelha.Rows(linha).Record(coluna).Caption = grelha.Rows(linha).Record(coluna).Value
            End If
        End If
    End Sub


    Private Sub FrmListaEncomendas_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        resizeAPL()
    End Sub

    Public Sub resizeAPL()



        tbcMoldes.Left = 5
        tbcMoldes.Width = Me.Width - -tbcMoldes.Left - 35

        ProgressBar1.Left = tbcMoldes.Width - ProgressBar1.Width

        txtNomeProjecto.Width = tbcMoldes.Width - txtNomeProjecto.Left - 25

        tabCtlMaterial.Left = tbcMoldes.Left
        tabCtlMaterial.Width = tbcMoldes.Width
        tabCtlMaterial.Height = Me.Height - tabCtlMaterial.Top - 60

        tabCtlGeral.Left = 0
        tabCtlGeral.Width = tabCtlMaterial.Width
        tabCtlGeral.Height = tabCtlMaterial.Height - 25


        txtFornecedor.Left = tabCtlMaterial.Width - txtFornecedor.Width - 25
        lblFornecedor.Left = txtFornecedor.Left
        ckbVerTodos.Left = tabCtlMaterial.Width - ckbVerTodos.Width - 25

        txtObservacoesLST.Left = txtFornecedor.Left
        txtObservacoesLST.Width = txtFornecedor.Width
        txtObservacoesLST.Top = tabCtlGeral.Height - txtObservacoesLST.Height - 35
        lblObsLST.Left = lblFornecedor.Left
        lblObsLST.Top = tabCtlGeral.Height - txtObservacoesLST.Height - 60

        tbcFornecedores.Left = txtFornecedor.Left
        tbcFornecedores.Width = txtFornecedor.Width
        tbcFornecedores.Height = txtObservacoesLST.Top - tbcFornecedores.Top - 25

        lstFornecedor.Height = tbcFornecedores.Height - 35

        gridArtigos.Width = txtFornecedor.Left - 20
        gridArtigos.Height = tabCtlGeral.Height - gridArtigos.Top - 35

        gridArtigosAlt.Width = gridArtigos.Width
        gridArtigosAlt.Height = gridArtigos.Height

        lstCarregarListaMaterial.Left = gridArtigos.Width - lstCarregarListaMaterial.Width + 10

        btnAddRowLST.Left = lstCarregarListaMaterial.Left - btnAddRowLST.Width - 15
        btnRemoveRowLST.Left = btnAddRowLST.Left - btnRemoveRowLST.Width - 5

        lblUtilActualizacaoLST.Left = btnRemoveRowLST.Left - lblUtilActualizacaoLST.Width
        lblUtilGravacaoLST.Left = btnRemoveRowLST.Left - lblUtilGravacaoLST.Width

        lblUtilActualizacaoLST.Top = btnRemoveRowLST.Top
        lblUtilGravacaoLST.Top = lblUtilActualizacaoLST.Top + 20


        'COTACAO

        gridArtigosCOT.Width = Me.Width - gridArtigosCOT.Left - 60
        gridArtigosCOT.Height = tabCtlMaterial.Height - gridArtigosCOT.Top - 140
        gridDocsCOT.Height = tabCtlMaterial.Height - gridDocsCOT.Top - 35

        txtObservacoesCOT.Top = gridDocsCOT.Height + gridDocsCOT.Top - txtObservacoesCOT.Height
        txtObservacoesCOT.Width = gridArtigosCOT.Width

        lblObsCOT.Top = gridArtigosCOT.Top + gridArtigosCOT.Height + 5

        btnAddRowPCO.Top = 3
        btnRemoveRowPCO.Top = btnAddRowPCO.Top
        btnAddRowPCO.Left = gridArtigosCOT.Left + gridArtigosCOT.Width - btnAddRowPCO.Width
        btnRemoveRowPCO.Left = btnAddRowPCO.Left - btnRemoveRowPCO.Width - 5


        lblUtilActualizacaoCOT.Left = btnRemoveRowPCO.Left - lblUtilActualizacaoCOT.Width
        lblUtilGravacaoCOT.Left = btnRemoveRowPCO.Left - lblUtilGravacaoCOT.Width

        lblUtilActualizacaoCOT.Top = btnRemoveRowPCO.Top
        lblUtilGravacaoCOT.Top = lblUtilActualizacaoCOT.Top + 20



        'ENCOMENDAS
        gridArtigosENC.Width = Me.Width - gridArtigosENC.Left - 60
        gridArtigosENC.Height = tabCtlMaterial.Height - gridArtigosENC.Top - 260
        gridDocsENC.Height = tabCtlMaterial.Height - gridDocsENC.Top - 35

        txtObservacoesENC.Top = gridDocsENC.Height + gridDocsENC.Top - txtObservacoesENC.Height


        txtObservacoesENC.Width = (gridArtigosENC.Width * (1 / 3)) - 5

        lblObsENC.Top = gridArtigosENC.Top + gridArtigosENC.Height + 5

        dtpDataFatura.Top = lblObsENC.Top
        dtpDataFatura.Left = gridArtigosENC.Left + gridArtigosENC.Width - dtpDataFatura.Width
        lblDtFatura.Top = lblObsENC.Top

        lblDtFatura.Left = dtpDataFatura.Left - lblDtFatura.Width - 5


        lblMatEntrega.Top = lblObsENC.Top
        gridArtigosENT.Top = txtObservacoesENC.Top
        gridArtigosENT.Height = txtObservacoesENC.Height
        gridArtigosENT.Left = txtObservacoesENC.Left + txtObservacoesENC.Width + 5
        gridArtigosENT.Width = (gridArtigosENC.Width * (2 / 3)) - 5

        lblMatEntrega.Left = gridArtigosENT.Left
        btnAddRowENC.Top = 3
        btnRemoveRowENC.Top = btnAddRowENC.Top
        btnAddRowENC.Left = gridArtigosENC.Left + gridArtigosENC.Width - btnAddRowENC.Width
        btnRemoveRowENC.Left = btnAddRowENC.Left - btnRemoveRowENC.Width - 5


        lblUtilActualizacaoENC.Left = btnRemoveRowENC.Left - lblUtilActualizacaoENC.Width
        lblUtilGravacaoENC.Left = btnRemoveRowENC.Left - lblUtilGravacaoENC.Width


        lblUtilActualizacaoENC.Top = btnRemoveRowENC.Top
        lblUtilGravacaoENC.Top = lblUtilActualizacaoENC.Top + 20


        lblDataUltCarregamentoLista.Top = tabCtlGeral.Top + tabCtlGeral.Height - 15

        Dim m As MotorLM
        Group3.Left = Me.Width - Group3.Width - 35
        Group2.Left = Group3.Left - Group2.Width - 5

        m = MotorLM.GetInstance
        If Not m.FuncionarioAdm Then
            gridArtigos.Width = Me.Width - gridArtigos.Left - 50
            ' Me.Width = gridArtigos.Left + gridArtigos.Width + gridArtigos.Left
            txtObservacoesLST.Top = lblArtigos.Top
            txtObservacoesLST.Left = gridArtigos.Left + gridArtigos.Width - txtObservacoesLST.Width
            lblObsLST.Top = txtObservacoesLST.Top
            lblObsLST.Left = txtObservacoesLST.Left - 50
            Group1.Location = Group3.Location
        Else
            Group1.Left = Group2.Left - Group1.Width - 5
        End If

        Group4.Left = Group1.Left - Group2.Width - 50

    End Sub

    Private Sub gridArtigos_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigos.ValueChanged
        validarValorCampo(mGridLST, gridArtigos, e)
    End Sub

    Private Sub gridArtigosCOT_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigosCOT.ValueChanged
        validarValorCampo(mGridCOT, gridArtigosCOT, e)
        actualizarTotaisDocumento(buscarObjetoDocumento(1, 1), gridArtigosCOT, mGridCOT, TotalDocCOT)
    End Sub

    Private Sub gridArtigosENC_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridArtigosENC.ValueChanged
        validarValorCampo(mGridENC, gridArtigosENC, e)
        actualizarTotaisDocumento(buscarObjetoDocumento(2, 2), gridArtigosENC, mGridENC, TotalDocENC)
    End Sub

    Private Function validarValorCampo(mgrid As MotorGrelhas, grid As AxXtremeReportControl.AxReportControl, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent)
        Dim result As String
        Dim row As DataRow

        result = mgrid.validarCampo(e.column.Tag, e.item.Value)

        If result = "" Then
            Dim valor As Object
            valor = mgrid.formatarValorCampo(e.column.Tag, e.item.Value, False)
            e.item.Value = valor
            e.item.Caption = e.item.Value
            row = e.row.Record.Tag

            If row.Table.Columns.IndexOf(e.column.Tag) <> -1 Then
                row(e.column.Tag) = valor
            End If

            If e.column.Tag <> "ARTIGO" Then
                e.item.Tag = e.item.Value
            End If

            If e.column.Tag = "QUANTFALTA" Then
                Dim index As Integer
                index = mgrid.buscarIndexCampo("CHECKBOX")
                e.row.Record.Item(index).Checked = True
            End If
        Else
            MsgBox(result, MsgBoxStyle.Critical)
            e.item.Value = e.item.Tag
            e.item.Caption = mgrid.formatarValorCampo(e.column.Tag, e.item.Tag, False)
        End If
    End Function


    ''' <summary>
    ''' FUnção que permite actualizar o estado
    ''' </summary>
    ''' <param name="grelha"></param>
    ''' <param name="mgrid"></param>
    Private Sub actualizarEstadoGrelha(ht As Hashtable, grelha As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas)

        Dim idLinha As String
        Dim index As Integer
        For Each idLinha In htLinhaModificadas.Keys
            If ht.ContainsKey(idLinha) Then
                index = ht(idLinha)
                actualizarEstadoLinhaGrelha(index, grelha, mgrid)
            End If

        Next
        'For Each linha In grelha.Records

        'Next
        grelha.Populate()
    End Sub



    ''' <summary>
    ''' FUnção que permite actualizar o estado
    ''' </summary>
    ''' <param name="grelha"></param>
    ''' <param name="mgrid"></param>
    Private Sub actualizarEstadoLinhaGrelha(indexLinha As Integer, grelha As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas)
        Dim linha As ReportRecord
        Dim dtRow As DataRow
        Dim m As MotorLM
        m = MotorLM.GetInstance()
        Dim estado As String
        Dim nummAlt As String
        Dim cor As String
        Dim index As Double
        Dim valor As String

        ' For Each linha In grelha.Records()

        linha = grelha.Records(indexLinha)

        dtRow = linha.Tag
        estado = m.consultaValor("SELECT CDU_ObraN5 FROM LinhasCompras WHERE id='" + dtRow("ID").ToString() + "'")
        dtRow("CDU_ObraN5") = estado
        nummAlt = mgrid.buscarNumeroAlteracoes(dtRow("ID").ToString())
        index = mgrid.buscarIndexCampo("NUMEROALTERACOES")
        If index <> -1 Then
            linha(index).Value = nummAlt
            linha(index).Caption = nummAlt
        End If

        index = mgrid.buscarIndexCampo("FORNECEDOR")
        valor = mgrid.buscarEntidade(dtRow("Id").ToString(), "", txtProjecto.Text)

        If index <> -1 Then
            linha(index).Value = valor
            linha(index).Caption = valor
        End If


        index = mgrid.buscarIndexCampo("DATAENTREGA")
        If index <> -1 And grelha.Name = "gridArtigos" Then
            valor = mgrid.buscarCampoLinhaAssociada(txtProjecto.Text, dtRow("Id").ToString(), "DATAENTREGA", "")
            If valor <> "" Then
                valor = FormatDateTime(m.NuloToDate(valor, Now), DateFormat.ShortDate)
            End If
            linha(index).Value = valor
            linha(index).Caption = valor
        End If


        index = mgrid.buscarIndexCampo("CDU_DATARECEPCAO")
        If index <> -1 And grelha.Name = "gridArtigos" Then
            valor = mgrid.buscarCampoLinhaAssociada(txtProjecto.Text, dtRow("Id").ToString(), "CDU_DATARECEPCAO", "")
            If valor <> "" Then
                valor = FormatDateTime(m.NuloToDate(valor, Now), DateFormat.ShortDate)
            End If

            linha(index).Value = valor
            linha(index).Caption = valor
        End If

        index = mgrid.buscarIndexCampo("CHECKBOX")

        If index <> -1 And grelha.Name = "gridArtigosENC" Then
            linha(index).Checked = Not IsDBNull(dtRow("CDU_DataRecepcao"))
        End If

        cor = buscarCor(estado)
        aplicarCorLinha(grelha, linha, cor)

        aplicarEstadoLinha(grelha, linha, estado)
        ' Next
        '   grelha.Populate()
    End Sub

    Private Function buscarCorDocumentoDocs(grelha As AxXtremeReportControl.AxReportControl) As String
        Dim linha As ReportRecord
        Dim cor As String
        Dim id As String
        Dim cor2 As String = ""
        Dim o As Objeto

        For Each linha In grelha.Records
            o = linha.Tag
            cor = buscarCorDocumento(grelha, o, cor2)
            aplicarCorLinha(grelha, linha, cor)
            ' aplicarEstadoLinha(grelha, linha, estado)
        Next
    End Function


    Private Function buscarCorDocumento(grid As AxXtremeReportControl.AxReportControl, o As Objeto, ByRef cor2 As String) As String
        Dim m As MotorLM
        m = MotorLM.GetInstance
        Dim count As String
        Dim cor As String
        Dim conf As String

        cor = buscarCor(ESTADO_NORMAL)
        If o.Id = "" Then
            Return cor
        End If
        If grid.Tag = "ENC" Then
            conf = LCase(m.consultaValor("SELECT CDU_Conferido FROM " + o.BaseDados + "..CabecCompras where id='" + o.Id + "'"))
            count = m.consultaValor("SELECT Count(*) FROM " + o.BaseDados + "..LinhasCompras WHERE tipoLinha<>60 and IdCabecCompras='" + o.Id + "' and ISNULL(CDU_ObraN5,'')<>'" + ESTADO_VER + "'")
            If count = "0" Then cor = buscarCor(ESTADO_VER)
            If conf = "true" Then cor = buscarCor(ESTADO_CONF)
        End If

        If grid.Tag = "COT" Then
            count = m.consultaValor("SELECT Count(*) FROM " + o.BaseDados + "..LinhasCompras WHERE IdCabecCompras='" + o.Id + "' and ISNULL(CDU_ObraN5,'')<>'" + ESTADO_ECF + "'")
            If count = "0" Then cor = buscarCor(ESTADO_ECF)

            count = m.consultaValor("SELECT Count(*) FROM " + o.BaseDados + "..LinhasCompras WHERE IdCabecCompras='" + o.Id + "' and ISNULL(CDU_ObraN5,'')<>'" + ESTADO_VER + "'")
            If count = "0" Then cor = buscarCor(ESTADO_VER)

        End If

        count = m.consultaValor("SELECT Count(*) FROM " + o.BaseDados + "..CabecComprasStatus WHERE IdCabecCompras='" + o.Id + "' AND (Anulado=1)")
        If count <> "0" Then cor = buscarCor(ESTADO_A)

        cor2 = cor

        count = m.consultaValor("SELECT Count(*) FROM " + o.BaseDados + "..CabecComprasStatus WHERE IdCabecCompras='" + o.Id + "' AND (Fechado=1)")
        If count <> "0" Then cor2 = buscarCor(ESTADO_A)

        Return cor
    End Function

    Private Sub btnRemoveRowPCO_Click(sender As Object, e As EventArgs) Handles btnRemoveRowPCO.Click
        apagarLinhaRegisto(hashCOT, gridArtigosCOT)
    End Sub

    Private Sub btnAddRowPCO_Click(sender As Object, e As EventArgs) Handles btnAddRowPCO.Click
        inserirNovaLinhaRegisto(False, gridArtigosCOT, mGridCOT, hashCOT, -1)
    End Sub


    Private Sub btnRemoveRowLST_Click(sender As Object, e As EventArgs) Handles btnRemoveRowLST.Click
        apagarLinhaRegisto(hashLST, gridArtigos)
    End Sub

    Private Sub btnAddRowLST_Click(sender As Object, e As EventArgs) Handles btnAddRowLST.Click
        inserirNovaLinhaRegisto(False, gridArtigos, mGridLST, hashLST, -1)

    End Sub

    Private Sub btnRemoveRowENC_Click(sender As Object, e As EventArgs) Handles btnRemoveRowENC.Click
        apagarLinhaRegisto(hashENC, gridArtigosENC)
    End Sub

    Private Sub btnAddRowENC_Click(sender As Object, e As EventArgs) Handles btnAddRowENC.Click
        inserirNovaLinhaRegisto(True, gridArtigosENC, mGridENC, hashENC, -1)

    End Sub

    ''' <summary>
    ''' Função que permite o envio interno de email à pessoa responsavel 
    ''' </summary>
    ''' <param name="botao"></param>
    ''' <param name="idDoc"></param>
    ''' <param name="docOrig"></param>
    Public Sub enviarEmailInterno(botao As Integer, doc As Objeto, docOrig As Objeto)
        Dim m As MotorLM
        m = MotorLM.GetInstance()

        If doc Is Nothing Then
            doc = New Objeto
        End If

        If docOrig Is Nothing Then
            docOrig = New Objeto
        End If

        If (doc.Id <> "" And docOrig.Id = "" And botao <> 0) Or (botao = 0 And doc.Id <> "") Then

            Dim enviarEmail As Boolean = False
            Dim email As String = ""

            If botao = 0 And m.InformModLstMaterial.EMAILLM <> "" Then
                enviarEmail = True
                email = m.InformModLstMaterial.EMAILLM
            End If

            If botao = 1 And m.InformModLstMaterial.EMAILCOT <> "" Then
                enviarEmail = True
                email = m.InformModLstMaterial.EMAILCOT
            End If
            If botao = 2 And m.InformModLstMaterial.EMAILECF <> "" Then
                enviarEmail = True
                email = m.InformModLstMaterial.EMAILECF
            End If

            If enviarEmail Then
                '  If MsgBox("Deseja Enviar email ás Compras?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                m.enviarEmailDocumento(doc, "C", botao, email)
                Me.Cursor = Cursors.Default
                'End If
            End If
        End If
    End Sub


    Private Sub actualizarCoresListaMateriais(o As Objeto, id As String, tipodoc As String)
        Dim m As MotorLM
        Dim dt As DataTable
        Dim dtrow As DataRow
        m = MotorLM.GetInstance()
        Dim estado As String
        Dim idLinha As String
        Dim idLinhaF As String

        'Dim o As Objeto
        'o = New Objeto

        'o.Id = id
        'o.BaseDados = m.Empresa

        Dim docOrig As Objeto

        docOrig = gridArtigos.Tag


        dt = m.daListaArtigosDocumento(o, "C")

        htLinhaModificadas = New Hashtable

        For Each dtrow In dt.Rows
            idLinha = m.buscarIdLinhaOriginal(o, dtrow("Id").ToString, "C")
            If idLinha <> "" Then
                idLinhaF = m.buscarIdLinhaFinal(o, idLinha, "C")
                If idLinhaF <> "" Then
                    estado = m.buscarEstadoIdLinha(o, idLinhaF, "C")
                Else
                    estado = m.buscarEstadoIdLinha(o, idLinha, "C")
                    idLinhaF = idLinha
                End If

                m.actualizarEstadoLinha(o.BaseDados, docOrig.BaseDados, docOrig.Projeto, htLinhaModificadas, "C", idLinhaF, estado, True, tipodoc)

            End If
            'estado = m.buscarEstadoLinhaValida(dtrow("Id").ToString, "C")
            'm.actualizarEstadoLinha(htLinhaModificadas, "C", dtrow("Id").ToString, estado, True, tipodoc)
        Next

        actualizarEstadoGrelha(hashLST, gridArtigos, mGridLST)
        actualizarEstadoGrelha(hashCOT, gridArtigosCOT, mGridCOT)
        actualizarEstadoGrelha(hashENC, gridArtigosENC, mGridENC)

    End Sub

    Private Sub gridArtigosENC_SelectionChanged(sender As Object, e As EventArgs) Handles gridArtigosENC.SelectionChanged
        actualizarGrelhaStocks()
    End Sub

    Private Sub txtProgressBar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub gridArtigosCOT_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles gridArtigosCOT.PreviewKeyDown
        If e.KeyCode = Keys.F4 Then
            If gridArtigosCOT.FocusedColumn Is Nothing Then Exit Sub
            If gridArtigosCOT.FocusedColumn.Tag = "" Then Exit Sub
            abrirFormLista(gridArtigosCOT, gridArtigosCOT.FocusedRow.Index, gridArtigosCOT.FocusedColumn.Index, mGridCOT)
        End If
    End Sub

    Private Sub gridArtigosENC_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles gridArtigosENC.PreviewKeyDown
        If e.KeyCode = Keys.F4 Then
            If gridArtigosENC.FocusedColumn Is Nothing Then Exit Sub
            If gridArtigosENC.FocusedColumn.Tag = "" Then Exit Sub
            abrirFormLista(gridArtigosENC, gridArtigosENC.FocusedRow.Index, gridArtigosENC.FocusedColumn.Index, mGridENC)
        End If
    End Sub

    Private Sub btnPreview_Click(sender As Object, e As EventArgs) Handles btnPreview.Click
        gridArtigos.PrintPreview(True)
    End Sub



    Private Sub aplicarFiltro(tag As String)

        If ssRodape.Tag <> tag Then
            ssRodape.Tag = tag
        Else
            ssRodape.Tag = "-1"
            tag = "-1"
        End If


        Dim r As ReportRecord
        For Each r In gridArtigos.Records
            Dim estado As String
            r.Visible = True
            estado = r.Item(mGridLST.buscarIndexCampo("CHECKBOX")).Tag
            If estado <> tag And tag <> "-1" Then
                r.Visible = False
            End If
        Next

        gridArtigos.Populate()
    End Sub
    Private Sub tsAberto_Click(sender As Object, e As EventArgs) Handles tsAberto.Click
        aplicarFiltro(tsAberto.Tag)
    End Sub

    Private Sub tsPCotacao_Click(sender As Object, e As EventArgs) Handles tsPCotacao.Click
        aplicarFiltro(tsPCotacao.Tag)
    End Sub

    Private Sub tsEncomendado_Click(sender As Object, e As EventArgs) Handles tsEncomendado.Click
        aplicarFiltro(tsEncomendado.Tag)
    End Sub

    Private Sub tsInterno_Click(sender As Object, e As EventArgs) Handles tsInterno.Click
        aplicarFiltro(tsInterno.Tag)
    End Sub

    Private Sub tsProblema_Click(sender As Object, e As EventArgs) Handles tsProblema.Click
        aplicarFiltro(tsProblema.Tag)
    End Sub

    Private Sub tsMaterialCliente_Click(sender As Object, e As EventArgs) Handles tsMaterialCliente.Click
        aplicarFiltro(tsMaterialCliente.Tag)
    End Sub

    Private Sub tssubcontratado_Click(sender As Object, e As EventArgs) Handles tssubcontratado.Click
        aplicarFiltro(tssubcontratado.Tag)
    End Sub

    Private Sub tsNovo_Click(sender As Object, e As EventArgs) Handles tsNovo.Click
        aplicarFiltro(tsNovo.Tag)
    End Sub

    Private Sub tsAtualizado_Click(sender As Object, e As EventArgs) Handles tsAtualizado.Click
        aplicarFiltro(tsAtualizado.Tag)
    End Sub

    Private Sub tsVerificado_Click(sender As Object, e As EventArgs) Handles tsVerificado.Click
        aplicarFiltro(tsVerificado.Tag)
    End Sub

    Private Sub dtPicker_Change(sender As Object, e As EventArgs) Handles dtPicker.Change

    End Sub

    Private Sub dtPicker_CloseUp(sender As Object, e As EventArgs) Handles dtPicker.CloseUp
        Dim str() As String
        str = dtPicker.Tag.ToString.Split(",")
        If str.Length = 2 Then
            Select Case str(0)
                Case "gridArtigosCOT" : actualizarValorData(gridArtigosCOT, CInt(str(1)))
                Case "gridArtigosENC" : actualizarValorData(gridArtigosENC, CInt(str(1)))
                Case "gridArtigosENT" : actualizarValorData(gridArtigosENT, CInt(str(1)))
            End Select
        End If

    End Sub

    Private Sub actualizarValorData(report As AxXtremeReportControl.AxReportControl, indexColuna As Integer)
        report.FocusedRow.Record(indexColuna).Value = CDate(dtPicker.Value).ToShortDateString
        report.Populate()
        MDtPicker.HideDatePicker(dtPicker)
    End Sub

    Private Sub gridArtigosENC_BeginEdit(sender As Object, e As AxXtremeReportControl._DReportControlEvents_BeginEditEvent) Handles gridArtigosENC.BeginEdit
        dtPicker.Visible = False
        If eCampoData(daTabelaModulo(), e.column.Tag) And e.column.Editable Then
            dtPicker.Tag = gridArtigosENC.Name + "," + CStr(e.column.Index)
            MDtPicker.ShowDatePicker(gridArtigosENC, dtPicker, e.row, e.column, e.item)
        End If

    End Sub

    Private Sub gridArtigosENT_BeginEdit(sender As Object, e As AxXtremeReportControl._DReportControlEvents_BeginEditEvent) Handles gridArtigosENT.BeginEdit
        dtPicker.Visible = False
        If eCampoData(daTabelaModulo(), e.column.Tag) And e.column.Editable Then
            dtPicker.Tag = gridArtigosENT.Name + "," + CStr(e.column.Index)
            MDtPicker.ShowDatePicker(gridArtigosENT, dtPicker, e.row, e.column, e.item)
        End If

    End Sub

    Private Sub gridArtigosENC_FocusChanging(sender As Object, e As AxXtremeReportControl._DReportControlEvents_FocusChangingEvent) Handles gridArtigosENC.FocusChanging
        MDtPicker.HideDatePicker(dtPicker)
    End Sub

    Private Sub gridArtigosENT_FocusChanging(sender As Object, e As AxXtremeReportControl._DReportControlEvents_FocusChangingEvent) Handles gridArtigosENT.FocusChanging
        MDtPicker.HideDatePicker(dtPicker)
    End Sub
    Private Function eCampoData(tabela As String, campo As String) As Boolean
        Dim type As String
        Dim m As MotorAPL
        m = New MotorAPL(Motor.GetInstance())
        type = m.daTipoCampo(tabela, campo)
        Return type = "datetime" Or type = "smalldatetime"
    End Function


    Private Function daTabelaModulo() As String
        Dim tabela As String
        tabela = ""
        Select Case modulo
            Case "V" : tabela = "LinhasDoc"
            Case "C" : tabela = "LinhasCompras"
        End Select
        Return tabela
    End Function

    Private Sub btnRepartirCustos_Click(sender As Object, e As EventArgs) Handles btnRepartirCustos.Click
        Dim frmCustos As FrmReparticaoValores
        frmCustos = New FrmReparticaoValores
        frmCustos.ShowDialog()

        If frmCustos.seleccionouOK Then
            repartirCustos(frmCustos.getValor(), gridArtigosENC, mGridENC)
        End If
        actualizarTotaisDocumento(buscarObjetoDocumento(2, 2), gridArtigosENC, mGridENC, TotalDocENC)
    End Sub

    Private Sub repartirCustos(total As Double, grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas)
        Dim quantidadeTotal As Double
        Dim custoIndividual As Double
        quantidadeTotal = daQuantidadeTotalDocumento(grid, mGridENC)

        If quantidadeTotal <> 0 Then
            custoIndividual = total / quantidadeTotal
        End If
        actualizarPrecUnitLinhas(custoIndividual, grid, mgrid)
    End Sub

    Private Sub btnAplicarTodos_Click(sender As Object, e As EventArgs) Handles btnAplicarTodos.Click
        aplicarTodos(gridArtigosENC)
    End Sub

    Private Function daQuantidadeTotalDocumento(grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas) As Double
        Dim total As Double
        Dim row As ReportRow
        Dim index As Integer

        For Each row In grid.Rows
            index = mgrid.buscarIndexCampo("QUANTIDADE")
            If index <> -1 Then
                If IsNumeric(row.Record(index).Value) Then
                    total += CDbl(row.Record(index).Value)

                End If
            End If
        Next
        Return total
    End Function


    Private Sub actualizarPrecUnitLinhas(precUnit As Double, grid As AxXtremeReportControl.AxReportControl, mgrid As MotorGrelhas)
        Dim indexQ As Integer
        Dim indexP As Integer
        indexQ = mgrid.buscarIndexCampo("QUANTIDADE")
        indexP = mgrid.buscarIndexCampo("PRECUNIT")

        If indexQ <> -1 And indexP <> -1 Then
            For Each row In grid.Rows
                If IsNumeric(row.Record(indexQ).Value) Then
                    If CDbl(row.Record(indexQ).Value) > 0 Then
                        row.Record(indexP).value = Math.Round(precUnit, 2)
                        row.Record(indexP).Caption = mgrid.formatarValorCampo("PRECUNIT", row.Record(indexP).value, False)
                    End If

                End If
            Next
        End If
        grid.Populate()

    End Sub

    Private Sub aplicarTodos(grid As AxXtremeReportControl.AxReportControl)
        Dim col As Integer
        Dim row As Integer
        Dim i As Integer
        Dim record As ReportRecord
        If Not grid.FocusedColumn Is Nothing And Not grid.FocusedRow Is Nothing Then
            col = grid.FocusedColumn.Index
            row = grid.FocusedRow.Index
            For i = row To grid.Records.Count - 1

                record = grid.Records(i)
                record(col).Value = grid.Records(row)(col).Value

                If record(col).HasCheckbox Then
                    record(col).Checked = grid.Records(row)(col).Checked
                End If
            Next
            grid.Populate()
        End If
    End Sub

    Private Sub dtPicker_Leave(sender As Object, e As EventArgs) Handles dtPicker.Leave

    End Sub

    Private Sub btn4_Click(sender As Object, e As EventArgs) Handles btn4.Click
        saidaStocks(gridArtigos)

    End Sub

    Private Sub saidaStocks(grid As AxReportControl)
        If Not validarGravacao(0) Then
            Exit Sub
        End If



        Dim dt As DataTable
        Dim r As DataRow
        Dim r1 As DataRow
        Dim m As Motor
        Dim row As ReportRecord
        m = Motor.GetInstance()
        Dim art As String
        Dim artInvalido As Boolean = False
        For Each row In grid.Records
            If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then

                art = row(buscarIndexCampo(grid, "ARTIGO")).Value

                If Not m.artigoValido(art, "", True) Then
                    artInvalido = True
                    Exit For
                End If
            End If
        Next

        If artInvalido Then
            MsgBox("Existem artigos inválidos seleccionados!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        dt = m.consultaDataTable("SELECT *,0 as StkActual FROM LinhasCompras WHERE 1=0")

        For Each row In grid.Records
            If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then
                r = dt.NewRow()
                r1 = row.Tag
                r("ID") = r1("ID")
                r("Artigo") = row(buscarIndexCampo(grid, "ARTIGO")).Value
                r("Descricao") = row(buscarIndexCampo(grid, "DESCRICAO")).Value
                r("CDU_ObraN1") = row(buscarIndexCampo(grid, "CDU_OBRAN1")).Value
                r("Quantidade") = row(buscarIndexCampo(grid, "QUANTIDADE")).Value
                r("StkActual") = m.consultaValor("SELECT StkActual FROM Artigo WHERE Artigo='" + r("Artigo") + "'")
                dt.Rows.Add(r)
            End If
        Next

        Dim fs As FrmStocks
        fs = New FrmStocks
        fs.setDataTable(dt)
        fs.setProjeto(txtProjecto.Text)
        fs.ShowDialog()


        Try
            If fs.GravouDocumento Then
                dt = fs.buscarLinhas()
                For Each row In grid.Records
                    If row(buscarIndexCampo(grid, "CHECKBOX")).Checked Then
                        Dim id As String
                        r1 = row.Tag
                        id = r1("ID").ToString()
                        If existeID(id, dt) Then
                            actualizarEstadoLinha(row, grid, "I")
                        End If
                        row(buscarIndexCampo(grid, "CHECKBOX")).Checked = False
                    End If
                Next

            End If
            fs.Close()
            fs = Nothing
        Catch ex As Exception

        End Try

        grid.Populate()

    End Sub

    Private Function existeID(id As String, dt As DataTable)

        For Each row In dt.Rows
            If LCase(row("ID").ToString()) = LCase(id) Then
                Return True
            End If
        Next
        Return False
    End Function





    Private Sub actualizarEstadoLinha(record As ReportRecord, grid As AxReportControl, estado As String)
        Dim m As MotorLM
        Dim dtRow As DataRow
        Dim msg As String = ""
        Dim ok As Boolean = True

        htLinhaModificadas = New Hashtable
        m = MotorLM.GetInstance

        Dim o As Objeto

        o = grid.Tag

        If Not record Is Nothing Then

            If ok Then
                dtRow = record.Tag
                record(buscarIndexCampo(grid, "CHECKBOX")).Tag = estado
                dtRow("CDU_ObraN5") = estado
                m.actualizarEstadoLinha(o.BaseDados, "PRI" + m.Empresa, txtProjecto.Text, htLinhaModificadas, modulo, dtRow("Id").ToString, estado)
                aplicarCorLinha(grid, record, buscarCor(estado))
                aplicarEstadoLinha(grid, record, estado)

            End If
        End If
    End Sub
End Class

