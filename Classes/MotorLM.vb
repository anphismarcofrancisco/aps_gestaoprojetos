﻿Imports PlatAPSNET
Imports Interop.GcpBE900
Imports Interop.StdBE900
Imports Interop.ErpBS900
Imports System.IO
Imports XtremeReportControl
Imports Interop.StdPlatBS900

Public Class MotorLM

    Shared myInstance As MotorLM
    Dim bso As ErpBS
    Dim PlataformaPrimavera As StdPlatBS
    Dim nomeEmpresa_ As String
    Dim codEmpresa As String
    Dim codEmpresaBase As String

    Dim utilizador As String
    Dim password As String
    Dim tipoPlat As String
    Dim ficheiroAccess_ As String
    Dim file As String
    Dim log As TextBox
    Dim funcionario_ As String
    Dim nomefuncionario_ As String

    Dim funcionarioAdm_ As Boolean

    ' Dim modoFuncionamento As String

    Dim empresas_ As String
    Dim apliInicializada As Boolean

    ' Dim ArmazemDefault As String

    Dim objConfApl As StdBSConfApl

    Dim listaDocumentosCompra As String
    Dim listaDocumentosVenda As String
    Dim listaDocumentosStock As String

    Dim campoMolde As String
    Dim campoRequisicao As String

    Dim apresentaEncomenda As Boolean
    Dim apresentaProjectos As Boolean
    ' Dim campoArtigoPreco As String
    Dim artInsp1 As String
    Dim artInsp2 As String
    Dim entidadeDefault As String

    Dim infModLM As InfModLstMaterial
    Dim emailDestino As String

    Dim EXCELFOLHA As String
    Dim visualizarEmail As String
    Dim apresentaArtigo As String
    Dim apresentaFornecedor As String

    Dim numLicenciamentos As Integer

    Dim indexExcel As Integer
    Dim apresentaFormAPL As Boolean


    Const COLUMN_PECA As Integer = 0
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTIDADE As Integer = 2
    Const COLUMN_DIMENSOES As Integer = 3
    Const COLUMN_MATERIAL As Integer = 4
    Const COLUMN_FORNECEDOR As Integer = 5
    Const COLUMN_ARTIGO As Integer = 6
    Const COLUMN_DATARECEPCAO As Integer = 7
    Const COLUMN_CHECKED As Integer = 8

    Private Sub New()
        Try
            Dim ficheiroConfiguracao As Configuracao
            Dim ficheiroIni As IniFile

            apliInicializada = False
            ficheiroConfiguracao = Configuracao.GetInstance
            ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)

            bso = New ErpBS
            PlataformaPrimavera = New StdPlatBS
            Dim seg As Seguranca
            seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

            utilizador = ficheiroIni.GetString("ERP", "UTILIZADOR", "")

            utilizador = seg.decifrarStringTripleDES(utilizador)

            password = ficheiroIni.GetString("ERP", "PASSWORD", "")

            password = seg.decifrarStringTripleDES(password)

            codEmpresa = ficheiroIni.GetString("ERP", "EMPRESA", "")
            codEmpresaBase = codEmpresa

            empresas_ = ficheiroIni.GetString("ERP", "EMPRESAS", "'" + codEmpresa + "'")
            tipoPlat = ficheiroIni.GetString("ERP", "TIPOPLAT", 1)

            campoMolde = ficheiroIni.GetString("DOCUMENTO", "CAMPOMOLDE", "CDU_VMolde")
            campoRequisicao = ficheiroIni.GetString("DOCUMENTO", "CAMPOREQUISICAO", "CDU_VRequisicao")

            listaDocumentosCompra = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSC", "")
            listaDocumentosVenda = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSV", "")
            listaDocumentosStock = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSS", "")

            tipoDoc_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOC", "")
            serieC_ = ficheiroIni.GetString("DOCUMENTO", "SERIEC", "")
            tipoEntidadeC_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADEC", "F")

            apresentaEncomenda = ficheiroIni.GetBoolean("DOCUMENTO", "APRESENTAENCOMENDA", True)

            artInsp1 = ficheiroIni.GetString("DOCUMENTO", "ARTINSP1", "")
            artInsp2 = ficheiroIni.GetString("DOCUMENTO", "ARTINSP2", "")

            apresentaProjectos = ficheiroIni.GetBoolean("DOCUMENTO", "APRESENTAPROJECTOS", True)

            entidadeDefault = ficheiroIni.GetString("DOCUMENTO", "ENTIDADEDEFAULT", "FVD")

            indexExcel = ficheiroIni.GetInteger("LISTAMATERIAIS", "VERSAOEXCEL", ficheiroIni.GetInteger("ERP", "VERSAOEXCEL", "1"))

            infModLM = New InfModLstMaterial
            infModLM.TIPODOCLM = ficheiroIni.GetString("LISTAMATERIAIS", "TIPODOCLM", "")
            infModLM.SERIELM = ficheiroIni.GetString("LISTAMATERIAIS", "SERIELM", "")
            infModLM.MAPALM = ficheiroIni.GetString("LISTAMATERIAIS", "MAPALM", "")
            infModLM.TIPODOCCOT = ficheiroIni.GetString("LISTAMATERIAIS", "TIPODOCCOT", "")
            infModLM.SERIECOT = ficheiroIni.GetString("LISTAMATERIAIS", "SERIECOT", "")
            infModLM.MAPACOT = ficheiroIni.GetString("LISTAMATERIAIS", "MAPACOT", "")
            infModLM.TIPODOCECF = ficheiroIni.GetString("LISTAMATERIAIS", "TIPODOCECF", "")
            infModLM.SERIEECF = ficheiroIni.GetString("LISTAMATERIAIS", "SERIEECF", "")
            infModLM.MAPAECF = ficheiroIni.GetString("LISTAMATERIAIS", "MAPAECF", "")
            infModLM.TIPODOCALT = ficheiroIni.GetString("LISTAMATERIAIS", "TIPODOCALT", "ALT")
            infModLM.SERIEALT = ficheiroIni.GetString("LISTAMATERIAIS", "SERIEALT", "")
            infModLM.ARTIGO = ficheiroIni.GetString("LISTAMATERIAIS", "ARTIGO", "")
            infModLM.EMAILECF = ficheiroIni.GetString("LISTAMATERIAIS", "EMAILECF", ficheiroIni.GetString("LISTAMATERIAIS", "EMAIL", ""))
            infModLM.EMAILLM = ficheiroIni.GetString("LISTAMATERIAIS", "EMAILLM", "")
            infModLM.EMAILCOT = ficheiroIni.GetString("LISTAMATERIAIS", "EMAILCOT", "")

            infModLM.TIPODOCSTK = ficheiroIni.GetString("LISTAMATERIAIS", "TIPODOCSTK", "")

            infModLM.EXCEL_INICIO = ficheiroIni.GetString("LISTAMATERIAIS", "EXCELINICIO", "dwg")

            infModLM.IMPRIMIEAOENVIAR = ficheiroIni.GetString("LISTAMATERIAIS", "IMPRIMEAOENVIAR", "0")

            infModLM.CSVgerarFicheiro = ficheiroIni.GetString("LISTAMATERIAIS", "CSVGERARFICHEIRO", "0") = "1"
            infModLM.CSVNomeFicheiro = ficheiroIni.GetString("LISTAMATERIAIS", "CSVNOMEFICHEIRO", "0")
            infModLM.CSVviewSQL = ficheiroIni.GetString("LISTAMATERIAIS", "CSVIEWSQL", "")

            infModLM.RefDocOrigLM = ficheiroIni.GetString("LISTAMATERIAIS", "REFDOCORIGLM", "1") = "1"
            infModLM.RefDocOrigCOT = ficheiroIni.GetString("LISTAMATERIAIS", "REFDOCORIGCOT", "1") = "1"
            infModLM.RefDocOrigECF = ficheiroIni.GetString("LISTAMATERIAIS", "REFDOCORIGECF", "1") = "1"
            infModLM.somenteAdmin = ficheiroIni.GetString("LISTAMATERIAIS", "SOMENTEADMIN", "1") = "1"


            infModLM.nomeFicheiroPDFECF = ficheiroIni.GetString("LISTAMATERIAIS", "FICHEIROPDFECF", "Documento") + ".pdf"
            infModLM.nomeFicheiroPDFCOT = ficheiroIni.GetString("LISTAMATERIAIS", "FICHEIROPDFCOT", "Documento") + ".pdf"
            infModLM.nomeFicheiroPDFLST = ficheiroIni.GetString("LISTAMATERIAIS", "FICHEIROPDFLST", "Documento") + ".pdf"

            infModLM.indexCampoInicial = ficheiroIni.GetInteger("LISTAMATERIAIS", "INDEXCAMPOINICIAL", "1")
            infModLM.corInvertida = ficheiroIni.GetString("LISTAMATERIAIS", "CORINVERTIDA", "0") = "1"

            infModLM.EMAILCOT = ficheiroIni.GetString("LISTAMATERIAIS", "EMAILCOT", "")



            infModLM.validaLinhasRemovidas = ficheiroIni.GetString("LISTAMATERIAIS", "VALIDAPECASREMOVIDAS", "1") = "1"


            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = utilizador
            objConfApl.PwdUtilizador = password
            objConfApl.LicVersaoMinima = "9.00"

            EXCELFOLHA = ficheiroIni.GetString("LISTAMATERIAIS", "EXCELFOLHA", "Português$")
            visualizarEmail = ficheiroIni.GetString("LISTAMATERIAIS", "VISUALIZAR_EMAIL", "1")
            apresentaArtigo = ficheiroIni.GetString("LISTAMATERIAIS", "APRESENTAARTIGO", "0")
            apresentaFornecedor = ficheiroIni.GetString("LISTAMATERIAIS", "APRESENTAFORNECEDOR", "0")

            NumeroCasasDecimaisPrecUnit_ = ficheiroIni.GetInteger("LISTAMATERIAIS", "CASASDECIMAISPRECUNIT", 2)
            NumeroCasasDecimaisQuantidade_ = ficheiroIni.GetInteger("LISTAMATERIAIS", "CASASDECIMAISQUANTIDADE", 2)

            verificarLicenciamento(ficheiroIni, seg)

            bso.AbreEmpresaTrabalho(tipoPlat, codEmpresa, utilizador, password, , objConfApl.Instancia)

            PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")

            apresentaFormAPL = ficheiroIni.GetString("ERP", "CONFIGARTIGO", "") = "1"

            aplicarversao001()


        Catch ex As Exception
            MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub verificarLicenciamento(ficheiroIni As IniFile, seg As Seguranca)
        Dim StrnumLicenciamentos As String
        StrnumLicenciamentos = ficheiroIni.GetString("ERP", "NUMPOSTOS", "-1")

        If StrnumLicenciamentos = "-1" Then
            StrnumLicenciamentos = "999999"
        Else
            If StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos) Then
                StrnumLicenciamentos = "0"
            Else
                StrnumLicenciamentos = seg.decifrarStringTripleDES(StrnumLicenciamentos)
            End If
        End If

        If IsNumeric(StrnumLicenciamentos) Then
            numLicenciamentos = CInt(StrnumLicenciamentos)
        Else
            numLicenciamentos = 0
        End If
    End Sub

    ''' <summary>
    ''' Devolve index da folha de Excel
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getIndexExcel() As Integer
        Return indexExcel
    End Function

    ''' <summary>
    ''' Funcao que permite retornar a a folha de Excel
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getExcelFolha() As String
        Return EXCELFOLHA
    End Function


    Public Function getViewFormAPL() As Boolean
        Return apresentaFormAPL
    End Function

    ''' <summary>
    ''' Funcao que permite retornar a entidae por defeito
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getEntidadeDefault() As String
        Return entidadeDefault
    End Function


    Public Function getApresentaColunaArtigo() As Boolean
        Return True '  apresentaArtigo = "1"
    End Function

    Public Function getApresentaColunaFornecedor() As Boolean
        Return True ' apresentaFornecedor = "1"
    End Function


    Public Sub setEmpresaOriginal()
        setEmpresa(codEmpresaBase)
    End Sub
    Public Sub setEmpresa(ByVal empresa_ As String)
        If codEmpresa <> empresa_ And empresa_ <> "" Then
            Try

                bso.FechaEmpresaTrabalho()
                bso.AbreEmpresaTrabalho(tipoPlat, empresa_, utilizador, password)
                codEmpresa = empresa_
                PlataformaPrimavera.FechaPlataformaEmpresa()
                PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "")

            Catch ex As Exception
                MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            End Try
        End If

    End Sub



    ''' <summary>
    ''' Função que permite ir buscar a Informacao do Modulo de Lista de Materiais.
    ''' </summary>
    ''' <remarks></remarks>

    ReadOnly Property InformModLstMaterial() As InfModLstMaterial
        Get
            InformModLstMaterial = infModLM
        End Get
    End Property


    ''' <summary>
    ''' Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Funcionario() As String
        Get
            Funcionario = funcionario_
        End Get
        Set(ByVal value As String)
            funcionario_ = value
        End Set
    End Property

    ''' <summary>
    ''' Nome Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property NomeFuncionario() As String
        Get
            NomeFuncionario = nomefuncionario_
        End Get
        Set(ByVal value As String)
            nomefuncionario_ = value
        End Set
    End Property


    ''' <summary>
    ''' Permite validar se um funcionario é administrador
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property FuncionarioAdm() As Boolean
        Get
            FuncionarioAdm = funcionarioAdm_
        End Get
        Set(ByVal value As Boolean)
            funcionarioAdm_ = value
        End Set
    End Property



    Dim tipoDoc_ As String = ""
    Property TipoDoc() As String
        Get
            TipoDoc = tipoDoc_
        End Get
        Set(ByVal value As String)
            tipoDoc_ = value
        End Set
    End Property


    Dim serieC_ As String = ""
    Property SerieC() As String
        Get
            SerieC = serieC_
        End Get
        Set(ByVal value As String)
            serieC_ = value
        End Set
    End Property


    Dim tipoEntidadeC_ As String = "C"
    Property TipoEntidadeC() As String
        Get
            TipoEntidadeC = tipoEntidadeC_
        End Get
        Set(ByVal value As String)
            tipoEntidadeC_ = value
        End Set
    End Property






    ReadOnly Property DocumentoCompra() As String
        Get
            DocumentoCompra = tipoDoc_
        End Get
    End Property

    ReadOnly Property Empresa() As String
        Get
            Empresa = codEmpresa
        End Get

    End Property

    ReadOnly Property EmpresaDescricao() As String
        Get
            EmpresaDescricao = Trim(bso.Contexto.IDNome)
        End Get

    End Property

    ReadOnly Property EmpresaAberta() As Boolean
        Get
            EmpresaAberta = bso.Contexto.EmpresaAberta
        End Get
    End Property



    Property AplicacaoInicializada() As Boolean
        Get
            AplicacaoInicializada = apliInicializada
        End Get
        Set(ByVal value As Boolean)
            apliInicializada = value
        End Set
    End Property



    Public Sub setLog(ByVal log_ As TextBox)
        log = log_
    End Sub

    Public Sub fechaEmpresa()
        bso.FechaEmpresaTrabalho()
        PlataformaPrimavera.FechaPlataforma()
    End Sub

    Public Shared Function GetInstance() As MotorLM
        If myInstance Is Nothing Then
            myInstance = New MotorLM
        End If
        Return myInstance
    End Function



    Dim NumeroCasasDecimaisPrecUnit_ As Integer = 2
    ReadOnly Property NumeroCasasDecimaisPrecUnit() As Integer
        Get
            NumeroCasasDecimaisPrecUnit = NumeroCasasDecimaisPrecUnit_
        End Get
    End Property



    Dim NumeroCasasDecimaisQuantidade_ As Integer = 2
    ReadOnly Property NumeroCasasDecimaisQuantidade() As Integer
        Get
            NumeroCasasDecimaisQuantidade = NumeroCasasDecimaisQuantidade_
        End Get
    End Property



    ''' <summary>
    ''' Função que permite a execução de um comando 
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function executarComando(ByVal comando As String, Optional mostraMsg As Boolean = True)
        Try
            bso.DSO.Plat.ExecSql.ExecutaXML(comando)
        Catch ex As Exception
            If mostraMsg Then MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

        Return ""
    End Function

    Public Function consulta(ByVal query As String, Optional apresentaMsgErro As Boolean = True) As StdBELista
        Try
            Return bso.Consulta(query)
        Catch ex As Exception
            If apresentaMsgErro Then
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End If
            Return Nothing
        End Try

    End Function

    Public Function consultaValor(ByVal query As String) As String
        Dim lista As StdBELista        '
        lista = bso.Consulta(query)
        If lista.NoFim Then
            Return ""
        Else
            Return lista.Valor(0).ToString
        End If
    End Function



    Public Function convertRecordSetToDataTable(ByVal MyRs As ADODB.Recordset) As DataTable
        'Create and fill the dataset from the recordset and populate grid from Dataset. 
        Dim myDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter()
        Dim myDS As DataSet = New DataSet("MyTable")
        myDA.Fill(myDS, MyRs, "MyTable")
        Return myDS.Tables("MyTable")
    End Function



    ''' <summary>
    ''' Funcao que permite ir buscar os Fornecedores inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaFornecedores(ByVal restricao As String, listaFornecedores As String) As StdBELista
        Try
            Dim lista As StdBELista

            If listaFornecedores = "" Then
                If restricao <> "" Then
                    lista = consulta("Select Fornecedor As Codigo, Nome FROM Fornecedores WHERE FornecedorAnulado=0 And Nome Like '%" + restricao + "%' ORDER BY Nome")
                Else
                    lista = consulta("SELECT Fornecedor as Codigo,Nome FROM Fornecedores WHERE FornecedorAnulado=0 Order By Nome")
                End If
            Else
                lista = consulta("SELECT Fornecedor as Codigo,Nome FROM Fornecedores WHERE Fornecedor in (" + listaFornecedores + ") Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    Public Function NuloToBoolean(ByVal obj) As Boolean
        If IsDBNull(obj) Then
            NuloToBoolean = 0
        Else
            NuloToBoolean = CBool(obj)
        End If

    End Function

    Public Function NuloToDate(ByVal obj As Object, ByVal def As DateTime) As DateTime
        If IsDBNull(obj) Then
            NuloToDate = def
        Else
            If Not IsDate(obj) Then
                NuloToDate = def
            Else
                NuloToDate = obj
            End If
        End If
    End Function

    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                NuloToDouble = 0
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Public Function NuloToString(ByVal obj) As String
        Try


            If IsDBNull(obj) Then
                NuloToString = ""
            Else
                NuloToString = obj.ToString().Trim()
            End If
        Catch ex As Exception

        End Try
    End Function




    Public Function daListaArtigosLinhaOriginal(ByVal idLinha As String, ByVal modulo As String) As DataTable
        Dim lista As StdBELista = Nothing
        Dim dt As DataTable = Nothing

        If modulo = "C" Then
            Dim query As String
            query = "SELECT  10000000 as nlinha,LinhasCompras.*,CDU_ObraN1 as Peca,CDU_ObraN3 as  Dimensoes,CDU_ObraN4 as Material,CDU_ObraN5 as Estado,COP_Obras.Codigo FROM LinhasCompras LEFT JOIN COP_Obras ON LinhasCompras.ObraId=Cop_Obras.ID INNER JOIN CabecCompras on LinhasCompras.idCabecCompras=CabecCompras.id WHERE CabecCompras.TipoDoc='" + infModLM.TIPODOCLM + "' and LinhasCompras.Id='" + idLinha + "'"
            query += " UNION "
            query += "SELECT Numlinha as nlinha,LinhasCompras.*,CDU_ObraN1 as Peca,CDU_ObraN3 as  Dimensoes,CDU_ObraN4 as Material,CDU_ObraN5 as Estado,COP_Obras.Codigo FROM LinhasCompras LEFT JOIN COP_Obras ON LinhasCompras.ObraId=Cop_Obras.ID INNER JOIN CabecCompras on LinhasCompras.idCabecCompras=CabecCompras.id WHERE CabecCompras.TipoDoc='" + infModLM.TIPODOCALT + "' and  LinhasCompras.IdLinhaOrigemCopia='" + idLinha + "'"
            lista = consulta("Select  * FROM (" + query + ") Query ORDER BY nlinha desc")

        End If

        If Not lista Is Nothing Then
            dt = convertRecordSetToDataTable(lista.DataSet)
        End If

        Return dt
    End Function


    Public Function daListaArtigosDocumento(ByVal o As Objeto, ByVal modulo As String) As DataTable
        Dim lista As StdBELista = Nothing
        Dim dt As DataTable = Nothing
        If modulo = "I" Then
            lista = consulta("SELECT APS_GP_Artigos.*,LinhasInternos.Quantidade FROM " + o.BaseDados + "..APS_GP_Artigos INNER JOIN " + o.BaseDados + "..LinhasInternos ON APS_GP_Artigos.Artigo=LinhasInternos.Artigo WHERE LinhasInternos.IdCabecInternos='" + o.Id + "' ORDER BY Numlinha")
        End If

        If modulo = "C" Then
            lista = consulta("SELECT  LC.*,LC.CDU_ObraN1 as Peca,LC.CDU_ObraN3 as  Dimensoes,LC.CDU_ObraN4 as Material,LC.CDU_ObraN5 as Estado,COP_Obras.Codigo,(SELECT Count(*) FROM " + o.BaseDados + "..LinhasCompras LCA INNER JOIN " + o.BaseDados + "..CABECCOMPRAS CCA ON LCA.IdCabecCompras=CCA.ID  WHERE CCA.TipoDoc='" + InformModLstMaterial.TIPODOCALT + "' and  LCA.IdLinhaOrigemCopia=LC.id) as NumAlt  FROM " + o.BaseDados + "..LinhasCompras LC LEFT JOIN COP_Obras On LC.ObraId=Cop_Obras.ID WHERE LC.IdCabecCompras='" + o.Id + "' and ((LC.Artigo<>'" + artInsp1 + "' and LC.Artigo<>'" + artInsp2 + "') or LC.Artigo IS NULL  ) ORDER BY Numlinha")
        End If

        If modulo = "V" Then
            lista = consulta("SELECT  LinhasDoc.*,COP_Obras.Codigo FROM " + o.BaseDados + "..LinhasDoc LEFT JOIN " + o.BaseDados + "..COP_Obras ON LinhasDoc.ObraId=Cop_Obras.ID WHERE LinhasDoc.IdCabecDoc='" + o.Id + "' and LinhasDoc.Artigo<>'" + artInsp1 + "' and LinhasDoc.Artigo<>'" + artInsp2 + "' ORDER BY Numlinha")
        End If

        If Not lista Is Nothing Then
            dt = convertRecordSetToDataTable(lista.DataSet)
        End If

        Return dt
    End Function
    Private Function daFuncionarioEmpresaDestino(func As String, empresaOrig As String, empresaDestino As String) As String

        Dim funcionario As String
        If UCase(empresaOrig) = UCase(empresaDestino) Then
            Return func
        End If
        Dim numContrib As String
        numContrib = consultaValor("SELECT NumContr FROM PRI" + empresaOrig + "..APS_GP_Funcionarios WHERE codigo='" + func + "'")
        If numContrib <> "" Then
            funcionario = consultaValor("SELECT codigo FROM PRI" + empresaDestino + "..APS_GP_Funcionarios WHERE NumContr='" + numContrib + "'")

            If funcionario <> "" Then
                Return funcionario
            Else
                Return func
            End If
        Else
            Return func
        End If

    End Function


    Private Function getDocumentoCompra(id As String) As GcpBEDocumentoCompra
        Dim lista As StdBELista
        lista = consulta("SELECT Filial, TipoDoc,Serie, NumDoc FROM CabecDoc WHERE id='" + id + "'")
        If Not lista.NoFim Then
            Return bso.Comercial.Compras.Edita(lista.Valor("Filial"), lista.Valor("TipoDoc"), lista.Valor("Serie"), lista.Valor("NumDoc"))
        Else
            Return Nothing
        End If
    End Function
    Public Function actualizarDocumentoCompra(ht As Hashtable, mgrid As MotorGrelhas, fornecedor As Fornecedor, ByVal tipodoc As String, ByVal func As String, ByVal projecto As String, observacoes As String, ByRef recordsGrid As Object, datafatura As String, Optional ByVal doc As Objeto = Nothing, Optional botao As Integer = 0, Optional indexTabSelel As Integer = 0, Optional conferido As Boolean = False, Optional reponsavel As String = "", Optional carregouLista As Boolean = False, Optional docRecep As String = "") As String

        Dim docC As GcpBEDocumentoCompra
        Dim linhaC As GcpBELinhaDocumentoCompra
        Dim idProjecto As String
        Dim id As String

        Dim empresaOrig As String
        Dim BDBOrig As String
        Dim empresaAtual As String
        Dim BDAtual As String

        Dim projetoOrig As String

        docC = Nothing

        projetoOrig = projecto

        empresaOrig = codEmpresa
        BDBOrig = "PRI" + empresaOrig

        func = daFuncionarioEmpresaDestino(func, codEmpresa, doc.Empresa)

        setEmpresa(doc.Empresa)

        If doc.Projeto <> "" Then
            projecto = doc.Projeto
        End If

        empresaAtual = codEmpresa
        BDAtual = "PRI" + codEmpresa

        idProjecto = bso.Comercial.Projectos.DaValorAtributo(projecto, "ID")

        Dim data As Date

        data = FormatDateTime(Now, DateFormat.ShortDate)

        If Not doc Is Nothing Then
            If doc.Id <> "" Then
                'id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
                id = doc.Id
                docC = bso.Comercial.Compras.EditaID(id)
                data = docC.DataDoc
            End If
        End If


        If docC Is Nothing Then
            docC = New GcpBEDocumentoCompra
            docC.Tipodoc = tipodoc
            If serieC_ <> "" Then
                docC.Serie = serieC_
            Else
                docC.Serie = bso.Comercial.Series.DaSerieDefeito("C", tipodoc, data)
            End If

            docC.Entidade = fornecedor.Fornecedor
            docC.TipoEntidade = tipoEntidadeC_

            bso.Comercial.Compras.PreencheDadosRelacionados(docC)

            If docC.CamposUtil.Existe("CDU_criadopor") Then
                docC.CamposUtil("CDU_criadopor").Valor = func
            End If

            If reponsavel <> "" Then
                docC.CamposUtil("CDU_CabVar1").Valor = reponsavel
            Else
                docC.CamposUtil("CDU_CabVar1").Valor = func
            End If
        End If

        docC.Utilizador = func
        docC.CamposUtil("CDU_CabVar2").Valor = datafatura
        docC.CamposUtil("CDU_Conferido").Valor = conferido

        docC.Utilizador = func
        docC.DataDoc = CDate(data)
        docC.DataVenc = docC.DataDoc
        docC.DataIntroducao = docC.DataDoc


        If docC.Tipodoc = infModLM.TIPODOCLM And infModLM.RefDocOrigLM Then
            docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
        End If

        If docC.Tipodoc = infModLM.TIPODOCCOT And infModLM.RefDocOrigCOT Then
            docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
        End If

        If docC.Tipodoc = infModLM.TIPODOCECF And infModLM.RefDocOrigECF Then
            docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
        End If

        If docC.Tipodoc = infModLM.TIPODOCLM And carregouLista Then
            If docC.CamposUtil.Existe("CDU_DataListaMat") Then
                docC.CamposUtil("CDU_DataListaMat").Valor = Now
            End If
            'docC.DataIntroducao = Now
        End If

        If docC.Tipodoc <> infModLM.TIPODOCLM Then
            docC.Observacoes = observacoes
        End If

        docC.IDObra = idProjecto

        Dim documentoT As String
        documentoT = consultaValor("SELECT BensCirculacao FROM DocumentosCompra WHERE Documento='" + tipodoc + "'")
        documentoT = LCase(documentoT)
        Dim index As Integer
        index = 1

        'Dim dt As Date
        'dt = Now

        If documentoT <> "true" Or Not docC.EmModoEdicao Then

            '   docC.Linhas.RemoveTodos()
            Dim artigo As String
            Dim quantidade As Double
            Dim row As ReportRecord
            Dim unidade As String
            '   Dim projecto As String
            Dim precunit As Double
            Dim desconto As Double
            Dim descricao As String
            Dim dtrow As DataRow

            Dim valor As Object
            Dim peca As String
            Dim dimensoes As String
            Dim material As String
            Dim artigoBase As String
            Dim indexlinha As Integer
            Dim dataEntrega As String
            Dim codIva As String
            Dim temperamento As String
            Dim campo7 As String
            Dim campo8 As String
            Dim campo9 As String
            Dim campo10 As String

            Dim htActivas As Hashtable
            Dim htEliminadas As Hashtable
            htActivas = New Hashtable
            htEliminadas = New Hashtable

            separarLinhasDocumento(docC, recordsGrid, htActivas, htEliminadas)

            Dim nomeFuncionario As String
            nomeFuncionario = consultaValor("SELECT Nome FROM APS_GP_Funcionarios WHERE Codigo='" + func + "'")

            ' docC.Linhas.RemoveTodos()
            ' docC.Linhas.Removidas.RemoveTodos()

            'For Each idLinha In htEliminadas.Keys

            '    docC.Linhas.Removidas.Insere(idLinha)
            'Next

            Dim numLinha As Integer
            For Each idLinha In htEliminadas.Keys

                For numLinha = docC.Linhas.NumItens To 1 Step -1
                    If docC.Linhas(numLinha).IdLinha = idLinha Then
                        docC.Linhas.Remove(numLinha)
                    End If
                Next
            Next


            For i = 0 To recordsGrid.Count - 1


                row = recordsGrid(i)
                dtrow = row.Tag

                ' linhaC = buscarLinha(dtrow("Id").ToString(), docC)
                linhaC = buscarLinha(dtrow("Id").ToString(), htActivas)

                valor = buscarValorAtributoLinha(mgrid, row, "ARTIGO")
                artigo = buscarArtigo(valor, infModLM.ARTIGO)

                descricao = buscarValorAtributoLinha(mgrid, row, "DESCRICAO")

                quantidade = buscarValorAtributoLinha(mgrid, row, "QUANTIDADE")
                unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
                peca = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN1")
                dimensoes = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN3")
                material = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN4")
                temperamento = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN6")
                campo7 = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN7")
                campo8 = buscarValorAtributoLinha(mgrid, row, "FORNECEDOR")

                campo9 = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN9")
                campo10 = buscarValorAtributoLinha(mgrid, row, "CDU_OBRAN10")

                dataEntrega = buscarValorAtributoLinha(mgrid, row, "DATAENTREGA", docC.Tipodoc = infModLM.TIPODOCLM)
                indexlinha = mgrid.buscarIndexCampo("ARTIGO")

                If indexlinha <> -1 Then
                    artigoBase = NuloToString(row(indexlinha).Tag)
                Else
                    artigoBase = infModLM.ARTIGO
                End If

                precunit = NuloToDouble(buscarValorAtributoLinha(mgrid, row, "PRECUNIT"))
                desconto = NuloToDouble(buscarValorAtributoLinha(mgrid, row, "DESCONTO1"))

                'permite actualizar o sistema de efivalencias por forma a ie buscar daqui para a frente sempre este artigo
                actualizarFichaEquivalenciasArtigos(peca, material, descricao, dimensoes, artigo, artigoBase)


                If linhaC Is Nothing Then

                    If artigo = "" Then
                        bso.Comercial.Compras.AdicionaLinhaEspecial(docC, compTipoLinhaEspecial.compLinha_Comentario, 0)
                    Else
                        bso.Comercial.Compras.AdicionaLinha(docC, artigo, quantidade)
                    End If


                    linhaC = docC.Linhas(docC.Linhas.NumItens)

                    If botao = indexTabSelel Then
                        linhaC.IdLinha = "{" + dtrow("Id").ToString() + "}"
                    End If

                    If botao = 0 Then
                        'linhaC.IdLinha = "{" + dtrow("Id").ToString() + "}"
                        dtrow("Id") = Replace(Replace(linhaC.IdLinha, "{", ""), "}", "")
                    End If
                Else
                    ' docC.Linhas.Insere(linhaC)
                    If docC.Tipodoc = infModLM.TIPODOCLM Then
                        Dim funcLinha As String = ""
                        funcLinha = func
                        If linhaC.CamposUtil.Existe("CDU_FuncAlteracao") Then
                            If NuloToString(linhaC.CamposUtil("CDU_FuncAlteracao").Valor) <> "" Then
                                funcLinha = linhaC.CamposUtil("CDU_FuncAlteracao").Valor
                            End If
                        End If
                        actualizarLinhaAlteracoes(funcLinha, fornecedor, projecto, idProjecto, linhaC, peca, material, descricao, dimensoes, artigo, precunit, quantidade, temperamento)
                    End If
                End If


                linhaC.Artigo = artigo
                linhaC.Descricao = descricao
                If linhaC.Artigo <> "" Then

                    Dim lista As StdBELista
                    lista = consulta("SELECT TipoLinha, Iva.Iva,Iva.Taxa  from TiposArtigo inner join Artigo on TiposArtigo.TipoArtigo=Artigo.TipoArtigo inner join IVA on Artigo.iva=Iva.iva where Artigo.Artigo='" + artigo + "'")

                    If lista.NumLinhas > 0 Then
                        'linhaC.TipoLinha = consultaValor("SELECT TipoLinha from TiposArtigo inner join Artigo on TiposArtigo.TipoArtigo=Artigo.TipoArtigo where Artigo.Artigo='" + artigo + "'")
                        'linhaC.CodIva = consultaValor("SELECT IVA FROM Artigo WHERE Artigo='" + artigo + "'")
                        'linhaC.TaxaIva = consultaValor("SELECT Taxa FROM Iva WHERE Iva='" + linhaC.CodIva + "'")
                        linhaC.TipoLinha = lista.Valor("TipoLinha")
                        linhaC.CodIva = lista.Valor("Iva")
                        linhaC.TaxaIva = lista.Valor("Taxa")
                    End If


                    linhaC.Unidade = unidade
                    linhaC.Quantidade = quantidade
                    linhaC.PrecUnit = precunit
                    linhaC.Desconto1 = desconto
                    linhaC.CamposUtil("CDU_ObraN1") = peca
                    linhaC.CamposUtil("CDU_ObraN3") = dimensoes
                    linhaC.CamposUtil("CDU_ObraN4") = material

                    linhaC.IDObra = idProjecto  'bso.Comercial.Projectos.DaValorAtributo(projecto, "ID")

                    If linhaC.CamposUtil.Existe("CDU_ObraN6") Then
                        linhaC.CamposUtil("CDU_ObraN6") = temperamento
                    End If

                    If linhaC.CamposUtil.Existe("CDU_ObraN7") Then
                        linhaC.CamposUtil("CDU_ObraN7") = campo7
                    End If

                    If linhaC.CamposUtil.Existe("CDU_ObraN8") Then
                        linhaC.CamposUtil("CDU_ObraN8") = campo8
                    End If

                    If linhaC.CamposUtil.Existe("CDU_ObraN9") Then
                        linhaC.CamposUtil("CDU_ObraN9") = campo9
                    End If

                    If linhaC.CamposUtil.Existe("CDU_ObraN10") Then
                        linhaC.CamposUtil("CDU_ObraN10") = campo10
                    End If


                    If linhaC.CamposUtil.Existe("CDU_FuncAlteracao") Then
                        linhaC.CamposUtil("CDU_FuncAlteracao") = nomeFuncionario
                    End If


                    If IsDate(dataEntrega) Then
                        linhaC.DataEntrega = dataEntrega
                    End If

                    'If docC.Tipodoc = infModLM.TIPODOCLM And IsDate(linhaC.DataEntrega) Then
                    '    linhaC.DataEntrega = Now.ToString
                    'End If

                    Dim campo As StdBECampo

                    For Each campo In linhaC.CamposUtil
                        Dim valorObj As Object
                        'If LCase(campo.Nome) = "cdu_notas" Then
                        '    campo.Valor = campo.Valor
                        'End If
                        If campo.Tipo = EnumTipoCampo.tcBit Then

                        End If
                        Dim indexCampo As Integer
                        indexCampo = mgrid.buscarIndexCampo(UCase(campo.Nome))
                        If indexCampo <> -1 And InStr(LCase(campo.Nome), "cdu_obra") = 0 And Not mgrid.ColunasSistema(UCase(campo.Nome)) Then
                            If campo.Tipo = EnumTipoCampo.tcBit Then
                                campo.Valor = row(indexCampo).Checked
                            Else
                                valorObj = buscarValorAtributoLinha(mgrid, row, campo.Nome)
                                If Not valorObj Is Nothing Then
                                    campo.Valor = valorObj
                                End If
                            End If

                        End If
                    Next



                    'If IsDate(dataEntrega) Then
                    '    linhaC.DataEntrega = CDate(dataEntrega)
                    '    dtrow("CDU_DataRecepcao") = dataEntrega
                    'End If

                    If NuloToString(dtrow("Id")).Trim() = "" Then
                        dtrow("Id") = linhaC.IdLinha
                    End If

                    If botao <> 0 And linhaC.TipoLinha <> "60" Then
                        ' linhaC.CamposUtil("CDU_ObraN5") = IIf(botao = 1, "C", "E")
                        If indexTabSelel <> 2 Then
                            If row(mgrid.buscarIndexCampo("CHECKBOX")).Checked Then
                                actualizarEstadoLinha(BDAtual, BDBOrig, projetoOrig, ht, "C", dtrow("Id").ToString, IIf(botao = 1, "C", "E"))
                            End If
                        Else

                            Dim quantFalta As Double
                            Dim quantEnt As Double

                            If row(mgrid.buscarIndexCampo("CHECKBOX")).Checked Then
                                quantFalta = buscarValorAtributoLinha(mgrid, row, "QUANTFALTA")
                            Else
                                quantFalta = 0
                            End If

                            'buscarValorAtributoLinha(mgrid, row, "QUANTENT")
                            quantEnt = mgrid.buscarQuantidadeEntregue(BDAtual, linhaC.IdLinha)


                            'é uma gravação de encomenda
                            If linhaTotalmenteSatisfeita(buscarValorAtributoLinha(mgrid, row, "QUANTIDADE"), quantFalta, quantEnt) Then
                                actualizarEstadoLinha(BDAtual, BDBOrig, projetoOrig, ht, "C", dtrow("Id").ToString, "V")
                                linhaC.CamposUtil("CDU_ObraN5") = "V"
                                linhaC.CamposUtil("CDU_DataRecepcao") = buscarValorAtributoLinha(mgrid, row, "CDU_DataRecepcao")

                                dtrow("CDU_DataRecepcao") = linhaC.CamposUtil("CDU_DataRecepcao").Valor
                            Else
                                actualizarEstadoLinha(BDAtual, BDBOrig, projetoOrig, ht, "C", dtrow("Id").ToString, "E")
                                linhaC.CamposUtil("CDU_ObraN5") = ""
                                linhaC.CamposUtil("CDU_DataRecepcao") = DBNull.Value
                                dtrow("CDU_DataRecepcao") = DBNull.Value
                            End If
                        End If
                        If Not docC.EmModoEdicao Then
                            linhaC.IdLinhaOrigemCopia = dtrow("Id").ToString
                            linhaC.ModuloOrigemCopia = "C"
                        End If
                    Else
                        linhaC.CamposUtil("CDU_ObraN5") = dtrow("CDU_ObraN5")
                    End If

                End If


                index = index + 1
            Next

        End If

        Dim avisos As String
        avisos = ""
        Try
            If docC.Linhas.NumItens > 0 Or Not doc Is Nothing Then

                bso.Comercial.Compras.Actualiza(docC, avisos)

                If indexTabSelel = 2 Then actualizarEntradasStocks(docC, mgrid, recordsGrid, idProjecto, docRecep)

                setEmpresa(empresaOrig)

                atualizarLigacaoEntreEmpresas(docC.Tipodoc, empresaOrig, projetoOrig, empresaAtual, projecto, docC.Id)


                '  MsgBox(DateDiff(DateInterval.Second, dt, Now).ToString)
                Return docC.Id
            End If
        Catch ex As Exception
            setEmpresa(empresaOrig)
            MsgBox(ex.Message + vbCrLf + avisos, MsgBoxStyle.Critical)
            Return ""
        End Try
        setEmpresa(empresaOrig)

        ' MsgBox(DateDiff(DateInterval.Second, dt, Now).ToString)
        Return ""
    End Function


    Private Sub atualizarLigacaoEntreEmpresas(tipodoc As String, empresaActual As String, projetoActual As String, empresaDestino As String, projetoDestino As String, idDoc As String)
        Dim comando As String

        If empresaActual = empresaDestino Or empresaDestino = "" Then
            Return
        End If

        Dim count As String

        count = consultaValor("SELECT Count(*) FROM TDU_GP_LigacaoProjetos WHERE CDU_idDoc='" + idDoc + "' ")

        If count = "0" Then
            comando = "INSERT INTO [dbo].[TDU_GP_LigacaoProjetos]
               ([CDU_id]
               ,[CDU_ProjetoOrigem]
               ,[CDU_TipoDoc]
               ,[CDU_BDDestino]
               ,[CDU_ProjetoDestino]
               ,[CDU_idDoc])
            VALUES
               (newid()
               ,'" + projetoActual + "'
               ,'" + tipodoc + "'
               ,'" + empresaDestino + "'
               ,'" + projetoDestino + "'
               ,'" + idDoc + "')"

            executarComando(comando)
        End If


    End Sub
    Private Sub actualizarEntradasStocks(doc As GcpBEDocumentoCompra, mgrid As MotorGrelhas, recordsGrid As Object, idProjecto As String, docRecp As String)


        If doc.Tipodoc <> infModLM.TIPODOCECF Then Exit Sub

        Dim row As ReportRecord
        Dim dtrow As DataRow
        Dim artigo As String
        Dim descricao As String
        Dim quantidade As Double
        Dim precunit As Double
        Dim unidade As String
        Dim linha As GcpBELinhaDocumentoCompra
        Dim movStock As String
        Dim armazem As String
        Dim localizacao As String

        Dim docStk As GcpBEDocumentoCompra
        docStk = buscarDocumentoStock(doc.Id)

        If docStk Is Nothing Then
            docStk = New GcpBEDocumentoCompra
            docStk.Tipodoc = infModLM.TIPODOCSTK
            docStk.Serie = doc.Serie
            docStk.DataDoc = doc.DataDoc

            docStk.Entidade = doc.Entidade
            docStk.TipoEntidade = doc.TipoEntidade
            bso.Comercial.Compras.PreencheDadosRelacionados(docStk, PreencheRelacaoCompras.compDadosTodos)
            docStk.DataIntroducao = doc.DataDoc
        End If

        docStk.NumDocExterno = doc.Filial + "/" + doc.Tipodoc + "/" + doc.Serie + "/" + CStr(doc.NumDoc)
        '   docStk.Linhas.RemoveTodos()

        For i = 0 To recordsGrid.Count - 1
            row = recordsGrid(i)
            dtrow = row.Tag
            artigo = buscarValorAtributoLinha(mgrid, row, "ARTIGO") 'buscarArtigo(row(ENCCOLUMN_ARTIGO).Value, infModLM.ARTIGO)
            descricao = buscarValorAtributoLinha(mgrid, row, "DESCRICAO")
            quantidade = buscarValorAtributoLinha(mgrid, row, "QUANTFALTA")
            precunit = buscarValorAtributoLinha(mgrid, row, "PRECUNIT")
            unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
            movStock = bso.Comercial.Artigos.DaValorAtributo(artigo, "MovStock")
            armazem = bso.Comercial.Artigos.DaValorAtributo(artigo, "ArmazemSugestao")
            localizacao = bso.Comercial.Artigos.DaValorAtributo(artigo, "LocalizacaoSugestao")
            If artigo <> "" And row(mgrid.buscarIndexCampo("CHECKBOX")).Checked And quantidade <> 0 Then
                bso.Comercial.Compras.AdicionaLinha(docStk, artigo, quantidade)
                linha = docStk.Linhas(docStk.Linhas.NumItens)
                linha.Descricao = descricao
                linha.Armazem = armazem
                linha.Localizacao = localizacao
                linha.PrecUnit = precunit
                linha.IdLinhaOrigemCopia = dtrow("ID").ToString
                linha.ModuloOrigemCopia = "C"
                linha.DataEntrega = buscarValorAtributoLinha(mgrid, row, "DATAENTREGA")
                linha.CamposUtil("CDU_DataRecepcao").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_DataRecepcao")
                linha.DataStock = linha.CamposUtil("CDU_DataRecepcao").Valor
                linha.CamposUtil("CDU_ObraN1").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_ObraN1")
                linha.CamposUtil("CDU_ObraN3").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_ObraN3")
                linha.CamposUtil("CDU_ObraN4").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_ObraN4")
                linha.CamposUtil("CDU_ObraN8").Valor = consultaValor("SELECT NomeAbreviado as Nome FROM APS_GP_Funcionarios WHERE Codigo='" + doc.Utilizador + "'") 'doc.Utilizador

                If linha.CamposUtil.Existe("CDU_DocRecp") Then
                    linha.CamposUtil("CDU_DocRecp").Valor = docRecp
                End If


                linha.IDObra = idProjecto
                ' linha.CamposUtil("CDU_ObraN6").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_ObraN6")
                ' linha.CamposUtil("CDU_ObraN7").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_ObraN7")
                'linha.CamposUtil("CDU_ObraN8").Valor = buscarValorAtributoLinha(mgrid, row, "CDU_ObraN8")
            End If
        Next

        docStk.IdDocOrigem = doc.Id
        docStk.ModuloOrigem = "C"
        ' docStk.CamposUtil("CDU_IDDocOrigem") = doc.ID
        If docStk.Linhas.NumItens > 0 Then
            bso.Comercial.Compras.Actualiza(docStk)
            'funcao que permite gerar o ficheiro CSV
            If infModLM.CSVgerarFicheiro Then
                exportarFicheiroCSV(docStk)
            End If

        End If

    End Sub

    ''' <summary>
    ''' Função que permite ir 
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Private Function buscarDocumentoStock(id As String) As GcpBEDocumentoCompra
        Dim lista As StdBELista

        lista = consulta("SELECT * FROM CabecCompras WHERE IdDocOrigem='" + id + "'")

        If Not lista.NoFim Then
            If lista.NumLinhas > 0 Then
                Return bso.Comercial.Compras.Edita(lista.Valor("Filial"), lista.Valor("TipoDoc"), lista.Valor("Serie"), lista.Valor("NumDoc"))
            End If
        End If
        Return Nothing
    End Function

    Private Function buscarValorAtributoLinha(mgrid As MotorGrelhas, row As ReportRecord, campo As String, Optional dataValida As Boolean = False) As Object
        Dim index As Integer
        campo = UCase(campo)
        index = mgrid.buscarIndexCampo(campo)
        If index <> -1 Then
            Return mgrid.formatarValorCampo(campo, row(index).Value, False, dataValida)
        Else
            Return ""
        End If
    End Function
    ''' <summary>
    ''' Permite ir buscar o artigo a ser utilizado na criação do documento
    ''' </summary>
    ''' <param name="artigo"></param>
    ''' <param name="artigoDefault"></param>
    ''' <returns></returns>
    Private Function buscarArtigo(artigo As String, artigoDefault As String)

        If artigo = "" Then
            Return ""
        End If

        If artigo Is Nothing Then
            Return artigoDefault
        End If
        If artigo.Trim() = "" Or artigo = artigoDefault Then
            Return artigoDefault
        End If

        If bso.Comercial.Artigos.Existe(artigo) Then
            Return artigo
        Else
            Return artigoDefault
        End If
    End Function

    Private Function buscarLinha(idLinha As String, ht As Hashtable) As GcpBELinhaDocumentoCompra

        idLinha = UCase(idLinha)
        idLinha = "{" + idLinha + "}"
        '  Dim linha As GcpBELinhaDocumentoCompra
        Return ht(idLinha)
        'Dim i As Integer
        'For i = 1 To doc.Linhas.NumItens
        '    If doc.Linhas(i).IdLinha = UCase("{" + idLinha + "}") Then
        '        Return doc.Linhas(i)
        '    End If
        'Next
        'Return Nothing
    End Function

    Private Sub separarLinhasDocumento(ByRef doc As GcpBEDocumentoCompra, reportRecord As Object, ByRef htActivas As Hashtable, ByRef htRemovidas As Hashtable)


        Dim i As Integer
        Dim j As Integer
        Dim existe As Boolean
        Dim row As DataRow
        For i = doc.Linhas.NumItens To 1 Step -1
            existe = False
            For j = reportRecord.count - 1 To 0 Step -1
                row = reportRecord(j).tag
                If doc.Linhas(i).IdLinha = UCase("{" + row("Id").ToString() + "}") Then
                    existe = True
                    Exit For
                End If
            Next
            If Not existe Then
                htRemovidas.Add(doc.Linhas(i).IdLinha, doc.Linhas(i))
            Else
                htActivas.Add(doc.Linhas(i).IdLinha, doc.Linhas(i))
            End If
        Next
    End Sub


    'Public Function VerficarFuncionarioAdm(ByVal funcionario As String) As Boolean
    '    Dim lista As StdBELista
    '    Try
    '        lista = bso.Consulta("SELECT ISNULL(CDU_Administrador,0) FROM APS_GP_Funcionarios WHERE Codigo='" + funcionario + "'")
    '        If lista.NoFim Then
    '            Return False
    '        Else
    '            Return CBool(lista.Valor(0))
    '        End If
    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function




    ''' <summary>
    ''' Função que permite verificar se um projecto existe
    ''' </summary>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeProjecto(ByVal projecto As String) As Boolean
        Return bso.Comercial.Projectos.Existe(projecto)
    End Function

    Public Sub enviarEmailDocumento(ByVal documento As Objeto, ByVal modulo As String, botao As Integer, Optional destino As String = "")
        Cursor.Current = Cursors.WaitCursor
        If modulo = "C" Then

            setEmpresa(documento.Empresa)

            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(documento.Id)
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, doc.Moeda, 2)
            Dim str As String
            Dim mapa As Mapa
            mapa = New Mapa
            Dim gcpSerie As GcpBESerie
            Dim tabCompras As GcpBETabCompra
            Dim tempFolder As String
            'tempFolder = System.IO.Path.GetTempPath + "GCPMailTemp\Document.pdf"
            tempFolder = System.IO.Path.GetTempPath + "GCPMailTemp\" + formatarNomeFicheiro(botao, doc)
            If Not Directory.Exists(System.IO.Path.GetTempPath + "GCPMailTemp") Then Directory.CreateDirectory(System.IO.Path.GetTempPath + "GCPMailTemp")
            tabCompras = bso.Comercial.TabCompras.Edita(doc.Tipodoc)
            gcpSerie = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
            '   mapa.iniciarComponente(gcpSerie.Config, codEmpresa, objConfApl.Instancia, utilizador, password, PlataformaPrimavera)
            mapa.iniciarComponente(buscarMapaDefault(doc.Tipodoc, gcpSerie.Config), codEmpresa, objConfApl.Instancia, utilizador, password, PlataformaPrimavera)
            mapa.Inicializar()
            str = "{CabecCompras.Filial}='" & doc.Filial & "' And {CabecCompras.Serie}='" & doc.Serie & "' And {CabecCompras.TipoDoc}='" & doc.Tipodoc & "' and {CabecCompras.NumDoc}=" & CStr(doc.NumDoc)
            mapa.selectionFormula(str)
            mapa.setFormulaFieldsByName("DadosEmpresa", rp.getFormula())

            Dim certificado As String
            certificado = bso.Comercial.Compras.DevolveTextoAssinaturaDocID(doc.Id)
            mapa.setFormulaFieldsByName("NumVia", "'Original'")
            mapa.setFormulaFieldsByName("InicializaParametros", rp.getFormulaIniciarParametros(doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado)))
            mapa.exportarDiscoPDF(tempFolder)
            PlataformaPrimavera.Mail.Inicializa()

            Dim emailDestino As String
            Dim profile As String
            profile = ""
            emailDestino = ""
            '  If PlataformaPrimavera.PrefUtilStd.EmailMAPI Then
            profile = PlataformaPrimavera.PrefUtilStd.EmailMAPIProfile
            ' Else
            'PlataformaPrimavera.Mail.SMTPServer = PlataformaPrimavera.PrefUtilStd.EmailServSMTP
            'PlataformaPrimavera.Mail.EnderecoLocal = PlataformaPrimavera.PrefUtilStd.EmailEndereco
            'End If

            If tabCompras.EmailFixo Then
                emailDestino = tabCompras.EmailTo
            Else
                If tabCompras.EmailTo <> "" Then
                    Dim listaEmails As StdBELista
                    listaEmails = consulta("Select Email FROM LinhasContactoEntidades WHERE TipoEntidade='" + doc.TipoEntidade + "' and Entidade='" + doc.Entidade + "' and TipoContacto='" + tabCompras.EmailTo + "'")
                    While Not listaEmails.NoFim
                        If emailDestino <> "" Then emailDestino = emailDestino + ";"
                        emailDestino = emailDestino + listaEmails.Valor("Email")
                        listaEmails.Seguinte()
                    End While
                    ' emailDestino = consultaValor("Select Email FROM LinhasContactoEntidades WHERE TipoEntidade='" + doc.TipoEntidade + "' and Entidade='" + doc.Entidade + "' and TipoContacto='" + tabCompras.EmailTo + "'")
                End If
            End If


            Try
                Dim previsualizar As Boolean
                If visualizarEmail = "1" Then
                    previsualizar = True
                Else
                    previsualizar = tabCompras.EmailVisualizar
                End If

                If destino <> "" Then
                    emailDestino = destino
                    previsualizar = False
                End If

                PlataformaPrimavera.Mail.EnviaMailEx(emailDestino, tabCompras.EmailCC, tabCompras.EmailBCC, doc.Tipodoc + " " + CStr(doc.NumDoc) + "/" + doc.Serie, tabCompras.EMailTexto, tempFolder, previsualizar)

            Catch ex As Exception
                Cursor.Current = Cursors.Default
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try

        End If

        setEmpresaOriginal()

        Cursor.Current = Cursors.Default


    End Sub

    Private Function formatarNomeFicheiro(botao As Integer, doc As GcpBEDocumentoCompra)
        Dim formato As String
        Select Case botao
            Case 0 : formato = infModLM.nomeFicheiroPDFLST
            Case 1 : formato = infModLM.nomeFicheiroPDFCOT
            Case 2 : formato = infModLM.nomeFicheiroPDFECF
            Case Else : formato = "Document.pdf"

        End Select

        formato = Replace(formato, "[TIPODOC]", doc.Tipodoc)
        formato = Replace(formato, "[NUMDOC]", doc.NumDoc)
        formato = Replace(formato, "[ENTIDADE]", doc.Entidade)
        formato = Replace(formato, "[NOME]", Left(doc.Nome, 10))
        formato = Replace(formato, "/", "_")
        formato = Replace(formato, "\", "_")
        formato = Replace(formato, ":", "_")
        formato = Replace(formato, "*", "_")
        formato = Replace(formato, "<", " _")
        formato = Replace(formato, ">", "_")
        formato = Replace(formato, "|", "_")

        Return formato
    End Function
    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, ByVal projecto As String) As String
        existeDocumentoCompra = ""
        Dim idProj As String
        Dim lista As StdBELista
        If documentoCompra <> "" And projecto <> "" Then
            idProj = bso.Comercial.Projectos.DaValorAtributo(projecto, "Id")
            If Not idProj Is Nothing Then
                lista = bso.Consulta("SELECT serie,numdoc from CabecCompras where Tipodoc='" + documentoCompra + "' and ObraID='" + idProj + "'")
                If Not lista.NoFim Then
                    existeDocumentoCompra = existeDocumentoCompra(documentoCompra, lista.Valor("Serie"), lista.Valor("Numdoc"), projecto)
                End If
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentosCompra(ByVal documentoCompra As String, ByVal projecto As String, verTodos As Boolean) As StdBELista

        Dim lista As StdBELista
        Dim empresaDestino As String

        If documentoCompra <> "" And projecto <> "" Then
            ' idProj = bso.Comercial.Projectos.DaValorAtributo(projecto, "Id")
            empresaDestino = "PRI" + codEmpresa

            Dim restricaoAbertoFechado As String = ""
            If Not verTodos Then
                '   restricaoAbertoFechado = " AND (CabecComprasStatus.Anulado=0 and CabecComprasStatus.Fechado=0)"
                restricaoAbertoFechado = " AND (CCS.Anulado=0)"
            End If

            Dim query As String
            ' query = "SELECT CabecCompras.ID,CabecCompras.Entidade,CabecCompras.Nome,CabecCompras.DataDoc,CabecCompras.Serie, CabecCompras.Numdoc,CabecComprasStatus.Anulado as StatusAnulado, CabecComprasStatus.Fechado as StatusFechado, (Select Min(DataEntrega) FROM LinhasCompras  WHERE  LinhasCompras.idCabecCompras=CabecCompras.id and not DataEntrega is null )  as DataEntrega from CabecCompras inner join CabecComprasStatus ON CabecCompras.ID=CabecComprasStatus.IDCabecCompras   where CabecCompras.Tipodoc='" + documentoCompra + "' and CabecCompras.ObraID='" + idProj + "' " + restricaoAbertoFechado + " order by DataDoc desc"
            query = "SELECT '" + codEmpresa + "' as BD,'" + projecto + "' as Projeto, C.ID,C.Entidade,C.Nome,C.DataDoc,C.Serie, C.Numdoc,CCS.Anulado as StatusAnulado, CCS.Fechado as StatusFechado, (Select Min(DataEntrega) FROM " + empresaDestino + "..LinhasCompras as LC  WHERE  LC.idCabecCompras=C.id and not DataEntrega is null )  as DataEntrega from " + empresaDestino + "..CabecCompras as C inner join " + empresaDestino + "..CabecComprasStatus as CCS ON C.ID=CCS.IDCabecCompras Inner join  " + empresaDestino + "..COP_Obras P ON  C.ObraID=P.Id and P.Codigo='" + projecto + "'   where C.Tipodoc='" + documentoCompra + "' " + restricaoAbertoFechado + " order by DataDoc desc"

            lista = bso.Consulta(query)
            Return lista

        End If
        Return Nothing
    End Function



    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentosCompraGrupo(ByVal documentoCompra As String, ByVal projecto As String, verTodos As Boolean) As StdBELista

        Dim lista As StdBELista
        Dim empresaDestino As String
        Dim projetoDestino As String
        If documentoCompra <> "" And projecto <> "" Then
            Dim query As String

            query = "SELECT  DISTINCT CDU_BDDestino,CDU_ProjetoDestino FROM TDU_GP_LigacaoProjetos WHERE CDU_ProjetoOrigem='" + projecto + "' and CDU_TipoDoc='" + documentoCompra + "'"
            lista = bso.Consulta(query)

            query = ""

            While Not lista.NoFim

                empresaDestino = "PRI" + lista.Valor("CDU_BDDestino")
                projetoDestino = lista.Valor("CDU_ProjetoDestino")


                Dim restricaoAbertoFechado As String = ""
                If Not verTodos Then
                    restricaoAbertoFechado = " AND (CCS.Anulado=0)"
                End If

                If query <> "" Then
                    query += " UNION "
                End If
                query += "SELECT  '" + lista.Valor("CDU_BDDestino") + "' as BD,'" + lista.Valor("CDU_ProjetoDestino") + "' as Projeto,C.ID,C.Entidade,C.Nome,C.DataDoc,C.Serie, C.Numdoc,CCS.Anulado as StatusAnulado, CCS.Fechado as StatusFechado, (Select Min(DataEntrega) FROM " + empresaDestino + "..LinhasCompras as LC  WHERE  LC.idCabecCompras=C.id and not DataEntrega is null )  as DataEntrega from " + empresaDestino + "..CabecCompras as C inner join " + empresaDestino + "..CabecComprasStatus as CCS ON C.ID=CCS.IDCabecCompras Inner join  " + empresaDestino + "..COP_Obras P ON  C.ObraID=P.Id and P.Codigo='" + projetoDestino + "'   where C.Tipodoc='" + documentoCompra + "' " + restricaoAbertoFechado

                lista.Seguinte()
            End While

            If query <> "" Then
                lista = bso.Consulta(query + " order by DataDoc Desc")
                Return lista
            Else
                Return Nothing
            End If

        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, serie As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        existeDocumentoCompra = ""
        Dim idProj As String
        If documentoCompra <> "" Then
            If bso.Comercial.Compras.Existe("000", documentoCompra, serie, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serie, "000", "Id")
                idProj = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serie, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        existeDocumentoCompra = ""
        Dim idProj As String
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            End If
            If bso.Comercial.Compras.Existe("000", documentoCompra, serieC_, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "Id")
                idProj = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function


    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Venda existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoVenda(ByVal documentoVenda As String, serie As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        existeDocumentoVenda = ""
        Dim idProj As String
        If documentoVenda <> "" Then
            If bso.Comercial.Vendas.Existe("000", DocumentoCompra, serie, numdoc) Then
                existeDocumentoVenda = bso.Comercial.Vendas.DaValorAtributo(DocumentoCompra, numdoc, serie, "000", "Id")
                idProj = bso.Comercial.Vendas.DaValorAtributo(DocumentoCompra, numdoc, serie, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoVenda(ByVal documentoVenda As String, ByVal projecto As String) As String
        existeDocumentoVenda = ""
        Dim idProj As String
        Dim lista As StdBELista
        If documentoVenda <> "" Then
            idProj = bso.Comercial.Projectos.DaValorAtributo(projecto, "Id")
            lista = bso.Consulta("SELECT * from CabeDoc where Tipodoc='" + documentoVenda + "' and ObraID='" + idProj + "'")

            If Not lista.NoFim Then
                existeDocumentoVenda = existeDocumentoVenda(DocumentoCompra, lista.Valor("Serie"), lista.Valor("Numdoc"), projecto)
            End If
        End If
    End Function

    Public Sub imprimirDocumento(ByVal documento As Objeto, ByVal modulo As String, ByVal owner As System.Windows.Forms.IWin32Window, Optional numVias As Integer = -1)
        setEmpresa(documento.Empresa)

        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(documento.Id)
            imprimir(doc, owner, numVias)
            doc = Nothing
        End If
        If modulo = "V" Then
            Dim doc As GcpBEDocumentoVenda
            doc = bso.Comercial.Vendas.EditaID(documento.Id)
            imprimir(doc, owner, numVias)
            doc = Nothing
        End If

        setEmpresaOriginal()
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoVenda, ByVal owner As System.Windows.Forms.IWin32Window)

        Dim se As GcpBESerie

        se = bso.Comercial.Series.Edita("V", doc.Tipodoc, doc.Serie)
        Dim numVia As String
        For i = 1 To se.NumVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            If se.Config <> "" Then
                Dim certificado As String
                certificado = bso.Comercial.Vendas.DevolveTextoAssinaturaDocID(doc.Id)
                imprimirMapa(se.Config, IIf(se.Previsao, "W", "P"), "{CabecDoc.Filial}='" + doc.Filial + "' AND {CabecDoc.Serie}='" + doc.Serie + "' AND {CabecDoc.TipoDoc}='" + doc.Tipodoc + "' AND {CabecDoc.Numdoc}=" + CStr(doc.NumDoc), numVia, doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado), owner)
            Else
                MsgBox("O Documento " + doc.Tipodoc + " não possui nenhum mapa configurado!", MsgBoxStyle.Exclamation, "Contactar Anphis")
            End If
        Next i
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoCompra, ByVal owner As System.Windows.Forms.IWin32Window, Optional numVias As Integer = -1)

        Dim se As GcpBESerie

        se = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
        Dim numVia As String

        If numVias = -1 Then
            numVias = se.NumVias
        End If

        For i = 1 To numVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            If se.Config <> "" Then
                Dim certificado As String
                certificado = bso.Comercial.Compras.DevolveTextoAssinaturaDocID(doc.Id)
                imprimirMapa(buscarMapaDefault(doc.Tipodoc, se.Config), IIf(se.Previsao, "W", "P"), "{CabecCompras.Filial}='" + doc.Filial + "' AND {CabecCompras.Serie}='" + doc.Serie + "' AND {CabecCompras.TipoDoc}='" + doc.Tipodoc + "' AND {CabecCompras.Numdoc}=" + CStr(doc.NumDoc), numVia, doc.Moeda, IIf(certificado = "", "Documento Processado por Computador", certificado), owner)
            Else
                MsgBox("O Documento " + doc.Tipodoc + " não possui nenhum mapa configurado!", MsgBoxStyle.Exclamation, "Contactar Anphis")
            End If
        Next i
    End Sub

    Private Function buscarMapaDefault(tipodoc As String, mapaBase As String) As String
        Dim mapa As String
        mapa = ""
        Select Case tipodoc
            Case infModLM.TIPODOCLM : mapa = infModLM.MAPALM
            Case infModLM.TIPODOCCOT : mapa = infModLM.MAPACOT
            Case infModLM.TIPODOCECF : mapa = infModLM.MAPAECF
        End Select

        If mapa <> "" Then
            Return mapa
        Else
            Return mapaBase
        End If

    End Function

    Private Sub imprimirMapa(ByVal report As String, ByVal destiny As String, ByVal selectionFormula As String, ByVal numVia As String, moeda As String, textoCertificacao As String, ByVal owner As System.Windows.Forms.IWin32Window)
        Try

            Dim p As New System.Windows.Forms.PrintDialog
            Dim rp As ReportConfiguracao
            rp = New ReportConfiguracao(codEmpresa, moeda, 2)
            '  Dim mapa As Mapa

            ' mapa = New Mapa()
            ' mapa.iniciarComponente(report, codEmpresa, objConfApl.Instancia, objConfApl.Utilizador, objConfApl.PwdUtilizador, PlataformaPrimavera)
            'buscar o nome da impressora predefinida
            'setDefaultPrinter(p.PrinterSettings.PrinterName)


            PlataformaPrimavera.Mapas.Inicializar("GCP")
            PlataformaPrimavera.Mapas.SelectionFormula = selectionFormula
            PlataformaPrimavera.Mapas.AddFormula("NumVia", numVia)
            PlataformaPrimavera.Mapas.AddFormula("DadosEmpresa", rp.getFormula)
            PlataformaPrimavera.Mapas.AddFormula("InicializaParametros", rp.getFormulaIniciarParametros(moeda, textoCertificacao))
            PlataformaPrimavera.Mapas.DefinicaoImpressoraEx2(p.PrinterSettings.PrinterName, "winspool", "winspool", "", CRPEOrientacaoFolha.ofPortrait, 1, CRPETipoFolha.tfA4, 23, 23, 1, p.PrinterSettings.Duplex, 1, 1, 0)
            PlataformaPrimavera.Mapas.ImprimeListagem(report, "Mapa", destiny, 1, "S", , , , , , True)
            '  PlataformaPrimavera.Mapas.ImprimeListagem(sReport:=report, sTitulo:="Mapa", sDestino:=destiny, iNumCopias:=1, sDocumento:="S", blnModal:=True)
            PlataformaPrimavera.Mapas.TerminaJanelas()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function setDefaultPrinter(ByVal strPrinterName As String) As Boolean
        Dim strOldPrinter As String

        Dim WshNetwork As Object

        Dim pd As New System.Drawing.Printing.PrintDocument

        Try

            strOldPrinter = pd.PrinterSettings.PrinterName

            WshNetwork = Microsoft.VisualBasic.CreateObject("WScript.Network")

            WshNetwork.SetDefaultPrinter(strPrinterName)

            pd.PrinterSettings.PrinterName = strPrinterName

            If pd.PrinterSettings.IsValid Then

                Return True

            Else

                WshNetwork.SetDefaultPrinter(strOldPrinter)

                Return False

            End If

        Catch exptd As Exception

            WshNetwork.SetDefaultPrinter(strOldPrinter)

            Return False

        Finally

            WshNetwork = Nothing

            pd = Nothing

        End Try

    End Function



    Public Function buscarDocumentoCompra(id As String) As GcpBEDocumentoCompra
        If bso.Comercial.Compras.ExisteId(id) Then
            Return bso.Comercial.Compras.EditaID(id)
        Else
            Return Nothing
        End If
    End Function

    Public Function buscarDocumentoVenda(id As String) As GcpBEDocumentoVenda
        If bso.Comercial.Vendas.ExisteId(id) Then
            Return bso.Comercial.Vendas.EditaID(id)
        Else
            Return Nothing
        End If
    End Function




    Public Function daProjectoCodigo(id As String) As String
        daProjectoCodigo = ""
        If bso.Comercial.Projectos.ExisteId(id) Then
            Return bso.Comercial.Projectos.DaValorAtributoID(id, "Codigo")
        End If
    End Function


    Public Function daProjectoNome(id As String) As String
        daProjectoNome = ""
        If bso.Comercial.Projectos.ExisteId(id) Then
            Return bso.Comercial.Projectos.DaValorAtributoID(id, "Descricao")
        End If
    End Function


    'Public Sub actualizarEstadoLinha(modulo As String, id As String, estado As String)
    '    If id = "" Then Exit Sub
    '    If modulo = "C" Then executarComando("UPDATE LINHASCOMPRAS SET CDU_ObraN5='" + estado + "' WHERE id='" + id + "'")

    'End Sub



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="modulo"></param>
    ''' <param name="id"></param>
    ''' <param name="estado"></param>
    ''' <param name="tudo"></param>
    Public Sub actualizarEstadoLinha(baseDadosAtual As String, baseDadosOrig As String, projetoOrig As String, ByRef ht As Hashtable, modulo As String, id As String, estado As String, Optional tudo As Boolean = True, Optional tipoDoc As String = "")

        If id.Trim() = "" Then Exit Sub
        If modulo = "C" Then

            If tipoDoc = "" Then
                executarComando("UPDATE " + baseDadosAtual + "..LINHASCOMPRAS SET CDU_ObraN5='" + estado + "' WHERE id='" + id + "'")
                executarComando("UPDATE " + baseDadosOrig + "..LINHASCOMPRAS SET CDU_ObraN5='" + estado + "' WHERE id='" + id + "'")
            Else
                executarComando("UPDATE LINHASCOMPRAS  SET CDU_ObraN5='" + estado + "' FROM " + baseDadosAtual + "..LINHASCOMPRAS Lc INNER JOIN " + baseDadosAtual + "..CabecCompras C on LC.IdCabecCompras=C.Id WHERE Lc.id='" + id + "' and C.TipoDoc<>'" + tipoDoc + "'")
                executarComando("UPDATE LINHASCOMPRAS  SET CDU_ObraN5='" + estado + "' FROM " + baseDadosOrig + "..LINHASCOMPRAS Lc INNER JOIN " + baseDadosOrig + "..CabecCompras C on LC.IdCabecCompras=C.Id WHERE Lc.id='" + id + "' and C.TipoDoc<>'" + tipoDoc + "'")
            End If

            id = id.Replace("{", "")
            id = id.Replace("}", "")
            id = UCase(id)
            'adicionar ao array o id da linha alterada
            ht.Add(id, id)

            If tudo = False Then
                Exit Sub
            Else
                Dim lista As StdBELista
                lista = bso.Consulta(buscarQueryMultiEmpresa(baseDadosOrig, projetoOrig, id, modulo))
                While Not lista.NoFim
                    If Not IsDBNull(lista.Valor("IdLinhaOrigemCopia")) Then
                        actualizarEstadoLinha(baseDadosAtual, baseDadosOrig, projetoOrig, ht, modulo, lista.Valor("IdLinhaOrigemCopia"), estado, tudo, tipoDoc)
                    End If
                    lista.Seguinte()
                End While
            End If
        End If

    End Sub


    Private Function buscarQueryMultiEmpresa(bd As String, projeto As String, id As String, modulo As String) As String

        Dim query As String = ""
        Dim entidades As String = ""
        query = "SELECT  DISTINCT CDU_BDDestino as BD FROM " + bd + "..TDU_GP_LigacaoProjetos WHERE CDU_ProjetoOrigem='" + projeto + "'"
        Dim lista As StdBELista

        lista = bso.Consulta(query)

        query = "SELECT IdLinhaOrigemCopia  from " + bd + "..LINHASCOMPRAS WHERE  id='" + id + "' and ModuloOrigemCopia='" + modulo + "' and id<>IdLinhaOrigemCopia"


        While Not lista.NoFim
            bd = "PRI" + lista.Valor("BD")
            query += " UNION SELECT IdLinhaOrigemCopia  FROM  " + bd + "..LINHASCOMPRAS WHERE  id='" + id + "' and ModuloOrigemCopia='" + modulo + "' and id<>IdLinhaOrigemCopia"
            lista.Seguinte()
        End While


        Return query



    End Function



    Public Sub actualizarPosto(posto As String, estado As String)
        executarComando("UPDATE TDU_POSTOSTRABALHO set CDU_ACTIVO=" + estado + "WHERE CDU_POSTO='" + posto + "'")
    End Sub

    Public Function VerificarEstadoDocumento(doc As Objeto, estadoAberto As Boolean, campo As String) As Boolean
        Dim count As String
        If estadoAberto Then
            count = consultaValor("SELECT Count(*) FROM " + doc.BaseDados + "..CabecComprasStatus WHERE IdCabecCompras='" + doc.Id + "' AND (" + campo + "=0)")
        Else
            count = consultaValor("SELECT Count(*) FROM " + doc.BaseDados + "..CabecComprasStatus WHERE IdCabecCompras='" + doc.Id + "' AND (" + campo + "=1)")
        End If
        Return count <> "0"
    End Function

    Public Function artigoExiste(artigo As String) As Boolean
        Return bso.Comercial.Artigos.Existe(artigo)
    End Function
    Public Function daArtigoEquivalente(artigoBase As String, row As DataRow) As String
        Dim artigo As String

        artigo = infModLM.ARTIGO
        If getApresentaColunaArtigo() Then
            ' If Not bso.Comercial.Artigos.Existe(artigoBase) Then
            artigo = daArtigoEquivalente(row)
            If Not bso.Comercial.Artigos.Existe(artigo) Then
                artigo = infModLM.ARTIGO
            End If
            ' Else
            'artigo = artigoBase
            'End If
        End If

        Return artigo
        'End Function

        'Private Function daArtigoEquivalente(row As DataRow) As String
        '    Dim lista As StdBELista
        '    Dim artigo As String = ""
        '    Dim marca, peca, descricao, referencia As String
        '    marca = NuloToString(row("Material"))
        '    descricao = NuloToString(row("Descricao"))
        '    peca = NuloToString(row("Peca"))
        '    referencia = NuloToString(row("Dimensoes"))

        '    lista = consulta("SELECT TDU_EQUIV_LSTM_ARTIGO.*,TDU_EQUIV_FORMATO_ARTIGO.CDU_CodigoVB FROM TDU_EQUIV_LSTM_ARTIGO LEFT OUTER JOIN TDU_EQUIV_FORMATO_ARTIGO ON TDU_EQUIV_LSTM_ARTIGO.CDU_FORMATACAOARTIGO=TDU_EQUIV_FORMATO_ARTIGO.CDU_ID  WHERE CDU_MARCALSTM='" + marca + "' CDU_DESCRICAOLSTM='" + descricao + "' and CDU_REFERENCIALSTM='" + referencia + "' AND NOT CDU_Artigo IS NULL")
        '    '
        '    If lista.NumLinhas = 0 Then
        '        lista = consulta("SELECT TDU_EQUIV_LSTM_ARTIGO.*,TDU_EQUIV_FORMATO_ARTIGO.CDU_CodigoVB FROM TDU_EQUIV_LSTM_ARTIGO LEFT OUTER JOIN TDU_EQUIV_FORMATO_ARTIGO ON TDU_EQUIV_LSTM_ARTIGO.CDU_FORMATACAOARTIGO=TDU_EQUIV_FORMATO_ARTIGO.CDU_ID  WHERE CDU_MARCALSTM='" + marca + "' and CDU_DESCRICAOLSTM='" + descricao + "' and CDU_REFERENCIALSTM='" + referencia + "' AND CDU_Artigo IS NULL")
        '        If lista.NumLinhas = 0 Then
        '            lista = consulta("SELECT TDU_EQUIV_LSTM_ARTIGO.*,TDU_EQUIV_FORMATO_ARTIGO.CDU_CodigoVB FROM TDU_EQUIV_LSTM_ARTIGO LEFT OUTER JOIN TDU_EQUIV_FORMATO_ARTIGO ON TDU_EQUIV_LSTM_ARTIGO.CDU_FORMATACAOARTIGO=TDU_EQUIV_FORMATO_ARTIGO.CDU_ID WHERE CDU_MARCALSTM='" + marca + "' and CDU_DESCRICAOLSTM='" + descricao + "' and CDU_REFERENCIALSTM='" + referencia + "'")
        '            If lista.NumLinhas = 0 Then
        '                lista = consulta("SELECT TDU_EQUIV_LSTM_ARTIGO.*,TDU_EQUIV_FORMATO_ARTIGO.CDU_CodigoVB FROM TDU_EQUIV_LSTM_ARTIGO LEFT OUTER JOIN TDU_EQUIV_FORMATO_ARTIGO ON TDU_EQUIV_LSTM_ARTIGO.CDU_FORMATACAOARTIGO=TDU_EQUIV_FORMATO_ARTIGO.CDU_ID  WHERE CDU_MARCALSTM='" + marca + "' and CDU_DESCRICAOLSTM='" + descricao + "'")
        '                If lista.NumLinhas <> 0 Then
        '                    artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
        '                End If
        '            End If
        '        End If
        '    Else
        '        artigo = lista.Valor("CDU_Artigo")
        '    End If

        '    Return artigo

        'End Function


    End Function

    Public Function daArtigoEquivalente(row As DataRow) As String
        Dim lista As StdBELista
        Dim artigo As String = ""
        Dim marca, peca, descricao, referencia As String
        marca = NuloToString(row("CDU_ObraN4"))
        descricao = NuloToString(row("Descricao"))
        peca = NuloToString(row("CDU_ObraN1"))
        referencia = NuloToString(row("CDU_ObraN3"))






        '============PROCURAR --> Marca -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida(marca, descricao, referencia, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida("", descricao, referencia, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR --> Descricao -->===================
        lista = procurarLinhaValida("", descricao, "", True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR --> ENTRE LINHAS  -->===================
        '============PROCURAR --> Marca -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida(marca, descricao, referencia, True, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If

        '============PROCURAR  -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida("", descricao, referencia, True, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR --> Descricao -->===================
        lista = procurarLinhaValida("", descricao, "", True, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If

        '============ FIM PROCURAR --> ENTRE LINHAS  -->===================


        '============PROCURAR --> Marca -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida(marca, descricao, referencia, False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If

        '============PROCURAR -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida("", descricao, referencia, False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If

        '============PROCURAR --> Descricao -->===================
        lista = procurarLinhaValida("", descricao, "", False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If



    End Function



    Public Function daArtigoEquivalenteOLD(row As DataRow) As String
        Dim lista As StdBELista
        Dim artigo As String = ""
        Dim marca, peca, descricao, referencia As String
        marca = NuloToString(row("CDU_ObraN4"))
        descricao = NuloToString(row("Descricao"))
        peca = NuloToString(row("CDU_ObraN1"))
        referencia = NuloToString(row("CDU_ObraN3"))






        '============PROCURAR --> Marca -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida(marca, descricao, referencia, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If

        '============PROCURAR --> Marca -->Descricao  ===================
        lista = procurarLinhaValida(marca, descricao, "", True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If

        '============PROCURAR --> Marca -->===================
        lista = procurarLinhaValida(marca, "", "", True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If

        '============PROCURAR --> Descricao -->===================
        lista = procurarLinhaValida("", descricao, "", True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR --> ENTRE LINHAS  -->===================
        '============PROCURAR --> Marca -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida(marca, descricao, referencia, True, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR  -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida("", descricao, referencia, True, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If


        '============PROCURAR --> Descricao -->===================
        lista = procurarLinhaValida("", descricao, "", True, True)
        If Not lista Is Nothing Then
            artigo = lista.Valor("CDU_Artigo")
            Return artigo
        End If

        '============ FIM PROCURAR --> ENTRE LINHAS  -->===================


        '============PROCURAR --> Marca -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida(marca, descricao, referencia, False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If

        '============PROCURAR  -->Descricao --> Referencia   ===================
        lista = procurarLinhaValida("", descricao, referencia, False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If



        '============PROCURAR --> Marca -->Descricao  ===================
        lista = procurarLinhaValida(marca, descricao, "", False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If

        '============PROCURAR --> Marca -->===================
        lista = procurarLinhaValida(marca, "", "", False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If

        '============PROCURAR --> Descricao -->===================
        lista = procurarLinhaValida("", descricao, "", False)
        If Not lista Is Nothing Then
            artigo = formatarArtigoEquivalente(lista, marca, descricao, peca, referencia)
            Return artigo
        End If



    End Function


    Private Function procurarLinhaValida(marca As String, descricao As String, referencia As String, artigo As Boolean, Optional procuraentreoutrasRef As Boolean = False) As StdBELista
        Dim lista As StdBELista
        Dim query As String
        Dim queryMarca As String = ""
        Dim queryDescricao As String = ""
        Dim queryReferencia As String = ""
        Dim queryArtigo As String = ""


        If marca <> "" Then
            queryMarca = " AND CDU_MARCALSTM='" + marca + "'"
        Else
            If Not procuraentreoutrasRef Then queryMarca = " AND CDU_MARCALSTM IS NULL"
        End If


        If descricao <> "" Then
            queryDescricao = " AND CDU_DESCRICAOLSTM='" + descricao + "'"
        Else
            If Not procuraentreoutrasRef Then queryDescricao = " AND CDU_DESCRICAOLSTM IS NULL"
        End If

        If referencia <> "" Then
            queryReferencia = " AND CDU_REFERENCIALSTM='" + referencia + "'"
        Else
            If Not procuraentreoutrasRef Then queryReferencia = " AND CDU_REFERENCIALSTM IS NULL"
        End If


        If artigo Then
            queryArtigo = " AND NOT CDU_Artigo IS NULL"
        Else
            queryArtigo = " AND CDU_Artigo IS NULL"
        End If



        query = "SELECT TDU_EQUIV_LSTM_ARTIGO.*,TDU_EQUIV_FORMATO_ARTIGO.CDU_CodigoVB FROM TDU_EQUIV_LSTM_ARTIGO LEFT OUTER JOIN TDU_EQUIV_FORMATO_ARTIGO ON TDU_EQUIV_LSTM_ARTIGO.CDU_FORMATACAOARTIGO=TDU_EQUIV_FORMATO_ARTIGO.CDU_ID  WHERE 1=1 " + queryMarca + queryDescricao + queryReferencia + queryArtigo
        Try


            'If marca = "" And descricao <> "" And referencia = "" Then
            '    InputBox("", "", query)
            'End If
            lista = consulta(query, False)

            If lista.NumLinhas = 0 Then
                Return Nothing
            Else
                Return lista
            End If

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function formatarArtigoEquivalente(lista As StdBELista, marca As String, descricao As String, peca As String, referencia As String) As String
        Dim formato As String

        Dim codigoVB As String

        marca = daValorCampo(lista.Valor("CDU_MARCALSTM"), lista.Valor("CDU_MARCAARTIGO"), marca)
        descricao = daValorCampo(lista.Valor("CDU_DESCRICAOLSTM"), lista.Valor("CDU_DESCRICAOARTIGO"), descricao)
        ' peca = daValorCampo(lista.Valor("CDU_PECALSTM"), lista.Valor("CDU_PECAARTIGO"), peca)
        referencia = daValorCampo(lista.Valor("CDU_REFERENCIALSTM"), lista.Valor("CDU_REFERENCIAARTIGO"), referencia)

        formato = NuloToString(lista.Valor("CDU_FORMATOARTIGO"))
        codigoVB = NuloToString(lista.Valor("CDU_CodigoVB"))

        'Dim valor As String
        'Dim str As String()
        'valor = "@R"
        'Dim result = valor.Substring(valor.LastIndexOf("/") + 1).ToUpper
        'str = result.Split("X")

        'If str.Length = 2 Then

        '    If Len(str(0)) = 1 Then
        '        result = "0" + str(0)
        '    Else
        '        result = str(0)
        '    End If

        '    If Len(str(1)) = 1 Then
        '        result += "0" + str(1)
        '    Else
        '        result += str(1)
        '    End If

        'End If
        'Return result



        codigoVB = Replace(codigoVB, """@M""", """" + marca + """")
        codigoVB = Replace(codigoVB, """@P""", """" + peca + """")
        codigoVB = Replace(codigoVB, """@D""", """" + descricao + """")
        codigoVB = Replace(codigoVB, """@R""", """" + referencia + """")

        codigoVB = CompileAndRunCode(codigoVB)

        formato = Replace(formato, "@M", marca)
        formato = Replace(formato, "@P", peca)
        formato = Replace(formato, "@D", descricao)
        formato = Replace(formato, "@R", referencia)
        formato = Replace(formato, "@F", codigoVB)

        Return formato
    End Function

    Private Function daValorCampo(valor1 As Object, valor2 As Object, valor3 As String) As String
        Dim strValor As String
        strValor = NuloToString(valor2)
        If strValor = "" Then
            strValor = NuloToString(valor1)
            If strValor = "" Then
                strValor = valor3
            End If
        End If
        Return strValor
    End Function

    Public Function CompileAndRunCode(ByVal VBCodeToExecute As String) As Object

        Dim sReturn_DataType As String
        Dim sReturn_Value As String = ""
        Try

            ' Instance our CodeDom wrapper
            Dim ep As New cVBEvalProvider

            ' Compile and run
            Dim objResult As Object = ep.Eval(VBCodeToExecute)
            If ep.CompilerErrors.Count <> 0 Then
                Diagnostics.Debug.WriteLine("CompileAndRunCode: Compile Error Count = " & ep.CompilerErrors.Count)
                Diagnostics.Debug.WriteLine(ep.CompilerErrors.Item(0))
                Return "ERROR" ' Forget it
            End If
            Dim t As Type = objResult.GetType()
            If t.ToString() = "System.String" Then
                sReturn_DataType = t.ToString
                sReturn_Value = Convert.ToString(objResult)
            Else
                ' Some other type of data - not really handled at 
                ' this point. rwd
                'ToDo: Add handlers for other data return types, if needed

                ' Here is an example to handle a dataset...
                'Dim ds As DataSet = DirectCast(objResult, DataSet)
                'DataGrid1.Visible = True
                'TextBox2.Visible = False
                'DataGrid1.DataSource = ds.Tables(0)
            End If

        Catch ex As Exception
            Dim sErrMsg As String
            sErrMsg = String.Format("{0}", ex.Message)
            ' Do Nothing - This is just a negative case
            ' This outcome is expected in late interpreting
            ' I suppose what I am saying is: Don't stop my program because the script writer can't write
            ' script very well.  To be fair, we could log this somewhere and notify somebody.
        End Try

        Return sReturn_Value

    End Function

    ''' <summary>
    ''' Função que permite actualizar a referencia de euivalencia do artigo
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <param name="marca"></param>
    ''' <param name="descricao"></param>
    ''' <param name="referencia"></param>
    ''' <param name="artigo"></param>
    ''' <param name="artigoBase"></param>
    Private Sub actualizarFichaEquivalenciasArtigos(peca As String, marca As String, descricao As String, referencia As String, artigo As String, artigoBase As String)
        If artigo = "" Or artigoBase = "" Then
            Return
        End If

        If artigo <> artigoBase Then
            Dim valor As String
            valor = consultaValor("SELECT count(*) from TDU_EQUIV_LSTM_ARTIGO WHERE CDU_MARCALSTM='" + marca + "' and CDU_DESCRICAOLSTM='" + descricao + "' and CDU_REFERENCIALSTM='" + referencia + "' and  CDU_ARTIGO='" + artigoBase + "'")
            If CDbl(valor) > 0 Then
                executarComando("UPDATE TDU_EQUIV_LSTM_ARTIGO set CDU_ARTIGO='" + artigo + "' WHERE CDU_MARCALSTM='" + marca + "' and CDU_DESCRICAOLSTM='" + descricao + "' and CDU_REFERENCIALSTM='" + referencia + "' and  CDU_ARTIGO='" + artigoBase + "'")
            Else
                executarComando("INSERT INTO TDU_EQUIV_LSTM_ARTIGO (CDU_MARCALSTM,CDU_DESCRICAOLSTM,CDU_REFERENCIALSTM,CDU_ARTIGO ) VALUES ('" + marca + "','" + descricao + "','" + referencia + "','" + artigo + "')")
                'executarComando("INSERT INTO TDU_EQUIV_LSTM_ARTIGO (CDU_MARCALSTM,CDU_DESCRICAOLSTM,CDU_REFERENCIALSTM,CDU_ARTIGO ) VALUES ('" + marca + "','" + descricao + "',NULL,'" + artigo + "')")
            End If
        End If
    End Sub


    Public Function buscarEntidadesLinha(idLinha As String, projeto As String) As String

        Dim query As String = ""
        Dim entidades As String = ""
        query = "SELECT  DISTINCT CDU_BDDestino as BD FROM TDU_GP_LigacaoProjetos WHERE CDU_ProjetoOrigem='" + projeto + "' UNION SELECT '" + codEmpresa + "'"
        Dim lista As StdBELista

        lista = bso.Consulta(query)
        While Not lista.NoFim
            entidades += buscarEntidadeLinha(idLinha, "", lista.Valor("BD"))
            lista.Seguinte()
        End While

        Return entidades
    End Function


    Private Function buscarEntidadeLinha(idLinha As String, ent As String, empresa As String) As String
        Dim dt As DataTable
        Dim dtECF As DataTable
        Dim lista As StdBELista
        Dim i As Integer
        Dim entidadeCOT As String = ""
        Dim entidadeECF As String = ""
        empresa = "PRI" + empresa

        lista = consulta("Select  C.Tipodoc, C.Entidade,C.Nome,L.Id  FROM " + empresa + "..CabecCompras C INNER JOIN " + empresa + "..LinhasCompras L on C.id=L.IdCabecCompras INNER JOIN " + empresa + "..CabecComprasStatus CCS ON C.id=CCS.IdCabecCompras WHERE CCS.Anulado=0 and  C.Tipodoc<>'" + infModLM.TIPODOCALT + "' and L.IdLinhaOrigemCopia ='" + idLinha + "'")

        If lista Is Nothing Then Return ""

        dt = convertRecordSetToDataTable(lista.DataSet)
        If dt.Rows.Count > 0 Then
            ent = ""
            For i = 0 To dt.Rows.Count - 1

                If dt.Rows(i)("Tipodoc") = infModLM.TIPODOCECF Then
                    entidadeECF = entidadeECF + dt.Rows(i)("Nome") + " ; "
                Else
                    entidadeCOT = entidadeCOT + dt.Rows(i)("Nome") + " ; "
                End If
                idLinha = dt.Rows(i)("Id").ToString
                lista = consulta("Select  C.Tipodoc, C.Entidade,C.Nome,L.Id  FROM " + empresa + "..CabecCompras C INNER JOIN " + empresa + "..LinhasCompras L on C.id=L.IdCabecCompras INNER JOIN " + empresa + "..CabecComprasStatus CCS ON C.id=CCS.IdCabecCompras WHERE CCS.Anulado=0 and C.Tipodoc<>'" + infModLM.TIPODOCALT + "' and L.IdLinhaOrigemCopia ='" + idLinha + "'")

                If Not lista Is Nothing Then
                    dtECF = convertRecordSetToDataTable(lista.DataSet)
                    If dtECF.Rows.Count > 0 Then
                        If dtECF.Rows(0)("Tipodoc") = infModLM.TIPODOCECF Then
                            entidadeECF = entidadeECF + dtECF.Rows(0)("Nome") + " ; "
                        Else
                            entidadeCOT = entidadeCOT + dtECF.Rows(0)("Nome") + " ; "
                        End If
                    End If
                End If

            Next
        End If

        If entidadeECF <> "" Then
            ent = entidadeECF
        Else
            ent = entidadeCOT
        End If

        If ent <> "" Then
            ent = Mid(ent, 1, Len(ent) - 2)
        End If

        Return ent

    End Function



    Public Function buscarCampoLinhaAssociada(idLinha As String, campo As String) As String
        Dim dt As DataTable

        Dim lista As StdBELista
        Dim i As Integer
        Dim tipodoc As String = ""
        Dim entidadeECF As String = ""
        Dim valor As String = ""

        Dim noFim As Boolean
        noFim = True

        While noFim

            lista = consulta("Select  CabecCompras.Tipodoc,LinhasCompras.Id," + campo + "  as AuxCampo  FROM CabecCompras INNER JOIN LinhasCompras on CabecCompras.id=LinhasCompras.IdCabecCompras INNER JOIN CabecComprasStatus ON CabecCompras.id=CabecComprasStatus.IdCabecCompras WHERE CabecComprasStatus.Anulado=0 and  CabecCompras.Tipodoc<>'" + infModLM.TIPODOCALT + "' and LinhasCompras.IdLinhaOrigemCopia ='" + idLinha + "' order by " + campo + " asc")

            If Not lista Is Nothing Then
                dt = convertRecordSetToDataTable(lista.DataSet)
                If dt.Rows.Count <> 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        If dt.Rows(i)("Tipodoc") <> infModLM.TIPODOCCOT Then
                            tipodoc = NuloToString(dt.Rows(i)("Tipodoc"))
                            valor = NuloToString(dt.Rows(i)("AuxCampo"))
                        End If
                        idLinha = NuloToString(dt.Rows(i)("Id"))
                    Next
                Else
                    noFim = False
                End If
            Else
                noFim = False
            End If

        End While


        If tipodoc <> infModLM.TIPODOCCOT Then
            Return valor
        Else
            Return ""
        End If

    End Function

    Public Function buscarCampoLinhaAssociada1(BD As String, idLinha As String, campo As String) As String

        Dim dt As DataTable
        Dim dtECF As DataTable
        Dim lista As StdBELista
        Dim i As Integer
        Dim valor As String = ""
        Dim entidadeECF As String = ""
        lista = consulta("Select  C.Tipodoc,L.Id," + campo + "  as AuxCampo  FROM " + BD + "..CabecCompras C INNER JOIN " + BD + "..LinhasCompras L on C.id=L.IdCabecCompras INNER JOIN " + BD + ".. CabecComprasStatus CCS ON C.id=CCS.IdCabecCompras WHERE CCS.Anulado=0 and  C.Tipodoc<>'" + infModLM.TIPODOCALT + "' and L.IdLinhaOrigemCopia ='" + idLinha + "'")

        If lista Is Nothing Then Return ""

        dt = convertRecordSetToDataTable(lista.DataSet)
        If dt.Rows.Count > 0 Then
            valor = ""
            For i = 0 To dt.Rows.Count - 1

                If dt.Rows(i)("Tipodoc") = infModLM.TIPODOCECF Then
                    valor = NuloToString(dt.Rows(i)("AuxCampo"))
                End If
                idLinha = dt.Rows(i)("Id").ToString
                lista = consulta("Select   C.Tipodoc,L.Id," + campo + "  as AuxCampo  FROM " + BD + "..CabecCompras C INNER JOIN " + BD + "..LinhasCompras L on C.id=L.IdCabecCompras INNER JOIN " + BD + "..CabecComprasStatus CCS ON C.id=CCS.IdCabecCompras WHERE CCS.Anulado=0 and C.Tipodoc<>'" + infModLM.TIPODOCALT + "' and L.IdLinhaOrigemCopia ='" + idLinha + "'")

                If Not lista Is Nothing Then
                    dtECF = convertRecordSetToDataTable(lista.DataSet)
                    If dtECF.Rows.Count > 0 Then
                        If dtECF.Rows(0)("Tipodoc") = infModLM.TIPODOCECF Then
                            valor = NuloToString(dtECF.Rows(0)("AuxCampo"))
                        End If
                    End If
                End If

            Next
        End If

        Return valor

    End Function

    Public Function inicializarAplicacao() As Boolean

        Dim msg As String
        msg = ""

        If Not existeDocumentoCompra(infModLM.TIPODOCLM) Then
            msg = "O Documento de Compra da Lista de Materais '" + infModLM.TIPODOCLM + "' não existe!"
        End If

        If Not existeDocumentoCompra(infModLM.TIPODOCCOT) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Documento de Compra do Pedido de cotação '" + infModLM.TIPODOCCOT + "' não existe!"
        End If

        If Not existeDocumentoCompra(infModLM.TIPODOCECF) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Documento de Compra de encomenda '" + infModLM.TIPODOCECF + "' não existe!"
        End If

        If Not existeDocumentoCompra(infModLM.TIPODOCALT) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Documento de Compra de Alterações '" + infModLM.TIPODOCALT + "' não existe!"
        End If


        If Not existeDocumentoCompra(infModLM.TIPODOCSTK) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Documento de Compra de Recepção de Material '" + infModLM.TIPODOCSTK + "' não existe!"
        End If


        If Not existeArtigo(infModLM.ARTIGO) Then
            If msg <> "" Then msg += vbCrLf + vbCrLf
            msg += "O Artigo  " + infModLM.ARTIGO + " não existe!"
        End If

        If msg <> "" Then
            MsgBox(msg, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erro")
            Return False
        Else
            Return True
        End If
    End Function


    Private Function existeView(view As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = " Select count(*) from sys.objects where name ='" + view + "' and type='V'"
        valor = consultaValor(consulta)
        Return valor <> "0"
    End Function

    Private Function existeTabela(tabela As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = " select count(*) from sys.objects where name ='" + tabela + "' and type='U'"
        valor = consultaValor(consulta)
        Return valor <> "0"
    End Function

    Private Function existeCampo(tabela As String, campo As String) As Boolean
        Dim consulta As String
        Dim valor As String
        consulta = " select count(*) from sys.columns where name ='" + campo + "' and OBJECT_ID('" + tabela + "')=object_id"
        valor = consultaValor(consulta)
        Return valor <> "0"
    End Function


    Private Function existeDocumentoCompra(documento As String) As Boolean
        Return bso.Comercial.TabCompras.Existe(documento)
    End Function

    Private Function existeArtigo(artigo As String) As Boolean
        Return bso.Comercial.Artigos.Existe(artigo)
    End Function

    Private Sub actualizarLinhaAlteracoes(func As String, fornecedor As Fornecedor, projecto As String, idProjecto As String, linha As GcpBELinhaDocumentoCompra, peca As String, material As String, descricao As String, dimensoes As String, artigo As String, precunit As Double, quantidade As Double, temperamento As String)

        If artigo = "" Then
            Exit Sub
        End If
        If existeAlteracoes(linha, peca, material, descricao, dimensoes, artigo, precunit, quantidade, temperamento) Then
            Dim docC As GcpBEDocumentoCompra
            Dim idCabecdoc As String
            Dim Data As Date
            Dim linhaC As GcpBELinhaDocumentoCompra
            docC = Nothing
            Data = FormatDateTime(Now, DateFormat.ShortDate)
            idCabecdoc = existeDocumentoCompra(infModLM.TIPODOCALT, projecto)
            If idCabecdoc <> "" Then
                docC = bso.Comercial.Compras.EditaID(idCabecdoc)
                Data = docC.DataDoc
            End If

            If docC Is Nothing Then
                docC = New GcpBEDocumentoCompra
                docC.Tipodoc = infModLM.TIPODOCALT
                If serieC_ <> "" Then
                    docC.Serie = serieC_
                Else
                    docC.Serie = bso.Comercial.Series.DaSerieDefeito("C", infModLM.TIPODOCALT, Data)
                End If

                docC.Entidade = fornecedor.Fornecedor
                docC.TipoEntidade = tipoEntidadeC_
                bso.Comercial.Compras.PreencheDadosRelacionados(docC)
            End If

            docC.Utilizador = func
            docC.DataDoc = CDate(Data)
            docC.DataVenc = docC.DataDoc
            docC.DataIntroducao = docC.DataDoc
            docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
            '  docC.Observacoes = observacoes

            docC.IDObra = idProjecto

            bso.Comercial.Compras.AdicionaLinha(docC, artigo)

            linhaC = docC.Linhas(docC.Linhas.NumItens)
            linhaC.Artigo = linha.Artigo
            linhaC.TipoLinha = consultaValor("SELECT TipoLinha from TiposArtigo inner join Artigo on TiposArtigo.TipoArtigo=Artigo.TipoArtigo where Artigo.Artigo='" + linha.Artigo + "'")
            linhaC.Unidade = linha.Unidade
            linhaC.Quantidade = linha.Quantidade
            linhaC.Descricao = linha.Descricao
            linhaC.PrecUnit = linha.PrecUnit
            linhaC.IDObra = idProjecto  'bso.Comercial.Projectos.DaValorAtributo(projecto, "ID")
            linhaC.CamposUtil("CDU_ObraN1") = linha.CamposUtil("CDU_ObraN1")
            linhaC.CamposUtil("CDU_ObraN3") = linha.CamposUtil("CDU_ObraN3")
            linhaC.CamposUtil("CDU_ObraN4") = linha.CamposUtil("CDU_ObraN4")
            If linhaC.CamposUtil.Existe("CDU_ObraN6") Then
                linhaC.CamposUtil("CDU_ObraN6") = linha.CamposUtil("CDU_ObraN6")
            End If

            If linhaC.CamposUtil.Existe("CDU_ObraN7") Then
                linhaC.CamposUtil("CDU_ObraN7") = linha.CamposUtil("CDU_ObraN7")
            End If

            If linhaC.CamposUtil.Existe("CDU_ObraN8") Then
                linhaC.CamposUtil("CDU_ObraN8") = linha.CamposUtil("CDU_ObraN8")
            End If

            If linhaC.CamposUtil.Existe("CDU_ObraN9") Then
                linhaC.CamposUtil("CDU_ObraN9") = linha.CamposUtil("CDU_ObraN9")
            End If

            If linhaC.CamposUtil.Existe("CDU_Obra10") Then
                linhaC.CamposUtil("CDU_Obra10") = linha.CamposUtil("CDU_Obra10")
            End If


            If linhaC.CamposUtil.Existe("CDU_FuncAlteracao") Then
                linhaC.CamposUtil("CDU_FuncAlteracao") = consultaValor("SELECT Nome FROM APS_GP_Funcionarios WHERE Codigo='" + func + "'")
                If linhaC.CamposUtil("CDU_FuncAlteracao").Valor = "" Then
                    linhaC.CamposUtil("CDU_FuncAlteracao").Valor = func
                End If
            End If


            linhaC.ModuloOrigemCopia = "C"
            linhaC.IdLinhaOrigemCopia = linha.IdLinha
            linhaC.DataEntrega = linha.DataEntrega


            If docC.Linhas.NumItens > 0 Or idCabecdoc <> "" Then
                bso.Comercial.Compras.Actualiza(docC)
            End If
        End If
    End Sub

    Private Function existeAlteracoes(linha As GcpBELinhaDocumentoCompra, peca As String, material As String, descricao As String, dimensoes As String, artigo As String, precunit As Double, quantidade As Double, temperamento As String) As Boolean

        'If linha.Artigo.ToUpper <> artigo.ToUpper Then
        '    Return True
        'End If

        If linha.Descricao.ToUpper <> descricao.ToUpper Then
            Return True
        End If

        If linha.Quantidade <> quantidade Then
            Return True
        End If


        If linha.PrecUnit <> precunit Then
            Return True
        End If

        If NuloToString(linha.CamposUtil("CDU_ObraN1").Valor).ToString.ToUpper() <> peca.ToUpper() Then
            Return True
        End If

        If NuloToString(linha.CamposUtil("CDU_ObraN3").Valor).ToString.ToUpper() <> dimensoes.ToUpper() Then
            Return True
        End If

        If NuloToString(linha.CamposUtil("CDU_ObraN4").Valor).ToString.ToUpper() <> material.ToUpper() Then
            Return True
        End If

        If linha.CamposUtil.Existe("CDU_ObraN6") Then
            If NuloToString(linha.CamposUtil("CDU_ObraN6").Valor).ToString.ToUpper() <> temperamento.ToUpper() Then
                Return True
            End If
        End If


    End Function



    Public Function existePeca(grid As AxXtremeReportControl.AxReportControl, dtrow As DataRow, indexPeca As Integer, indexDescricao As Integer, indexDimensoes As Integer, indexMaterial As Integer) As Integer
        Dim row As ReportRow
        Dim peca As String
        Dim descricao As String
        Dim dimensoes As String
        Dim material As String
        peca = dtrow("CDU_ObraN1").ToString()
        descricao = dtrow("Descricao").ToString()
        dimensoes = dtrow("CDU_ObraN3").ToString()
        material = dtrow("CDU_ObraN4").ToString()


        'For Each row In grid.Rows
        '    If UCase(row.Record(indexPeca).Value) = peca.ToUpper() And UCase(row.Record(indexDescricao).Value) = descricao.ToUpper() And UCase(row.Record(indexDimensoes).Value) = dimensoes.ToUpper() And UCase(row.Record(indexMaterial).Value) = material.ToUpper() Then
        '        Return row.Record.Index
        '    End If
        'Next

        For Each row In grid.Rows
            If UCase(row.Record(indexPeca).Value) = peca.ToUpper() Then
                Return row.Record.Index
            End If
        Next
        Return -1
    End Function


    Public Function existePeca(ht As Hashtable, dtrow As DataRow) As Integer
        Dim row As ReportRow
        Dim peca As String
        Dim descricao As String
        Dim dimensoes As String
        Dim material As String
        peca = dtrow("CDU_ObraN1").ToString()
        descricao = dtrow("Descricao").ToString()
        dimensoes = dtrow("CDU_ObraN3").ToString()
        material = dtrow("CDU_ObraN4").ToString()


        'For Each row In grid.Rows
        '    If UCase(row.Record(indexPeca).Value) = peca.ToUpper() And UCase(row.Record(indexDescricao).Value) = descricao.ToUpper() And UCase(row.Record(indexDimensoes).Value) = dimensoes.ToUpper() And UCase(row.Record(indexMaterial).Value) = material.ToUpper() Then
        '        Return row.Record.Index
        '    End If
        'Next


        If ht.ContainsKey(peca) Then
            Return ht(peca)
        Else
            Return -1
        End If

    End Function


    ''' <summary>
    ''' Permite verificar se existe alterações na linha
    ''' </summary>
    ''' <param name="row1"></param>
    ''' <param name="row2"></param>
    ''' <returns></returns>
    Public Function existeAlteracoes(row1 As DataRow, row2 As DataRow, mgrid As MotorGrelhas, grid As AxXtremeReportControl.AxReportControl, ByRef indexLinha As Integer) As Boolean
        Dim coluna As String
        Dim existeAlt As Boolean = False
        'Dim i As Integer

        'coluna = "Artigo"

        'If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString()) Then
        '    Return True
        'End If

        coluna = "Descricao"

        If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
            existeAlt = True
        End If


        coluna = "Quantidade"
        '  If IsNumeric(row1(coluna)) And IsNumeric(row2(coluna)) Then
        ' row1(coluna) = row1(coluna) * mgrid.getMultiplicador
        '  row1(coluna)
        If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
            existeAlt = True
        End If
        ' End If

        'coluna = "PrecUnit"

        'If existeAlteracao(row1(coluna), row2(coluna)) Then
        '    Return True
        'End If

        coluna = "CDU_ObraN1"



        If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
            existeAlt = True
        End If

        coluna = "CDU_ObraN3"

        If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
            existeAlt = True
        End If

        coluna = "CDU_ObraN4"

        If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
            existeAlt = True
        End If

        coluna = "CDU_ObraN6"
        If row1.Table.Columns.IndexOf(coluna) <> -1 Then

            If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then

                existeAlt = True

            End If
        End If


        coluna = "CDU_ObraN7"
        If row1.Table.Columns.IndexOf(coluna) <> -1 Then

            If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then

                existeAlt = True

            End If
        End If

        coluna = "CDU_ObraN8"
        If row1.Table.Columns.IndexOf(coluna) <> -1 Then
            If existeAlteracao(row1(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
                existeAlt = True
            End If
        End If


        If existeAlt Then
            'Dim existealt1 As Boolean
            If grid.Records.Count > indexLinha + 1 Then
                Dim rowGrelha As DataRow
                rowGrelha = grid.Records(indexLinha + 1).Tag
                coluna = "CDU_ObraN1"
                If Not existeAlteracao(rowGrelha(coluna).ToString(), row2(coluna).ToString(), mgrid, coluna) Then
                    indexLinha = indexLinha + 1
                    existeAlt = existeAlteracoes(rowGrelha, row2, mgrid, grid, indexLinha)
                End If
            End If
        End If

        Return existeAlt
    End Function

    Private Function existeAlteracao(valor1 As String, valor2 As String, mgrid As MotorGrelhas, coluna As String) As Boolean
        Return valor1.ToUpper().Trim() <> valor2.ToUpper().Trim() And mgrid.buscarIndexCampo(UCase(coluna)) <> -1
    End Function


    Public Function daTotalDocumento(doc As Objeto, modulo As String) As String

        If modulo = "V" Then
            Return FormatNumber(consultaValor("SELECT isnull(TotalMerc,0) FROM " + doc.BaseDados + "..CabecDoc WHERE id='" + doc.Id + "'"), 2).ToString()
        End If

        If modulo = "C" Then
            Return FormatNumber(consultaValor("SELECT isnull(TotalMerc,0)*-1 FROM " + doc.BaseDados + "..CabecCompras WHERE id='" + doc.Id + "'"), 2).ToString()
        End If

        Return "0"
    End Function


    Public Sub aplicarversao001()
        If Not existeCampo("LinhasCompras", "CDU_ObraN6") Then
            executarComando("ALTER TABLE LinhasCompras ADD CDU_ObraN6 nvarchar(255)", False)
        End If
        If Not existeCampo("LinhasCompras", "CDU_ObraN7") Then
            executarComando("ALTER TABLE LinhasCompras ADD CDU_ObraN7 nvarchar(255)", False)
        End If
        If Not existeCampo("LinhasCompras", "CDU_ObraN8") Then
            executarComando("ALTER TABLE LinhasCompras ADD CDU_ObraN8 nvarchar(255)", False)
        End If
    End Sub


    Public Sub buscarUltimoTipoDocDocumentoTransformado(modulo As String, idLinha As String, ByRef msg As String)

        Dim lista As StdBELista
        Dim query As String


        query = "SELECT LINHASCOMPRAS.id,CABECCOMPRAS.tipodoc, CABECCOMPRAS.Serie,CABECCOMPRAS.NumDoc,CABECCOMPRAS.DataDoc, CABECCOMPRAS.Nome from LINHASCOMPRAS INNER JOIN CABECCOMPRAS ON LINHASCOMPRAS.idCabecCompras=CABECCOMPRAS.Id INNER JOIN CabecComprasStatus On CabecComprasStatus.IdCabecCompras=CABECCOMPRAS.id and CabecComprasStatus.anulado=0  WHERE CABECCOMPRAS.TipoDoc<>'" + infModLM.TIPODOCALT + "' and   LINHASCOMPRAS.IdLinhaOrigemCopia='" + idLinha + "' and ModuloOrigemCopia='" + modulo + "' and LINHASCOMPRAS.id<>LINHASCOMPRAS.IdLinhaOrigemCopia"
        lista = bso.Consulta(query)
        While Not lista.NoFim
            If Not IsDBNull(lista.Valor("Id")) Then
                If msg <> "" Then
                    msg += vbCrLf
                End If
                msg += lista.Valor("tipodoc") + " " + lista.Valor("serie") + "/" + CStr(lista.Valor("numdoc")) + " de " + CStr(lista.Valor("DataDoc")) + " - " + lista.Valor("Nome")
                buscarUltimoTipoDocDocumentoTransformado(modulo, lista.Valor("Id"), msg)
            End If
            lista.Seguinte()
        End While

    End Sub


    Const ESTADO_NORMAL = ""
    Const ESTADO_INTERNO = "I"
    Const ESTADO_PROBLEMAS = "P"
    Const ESTADO_COT = "C"
    Const ESTADO_ECF = "E"
    Const ESTADO_VER = "V"
    Const ESTADO_CONF = "F"
    Const ESTADO_A = "A"
    Const ESTADO_CLIENTE = "L"

    'Public Function buscarEstadoLinhaValida(idLinha As String, modulo As String)
    '    Dim lista As StdBELista
    '    Dim query As String

    '    query = "SELECT LINHASCOMPRAS.IdLinhaOrigemCopia from LINHASCOMPRAS INNER JOIN CABECCOMPRAS ON LINHASCOMPRAS.idCabecCompras=CABECCOMPRAS.Id INNER JOIN CabecComprasStatus On CabecComprasStatus.IdCabecCompras=CABECCOMPRAS.id  WHERE  LINHASCOMPRAS.id='" + idLinha + "' and ModuloOrigemCopia='" + modulo + "' and LINHASCOMPRAS.id<>LINHASCOMPRAS.IdLinhaOrigemCopia"
    '    lista = bso.Consulta(query)
    '    If Not lista.NoFim Then
    '        idLinha = lista.Valor("IdLinhaOrigemCopia")
    '        query = "SELECT CABECCOMPRAS.TipoDoc from LINHASCOMPRAS INNER JOIN CABECCOMPRAS ON LINHASCOMPRAS.idCabecCompras=CABECCOMPRAS.Id INNER JOIN CabecComprasStatus On CabecComprasStatus.IdCabecCompras=CABECCOMPRAS.id and CabecComprasStatus.anulado=0  WHERE  LINHASCOMPRAS.IdLinhaOrigemCopia='" + idLinha + "' and ModuloOrigemCopia='" + modulo + "' and LINHASCOMPRAS.id<>LINHASCOMPRAS.IdLinhaOrigemCopia"
    '        lista = bso.Consulta(query)

    '        If lista.NumLinhas > 0 Then
    '            Return buscarEstadoDocumento(lista.Valor("TipoDoc"))
    '        Else

    '            query = "SELECT CABECCOMPRAS.TipoDoc from LINHASCOMPRAS INNER JOIN CABECCOMPRAS ON LINHASCOMPRAS.idCabecCompras=CABECCOMPRAS.Id INNER JOIN CabecComprasStatus On CabecComprasStatus.IdCabecCompras=CABECCOMPRAS.id and CabecComprasStatus.anulado=0  WHERE  LINHASCOMPRAS.id='" + idLinha + "' and ModuloOrigemCopia='" + modulo + "' and LINHASCOMPRAS.id<>LINHASCOMPRAS.IdLinhaOrigemCopia"
    '            lista = bso.Consulta(query)
    '            If Not lista.NoFim Then
    '                Return buscarEstadoDocumento(lista.Valor("TipoDoc"))
    '            End If
    '        End If
    '    End If
    '    Return ESTADO_NORMAL
    'End Function

    Private Function buscarEstadoDocumento(tipodoc As String)
        Select Case UCase(tipodoc)
            Case infModLM.TIPODOCLM : Return ESTADO_NORMAL
            Case infModLM.TIPODOCCOT : Return ESTADO_COT
            Case infModLM.TIPODOCECF : Return ESTADO_ECF
        End Select
        Return ESTADO_NORMAL
    End Function




    Public Function buscarIdLinhaOriginal(doc As Objeto, idLinha As String, modulo As String) As String
        Dim lista As StdBELista
        Dim query As String
        Dim id As String = ""

        query = "SELECT L.IdLinhaOrigemCopia from " + doc.BaseDados + "..LINHASCOMPRAS L INNER JOIN " + doc.BaseDados + "..CABECCOMPRAS C ON L.idCabecCompras=C.Id INNER JOIN " + doc.BaseDados + "..CabecComprasStatus CCS On CCS.IdCabecCompras=C.id  WHERE  L.id='" + idLinha + "' and ModuloOrigemCopia='" + modulo + "' and L.id<>L.IdLinhaOrigemCopia"
        lista = bso.Consulta(query)

        If Not lista.NoFim Then
            id = buscarIdLinhaOriginal(doc, lista.Valor("IdLinhaOrigemCopia"), modulo)
            If id = "" Then
                Return lista.Valor("IdLinhaOrigemCopia")
            End If
        End If

        Return id
    End Function

    Public Function buscarIdLinhaFinal(doc As Objeto, idLinha As String, modulo As String) As String
        Dim lista As StdBELista
        Dim query As String
        Dim id As String = ""
        Dim idAux As String = ""

        query = "SELECT  L.id from " + doc.BaseDados + "..LINHASCOMPRAS L INNER JOIN " + doc.BaseDados + "..CABECCOMPRAS C ON L.idCabecCompras=C.Id INNER JOIN " + doc.BaseDados + "..CabecComprasStatus CCS On CCS.IdCabecCompras=C.id and CCS.Anulado=0 WHERE C.tipoDoc<>'" + infModLM.TIPODOCALT + "'  and L.IdLinhaOrigemCopia='" + idLinha + "' and ModuloOrigemCopia='" + modulo + "' and L.id<>L.IdLinhaOrigemCopia order by case  C.TipoDoc WHEN '" + infModLM.TIPODOCLM + "' THEN 0  WHEN '" + infModLM.TIPODOCCOT + "' THEN 1 WHEN '" + infModLM.TIPODOCECF + "' THEN 2 END asc"
        lista = bso.Consulta(query)

        While Not lista.NoFim
            idAux = buscarIdLinhaFinal(doc, lista.Valor("Id"), modulo)
            If idAux <> "" Then
                id = idAux
            Else
                id = lista.Valor("Id")
            End If
            lista.Seguinte()
        End While
        Return id
    End Function


    Public Function buscarEstadoIdLinha(doc As Objeto, idLinha As String, modulo As String) As String
        Dim lista As StdBELista
        Dim query As String
        Dim estado As String = ""
        Dim idAux As String = ""

        query = "SELECT C.Tipodoc from " + doc.BaseDados + "..LINHASCOMPRAS L INNER JOIN " + doc.BaseDados + "..CABECCOMPRAS C ON L.idCabecCompras=C.Id INNER JOIN " + doc.BaseDados + "..CabecComprasStatus  CCS On CCS.IdCabecCompras=C.id  WHERE  L.Id='" + idLinha + "'"
        lista = bso.Consulta(query)

        If Not lista.NoFim Then
            Return buscarEstadoDocumento(lista.Valor("Tipodoc"))
        End If
        Return ESTADO_NORMAL
    End Function



    ''' <summary>
    ''' Função que permite a actualização dos movimentos de stocks
    ''' </summary>
    ''' <param name="Records"></param>
    Public Sub gravarDocumentoStocks(doc As Objeto, idoc As String, Records As ReportRecords, mgrid As MotorGrelhas)

        Dim docEnt As GcpBEDocumentoCompra
        Dim record As ReportRecord
        Dim i As Integer
        Dim id As String
        Dim linha As GcpBELinhaDocumentoCompra
        Dim dtRow As DataRow
        Dim empresaOrig As String = ""
        Dim BDBOrig As String = ""
        Dim empresaAtual As String = ""
        Dim BDAtual As String = ""
        Try


            If idoc = "" Then
                Exit Sub
            End If

            empresaOrig = codEmpresa
            BDBOrig = "PRI" + empresaOrig


            setEmpresa(doc.Empresa)

            empresaAtual = codEmpresa
            BDAtual = "PRI" + codEmpresa


            If Not bso.Comercial.Compras.ExisteId(idoc) Then
                setEmpresa(empresaOrig)
                Exit Sub
            End If

            docEnt = bso.Comercial.Compras.EditaID(idoc)

            For Each record In Records
                dtRow = record.Tag
                id = dtRow("ID").ToString()
                id = "{" + id.ToUpper + "}"
                For Each linha In docEnt.Linhas
                    If linha.IdLinha.ToUpper = id Then
                        linha.Quantidade = buscarValorAtributoLinha(mgrid, record, "QUANTIDADE")
                        linha.DataStock = buscarValorAtributoLinha(mgrid, record, "CDU_DATARECEPCAO")
                        linha.CamposUtil("CDU_DATARECEPCAO").Valor = buscarValorAtributoLinha(mgrid, record, "CDU_DATARECEPCAO")
                        If linha.CamposUtil.Existe("CDU_DocRecp") Then
                            linha.CamposUtil("CDU_DocRecp").Valor = buscarValorAtributoLinha(mgrid, record, "CDU_DocRecp")
                        End If
                        Exit For
                    End If
                Next
            Next

            bso.Comercial.Compras.Actualiza(docEnt)
            setEmpresa(empresaOrig)
        Catch ex As Exception
            MsgBox("Erro na actualização dos movimentos de stocks --> gravarDocumentoStocks", MsgBoxStyle.Critical)
            setEmpresa(empresaOrig)
        End Try
    End Sub


    Private Function linhaTotalmenteSatisfeita(quantidade As Double, quantFalta As Double, quantEntregue As Double) As Boolean

        If quantEntregue + quantFalta >= quantidade Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub exportarFicheiroCSV(doc As GcpBEDocumentoCompra)
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim strQuery As String

        strQuery = "SELECT * FROM " + infModLM.CSVviewSQL + " WHERE id='" + doc.Id + "'"

        lista = consulta(strQuery)

        If Not lista Is Nothing Then
            dt = convertRecordSetToDataTable(lista.DataSet)


            Dim ficheiro As String
            ficheiro = formatarFicheiro(infModLM.CSVNomeFicheiro, doc)
            If System.IO.File.Exists(ficheiro) Then
                System.IO.File.Delete(ficheiro)
            End If
            DataTable2CSV(dt, ficheiro, ";", "id")
        End If
    End Sub


    Sub DataTable2CSV(ByVal table As DataTable, ByVal filename As String,
    ByVal sepChar As String, columnStep As String)
        Dim writer As System.IO.StreamWriter
        Try
            writer = New System.IO.StreamWriter(filename)

            ' first write a line with the columns name
            Dim sep As String = ""
            Dim builder As New System.Text.StringBuilder
            For Each col As DataColumn In table.Columns
                If UCase(col.ColumnName) <> UCase(columnStep) Then
                    builder.Append(sep).Append(col.ColumnName)
                    sep = sepChar
                End If

            Next
            writer.WriteLine(builder.ToString())

            ' then write all the rows
            For Each row As DataRow In table.Rows
                sep = ""
                builder = New System.Text.StringBuilder

                For Each col As DataColumn In table.Columns
                    If UCase(col.ColumnName) <> UCase(columnStep) Then
                        builder.Append(sep).Append(row(col.ColumnName))
                        sep = sepChar
                    End If
                Next
                writer.WriteLine(builder.ToString())
            Next
        Finally
            If Not writer Is Nothing Then writer.Close()
        End Try
    End Sub


    Private Function formatarFicheiro(formato As String, doc As GcpBEDocumentoCompra) As String
        formato = formato.Replace("{TIPODOC}", doc.Tipodoc)
        formato = formato.Replace("{SERIE}", doc.Serie)
        formato = formato.Replace("{NUMDOC}", CStr(doc.NumDoc))
        formato = formato.Replace("{ENTIDADE}", CStr(doc.Entidade))
        formato = formato.Replace("{FILIAL}", CStr(doc.Filial))
        Return formato
    End Function


    Public Function DocumentoJaConvertido(doc As Objeto) As Boolean
        Dim query As String
        Dim valor As String
        query = "SELECT count(*) FROM " + doc.BaseDados + "..LinhasCompras where idLinhaOrigemCopia in (Select id from LinhasCompras where idCabecCompras='" + doc.Id + "' )"
        valor = consultaValor(query)
        Return valor <> "0" And valor <> ""
    End Function

    Public Function buscarDocumentoCompraValorAtributo(id As String, campo As String) As String

        '  Dim lista As StdBELista

        'lista = bso.Consulta("SELECT Filial, TipoDoc, Serie, NumDoc FROM CabecCompras Where id='" + id + "'")
        'If Not lista.NoFim Then
        '    Return NuloToString(bso.Comercial.Compras.DaValorAtributo(lista.Valor("TipoDoc"), lista.Valor("NumDoc"), lista.Valor("Serie"), lista.Valor("Filial"), campo))
        'Else
        '    Return ""
        'End If
        Return NuloToString(bso.Comercial.Compras.DaValorAtributoID(id, campo))

    End Function


End Class
