﻿Public Class ArtigoComposicao
    Public Sub New()

    End Sub


    ''' <summary>
    ''' Artigo
    ''' </summary>
    ''' <remarks></remarks>
    Dim artigo_ As String
    Property Artigo() As String
        Get
            Artigo = artigo_
        End Get
        Set(ByVal value As String)
            artigo_ = value
        End Set
    End Property

    ''' <summary>
    ''' Quantidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim quantidade_ As Double
    Property Quantidade As Double
        Get
            Quantidade = quantidade_
        End Get
        Set(ByVal value As Double)
            quantidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' Descricao
    ''' </summary>
    ''' <remarks></remarks>
    Dim descricao_ As String
    Property Descricao As String
        Get
            Descricao = descricao_
        End Get
        Set(ByVal value As String)
            descricao_ = value
        End Set
    End Property


    ''' <summary>
    ''' Armazem
    ''' </summary>
    ''' <remarks></remarks>
    Dim armazem_ As String
    Property Armazem As String
        Get
            Armazem = armazem_
        End Get
        Set(ByVal value As String)
            armazem_ = value
        End Set
    End Property

    ''' <summary>
    ''' Localizacao
    ''' </summary>
    ''' <remarks></remarks>
    Dim localizacao_ As String
    Property Localizacao As String
        Get
            Localizacao = localizacao_
        End Get
        Set(ByVal value As String)
            localizacao_ = value
        End Set
    End Property


    ''' <summary>
    ''' Descricao
    ''' </summary>
    ''' <remarks></remarks>
    Dim lote_ As String
    Property Lote As String
        Get
            Lote = lote_
        End Get
        Set(ByVal value As String)
            lote_ = value
        End Set
    End Property


    ''' <summary>
    ''' Unidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim unidade_ As String
    Property Unidade As String
        Get
            Unidade = unidade_
        End Get
        Set(ByVal value As String)
            unidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' DataStock
    ''' </summary>
    ''' <remarks></remarks>
    Dim dataStock_ As String
    Property DataStock As DateTime
        Get
            DataStock = dataStock_
        End Get
        Set(ByVal value As DateTime)
            dataStock_ = value
        End Set
    End Property



End Class
