﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListaEncomendasECF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListaEncomendasECF))
        Me.gridArtigos = New AxXtremeReportControl.AxReportControl()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.AxImageManager1 = New AxXtremeCommandBars.AxImageManager()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        Me.lblTipoDoc = New System.Windows.Forms.Label()
        Me.lblNomeFornecedor = New System.Windows.Forms.Label()
        Me.btnRemoveRowLST = New System.Windows.Forms.Button()
        Me.btnAddRowLST = New System.Windows.Forms.Button()
        Me.TotalDocENC = New System.Windows.Forms.Label()
        Me.txtResponsavel = New System.Windows.Forms.TextBox()
        Me.lblResponsavel = New System.Windows.Forms.Label()
        Me.dtPicker = New AxXtremeSuiteControls.AxDateTimePicker()
        Me.btnAplicarTodos = New System.Windows.Forms.Button()
        Me.btnRepartirCustos = New System.Windows.Forms.Button()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxImageManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtPicker, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridArtigos
        '
        Me.gridArtigos.Location = New System.Drawing.Point(12, 117)
        Me.gridArtigos.Name = "gridArtigos"
        Me.gridArtigos.OcxState = CType(resources.GetObject("gridArtigos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigos.Size = New System.Drawing.Size(896, 428)
        Me.gridArtigos.TabIndex = 69
        Me.gridArtigos.Tag = "0"
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(884, 12)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 87
        '
        'AxImageManager1
        '
        Me.AxImageManager1.Enabled = True
        Me.AxImageManager1.Location = New System.Drawing.Point(854, 12)
        Me.AxImageManager1.Name = "AxImageManager1"
        Me.AxImageManager1.OcxState = CType(resources.GetObject("AxImageManager1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxImageManager1.Size = New System.Drawing.Size(24, 24)
        Me.AxImageManager1.TabIndex = 86
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(12, 1)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(243, 55)
        Me.CommandBarsFrame1.TabIndex = 85
        '
        'lblTipoDoc
        '
        Me.lblTipoDoc.AutoSize = True
        Me.lblTipoDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDoc.Location = New System.Drawing.Point(287, 12)
        Me.lblTipoDoc.Name = "lblTipoDoc"
        Me.lblTipoDoc.Size = New System.Drawing.Size(310, 24)
        Me.lblTipoDoc.TabIndex = 88
        Me.lblTipoDoc.Text = "Resumo Encomenda a Fornecedor"
        '
        'lblNomeFornecedor
        '
        Me.lblNomeFornecedor.AutoSize = True
        Me.lblNomeFornecedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNomeFornecedor.Location = New System.Drawing.Point(12, 71)
        Me.lblNomeFornecedor.Name = "lblNomeFornecedor"
        Me.lblNomeFornecedor.Size = New System.Drawing.Size(167, 24)
        Me.lblNomeFornecedor.TabIndex = 89
        Me.lblNomeFornecedor.Text = "Nome Fornecedor"
        '
        'btnRemoveRowLST
        '
        Me.btnRemoveRowLST.Image = CType(resources.GetObject("btnRemoveRowLST.Image"), System.Drawing.Image)
        Me.btnRemoveRowLST.Location = New System.Drawing.Point(822, 71)
        Me.btnRemoveRowLST.Name = "btnRemoveRowLST"
        Me.btnRemoveRowLST.Size = New System.Drawing.Size(40, 40)
        Me.btnRemoveRowLST.TabIndex = 91
        Me.btnRemoveRowLST.UseVisualStyleBackColor = True
        '
        'btnAddRowLST
        '
        Me.btnAddRowLST.Image = CType(resources.GetObject("btnAddRowLST.Image"), System.Drawing.Image)
        Me.btnAddRowLST.Location = New System.Drawing.Point(867, 71)
        Me.btnAddRowLST.Name = "btnAddRowLST"
        Me.btnAddRowLST.Size = New System.Drawing.Size(40, 40)
        Me.btnAddRowLST.TabIndex = 90
        Me.btnAddRowLST.UseVisualStyleBackColor = True
        '
        'TotalDocENC
        '
        Me.TotalDocENC.AutoSize = True
        Me.TotalDocENC.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalDocENC.Location = New System.Drawing.Point(597, 71)
        Me.TotalDocENC.Name = "TotalDocENC"
        Me.TotalDocENC.Size = New System.Drawing.Size(119, 20)
        Me.TotalDocENC.TabIndex = 92
        Me.TotalDocENC.Text = "Total: 300000"
        Me.TotalDocENC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtResponsavel
        '
        Me.txtResponsavel.Location = New System.Drawing.Point(777, 42)
        Me.txtResponsavel.Name = "txtResponsavel"
        Me.txtResponsavel.Size = New System.Drawing.Size(130, 20)
        Me.txtResponsavel.TabIndex = 93
        '
        'lblResponsavel
        '
        Me.lblResponsavel.AutoSize = True
        Me.lblResponsavel.Location = New System.Drawing.Point(707, 43)
        Me.lblResponsavel.Name = "lblResponsavel"
        Me.lblResponsavel.Size = New System.Drawing.Size(69, 13)
        Me.lblResponsavel.TabIndex = 94
        Me.lblResponsavel.Text = "Responsável"
        '
        'dtPicker
        '
        Me.dtPicker.Location = New System.Drawing.Point(433, 54)
        Me.dtPicker.Name = "dtPicker"
        Me.dtPicker.OcxState = CType(resources.GetObject("dtPicker.OcxState"), System.Windows.Forms.AxHost.State)
        Me.dtPicker.Size = New System.Drawing.Size(100, 22)
        Me.dtPicker.TabIndex = 97
        Me.dtPicker.Visible = False
        '
        'btnAplicarTodos
        '
        Me.btnAplicarTodos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.edit16
        Me.btnAplicarTodos.Location = New System.Drawing.Point(765, 71)
        Me.btnAplicarTodos.Name = "btnAplicarTodos"
        Me.btnAplicarTodos.Size = New System.Drawing.Size(39, 40)
        Me.btnAplicarTodos.TabIndex = 102
        Me.btnAplicarTodos.UseVisualStyleBackColor = True
        '
        'btnRepartirCustos
        '
        Me.btnRepartirCustos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checklist64
        Me.btnRepartirCustos.Location = New System.Drawing.Point(722, 71)
        Me.btnRepartirCustos.Name = "btnRepartirCustos"
        Me.btnRepartirCustos.Size = New System.Drawing.Size(37, 40)
        Me.btnRepartirCustos.TabIndex = 103
        Me.btnRepartirCustos.UseVisualStyleBackColor = True
        '
        'FrmListaEncomendasECF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 553)
        Me.Controls.Add(Me.btnAplicarTodos)
        Me.Controls.Add(Me.btnRepartirCustos)
        Me.Controls.Add(Me.dtPicker)
        Me.Controls.Add(Me.lblResponsavel)
        Me.Controls.Add(Me.txtResponsavel)
        Me.Controls.Add(Me.TotalDocENC)
        Me.Controls.Add(Me.btnRemoveRowLST)
        Me.Controls.Add(Me.btnAddRowLST)
        Me.Controls.Add(Me.lblNomeFornecedor)
        Me.Controls.Add(Me.lblTipoDoc)
        Me.Controls.Add(Me.gridArtigos)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.AxImageManager1)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmListaEncomendasECF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Resumo Encomenda Fornecedor"
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxImageManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtPicker, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gridArtigos As AxXtremeReportControl.AxReportControl
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents AxImageManager1 As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lblTipoDoc As Label
    Friend WithEvents lblNomeFornecedor As Label
    Friend WithEvents btnRemoveRowLST As Button
    Friend WithEvents btnAddRowLST As Button
    Friend WithEvents TotalDocENC As Label
    Friend WithEvents txtResponsavel As TextBox
    Friend WithEvents lblResponsavel As Label
    Friend WithEvents dtPicker As AxXtremeSuiteControls.AxDateTimePicker
    Friend WithEvents btnAplicarTodos As Button
    Friend WithEvents btnRepartirCustos As Button
End Class
