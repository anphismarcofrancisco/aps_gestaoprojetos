﻿Imports PlatAPSNET

Public Class email

    Dim origem As String
    Dim emailCC As String
    Dim emailBCC As String
    Dim ficheiroINI As IniFile
    Dim entrada As String
    Public Sub New(ByVal entrada_ As String)

        entrada = entrada_
        Dim ficheiroConfiguracao As Configuracao = Configuracao.GetInstance

        ficheiroINI = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)

        origem = Trim(Replace(ficheiroINI.GetString("APLICACAO", "FROM", "marcofrancisco@anphis.pt"), vbTab, ""))
        emailCC = Trim(Replace(ficheiroINI.GetString(entrada, "CC", ""), vbTab, ""))
        emailBCC = Trim(Replace(ficheiroINI.GetString(entrada, "BCC", ""), vbTab, ""))

    End Sub

    Protected Overridable Function getFicheiroINI() As IniFile
        Return ficheiroINI
    End Function

    Protected Overridable Function getEntrada() As String
        Return entrada
    End Function


    Public Overridable Function enviar(ByVal enderecoDestino As String, ByVal assunto As String, ByVal corpo As String, ByVal corpoHTML As String) As String
        Return ""
    End Function


    Public Overridable Sub adicionarAnexo(ByVal ficheiro As String, ByVal primeiravez As Boolean)

    End Sub

    Protected Overridable Function getEmailOrigem() As String
        Return origem
    End Function

    Protected Overridable Function getEmailCC() As String
        Return emailCC
    End Function

    Protected Overridable Function getEmailBCC() As String
        Return emailBCC
    End Function

End Class
