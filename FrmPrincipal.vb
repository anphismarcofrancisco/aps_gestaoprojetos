﻿Imports Interop.StdBE900
Imports PlatAPSNET
Imports Microsoft.VisualBasic


Public Class FrmPrincipal

    Const MSGBOX1 = "Tem de seleccionar um Centro Trabalho"
    Const MSGBOX2 = "Tem de seleccionar um Molde"
    Const MSGBOX3 = "Tem de seleccionar uma Peça"
    Const MSGBOX4 = "Tem de seleccionar uma Operação"

    Dim idLinha_ As List(Of String)

    Dim htCores As Hashtable
    '   Dim ColorNames As New List(Of String)
    '  Dim ColorName As System.Type = GetType(System.Drawing.Color)
    '  Dim ColorPropInfo As System.Reflection.PropertyInfo() = ColorName.GetProperties()
    Dim Rand As New Random

    Dim aplicacaoInicializada As Boolean


    Property idLinha() As List(Of String)
        Get
            idLinha = idLinha_
        End Get
        Set(ByVal value As List(Of String))
            idLinha_ = value
        End Set
    End Property


    Dim idRegisto_ As String
    Property idRegisto() As String
        Get
            idRegisto = idRegisto_
        End Get
        Set(ByVal value As String)
            idRegisto_ = value
        End Set
    End Property


    Private Sub FrmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'derivado as casas decimais
        Application.CurrentCulture = New System.Globalization.CultureInfo("PT-pt")

        Application.CurrentCulture.NumberFormat.NumberDecimalSeparator = ","
        Application.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = ","
        Application.CurrentCulture.NumberFormat.CurrencyGroupSeparator = "."
        Application.CurrentCulture.NumberFormat.NumberGroupSeparator = "."

        aplicacaoInicializada = False


        Dim fileINI As String
        Dim ficheiroConfiguracao As Configuracao
        fileINI = Application.StartupPath + "\\CONFIG.ini"
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)
        Dim m As MotorRP
        m = MotorRP.GetInstance()
        inicializarForm()
        buscarListaFuncionariosPermissoes()

        idLinha_ = New List(Of String)
        idRegisto_ = ""
        aplicacaoInicializada = True


        If m.daValorStringIni("DOCUMENTORP", "CORES", "1") = "-1" Then
            lstOperacao.DrawMode = DrawMode.Normal
            lstPecas.DrawMode = DrawMode.Normal
            lstMoldes.DrawMode = DrawMode.Normal
            'lstPostos.DrawMode = DrawMode.Normal
        End If

    End Sub

    Private Sub inicializarForm()



        inicializarPostos()

        inicializarMoldes()

        inicializarPecas(getMoldeActivo)

        inicializarOperacoes(getPostoActivo)


        Dim m As MotorRP
        m = MotorRP.GetInstance

        lblNomeCliente.Visible = m.MostraCliente


        lstPostos.Width = m.daValorStringIni("DOCUMENTORP", "TAMANHOPOSTOS", "154")
        lstMoldes.Width = m.daValorStringIni("DOCUMENTORP", "TAMANHOMOLDES", "200")
        lstPecas.Width = m.daValorStringIni("DOCUMENTORP", "TAMANHOPECAS", "253")
        lstOperacao.Width = m.daValorStringIni("DOCUMENTORP", "TAMANHOOPERACOES", "315")

        btnIniciar.Visible = m.daValorStringIni("DOCUMENTORP", "MOSTRAPLAY", "1") = "1"

        lstMoldes.Left = lstPostos.Left + lstPostos.Width + 10
        lstPecas.Left = lstMoldes.Left + lstMoldes.Width + 10
        lstOperacao.Left = lstPecas.Left + lstPecas.Width + 10

        txtPesqPosto.Width = lstPostos.Width
        txtPesqMolde.Width = lstMoldes.Width - 43
        txtPesqPecas.Width = lstPecas.Width - 43
        txtPesqOperacao.Width = lstOperacao.Width

        txtPesqPosto.Left = lstPostos.Left
        txtPesqMolde.Left = lstMoldes.Left
        txtPesqPecas.Left = lstPecas.Left
        txtPesqOperacao.Left = lstOperacao.Left


        lblPostos.Left = lstPostos.Left
        lblMoldes.Left = lstMoldes.Left
        lblPecas.Left = lstPecas.Left
        lblOperacao.Left = lstOperacao.Left



        btnAdd.Visible = m.getFuncionarioAddPecas
        ' If m.getFuncionarioAdministrador Then

        If Not m.getFuncionarioAddPecas Then
            btnAdd.Visible = False
            btnAddP.Visible = False
            txtPesqPecas.Width = lstPecas.Width
            txtPesqMolde.Width = lstMoldes.Width
        Else
            btnAdd.Visible = True
            btnAddP.Visible = True
            btnAdd.Left = txtPesqPecas.Left + lstPecas.Width - btnAdd.Width
            btnAddP.Left = txtPesqMolde.Left + lstMoldes.Width - btnAddP.Width
        End If




        idLinha_ = New List(Of String)
        idRegisto_ = ""


        btnEditar.Left = lstOperacao.Left + lstOperacao.Width + 10
        cmbFuncionarios.Left = btnEditar.Left
        btnIniciar.Left = btnEditar.Left
        btnParar.Left = btnEditar.Left
        btnSair.Left = btnEditar.Left
        Me.Width = btnSair.Left + btnSair.Width + 30

        btnGestConf.Visible = m.getFuncionarioAdministrador

        btnGestConf.Left = btnEditar.Left + btnEditar.Width - btnGestConf.Width
        'ToolTip.SetToolTip(btnEditar, " ")
        'ToolTipIniciar.SetToolTip(btnIniciar, " ")
        'ToolTipTerminar.SetToolTip(btnParar, " ")
        'ToolTipSair.SetToolTip(btnSair, " ")
        'ToolTipAdd.SetToolTip(btnAdd, " ")
    End Sub

    Private Sub buscarListaFuncionariosPermissoes()
        Dim dt As DataTable
        Dim m As MotorRP
        m = MotorRP.GetInstance
        dt = m.daListaFuncionariosPermissoes(False)
        cmbFuncionarios.DataSource = dt
        cmbFuncionarios.ValueMember = "Codigo"
        cmbFuncionarios.DisplayMember = "Nome"
        cmbFuncionarios.SelectedValue = m.Funcionario
        cmbFuncionarios.Visible = m.getFuncionarioAdministrador()
    End Sub

    Private Sub inicializarPostos()

        lstPostos.Tag = ""


        Dim m As MotorRP
        Dim lista As StdBELista
        m = MotorRP.GetInstance()
        lstPostos.SelectedIndex = -1
        lista = m.daListaPostos(m.Funcionario, txtPesqPosto.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)

        htCores = New Hashtable

        lstPostos.Tag = ""
        lstPostos.ValueMember = "Codigo"
        lstPostos.DisplayMember = "Nome"
        lstPostos.DataSource = dt
        lstPostos.ClearSelected()

        Dim row As DataRow

        For Each row In dt.Rows
            htCores.Add(row("Codigo"), Color.FromArgb(200, Rand.Next(0, 256), Rand.Next(0, 256)))
        Next



        If m.MultiplaSeleccao Then
            lstPostos.SelectionMode = SelectionMode.MultiSimple
        Else
            lstPostos.SelectionMode = SelectionMode.One
        End If


    End Sub

    Private Sub inicializarMoldes()

        lstMoldes.Tag = ""


        Dim m As MotorRP
        Dim lista As StdBELista
        m = MotorRP.GetInstance()
        lstMoldes.SelectedIndex = -1
        lista = m.daListaMoldes(txtPesqMolde.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)

        lstMoldes.ValueMember = "Codigo"
        lstMoldes.DisplayMember = "Nome"
        lstMoldes.DataSource = dt

        lstMoldes.SelectionMode = SelectionMode.One

    End Sub
    Private Sub inicializarPecas(molde As String)


        lstPecas.Tag = ""


        Dim m As MotorRP
        Dim lista As StdBELista
        m = MotorRP.GetInstance()
        lstPecas.SelectedIndex = -1
        lista = m.daListaPecas(molde, txtPesqPecas.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        ' lstPecas.MultiColumn = True
        ' lstPecas.ColumnWidth = 10


        lstPecas.ValueMember = "Codigo"
        lstPecas.DisplayMember = "Nome"
        lstPecas.DataSource = dt

        lstPecas.SelectionMode = SelectionMode.One

    End Sub

    Private Sub inicializarOperacoes(posto As String)

        lstOperacao.Tag = ""


        Dim m As MotorRP
        Dim lista As StdBELista
        lstOperacao.SelectedIndex = -1
        m = MotorRP.GetInstance()
        lista = m.daListaOperacoes(posto, m.Funcionario, txtPesqOperacao.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)

        lstOperacao.ValueMember = "Codigo"
        lstOperacao.DisplayMember = "Nome"
        lstOperacao.DataSource = dt

        lstOperacao.SelectionMode = SelectionMode.One
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub txtPesqMolde_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqMolde.Change
        inicializarMoldes()
    End Sub

    Private Sub txtPesqPecas_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqPecas.Change
        inicializarPecas(getMoldeActivo)
    End Sub

    Private Sub txtPesqOperacao_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqOperacao.Change
        inicializarOperacoes(getPostoActivo())
    End Sub


    Private Sub txtPesqPosto_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqPosto.Change
        inicializarPostos()
    End Sub


    Private Sub btnIniciar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIniciar.Click

        If lstPostos.SelectedIndex = -1 Then
            MsgBox(MSGBOX1, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstMoldes.SelectedIndex = -1 Then
            MsgBox(MSGBOX2, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstPecas.SelectedIndex = -1 Then
            MsgBox(MSGBOX3, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstOperacao.SelectedIndex = -1 Then
            MsgBox(MSGBOX4, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Dim m As MotorRP
        m = MotorRP.GetInstance
        Dim erro As String = ""
        Dim rowCT As DataRowView
        Dim rowMolde As DataRowView
        Dim rowPeca As DataRowView
        Dim rowOperacao As DataRowView

        '  rowCT = lstPostos.SelectedItem
        rowMolde = lstMoldes.SelectedItem
        rowPeca = lstPecas.SelectedItem
        rowOperacao = lstOperacao.SelectedItem
        Dim idPonto As String
        idPonto = m.consultaValor("SELECT NEWID()")
        Dim postos As List(Of String) = New List(Of String)
        For Each rowCT In lstPostos.SelectedItems
            postos.Add(rowCT("Nome"))
        Next
        For Each rowCT In lstPostos.SelectedItems
            erro += m.iniciarOperacao(rowCT("Codigo"), rowCT("Nome"), rowMolde("Codigo"), rowPeca("Codigo"), rowOperacao("Codigo"), idPonto, postos)
        Next


        If erro <> "" Then
            MsgBox(erro, MsgBoxStyle.Exclamation)
        Else
            MsgBox("Obra Inicializada com sucesso", MsgBoxStyle.Information)
            lstPostos.Refresh()
            lstMoldes.Refresh()
            lstPecas.Refresh()
            lstOperacao.Refresh()
        End If
    End Sub



    Private Sub lstPostos_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lstPostos.DrawItem


        If e.Index = -1 Then Exit Sub
        e.DrawBackground()
        Dim myBrush As Brush
        Dim myBackColor As Color = Color.White
        Dim myForeColor As Color = Color.Black
        Dim myFont As Font = e.Font

        Dim m As MotorRP
        m = MotorRP.GetInstance

        Dim row As DataRowView
        row = CType(sender, ListBox).Items(e.Index)
        Dim postos As List(Of String) = New List(Of String)
        postos.Add(row("Codigo"))
        If m.obraOperacaoJaIniciada(row("Codigo"), postos, "", "", "", "", False) <> "" Then
            myForeColor = htCores(row("Codigo"))
        Else
            myForeColor = Color.Black
        End If

        If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
            myBackColor = e.BackColor
            If myForeColor = Color.Black Then
                myForeColor = Color.White
            Else
                myForeColor = Color.Lime
            End If
        End If
        'Checking the property of item in DataSet     
        'If isDisabled(lstSitename.Items(e.Index)) Then
        '    myFont = New Font(Me.Font, FontStyle.Strikeout)
        '    myForeColor = Color.Maroon
        'End If
        myBrush = New SolidBrush(myBackColor)
        e.Graphics.FillRectangle(myBrush, e.Bounds)
        myBrush = New SolidBrush(myForeColor)



        e.Graphics.DrawString(row("Nome"), e.Font, myBrush, e.Bounds, StringFormat.GenericDefault)

        ' e.Graphics.DrawString(drv("Nome").ToString, myFont, myBrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        e.DrawFocusRectangle()

        '     //
        '// Draw the background of the ListBox control for each item.
        '// Create a new Brush and initialize to a Black colored brush
        '// by default.
        '//
        ' e.DrawBackground()

        lstPostos.Tag = "1"

    End Sub

    Private Sub lstMoldes_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lstMoldes.DrawItem

        If e.Index = -1 Then Exit Sub
        e.DrawBackground()
        Dim myBrush As Brush
        Dim myBackColor As Color = Color.White
        Dim myForeColor As Color = Color.Black
        Dim myFont As Font = e.Font

        Dim m As MotorRP
        m = MotorRP.GetInstance

        Dim row As DataRowView
        row = CType(sender, ListBox).Items(e.Index)
        Dim posto As String

        posto = m.daPostoSeleccionadoMolde(row("codigo"))

        If posto <> "" Then
            If htCores.ContainsKey(posto) Then
                myForeColor = htCores(posto)
            Else
                myForeColor = Color.Gray
            End If
        Else
                myForeColor = Color.Black
        End If


        If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
            myBackColor = e.BackColor
            If myForeColor = Color.Black Then
                myForeColor = Color.White
            Else
                myForeColor = Color.Lime
            End If
        End If
        'Checking the property of item in DataSet     
        'If isDisabled(lstSitename.Items(e.Index)) Then
        '    myFont = New Font(Me.Font, FontStyle.Strikeout)
        '    myForeColor = Color.Maroon
        'End If
        myBrush = New SolidBrush(myBackColor)
        e.Graphics.FillRectangle(myBrush, e.Bounds)
        myBrush = New SolidBrush(myForeColor)

        e.Graphics.DrawString(row("Nome"), e.Font, myBrush, e.Bounds, StringFormat.GenericDefault)

        ' e.Graphics.DrawString(drv("Nome").ToString, myFont, myBrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        e.DrawFocusRectangle()

        lstMoldes.Tag = "1"
    End Sub



    Private Sub lstPecas_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lstPecas.DrawItem

        If e.Index = -1 Then Exit Sub
        e.DrawBackground()
        Dim myBrush As Brush
        Dim myBackColor As Color = Color.White
        Dim myForeColor As Color = Color.Black
        Dim myFont As Font = e.Font

        Dim m As MotorRP
        m = MotorRP.GetInstance

        Dim row As DataRowView
        row = CType(sender, ListBox).Items(e.Index)
        Dim posto As String

        posto = m.daPostoSeleccionadoPeca(row("Codigo"))

        If posto <> "" Then
            If htCores.ContainsKey(posto) Then
                myForeColor = htCores(posto)
            Else
                myForeColor = Color.Gray
            End If
        Else
            myForeColor = Color.Black
        End If

        If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
            myBackColor = e.BackColor
            If myForeColor = Color.Black Then
                myForeColor = Color.White
            Else
                myForeColor = Color.Lime
            End If
        End If

        'Checking the property of item in DataSet     
        'If isDisabled(lstSitename.Items(e.Index)) Then
        '    myFont = New Font(Me.Font, FontStyle.Strikeout)
        '    myForeColor = Color.Maroon
        'End If

        myBrush = New SolidBrush(myBackColor)
        e.Graphics.FillRectangle(myBrush, e.Bounds)
        myBrush = New SolidBrush(myForeColor)


        e.Graphics.DrawString(row("Nome").ToString, myFont, myBrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        e.DrawFocusRectangle()



    End Sub

    Private Sub lstOperacao_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lstOperacao.DrawItem

        If e.Index = -1 Then Exit Sub
        e.DrawBackground()
        Dim myBrush As Brush
        Dim myBackColor As Color = Color.White
        Dim myForeColor As Color = Color.Black
        Dim myFont As Font = e.Font

        Dim m As MotorRP
        m = MotorRP.GetInstance

        Dim row As DataRowView
        row = CType(sender, ListBox).Items(e.Index)
        Dim posto As String

        posto = m.daPostoSeleccionadoOperacao(row("Codigo"))

        If posto <> "" Then
            If htCores.ContainsKey(posto) Then
                myForeColor = htCores(posto)
            Else
                myForeColor = Color.Gray
            End If
        Else
            myForeColor = Color.Black
        End If

        If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
            myBackColor = e.BackColor
            If myForeColor = Color.Black Then
                myForeColor = Color.White
            Else
                myForeColor = Color.Lime
            End If
        End If
        'Checking the property of item in DataSet     
        'If isDisabled(lstSitename.Items(e.Index)) Then
        '    myFont = New Font(Me.Font, FontStyle.Strikeout)
        '    myForeColor = Color.Maroon
        'End If
        myBrush = New SolidBrush(myBackColor)
        e.Graphics.FillRectangle(myBrush, e.Bounds)
        myBrush = New SolidBrush(myForeColor)

        e.Graphics.DrawString(row("Nome"), e.Font, myBrush, e.Bounds, StringFormat.GenericDefault)

        ' e.Graphics.DrawString(drv("Nome").ToString, myFont, myBrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        e.DrawFocusRectangle()

        lstOperacao.Tag = ""
    End Sub

    Private Sub btnParar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParar.Click
        If lstPostos.SelectedIndex = -1 Then
            MsgBox(MSGBOX1, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstMoldes.SelectedIndex = -1 Then
            MsgBox(MSGBOX2, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstPecas.SelectedIndex = -1 Then
            MsgBox(MSGBOX3, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If lstOperacao.SelectedIndex = -1 Then
            MsgBox(MSGBOX4, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Dim m As MotorRP
        m = MotorRP.GetInstance
        Dim erro As String
        Dim rowCT As DataRowView
        Dim rowMolde As DataRowView
        Dim rowPeca As DataRowView
        Dim rowOperacao As DataRowView


        rowCT = lstPostos.SelectedItem
        rowMolde = lstMoldes.SelectedItem
        rowPeca = lstPecas.SelectedItem
        rowOperacao = lstOperacao.SelectedItem
        Dim operador As String
        Dim msg As String
        Dim postos As String
        Dim listapostos As List(Of String) = New List(Of String)

        operador = ""

        If idRegisto_ = "" Then
            msg = ""
            postos = ""
            For Each rowCT In lstPostos.SelectedItems
                listapostos.Add(rowCT("Codigo"))
            Next
            msg = m.obraOperacaoJaIniciada(rowCT("Codigo"), listapostos, rowMolde("Codigo"), rowPeca("Codigo"), rowOperacao("Codigo"), operador, True)
            'IMPLEMENTACAO 23-10-2014 Normolde
            'If erro <> "" Then
            '    msg += erro
            '    If postos <> "" Then
            '        postos += ","
            '    End If
            '    postos += rowCT("Nome")
            'End If


            Dim podeTerminarObra As Boolean
            podeTerminarObra = True

            If msg <> "" Then
                If operador = m.Funcionario Then
                    erro = ""
                    postos = ""
                    msg = ""
                    For Each rowCT In lstPostos.SelectedItems
                        msg += m.obraPodeSerTerminada(rowCT("Codigo"), rowMolde("Codigo"), rowPeca("Codigo"), rowOperacao("Codigo"), True)

                        ''IMPLEMENTACAO 23-10-2014 Normolde
                        'If erro <> "" Then
                        '    msg += erro
                        '    If postos <> "" Then
                        '        postos += ","
                        '    End If
                        '    postos += rowCT("Nome")
                        'End If
                    Next
                    If msg <> "" Then
                        MsgBox("Existem erros no terminado dos Pontos: " + msg, MsgBoxStyle.Exclamation)
                        Exit Sub
                    Else
                        podeTerminarObra = True
                    End If
                Else
                    MsgBox("Existem erros no terminado dos Pontos: " + vbCrLf + vbCrLf + msg, MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
            End If
        End If

        Dim botaoSeleccionado As Integer
        Dim centrosTrabalho As List(Of String) = New List(Of String)
        Dim artigos As List(Of String) = New List(Of String)

        For Each rowCT In lstPostos.SelectedItems

            centrosTrabalho.Add(rowCT("Nome"))
            artigos.Add(rowCT("Codigo"))

        Next
        Dim msgDocumento As String
        msgDocumento = ""
        botaoSeleccionado = m.TerminarOperacao(centrosTrabalho, artigos, rowMolde("Codigo"), rowPeca("Codigo"), rowOperacao("Codigo"), idRegisto, idLinha_, msgDocumento)

        If botaoSeleccionado <> 3 Then

            idLinha = New List(Of String)
            idLinha_ = New List(Of String)

            idRegisto = ""
            lblREdicao.Visible = False
            btnIniciar.Enabled = True

            If botaoSeleccionado = 1 Then
                Dim texto As String
                texto = "Registo Ponto terminado com sucesso"
                texto += vbCrLf + vbCrLf + msgDocumento
                texto += vbCrLf + vbCrLf + "Deseja inserir mais pontos?"
                If MsgBox(texto, MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Me.Close()
                Else
                    inicializarForm()
                End If
            Else
                inicializarForm()
            End If
        End If



        'If erro <> "" Then
        '    MsgBox(erro, MsgBoxStyle.Exclamation)
        'Else

        'End If
    End Sub


    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Dim fEditar As FrmEditarRP
        fEditar = New FrmEditarRP
        fEditar.ShowDialog()
        idLinha = New List(Of String)()
        If fEditar.clicouConfirmar Then
            Dim m As MotorRP
            Dim id As String
            m = MotorRP.GetInstance
            Dim lista As StdBELista
            id = fEditar.getIdLinha
            lista = m.consulta("SELECT LinhasInternos.id as CDU_IdLinha,APS_GP_Postos.Nome as CDU_Posto,COP_Obras.Codigo as CDU_Molde,  CDU_ObraN1 as CDU_Peca,CDU_ObraN2 as CDU_Operacao FROM LinhasInternos INNER JOIN APS_GP_Postos ON APS_GP_Postos.Codigo=LinhasInternos.Artigo INNER JOIN COP_Obras ON COP_Obras.Id=LinhasInternos.ObraId WHERE IdLinhaOrigemCopia='" + id + "'")
            idRegisto = ""
            idLinha_ = New List(Of String)
            If Not lista.NoFim Then
                lstPostos.ClearSelected()
                While Not lista.NoFim
                    idRegisto = id
                    idLinha_.Add(lista.Valor("CDU_IdLinha"))
                    seleccionarListbox(lstPostos, lista.Valor("CDU_Posto"), "Nome", False)
                    seleccionarListbox(lstMoldes, lista.Valor("CDU_Molde"))
                    seleccionarListbox(lstPecas, lista.Valor("CDU_Peca"))
                    seleccionarListbox(lstOperacao, lista.Valor("CDU_Operacao"))
                    lblREdicao.Visible = True
                    btnIniciar.Enabled = False
                    lista.Seguinte()
                End While
            End If
        End If
    End Sub

    ''' <summary>
    ''' função que permite a seleccção automática das listbox 
    ''' </summary>
    ''' <param name="lstBox"></param>
    ''' <param name="valor"></param>
    ''' <remarks></remarks>
    Private Sub seleccionarListbox(ByVal lstBox As ListBox, ByVal valor As String, Optional ByVal campo As String = "Codigo", Optional clearSelected As Boolean = True)
        Dim i As Integer
        Dim drv As DataRowView
        If clearSelected Then lstBox.ClearSelected()
        For i = 0 To lstBox.Items.Count - 1
            drv = lstBox.Items(i)
            If drv.Item(campo) = valor Then
                lstBox.SelectedItems.Add(drv)
                Exit For
            End If
        Next
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click



        Dim f As FrmAddPecas
        f = New FrmAddPecas
        f.ShowDialog()
        If f.BotaoSeleccionado = 1 Then
            inicializarPecas(getMoldeActivo)
            seleccionarListbox(lstPecas, f.txtPeca.Text.Trim)
        End If
    End Sub




    Private Sub cmbFuncionarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFuncionarios.SelectedIndexChanged
        If aplicacaoInicializada Then
            Dim m As MotorRP
            m = MotorRP.GetInstance()
            m.Funcionario = cmbFuncionarios.SelectedValue
            inicializarForm()
            idLinha_ = New List(Of String)
            idRegisto_ = ""
        End If
    End Sub

    Private Sub lstMoldes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstMoldes.SelectedIndexChanged
        Dim m As MotorRP
        m = MotorRP.GetInstance()
        Dim projecto As String
        '   Dim row As DataRowView
        '  row = lstMoldes.SelectedValue
        projecto = lstMoldes.SelectedValue 'row("Codigo")
        lblNomeCliente.Text = "Cliente: " + m.daNomeClienteProjecto(projecto)

        If m.integraLstaMateriais Then
            inicializarPecas(getMoldeActivo)
        End If
    End Sub

    Private Sub lstPostos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstPostos.SelectedIndexChanged

        Dim m As MotorRP
        m = MotorRP.GetInstance()
        Dim valor As String
        valor = m.consultaValor("SELECT count(*) FROM TDU_PostosOperacoes")
        If valor <> "0" Then inicializarOperacoes(getIDPostoActivo(getPostoActivo))
    End Sub

    Public Function getIDPostoActivo(codigo As String) As String
        Dim m As MotorRP
        m = MotorRP.GetInstance()
        Dim valor As String
        valor = m.consultaValor("SELECT CDU_Posto FROM APS_GP_Postos WHERE Codigo='" + codigo + "'")
        Return valor
    End Function





    Public Function getPostoActivo() As String
        If IsNothing(lstPostos.SelectedValue) Then
            Return ""
        Else
            Return lstPostos.SelectedValue
        End If
    End Function

    Public Function getMoldeActivo() As String
        If IsNothing(lstMoldes.SelectedValue) Then
            Return ""
        Else
            Return lstMoldes.SelectedValue
        End If
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Dim frm As FrmConfPontos
        frm = New FrmConfPontos

        frm.ShowDialog()
    End Sub

    Private Sub btnGestConf_Click(sender As Object, e As EventArgs) Handles btnGestConf.Click
        Dim f As FrmConfPontos
        f = New FrmConfPontos
        f.ShowDialog()
        inicializarPecas(getMoldeActivo)
    End Sub

    Private Sub btnAddP_Click(sender As Object, e As EventArgs) Handles btnAddP.Click


        Dim f As FrmAddProjetos
        f = New FrmAddProjetos
        f.ShowDialog()
        If f.BotaoSeleccionado = 1 Then
            inicializarMoldes()
            seleccionarListbox(lstMoldes, f.txtProjeto.Text.Trim)
        End If
    End Sub
End Class
