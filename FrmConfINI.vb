﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports PlatAPSNET
Imports Interop.StdBE900


Public Class FrmConfINI

    Dim ficheiroConfiguracao As Configuracao
    Dim ini As IniFile
    Dim seg As Seguranca
    Dim lic As Licenca

    Private Sub FrmConfINI_Load(sender As Object, e As EventArgs) Handles MyBase.Load

       

        Dim fileINI As String
        Dim iniFile As String
        iniFile = "CONFIG.ini"
        fileINI = Application.StartupPath + "\\" + iniFile
        ficheiroConfiguracao = Configuracao.GetInstance(fileINI)

        ficheiroConfiguracao = Configuracao.GetInstance()
        ini = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao())
        seg = New Seguranca("p/TAM0BIyThZcD+H2K17flvuzgA=")

        Dim m As Motor
        m = Motor.GetInstance
        m.AplicacaoInicializada = True

        iniciarToolBox()
        carregarDados()

    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"

            Control = .Add(xtpControlButton, 2, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Public Sub carregarDados()

        carregarLicenca()

        carregarERP()

        If lic.possuiModulo("R") Then
            carregarModuloRegistoPontos()
        Else
            TabControl.TabPages.Remove(TabRegistoPontos)
        End If

        If lic.possuiModulo("P") Then
            carregarModuloPontosClientes()
        Else
            TabControl.TabPages.Remove(TabPontos)
        End If

        If lic.possuiModulo("E") Then
            carregarModuloVendasCompras()
        Else
            TabControl.TabPages.Remove(TabVendasEncomendas)
        End If

        If lic.possuiModulo("S") Then
            carregarModuloStocks()
        Else
            TabControl.TabPages.Remove(TabStocks)
        End If

        If lic.possuiModulo("M") Then
            carregarModuloListaMateriais()
        Else
            TabControl.TabPages.Remove(TabListaMateriais)
        End If

    End Sub

    Public Sub carregarERP()
  

        Dim entrada As String
        entrada = "ERP"
        ERPcmbTipoPlataforma.Items.Add("Empresarial")
        ERPcmbTipoPlataforma.Items.Add("Profissional")
        ERPcmbTipoPlataforma.Items.Add("Starter")
        ERPcmbTipoPlataforma.SelectedIndex = ini.GetString(entrada, "TIPOPLAT", "1")

        ERPtxtInstancia.Text = ini.GetString(entrada, "INSTANCIA", "Default")
        ERPtxtAplicacao.Text = ini.GetString(entrada, "APLICACAO", "GCP")

        ERPtxtUtilizadorERP.Text = ini.GetString(entrada, "UTILIZADOR", "")
        ERPtxtPasswordERP.Text = seg.decifrarStringTripleDES(ini.GetString(entrada, "PASSWORD", ""))
        ERPtxtPasswordERP.Tag = ini.GetString(entrada, "PASSWORD", "")

        '  ERPcmbEmpresaDefault.SelectedText = ini.GetString(entrada, "EMPRESA", "")
        '  ERPcmbEmpresaDefault.SelectedText = "ITJ"

        ERPcmbEmpresaDefault.SelectedItem = ini.GetString(entrada, "EMPRESA", "")


        ERPLstBoxEmpresas.SelectedItems.Clear()
        ERPLstBoxEmpresas.CheckOnClick = True

        For i = 0 To ERPLstBoxEmpresas.CheckedItems.Count - 1
            ERPLstBoxEmpresas.SetItemChecked(i, False)
        Next

        Dim listaempresas() As String
        Dim empresas As String
        Dim empresa As String
        empresas = ini.GetString(entrada, "EMPRESAS", "")
        listaempresas = empresas.Split(",")
        For i = 0 To listaempresas.Length - 1
            Dim index As Integer
            empresa = Replace(listaempresas(i), "'", "")
            index = getIndexComboBox(ERPLstBoxEmpresas, empresa)
            If index <> -1 Then
                ERPLstBoxEmpresas.SetItemChecked(index, True)
            End If

        Next

      
        ERPcmbModulo.SelectedItem = buscarModulo(ini.GetString(entrada, "MODULO", ""))



        Dim m As Motor
        m = Motor.GetInstance

        Dim valor As String

        valor = ini.GetString(entrada, "ART_MAT_LIM", "")
        ERPtxtArtMaterial.Text = valor
        valor = m.consultaValor("SELECT Descricao FROM Artigo WHERE Artigo='" + valor + "'")
        ERPtxtArtMaterialDescricao.Text = valor

        valor = ini.GetString(entrada, "ARMAZEMDEFAULT", "")
        ERPtxtArmazem.Text = valor
        valor = m.consultaValor("SELECT Descricao FROM Armazens WHERE Armazem='" + valor + "'")
        ERPtxtArmazemDescricao.Text = valor

    End Sub
    Public Function getIndexComboBox(ckbLst As CheckedListBox, value As String) As Integer
        For i = 0 To ckbLst.Items.Count - 1
            If ckbLst.Items(i).ToString.ToUpper() = value.ToUpper() Then
                Return i
            End If
        Next
        Return -1
    End Function
    Public Sub carregarModuloRegistoPontos()
        Dim entrada As String
        entrada = "ERP"

        RPcmbModoRegisto.Items.Clear()
        RPcmbModoRegisto.Items.Add("Diária")
        RPcmbModoRegisto.Items.Add("Semanal")
        RPcmbModoRegisto.Items.Add("Mensal")

        RPcmbTipoEntidade.Items.Clear()
        RPcmbTipoEntidade.Items.Add("Clientes")
        RPcmbTipoEntidade.Items.Add("Fornecedores")
        RPcmbTipoEntidade.Items.Add("Funcionários")



        entrada = "DOCUMENTORP"
        RPtxtRPDocumento.Text = ini.GetString(entrada, "TIPODOC", "")
        RPtxtRPSerie.Text = ini.GetString(entrada, "SERIE", "")

        RPcmbModoRegisto.SelectedItem = buscarDescricaoCodigoModoRegisto(ini.GetString(entrada, "MODO", ini.GetString("ERP", "MODO", "")))
        RPcmbTipoEntidade.SelectedItem = buscarDescricaoCodigoTipoEntidade(ini.GetString(entrada, "TIPOENTIDADE", ""))

        Dim m As Motor
        m = Motor.GetInstance
        RPtxtRPDocumentoDescricao.Text = m.consultaValor("SELECT Descricao from DocumentosInternos WHERE Documento='" + RPtxtRPDocumento.Text + "'")
        RPtxtRPSerieDescricao.Text = m.consultaValor("SELECT Descricao from SeriesInternos WHERE TipoDoc='" + RPtxtRPDocumento.Text + "' and Serie='" + RPtxtRPSerie.Text + "'")
        RPckbMultiplaSelecção.Checked = ini.GetBoolean(entrada, "MULTIPLASELECCAO", False)


        iniciarFuncionarios(RPlstFuncionariosPerm, False)
        iniciarOperacaoes()
        iniciarPostos(RPlstPostos)
        iniciarPostos(RPlstPostos1)
        iniciarFuncionarios(RPlstFuncionarios)

    End Sub


    Private Sub iniciarFuncionarios(lst As ListBox, Optional selecionar As Boolean = True)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.daListaFuncionarios("", "")
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lst.DataSource = dt
        lst.ValueMember = "Codigo"
        lst.DisplayMember = "Nome"
        lst.ClearSelected()
        If selecionar Then
            If lst.Items.Count > 0 Then
                lst.SelectedIndex = 0
            End If
        End If
    End Sub


    Private Sub iniciarOperacaoes()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.daListaOperacoes()
        Dim dt As DataTable
        Dim row As DataRow
        dt = m.convertRecordSetToDataTable(lista.DataSet)

        RPlstOperacao.DataSource = dt
        RPlstOperacao.ValueMember = "Codigo"
        RPlstOperacao.DisplayMember = "Nome"
        RPlstOperacao.ClearSelected()

        dt = dt.Copy

        For Each row In dt.Rows
            row("Nome") = row("ID") + " - " + row("Nome")
        Next


        RPlstOperacao1.DataSource = dt
        'RPlstOperacao1.MultiColumn = True
        RPlstOperacao1.ValueMember = "ID"
        RPlstOperacao1.DisplayMember = "Nome"
        RPlstOperacao1.ClearSelected()
    End Sub

    Private Sub iniciarPostos(lst As ListBox)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.daListaPostos()
        Dim dt As DataTable
        Dim row As DataRow
        dt = m.convertRecordSetToDataTable(lista.DataSet)

        If lst.Name = "RPlstPostos1" Then
            For Each row In dt.Rows
                row("Nome") = row("Codigo") + " - " + row("Nome")
            Next
        End If
        lst.DataSource = dt
        lst.ValueMember = "Codigo"
        lst.DisplayMember = "Nome"
        lst.ClearSelected()
    End Sub


    Private Sub iniciarPostos(operacao As String)
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance
        lista = m.daListaPostos(operacao)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        RPlstPostos1.DataSource = dt
        RPlstPostos1.ValueMember = "Codigo"
        RPlstPostos1.DisplayMember = "Nome"
        '   lstPostos1.ClearSelected()
    End Sub

    Private Function buscarDescricaoCodigoModoRegisto(modo As String)
        Select Case modo
            Case "D" : Return "Diária"
            Case "S" : Return "Semanal"
            Case "M" : Return "Mensal"
            Case "Diária" : Return "D"
            Case "Semanal" : Return "S"
            Case "Mensal" : Return "M"
            Case Else : Return ""
        End Select
    End Function

    Private Function buscarDescricaoCodigoTipoEntidade(tipoentidade As String)
        Select Case tipoentidade
            Case "C" : Return "Clientes"
            Case "F" : Return "Fornecedores"
            Case "U" : Return "Funcionários"
            Case "Clientes" : Return "C"
            Case "Fornecedores" : Return "F"
            Case "Funcionários" : Return "U"
            Case Else : Return ""
        End Select
    End Function

    Public Sub carregarModuloPontosClientes()

    End Sub



    Public Sub carregarModuloVendasCompras()

    End Sub


    Private Sub carregarModuloStocks()
        Dim entrada As String
        entrada = "ERP"
        ScmbTipoEntidade.Items.Clear()
        ScmbTipoEntidade.Items.Add("Clientes")
        ScmbTipoEntidade.Items.Add("Fornecedores")
        ScmbTipoEntidade.Items.Add("Funcionários")

        SEmpresa.SelectedItem = ini.GetString(entrada, "EMPRESA", "")

        entrada = "DOCUMENTO"
        StxtDocumento.Text = ini.GetString(entrada, "TIPODOCS", "")
        StxtSerie.Text = ini.GetString(entrada, "SERIES", "")

        ScmbTipoEntidade.SelectedItem = buscarDescricaoCodigoTipoEntidade(ini.GetString(entrada, "TIPOENTIDADES", ""))

        Dim m As Motor
        m = Motor.GetInstance
        m.setEmpresa(SEmpresa.SelectedItem)
        StxtDocumentoDescricao.Text = m.consultaValor("SELECT Descricao from DocumentosInternos WHERE Documento='" + StxtDocumento.Text + "'")
        StxtSerieDescricao.Text = m.consultaValor("SELECT Descricao from SeriesInternos WHERE TipoDoc='" + StxtDocumento.Text + "' and Serie='" + StxtSerie.Text + "'")

        SckbApresentaEncomenda.Checked = ini.GetBoolean(entrada, "APRESENTAENCOMENDA", False)

        Dim dt As DataTable
        dt = m.consultaDataTable("Select Documento from DocumentosInternos")
        SLstBoxDocumentos.Items.Clear()
        For i = 0 To dt.Rows.Count - 1
            SLstBoxDocumentos.Items.Add(dt.Rows(i)("Documento"))
        Next


        SLstBoxDocumentos.SelectedItems.Clear()
        SLstBoxDocumentos.CheckOnClick = True

        For i = 0 To SLstBoxDocumentos.CheckedItems.Count - 1
            SLstBoxDocumentos.SetItemChecked(i, False)
        Next
        Dim index As Integer

        Dim listaDocumentos() As String
        Dim documento As String
        listaDocumentos = ini.GetString(entrada, "DOCUMENTOSS", "").Split(",")

        For j = 0 To listaDocumentos.Length - 1
            documento = listaDocumentos(j).Replace("'", "")

            For i = 0 To SLstBoxDocumentos.Items.Count - 1

                index = getIndexComboBox(SLstBoxDocumentos, documento)
                If index <> -1 Then
                    SLstBoxDocumentos.SetItemChecked(index, True)
                End If
            Next
        Next

    End Sub

    Private Sub carregarModuloListaMateriais()

    End Sub


    Private Sub carregarLicenca()
        LictxtLicenciamento.Text = ini.GetString("ERP", "LICENCA", "")
        carregarLicenciamento(seg.decifrarStringTripleDES(LictxtLicenciamento.Text))
    End Sub
    Private Sub carregarLicenciamento(licenca As String)

        Dim m As Motor
        m = Motor.GetInstance()
        Dim empresas As String
        Dim listaEmpresas As String()
        Dim licemp As EmpresasLicenciamento
        Dim lvi As ListViewItem
        Dim empresa As String
        Dim modulos As String()
        lic = New Licenca()
        lic.lerLicenca(licenca, m.getNumeroLicenciamentos)
        empresas = lic.daListaEmpresas()
        listaEmpresas = empresas.Split(",")
        LicLstView.Items.Clear()
        LicLstView.BeginUpdate()
        For i = 0 To listaEmpresas.Length - 1
            empresa = Replace(listaEmpresas(i), "'", "")
            licemp = lic.daCaracteristicasLicencaEmpresa(empresa)
            modulos = licemp.Modulos.Split(",")
            For j = 0 To modulos.Length - 1
                lvi = LicLstView.Items.Add(i.ToString + j.ToString())
                lvi.SubItems.Add(licemp.Empresa)
                lvi.SubItems.Add(buscarModulo(modulos(j)))
                lvi.SubItems.Add(licemp.DataLimite)
                lvi.SubItems.Add(licemp.NumPostos)
            Next

        Next
        LicLstView.Update()
        LicLstView.EndUpdate()
        actualizarEmpresasERP()
        ' actualizarModulosERP()
    End Sub

    Private Function buscarModulo(modulo As String)
        Select Case modulo.ToUpper
            Case "S" : Return "Stocks"
            Case "P" : Return "Pontos"
            Case "E" : Return "Encomendas/Guias Transporte"
            Case "R" : Return "Registo Pontos"
            Case "M" : Return "Lista Materiais"
            Case "G" : Return "Gestão Encomendas"
        End Select
        Return ""
    End Function

    Private Sub LicBtnLicenca_Click(sender As Object, e As EventArgs) Handles LicBtnLicenca.Click
        Dim lic As String
        lic = seg.decifrarStringTripleDES(LictxtLicenciamento.Text)

        If lic = LictxtLicenciamento.Text Then
            MsgBox("Licenciamento inválido", MsgBoxStyle.Exclamation)
        Else
            carregarLicenciamento(lic)
        End If
    End Sub

    Private Sub actualizarEmpresasERP()
        Dim empresas As String
        Dim empresa As String
        Dim listaEmpresas() As String

        empresas = lic.daListaEmpresas()
        listaEmpresas = empresas.Split(",")

        ERPLstBoxEmpresas.Items.Clear()
        '  ERPcmbModulo.Items.Clear()
        ERPcmbEmpresaDefault.Items.Clear()

        For i = 0 To listaEmpresas.Length - 1
            empresa = Replace(listaEmpresas(i), "'", "")
            ERPLstBoxEmpresas.Items.Add(empresa)
            ERPcmbEmpresaDefault.Items.Add(empresa)
            SEmpresa.Items.Add(empresa)
        Next
    End Sub

    Private Sub actualizarModulosERP()
        Dim modulos As String
        Dim modulo As String
        Dim listaModulos() As String

        modulos = lic.daListaModulosEmpresa(ERPLstBoxEmpresas.SelectedValue)
        listaModulos = modulos.Split(",")

        ERPLstBoxModulos.Items.Clear()
        '  ERPcmbModulo.Items.Clear()
        ERPcmbEmpresaDefault.Items.Clear()
        For i = 0 To listaModulos.Length - 1
            modulo = Replace(listaModulos(i), "'", "")
            ERPLstBoxEmpresas.Items.Add(Empresa)
            ERPcmbEmpresaDefault.Items.Add(Empresa)
            SEmpresa.Items.Add(Empresa)
        Next
    End Sub

    Private Sub ERPcmbEmpresaDefault_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ERPcmbEmpresaDefault.SelectedIndexChanged
        Dim empresa As String
        Dim listaModulos() As String
        Dim hashModulos As Hashtable
        Dim modulo As Object
        hashModulos = New Hashtable

        ERPcmbModulo.DisplayMember = "Key"
        ERPcmbModulo.ValueMember = "Value"

        ERPcmbModulo.Items.Clear()
        If ERPcmbEmpresaDefault.SelectedItem <> "" Then
            empresa = ERPcmbEmpresaDefault.SelectedItem
            listaModulos = lic.daListaModulosEmpresa(empresa).Split(",")
            For j = 0 To listaModulos.Length - 1
                modulo = hashModulos(listaModulos(j))
                If modulo Is Nothing Then
                    ERPcmbModulo.Items.Add(New DictionaryEntry(buscarModulo(listaModulos(j)), listaModulos(j)))
                    hashModulos.Add(listaModulos(j), listaModulos(j))
                End If
            Next
        End If

        If ERPcmbModulo.Items.Count > 0 Then ERPcmbModulo.SelectedIndex = 0
    End Sub


    Private Sub RPcmbDocumento_Click(sender As Object, e As EventArgs) Handles RPcmbDocumento.Click

        Dim frm As FrmLista
        frm = New FrmLista
        frm.setComando("SELECT Documento,Descricao FROM DocumentosInternos")

        frm.ShowDialog()

        If frm.seleccionouOK Then
            RPtxtRPDocumento.Text = frm.getValor(0)
            RPtxtRPDocumentoDescricao.Text = frm.getValor(1)
        End If
    End Sub

    Private Sub RPcmbSerie_Click(sender As Object, e As EventArgs) Handles RPcmbSerie.Click

        Dim frm As FrmLista
        frm = New FrmLista
        frm.setComando("SELECT Serie,Descricao,DataInicial,DataFinal FROM SeriesInternos where TipoDoc='" + RPtxtRPDocumento.Text + "'")
        frm.ShowDialog()

        If frm.seleccionouOK Then
            RPtxtRPSerie.Text = frm.getValor(0)
            RPtxtRPSerieDescricao.Text = frm.getValor(1)
        End If
    End Sub

    Private Sub ScmbDocumento_Click(sender As Object, e As EventArgs) Handles ScmbDocumento.Click

        Dim frm As FrmLista
        frm = New FrmLista
        frm.setComando("SELECT Documento,Descricao FROM DocumentosInternos")
        frm.ShowDialog()
        If frm.seleccionouOK Then
            StxtDocumento.Text = frm.getValor(0)
            StxtDocumentoDescricao.Text = frm.getValor(1)
        End If

    End Sub

    Private Sub ScmbSerie_Click(sender As Object, e As EventArgs) Handles ScmbSerie.Click
        Dim frm As FrmLista
        frm = New FrmLista
        frm.setComando("SELECT Serie,Descricao,DataInicial,DataFinal FROM SeriesInternos where TipoDoc='" + StxtDocumento.Text + "'")
        frm.ShowDialog()
        If frm.seleccionouOK Then
            StxtSerie.Text = frm.getValor(0)
            StxtSerieDescricao.Text = frm.getValor(1)
        End If
    End Sub

    Private Sub SEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SEmpresa.SelectedIndexChanged
        Dim m As Motor
        m = Motor.GetInstance
        m.setEmpresa(SEmpresa.SelectedItem)
        Dim dt As DataTable
        dt = m.consultaDataTable("Select Documento from DocumentosInternos")
        SLstBoxDocumentos.Items.Clear()
        For i = 0 To dt.Rows.Count - 1
            SLstBoxDocumentos.Items.Add(dt.Rows(i)("Documento"))
        Next
    End Sub

    Private Sub btnAddOP_Click(sender As Object, e As EventArgs) Handles RPbtnAddOP.Click
        Dim f As FrmAddOPPostos
        f = New FrmAddOPPostos
        f.Modo = "O"
        f.ShowDialog()
        iniciarOperacaoes()
        iniciarFormPermissoes()
    End Sub

    Private Sub btnAddPostos_Click(sender As Object, e As EventArgs) Handles RPbtnAddPostos.Click
        Dim f As FrmAddOPPostos
        f = New FrmAddOPPostos
        f.Modo = "P"
        f.ShowDialog()
        iniciarPostos(RPlstPostos)
        iniciarPostos(RPlstPostos1)
        iniciarFormPermissoes()
    End Sub

    Private Sub btnRemoveOP_Click(sender As Object, e As EventArgs) Handles RPbtnRemoveOP.Click
        Dim m As Motor
        m = Motor.GetInstance
        If MsgBox("Deseja remover a operação seleccionada?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If m.removerOperacao(getSeleccionadoOperacaoID, getSeleccionadoOperacao, getSeleccionadoOperacaoNome) Then
                iniciarOperacaoes()
                iniciarPostos(RPlstPostos)
                iniciarPostos(RPlstPostos1)
                iniciarFormPermissoes()
            End If
        End If
    End Sub

    Private Sub btnRemovePostos_Click(sender As Object, e As EventArgs) Handles RPbtnRemovePostos.Click
        Dim m As Motor
        m = Motor.GetInstance
        If MsgBox("Deseja remover o posto seleccionado? " + vbCrLf + vbCrLf + "Todas as permissões dos Postos de Trabalho serão eliminados.", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If m.removerPosto("", getSeleccionadPosto, getSeleccionadoOperacao) Then
                iniciarPostos(getSeleccionadoOperacao)
                iniciarPostos(RPlstPostos)
                iniciarPostos(RPlstPostos1)
                iniciarFormPermissoes()
            End If
        End If
    End Sub



    Private Sub iniciarFormPermissoes()
        Dim drview As DataRowView
        drview = RPlstFuncionarios.SelectedItem
        If Not drview Is Nothing Then
            buscarOPeracoesFuncionario(drview("Codigo"))
            buscarPostosFuncionario(drview("Codigo"))
            buscarFuncionariosFuncionario(drview("Codigo"))
        End If

    End Sub

    Private Sub buscarOPeracoesFuncionario(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        RPlstOperacao.ClearSelected()
        For i = 0 To RPlstOperacao.Items.Count - 1
            drview = RPlstOperacao.Items(i)
            If m.daPermissoesOperacaoesFuncionario(funcionario, drview("Codigo")) Then
                RPlstOperacao.SetSelected(i, True)
            End If
        Next
    End Sub

    Private Sub buscarPostosFuncionario(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        RPlstPostos.ClearSelected()
        For i = 0 To RPlstPostos.Items.Count - 1
            drview = RPlstPostos.Items(i)
            If m.daPermissoesPostosFuncionario(funcionario, drview("CDU_Posto")) Then
                RPlstPostos.SetSelected(i, True)
            End If
        Next
    End Sub


    Private Sub buscarFuncionariosFuncionario(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        RPlstFuncionariosPerm.ClearSelected()
        For i = 0 To RPlstFuncionariosPerm.Items.Count - 1
            drview = RPlstFuncionariosPerm.Items(i)
            If m.daPermissoesFuncionariosFuncionario(funcionario, drview("Codigo")) Or funcionario = drview("Codigo") Then
                RPlstFuncionariosPerm.SetSelected(i, True)
            End If
        Next
    End Sub


    Private Function getSeleccionadoOperacao()
        Dim drView As DataRowView
        drView = RPlstOperacao1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoOperacao = drView("Codigo")
    End Function

    Private Function getSeleccionadoOperacaoID()
        Dim drView As DataRowView
        drView = RPlstOperacao1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoOperacaoID = drView("ID")
    End Function

    Private Function getSeleccionadPosto()
        Dim drView As DataRowView
        drView = RPlstPostos1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadPosto = drView("Codigo")
    End Function



    Private Function getSeleccionadoOperacaoNome()
        Dim drView As DataRowView
        drView = RPlstOperacao1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadoOperacaoNome = drView("Nome")
    End Function

    Private Function getSeleccionadPostoNome()
        Dim drView As DataRowView
        drView = RPlstPostos1.SelectedItem
        If drView Is Nothing Then Return ""
        getSeleccionadPostoNome = drView("Nome")
    End Function

    Private Sub RPlstFuncionarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RPlstFuncionarios.SelectedIndexChanged
        iniciarFormPermissoes()
    End Sub

    Private Sub CommandBarsFrame1_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        gravarConfiguracoesGerais
        gravarRegistoPontos()
    End Sub


    Private Sub gravarConfiguracoesGerais()


        Dim entrada As String
        entrada = "ERP"

        ini.WriteString(entrada, "TIPOPLAT", ERPcmbTipoPlataforma.SelectedIndex)
        ini.WriteString(entrada, "INSTANCIA", ERPtxtInstancia.Text)
        ini.WriteString(entrada, "APLICACAO", ERPtxtAplicacao.Text)
        ini.WriteString(entrada, "UTILIZADOR", ERPtxtUtilizadorERP.Text)

        'ERPtxtPasswordERP.Text = seg.decifrarStringTripleDES(ini.GetString(entrada, "PASSWORD", ""))

        ini.WriteString(entrada, "PASSWORD", seg.encriptarStringTripleDES(ERPtxtPasswordERP.Text))
        ini.WriteString(entrada, "EMPRESA", ERPcmbEmpresaDefault.SelectedItem)

        Dim empresas As String
        empresas = buscarEmpresasVisiveis()
        ini.WriteString(entrada, "EMPRESAS", empresas)


        ini.WriteString(entrada, "MODULO", ERPcmbModulo.SelectedItem.Value)

        ini.WriteString(entrada, "ART_MAT_LIM", ERPtxtArtMaterial.Text)
        ini.WriteString(entrada, "ARMAZEM_DEFAULT", ERPtxtArmazem.Text)


    End Sub


    Private Function buscarEmpresasVisiveis() As String
        Dim empresas As String = ""
        Dim empresa As String = ""

        For i = 0 To ERPLstBoxEmpresas.Items.Count - 1
            If ERPLstBoxEmpresas.GetItemChecked(i) Then
                empresa = ERPLstBoxEmpresas.Items(i)
                If empresas <> "" Then
                    empresas = empresas + ","
                End If
                empresas = empresas + "'" + empresa + "'"
            End If

        Next

        Return empresa

    End Function
    Private Sub gravarRegistoPontos()
        Dim m As Motor
        Dim funcionario As String
        m = Motor.GetInstance
        m.IniciaTransacao()
        Try

            funcionario = RPlstFuncionarios.SelectedValue

            m.removePermissoes(funcionario)
            inserePermissoesFuncionarioOperacoes(funcionario)
            inserePermissoesFuncionarioPostos(funcionario)
            inserePermissoesFuncionarioFuncionarios(funcionario)

            m.TerminaTransacao()
            MsgBox("Permissões configurados com sucesso", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            m.DesfazTransacao()
        End Try
    End Sub


    Public Sub inserePermissoesFuncionarioOperacoes(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In RPlstOperacao.SelectedItems
            m.inserePermissoesOperacao(funcionario, drview("Codigo"))
        Next
    End Sub

    Public Sub inserePermissoesFuncionarioPostos(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In RPlstPostos.SelectedItems
            m.inserePermissoesPosto(funcionario, drview("Codigo"))
        Next
    End Sub

    Public Sub inserePermissoesFuncionarioFuncionarios(funcionario As String)
        Dim m As Motor
        m = Motor.GetInstance
        Dim drview As DataRowView
        For Each drview In RPlstFuncionariosPerm.SelectedItems
            m.inserePermissoesFuncionario(funcionario, drview("Codigo"))
        Next
    End Sub

    Private Sub ERPbtnArt_Click(sender As Object, e As EventArgs) Handles ERPbtnArt.Click
        Dim frm As FrmLista
        frm = New FrmLista
        frm.setComando("SELECT Artigo,Descricao FROM Artigo")

        frm.ShowDialog()

        If frm.seleccionouOK Then
            ERPtxtArtMaterial.Text = frm.getValor(0)
            ERPtxtArtMaterialDescricao.Text = frm.getValor(1)
        End If
    End Sub

    Private Sub ERPbtnArm_Click(sender As Object, e As EventArgs) Handles ERPbtnArm.Click
        Dim frm As FrmLista
        frm = New FrmLista
        frm.setComando("SELECT Armazem,Descricao FROM Armazens")

        frm.ShowDialog()

        If frm.seleccionouOK Then
            ERPtxtArmazem.Text = frm.getValor(0)
            ERPtxtArmazemDescricao.Text = frm.getValor(1)
        End If
    End Sub
End Class