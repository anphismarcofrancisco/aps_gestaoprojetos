﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmArtigo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmArtigo))
        Me.txtArtigo = New AxXtremeSuiteControls.AxFlatEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.gridArtigosStock = New AxXtremeReportControl.AxReportControl
        Me.gridArtigos = New AxXtremeReportControl.AxReportControl
        Me.btnGravar = New System.Windows.Forms.Button
        Me.btnEAT = New System.Windows.Forms.Button
        Me.btnDAT = New System.Windows.Forms.Button
        Me.btnEPA = New System.Windows.Forms.Button
        Me.btnDPA = New System.Windows.Forms.Button
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtArtigo
        '
        Me.txtArtigo.Location = New System.Drawing.Point(12, 116)
        Me.txtArtigo.Name = "txtArtigo"
        Me.txtArtigo.OcxState = CType(resources.GetObject("txtArtigo.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtArtigo.Size = New System.Drawing.Size(451, 32)
        Me.txtArtigo.TabIndex = 60
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(532, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(129, 25)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Artigos Saida"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(7, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 25)
        Me.Label1.TabIndex = 58
        Me.Label1.Text = "Artigos"
        '
        'gridArtigosStock
        '
        Me.gridArtigosStock.Location = New System.Drawing.Point(537, 116)
        Me.gridArtigosStock.Name = "gridArtigosStock"
        Me.gridArtigosStock.OcxState = CType(resources.GetObject("gridArtigosStock.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosStock.Size = New System.Drawing.Size(540, 567)
        Me.gridArtigosStock.TabIndex = 52
        Me.gridArtigosStock.Tag = "1"
        '
        'gridArtigos
        '
        Me.gridArtigos.Location = New System.Drawing.Point(12, 154)
        Me.gridArtigos.Name = "gridArtigos"
        Me.gridArtigos.OcxState = CType(resources.GetObject("gridArtigos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigos.Size = New System.Drawing.Size(451, 529)
        Me.gridArtigos.TabIndex = 51
        Me.gridArtigos.Tag = "0"
        '
        'btnGravar
        '
        Me.btnGravar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGravar.Image = CType(resources.GetObject("btnGravar.Image"), System.Drawing.Image)
        Me.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGravar.Location = New System.Drawing.Point(937, 28)
        Me.btnGravar.Name = "btnGravar"
        Me.btnGravar.Size = New System.Drawing.Size(140, 62)
        Me.btnGravar.TabIndex = 61
        Me.btnGravar.Text = " Gravar   "
        Me.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGravar.UseVisualStyleBackColor = True
        '
        'btnEAT
        '
        Me.btnEAT.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PDirT
        Me.btnEAT.Location = New System.Drawing.Point(469, 544)
        Me.btnEAT.Name = "btnEAT"
        Me.btnEAT.Size = New System.Drawing.Size(62, 62)
        Me.btnEAT.TabIndex = 57
        Me.btnEAT.UseVisualStyleBackColor = True
        Me.btnEAT.Visible = False
        '
        'btnDAT
        '
        Me.btnDAT.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PEsqT
        Me.btnDAT.Location = New System.Drawing.Point(469, 195)
        Me.btnDAT.Name = "btnDAT"
        Me.btnDAT.Size = New System.Drawing.Size(62, 62)
        Me.btnDAT.TabIndex = 56
        Me.btnDAT.UseVisualStyleBackColor = True
        Me.btnDAT.Visible = False
        '
        'btnEPA
        '
        Me.btnEPA.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PEsq
        Me.btnEPA.Location = New System.Drawing.Point(469, 418)
        Me.btnEPA.Name = "btnEPA"
        Me.btnEPA.Size = New System.Drawing.Size(62, 62)
        Me.btnEPA.TabIndex = 55
        Me.btnEPA.UseVisualStyleBackColor = True
        '
        'btnDPA
        '
        Me.btnDPA.Image = CType(resources.GetObject("btnDPA.Image"), System.Drawing.Image)
        Me.btnDPA.Location = New System.Drawing.Point(469, 330)
        Me.btnDPA.Name = "btnDPA"
        Me.btnDPA.Size = New System.Drawing.Size(62, 62)
        Me.btnDPA.TabIndex = 54
        Me.btnDPA.UseVisualStyleBackColor = True
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(227, 32)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 63
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(2, 1)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(204, 55)
        Me.CommandBarsFrame1.TabIndex = 62
        '
        'FrmArtigo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        ' ' Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1091, 689)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.btnGravar)
        Me.Controls.Add(Me.txtArtigo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnEAT)
        Me.Controls.Add(Me.btnDAT)
        Me.Controls.Add(Me.btnEPA)
        Me.Controls.Add(Me.btnDPA)
        Me.Controls.Add(Me.gridArtigosStock)
        Me.Controls.Add(Me.gridArtigos)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmArtigo"
        Me.Text = "Artigos"
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGravar As System.Windows.Forms.Button
    Friend WithEvents txtArtigo As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnEAT As System.Windows.Forms.Button
    Friend WithEvents btnDAT As System.Windows.Forms.Button
    Friend WithEvents btnEPA As System.Windows.Forms.Button
    Friend WithEvents btnDPA As System.Windows.Forms.Button
    Friend WithEvents gridArtigosStock As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigos As AxXtremeReportControl.AxReportControl
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
End Class
