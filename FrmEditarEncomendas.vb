﻿
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900

Public Class FrmEditarEncomendas

    Dim clicouConfirmar_ As Boolean


    Const COLUMN_DOCUMENTO As Integer = 0
    Const COLUMN_CODIGOENTIDADE As Integer = 1
    Const COLUMN_NOMEENTIDADE As Integer = 2
    Const COLUMN_ARTIGO As Integer = 3
    Const COLUMN_DESCRICAO As Integer = 4
    Const COLUMN_QUANTIDADE As Integer = 5
    Const COLUMN_PROJECTO As Integer = 6
    '  Const COLUMN_DATA As Integer = 6

    ReadOnly Property clicouConfirmar() As Boolean
        Get
            clicouConfirmar = clicouConfirmar_
        End Get

    End Property

    Dim tipodoc_ As String
    ''' <summary>
    ''' TipoDocumento a Validar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property TipoDocumento() As String
        Get
            TipoDocumento = tipodoc_
        End Get
        Set(ByVal value As String)
            tipodoc_ = value
        End Set
    End Property

    Dim modulo_ As String
    ''' <summary>
    ''' Modulo a Validar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Modulo() As String
        Get
            Modulo = modulo_
        End Get
        Set(ByVal value As String)
            modulo_ = value
        End Set
    End Property

    Private Sub FrmEditar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim m As Motor
        m = Motor.GetInstance
        iniciarToolBox()
        dtpDataI.Value = m.DayStart(Now) 'm.firstDayMonth(Now)
        dtpDataF.Value = m.DayEnd(Now) 'm.lastDayMonth(Now)
        ' gridRegistos.AutoGenerateColumns = True

        gridRegistos.AutoColumnSizing = False
        gridRegistos.AutoColumnSizing = False
        gridRegistos.ShowGroupBox = True
        gridRegistos.ShowItemsInGroups = False

        '  buscarListaCentrosTrabalho()
        buscarListaFuncionariosPermissoes()
        inserirColunasRegistos()
        gridRegistos.SortOrder.DeleteAll()
        'Try
        gridRegistos.SortOrder.Add(gridRegistos.Columns(COLUMN_DOCUMENTO))
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

        'Dim rowCT As DataRowView
        'rowCT = cmbCentroTrabalho.SelectedItem

        ' buscarRegistos(rowCT("Nome"), cmbFuncionarios.SelectedValue)
        buscarRegistos("", cmbFuncionarios.SelectedValue)
        clicouConfirmar_ = False

    End Sub


    'Private Sub buscarListaCentrosTrabalho()
    '    Dim dt As DataTable
    '    Dim lista As StdBELista
    '    Dim m As Motor
    '    m = Motor.GetInstance
    '    lista = m.daListaPostos(m.Funcionario, "", True)
    '    dt = m.convertRecordSetToDataTable(lista.DataSet)
    '    cmbCentroTrabalho.DataSource = dt
    '    cmbCentroTrabalho.ValueMember = "Codigo"
    '    cmbCentroTrabalho.DisplayMember = "Nome"
    'End Sub

    Private Sub buscarListaFuncionariosPermissoes()
        Dim dt As DataTable
        Dim m As Motor
        m = Motor.GetInstance
        dt = m.daListaFuncionariosPermissoes()
        cmbFuncionarios.DataSource = dt
        cmbFuncionarios.ValueMember = "Codigo"
        cmbFuncionarios.DisplayMember = "Nome"
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Actualizar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Actualizar"

            Control = .Add(xtpControlButton, 2, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    'Private Sub formatarColunas()
    '    gridRegistos.Columns("CDU_ID").Visible = False
    '    gridRegistos.Columns("CDU_IDLinha").Visible = False
    '    gridRegistos.Columns("Centro Trabalho").Width = gridRegistos.Columns("Centro Trabalho").Width + 10
    '    'gridRegistos.Columns("CDU_Funcionario").Visible = False
    'End Sub

    Public Function getIdLinha() As String
        getIdLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    getIdLinha = gridRegistos.SelectedRows(0).Record.Tag.ToString
                End If
            End If
        End If
    End Function

    Public Function getValorLinha(ByVal coluna As Integer) As String
        getValorLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    If coluna <= gridRegistos.Columns.Count Then getValorLinha = gridRegistos.SelectedRows(0).Record(coluna).Value.ToString()
                End If
            End If
        End If
    End Function

    Private Sub gridRegistos_CellMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs)
        clicouConfirmar_ = True
        Me.Close()
    End Sub

    Private Sub inserirColunasRegistos()

        Dim Column As ReportColumn
        Column = inserirColuna(True, COLUMN_DOCUMENTO, "Documento", 180, True, True)
        Column.Tag = "Documento"
        Column = inserirColuna(True, COLUMN_CODIGOENTIDADE, "CODIGOENTIDADE", 180, True, True)
        Column.Tag = "Entidade"
        Column.Visible = False
        Column = inserirColuna(True, COLUMN_NOMEENTIDADE, "Entidade", 180, True, True)
        Column.Tag = "Nome"
        Column = inserirColuna(True, COLUMN_ARTIGO, "Artigo", 180, True, True)
        Column.Tag = "Artigo"
        Column = inserirColuna(True, COLUMN_DESCRICAO, "Descrição", 340, True, True)
        Column.Tag = "Descricao"
        Column = inserirColuna(True, COLUMN_QUANTIDADE, "Quantidade", 100, True, True)
        Column.Tag = "Quantidade"
        Column.Alignment = xtpAlignmentRight
        Column = inserirColuna(True, COLUMN_PROJECTO, "Projecto", 100, True, True)
        Column.Tag = "Projecto"

    End Sub

    Private Function inserirColuna(ByVal isGridSE As Boolean, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = gridRegistos.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function

    Private Sub buscarRegistos(ByVal centroTrabalho As String, ByVal funcionario As String)
        Dim m As Motor
        Dim total As Double
        m = Motor.GetInstance

        Dim consulta As String
        consulta = ""
        gridRegistos.Records.DeleteAll()
        gridRegistos.HeaderRecords.DeleteAll()
        gridRegistos.FooterRecords.DeleteAll()
        Dim dtRegistos As DataTable
        dtRegistos = Nothing
        If modulo_ = "C" Then dtRegistos = m.daListaRegistosDocumentoCompra(tipodoc_, centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value, consulta)
        If modulo_ = "V" Then dtRegistos = m.daListaRegistosDocumentoVenda(tipodoc_, centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value, consulta)


        inserirLinhasRegistos(dtRegistos)
        If gridRegistos.GroupsOrder.Count = 0 Then
            gridRegistos.GroupsOrder.DeleteAll()
            gridRegistos.GroupsOrder.Add(gridRegistos.Columns(buscarIndexColunaDocumento))
            gridRegistos.Columns(buscarIndexColunaDocumento).Visible = False
        End If
        If modulo_ = "C" Then total = m.daTotalRegistosDocumentoCompra(tipodoc_, centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value, "S")
        If modulo_ = "V" Then total = m.daTotalRegistosDocumentoVenda(tipodoc_, centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value, "S")
        'total = m.daTotalRegistosDocumentoCompra(tipodoc_, centroTrabalho, funcionario, dtpDataI.Value, dtpDataF.Value, "S")

        inserirCabecalhoPesquisa(dtRegistos)
        inserirLinhaTotal(total)
        gridRegistos.Populate()
        gridRegistos.Tag = consulta
        inserirSubtotais()
    End Sub


    Private Sub inserirCabecalhoPesquisa(ByVal lista As DataTable)

        gridRegistos.ShowHeaderRows = True
        gridRegistos.HeaderRowsAllowEdit = True
        gridRegistos.AllowEdit = True
        Dim col As Integer
        Dim Item As ReportRecordItem
        Dim record As ReportRecord
        record = gridRegistos.HeaderRecords.Add
        For col = 0 To lista.Columns.Count - 1
            Item = record.AddItem("")
        Next col
    End Sub

    Private Function buscarIndexColunaDocumento() As Integer

        Dim col As ReportColumn
        For Each col In gridRegistos.Columns
            If col.Caption = "Documento" Then
                Return col.Index
            End If
        Next
        Return COLUMN_DOCUMENTO
    End Function
    Private Sub inserirLinhaTotal(ByVal total As Double)
        Dim frr As ReportRecord
        ' gridRegistos.ShowFooter = True
        gridRegistos.ShowFooterRows = True
        ' gridRegistos.ShowHeaderRows = True
        'frr = gridRegistos.HeaderRecords.Add()
        'frr.AddItem(CStr(dtpDataI.Value) + " a " + CStr(dtpDataF.Value))
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")

        frr = gridRegistos.FooterRecords.Add()

        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("Total")
        frr.AddItem(IIf(total < 0, -total, total))
        frr.AddItem("")



    End Sub
    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable)
        Dim dtrow As DataRow

        gridRegistos.Records.DeleteAll()
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow)
        Next
        gridRegistos.Populate()
    End Sub

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim documento As String
        record = gridRegistos.Records.Add()

        record.Tag = dtRow("ID")
        documento = dtRow("TipoDoc") + " | " + dtRow("Serie") + " | " + CStr(dtRow("NumDoc")) + " de " + FormatDateTime(dtRow("DataDoc"), DateFormat.ShortDate)
        Item = record.AddItem(documento)
        Item.Caption = documento
        Item.Tag = dtRow("ID")
        aplicarFormatoLinha(dtRow("Estado"), Item)
        Item = record.AddItem(dtRow("Entidade"))
        Item.Tag = "Entidade"
        aplicarFormatoLinha(dtRow("Estado"), Item)
        Item = record.AddItem(dtRow("Nome"))
        Item.Tag = "Nome"
        aplicarFormatoLinha(dtRow("Estado"), Item)
        Item = record.AddItem(dtRow("Artigo"))
        Item.Tag = "Artigo"
        aplicarFormatoLinha(dtRow("Estado"), Item)
        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = "Descricao"
        aplicarFormatoLinha(dtRow("Estado"), Item)
        Item = record.AddItem(IIf(dtRow("Quantidade") < 0, -dtRow("Quantidade"), dtRow("Quantidade")))
        Item.Tag = "Quantidade"
        aplicarFormatoLinha(dtRow("Estado"), Item)
        Item = record.AddItem(dtRow("Projecto"))
        Item.Tag = "Projecto"

        aplicarFormatoLinha(dtRow("Estado"), Item)

    End Sub

    Private Sub aplicarFormatoLinha(ByVal estado As String, ByVal item As ReportRecordItem)

        item.Editable = False
        Select Case estado
            Case "N" : item.BackColor = 12632256
            Case "F" : item.BackColor = 14671839
        End Select


    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute

        ' Dim rowCT As DataRowView
        ' rowCT = cmbCentroTrabalho.SelectedItem

        Select Case e.control.Index
            Case 1 : buscarRegistos("", cmbFuncionarios.SelectedValue)
            Case 2 : imprimir()
            Case 3 : Me.Close()
        End Select
    End Sub
    Private Sub imprimir()
        'gridRegistos.PrintPreviewOptions.Title = ""
        gridRegistos.PrintOptions.Header.FormatString = "Registos de Folhas Ponto" + vbCrLf + vbCrLf + "De " + CStr(dtpDataI.Value) + " a " + CStr(dtpDataF.Value)
        gridRegistos.PrintOptions.Header.Font.Size = 12
        gridRegistos.PrintOptions.BlackWhitePrinting = True
        gridRegistos.PrintOptions.Landscape = False
        gridRegistos.PrintPreview(True)
    End Sub

    Private Sub FrmEditar_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        gridRegistos.Width = Me.Width - (45)
        gridRegistos.Height = Me.Height - (gridRegistos.Top + 45)
    End Sub

    Private Sub gridRegistos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridRegistos.RowDblClick


        If Not e.item Is Nothing Then
            Dim estado As String
            estado = e.item.Record.Item(COLUMN_QUANTIDADE).Tag
            If estado <> "N" And estado <> "F" Then
                Dim m As Motor
                m = Motor.GetInstance
                '  If e.item.Record.Item(COLUMN_CODIGOENTIDADE).Value = m.Funcionario Then
                clicouConfirmar_ = True
                Me.Close()
                'Else
                '  mostrarMensagem("O")
                ' End If
            Else
                mostrarMensagem(estado)
            End If
        End If
    End Sub

    Private Sub mostrarMensagem(ByVal estado As String)
        Select Case estado
            Case "N" : MsgBox("O registo já se encontra Anulado", MsgBoxStyle.Exclamation)
            Case "F" : MsgBox("O registo já se encontra Fechado", MsgBoxStyle.Exclamation)
            Case "O" : MsgBox("O registo seleccionado é de outro funcionário", MsgBoxStyle.Exclamation)
        End Select
    End Sub

    Private Sub inserirSubtotais()
        '   gridRegistos.ReCalc(True)
        Dim m As Motor
        Dim row As ReportRow
        Dim groupRow As ReportGroupRow
        m = Motor.GetInstance
        For i = 0 To gridRegistos.Rows.Count - 1
            row = gridRegistos.Rows(i)
            If row.GroupRow Then
                groupRow = row

                'groupRow.GroupFormat = " [SubTotal $=%.02f]"
                groupRow.GroupCaption = groupRow.GroupCaption + " - " + "Qtd: " + CStr(buscarTotaisGroup(groupRow))  ' CStr(m.daTotalRegistosCT(row.Childs(0).Record.Item(COLUMN_POSTO).Value, dtpDataI.Value, dtpDataF.Value)) + " H"
                'gridRegistos.GroupsOrder.
                'groupRow.GroupFormula = "SUMSUB(R*C2:R*C4)" 'Old notation
                '  groupRow.GroupFormula = "SUMSUB(C2:C3) SUMSUB(C3:C4)" 'New (short) notation
                ' groupRow.GroupCaption = "x"
            End If
        Next

    End Sub

    Private Function buscarTotaisGroup(ByVal groupRow As ReportGroupRow) As Double
        Dim total As Double
        total = 0
        Dim linha As ReportRow
        For Each linha In groupRow.Childs
            If Not linha.GroupRow Then
                total = total + linha.Record(COLUMN_QUANTIDADE).Value
            Else
                total = total + buscarTotaisGroup(linha)
            End If
        Next
        Return total
    End Function

    Dim bEnterPressed As Boolean
    Private Sub gridRegistos_ValueChanged(sender As Object, e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles gridRegistos.ValueChanged
        If bEnterPressed Then
            aplicarFiltro()
        End If
    End Sub

    Private Sub gridRegistos_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles gridRegistos.PreviewKeyDown
        If e.KeyCode = 13 And e.Shift = 0 Then
            bEnterPressed = True
        Else
            bEnterPressed = False
        End If
    End Sub

    Private Sub aplicarFiltro()
        Dim lista As DataTable
        Dim restricao As String
        Dim index As Integer
        Dim Item As ReportRecordItem
        Dim consulta As String
        Dim m As Motor
        m = Motor.GetInstance()
        consulta = gridRegistos.Tag
        restricao = " WHERE "

        If gridRegistos.SelectedRows.Count > 0 Then
            For index = 0 To gridRegistos.Columns.Count - 1
                Item = gridRegistos.HeaderRecords(0).Item(index)
                'Set Item = grid.SelectedRows(0).record(index)
                If Item.Value <> "" Then
                    restricao = restricao & "[" & gridRegistos.Columns(index).Tag & "]" & " LIKE '%" & Item.Value & "%' AND "
                End If
            Next index

            If restricao <> " WHERE " Then
                restricao = Mid(restricao, 1, Len(restricao) - 5)
            Else
                restricao = ""
            End If

            lista = m.consultaDataTable("SELECT * FROM (" & consulta & ") tmpQuery" & restricao)

            inserirLinhasRegistos(lista)
        End If
        bEnterPressed = False
    End Sub

End Class
