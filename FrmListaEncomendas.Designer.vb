﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmListaEncomendas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListaEncomendas))
        Me.lblFornecedor = New System.Windows.Forms.Label()
        Me.lblArtigos = New System.Windows.Forms.Label()
        Me.lblText = New System.Windows.Forms.Label()
        Me.lblObsLST = New System.Windows.Forms.Label()
        Me.lblRegisto1 = New System.Windows.Forms.Label()
        Me.txtObservacoesLST = New System.Windows.Forms.TextBox()
        Me.tbcMoldes = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnProjecto = New System.Windows.Forms.Button()
        Me.tabCtlMaterial = New System.Windows.Forms.TabControl()
        Me.TabListaMaterial = New System.Windows.Forms.TabPage()
        Me.lblDataUltCarregamentoLista = New System.Windows.Forms.Label()
        Me.tabCtlGeral = New System.Windows.Forms.TabControl()
        Me.TabPageGeral = New System.Windows.Forms.TabPage()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.lblUtilActualizacaoLST = New System.Windows.Forms.Label()
        Me.lblUtilGravacaoLST = New System.Windows.Forms.Label()
        Me.ckbVerTodos = New System.Windows.Forms.CheckBox()
        Me.btnRemoveRowLST = New System.Windows.Forms.Button()
        Me.btnAddRowLST = New System.Windows.Forms.Button()
        Me.tbcFornecedores = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lstFornecedor = New System.Windows.Forms.CheckedListBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lstFornPC = New System.Windows.Forms.ListBox()
        Me.lstCarregarListaMaterial = New System.Windows.Forms.Button()
        Me.TabPageAlt = New System.Windows.Forms.TabPage()
        Me.TabPedidoCotacao = New System.Windows.Forms.TabPage()
        Me.lblUtilActualizacaoCOT = New System.Windows.Forms.Label()
        Me.TotalDocCOT = New System.Windows.Forms.Label()
        Me.lblUtilGravacaoCOT = New System.Windows.Forms.Label()
        Me.cbTodosCOT = New System.Windows.Forms.CheckBox()
        Me.lblObsCOT = New System.Windows.Forms.Label()
        Me.txtObservacoesCOT = New System.Windows.Forms.TextBox()
        Me.lblRegisto2 = New System.Windows.Forms.Label()
        Me.btnRemoveRowPCO = New System.Windows.Forms.Button()
        Me.btnAddRowPCO = New System.Windows.Forms.Button()
        Me.tabEncomendas = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TotalDocENC = New System.Windows.Forms.Label()
        Me.txtDocRecepcao = New System.Windows.Forms.TextBox()
        Me.lblMatEntrega = New System.Windows.Forms.Label()
        Me.lblUtilActualizacaoENC = New System.Windows.Forms.Label()
        Me.lblUtilGravacaoENC = New System.Windows.Forms.Label()
        Me.cbTodosENC = New System.Windows.Forms.CheckBox()
        Me.lblDtFatura = New System.Windows.Forms.Label()
        Me.dtpDataFatura = New System.Windows.Forms.DateTimePicker()
        Me.lblObsENC = New System.Windows.Forms.Label()
        Me.txtObservacoesENC = New System.Windows.Forms.TextBox()
        Me.lblRegisto3 = New System.Windows.Forms.Label()
        Me.btnAplicarTodos = New System.Windows.Forms.Button()
        Me.btnRepartirCustos = New System.Windows.Forms.Button()
        Me.btnRemoveRowENC = New System.Windows.Forms.Button()
        Me.btnAddRowENC = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Group1 = New System.Windows.Forms.GroupBox()
        Me.rb3 = New System.Windows.Forms.RadioButton()
        Me.rb2 = New System.Windows.Forms.RadioButton()
        Me.rb1 = New System.Windows.Forms.RadioButton()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.Group2 = New System.Windows.Forms.GroupBox()
        Me.rb6 = New System.Windows.Forms.RadioButton()
        Me.rb5 = New System.Windows.Forms.RadioButton()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.rb4 = New System.Windows.Forms.RadioButton()
        Me.Group3 = New System.Windows.Forms.GroupBox()
        Me.rb9 = New System.Windows.Forms.RadioButton()
        Me.rb8 = New System.Windows.Forms.RadioButton()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.rb7 = New System.Windows.Forms.RadioButton()
        Me.ssRodape = New System.Windows.Forms.StatusStrip()
        Me.tsAberto = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsPCotacao = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsEncomendado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsInterno = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsProblema = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsMaterialCliente = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tssubcontratado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsNovo = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsAtualizado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsVerificado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsConferido = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Group4 = New System.Windows.Forms.GroupBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.txtArtigo = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtFornecedor = New AxXtremeSuiteControls.AxFlatEdit()
        Me.dtPicker = New AxXtremeSuiteControls.AxDateTimePicker()
        Me.txtProjecto = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtNomeProjecto = New AxXtremeSuiteControls.AxFlatEdit()
        Me.gridArtigos = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosAlt = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosCOT = New AxXtremeReportControl.AxReportControl()
        Me.gridDocsCOT = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosENC = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigosENT = New AxXtremeReportControl.AxReportControl()
        Me.gridDocsENC = New AxXtremeReportControl.AxReportControl()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.AxImageManager1 = New AxXtremeCommandBars.AxImageManager()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        Me.tbcMoldes.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.tabCtlMaterial.SuspendLayout()
        Me.TabListaMaterial.SuspendLayout()
        Me.tabCtlGeral.SuspendLayout()
        Me.TabPageGeral.SuspendLayout()
        Me.tbcFornecedores.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPageAlt.SuspendLayout()
        Me.TabPedidoCotacao.SuspendLayout()
        Me.tabEncomendas.SuspendLayout()
        Me.Group1.SuspendLayout()
        Me.Group2.SuspendLayout()
        Me.Group3.SuspendLayout()
        Me.ssRodape.SuspendLayout()
        Me.Group4.SuspendLayout()
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFornecedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtPicker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtProjecto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNomeProjecto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosAlt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosCOT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridDocsCOT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosENC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosENT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridDocsENC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxImageManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFornecedor
        '
        Me.lblFornecedor.AutoSize = True
        Me.lblFornecedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFornecedor.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblFornecedor.Location = New System.Drawing.Point(747, 15)
        Me.lblFornecedor.Name = "lblFornecedor"
        Me.lblFornecedor.Size = New System.Drawing.Size(112, 25)
        Me.lblFornecedor.TabIndex = 44
        Me.lblFornecedor.Text = "Fornecedor"
        '
        'lblArtigos
        '
        Me.lblArtigos.AutoSize = True
        Me.lblArtigos.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArtigos.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblArtigos.Location = New System.Drawing.Point(6, 6)
        Me.lblArtigos.Name = "lblArtigos"
        Me.lblArtigos.Size = New System.Drawing.Size(73, 25)
        Me.lblArtigos.TabIndex = 45
        Me.lblArtigos.Text = "Artigos"
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.Location = New System.Drawing.Point(81, 66)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(152, 29)
        Me.lblText.TabIndex = 54
        Me.lblText.Text = "TJ Aços, Lda"
        Me.lblText.Visible = False
        '
        'lblObsLST
        '
        Me.lblObsLST.AutoSize = True
        Me.lblObsLST.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObsLST.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblObsLST.Location = New System.Drawing.Point(746, 377)
        Me.lblObsLST.Name = "lblObsLST"
        Me.lblObsLST.Size = New System.Drawing.Size(129, 25)
        Me.lblObsLST.TabIndex = 58
        Me.lblObsLST.Text = "Observações"
        '
        'lblRegisto1
        '
        Me.lblRegisto1.AutoSize = True
        Me.lblRegisto1.Location = New System.Drawing.Point(85, 15)
        Me.lblRegisto1.Name = "lblRegisto1"
        Me.lblRegisto1.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto1.TabIndex = 59
        Me.lblRegisto1.Text = "0 Registos"
        '
        'txtObservacoesLST
        '
        Me.txtObservacoesLST.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoesLST.Location = New System.Drawing.Point(743, 405)
        Me.txtObservacoesLST.Multiline = True
        Me.txtObservacoesLST.Name = "txtObservacoesLST"
        Me.txtObservacoesLST.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoesLST.Size = New System.Drawing.Size(322, 66)
        Me.txtObservacoesLST.TabIndex = 7
        Me.txtObservacoesLST.WordWrap = False
        '
        'tbcMoldes
        '
        Me.tbcMoldes.Controls.Add(Me.TabPage1)
        Me.tbcMoldes.Location = New System.Drawing.Point(15, 82)
        Me.tbcMoldes.Name = "tbcMoldes"
        Me.tbcMoldes.SelectedIndex = 0
        Me.tbcMoldes.Size = New System.Drawing.Size(1097, 90)
        Me.tbcMoldes.TabIndex = 70
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtProjecto)
        Me.TabPage1.Controls.Add(Me.txtNomeProjecto)
        Me.TabPage1.Controls.Add(Me.btnProjecto)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1089, 64)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Projecto"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnProjecto
        '
        Me.btnProjecto.Image = CType(resources.GetObject("btnProjecto.Image"), System.Drawing.Image)
        Me.btnProjecto.Location = New System.Drawing.Point(162, 18)
        Me.btnProjecto.Name = "btnProjecto"
        Me.btnProjecto.Size = New System.Drawing.Size(40, 35)
        Me.btnProjecto.TabIndex = 68
        Me.btnProjecto.UseVisualStyleBackColor = True
        '
        'tabCtlMaterial
        '
        Me.tabCtlMaterial.Controls.Add(Me.TabListaMaterial)
        Me.tabCtlMaterial.Controls.Add(Me.TabPedidoCotacao)
        Me.tabCtlMaterial.Controls.Add(Me.tabEncomendas)
        Me.tabCtlMaterial.Location = New System.Drawing.Point(19, 178)
        Me.tabCtlMaterial.Name = "tabCtlMaterial"
        Me.tabCtlMaterial.SelectedIndex = 0
        Me.tabCtlMaterial.Size = New System.Drawing.Size(1201, 558)
        Me.tabCtlMaterial.TabIndex = 71
        '
        'TabListaMaterial
        '
        Me.TabListaMaterial.Controls.Add(Me.lblDataUltCarregamentoLista)
        Me.TabListaMaterial.Controls.Add(Me.tabCtlGeral)
        Me.TabListaMaterial.Location = New System.Drawing.Point(4, 22)
        Me.TabListaMaterial.Name = "TabListaMaterial"
        Me.TabListaMaterial.Padding = New System.Windows.Forms.Padding(3)
        Me.TabListaMaterial.Size = New System.Drawing.Size(1193, 532)
        Me.TabListaMaterial.TabIndex = 0
        Me.TabListaMaterial.Text = "Lista Material"
        Me.TabListaMaterial.UseVisualStyleBackColor = True
        '
        'lblDataUltCarregamentoLista
        '
        Me.lblDataUltCarregamentoLista.AutoSize = True
        Me.lblDataUltCarregamentoLista.Location = New System.Drawing.Point(115, 493)
        Me.lblDataUltCarregamentoLista.Name = "lblDataUltCarregamentoLista"
        Me.lblDataUltCarregamentoLista.Size = New System.Drawing.Size(0, 13)
        Me.lblDataUltCarregamentoLista.TabIndex = 87
        '
        'tabCtlGeral
        '
        Me.tabCtlGeral.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.tabCtlGeral.Controls.Add(Me.TabPageGeral)
        Me.tabCtlGeral.Controls.Add(Me.TabPageAlt)
        Me.tabCtlGeral.Location = New System.Drawing.Point(0, 0)
        Me.tabCtlGeral.Name = "tabCtlGeral"
        Me.tabCtlGeral.SelectedIndex = 0
        Me.tabCtlGeral.Size = New System.Drawing.Size(1093, 508)
        Me.tabCtlGeral.TabIndex = 85
        '
        'TabPageGeral
        '
        Me.TabPageGeral.Controls.Add(Me.btnPreview)
        Me.TabPageGeral.Controls.Add(Me.btnAplicar)
        Me.TabPageGeral.Controls.Add(Me.lblUtilActualizacaoLST)
        Me.TabPageGeral.Controls.Add(Me.txtArtigo)
        Me.TabPageGeral.Controls.Add(Me.lblArtigos)
        Me.TabPageGeral.Controls.Add(Me.lblUtilGravacaoLST)
        Me.TabPageGeral.Controls.Add(Me.txtFornecedor)
        Me.TabPageGeral.Controls.Add(Me.lblFornecedor)
        Me.TabPageGeral.Controls.Add(Me.gridArtigos)
        Me.TabPageGeral.Controls.Add(Me.txtObservacoesLST)
        Me.TabPageGeral.Controls.Add(Me.ckbVerTodos)
        Me.TabPageGeral.Controls.Add(Me.lblObsLST)
        Me.TabPageGeral.Controls.Add(Me.btnRemoveRowLST)
        Me.TabPageGeral.Controls.Add(Me.lblRegisto1)
        Me.TabPageGeral.Controls.Add(Me.btnAddRowLST)
        Me.TabPageGeral.Controls.Add(Me.tbcFornecedores)
        Me.TabPageGeral.Controls.Add(Me.lstCarregarListaMaterial)
        Me.TabPageGeral.Location = New System.Drawing.Point(4, 4)
        Me.TabPageGeral.Name = "TabPageGeral"
        Me.TabPageGeral.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageGeral.Size = New System.Drawing.Size(1085, 482)
        Me.TabPageGeral.TabIndex = 0
        Me.TabPageGeral.Text = "Geral"
        Me.TabPageGeral.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Image = CType(resources.GetObject("btnPreview.Image"), System.Drawing.Image)
        Me.btnPreview.Location = New System.Drawing.Point(481, 28)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(40, 40)
        Me.btnPreview.TabIndex = 88
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnAplicar
        '
        Me.btnAplicar.Image = CType(resources.GetObject("btnAplicar.Image"), System.Drawing.Image)
        Me.btnAplicar.Location = New System.Drawing.Point(435, 28)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(40, 40)
        Me.btnAplicar.TabIndex = 61
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'lblUtilActualizacaoLST
        '
        Me.lblUtilActualizacaoLST.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUtilActualizacaoLST.Location = New System.Drawing.Point(371, 47)
        Me.lblUtilActualizacaoLST.Name = "lblUtilActualizacaoLST"
        Me.lblUtilActualizacaoLST.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblUtilActualizacaoLST.Size = New System.Drawing.Size(221, 20)
        Me.lblUtilActualizacaoLST.TabIndex = 86
        Me.lblUtilActualizacaoLST.Text = "ZZZZZZZZZ"
        Me.lblUtilActualizacaoLST.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUtilGravacaoLST
        '
        Me.lblUtilGravacaoLST.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUtilGravacaoLST.Location = New System.Drawing.Point(334, 28)
        Me.lblUtilGravacaoLST.Name = "lblUtilGravacaoLST"
        Me.lblUtilGravacaoLST.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblUtilGravacaoLST.Size = New System.Drawing.Size(258, 19)
        Me.lblUtilGravacaoLST.TabIndex = 85
        Me.lblUtilGravacaoLST.Text = "ZZZZZZZZZZ"
        Me.lblUtilGravacaoLST.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ckbVerTodos
        '
        Me.ckbVerTodos.AutoSize = True
        Me.ckbVerTodos.Location = New System.Drawing.Point(950, 20)
        Me.ckbVerTodos.Name = "ckbVerTodos"
        Me.ckbVerTodos.Size = New System.Drawing.Size(109, 17)
        Me.ckbVerTodos.TabIndex = 64
        Me.ckbVerTodos.Text = "Ver Selecionados"
        Me.ckbVerTodos.UseVisualStyleBackColor = True
        '
        'btnRemoveRowLST
        '
        Me.btnRemoveRowLST.Image = CType(resources.GetObject("btnRemoveRowLST.Image"), System.Drawing.Image)
        Me.btnRemoveRowLST.Location = New System.Drawing.Point(598, 28)
        Me.btnRemoveRowLST.Name = "btnRemoveRowLST"
        Me.btnRemoveRowLST.Size = New System.Drawing.Size(40, 40)
        Me.btnRemoveRowLST.TabIndex = 67
        Me.btnRemoveRowLST.UseVisualStyleBackColor = True
        '
        'btnAddRowLST
        '
        Me.btnAddRowLST.Image = CType(resources.GetObject("btnAddRowLST.Image"), System.Drawing.Image)
        Me.btnAddRowLST.Location = New System.Drawing.Point(643, 28)
        Me.btnAddRowLST.Name = "btnAddRowLST"
        Me.btnAddRowLST.Size = New System.Drawing.Size(40, 40)
        Me.btnAddRowLST.TabIndex = 66
        Me.btnAddRowLST.UseVisualStyleBackColor = True
        '
        'tbcFornecedores
        '
        Me.tbcFornecedores.Controls.Add(Me.TabPage2)
        Me.tbcFornecedores.Controls.Add(Me.TabPage3)
        Me.tbcFornecedores.Location = New System.Drawing.Point(747, 80)
        Me.tbcFornecedores.Name = "tbcFornecedores"
        Me.tbcFornecedores.SelectedIndex = 0
        Me.tbcFornecedores.Size = New System.Drawing.Size(322, 288)
        Me.tbcFornecedores.TabIndex = 65
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lstFornecedor)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(314, 262)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "Fornecedores"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lstFornecedor
        '
        Me.lstFornecedor.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstFornecedor.FormattingEnabled = True
        Me.lstFornecedor.Location = New System.Drawing.Point(7, 6)
        Me.lstFornecedor.Name = "lstFornecedor"
        Me.lstFornecedor.Size = New System.Drawing.Size(301, 251)
        Me.lstFornecedor.TabIndex = 63
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lstFornPC)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(314, 262)
        Me.TabPage3.TabIndex = 1
        Me.TabPage3.Text = "Fornecedores P. Cotação"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lstFornPC
        '
        Me.lstFornPC.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.lstFornPC.FormattingEnabled = True
        Me.lstFornPC.ItemHeight = 16
        Me.lstFornPC.Location = New System.Drawing.Point(7, 6)
        Me.lstFornPC.Name = "lstFornPC"
        Me.lstFornPC.Size = New System.Drawing.Size(301, 244)
        Me.lstFornPC.TabIndex = 66
        '
        'lstCarregarListaMaterial
        '
        Me.lstCarregarListaMaterial.Image = CType(resources.GetObject("lstCarregarListaMaterial.Image"), System.Drawing.Image)
        Me.lstCarregarListaMaterial.Location = New System.Drawing.Point(694, 28)
        Me.lstCarregarListaMaterial.Name = "lstCarregarListaMaterial"
        Me.lstCarregarListaMaterial.Size = New System.Drawing.Size(40, 40)
        Me.lstCarregarListaMaterial.TabIndex = 62
        Me.lstCarregarListaMaterial.UseVisualStyleBackColor = True
        '
        'TabPageAlt
        '
        Me.TabPageAlt.Controls.Add(Me.gridArtigosAlt)
        Me.TabPageAlt.Location = New System.Drawing.Point(4, 4)
        Me.TabPageAlt.Name = "TabPageAlt"
        Me.TabPageAlt.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageAlt.Size = New System.Drawing.Size(1085, 482)
        Me.TabPageAlt.TabIndex = 1
        Me.TabPageAlt.Text = "Alterações"
        Me.TabPageAlt.UseVisualStyleBackColor = True
        '
        'TabPedidoCotacao
        '
        Me.TabPedidoCotacao.Controls.Add(Me.lblUtilActualizacaoCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.TotalDocCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.lblUtilGravacaoCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.cbTodosCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.lblObsCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.txtObservacoesCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.lblRegisto2)
        Me.TabPedidoCotacao.Controls.Add(Me.btnRemoveRowPCO)
        Me.TabPedidoCotacao.Controls.Add(Me.btnAddRowPCO)
        Me.TabPedidoCotacao.Controls.Add(Me.gridArtigosCOT)
        Me.TabPedidoCotacao.Controls.Add(Me.gridDocsCOT)
        Me.TabPedidoCotacao.Location = New System.Drawing.Point(4, 22)
        Me.TabPedidoCotacao.Name = "TabPedidoCotacao"
        Me.TabPedidoCotacao.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPedidoCotacao.Size = New System.Drawing.Size(1193, 532)
        Me.TabPedidoCotacao.TabIndex = 1
        Me.TabPedidoCotacao.Text = "Pedido Cotação"
        Me.TabPedidoCotacao.UseVisualStyleBackColor = True
        '
        'lblUtilActualizacaoCOT
        '
        Me.lblUtilActualizacaoCOT.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUtilActualizacaoCOT.Location = New System.Drawing.Point(421, 24)
        Me.lblUtilActualizacaoCOT.Name = "lblUtilActualizacaoCOT"
        Me.lblUtilActualizacaoCOT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblUtilActualizacaoCOT.Size = New System.Drawing.Size(578, 20)
        Me.lblUtilActualizacaoCOT.TabIndex = 88
        Me.lblUtilActualizacaoCOT.Text = "ZZZZZZZZZZZ"
        Me.lblUtilActualizacaoCOT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TotalDocCOT
        '
        Me.TotalDocCOT.AutoSize = True
        Me.TotalDocCOT.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalDocCOT.Location = New System.Drawing.Point(346, 15)
        Me.TotalDocCOT.Name = "TotalDocCOT"
        Me.TotalDocCOT.Size = New System.Drawing.Size(54, 20)
        Me.TotalDocCOT.TabIndex = 75
        Me.TotalDocCOT.Text = "Total:"
        '
        'lblUtilGravacaoCOT
        '
        Me.lblUtilGravacaoCOT.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUtilGravacaoCOT.Location = New System.Drawing.Point(526, 4)
        Me.lblUtilGravacaoCOT.Name = "lblUtilGravacaoCOT"
        Me.lblUtilGravacaoCOT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblUtilGravacaoCOT.Size = New System.Drawing.Size(473, 19)
        Me.lblUtilGravacaoCOT.TabIndex = 87
        Me.lblUtilGravacaoCOT.Text = "ZZZZZZZZZZ"
        Me.lblUtilGravacaoCOT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbTodosCOT
        '
        Me.cbTodosCOT.AutoSize = True
        Me.cbTodosCOT.Location = New System.Drawing.Point(225, 15)
        Me.cbTodosCOT.Name = "cbTodosCOT"
        Me.cbTodosCOT.Size = New System.Drawing.Size(94, 17)
        Me.cbTodosCOT.TabIndex = 66
        Me.cbTodosCOT.Text = "Mostrar Todos"
        Me.cbTodosCOT.UseVisualStyleBackColor = True
        '
        'lblObsCOT
        '
        Me.lblObsCOT.AutoSize = True
        Me.lblObsCOT.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObsCOT.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblObsCOT.Location = New System.Drawing.Point(345, 402)
        Me.lblObsCOT.Name = "lblObsCOT"
        Me.lblObsCOT.Size = New System.Drawing.Size(129, 25)
        Me.lblObsCOT.TabIndex = 63
        Me.lblObsCOT.Text = "Observações"
        '
        'txtObservacoesCOT
        '
        Me.txtObservacoesCOT.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoesCOT.Location = New System.Drawing.Point(345, 430)
        Me.txtObservacoesCOT.Multiline = True
        Me.txtObservacoesCOT.Name = "txtObservacoesCOT"
        Me.txtObservacoesCOT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoesCOT.Size = New System.Drawing.Size(738, 71)
        Me.txtObservacoesCOT.TabIndex = 62
        Me.txtObservacoesCOT.WordWrap = False
        '
        'lblRegisto2
        '
        Me.lblRegisto2.AutoSize = True
        Me.lblRegisto2.Location = New System.Drawing.Point(6, 15)
        Me.lblRegisto2.Name = "lblRegisto2"
        Me.lblRegisto2.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto2.TabIndex = 60
        Me.lblRegisto2.Text = "0 Registos"
        '
        'btnRemoveRowPCO
        '
        Me.btnRemoveRowPCO.Image = CType(resources.GetObject("btnRemoveRowPCO.Image"), System.Drawing.Image)
        Me.btnRemoveRowPCO.Location = New System.Drawing.Point(999, 3)
        Me.btnRemoveRowPCO.Name = "btnRemoveRowPCO"
        Me.btnRemoveRowPCO.Size = New System.Drawing.Size(40, 40)
        Me.btnRemoveRowPCO.TabIndex = 69
        Me.btnRemoveRowPCO.UseVisualStyleBackColor = True
        '
        'btnAddRowPCO
        '
        Me.btnAddRowPCO.Image = CType(resources.GetObject("btnAddRowPCO.Image"), System.Drawing.Image)
        Me.btnAddRowPCO.Location = New System.Drawing.Point(1043, 4)
        Me.btnAddRowPCO.Name = "btnAddRowPCO"
        Me.btnAddRowPCO.Size = New System.Drawing.Size(40, 40)
        Me.btnAddRowPCO.TabIndex = 68
        Me.btnAddRowPCO.UseVisualStyleBackColor = True
        '
        'tabEncomendas
        '
        Me.tabEncomendas.Controls.Add(Me.dtPicker)
        Me.tabEncomendas.Controls.Add(Me.Label1)
        Me.tabEncomendas.Controls.Add(Me.TotalDocENC)
        Me.tabEncomendas.Controls.Add(Me.txtDocRecepcao)
        Me.tabEncomendas.Controls.Add(Me.lblMatEntrega)
        Me.tabEncomendas.Controls.Add(Me.lblUtilActualizacaoENC)
        Me.tabEncomendas.Controls.Add(Me.lblUtilGravacaoENC)
        Me.tabEncomendas.Controls.Add(Me.cbTodosENC)
        Me.tabEncomendas.Controls.Add(Me.lblDtFatura)
        Me.tabEncomendas.Controls.Add(Me.dtpDataFatura)
        Me.tabEncomendas.Controls.Add(Me.lblObsENC)
        Me.tabEncomendas.Controls.Add(Me.txtObservacoesENC)
        Me.tabEncomendas.Controls.Add(Me.lblRegisto3)
        Me.tabEncomendas.Controls.Add(Me.btnAplicarTodos)
        Me.tabEncomendas.Controls.Add(Me.btnRepartirCustos)
        Me.tabEncomendas.Controls.Add(Me.btnRemoveRowENC)
        Me.tabEncomendas.Controls.Add(Me.btnAddRowENC)
        Me.tabEncomendas.Controls.Add(Me.gridArtigosENC)
        Me.tabEncomendas.Controls.Add(Me.gridArtigosENT)
        Me.tabEncomendas.Controls.Add(Me.gridDocsENC)
        Me.tabEncomendas.Location = New System.Drawing.Point(4, 22)
        Me.tabEncomendas.Name = "tabEncomendas"
        Me.tabEncomendas.Padding = New System.Windows.Forms.Padding(3)
        Me.tabEncomendas.Size = New System.Drawing.Size(1193, 532)
        Me.tabEncomendas.TabIndex = 2
        Me.tabEncomendas.Text = "Encomendas"
        Me.tabEncomendas.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(424, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 25)
        Me.Label1.TabIndex = 94
        Me.Label1.Text = "Doc Recp."
        '
        'TotalDocENC
        '
        Me.TotalDocENC.AutoSize = True
        Me.TotalDocENC.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalDocENC.Location = New System.Drawing.Point(675, 15)
        Me.TotalDocENC.Name = "TotalDocENC"
        Me.TotalDocENC.Size = New System.Drawing.Size(54, 20)
        Me.TotalDocENC.TabIndex = 74
        Me.TotalDocENC.Text = "Total:"
        '
        'txtDocRecepcao
        '
        Me.txtDocRecepcao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.txtDocRecepcao.Location = New System.Drawing.Point(532, 14)
        Me.txtDocRecepcao.Name = "txtDocRecepcao"
        Me.txtDocRecepcao.Size = New System.Drawing.Size(137, 30)
        Me.txtDocRecepcao.TabIndex = 93
        '
        'lblMatEntrega
        '
        Me.lblMatEntrega.AutoSize = True
        Me.lblMatEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMatEntrega.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblMatEntrega.Location = New System.Drawing.Point(706, 297)
        Me.lblMatEntrega.Name = "lblMatEntrega"
        Me.lblMatEntrega.Size = New System.Drawing.Size(165, 25)
        Me.lblMatEntrega.TabIndex = 92
        Me.lblMatEntrega.Text = "Material Entregue"
        '
        'lblUtilActualizacaoENC
        '
        Me.lblUtilActualizacaoENC.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUtilActualizacaoENC.Location = New System.Drawing.Point(790, 25)
        Me.lblUtilActualizacaoENC.Name = "lblUtilActualizacaoENC"
        Me.lblUtilActualizacaoENC.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblUtilActualizacaoENC.Size = New System.Drawing.Size(212, 20)
        Me.lblUtilActualizacaoENC.TabIndex = 90
        Me.lblUtilActualizacaoENC.Text = "ZZZZZZZZZZ"
        Me.lblUtilActualizacaoENC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUtilGravacaoENC
        '
        Me.lblUtilGravacaoENC.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUtilGravacaoENC.Location = New System.Drawing.Point(750, 6)
        Me.lblUtilGravacaoENC.Name = "lblUtilGravacaoENC"
        Me.lblUtilGravacaoENC.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblUtilGravacaoENC.Size = New System.Drawing.Size(252, 19)
        Me.lblUtilGravacaoENC.TabIndex = 89
        Me.lblUtilGravacaoENC.Text = "ZZZZZZZZZZ"
        Me.lblUtilGravacaoENC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbTodosENC
        '
        Me.cbTodosENC.AutoSize = True
        Me.cbTodosENC.Location = New System.Drawing.Point(324, 14)
        Me.cbTodosENC.Name = "cbTodosENC"
        Me.cbTodosENC.Size = New System.Drawing.Size(94, 17)
        Me.cbTodosENC.TabIndex = 71
        Me.cbTodosENC.Text = "Mostrar Todos"
        Me.cbTodosENC.UseVisualStyleBackColor = True
        '
        'lblDtFatura
        '
        Me.lblDtFatura.AutoSize = True
        Me.lblDtFatura.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDtFatura.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblDtFatura.Location = New System.Drawing.Point(991, 300)
        Me.lblDtFatura.Name = "lblDtFatura"
        Me.lblDtFatura.Size = New System.Drawing.Size(89, 16)
        Me.lblDtFatura.TabIndex = 68
        Me.lblDtFatura.Text = "Data Fatura"
        '
        'dtpDataFatura
        '
        Me.dtpDataFatura.CustomFormat = ""
        Me.dtpDataFatura.Location = New System.Drawing.Point(1091, 300)
        Me.dtpDataFatura.Name = "dtpDataFatura"
        Me.dtpDataFatura.ShowCheckBox = True
        Me.dtpDataFatura.Size = New System.Drawing.Size(96, 20)
        Me.dtpDataFatura.TabIndex = 67
        '
        'lblObsENC
        '
        Me.lblObsENC.AutoSize = True
        Me.lblObsENC.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObsENC.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblObsENC.Location = New System.Drawing.Point(423, 300)
        Me.lblObsENC.Name = "lblObsENC"
        Me.lblObsENC.Size = New System.Drawing.Size(129, 25)
        Me.lblObsENC.TabIndex = 66
        Me.lblObsENC.Text = "Observações"
        '
        'txtObservacoesENC
        '
        Me.txtObservacoesENC.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoesENC.Location = New System.Drawing.Point(428, 325)
        Me.txtObservacoesENC.Multiline = True
        Me.txtObservacoesENC.Name = "txtObservacoesENC"
        Me.txtObservacoesENC.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoesENC.Size = New System.Drawing.Size(277, 192)
        Me.txtObservacoesENC.TabIndex = 65
        Me.txtObservacoesENC.WordWrap = False
        '
        'lblRegisto3
        '
        Me.lblRegisto3.AutoSize = True
        Me.lblRegisto3.Location = New System.Drawing.Point(6, 15)
        Me.lblRegisto3.Name = "lblRegisto3"
        Me.lblRegisto3.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto3.TabIndex = 64
        Me.lblRegisto3.Text = "0 Registos"
        '
        'btnAplicarTodos
        '
        Me.btnAplicarTodos.Image = CType(resources.GetObject("btnAplicarTodos.Image"), System.Drawing.Image)
        Me.btnAplicarTodos.Location = New System.Drawing.Point(1058, 6)
        Me.btnAplicarTodos.Name = "btnAplicarTodos"
        Me.btnAplicarTodos.Size = New System.Drawing.Size(39, 38)
        Me.btnAplicarTodos.TabIndex = 100
        Me.btnAplicarTodos.UseVisualStyleBackColor = True
        '
        'btnRepartirCustos
        '
        Me.btnRepartirCustos.Image = CType(resources.GetObject("btnRepartirCustos.Image"), System.Drawing.Image)
        Me.btnRepartirCustos.Location = New System.Drawing.Point(1015, 6)
        Me.btnRepartirCustos.Name = "btnRepartirCustos"
        Me.btnRepartirCustos.Size = New System.Drawing.Size(37, 38)
        Me.btnRepartirCustos.TabIndex = 101
        Me.btnRepartirCustos.UseVisualStyleBackColor = True
        '
        'btnRemoveRowENC
        '
        Me.btnRemoveRowENC.Image = CType(resources.GetObject("btnRemoveRowENC.Image"), System.Drawing.Image)
        Me.btnRemoveRowENC.Location = New System.Drawing.Point(1103, 5)
        Me.btnRemoveRowENC.Name = "btnRemoveRowENC"
        Me.btnRemoveRowENC.Size = New System.Drawing.Size(40, 40)
        Me.btnRemoveRowENC.TabIndex = 73
        Me.btnRemoveRowENC.UseVisualStyleBackColor = True
        '
        'btnAddRowENC
        '
        Me.btnAddRowENC.Image = CType(resources.GetObject("btnAddRowENC.Image"), System.Drawing.Image)
        Me.btnAddRowENC.Location = New System.Drawing.Point(1147, 6)
        Me.btnAddRowENC.Name = "btnAddRowENC"
        Me.btnAddRowENC.Size = New System.Drawing.Size(40, 40)
        Me.btnAddRowENC.TabIndex = 72
        Me.btnAddRowENC.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = resources.GetString("OpenFileDialog1.Filter")
        Me.OpenFileDialog1.FilterIndex = 2
        '
        'Group1
        '
        Me.Group1.Controls.Add(Me.rb3)
        Me.Group1.Controls.Add(Me.rb2)
        Me.Group1.Controls.Add(Me.rb1)
        Me.Group1.Controls.Add(Me.btn1)
        Me.Group1.Location = New System.Drawing.Point(496, 2)
        Me.Group1.Name = "Group1"
        Me.Group1.Size = New System.Drawing.Size(200, 96)
        Me.Group1.TabIndex = 78
        Me.Group1.TabStop = False
        Me.Group1.Text = "Lista Material"
        '
        'rb3
        '
        Me.rb3.AutoSize = True
        Me.rb3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb3.Location = New System.Drawing.Point(120, 68)
        Me.rb3.Name = "rb3"
        Me.rb3.Size = New System.Drawing.Size(66, 21)
        Me.rb3.TabIndex = 79
        Me.rb3.TabStop = True
        Me.rb3.Text = "Enviar"
        Me.rb3.UseVisualStyleBackColor = True
        '
        'rb2
        '
        Me.rb2.AutoSize = True
        Me.rb2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb2.Location = New System.Drawing.Point(120, 45)
        Me.rb2.Name = "rb2"
        Me.rb2.Size = New System.Drawing.Size(75, 21)
        Me.rb2.TabIndex = 75
        Me.rb2.TabStop = True
        Me.rb2.Text = "Imprimir"
        Me.rb2.UseVisualStyleBackColor = True
        '
        'rb1
        '
        Me.rb1.AutoSize = True
        Me.rb1.Checked = True
        Me.rb1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb1.Location = New System.Drawing.Point(120, 22)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(70, 21)
        Me.rb1.TabIndex = 74
        Me.rb1.TabStop = True
        Me.rb1.Text = "Gravar"
        Me.rb1.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.btn1.Image = CType(resources.GetObject("btn1.Image"), System.Drawing.Image)
        Me.btn1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn1.Location = New System.Drawing.Point(6, 25)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(108, 59)
        Me.btn1.TabIndex = 73
        Me.btn1.Text = "Lista Material"
        Me.btn1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn1.UseVisualStyleBackColor = True
        '
        'Group2
        '
        Me.Group2.Controls.Add(Me.rb6)
        Me.Group2.Controls.Add(Me.rb5)
        Me.Group2.Controls.Add(Me.btn2)
        Me.Group2.Controls.Add(Me.rb4)
        Me.Group2.Location = New System.Drawing.Point(702, 2)
        Me.Group2.Name = "Group2"
        Me.Group2.Size = New System.Drawing.Size(200, 96)
        Me.Group2.TabIndex = 80
        Me.Group2.TabStop = False
        Me.Group2.Text = "Pedido Cotação"
        '
        'rb6
        '
        Me.rb6.AutoSize = True
        Me.rb6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb6.Location = New System.Drawing.Point(120, 68)
        Me.rb6.Name = "rb6"
        Me.rb6.Size = New System.Drawing.Size(66, 21)
        Me.rb6.TabIndex = 79
        Me.rb6.TabStop = True
        Me.rb6.Text = "Enviar"
        Me.rb6.UseVisualStyleBackColor = True
        '
        'rb5
        '
        Me.rb5.AutoSize = True
        Me.rb5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb5.Location = New System.Drawing.Point(120, 45)
        Me.rb5.Name = "rb5"
        Me.rb5.Size = New System.Drawing.Size(75, 21)
        Me.rb5.TabIndex = 75
        Me.rb5.TabStop = True
        Me.rb5.Text = "Imprimir"
        Me.rb5.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.Image = CType(resources.GetObject("btn2.Image"), System.Drawing.Image)
        Me.btn2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn2.Location = New System.Drawing.Point(6, 25)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(108, 59)
        Me.btn2.TabIndex = 12
        Me.btn2.Text = " P. Cotação"
        Me.btn2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn2.UseVisualStyleBackColor = True
        '
        'rb4
        '
        Me.rb4.AutoSize = True
        Me.rb4.Checked = True
        Me.rb4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb4.Location = New System.Drawing.Point(120, 22)
        Me.rb4.Name = "rb4"
        Me.rb4.Size = New System.Drawing.Size(70, 21)
        Me.rb4.TabIndex = 74
        Me.rb4.TabStop = True
        Me.rb4.Text = "Gravar"
        Me.rb4.UseVisualStyleBackColor = True
        '
        'Group3
        '
        Me.Group3.Controls.Add(Me.rb9)
        Me.Group3.Controls.Add(Me.rb8)
        Me.Group3.Controls.Add(Me.btn3)
        Me.Group3.Controls.Add(Me.rb7)
        Me.Group3.Location = New System.Drawing.Point(908, 2)
        Me.Group3.Name = "Group3"
        Me.Group3.Size = New System.Drawing.Size(200, 96)
        Me.Group3.TabIndex = 81
        Me.Group3.TabStop = False
        Me.Group3.Text = "Encomenda"
        '
        'rb9
        '
        Me.rb9.AutoSize = True
        Me.rb9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb9.Location = New System.Drawing.Point(120, 68)
        Me.rb9.Name = "rb9"
        Me.rb9.Size = New System.Drawing.Size(66, 21)
        Me.rb9.TabIndex = 79
        Me.rb9.TabStop = True
        Me.rb9.Text = "Enviar"
        Me.rb9.UseVisualStyleBackColor = True
        '
        'rb8
        '
        Me.rb8.AutoSize = True
        Me.rb8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb8.Location = New System.Drawing.Point(120, 45)
        Me.rb8.Name = "rb8"
        Me.rb8.Size = New System.Drawing.Size(75, 21)
        Me.rb8.TabIndex = 75
        Me.rb8.TabStop = True
        Me.rb8.Text = "Imprimir"
        Me.rb8.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn3.Image = CType(resources.GetObject("btn3.Image"), System.Drawing.Image)
        Me.btn3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn3.Location = New System.Drawing.Point(6, 25)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(108, 59)
        Me.btn3.TabIndex = 12
        Me.btn3.Text = "Encomendar"
        Me.btn3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn3.UseVisualStyleBackColor = True
        '
        'rb7
        '
        Me.rb7.AutoSize = True
        Me.rb7.Checked = True
        Me.rb7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb7.Location = New System.Drawing.Point(120, 22)
        Me.rb7.Name = "rb7"
        Me.rb7.Size = New System.Drawing.Size(70, 21)
        Me.rb7.TabIndex = 74
        Me.rb7.TabStop = True
        Me.rb7.Text = "Gravar"
        Me.rb7.UseVisualStyleBackColor = True
        '
        'ssRodape
        '
        Me.ssRodape.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsAberto, Me.tsPCotacao, Me.tsEncomendado, Me.tsInterno, Me.tsProblema, Me.tsMaterialCliente, Me.tssubcontratado, Me.tsNovo, Me.tsAtualizado, Me.tsVerificado, Me.tsConferido, Me.ToolStripStatusLabel1})
        Me.ssRodape.Location = New System.Drawing.Point(0, 754)
        Me.ssRodape.Name = "ssRodape"
        Me.ssRodape.Size = New System.Drawing.Size(1484, 22)
        Me.ssRodape.TabIndex = 82
        Me.ssRodape.Text = "ssRodape"
        '
        'tsAberto
        '
        Me.tsAberto.Name = "tsAberto"
        Me.tsAberto.Size = New System.Drawing.Size(77, 17)
        Me.tsAberto.Text = "     Normal     "
        '
        'tsPCotacao
        '
        Me.tsPCotacao.Name = "tsPCotacao"
        Me.tsPCotacao.Size = New System.Drawing.Size(94, 17)
        Me.tsPCotacao.Tag = "C"
        Me.tsPCotacao.Text = "     P. Cotação     "
        '
        'tsEncomendado
        '
        Me.tsEncomendado.Name = "tsEncomendado"
        Me.tsEncomendado.Size = New System.Drawing.Size(114, 17)
        Me.tsEncomendado.Tag = "E"
        Me.tsEncomendado.Text = "     Encomendado     "
        '
        'tsInterno
        '
        Me.tsInterno.Name = "tsInterno"
        Me.tsInterno.Size = New System.Drawing.Size(75, 17)
        Me.tsInterno.Tag = "I"
        Me.tsInterno.Text = "     Interno     "
        '
        'tsProblema
        '
        Me.tsProblema.Name = "tsProblema"
        Me.tsProblema.Size = New System.Drawing.Size(88, 17)
        Me.tsProblema.Tag = "P"
        Me.tsProblema.Text = "     Problema     "
        '
        'tsMaterialCliente
        '
        Me.tsMaterialCliente.Name = "tsMaterialCliente"
        Me.tsMaterialCliente.Size = New System.Drawing.Size(90, 17)
        Me.tsMaterialCliente.Tag = "L"
        Me.tsMaterialCliente.Text = "Material Cliente"
        '
        'tssubcontratado
        '
        Me.tssubcontratado.Name = "tssubcontratado"
        Me.tssubcontratado.Size = New System.Drawing.Size(115, 17)
        Me.tssubcontratado.Tag = "S"
        Me.tssubcontratado.Text = "     Subcontratado     "
        '
        'tsNovo
        '
        Me.tsNovo.Name = "tsNovo"
        Me.tsNovo.Size = New System.Drawing.Size(66, 17)
        Me.tsNovo.Tag = "N"
        Me.tsNovo.Text = "     Novo     "
        '
        'tsAtualizado
        '
        Me.tsAtualizado.Name = "tsAtualizado"
        Me.tsAtualizado.Size = New System.Drawing.Size(93, 17)
        Me.tsAtualizado.Tag = "M"
        Me.tsAtualizado.Text = "     Atualizado     "
        '
        'tsVerificado
        '
        Me.tsVerificado.Name = "tsVerificado"
        Me.tsVerificado.Size = New System.Drawing.Size(89, 17)
        Me.tsVerificado.Tag = "V"
        Me.tsVerificado.Text = "     Verificado     "
        '
        'tsConferido
        '
        Me.tsConferido.Name = "tsConferido"
        Me.tsConferido.Size = New System.Drawing.Size(90, 17)
        Me.tsConferido.Tag = "C"
        Me.tsConferido.Text = "     Conferido     "
        Me.tsConferido.ToolTipText = "     Conferido     "
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(320, 17)
        Me.ToolStripStatusLabel1.Text = "Anphis - Novas Tec. em Inf. e Tel . Lda @2018 -  Licenciado: "
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(614, 172)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(488, 23)
        Me.ProgressBar1.TabIndex = 85
        Me.ProgressBar1.Visible = False
        '
        'Group4
        '
        Me.Group4.Controls.Add(Me.RadioButton3)
        Me.Group4.Controls.Add(Me.btn4)
        Me.Group4.Location = New System.Drawing.Point(290, 2)
        Me.Group4.Name = "Group4"
        Me.Group4.Size = New System.Drawing.Size(200, 96)
        Me.Group4.TabIndex = 86
        Me.Group4.TabStop = False
        Me.Group4.Text = "Saida Material"
        Me.Group4.Visible = False
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Checked = True
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(120, 45)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(70, 21)
        Me.RadioButton3.TabIndex = 74
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Gravar"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.btn4.Image = CType(resources.GetObject("btn4.Image"), System.Drawing.Image)
        Me.btn4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn4.Location = New System.Drawing.Point(6, 25)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(108, 59)
        Me.btn4.TabIndex = 73
        Me.btn4.Text = "Saída Material"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn4.UseVisualStyleBackColor = True
        '
        'txtArtigo
        '
        Me.txtArtigo.Location = New System.Drawing.Point(5, 36)
        Me.txtArtigo.Name = "txtArtigo"
        Me.txtArtigo.OcxState = CType(resources.GetObject("txtArtigo.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtArtigo.Size = New System.Drawing.Size(421, 32)
        Me.txtArtigo.TabIndex = 70
        '
        'txtFornecedor
        '
        Me.txtFornecedor.Location = New System.Drawing.Point(747, 42)
        Me.txtFornecedor.Name = "txtFornecedor"
        Me.txtFornecedor.OcxState = CType(resources.GetObject("txtFornecedor.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtFornecedor.Size = New System.Drawing.Size(328, 32)
        Me.txtFornecedor.TabIndex = 69
        '
        'dtPicker
        '
        Me.dtPicker.Location = New System.Drawing.Point(750, 15)
        Me.dtPicker.Name = "dtPicker"
        Me.dtPicker.OcxState = CType(resources.GetObject("dtPicker.OcxState"), System.Windows.Forms.AxHost.State)
        Me.dtPicker.Size = New System.Drawing.Size(100, 22)
        Me.dtPicker.TabIndex = 96
        Me.dtPicker.Visible = False
        '
        'txtProjecto
        '
        Me.txtProjecto.Location = New System.Drawing.Point(6, 18)
        Me.txtProjecto.Name = "txtProjecto"
        Me.txtProjecto.OcxState = CType(resources.GetObject("txtProjecto.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtProjecto.Size = New System.Drawing.Size(151, 35)
        Me.txtProjecto.TabIndex = 70
        '
        'txtNomeProjecto
        '
        Me.txtNomeProjecto.Location = New System.Drawing.Point(212, 18)
        Me.txtNomeProjecto.Name = "txtNomeProjecto"
        Me.txtNomeProjecto.OcxState = CType(resources.GetObject("txtNomeProjecto.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtNomeProjecto.Size = New System.Drawing.Size(858, 35)
        Me.txtNomeProjecto.TabIndex = 69
        '
        'gridArtigos
        '
        Me.gridArtigos.Location = New System.Drawing.Point(7, 74)
        Me.gridArtigos.Name = "gridArtigos"
        Me.gridArtigos.OcxState = CType(resources.GetObject("gridArtigos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigos.Size = New System.Drawing.Size(727, 397)
        Me.gridArtigos.TabIndex = 68
        Me.gridArtigos.Tag = "0"
        '
        'gridArtigosAlt
        '
        Me.gridArtigosAlt.Location = New System.Drawing.Point(6, 6)
        Me.gridArtigosAlt.Name = "gridArtigosAlt"
        Me.gridArtigosAlt.OcxState = CType(resources.GetObject("gridArtigosAlt.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosAlt.Size = New System.Drawing.Size(1069, 473)
        Me.gridArtigosAlt.TabIndex = 69
        Me.gridArtigosAlt.Tag = "0"
        '
        'gridArtigosCOT
        '
        Me.gridArtigosCOT.Location = New System.Drawing.Point(344, 47)
        Me.gridArtigosCOT.Name = "gridArtigosCOT"
        Me.gridArtigosCOT.OcxState = CType(resources.GetObject("gridArtigosCOT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosCOT.Size = New System.Drawing.Size(739, 352)
        Me.gridArtigosCOT.TabIndex = 65
        Me.gridArtigosCOT.Tag = "0"
        '
        'gridDocsCOT
        '
        Me.gridDocsCOT.Location = New System.Drawing.Point(7, 47)
        Me.gridDocsCOT.Name = "gridDocsCOT"
        Me.gridDocsCOT.OcxState = CType(resources.GetObject("gridDocsCOT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridDocsCOT.Size = New System.Drawing.Size(332, 454)
        Me.gridDocsCOT.TabIndex = 64
        Me.gridDocsCOT.Tag = "COT"
        '
        'gridArtigosENC
        '
        Me.gridArtigosENC.Location = New System.Drawing.Point(428, 47)
        Me.gridArtigosENC.Name = "gridArtigosENC"
        Me.gridArtigosENC.OcxState = CType(resources.GetObject("gridArtigosENC.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosENC.Size = New System.Drawing.Size(759, 247)
        Me.gridArtigosENC.TabIndex = 70
        Me.gridArtigosENC.Tag = "0"
        '
        'gridArtigosENT
        '
        Me.gridArtigosENT.Location = New System.Drawing.Point(711, 325)
        Me.gridArtigosENT.Name = "gridArtigosENT"
        Me.gridArtigosENT.OcxState = CType(resources.GetObject("gridArtigosENT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosENT.Size = New System.Drawing.Size(476, 192)
        Me.gridArtigosENT.TabIndex = 91
        Me.gridArtigosENT.Tag = ""
        '
        'gridDocsENC
        '
        Me.gridDocsENC.Location = New System.Drawing.Point(7, 47)
        Me.gridDocsENC.Name = "gridDocsENC"
        Me.gridDocsENC.OcxState = CType(resources.GetObject("gridDocsENC.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridDocsENC.Size = New System.Drawing.Size(410, 470)
        Me.gridDocsENC.TabIndex = 69
        Me.gridDocsENC.Tag = "ENC"
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(12, 52)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 84
        '
        'AxImageManager1
        '
        Me.AxImageManager1.Enabled = True
        Me.AxImageManager1.Location = New System.Drawing.Point(42, 52)
        Me.AxImageManager1.Name = "AxImageManager1"
        Me.AxImageManager1.OcxState = CType(resources.GetObject("AxImageManager1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxImageManager1.Size = New System.Drawing.Size(24, 24)
        Me.AxImageManager1.TabIndex = 83
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(12, 2)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(373, 55)
        Me.CommandBarsFrame1.TabIndex = 35
        '
        'FrmListaEncomendas
        '
        Me.ClientSize = New System.Drawing.Size(1484, 776)
        Me.Controls.Add(Me.Group4)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.tabCtlMaterial)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.AxImageManager1)
        Me.Controls.Add(Me.ssRodape)
        Me.Controls.Add(Me.Group3)
        Me.Controls.Add(Me.Group2)
        Me.Controls.Add(Me.Group1)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.tbcMoldes)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(1143, 726)
        Me.Name = "FrmListaEncomendas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anphis - Gestão Materiais - "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcMoldes.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.tabCtlMaterial.ResumeLayout(False)
        Me.TabListaMaterial.ResumeLayout(False)
        Me.TabListaMaterial.PerformLayout()
        Me.tabCtlGeral.ResumeLayout(False)
        Me.TabPageGeral.ResumeLayout(False)
        Me.TabPageGeral.PerformLayout()
        Me.tbcFornecedores.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPageAlt.ResumeLayout(False)
        Me.TabPedidoCotacao.ResumeLayout(False)
        Me.TabPedidoCotacao.PerformLayout()
        Me.tabEncomendas.ResumeLayout(False)
        Me.tabEncomendas.PerformLayout()
        Me.Group1.ResumeLayout(False)
        Me.Group1.PerformLayout()
        Me.Group2.ResumeLayout(False)
        Me.Group2.PerformLayout()
        Me.Group3.ResumeLayout(False)
        Me.Group3.PerformLayout()
        Me.ssRodape.ResumeLayout(False)
        Me.ssRodape.PerformLayout()
        Me.Group4.ResumeLayout(False)
        Me.Group4.PerformLayout()
        CType(Me.txtArtigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFornecedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtPicker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtProjecto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNomeProjecto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosAlt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosCOT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridDocsCOT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosENC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosENT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridDocsENC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxImageManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents lblFornecedor As System.Windows.Forms.Label
    Friend WithEvents lblArtigos As System.Windows.Forms.Label
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents lblObsLST As System.Windows.Forms.Label
    Friend WithEvents lblRegisto1 As System.Windows.Forms.Label
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents txtObservacoesLST As System.Windows.Forms.TextBox
    Friend WithEvents tbcMoldes As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnProjecto As System.Windows.Forms.Button
    Friend WithEvents tabCtlMaterial As System.Windows.Forms.TabControl
    Friend WithEvents TabListaMaterial As System.Windows.Forms.TabPage
    Friend WithEvents TabPedidoCotacao As System.Windows.Forms.TabPage
    Friend WithEvents lstCarregarListaMaterial As System.Windows.Forms.Button
    Friend WithEvents lblRegisto2 As System.Windows.Forms.Label
    Friend WithEvents lstFornecedor As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabEncomendas As System.Windows.Forms.TabPage
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents Group1 As System.Windows.Forms.GroupBox
    Friend WithEvents rb3 As System.Windows.Forms.RadioButton
    Friend WithEvents rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents Group2 As System.Windows.Forms.GroupBox
    Friend WithEvents rb6 As System.Windows.Forms.RadioButton
    Friend WithEvents rb5 As System.Windows.Forms.RadioButton
    Friend WithEvents rb4 As System.Windows.Forms.RadioButton
    Friend WithEvents lblRegisto3 As System.Windows.Forms.Label
    Friend WithEvents Group3 As System.Windows.Forms.GroupBox
    Friend WithEvents rb9 As System.Windows.Forms.RadioButton
    Friend WithEvents rb8 As System.Windows.Forms.RadioButton
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents rb7 As System.Windows.Forms.RadioButton
    Friend WithEvents lblObsCOT As System.Windows.Forms.Label
    Friend WithEvents txtObservacoesCOT As System.Windows.Forms.TextBox
    Friend WithEvents lblObsENC As System.Windows.Forms.Label
    Friend WithEvents txtObservacoesENC As System.Windows.Forms.TextBox
    Friend WithEvents tbcFornecedores As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents lstFornPC As System.Windows.Forms.ListBox
    Friend WithEvents ssRodape As System.Windows.Forms.StatusStrip
    Friend WithEvents tsAberto As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsPCotacao As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsEncomendado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsInterno As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsProblema As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnRemoveRowLST As System.Windows.Forms.Button
    Friend WithEvents btnAddRowLST As System.Windows.Forms.Button
    Friend WithEvents ckbVerTodos As System.Windows.Forms.CheckBox
    Friend WithEvents dtpDataFatura As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDtFatura As System.Windows.Forms.Label
    Friend WithEvents tsVerificado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtProjecto As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtNomeProjecto As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtArtigo As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents txtFornecedor As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents gridArtigos As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigosCOT As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridDocsCOT As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigosENC As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridDocsENC As AxXtremeReportControl.AxReportControl
    Friend WithEvents AxImageManager1 As AxXtremeCommandBars.AxImageManager
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents tsConferido As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbTodosCOT As System.Windows.Forms.CheckBox
    Friend WithEvents cbTodosENC As System.Windows.Forms.CheckBox
    Friend WithEvents btnRemoveRowPCO As Button
    Friend WithEvents btnAddRowPCO As Button
    Friend WithEvents tabCtlGeral As TabControl
    Friend WithEvents TabPageGeral As TabPage
    Friend WithEvents TabPageAlt As TabPage
    Friend WithEvents gridArtigosAlt As AxXtremeReportControl.AxReportControl
    Friend WithEvents btnRemoveRowENC As Button
    Friend WithEvents btnAddRowENC As Button
    Friend WithEvents TotalDocCOT As Label
    Friend WithEvents TotalDocENC As Label
    Friend WithEvents tsNovo As ToolStripStatusLabel
    Friend WithEvents tsAtualizado As ToolStripStatusLabel
    Friend WithEvents tsMaterialCliente As ToolStripStatusLabel
    Friend WithEvents lblUtilGravacaoLST As Label
    Friend WithEvents lblUtilActualizacaoLST As Label
    Friend WithEvents lblUtilActualizacaoCOT As Label
    Friend WithEvents lblUtilGravacaoCOT As Label
    Friend WithEvents lblUtilActualizacaoENC As Label
    Friend WithEvents lblUtilGravacaoENC As Label
    Friend WithEvents lblMatEntrega As Label
    Friend WithEvents gridArtigosENT As AxXtremeReportControl.AxReportControl
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents tssubcontratado As ToolStripStatusLabel
    Friend WithEvents lblDataUltCarregamentoLista As Label
    Friend WithEvents btnPreview As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDocRecepcao As TextBox
    Friend WithEvents btnRepartirCustos As Button
    Friend WithEvents btnAplicarTodos As Button
    Friend WithEvents dtPicker As AxXtremeSuiteControls.AxDateTimePicker
    Friend WithEvents Group4 As GroupBox
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents btn4 As Button
End Class
