﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmQt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmQt))
        Me.btn0 = New System.Windows.Forms.Button
        Me.btnC = New System.Windows.Forms.Button
        Me.btn9 = New System.Windows.Forms.Button
        Me.btn8 = New System.Windows.Forms.Button
        Me.btn7 = New System.Windows.Forms.Button
        Me.btn6 = New System.Windows.Forms.Button
        Me.btn5 = New System.Windows.Forms.Button
        Me.btn4 = New System.Windows.Forms.Button
        Me.btn3 = New System.Windows.Forms.Button
        Me.btn2 = New System.Windows.Forms.Button
        Me.btn1 = New System.Windows.Forms.Button
        Me.btnP = New System.Windows.Forms.Button
        Me.txtQuant = New System.Windows.Forms.TextBox
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn0
        '
        Me.btn0.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn0.Location = New System.Drawing.Point(86, 381)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(70, 70)
        Me.btn0.TabIndex = 11
        Me.btn0.Tag = "0"
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = True
        '
        'btnC
        '
        Me.btnC.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnC.Location = New System.Drawing.Point(13, 381)
        Me.btnC.Name = "btnC"
        Me.btnC.Size = New System.Drawing.Size(70, 70)
        Me.btnC.TabIndex = 12
        Me.btnC.Tag = "-1"
        Me.btnC.Text = "C"
        Me.btnC.UseVisualStyleBackColor = True
        '
        'btn9
        '
        Me.btn9.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn9.Location = New System.Drawing.Point(162, 305)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(70, 70)
        Me.btn9.TabIndex = 10
        Me.btn9.Tag = "9"
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = True
        '
        'btn8
        '
        Me.btn8.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn8.Location = New System.Drawing.Point(86, 305)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(70, 70)
        Me.btn8.TabIndex = 9
        Me.btn8.Tag = "8"
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = True
        '
        'btn7
        '
        Me.btn7.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn7.Location = New System.Drawing.Point(13, 305)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(70, 70)
        Me.btn7.TabIndex = 8
        Me.btn7.Tag = "7"
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = True
        '
        'btn6
        '
        Me.btn6.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn6.Location = New System.Drawing.Point(162, 229)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(70, 70)
        Me.btn6.TabIndex = 7
        Me.btn6.Tag = "6"
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = True
        '
        'btn5
        '
        Me.btn5.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn5.Location = New System.Drawing.Point(86, 229)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(70, 70)
        Me.btn5.TabIndex = 6
        Me.btn5.Tag = "5"
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn4.Location = New System.Drawing.Point(13, 229)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(70, 70)
        Me.btn4.TabIndex = 5
        Me.btn4.Tag = "4"
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn3.Location = New System.Drawing.Point(162, 153)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(70, 70)
        Me.btn3.TabIndex = 4
        Me.btn3.Tag = "3"
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.Location = New System.Drawing.Point(86, 153)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(70, 70)
        Me.btn2.TabIndex = 3
        Me.btn2.Tag = "2"
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.Location = New System.Drawing.Point(13, 153)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(70, 70)
        Me.btn1.TabIndex = 2
        Me.btn1.Tag = "1"
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'btnP
        '
        Me.btnP.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnP.Location = New System.Drawing.Point(162, 381)
        Me.btnP.Name = "btnP"
        Me.btnP.Size = New System.Drawing.Size(70, 70)
        Me.btnP.TabIndex = 13
        Me.btnP.Tag = ""
        Me.btnP.Text = "."
        Me.btnP.UseVisualStyleBackColor = True
        '
        'txtQuant
        '
        Me.txtQuant.Enabled = False
        Me.txtQuant.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuant.Location = New System.Drawing.Point(13, 95)
        Me.txtQuant.Name = "txtQuant"
        Me.txtQuant.Size = New System.Drawing.Size(219, 30)
        Me.txtQuant.TabIndex = 0
        Me.txtQuant.Text = "1"
        Me.txtQuant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(1, 1)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(204, 55)
        Me.CommandBarsFrame1.TabIndex = 28
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(181, 65)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 29
        '
        'FrmQt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(247, 470)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.txtQuant)
        Me.Controls.Add(Me.btnP)
        Me.Controls.Add(Me.btn0)
        Me.Controls.Add(Me.btnC)
        Me.Controls.Add(Me.btn9)
        Me.Controls.Add(Me.btn8)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.btn6)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(263, 508)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(263, 508)
        Me.Name = "FrmQt"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestão Projectos - Quantidade"
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn0 As System.Windows.Forms.Button
    Friend WithEvents btnC As System.Windows.Forms.Button
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btnP As System.Windows.Forms.Button
    Friend WithEvents txtQuant As System.Windows.Forms.TextBox
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
End Class
