﻿Public Class Objeto
    Public Sub New()

    End Sub

    ''' <summary>
    ''' BaseDados
    ''' </summary>
    ''' <remarks></remarks>
    Dim baseDados_ As String
    Property BaseDados() As String
        Get
            If baseDados_ <> "" Then
                Return "PRI" + baseDados_
            Else
                Return baseDados_
            End If
        End Get
        Set(ByVal value As String)
            baseDados_ = value
        End Set
    End Property

    ReadOnly Property Empresa() As String
        Get
            Return baseDados_
        End Get
    End Property


    ''' <summary>
    ''' Id do Registo
    ''' </summary>
    ''' <remarks></remarks>
    Dim id_ As String
    Property Id() As String
        Get
            Id = id_
        End Get
        Set(ByVal value As String)
            id_ = value
        End Set
    End Property

    ''' <summary>
    ''' Id do Registo
    ''' </summary>
    ''' <remarks></remarks>
    Dim projeto_ As String
    Property Projeto() As String
        Get
            Projeto = projeto_
        End Get
        Set(ByVal value As String)
            projeto_ = value
        End Set
    End Property


End Class
