﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports Interop.StdBE900

Public Class FrmTerminar

    Const MSGERRO1 As String = "O tempo despendido não se encontra preenchido"
    Const MSGPERGUNTA1 As String = "Deseja realmente remover o registo?"
    Dim limpar As Boolean

    Dim registo_ As Registo
    Property Registo() As Registo
        Get
            Registo = registo_
        End Get
        Set(ByVal value As Registo)
            registo_ = value
        End Set
    End Property


    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Private Sub FrmTerminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        botaoSeleccionado_ = 3
        limpar = True
        iniciarForm()
    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Gravar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Gravar"

            Control = .Add(xtpControlButton, 2, " Remover", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Remover"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    Private Sub iniciarForm()
        iniciarToolBox()
        dtpData.Value = CDate(registo_.DataInicial)
        dtpTempoI.Value = CDate(registo_.DataInicial)
        dtpTempoF.Value = CDate(registo_.DataFinal)
        dtpTempoDescontar.Value = CDate(registo_.TempoDescontado)

        txtObservacoes.Text = registo_.Observacoes

        inicializarProblemas()
        lstProblemas.SelectedItems.Clear()

        If Not registo_.Problema Is Nothing Then
            Dim problemas As String()
            Dim problema As String
            problemas = registo_.Problema.Split(";")
            For i = 0 To problemas.Length - 1
                problema = Trim(problemas(i))
                seleccionarProblema(problema)
            Next
            'lstProblemas.Text = registo_.Problema
        End If



        'iniciarToolBox()
        'dtpData.Value = CDate(registo_.DataInicial)
        'dtpTempoI.Value = FormatDateTime(dtpData.Value, DateFormat.ShortDate)
        'dtpTempoI.Value = DateAdd(DateInterval.Minute, registo_.LinhaRegistoMaoObra.Quantidade * 60, dtpTempoI.Value)
        'inicializarProblemas()

    End Sub


    Private Sub seleccionarProblema(ByVal problema As String)
        Dim m As Motor

        m = Motor.GetInstance()

        Dim drow As DataRowView
        Dim i As Integer
        For i = 0 To lstProblemas.Items.Count - 1
            drow = lstProblemas.Items(i)
            If UCase(m.NuloToString(drow("Nome"))) = UCase(problema) Then
                lstProblemas.SelectedIndices.Add(i)
            End If
        Next

    End Sub

    'Private Sub txtTempo_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtTempo.Validating

    '    e.Cancel = IsNumeric(txtTempo.Text)
    'End Sub



    Private Sub colocarTempo(ByVal valor As String)
        If limpar Then
            'txtTempo.Text = ""
            limpar = False
        End If
        If valor = "" Then
            '  txtTempo.Text = ""
        Else
            '  txtTempo.Text = txtTempo.Text + valor
        End If
    End Sub



    'Private Sub habilitarBotoes(ByVal b As Boolean)
    '    btn1.Enabled = b
    '    btn2.Enabled = b
    '    btn3.Enabled = b
    '    btn4.Enabled = b
    '    If b = True Then btn5.Enabled = b
    '    btn6.Enabled = b
    '    btn7.Enabled = b
    '    btn8.Enabled = b
    '    btn9.Enabled = b
    '    btn0.Enabled = b
    '    btnP.Enabled = b
    'End Sub

    'Private Sub btnP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    habilitarBotoes(False)
    '    colocarTempo(",")
    'End Sub

    'Private Sub btnC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo("")
    '    habilitarBotoes(True)
    'End Sub

    'Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn1.Tag)
    'End Sub

    'Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn2.Tag)
    'End Sub

    'Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn3.Tag)
    'End Sub

    'Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn4.Tag)
    'End Sub

    'Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn5.Tag)
    '    If btn0.Enabled = False Then
    '        btn5.Enabled = False
    '    End If
    'End Sub

    'Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn6.Tag)
    'End Sub

    'Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn7.Tag)
    'End Sub

    'Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn8.Tag)
    'End Sub

    'Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn9.Tag)
    'End Sub

    'Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    colocarTempo(btn0.Tag)
    'End Sub


    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        botaoSeleccionado_ = e.control.Index

        ' terminar o registo
        If e.control.Index = 1 Then
            terminarServico()
        End If

        'remover o registo
        If e.control.Index = 2 Then
            If MsgBox(MSGPERGUNTA1, MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
            Dim m As Motor
            m = Motor.GetInstance
            m.removerRegisto(registo_)
            '  clicouConfirmar_ = True
            Me.Close()
        End If

        'fechar o serviço
        If e.control.Index = 3 Then
            Me.Close()
        End If
    End Sub

    ''' <summary>
    ''' função que permite o registo dos pontos no sistema
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub terminarServico()
        Dim m As Motor
        Dim linha As LinhaRegisto
        Dim idCabecDoc_ As String
        m = Motor.GetInstance

        registo_.DataInicial = FormatDateTime(dtpData.Value, DateFormat.ShortDate) + " " + FormatDateTime(dtpTempoI.Value, DateFormat.ShortTime)
        registo_.DataFinal = FormatDateTime(dtpData.Value, DateFormat.ShortDate) + " " + FormatDateTime(dtpTempoF.Value, DateFormat.ShortTime)
        registo_.TempoDescontado = FormatDateTime(dtpData.Value, DateFormat.ShortDate) + " " + FormatDateTime(dtpTempoDescontar.Value, DateFormat.ShortTime)
        Dim totalMinutos As Double

        totalMinutos = DateDiff(DateInterval.Minute, CDate(registo_.DataInicial), CDate(registo_.DataFinal))

        If totalMinutos <= 0 Then
            MsgBox("Tempo Inicial superior ou igual ao Tempo Final", MsgBoxStyle.Critical)
            Exit Sub
        End If

        totalMinutos = totalMinutos - ((CDate(registo_.TempoDescontado).Hour * 60) + CDate(registo_.TempoDescontado).Minute)

        If totalMinutos < 0 Then
            MsgBox("Tempo a descontar superior ao tempo de serviço", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If totalMinutos = 0 Then
            If MsgBox("Tempo Serviço igual a Zero(0)" + vbCrLf + vbCrLf + "Deseja Continuar?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        ' Dim quant As Double

        ' If totalMinutos < 15 And totalMinutos <> 0 Then totalMinutos = 15
        totalMinutos = ((CDate(registo_.TempoDescontado).Hour * 60) + CDate(registo_.TempoDescontado).Minute)

        totalMinutos = m.calculaTempoMaoObra(registo_.DataInicial, DateAdd(DateInterval.Minute, -totalMinutos, CDate(registo_.DataFinal)))

        registo_.LinhaRegistoMaoObra.Quantidade = totalMinutos '/ m.calculaTempoMaoObra(registo_.DataInicial, registo_.DataFinal) 'Math.Round(totalMinutos / 60, 2)

        registo_.Problema = ""

        Dim drow As DataRowView
        For Each drow In lstProblemas.SelectedItems
            If registo_.Problema = "" Then
                registo_.Problema = m.NuloToString(drow("Nome"))
            Else
                registo_.Problema = registo_.Problema + " ; " + drow("Nome")
            End If
        Next

        registo_.Observacoes = txtObservacoes.Text

        idCabecDoc_ = ""
        If m.getGRAVAFINALIZAR() Then
            If m.getMOLDULOP() = "I" Then
                idCabecDoc_ = m.actualizarDocumentoInterno(registo_, dtpData.Value)
            Else
                Try
                    idCabecDoc_ = m.actualizarDocumentoVenda(registo_, dtpData.Value)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            End If
            registo_.CDU_IdCabec = idCabecDoc_
        End If

        If idCabecDoc_ <> "" Or Not m.getGRAVAFINALIZAR() Then
            m.TerminarOperacaoFinal(registo_, dtpData.Value)
        End If
        Me.Hide()

    End Sub


    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        botaoSeleccionado_ = 1
        terminarServico()
    End Sub


    Private Sub inicializarProblemas()
        Dim m As Motor
        Dim lista As StdBELista
        lstProblemas.SelectedIndex = -1
        m = Motor.GetInstance()
        lista = m.daListaProblemas(m.Funcionario)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstProblemas.DataSource = dt
        lstProblemas.ValueMember = "Codigo"
        lstProblemas.DisplayMember = "Nome"
        lstProblemas.ClearSelected()
        ' lstProblemas.SelectedIndex = -1
    End Sub

    Private Sub dtpTempoDescontar_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTempoDescontar.ValueChanged
        'If it's already an increment of 15, exit sub 

        If (sender.Value.Minute = 0) Or (sender.Value.Minute = 15) Or (sender.Value.Minute = 30) Or (sender.Value.Minute = 45) Then Exit Sub

        'If it's a 1, 16, 31, or 46 (i.e. they clicked "up")... 

        If sender.Value.Minute = 1 Or sender.Value.Minute = 16 Or sender.Value.Minute = 31 Or sender.Value.Minute = 46 Then

            'Add 14 minutes to the value 

            sender.Value = DateAdd(DateInterval.Minute, 14, sender.Value)

        ElseIf sender.Value.Minute = 14 Or sender.Value.Minute = 29 Or sender.Value.Minute = 44 Or sender.Value.Minute = 59 Then

            'Otheriwse, if it's a 14, 29, 44, or 59 (i.e. they clicked "down"), subtract 14 minutes from the value

            '   You have to subtract 74 (instead of 14) if it's 59, to take it down one hour

            If sender.Value.Minute = 59 Then sender.Value = DateAdd(DateInterval.Minute, -74, sender.Value) Else 

            sender.Value = DateAdd(DateInterval.Minute, -14, sender.Value)

        Else 'Otherwise, if it's not one digit away from a 15 minute interval (i.e. they manually entered a number)

            'Round the number up to the next 15 minute increment 

            sender.Value = New DateTime(sender.Value.Year, sender.Value.Month, sender.Value.Day, sender.Value.Hour, RoundUp(sender.Value.Minute), 0)

        End If

    End Sub

    Private Function RoundUp(ByVal iMin) As Integer

        Dim x As Integer

        For i As Integer = 1 To 15

            x = iMin + i

            If x = 15 Or x = 30 Or x = 45 Or x = 0 Then Return x

        Next

    End Function


   

End Class