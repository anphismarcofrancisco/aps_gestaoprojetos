﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEncomendas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEncomendas))
        Me.lblMoldes = New System.Windows.Forms.Label()
        Me.lstMoldes = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblData = New System.Windows.Forms.Label()
        Me.dtpData = New System.Windows.Forms.DateTimePicker()
        Me.lblText = New System.Windows.Forms.Label()
        Me.txtNumDoc = New System.Windows.Forms.NumericUpDown()
        Me.lstDocumentos = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblRegisto2 = New System.Windows.Forms.Label()
        Me.lblRegisto1 = New System.Windows.Forms.Label()
        Me.txtObservacoes = New System.Windows.Forms.TextBox()
        Me.lblFornecedor = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.Geral = New System.Windows.Forms.TabPage()
        Me.txtNumContrib = New AxXtremeSuiteControls.AxFlatEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtNomeEntidade = New AxXtremeSuiteControls.AxFlatEdit()
        Me.txtEntidade = New AxXtremeSuiteControls.AxFlatEdit()
        Me.btnForn = New System.Windows.Forms.Button()
        Me.Morada = New System.Windows.Forms.TabPage()
        Me.txtLocalidadeCodPostal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCodPostal = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtMorada2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtLocalidade = New System.Windows.Forms.TextBox()
        Me.txtMorada1 = New System.Windows.Forms.TextBox()
        Me.CargaDescarga = New System.Windows.Forms.TabPage()
        Me.txtLocalidadeCodPostalCarga = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtMorada1Carga = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCodPostalCarga = New System.Windows.Forms.TextBox()
        Me.txtMorada2Carga = New System.Windows.Forms.TextBox()
        Me.txtLocalCarga = New System.Windows.Forms.TextBox()
        Me.txtLocalidadeCarga = New System.Windows.Forms.TextBox()
        Me.txtHoraCarga = New System.Windows.Forms.MaskedTextBox()
        Me.DtpDataCarga = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtMatricula = New System.Windows.Forms.MaskedTextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtLocalidadeCodPostalDescarga = New System.Windows.Forms.TextBox()
        Me.lstMoradas = New System.Windows.Forms.ComboBox()
        Me.txtMorada1Descarga = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCodPostalDescarga = New System.Windows.Forms.TextBox()
        Me.txtMorada2Descarga = New System.Windows.Forms.TextBox()
        Me.txtLocalidadeDescarga = New System.Windows.Forms.TextBox()
        Me.txtHoraDescarga = New System.Windows.Forms.MaskedTextBox()
        Me.DtpDataDescarga = New System.Windows.Forms.DateTimePicker()
        Me.txtLocalDescarga = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblInsp1 = New System.Windows.Forms.Label()
        Me.lblInsp2 = New System.Windows.Forms.Label()
        Me.txtArtigo = New System.Windows.Forms.TextBox()
        Me.btnRepartirCustos = New System.Windows.Forms.Button()
        Me.btnAplicarTodos = New System.Windows.Forms.Button()
        Me.btnImportarLista = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnEnviar = New System.Windows.Forms.Button()
        Me.ckbInsp2 = New System.Windows.Forms.CheckBox()
        Me.ckbInsp1 = New System.Windows.Forms.CheckBox()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.btnEAT = New System.Windows.Forms.Button()
        Me.btnDAT = New System.Windows.Forms.Button()
        Me.btnEPA = New System.Windows.Forms.Button()
        Me.btnDPA = New System.Windows.Forms.Button()
        Me.lblTotalDoc = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lblTotalDesc = New System.Windows.Forms.Label()
        Me.lblTotalMerc = New System.Windows.Forms.Label()
        Me.dtPicker = New AxXtremeSuiteControls.AxDateTimePicker()
        Me.txtPesqMolde = New AxXtremeSuiteControls.AxFlatEdit()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        Me.gridArtigosStock = New AxXtremeReportControl.AxReportControl()
        Me.gridArtigos = New AxXtremeReportControl.AxReportControl()
        CType(Me.txtNumDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.Geral.SuspendLayout()
        CType(Me.txtNumContrib, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNomeEntidade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEntidade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Morada.SuspendLayout()
        Me.CargaDescarga.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dtPicker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqMolde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigosStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMoldes
        '
        Me.lblMoldes.AutoSize = True
        Me.lblMoldes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoldes.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblMoldes.Location = New System.Drawing.Point(1259, 213)
        Me.lblMoldes.Name = "lblMoldes"
        Me.lblMoldes.Size = New System.Drawing.Size(66, 25)
        Me.lblMoldes.TabIndex = 44
        Me.lblMoldes.Text = "Molde"
        '
        'lstMoldes
        '
        Me.lstMoldes.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMoldes.FormattingEnabled = True
        Me.lstMoldes.ItemHeight = 25
        Me.lstMoldes.Location = New System.Drawing.Point(1264, 283)
        Me.lstMoldes.Name = "lstMoldes"
        Me.lstMoldes.Size = New System.Drawing.Size(180, 379)
        Me.lstMoldes.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(10, 174)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 25)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Artigos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(547, 275)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(178, 25)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Artigos Documento"
        '
        'lblData
        '
        Me.lblData.AutoSize = True
        Me.lblData.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblData.Location = New System.Drawing.Point(832, 3)
        Me.lblData.Name = "lblData"
        Me.lblData.Size = New System.Drawing.Size(44, 20)
        Me.lblData.TabIndex = 48
        Me.lblData.Text = "Data"
        '
        'dtpData
        '
        Me.dtpData.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpData.Location = New System.Drawing.Point(832, 27)
        Me.dtpData.Name = "dtpData"
        Me.dtpData.Size = New System.Drawing.Size(153, 35)
        Me.dtpData.TabIndex = 4
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.Location = New System.Drawing.Point(340, 57)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(152, 29)
        Me.lblText.TabIndex = 54
        Me.lblText.Text = "TJ Aços, Lda"
        '
        'txtNumDoc
        '
        Me.txtNumDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDoc.Location = New System.Drawing.Point(686, 27)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(140, 35)
        Me.txtNumDoc.TabIndex = 3
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lstDocumentos
        '
        Me.lstDocumentos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.lstDocumentos.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDocumentos.FormattingEnabled = True
        Me.lstDocumentos.Location = New System.Drawing.Point(673, 9)
        Me.lstDocumentos.Name = "lstDocumentos"
        Me.lstDocumentos.Size = New System.Drawing.Size(375, 37)
        Me.lstDocumentos.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label3.Location = New System.Drawing.Point(547, 176)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 25)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "Observações"
        '
        'lblRegisto2
        '
        Me.lblRegisto2.AutoSize = True
        Me.lblRegisto2.Location = New System.Drawing.Point(537, 669)
        Me.lblRegisto2.Name = "lblRegisto2"
        Me.lblRegisto2.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto2.TabIndex = 60
        Me.lblRegisto2.Text = "0 Registos"
        '
        'lblRegisto1
        '
        Me.lblRegisto1.AutoSize = True
        Me.lblRegisto1.Location = New System.Drawing.Point(12, 669)
        Me.lblRegisto1.Name = "lblRegisto1"
        Me.lblRegisto1.Size = New System.Drawing.Size(57, 13)
        Me.lblRegisto1.TabIndex = 59
        Me.lblRegisto1.Text = "0 Registos"
        '
        'txtObservacoes
        '
        Me.txtObservacoes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacoes.Location = New System.Drawing.Point(552, 204)
        Me.txtObservacoes.Multiline = True
        Me.txtObservacoes.Name = "txtObservacoes"
        Me.txtObservacoes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacoes.Size = New System.Drawing.Size(654, 71)
        Me.txtObservacoes.TabIndex = 6
        Me.txtObservacoes.WordWrap = False
        '
        'lblFornecedor
        '
        Me.lblFornecedor.AutoSize = True
        Me.lblFornecedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFornecedor.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblFornecedor.Location = New System.Drawing.Point(6, 0)
        Me.lblFornecedor.Name = "lblFornecedor"
        Me.lblFornecedor.Size = New System.Drawing.Size(112, 25)
        Me.lblFornecedor.TabIndex = 64
        Me.lblFornecedor.Text = "Fornecedor"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.Geral)
        Me.TabControl1.Controls.Add(Me.Morada)
        Me.TabControl1.Controls.Add(Me.CargaDescarga)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(13, 72)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1044, 92)
        Me.TabControl1.TabIndex = 65
        '
        'Geral
        '
        Me.Geral.Controls.Add(Me.txtNumContrib)
        Me.Geral.Controls.Add(Me.Label15)
        Me.Geral.Controls.Add(Me.txtNomeEntidade)
        Me.Geral.Controls.Add(Me.txtEntidade)
        Me.Geral.Controls.Add(Me.lblFornecedor)
        Me.Geral.Controls.Add(Me.btnForn)
        Me.Geral.Controls.Add(Me.txtNumDoc)
        Me.Geral.Controls.Add(Me.dtpData)
        Me.Geral.Controls.Add(Me.lblData)
        Me.Geral.Location = New System.Drawing.Point(4, 22)
        Me.Geral.Name = "Geral"
        Me.Geral.Padding = New System.Windows.Forms.Padding(3)
        Me.Geral.Size = New System.Drawing.Size(1036, 66)
        Me.Geral.TabIndex = 0
        Me.Geral.Text = "    Geral    "
        Me.Geral.UseVisualStyleBackColor = True
        '
        'txtNumContrib
        '
        Me.txtNumContrib.Location = New System.Drawing.Point(525, 27)
        Me.txtNumContrib.Name = "txtNumContrib"
        Me.txtNumContrib.OcxState = CType(resources.GetObject("txtNumContrib.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtNumContrib.Size = New System.Drawing.Size(155, 35)
        Me.txtNumContrib.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label15.Location = New System.Drawing.Point(520, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(117, 25)
        Me.Label15.TabIndex = 66
        Me.Label15.Text = "Contribuinte"
        '
        'txtNomeEntidade
        '
        Me.txtNomeEntidade.Location = New System.Drawing.Point(209, 27)
        Me.txtNomeEntidade.Name = "txtNomeEntidade"
        Me.txtNomeEntidade.OcxState = CType(resources.GetObject("txtNomeEntidade.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtNomeEntidade.Size = New System.Drawing.Size(310, 35)
        Me.txtNomeEntidade.TabIndex = 1
        '
        'txtEntidade
        '
        Me.txtEntidade.Location = New System.Drawing.Point(6, 27)
        Me.txtEntidade.Name = "txtEntidade"
        Me.txtEntidade.OcxState = CType(resources.GetObject("txtEntidade.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtEntidade.Size = New System.Drawing.Size(151, 35)
        Me.txtEntidade.TabIndex = 5
        '
        'btnForn
        '
        Me.btnForn.Image = Global.APS_GestaoProjectos.My.Resources.Resources.ZoomHS
        Me.btnForn.Location = New System.Drawing.Point(163, 27)
        Me.btnForn.Name = "btnForn"
        Me.btnForn.Size = New System.Drawing.Size(40, 35)
        Me.btnForn.TabIndex = 51
        Me.btnForn.UseVisualStyleBackColor = True
        '
        'Morada
        '
        Me.Morada.Controls.Add(Me.txtLocalidadeCodPostal)
        Me.Morada.Controls.Add(Me.Label5)
        Me.Morada.Controls.Add(Me.txtCodPostal)
        Me.Morada.Controls.Add(Me.Label14)
        Me.Morada.Controls.Add(Me.txtMorada2)
        Me.Morada.Controls.Add(Me.Label6)
        Me.Morada.Controls.Add(Me.txtLocalidade)
        Me.Morada.Controls.Add(Me.txtMorada1)
        Me.Morada.Location = New System.Drawing.Point(4, 22)
        Me.Morada.Name = "Morada"
        Me.Morada.Size = New System.Drawing.Size(1036, 66)
        Me.Morada.TabIndex = 2
        Me.Morada.Text = "    Morada    "
        Me.Morada.UseVisualStyleBackColor = True
        '
        'txtLocalidadeCodPostal
        '
        Me.txtLocalidadeCodPostal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalidadeCodPostal.Location = New System.Drawing.Point(772, 35)
        Me.txtLocalidadeCodPostal.Name = "txtLocalidadeCodPostal"
        Me.txtLocalidadeCodPostal.Size = New System.Drawing.Size(238, 26)
        Me.txtLocalidadeCodPostal.TabIndex = 82
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label5.Location = New System.Drawing.Point(518, 36)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 20)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Código Postal:"
        '
        'txtCodPostal
        '
        Me.txtCodPostal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodPostal.Location = New System.Drawing.Point(635, 35)
        Me.txtCodPostal.Name = "txtCodPostal"
        Me.txtCodPostal.Size = New System.Drawing.Size(128, 26)
        Me.txtCodPostal.TabIndex = 80
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label14.Location = New System.Drawing.Point(538, 7)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(90, 20)
        Me.Label14.TabIndex = 79
        Me.Label14.Text = "Localidade:"
        '
        'txtMorada2
        '
        Me.txtMorada2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMorada2.Location = New System.Drawing.Point(81, 35)
        Me.txtMorada2.Name = "txtMorada2"
        Me.txtMorada2.Size = New System.Drawing.Size(387, 26)
        Me.txtMorada2.TabIndex = 77
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(7, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 20)
        Me.Label6.TabIndex = 76
        Me.Label6.Text = "Morada:"
        '
        'txtLocalidade
        '
        Me.txtLocalidade.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalidade.Location = New System.Drawing.Point(635, 5)
        Me.txtLocalidade.Name = "txtLocalidade"
        Me.txtLocalidade.Size = New System.Drawing.Size(375, 26)
        Me.txtLocalidade.TabIndex = 78
        '
        'txtMorada1
        '
        Me.txtMorada1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMorada1.Location = New System.Drawing.Point(80, 4)
        Me.txtMorada1.Name = "txtMorada1"
        Me.txtMorada1.Size = New System.Drawing.Size(387, 26)
        Me.txtMorada1.TabIndex = 75
        '
        'CargaDescarga
        '
        Me.CargaDescarga.Controls.Add(Me.txtLocalidadeCodPostalCarga)
        Me.CargaDescarga.Controls.Add(Me.Label18)
        Me.CargaDescarga.Controls.Add(Me.txtMorada1Carga)
        Me.CargaDescarga.Controls.Add(Me.Label19)
        Me.CargaDescarga.Controls.Add(Me.Label17)
        Me.CargaDescarga.Controls.Add(Me.Label8)
        Me.CargaDescarga.Controls.Add(Me.txtCodPostalCarga)
        Me.CargaDescarga.Controls.Add(Me.txtMorada2Carga)
        Me.CargaDescarga.Controls.Add(Me.txtLocalCarga)
        Me.CargaDescarga.Controls.Add(Me.txtLocalidadeCarga)
        Me.CargaDescarga.Controls.Add(Me.txtHoraCarga)
        Me.CargaDescarga.Controls.Add(Me.DtpDataCarga)
        Me.CargaDescarga.Controls.Add(Me.Label13)
        Me.CargaDescarga.Controls.Add(Me.Label11)
        Me.CargaDescarga.Location = New System.Drawing.Point(4, 22)
        Me.CargaDescarga.Name = "CargaDescarga"
        Me.CargaDescarga.Padding = New System.Windows.Forms.Padding(3)
        Me.CargaDescarga.Size = New System.Drawing.Size(1036, 66)
        Me.CargaDescarga.TabIndex = 1
        Me.CargaDescarga.Text = "    Carga    "
        Me.CargaDescarga.UseVisualStyleBackColor = True
        '
        'txtLocalidadeCodPostalCarga
        '
        Me.txtLocalidadeCodPostalCarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalidadeCodPostalCarga.Location = New System.Drawing.Point(600, 35)
        Me.txtLocalidadeCodPostalCarga.Name = "txtLocalidadeCodPostalCarga"
        Me.txtLocalidadeCodPostalCarga.Size = New System.Drawing.Size(260, 26)
        Me.txtLocalidadeCodPostalCarga.TabIndex = 85
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label18.Location = New System.Drawing.Point(370, 7)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(90, 20)
        Me.Label18.TabIndex = 84
        Me.Label18.Text = "Localidade:"
        '
        'txtMorada1Carga
        '
        Me.txtMorada1Carga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMorada1Carga.Location = New System.Drawing.Point(80, 4)
        Me.txtMorada1Carga.Name = "txtMorada1Carga"
        Me.txtMorada1Carga.Size = New System.Drawing.Size(288, 26)
        Me.txtMorada1Carga.TabIndex = 86
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label19.Location = New System.Drawing.Point(374, 37)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(90, 20)
        Me.Label19.TabIndex = 84
        Me.Label19.Text = "Cód. Postal"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label17.Location = New System.Drawing.Point(7, 7)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 20)
        Me.Label17.TabIndex = 87
        Me.Label17.Text = "Morada:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(668, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 20)
        Me.Label8.TabIndex = 72
        Me.Label8.Text = "Local :"
        '
        'txtCodPostalCarga
        '
        Me.txtCodPostalCarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodPostalCarga.Location = New System.Drawing.Point(466, 35)
        Me.txtCodPostalCarga.Name = "txtCodPostalCarga"
        Me.txtCodPostalCarga.Size = New System.Drawing.Size(128, 26)
        Me.txtCodPostalCarga.TabIndex = 83
        '
        'txtMorada2Carga
        '
        Me.txtMorada2Carga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMorada2Carga.Location = New System.Drawing.Point(80, 35)
        Me.txtMorada2Carga.Name = "txtMorada2Carga"
        Me.txtMorada2Carga.Size = New System.Drawing.Size(288, 26)
        Me.txtMorada2Carga.TabIndex = 88
        '
        'txtLocalCarga
        '
        Me.txtLocalCarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalCarga.Location = New System.Drawing.Point(729, 4)
        Me.txtLocalCarga.Name = "txtLocalCarga"
        Me.txtLocalCarga.Size = New System.Drawing.Size(131, 26)
        Me.txtLocalCarga.TabIndex = 71
        '
        'txtLocalidadeCarga
        '
        Me.txtLocalidadeCarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalidadeCarga.Location = New System.Drawing.Point(466, 4)
        Me.txtLocalidadeCarga.Name = "txtLocalidadeCarga"
        Me.txtLocalidadeCarga.Size = New System.Drawing.Size(196, 26)
        Me.txtLocalidadeCarga.TabIndex = 83
        '
        'txtHoraCarga
        '
        Me.txtHoraCarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtHoraCarga.Location = New System.Drawing.Point(972, 35)
        Me.txtHoraCarga.Mask = "00:00"
        Me.txtHoraCarga.Name = "txtHoraCarga"
        Me.txtHoraCarga.Size = New System.Drawing.Size(52, 26)
        Me.txtHoraCarga.TabIndex = 84
        Me.txtHoraCarga.ValidatingType = GetType(Date)
        '
        'DtpDataCarga
        '
        Me.DtpDataCarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DtpDataCarga.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtpDataCarga.Location = New System.Drawing.Point(921, 6)
        Me.DtpDataCarga.Name = "DtpDataCarga"
        Me.DtpDataCarga.Size = New System.Drawing.Size(103, 26)
        Me.DtpDataCarga.TabIndex = 81
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label13.Location = New System.Drawing.Point(917, 37)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(48, 20)
        Me.Label13.TabIndex = 79
        Me.Label13.Text = "Hora:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label11.Location = New System.Drawing.Point(867, 7)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 20)
        Me.Label11.TabIndex = 76
        Me.Label11.Text = "Data:"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtMatricula)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.txtLocalidadeCodPostalDescarga)
        Me.TabPage1.Controls.Add(Me.lstMoradas)
        Me.TabPage1.Controls.Add(Me.txtMorada1Descarga)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.txtCodPostalDescarga)
        Me.TabPage1.Controls.Add(Me.txtMorada2Descarga)
        Me.TabPage1.Controls.Add(Me.txtLocalidadeDescarga)
        Me.TabPage1.Controls.Add(Me.txtHoraDescarga)
        Me.TabPage1.Controls.Add(Me.DtpDataDescarga)
        Me.TabPage1.Controls.Add(Me.txtLocalDescarga)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1036, 66)
        Me.TabPage1.TabIndex = 3
        Me.TabPage1.Text = "    Descarga    "
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtMatricula
        '
        Me.txtMatricula.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtMatricula.Location = New System.Drawing.Point(775, 7)
        Me.txtMatricula.Mask = "&&-&&-&&"
        Me.txtMatricula.Name = "txtMatricula"
        Me.txtMatricula.Size = New System.Drawing.Size(82, 26)
        Me.txtMatricula.TabIndex = 98
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label16.Location = New System.Drawing.Point(370, 7)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(90, 20)
        Me.Label16.TabIndex = 95
        Me.Label16.Text = "Localidade:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label20.Location = New System.Drawing.Point(374, 37)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(90, 20)
        Me.Label20.TabIndex = 96
        Me.Label20.Text = "Cód. Postal"
        '
        'txtLocalidadeCodPostalDescarga
        '
        Me.txtLocalidadeCodPostalDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalidadeCodPostalDescarga.Location = New System.Drawing.Point(548, 35)
        Me.txtLocalidadeCodPostalDescarga.Name = "txtLocalidadeCodPostalDescarga"
        Me.txtLocalidadeCodPostalDescarga.Size = New System.Drawing.Size(149, 26)
        Me.txtLocalidadeCodPostalDescarga.TabIndex = 91
        Me.txtLocalidadeCodPostalDescarga.Text = "Marinha Grande"
        '
        'lstMoradas
        '
        Me.lstMoradas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.lstMoradas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMoradas.FormattingEnabled = True
        Me.lstMoradas.Location = New System.Drawing.Point(3, 37)
        Me.lstMoradas.Name = "lstMoradas"
        Me.lstMoradas.Size = New System.Drawing.Size(80, 23)
        Me.lstMoradas.TabIndex = 70
        '
        'txtMorada1Descarga
        '
        Me.txtMorada1Descarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMorada1Descarga.Location = New System.Drawing.Point(86, 4)
        Me.txtMorada1Descarga.Name = "txtMorada1Descarga"
        Me.txtMorada1Descarga.Size = New System.Drawing.Size(282, 26)
        Me.txtMorada1Descarga.TabIndex = 92
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(698, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 20)
        Me.Label7.TabIndex = 70
        Me.Label7.Text = "Matricula:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(7, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 20)
        Me.Label4.TabIndex = 93
        Me.Label4.Text = "Morada:"
        '
        'txtCodPostalDescarga
        '
        Me.txtCodPostalDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodPostalDescarga.Location = New System.Drawing.Point(466, 35)
        Me.txtCodPostalDescarga.Name = "txtCodPostalDescarga"
        Me.txtCodPostalDescarga.Size = New System.Drawing.Size(76, 26)
        Me.txtCodPostalDescarga.TabIndex = 89
        Me.txtCodPostalDescarga.Text = "2430-520"
        '
        'txtMorada2Descarga
        '
        Me.txtMorada2Descarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMorada2Descarga.Location = New System.Drawing.Point(86, 35)
        Me.txtMorada2Descarga.Name = "txtMorada2Descarga"
        Me.txtMorada2Descarga.Size = New System.Drawing.Size(282, 26)
        Me.txtMorada2Descarga.TabIndex = 94
        '
        'txtLocalidadeDescarga
        '
        Me.txtLocalidadeDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalidadeDescarga.Location = New System.Drawing.Point(466, 4)
        Me.txtLocalidadeDescarga.Name = "txtLocalidadeDescarga"
        Me.txtLocalidadeDescarga.Size = New System.Drawing.Size(232, 26)
        Me.txtLocalidadeDescarga.TabIndex = 90
        '
        'txtHoraDescarga
        '
        Me.txtHoraDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtHoraDescarga.Location = New System.Drawing.Point(983, 35)
        Me.txtHoraDescarga.Mask = "00:00"
        Me.txtHoraDescarga.Name = "txtHoraDescarga"
        Me.txtHoraDescarga.Size = New System.Drawing.Size(49, 26)
        Me.txtHoraDescarga.TabIndex = 85
        Me.txtHoraDescarga.ValidatingType = GetType(Date)
        '
        'DtpDataDescarga
        '
        Me.DtpDataDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DtpDataDescarga.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtpDataDescarga.Location = New System.Drawing.Point(928, 4)
        Me.DtpDataDescarga.Name = "DtpDataDescarga"
        Me.DtpDataDescarga.Size = New System.Drawing.Size(104, 26)
        Me.DtpDataDescarga.TabIndex = 82
        '
        'txtLocalDescarga
        '
        Me.txtLocalDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocalDescarga.Location = New System.Drawing.Point(775, 35)
        Me.txtLocalDescarga.Name = "txtLocalDescarga"
        Me.txtLocalDescarga.Size = New System.Drawing.Size(147, 26)
        Me.txtLocalDescarga.TabIndex = 73
        Me.txtLocalDescarga.Text = "V\Morada"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label9.Location = New System.Drawing.Point(704, 37)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 20)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "Local:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label10.Location = New System.Drawing.Point(871, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 20)
        Me.Label10.TabIndex = 78
        Me.Label10.Text = "Data:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label12.Location = New System.Drawing.Point(924, 37)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(48, 20)
        Me.Label12.TabIndex = 80
        Me.Label12.Text = "Hora:"
        '
        'lblInsp1
        '
        Me.lblInsp1.AutoSize = True
        Me.lblInsp1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsp1.Location = New System.Drawing.Point(849, 176)
        Me.lblInsp1.Name = "lblInsp1"
        Me.lblInsp1.Size = New System.Drawing.Size(53, 13)
        Me.lblInsp1.TabIndex = 67
        Me.lblInsp1.Text = "Insp 2.2"
        '
        'lblInsp2
        '
        Me.lblInsp2.AutoSize = True
        Me.lblInsp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsp2.Location = New System.Drawing.Point(1004, 176)
        Me.lblInsp2.Name = "lblInsp2"
        Me.lblInsp2.Size = New System.Drawing.Size(53, 13)
        Me.lblInsp2.TabIndex = 69
        Me.lblInsp2.Text = "Insp 3.1"
        '
        'txtArtigo
        '
        Me.txtArtigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.txtArtigo.Location = New System.Drawing.Point(12, 206)
        Me.txtArtigo.Name = "txtArtigo"
        Me.txtArtigo.Size = New System.Drawing.Size(404, 30)
        Me.txtArtigo.TabIndex = 0
        '
        'btnRepartirCustos
        '
        Me.btnRepartirCustos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.checklist64
        Me.btnRepartirCustos.Location = New System.Drawing.Point(1115, 55)
        Me.btnRepartirCustos.Name = "btnRepartirCustos"
        Me.btnRepartirCustos.Size = New System.Drawing.Size(37, 35)
        Me.btnRepartirCustos.TabIndex = 100
        Me.btnRepartirCustos.UseVisualStyleBackColor = True
        '
        'btnAplicarTodos
        '
        Me.btnAplicarTodos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.edit16
        Me.btnAplicarTodos.Location = New System.Drawing.Point(1169, 279)
        Me.btnAplicarTodos.Name = "btnAplicarTodos"
        Me.btnAplicarTodos.Size = New System.Drawing.Size(37, 23)
        Me.btnAplicarTodos.TabIndex = 99
        Me.btnAplicarTodos.UseVisualStyleBackColor = True
        '
        'btnImportarLista
        '
        Me.btnImportarLista.Image = CType(resources.GetObject("btnImportarLista.Image"), System.Drawing.Image)
        Me.btnImportarLista.Location = New System.Drawing.Point(1169, 55)
        Me.btnImportarLista.Name = "btnImportarLista"
        Me.btnImportarLista.Size = New System.Drawing.Size(37, 35)
        Me.btnImportarLista.TabIndex = 98
        Me.btnImportarLista.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Image = Global.APS_GestaoProjectos.My.Resources.Resources.print
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnImprimir.Location = New System.Drawing.Point(1244, 6)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(92, 80)
        Me.btnImprimir.TabIndex = 12
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnEnviar
        '
        Me.btnEnviar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Send_email
        Me.btnEnviar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEnviar.Location = New System.Drawing.Point(1352, 6)
        Me.btnEnviar.Name = "btnEnviar"
        Me.btnEnviar.Size = New System.Drawing.Size(92, 80)
        Me.btnEnviar.TabIndex = 13
        Me.btnEnviar.Text = "Enviar"
        Me.btnEnviar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEnviar.UseVisualStyleBackColor = True
        '
        'ckbInsp2
        '
        Me.ckbInsp2.Appearance = System.Windows.Forms.Appearance.Button
        Me.ckbInsp2.AutoSize = True
        Me.ckbInsp2.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        Me.ckbInsp2.Location = New System.Drawing.Point(960, 163)
        Me.ckbInsp2.Name = "ckbInsp2"
        Me.ckbInsp2.Size = New System.Drawing.Size(38, 38)
        Me.ckbInsp2.TabIndex = 68
        Me.ckbInsp2.UseVisualStyleBackColor = True
        '
        'ckbInsp1
        '
        Me.ckbInsp1.Appearance = System.Windows.Forms.Appearance.Button
        Me.ckbInsp1.AutoSize = True
        Me.ckbInsp1.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        Me.ckbInsp1.Location = New System.Drawing.Point(805, 163)
        Me.ckbInsp1.Name = "ckbInsp1"
        Me.ckbInsp1.Size = New System.Drawing.Size(38, 38)
        Me.ckbInsp1.TabIndex = 66
        Me.ckbInsp1.UseVisualStyleBackColor = True
        '
        'btnAplicar
        '
        Me.btnAplicar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.search
        Me.btnAplicar.Location = New System.Drawing.Point(422, 206)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(40, 32)
        Me.btnAplicar.TabIndex = 61
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'btnEAT
        '
        Me.btnEAT.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PDirT
        Me.btnEAT.Location = New System.Drawing.Point(474, 599)
        Me.btnEAT.Name = "btnEAT"
        Me.btnEAT.Size = New System.Drawing.Size(62, 62)
        Me.btnEAT.TabIndex = 5
        Me.btnEAT.UseVisualStyleBackColor = True
        Me.btnEAT.Visible = False
        '
        'btnDAT
        '
        Me.btnDAT.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PEsqT
        Me.btnDAT.Location = New System.Drawing.Point(474, 238)
        Me.btnDAT.Name = "btnDAT"
        Me.btnDAT.Size = New System.Drawing.Size(62, 62)
        Me.btnDAT.TabIndex = 2
        Me.btnDAT.UseVisualStyleBackColor = True
        Me.btnDAT.Visible = False
        '
        'btnEPA
        '
        Me.btnEPA.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PEsq
        Me.btnEPA.Location = New System.Drawing.Point(474, 465)
        Me.btnEPA.Name = "btnEPA"
        Me.btnEPA.Size = New System.Drawing.Size(62, 62)
        Me.btnEPA.TabIndex = 4
        Me.btnEPA.UseVisualStyleBackColor = True
        '
        'btnDPA
        '
        Me.btnDPA.Image = CType(resources.GetObject("btnDPA.Image"), System.Drawing.Image)
        Me.btnDPA.Location = New System.Drawing.Point(474, 377)
        Me.btnDPA.Name = "btnDPA"
        Me.btnDPA.Size = New System.Drawing.Size(62, 62)
        Me.btnDPA.TabIndex = 3
        Me.btnDPA.UseVisualStyleBackColor = True
        '
        'lblTotalDoc
        '
        Me.lblTotalDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalDoc.Location = New System.Drawing.Point(64, 81)
        Me.lblTotalDoc.Name = "lblTotalDoc"
        Me.lblTotalDoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblTotalDoc.Size = New System.Drawing.Size(114, 16)
        Me.lblTotalDoc.TabIndex = 101
        Me.lblTotalDoc.Text = "0,00"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.lblTotalDesc)
        Me.GroupBox1.Controls.Add(Me.lblTotalMerc)
        Me.GroupBox1.Controls.Add(Me.lblTotalDoc)
        Me.GroupBox1.Location = New System.Drawing.Point(1259, 89)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 110)
        Me.GroupBox1.TabIndex = 102
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Totais"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(6, 81)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(80, 16)
        Me.Label23.TabIndex = 106
        Me.Label23.Text = "Total Doc:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(6, 48)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(86, 16)
        Me.Label22.TabIndex = 105
        Me.Label22.Text = "Descontos:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(6, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(99, 16)
        Me.Label21.TabIndex = 104
        Me.Label21.Text = "Merc. / Serv.:"
        '
        'lblTotalDesc
        '
        Me.lblTotalDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalDesc.Location = New System.Drawing.Point(93, 48)
        Me.lblTotalDesc.Name = "lblTotalDesc"
        Me.lblTotalDesc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblTotalDesc.Size = New System.Drawing.Size(85, 16)
        Me.lblTotalDesc.TabIndex = 103
        Me.lblTotalDesc.Text = "0,00"
        '
        'lblTotalMerc
        '
        Me.lblTotalMerc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalMerc.Location = New System.Drawing.Point(64, 16)
        Me.lblTotalMerc.Name = "lblTotalMerc"
        Me.lblTotalMerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblTotalMerc.Size = New System.Drawing.Size(114, 16)
        Me.lblTotalMerc.TabIndex = 102
        Me.lblTotalMerc.Text = "200,00"
        '
        'dtPicker
        '
        Me.dtPicker.Location = New System.Drawing.Point(1063, 278)
        Me.dtPicker.Name = "dtPicker"
        Me.dtPicker.OcxState = CType(resources.GetObject("dtPicker.OcxState"), System.Windows.Forms.AxHost.State)
        Me.dtPicker.Size = New System.Drawing.Size(100, 22)
        Me.dtPicker.TabIndex = 97
        Me.dtPicker.Visible = False
        '
        'txtPesqMolde
        '
        Me.txtPesqMolde.Location = New System.Drawing.Point(1264, 245)
        Me.txtPesqMolde.Name = "txtPesqMolde"
        Me.txtPesqMolde.OcxState = CType(resources.GetObject("txtPesqMolde.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqMolde.Size = New System.Drawing.Size(180, 32)
        Me.txtPesqMolde.TabIndex = 9
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(1214, 9)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 36
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(12, 2)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(450, 55)
        Me.CommandBarsFrame1.TabIndex = 35
        '
        'gridArtigosStock
        '
        Me.gridArtigosStock.Location = New System.Drawing.Point(552, 303)
        Me.gridArtigosStock.Name = "gridArtigosStock"
        Me.gridArtigosStock.OcxState = CType(resources.GetObject("gridArtigosStock.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigosStock.Size = New System.Drawing.Size(654, 362)
        Me.gridArtigosStock.TabIndex = 7
        Me.gridArtigosStock.Tag = "1"
        '
        'gridArtigos
        '
        Me.gridArtigos.Location = New System.Drawing.Point(12, 242)
        Me.gridArtigos.Name = "gridArtigos"
        Me.gridArtigos.OcxState = CType(resources.GetObject("gridArtigos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridArtigos.Size = New System.Drawing.Size(450, 423)
        Me.gridArtigos.TabIndex = 1
        Me.gridArtigos.Tag = "0"
        '
        'FrmEncomendas
        '
        Me.ClientSize = New System.Drawing.Size(1468, 700)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnRepartirCustos)
        Me.Controls.Add(Me.btnAplicarTodos)
        Me.Controls.Add(Me.btnImportarLista)
        Me.Controls.Add(Me.dtPicker)
        Me.Controls.Add(Me.txtArtigo)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnEnviar)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.lblInsp2)
        Me.Controls.Add(Me.ckbInsp2)
        Me.Controls.Add(Me.lblInsp1)
        Me.Controls.Add(Me.ckbInsp1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtObservacoes)
        Me.Controls.Add(Me.btnAplicar)
        Me.Controls.Add(Me.lblRegisto2)
        Me.Controls.Add(Me.lblRegisto1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lstDocumentos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblMoldes)
        Me.Controls.Add(Me.txtPesqMolde)
        Me.Controls.Add(Me.lstMoldes)
        Me.Controls.Add(Me.btnEAT)
        Me.Controls.Add(Me.btnDAT)
        Me.Controls.Add(Me.btnEPA)
        Me.Controls.Add(Me.btnDPA)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.gridArtigosStock)
        Me.Controls.Add(Me.gridArtigos)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(1298, 739)
        Me.Name = "FrmEncomendas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anphis - Stocks - "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.txtNumDoc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.Geral.ResumeLayout(False)
        Me.Geral.PerformLayout()
        CType(Me.txtNumContrib, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNomeEntidade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEntidade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Morada.ResumeLayout(False)
        Me.Morada.PerformLayout()
        Me.CargaDescarga.ResumeLayout(False)
        Me.CargaDescarga.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dtPicker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqMolde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigosStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridArtigos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridArtigos As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridArtigosStock As AxXtremeReportControl.AxReportControl
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents btnDPA As System.Windows.Forms.Button
    Friend WithEvents btnEPA As System.Windows.Forms.Button
    Friend WithEvents btnEAT As System.Windows.Forms.Button
    Friend WithEvents btnDAT As System.Windows.Forms.Button
    Friend WithEvents lblMoldes As System.Windows.Forms.Label
    Friend WithEvents txtPesqMolde As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lstMoldes As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblData As System.Windows.Forms.Label
    Friend WithEvents dtpData As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnForn As System.Windows.Forms.Button
    Friend WithEvents txtEntidade As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents txtNumDoc As System.Windows.Forms.NumericUpDown
    Friend WithEvents lstDocumentos As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblRegisto2 As System.Windows.Forms.Label
    Friend WithEvents lblRegisto1 As System.Windows.Forms.Label
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents btnEnviar As System.Windows.Forms.Button
    Friend WithEvents txtObservacoes As System.Windows.Forms.TextBox
    Friend WithEvents lblFornecedor As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Geral As System.Windows.Forms.TabPage
    Friend WithEvents CargaDescarga As System.Windows.Forms.TabPage
    Friend WithEvents txtNomeEntidade As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lblInsp1 As System.Windows.Forms.Label
    Friend WithEvents ckbInsp1 As System.Windows.Forms.CheckBox
    Friend WithEvents lblInsp2 As System.Windows.Forms.Label
    Friend WithEvents ckbInsp2 As System.Windows.Forms.CheckBox
    Friend WithEvents DtpDataDescarga As System.Windows.Forms.DateTimePicker
    Friend WithEvents DtpDataCarga As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtLocalDescarga As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtLocalCarga As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Morada As System.Windows.Forms.TabPage
    Friend WithEvents txtMorada2 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMorada1 As System.Windows.Forms.TextBox
    Friend WithEvents txtNumContrib As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtLocalidadeCodPostal As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCodPostal As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtLocalidade As System.Windows.Forms.TextBox
    Friend WithEvents lstMoradas As System.Windows.Forms.ComboBox
    Friend WithEvents txtHoraDescarga As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtHoraCarga As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents txtLocalidadeCodPostalCarga As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtMorada1Carga As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtLocalidadeCarga As System.Windows.Forms.TextBox
    Friend WithEvents txtMorada2Carga As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtLocalidadeCodPostalDescarga As System.Windows.Forms.TextBox
    Friend WithEvents txtMorada1Descarga As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCodPostalDescarga As System.Windows.Forms.TextBox
    Friend WithEvents txtMorada2Descarga As System.Windows.Forms.TextBox
    Friend WithEvents txtLocalidadeDescarga As System.Windows.Forms.TextBox
    Friend WithEvents txtCodPostalCarga As System.Windows.Forms.TextBox
    Friend WithEvents txtArtigo As System.Windows.Forms.TextBox
    Friend WithEvents txtMatricula As MaskedTextBox
    Friend WithEvents dtPicker As AxXtremeSuiteControls.AxDateTimePicker
    Friend WithEvents btnImportarLista As Button
    Friend WithEvents btnAplicarTodos As Button
    Friend WithEvents btnRepartirCustos As Button
    Friend WithEvents lblTotalDoc As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblTotalDesc As Label
    Friend WithEvents lblTotalMerc As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
End Class
