﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900

Public Class FrmStocksArm

    Const COLUMN_ARTIGO As Integer = 0
    Const COLUMN_ARMAZEM As Integer = 3
    Const COLUMN_DESCRICAO As Integer = 1
    Const COLUMN_QUANTIDADE As Integer = 2
    ' Const COLUMN_QUANTIDADE As Integer = 3

    Dim aplIniciada As Boolean

    Private Sub FrmStocks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim m As Motor
        m = Motor.GetInstance
        aplIniciada = False

        lblText.Text = m.EmpresaDescricao
        Me.Text = Me.Text + "   " + m.EmpresaDescricao() + "   " + m.NomeFuncionario

        gridArtigosStock.Tag = ""

        If Not m.getApresentaProjectos Then
            lblMoldes.Text = "Armazéns"
        End If

        iniciarToolBox()
        '  inicializarMoldes()

        inicializarDocumentos()

        gridArtigos.AutoColumnSizing = True
        gridArtigos.AutoColumnSizing = True
        '  gridArtigos.ShowGroupBox = True
        ' gridArtigos.ShowItemsInGroups = False

        gridArtigosStock.AutoColumnSizing = True
        gridArtigosStock.AutoColumnSizing = True
        ' gridArtigosStock.ShowGroupBox = True
        ' gridArtigosStock.ShowItemsInGroups = False

        inserirColunasRegistos(1, gridArtigos)
        inserirColunasRegistos(2, gridArtigosStock)

        buscarArtigos(gridArtigos, txtArtigo.Text)

        If gridArtigos.SelectedRows.Count > 0 Then
            inicializarMoldes(gridArtigos.SelectedRows(0).Record(COLUMN_ARTIGO).Value)
        End If
        'gridArtigos.SortOrder.DeleteAll()
        'gridArtigos.SortOrder.Add(gridArtigos.Columns(COLUMN_ARTIGO))
        'gridArtigosStock.SortOrder.DeleteAll()
        'gridArtigos.SortOrder.Add(gridArtigos.Columns(COLUMN_ARTIGO))
        actualizarMedidas()
        aplIniciada = True
    End Sub

    Private Sub inicializarDocumentos()
        Dim m As Motor
        Dim lista As StdBELista
        m = Motor.GetInstance()
        lstDocumentos.SelectedIndex = -1
        lista = m.daListaDocumentos(True)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstDocumentos.ValueMember = "Codigo"
        lstDocumentos.DisplayMember = "Nome"
        lstDocumentos.DataSource = dt
        lstDocumentos.SelectedValue = m.DocumentoStock
        m.AplicacaoInicializada = True
    End Sub

    Private Sub inicializarMoldes(artigo As String)
        Dim m As Motor
        Dim lista As StdBELista
        Dim tipoDoc As String

        m = Motor.GetInstance()
        lstMoldes.SelectedIndex = -1
        tipoDoc = m.consultaValor("SELECT TipoDocStk FROM DocumentosInternos where documento='" + lstDocumentos.SelectedValue + "'")
        lista = m.daListaArmazens(IIf(tipoDoc = "S", artigo, ""), txtPesqMolde.Text)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        lstMoldes.DataSource = dt
        lstMoldes.ValueMember = "Codigo"
        lstMoldes.DisplayMember = "Nome"

        If tipoDoc <> "S" Then
            Dim arm As String
            arm = m.consultaValor("Select ArtigoArmazem.Armazem from ArtigoArmazem,Armazens where ArtigoArmazem.Armazem=Armazens.Armazem and Artigo='" + artigo + "' and Stkactual>0 ORDER by Armazens.Descricao")
            If arm <> "" Then
                lstMoldes.SelectedValue = arm
            End If

        End If
        '  lstMoldes.ClearSelected()
    End Sub


    Private Sub iniciarToolBox()
        Dim m As Motor
        m = Motor.GetInstance
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Limpar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Limpar"

            'Control = .Add(xtpControlButton, 2, " Imprimir", -1, False)
            'Control.BeginGroup = True
            'Control.DescriptionText = "Imprimir"
            'Control.Style = xtpButtonIconAndCaptionBelow
            Control = .Add(xtpControlButton, 4, " Registos", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Registos"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = True

            Control = .Add(xtpControlButton, 5, " Encomendas", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Encomendas"
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.Visible = m.getApresentaEncomendas

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub

    Private Sub inserirColunasRegistos(ByVal indexGrid As Integer, ByVal grid As AxXtremeReportControl.AxReportControl)

        Dim Column As ReportColumn
        Column = inserirColuna(grid, COLUMN_ARTIGO, "Artigo", 150, False, True)
        '  Column = inserirColuna(grid, COLUMN_ARMAZEM, "Armazém", 100, False, True)
        Column = inserirColuna(grid, COLUMN_DESCRICAO, "Descrição", 250, False, True)
        Column = inserirColuna(grid, COLUMN_QUANTIDADE, IIf(indexGrid = 1, "Quant Stk", "Quant"), 80, False, True)
        Column.Alignment = xtpAlignmentRight
        If indexGrid = 2 Then
            Column = inserirColuna(grid, COLUMN_ARMAZEM, "Arm.", 80, False, True)
            Column.Alignment = xtpAlignmentLeft
        End If


    End Sub

    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Sub txtPesqMolde_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesqMolde.Change
        If gridArtigos.SelectedRows.Count > 0 Then
            inicializarMoldes(gridArtigos.SelectedRows(0).Record(COLUMN_ARTIGO).Value)
        End If
    End Sub

    Private Sub buscarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim listaExclusao As String
        Dim m As Motor

        Me.Cursor = Cursors.WaitCursor
        If documento <> "" Then
            lblEmModoEdicao.Text = "Documento " + documento + " em modo de edição"
            lblEmModoEdicao.Visible = True
        Else
            lblEmModoEdicao.Visible = False
        End If
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        grid.Records.DeleteAll()
        listaExclusao = daListaArtigosExcluidos()
        '  listaExclusao = ""
        dtRegistos = m.daListaArtigos(IIf(m.getApresentaProjectos, "APS_GP_Artigos", "APS_GP_Artigos1"), pesquisa, "", listaExclusao)
        inserirLinhasRegistos(m, dtRegistos, grid)
        grid.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub inserirLinhasRegistos(m As Motor, ByVal dtRegistos As DataTable, ByVal grid As AxXtremeReportControl.AxReportControl)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(m, dtrow, grid)
        Next
    End Sub

    Private Sub inserirLinhaRegisto(m As Motor, ByVal dtRow As DataRow, ByVal grid As AxXtremeReportControl.AxReportControl, Optional ByVal valorStk As Double = -1)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem


       

        record = grid.Records.Add()


        '  record.PreviewText = "asd"
        record.Tag = dtRow
        Item = record.AddItem(dtRow("Artigo"))
       

        Item = record.AddItem(dtRow("Descricao"))
        Item.Tag = 1

        If m.getApresentaProjectos Then
            Item = record.AddItem(dtRow("StkActual"))
        Else

            If grid.Name <> "gridArtigosStock" Then
                'If aplIniciada Then
                '    If lstMoldes.SelectedValue Is Nothing Then
                '        Item = record.AddItem(dtRow("StkActual"))
                '    Else
                '        Dim stk As String
                '        stk = m.consultaValor("SELECT StkActual FROM ArtigoArmazem WHERE Artigo='" + dtRow("Artigo") + "' and Armazem='" + lstMoldes.SelectedValue + "'")
                '        If stk = "" Then stk = "0"
                '        Item = record.AddItem(stk)
                '    End If
                ' Else
                Item = record.AddItem(dtRow("StkActual"))
                '  End If
            Else
                If valorStk < 0 Then valorStk = valorStk * -1
                Item = record.AddItem(valorStk)

                If dtRow.Table.Columns.IndexOf("Armazem") = -1 Then
                    Item = record.AddItem(lstMoldes.Text)
                    Item.Tag = lstMoldes.SelectedValue
                Else
                    Item = record.AddItem(m.consultaValor("SELECT Descricao FROM Armazens WHERE  Armazem='" + dtRow("Armazem") + "'"))
                    Item.Tag = dtRow("Armazem")
                End If

            End If

        End If



    End Sub

    'Private Sub tbArtigo_Change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArtigo.Change

    '    ' buscarArtigos(gridArtigos, txtArtigo.Text)

    'End Sub

    Private Sub btnDPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDPA.Click
        enviarArtigoParaSaida()
    End Sub

    Private Sub enviarArtigoParaSaida()
        Dim dtrow As DataRow
        Dim m As Motor
        m = Motor.GetInstance
        If gridArtigos.SelectedRows.Count > 0 Then
            Dim i As Integer
            Dim index As Integer
            Dim topindex As Integer
            i = 0
            If Not m.artigoValido(gridArtigos.SelectedRows(i).Record.Item(COLUMN_ARTIGO).Value, gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Value, True) Then
                MsgBox("O Artigo selecionado não é está configurado para movimentar Stock.", MsgBoxStyle.Information, "Anphis")
                Exit Sub
            End If
            '  For i = 0 To gridArtigos.SelectedRows.Count - 1
            index = gridArtigos.SelectedRows(i).Index
            topindex = gridArtigos.TopRowIndex
            dtrow = gridArtigos.SelectedRows(i).Record.Tag
            Dim f As FrmQt
            f = New FrmQt
            f.Valor = 1
            f.ShowDialog()
            If f.BotaoSeleccionado = 1 Then
                inserirLinhaRegisto(m, dtrow, gridArtigosStock, f.Valor)
                '   gridArtigos.SelectedRows(i).Record.Visible = False
                ' gridArtigos.SelectedRows(i).Record.Item(COLUMN_DESCRICAO).Tag = 0

                gridArtigosStock.Populate()
                '    gridArtigos.Populate()
                ' gridArtigos.TopRowIndex = topindex
                '  If index + 1 < gridArtigos.Rows.Count Then
                'gridArtigos.SelectedRows.Add(gridArtigos.Rows(index))
                'gridArtigos.Navigator.MoveToRow(index)
                '  End If

            End If
            lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
            lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
            Me.Activate()
        End If
    End Sub

    Private Sub seleccionarBotao()

    End Sub
    Private Sub btnEPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPA.Click
        retirarArtigoSaida()
    End Sub

    Private Sub retirarArtigoSaida()
        If gridArtigosStock.SelectedRows.Count > 0 Then
            Dim index As Integer
            '  Dim index1 As Integer
            Dim record As ReportRecord
            Dim i As Integer
            i = 0
            ' For i = 0 To gridArtigosStock.SelectedRows.Count - 1
            'ir buscar o index dos artigos a movimentar stock
            index = gridArtigosStock.SelectedRows(i).Record.Index
            'ir buscar o index da  grelha dos artigos 
            'buscar a linha
            record = gridArtigosStock.SelectedRows(i).Record
            gridArtigosStock.Records.RemoveAt(index)
            ' Next i
            colocarArtigoVisivel(record(COLUMN_ARTIGO).Value)
            gridArtigosStock.Populate()

            'buscarArtigos(gridArtigos, txtArtigo.Text)

        End If
    End Sub

    Private Sub colocarArtigoVisivel(artigo As String)
        Dim row As ReportRecord
        For Each row In gridArtigos.Records
            If LCase(row(COLUMN_ARTIGO).Value) = LCase(artigo) Then
                row.Visible = True
            End If
        Next
        gridArtigos.Populate()
    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute
        Select Case e.control.Index
            Case 1 : actualizar(True)
            Case 2 : abrirEditorRegistos()
            Case 3 : abrirEditorEncomendas()
            Case 4 : If MsgBox("Deseja realmente sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Deseja Sair") = MsgBoxResult.Yes Then Me.Close()
        End Select
    End Sub

    Private Sub actualizar(Optional ByVal pergunta As Boolean = False)

        If pergunta Then
            If MsgBox("Deseja realmente actualizar e limpar a informação existente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
        End If

        lblEmModoEdicao.Visible = False
        txtPesqMolde.Text = ""
        txtArtigo.Text = ""
        gridArtigosStock.Tag = ""
        '   aplicarFiltro()
        gridArtigos.Records.DeleteAll()
        gridArtigosStock.Records.DeleteAll()
        gridArtigosStock.Populate()
        If gridArtigos.SelectedRows.Count > 0 Then
            inicializarMoldes(gridArtigos.SelectedRows(0).Record(COLUMN_ARTIGO).Value)
        End If
        buscarArtigos(gridArtigos, txtArtigo.Text)
    End Sub

    Private Function daListaArtigosExcluidos() As String
        daListaArtigosExcluidos = ""
        Dim lista As StdBELista
        Dim m As Motor
        m = Motor.GetInstance
        Dim tipoDoc As String

        tipoDoc = m.consultaValor("SELECT TipoDocStk FROM DocumentosInternos where documento='" + lstDocumentos.SelectedValue + "'")

        If tipoDoc = "E" Then Exit Function

        lista = m.consulta("SELECT Artigo FROM Artigo WHERE StkActual=0")

        While Not lista.NoFim
            If daListaArtigosExcluidos = "" Then
                daListaArtigosExcluidos = "'" + lista.Valor("Artigo") + "'"
            Else
                daListaArtigosExcluidos = daListaArtigosExcluidos + ",'" + lista.Valor("Artigo") + "'"
            End If
            lista.Seguinte()
        End While

        '  End If

    End Function

    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        Dim m As Motor
        m = Motor.GetInstance
        If MsgBox("Deseja realmente efectuar a gravação da " + lstDocumentos.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        If lstMoldes.SelectedValue = Nothing Then
            MsgBox("Tem de seleccionar um " + IIf(m.getApresentaProjectos, "Molde", "Armazém"), MsgBoxStyle.Critical)
            Exit Sub

        End If
        Me.Cursor = Cursors.WaitCursor

        Dim idCabecDoc As String
        idCabecDoc = gridArtigosStock.Tag

        idCabecDoc = m.actualizarDocumentoInterno(lstDocumentos.SelectedValue, m.Funcionario, lstMoldes.SelectedValue, dtpData.Value, gridArtigosStock.Records, idCabecDoc)

        Me.Cursor = Cursors.Default

        If idCabecDoc <> "" Then
            MsgBox(lstDocumentos.Text + " realidada com sucesso.", MsgBoxStyle.Information)
            actualizar()
        End If
    End Sub

    Private Sub gridArtigos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigos.RowDblClick
        ' enviarArtigoParaSaida()
    End Sub

    Private Sub gridArtigosStock_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridArtigosStock.RowDblClick
        ' retirarArtigoSaida()
        Dim f As FrmQt
        f = New FrmQt
        f.Valor = e.row.Record.Item(COLUMN_QUANTIDADE).Value
        f.ShowDialog()
        If f.BotaoSeleccionado = 1 Then
            e.row.Record.Item(COLUMN_QUANTIDADE).Value = f.Valor
        End If
        f = Nothing
        Me.Activate()
        gridArtigosStock.Populate()
    End Sub


    Private Sub abrirEditorRegistos()
        Dim f As FrmEditarStocks
        f = New FrmEditarStocks
        f.documento = lstDocumentos.SelectedValue
        f.ShowDialog()
        If f.clicouConfirmar Then
            actualizarFormulario(f.getIdLinha, f.getValorLinha(0), f.getValorLinha(6))
        End If
    End Sub

    Private Sub actualizarFormulario(ByVal id As String, ByVal documento As String, ByVal projecto As String)
        Me.Cursor = Cursors.WaitCursor
        gridArtigosStock.Tag = id
        Dim m As Motor
        m = Motor.GetInstance
        Dim dtRegistos As DataTable
        dtRegistos = m.daListaArtigosDocumento(id, "I")
        Dim dt As Date
        lstMoldes.SelectedValue = projecto
        dt = m.daDataDocumento(id, dtpData.Value, "I")
        dtpData.Value = dt
        Dim dtrow As DataRow
        gridArtigosStock.Records.DeleteAll()
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(m, dtrow, gridArtigosStock, dtrow("Quantidade"))
        Next
        txtArtigo.Text = ""
        buscarArtigos(gridArtigos, txtArtigo.Text, documento)
        gridArtigosStock.Populate()

        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"

        Me.Cursor = Cursors.Default
    End Sub


    Private Sub btnDAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDAT.Click

    End Sub

    Private Sub abrirEditorEncomendas()
        Dim fEnc As FrmEncomendas
        fEnc = New FrmEncomendas
        Me.Hide()
        fEnc.ShowDialog()
        Me.Show()

    End Sub

    Private Sub txtArtigo_KeyDownEvent(sender As Object, e As AxXtremeSuiteControls._DFlatEditEvents_KeyDownEvent) Handles txtArtigo.KeyDownEvent
        If e.keyCode = Keys.Enter Then
            aplicarFiltro()
        End If
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click
        aplicarFiltro()
    End Sub

    Private Sub filtrarArtigos(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal pesquisa As String, Optional ByVal documento As String = "")
        Dim row As ReportRecord
        Dim valor() As String
        Dim i As Integer
        pesquisa = Trim(pesquisa)
        Me.Cursor = Cursors.WaitCursor
        For Each row In grid.Records
            row.Visible = True
            If pesquisa <> "" Then
                valor = pesquisa.Split(" ")
                For i = 0 To valor.Length - 1
                    If InStr(LCase(row(COLUMN_ARTIGO).Value), LCase(valor(i))) = 0 And InStr(LCase(row(COLUMN_DESCRICAO).Value), LCase(valor(i))) = 0 Then
                        row.Visible = False
                    End If
                Next
            End If
        Next
        grid.Populate()
        lblRegisto1.Text = CStr(gridArtigos.Rows.Count) + " Registos"
        lblRegisto2.Text = CStr(gridArtigosStock.Rows.Count) + " Registos"
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub aplicarFiltro()
        filtrarArtigos(gridArtigos, formatarTexto(txtArtigo.Text))
    End Sub

    Private Function formatarTexto(texto As String) As String
        texto = Replace(texto, "$$$$", "-")
        texto = Replace(texto, "$$$", ",")
        texto = Replace(texto, "$$", "/")
        texto = Replace(texto, "$", " ")
        Return texto
    End Function


    Private Sub lstDocumentos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstDocumentos.SelectedIndexChanged
        '    If gridArtigosStock.Tag <> "" Then
        actualizar()
        'End If
        If gridArtigos.SelectedRows.Count > 0 Then
            inicializarMoldes(gridArtigos.SelectedRows(0).Record(COLUMN_ARTIGO).Value)
        End If
    End Sub

    Private Sub lstMoldes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstMoldes.SelectedIndexChanged

        If gridArtigos.SelectedRows.Count > 0 And Not lstMoldes.SelectedValue Is Nothing Then
            buscarStockArtigoArmazem(gridArtigos.SelectedRows(0).Record(COLUMN_ARTIGO).Value, lstMoldes.SelectedValue.ToString)
        End If
        'Dim m As Motor
        'm = Motor.GetInstance
        'If Not m.getApresentaProjectos Then
        '    buscarArtigos(gridArtigos, txtArtigo.Text)
        'End If
    End Sub


    Private Sub gridArtigos_SelectionChanged(sender As Object, e As EventArgs) Handles gridArtigos.SelectionChanged
        If gridArtigos.SelectedRows.Count > 0 Then
            inicializarMoldes(gridArtigos.SelectedRows(0).Record(COLUMN_ARTIGO).Value)
        End If
    End Sub

    Private Sub buscarStockArtigoArmazem(artigo As String, armazem As String)
        Dim stk As String
        Dim m As Motor
        m = Motor.GetInstance
        stk = m.consultaValor("Select StkActual FROM ArtigoArmazem where Artigo='" + artigo + "' and Armazem='" + armazem + "'")

        If stk = "" Then stk = "0"

        lblStk.Text = "Stock Actual: " + stk
    End Sub
   
 
  
    Private Sub FrmStocksArm_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        actualizarMedidas()
    End Sub

    Private Sub actualizarMedidas()
        Dim total As Double
        total = 1308 - Me.Width
        gridArtigos.Width = Me.Width - 850 + (total / 2)
        If gridArtigos.Width > 450 Then
            gridArtigos.Width = 450
        End If

        txtArtigo.Left = gridArtigos.Left
        txtArtigo.Width = gridArtigos.Width - 44

        btnAplicar.Left = gridArtigos.Left + gridArtigos.Width - btnAplicar.Width

        btnDPA.Left = gridArtigos.Width + gridArtigos.Left + 7
        btnEPA.Left = gridArtigos.Width + gridArtigos.Left + 7

        gridArtigosStock.Left = btnEPA.Left + 7 + btnEPA.Width
        gridArtigosStock.Width = Me.Width - gridArtigosStock.Left - lstMoldes.Width - 35

        lstMoldes.Left = gridArtigosStock.Left + gridArtigosStock.Width + 7
        txtPesqMolde.Left = lstMoldes.Left
        lstDocumentos.Left = gridArtigosStock.Width + gridArtigosStock.Left - lstDocumentos.Width
        dtpData.Left = gridArtigosStock.Width + gridArtigosStock.Left - dtpData.Width
        lblData.Left = dtpData.Left - lblData.Width - 5
        lblMoldes.Left = txtPesqMolde.Left
        btnGravar.Left = lstMoldes.Left + lstMoldes.Width - btnGravar.Width

        lblRegisto2.Left = gridArtigosStock.Left
        lblStk.Left = lstMoldes.Left
        Label2.Left = gridArtigosStock.Left
    End Sub

  
End Class