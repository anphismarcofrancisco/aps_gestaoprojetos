﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEditarRP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEditarRP))
        Me.dtpDataI = New System.Windows.Forms.DateTimePicker()
        Me.dtpDataF = New System.Windows.Forms.DateTimePicker()
        Me.CommandBarsFrame1 = New AxXtremeCommandBars.AxCommandBarsFrame()
        Me.cmbFuncionarios = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblCT = New System.Windows.Forms.Label()
        Me.cmbCentroTrabalho = New System.Windows.Forms.ComboBox()
        Me.lblMolde = New System.Windows.Forms.Label()
        Me.cmbMolde = New System.Windows.Forms.ComboBox()
        Me.gridRegistos = New AxXtremeReportControl.AxReportControl()
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager()
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridRegistos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtpDataI
        '
        Me.dtpDataI.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDataI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDataI.Location = New System.Drawing.Point(685, 36)
        Me.dtpDataI.Name = "dtpDataI"
        Me.dtpDataI.Size = New System.Drawing.Size(121, 29)
        Me.dtpDataI.TabIndex = 2
        '
        'dtpDataF
        '
        Me.dtpDataF.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDataF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDataF.Location = New System.Drawing.Point(812, 35)
        Me.dtpDataF.Name = "dtpDataF"
        Me.dtpDataF.Size = New System.Drawing.Size(121, 29)
        Me.dtpDataF.TabIndex = 3
        '
        'CommandBarsFrame1
        '
        Me.CommandBarsFrame1.Enabled = True
        Me.CommandBarsFrame1.Location = New System.Drawing.Point(-2, -4)
        Me.CommandBarsFrame1.Name = "CommandBarsFrame1"
        Me.CommandBarsFrame1.OcxState = CType(resources.GetObject("CommandBarsFrame1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBarsFrame1.Size = New System.Drawing.Size(211, 55)
        Me.CommandBarsFrame1.TabIndex = 30
        '
        'cmbFuncionarios
        '
        Me.cmbFuncionarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFuncionarios.DropDownWidth = 300
        Me.cmbFuncionarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFuncionarios.FormattingEnabled = True
        Me.cmbFuncionarios.Location = New System.Drawing.Point(483, 36)
        Me.cmbFuncionarios.Name = "cmbFuncionarios"
        Me.cmbFuncionarios.Size = New System.Drawing.Size(180, 33)
        Me.cmbFuncionarios.TabIndex = 33
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(483, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 20)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Funcionários"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(681, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 20)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Data Inicial"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(808, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 20)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "Data Final"
        '
        'lblCT
        '
        Me.lblCT.AutoSize = True
        Me.lblCT.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCT.Location = New System.Drawing.Point(211, 11)
        Me.lblCT.Name = "lblCT"
        Me.lblCT.Size = New System.Drawing.Size(123, 20)
        Me.lblCT.TabIndex = 38
        Me.lblCT.Text = "Centro Trabalho"
        '
        'cmbCentroTrabalho
        '
        Me.cmbCentroTrabalho.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCentroTrabalho.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCentroTrabalho.FormattingEnabled = True
        Me.cmbCentroTrabalho.Location = New System.Drawing.Point(215, 36)
        Me.cmbCentroTrabalho.Name = "cmbCentroTrabalho"
        Me.cmbCentroTrabalho.Size = New System.Drawing.Size(119, 33)
        Me.cmbCentroTrabalho.TabIndex = 37
        '
        'lblMolde
        '
        Me.lblMolde.AutoSize = True
        Me.lblMolde.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMolde.Location = New System.Drawing.Point(345, 11)
        Me.lblMolde.Name = "lblMolde"
        Me.lblMolde.Size = New System.Drawing.Size(52, 20)
        Me.lblMolde.TabIndex = 40
        Me.lblMolde.Text = "Molde"
        '
        'cmbMolde
        '
        Me.cmbMolde.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMolde.DropDownWidth = 200
        Me.cmbMolde.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMolde.FormattingEnabled = True
        Me.cmbMolde.Location = New System.Drawing.Point(341, 36)
        Me.cmbMolde.Name = "cmbMolde"
        Me.cmbMolde.Size = New System.Drawing.Size(136, 33)
        Me.cmbMolde.TabIndex = 39
        '
        'gridRegistos
        '
        Me.gridRegistos.Location = New System.Drawing.Point(12, 73)
        Me.gridRegistos.Name = "gridRegistos"
        Me.gridRegistos.OcxState = CType(resources.GetObject("gridRegistos.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridRegistos.Size = New System.Drawing.Size(921, 592)
        Me.gridRegistos.TabIndex = 41
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(909, 5)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 42
        '
        'FrmEditarRP
        '
        Me.ClientSize = New System.Drawing.Size(945, 677)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.gridRegistos)
        Me.Controls.Add(Me.lblMolde)
        Me.Controls.Add(Me.cmbMolde)
        Me.Controls.Add(Me.lblCT)
        Me.Controls.Add(Me.cmbCentroTrabalho)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbFuncionarios)
        Me.Controls.Add(Me.CommandBarsFrame1)
        Me.Controls.Add(Me.dtpDataF)
        Me.Controls.Add(Me.dtpDataI)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(930, 715)
        Me.Name = "FrmEditarRP"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edição de Registos Pontos"
        CType(Me.CommandBarsFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridRegistos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpDataI As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDataF As System.Windows.Forms.DateTimePicker
    Friend WithEvents CommandBarsFrame1 As AxXtremeCommandBars.AxCommandBarsFrame
    Friend WithEvents cmbFuncionarios As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblCT As System.Windows.Forms.Label
    Friend WithEvents cmbCentroTrabalho As System.Windows.Forms.ComboBox
    Friend WithEvents lblMolde As System.Windows.Forms.Label
    Friend WithEvents cmbMolde As System.Windows.Forms.ComboBox
    Friend WithEvents gridRegistos As AxXtremeReportControl.AxReportControl
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
End Class
