﻿
Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle
Imports XtremeReportControl
Imports XtremeReportControl.XTPColumnAlignment
Imports Interop.StdBE900

Public Class FrmEditarRP

    Dim clicouConfirmar_ As Boolean

    Const COLUMN_CODIGOFUNC As Integer = 0
    Const COLUMN_NOMEFUNC As Integer = 1
    Const COLUMN_POSTO As Integer = 2
    Const COLUMN_MOLDE As Integer = 3
    Const COLUMN_PECA As Integer = 4
    Const COLUMN_OPERACAO As Integer = 5
    Const COLUMN_DATA As Integer = 6
    Const COLUMN_ESTADO As Integer = 7
    Const COLUMN_OBSERVACOES As Integer = 8
    Const COLUMN_TEMPO As Integer = 9


    ReadOnly Property clicouConfirmar() As Boolean
        Get
            clicouConfirmar = clicouConfirmar_
        End Get

    End Property

    Private Sub FrmEditar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim m As MotorRP
        m = MotorRP.GetInstance
        iniciarToolBox()
        dtpDataI.Value = m.firstDayMonth(Now)
        dtpDataF.Value = m.lastDayMonth(Now)
        ' gridRegistos.AutoGenerateColumns = True

        gridRegistos.AutoColumnSizing = False
        gridRegistos.AutoColumnSizing = False
        gridRegistos.ShowGroupBox = True
        gridRegistos.ShowItemsInGroups = False

        buscarListaCentrosTrabalho()
        buscarListaMoldes()
        buscarListaFuncionariosPermissoes()
        inserirColunasRegistos()
        gridRegistos.SortOrder.DeleteAll()
        gridRegistos.SortOrder.Add(gridRegistos.Columns(COLUMN_DATA))
        Dim rowCT As DataRowView
        rowCT = cmbCentroTrabalho.SelectedItem
        Dim rowM As DataRowView
        rowM = cmbMolde.SelectedItem

        buscarRegistos(rowCT("Codigo"), rowM("Codigo"), cmbFuncionarios.SelectedValue)
        clicouConfirmar_ = False

    End Sub


    Private Sub buscarListaCentrosTrabalho()
        Dim dt As DataTable
        Dim lista As StdBELista
        Dim m As MotorRP
        m = MotorRP.GetInstance
        lista = m.daListaPostos(m.Funcionario, "", True)
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        cmbCentroTrabalho.DataSource = dt
        cmbCentroTrabalho.ValueMember = "Codigo"
        cmbCentroTrabalho.DisplayMember = "Nome"
    End Sub

    Private Sub buscarListaFuncionariosPermissoes()
        Dim dt As DataTable
        Dim m As MotorRP
        m = MotorRP.GetInstance
        dt = m.daListaFuncionariosPermissoes()
        cmbFuncionarios.DataSource = dt
        cmbFuncionarios.ValueMember = "Codigo"
        cmbFuncionarios.DisplayMember = "Nome"
        cmbFuncionarios.SelectedValue = m.Funcionario
    End Sub

    Private Sub buscarListaMoldes()
        Dim m As MotorRP
        Dim lista As StdBELista
        m = MotorRP.GetInstance()

        lista = m.daListaMoldes("", True)
        Dim dt As DataTable
        dt = m.convertRecordSetToDataTable(lista.DataSet)
        cmbMolde.DataSource = dt
        cmbMolde.ValueMember = "Codigo"
        cmbMolde.DisplayMember = "Nome"
    End Sub
    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Actualizar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Actualizar"

            Control = .Add(xtpControlButton, 2, " Imprimir", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Imprimir"
            Control.Style = xtpButtonIconAndCaptionBelow

            Control = .Add(xtpControlButton, 3, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    'Private Sub formatarColunas()
    '    gridRegistos.Columns("CDU_ID").Visible = False
    '    gridRegistos.Columns("CDU_IDLinha").Visible = False
    '    gridRegistos.Columns("Centro Trabalho").Width = gridRegistos.Columns("Centro Trabalho").Width + 10
    '    'gridRegistos.Columns("CDU_Funcionario").Visible = False
    'End Sub

    Public Function getIdLinha() As String
        getIdLinha = ""
        If clicouConfirmar_ Then
            If gridRegistos.SelectedRows.Count > 0 Then
                If Not gridRegistos.SelectedRows.Row(0).GroupRow Then
                    getIdLinha = gridRegistos.SelectedRows(0).Record.Tag.ToString
                End If
            End If
        End If
    End Function

    Private Sub gridRegistos_CellMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs)
        clicouConfirmar_ = True
        Me.Close()
    End Sub

    Private Sub inserirColunasRegistos()

        Dim Column As ReportColumn
        Column = inserirColuna(True, COLUMN_CODIGOFUNC, "Código", 200, False, True)
        Column.Visible = False
        Column = inserirColuna(True, COLUMN_NOMEFUNC, "Funcionário", 130, False, True)
        Column = inserirColuna(True, COLUMN_POSTO, "Centro Trabalho", 100, False, True)
        Column = inserirColuna(True, COLUMN_MOLDE, "Molde", 100, False, True)
        Column = inserirColuna(True, COLUMN_PECA, "Peça", 100, False, True)
        Column = inserirColuna(True, COLUMN_OPERACAO, "Operação", 100, False, True)
        Column = inserirColuna(True, COLUMN_DATA, "Data", 100, False, True)
        Column.Alignment = xtpAlignmentCenter
        Column = inserirColuna(True, COLUMN_ESTADO, "Estado", 100, False, True)
        Column.Alignment = xtpAlignmentLeft
        Column = inserirColuna(True, COLUMN_OBSERVACOES, "Observacoes", 100, False, True)
        Column.Alignment = xtpAlignmentLeft
        Column = inserirColuna(True, COLUMN_TEMPO, "Tempo", 60, False, True)
        Column.Alignment = xtpAlignmentRight

    End Sub

    Private Function inserirColuna(ByVal isGridSE As Boolean, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = gridRegistos.Columns.Add(index, texto, tamanho, Resizable)

        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function

    Private Sub buscarRegistos(ByVal centroTrabalho As String, ByVal molde As String, ByVal funcionario As String)
        Dim m As MotorRP
        Dim total As Double
        m = MotorRP.GetInstance
        Me.Cursor = Cursors.WaitCursor
        gridRegistos.Records.DeleteAll()
        gridRegistos.HeaderRecords.DeleteAll()
        gridRegistos.FooterRecords.DeleteAll()
        Dim dtRegistos As DataTable
        dtRegistos = m.daListaRegistos(centroTrabalho, molde, funcionario, dtpDataI.Value, dtpDataF.Value)

        inserirLinhasRegistos(dtRegistos)
        If gridRegistos.GroupsOrder.Count = 0 Then
            gridRegistos.GroupsOrder.DeleteAll()
            gridRegistos.GroupsOrder.Add(gridRegistos.Columns(buscarIndexColunaPosto))
            gridRegistos.Columns(buscarIndexColunaPosto).Visible = False
        End If
        total = m.daTotalRegistos(centroTrabalho, molde, funcionario, dtpDataI.Value, dtpDataF.Value)

        inserirLinhaTotal(total)
        gridRegistos.Populate()
        inserirSubtotais()
        Me.Cursor = Cursors.Default
    End Sub

    Private Function buscarIndexColunaPosto() As Integer

        Dim col As ReportColumn
        For Each col In gridRegistos.Columns
            If col.Caption = "Data" Then
                Return col.Index
            End If
        Next
        Return COLUMN_DATA
    End Function
    Private Sub inserirLinhaTotal(ByVal total As Double)
        Dim frr As ReportRecord
        ' gridRegistos.ShowFooter = True
        gridRegistos.ShowFooterRows = True
        ' gridRegistos.ShowHeaderRows = True
        'frr = gridRegistos.HeaderRecords.Add()
        'frr.AddItem(CStr(dtpDataI.Value) + " a " + CStr(dtpDataF.Value))
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")
        'frr.AddItem("")

        If total < 0 Then total = total * -1

        frr = gridRegistos.FooterRecords.Add()
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("")
        frr.AddItem("Total")
        frr.AddItem(total)


    End Sub
    Private Sub inserirLinhasRegistos(ByVal dtRegistos As DataTable)
        Dim dtrow As DataRow
        For Each dtrow In dtRegistos.Rows
            inserirLinhaRegisto(dtrow)
        Next
    End Sub

    Private Sub inserirLinhaRegisto(ByVal dtRow As DataRow)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim estado As String
        estado = NuloToString(dtRow("Estado"))
        record = gridRegistos.Records.Add()
        '  record.PreviewText = "asd"
        record.Tag = dtRow("CDU_IDPonto")
        Item = record.AddItem(dtRow("Codigo"))
        Item.Tag = dtRow("CDU_IdLinha")
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Funcionario"))
        Item.Tag = "CDU_Funcionario"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Centro Trabalho"))
        Item.Tag = "CDU_Posto"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Molde"))
        Item.Tag = "CDU_Molde"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Peça"))
        Item.Tag = "CDU_Peca"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Operação"))
        Item.Tag = "CDU_Operacao"
        aplicarFormatoLinha(estado, Item)
        Item = record.AddItem(dtRow("Data"))
        Item.Tag = "CDU_Data"
        aplicarFormatoLinha(estado, Item)
        Item.Caption = FormatDateTime(dtRow("Data"), DateFormat.ShortDate)
        aplicarFormatoLinha(estado, Item)

        Item = record.AddItem(dtRow("EstadoTrabalho"))
        aplicarFormatoLinha(estado, Item)

        Item = record.AddItem(dtRow("Observacoes"))
        aplicarFormatoLinha(estado, Item)

        If dtRow("Tempo") < 0 Then
            Item = record.AddItem(dtRow("Tempo") * -1)
        Else
            Item = record.AddItem(dtRow("Tempo"))
        End If

        Item.Tag = estado
        aplicarFormatoLinha(estado, Item)
    End Sub

    Private Function NuloToString(ByVal valor As Object)
        If IsDBNull(valor) Then
            Return ""
        Else
            Return valor
        End If
    End Function

    Private Sub aplicarFormatoLinha(ByVal estado As String, ByVal item As ReportRecordItem)

        Select Case estado
            Case "N" : item.BackColor = 12632256
            Case "F" : item.BackColor = 14671839
        End Select


    End Sub

    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute

        Dim rowCT As DataRowView
        Dim rowM As DataRowView
        rowCT = cmbCentroTrabalho.SelectedItem
        rowM = cmbMolde.SelectedItem

        Select Case e.control.Index
            Case 1 : buscarRegistos(rowCT("Codigo"), rowM("Codigo"), cmbFuncionarios.SelectedValue)
            Case 2 : imprimir()
            Case 3 : Me.Close()
        End Select
    End Sub
    Private Sub imprimir()
        'gridRegistos.PrintPreviewOptions.Title = ""
        gridRegistos.PrintOptions.Header.FormatString = "Registos de Folhas Ponto" + vbCrLf + vbCrLf + "De " + CStr(dtpDataI.Value) + " a " + CStr(dtpDataF.Value)
        gridRegistos.PrintOptions.Header.Font.Size = 12
        gridRegistos.PrintOptions.BlackWhitePrinting = True
        gridRegistos.PrintOptions.Landscape = False
        gridRegistos.PrintPreview(True)
    End Sub

    Private Sub FrmEditar_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        gridRegistos.Width = Me.Width - (45)
        gridRegistos.Height = Me.Height - (gridRegistos.Top + 45)
    End Sub

    Private Sub gridRegistos_RowDblClick(ByVal sender As System.Object, ByVal e As AxXtremeReportControl._DReportControlEvents_RowDblClickEvent) Handles gridRegistos.RowDblClick


        If Not e.item Is Nothing Then
            Dim estado As String
            estado = e.item.Record.Item(COLUMN_TEMPO).Tag
            If estado <> "N" And estado <> "F" Then
                Dim m As MotorRP
                m = MotorRP.GetInstance
                If e.item.Record.Item(COLUMN_CODIGOFUNC).Value = m.Funcionario Then
                    clicouConfirmar_ = True
                    Me.Close()
                Else
                    mostrarMensagem("O")
                End If
            Else
                mostrarMensagem(estado)
            End If
        End If
    End Sub

    Private Sub mostrarMensagem(ByVal estado As String)
        Select Case estado
            Case "N" : MsgBox("O registo já se encontra Anulado", MsgBoxStyle.Exclamation)
            Case "F" : MsgBox("O registo já se encontra Fechado", MsgBoxStyle.Exclamation)
            Case "O" : MsgBox("O registo seleccionado é de outro funcionário", MsgBoxStyle.Exclamation)
        End Select
    End Sub

    Private Sub inserirSubtotais()
        '   gridRegistos.ReCalc(True)
        Dim m As MotorRP
        Dim row As ReportRow
        Dim groupRow As ReportGroupRow
        m = MotorRP.GetInstance
        For i = 0 To gridRegistos.Rows.Count - 1
            row = gridRegistos.Rows(i)
            If row.GroupRow Then
                groupRow = row

                'groupRow.GroupFormat = " [SubTotal $=%.02f]"
                groupRow.GroupCaption = groupRow.GroupCaption + " - " + CStr(buscarTotaisGroup(groupRow)) + " H"  ' CStr(m.daTotalRegistosCT(row.Childs(0).Record.Item(COLUMN_POSTO).Value, dtpDataI.Value, dtpDataF.Value)) + " H"
                'gridRegistos.GroupsOrder.
                'groupRow.GroupFormula = "SUMSUB(R*C2:R*C4)" 'Old notation
                '  groupRow.GroupFormula = "SUMSUB(C2:C3) SUMSUB(C3:C4)" 'New (short) notation
                ' groupRow.GroupCaption = "x"
            End If
        Next

    End Sub

    Private Sub gridRegistos_SortOrderChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Function buscarTotaisGroup(ByVal groupRow As ReportGroupRow) As Double
        Dim total As Double
        total = 0
        Dim linha As ReportRow
        For Each linha In groupRow.Childs
            If Not linha.GroupRow Then
                total = total + linha.Record(COLUMN_TEMPO).Value
            Else
                total = total + buscarTotaisGroup(linha)
            End If
        Next
        Return total
    End Function


   
End Class
