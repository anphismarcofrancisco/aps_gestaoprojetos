﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Windows.Forms

Namespace Primavera.Integration2
    NotInheritable Class Program
        Private Sub New()
        End Sub
        ''' <summary>
        ''' The main entry point for the application.
        ''' </summary>
        <STAThread> _
        Shared Sub Main()
            ' permite carregar as dlls automaticamente quando as mesmas são necessárias através da pasta colocada no método AssemblyResolver() 
            AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf AssemblyLoader.AssemblyResolver900

            Application.EnableVisualStyles()
            Application.SetCompatibleTextRenderingDefault(False)
            Application.Run(New FrmLogin())
            'Application.Run(New FrmConfINI())
            'Application.Run(New FrmConfPontos())
        End Sub
    End Class
End Namespace

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
