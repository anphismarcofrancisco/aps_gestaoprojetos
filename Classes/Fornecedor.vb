﻿Public Class Fornecedor

    Public Sub New()

    End Sub


    ''' <summary>
    ''' Fornecedpr
    ''' </summary>
    ''' <remarks></remarks>
    Dim fornecedor_ As String
    Property Fornecedor() As String
        Get
            Fornecedor = Fornecedor_
        End Get
        Set(ByVal value As String)
            Fornecedor_ = value
        End Set
    End Property

    ''' <summary>
    ''' Nome Fornecedor
    ''' </summary>
    ''' <remarks></remarks>
    Dim nome_ As String
    Property Nome As String
        Get
            Nome = nome_
        End Get
        Set(ByVal value As String)
            nome_ = value
        End Set
    End Property

    ''' <summary>
    ''' Contribuinte
    ''' </summary>
    ''' <remarks></remarks>
    Dim contribuinte_ As String
    Property Contribuinte As String
        Get
            Contribuinte = contribuinte_
        End Get
        Set(ByVal value As String)
            contribuinte_ = value
        End Set
    End Property

    ''' <summary>
    ''' Morada
    ''' </summary>
    ''' <remarks></remarks>
    Dim morada_ As String
    Property Morada As String
        Get
            Morada = morada_
        End Get
        Set(ByVal value As String)
            morada_ = value
        End Set
    End Property

    ''' <summary>
    ''' Morada 2
    ''' </summary>
    ''' <remarks></remarks>
    Dim morada2_ As String
    Property Morada2 As String
        Get
            Morada2 = morada2_
        End Get
        Set(ByVal value As String)
            morada2_ = value
        End Set
    End Property

    ''' <summary>
    ''' Localidade
    ''' </summary>
    ''' <remarks></remarks>
    Dim localidade_ As String
    Property Localidade As String
        Get
            Localidade = localidade_
        End Get
        Set(ByVal value As String)
            localidade_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal
    ''' </summary>
    ''' <remarks></remarks>
    Dim codPostal_ As String
    Property CodPostal As String
        Get
            CodPostal = codPostal_
        End Get
        Set(ByVal value As String)
            codPostal_ = value
        End Set
    End Property

    ''' <summary>
    ''' CodPostal
    ''' </summary>
    ''' <remarks></remarks>
    Dim CodPostalLocalidade_ As String
    Property CodPostalLocalidade As String
        Get
            CodPostalLocalidade = CodPostalLocalidade_
        End Get
        Set(ByVal value As String)
            CodPostalLocalidade_ = value
        End Set
    End Property


End Class
