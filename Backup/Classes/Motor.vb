﻿Imports PlatAPSNET
Imports GcpBE800
Imports StdBE800
Imports ErpBS800
Imports System.IO
Imports XtremeReportControl
Imports StdPlatBS800

Public Class Motor

    Shared myInstance As Motor
    Dim bso As ErpBS
    Dim PlataformaPrimavera As StdPlatBS
    Dim nomeEmpresa_ As String
    Dim codEmpresa As String
    Dim utilizador As String
    Dim password As String
    Dim tipoPlat As String
    Dim ficheiroAccess_ As String
    Dim file As String
    Dim log As TextBox
    Dim funcionario_ As String
    Dim nomefuncionario_ As String

    Dim funcionarioAdm_ As Boolean

    Dim modoFuncionamento As String

    Dim empresas_ As String
    Dim apliInicializada As Boolean

    Dim ArmazemDefault As String

    Dim artigoML As String
    Dim objConfApl As StdBSConfApl

    Dim listaDocumentos As String

    Dim campoMolde As String
    Dim campoRequisicao As String

    Private Sub New()
        Try
            Dim ficheiroConfiguracao As Configuracao
            Dim ficheiroIni As IniFile

            apliInicializada = False
            ficheiroConfiguracao = Configuracao.GetInstance
            ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)

            bso = New ErpBS
            PlataformaPrimavera = New StdPlatBS

            utilizador = ficheiroIni.GetString("ERP", "UTILIZADOR", "")
            password = ficheiroIni.GetString("ERP", "PASSWORD", "")
            codEmpresa = ficheiroIni.GetString("ERP", "EMPRESA", "")
            empresas_ = ficheiroIni.GetString("ERP", "EMPRESAS", "'" + codEmpresa + "'")
            tipoPlat = ficheiroIni.GetString("ERP", "TIPOPLAT", 1)
            modoFuncionamento = ficheiroIni.GetString("ERP", "MODO", "M")

            tipoDocP_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCP", "")
            serieP_ = ficheiroIni.GetString("DOCUMENTO", "SERIEP", "")
            tipoEntidadeP_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADEP", "")

            tipoDocPS_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCPS", "")
            seriePS_ = ficheiroIni.GetString("DOCUMENTO", "SERIEPS", "")

            campoMolde = ficheiroIni.GetString("DOCUMENTO", "CAMPOMOLDE", "CDU_VMolde")
            campoRequisicao = ficheiroIni.GetString("DOCUMENTO", "CAMPOREQUISICAO", "CDU_VRequisicao")


            tipoDocS_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCS", "")
            serieS_ = ficheiroIni.GetString("DOCUMENTO", "SERIES", "")
            tipoEntidadeS_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADES", "")

            tipoDocC_ = ficheiroIni.GetString("DOCUMENTO", "TIPODOCC", "")
            serieC_ = ficheiroIni.GetString("DOCUMENTO", "SERIEC", "")
            tipoEntidadeC_ = ficheiroIni.GetString("DOCUMENTO", "TIPOENTIDADEC", "")
            listaDocumentos = ficheiroIni.GetString("DOCUMENTO", "DOCUMENTOSC", "")

            artigoML = ficheiroIni.GetString("ERP", "ART_MAT_LIM", "")

            ArmazemDefault = ficheiroIni.GetString("ERP", "ARMAZEMDEFAULT", "A1")

            MODULO_STOCKS_ = ficheiroIni.GetString("ERP", "MODULO", "S")

            bso.AbreEmpresaTrabalho(tipoPlat, codEmpresa, utilizador, password)


            objConfApl = New StdBSConfApl
            objConfApl.AbvtApl = ficheiroIni.GetString("ERP", "APLICACAO", "")
            objConfApl.Instancia = ficheiroIni.GetString("ERP", "INSTANCIA", "")
            objConfApl.Utilizador = utilizador
            objConfApl.PwdUtilizador = password

            PlataformaPrimavera.AbrePlataformaEmpresaIntegrador(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional)
            'PlataformaPrimavera.AbrePlataformaEmpresa(codEmpresa, Nothing, objConfApl, EnumTipoPlataforma.tpProfissional, "primavera")

        Catch ex As Exception
            MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    ''' <summary>
    ''' Funcao que permite retornar a empresa selecionada
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getArmazemDefault() As String
        Return ArmazemDefault
    End Function

    ''' <summary>
    ''' Funcao que permite o modulo do documento a criar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getMOLDULOP() As String
        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        Return ficheiroIni.GetString("DOCUMENTO", "MOLDULOP", "I")
    End Function

    ''' <summary>
    ''' Funcao que permite o verificar se grava o documento ao terminar um ponto
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getGRAVAFINALIZAR() As Boolean
        Dim ficheiroIni As IniFile
        Dim ficheiroConfiguracao As Configuracao
        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        Return ficheiroIni.GetBoolean("DOCUMENTO", "GRAVAFINALIZAR", True)
    End Function

    ''' <summary>
    ''' Funcao que permite retornar a empresa selecionada
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getEmpresa() As String
        Return codEmpresa
    End Function

    Public Sub setEmpresa(ByVal empresa_ As String)
        If apliInicializada Then
            Try
                bso.FechaEmpresaTrabalho()
                bso.AbreEmpresaTrabalho(tipoPlat, empresa_, utilizador, password)
                codEmpresa = empresa_
            Catch ex As Exception
                MsgBox("Erro ao inicializar a plataforma " + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            End Try
        End If

    End Sub
    ''' <summary>
    ''' Função que permite verificar que modulo é necessário inicializar
    ''' </summary>
    ''' <remarks></remarks>
    Dim MODULO_STOCKS_ As String
    ReadOnly Property MODULO_STOCKS() As Boolean
        Get
            MODULO_STOCKS = MODULO_STOCKS_ = "S"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_MAOOBRA() As String
        Get
            MATERIAL_MAOOBRA = "MO"
        End Get
    End Property

    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_LIMPEZA() As String
        Get
            MATERIAL_LIMPEZA = "ML"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_RTJ() As String
        Get
            MATERIAL_RTJ = "MR"
        End Get
    End Property


    ''' <summary>
    ''' Código de Material de Limpeza
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property MATERIAL_CLIENTE() As String
        Get
            MATERIAL_CLIENTE = "MC"
        End Get
    End Property

    ''' <summary>
    ''' Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Funcionario() As String
        Get
            Funcionario = funcionario_
        End Get
        Set(ByVal value As String)
            funcionario_ = value
        End Set
    End Property

    ''' <summary>
    ''' Nome Funcionario logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property NomeFuncionario() As String
        Get
            NomeFuncionario = nomefuncionario_
        End Get
        Set(ByVal value As String)
            nomefuncionario_ = value
        End Set
    End Property


    ''' <summary>
    ''' Permite validar se um funcionario é administrador
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property FuncionarioAdm() As Boolean
        Get
            FuncionarioAdm = funcionarioAdm_
        End Get
        Set(ByVal value As Boolean)
            funcionarioAdm_ = value
        End Set
    End Property

    Dim tipoDocC_ As String = ""
    Property TipoDocC() As String
        Get
            TipoDocC = tipoDocC_
        End Get
        Set(ByVal value As String)
            tipoDocC_ = value
        End Set
    End Property

    Dim serieC_ As String = ""
    Property SerieC() As String
        Get
            SerieC = serieC_
        End Get
        Set(ByVal value As String)
            serieC_ = value
        End Set
    End Property


    Dim tipoEntidadeC_ As String = "F"
    Property TipoEntidadeC() As String
        Get
            TipoEntidadeC = tipoEntidadeC_
        End Get
        Set(ByVal value As String)
            tipoEntidadeC_ = value
        End Set
    End Property


    Dim tipoDocS_ As String = ""
    Property TipoDocS() As String
        Get
            TipoDocS = tipoDocS_
        End Get
        Set(ByVal value As String)
            tipoDocS_ = value
        End Set
    End Property

    Dim serieS_ As String = ""
    Property SerieS() As String
        Get
            SerieS = serieS_
        End Get
        Set(ByVal value As String)
            serieS_ = value
        End Set
    End Property


    Dim tipoEntidadeS_ As String = "U"
    Property TipoEntidadeS() As String
        Get
            TipoEntidadeS = tipoEntidadeS_
        End Get
        Set(ByVal value As String)
            tipoEntidadeS_ = value
        End Set
    End Property


    Dim tipoDocP_ As String = ""
    Property TipoDocP() As String
        Get
            TipoDocP = tipoDocP_
        End Get
        Set(ByVal value As String)
            tipoDocP_ = value
        End Set
    End Property

    Dim serieP_ As String = ""
    Property SerieP() As String
        Get
            SerieP = serieP_
        End Get
        Set(ByVal value As String)
            serieP_ = value
        End Set
    End Property

    Dim tipoDocPS_ As String = ""
    Property TipoDocPS() As String
        Get
            TipoDocPS = tipoDocPS_
        End Get
        Set(ByVal value As String)
            tipoDocPS_ = value
        End Set
    End Property

    Dim seriePS_ As String = ""
    Property SeriePS() As String
        Get
            SeriePS = seriePS_
        End Get
        Set(ByVal value As String)
            seriePS_ = value
        End Set
    End Property


    Dim tipoEntidadeP_ As String = "U"
    Property TipoEntidadeP() As String
        Get
            TipoEntidadeP = tipoEntidadeP_
        End Get
        Set(ByVal value As String)
            tipoEntidadeP_ = value
        End Set
    End Property


    ReadOnly Property DocumentoCompra() As String
        Get
            DocumentoCompra = tipoDocC_
        End Get

    End Property

    ReadOnly Property Empresa() As String
        Get
            Empresa = codEmpresa
        End Get

    End Property

    ReadOnly Property EmpresaDescricao() As String
        Get
            EmpresaDescricao = bso.Contexto.IDNome
        End Get

    End Property


    Property AplicacaoInicializada() As Boolean
        Get
            AplicacaoInicializada = apliInicializada
        End Get
        Set(ByVal value As Boolean)
            apliInicializada = value
        End Set
    End Property



    Public Sub setLog(ByVal log_ As TextBox)
        log = log_
    End Sub

    Public Sub fechaEmpresa()
        bso.FechaEmpresaTrabalho()
        PlataformaPrimavera.FechaPlataforma()
    End Sub

    Public Shared Function GetInstance() As Motor
        If myInstance Is Nothing Then
            myInstance = New Motor
        End If
        Return myInstance
    End Function

    Private Function buscarIva(ByVal taxa As Double) As String
        Return consultaValor("SELECT Iva FROM iva WHERE Taxa=" + taxa.ToString())
    End Function

    Private Function buscarTaxaIva(ByVal codIva As String) As String
        Return consultaValor("SELECT Taxa FROM iva WHERE iva=" + codIva)
    End Function

    ''' <summary>
    ''' Função que permite a execução de um comando 
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function executarComando(ByVal comando As String)
        Try
            bso.DSO.Plat.ExecSql.ExecutaXML(comando)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return ""
    End Function

    Public Function consulta(ByVal query As String) As StdBELista

        Try
            Return bso.Consulta(query)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Function

    Public Function consultaValor(ByVal query As String) As String
        Dim lista As StdBELista
        '    InputBox("", "", query)
        lista = bso.Consulta(query)
        If lista.NoFim Then
            Return ""
        Else
            Return lista.Valor(0).ToString
        End If
    End Function
   

    ''' <summary>
    ''' Função que permite validar a password do respectivo funcionario
    ''' </summary>
    ''' <param name="funcionario"></param>
    ''' <param name="password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function validaPassword(ByVal funcionario As String, ByVal password As String) As Boolean
        Try
            Dim numLinhas As String
            numLinhas = consultaValor("SELECT  COUNT(*) FROM Funcionarios WHERE Codigo='" + funcionario + "' and CDU_Password='" + password + "'")
            Return numLinhas = "1"
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Função que permite ir buscar o posto local
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daPosto() As String
        Dim ficheiroConfiguracao As Configuracao
        Dim ficheiroIni As IniFile

        ficheiroConfiguracao = Configuracao.GetInstance
        ficheiroIni = New IniFile(ficheiroConfiguracao.getFicheiroConfiguracao)
        daPosto = ficheiroIni.GetString("ERP", "POSTO", "")
        ficheiroIni = Nothing
    End Function


    Public Function convertRecordSetToDataTable(ByVal MyRs As ADODB.Recordset) As DataTable
        'Create and fill the dataset from the recordset and populate grid from Dataset. 
        Dim myDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter()
        Dim myDS As DataSet = New DataSet("MyTable")
        myDA.Fill(myDS, MyRs, "MyTable")
        Return myDS.Tables("MyTable")
    End Function


    Public Function firstDayMonth(ByVal data As Date) As Date
        Dim firstDay As DateTime
        firstDay = CDate("01-" + CStr(Month(data)) + "-" + CStr(Year(data)))
        Return firstDay
    End Function

    Public Function lastDayMonth(ByVal data As Date) As Date
        Dim lastDay As Date
        lastDay = CDate(data)
        lastDay = DateAdd(DateInterval.Month, 1, lastDay)
        lastDay = Convert.ToDateTime("1-" & Month(lastDay).ToString() & "-" & Year(lastDay).ToString())
        lastDay = DateAdd(DateInterval.Day, -1, lastDay)
        Return lastDay
    End Function

    Public Function daListaRegistos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim restricaoCT As String
        restricaoCT = ""
        If centroTrabalho <> "Todos" Then
            restricaoCT = "AND TDU_RegistoFolhaPonto.CDU_Posto='" + centroTrabalho + "'"
        End If
        If func = "0" Then
            ' InputBox("", "", "Select LinhasInternos.Estado, Funcionarios.Codigo as Codigo, Funcionarios.Nome as Funcionario, TDU_RegistoFolhaPonto.CDU_Posto as [Centro Trabalho],CDU_Molde as Molde,CDU_Peca as Peça,CDU_Operacao as Operação,  CDU_DataFinal as Data, CDU_Tempo as Tempo,TDU_RegistoFolhaPonto.CDU_EstadoTrabalho as EstadoTrabalho, TDU_RegistoFolhaPonto.CDU_Observacoes as Observacoes,CDU_Id, CDU_IdLinha FROM TDU_RegistoFolhaPonto INNER JOIN Funcionarios ON TDU_RegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN LinhasInternos ON TDU_RegistoFolhaPonto.CDU_IdLinha = LinhasInternos.Id WHERE CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT + " order by CDU_DataInicial")
            lista = consulta("Select LinhasInternos.Estado, Funcionarios.Codigo as Codigo, Funcionarios.Nome as Funcionario, TDU_RegistoFolhaPonto.CDU_Posto as [Centro Trabalho],CDU_Molde as Molde,CDU_Peca as Peça,CDU_Operacao as Operação,  CDU_DataFinal as Data, CDU_Tempo as Tempo,TDU_RegistoFolhaPonto.CDU_EstadoTrabalho as EstadoTrabalho, TDU_RegistoFolhaPonto.CDU_Observacoes as Observacoes,CDU_Id, CDU_IdLinha FROM TDU_RegistoFolhaPonto INNER JOIN Funcionarios ON TDU_RegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN LinhasInternos ON TDU_RegistoFolhaPonto.CDU_IdLinha = LinhasInternos.Id WHERE CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT + " order by CDU_DataInicial")
        Else
            lista = consulta("Select LinhasInternos.Estado, Funcionarios.Codigo as Codigo,Funcionarios.Nome as Funcionario, TDU_RegistoFolhaPonto.CDU_Posto as [Centro Trabalho],CDU_Molde as Molde,CDU_Peca as Peça,CDU_Operacao as Operação,  CDU_DataFinal as Data, CDU_Tempo as Tempo,TDU_RegistoFolhaPonto.CDU_EstadoTrabalho as EstadoTrabalho, TDU_RegistoFolhaPonto.CDU_Observacoes as Observacoes,CDU_Id, CDU_IdLinha FROM TDU_RegistoFolhaPonto INNER JOIN Funcionarios ON TDU_RegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN LinhasInternos ON TDU_RegistoFolhaPonto.CDU_IdLinha = LinhasInternos.Id WHERE  CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario='" + func + "' " + restricaoCT + " order by CDU_DataInicial")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daListaRegistosStocks(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        Dim tipodoc As String

        If modulo = "S" Then
            tipodoc = tipoDocS_
        Else
            tipodoc = tipoDocP_
        End If

        If func = "0" Then
            lista = consulta("SELECT CB.ID, CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.Data,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,LI.Estado,COP_Obras.Codigo as Projecto FROM LinhasInternos LI INNER JOIN CabecInternos CB ON LI.IdCabecInternos = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID   WHERE CB.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipodoc + "' and CB.Entidade IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CB.Data")
        Else
            lista = consulta("SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.Data,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,LI.Estado,COP_Obras.Codigo as Projecto FROM LinhasInternos LI INNER JOIN CabecInternos CB ON LI.IdCabecInternos = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.Entidade='" + func + "' and CB.TipoDoc='" + tipodoc + "' ORDER BY CB.Data")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daListaRegistosDocumentoCompra(ByVal tipodoc As String, ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        'If func = "0" Then
        '    lista = consulta("SELECT CB.ID, CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id  INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID   WHERE CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipoDocC_ + "' and CB.Utilizador IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT " + IIf(funcionario_ = "", "NULL", "'" + funcionario_ + "'") + ")  ORDER BY CB.DataDoc")
        'Else
        '    lista = consulta("SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,-LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.Utilizador='" + func + "' and CB.TipoDoc='" + tipoDocC_ + "' ORDER BY CB.DataDoc")
        'End If
        lista = consulta("SELECT CB.ID,CB.Entidade,CB.Nome,CB.TipoDoc,CB.Serie,CB.Numdoc,CB.DataDoc,LI.Artigo,LI.Descricao,LI.Quantidade as Quantidade,CS.Estado, COP_Obras.Codigo as Projecto FROM LinhasCompras LI INNER JOIN CabecCompras CB ON LI.IdCabecCompras = CB.Id INNER JOIN CabecComprasStatus CS ON CS.IdCabecCompras = CB.Id LEFT OUTER JOIN COP_Obras ON LI.ObraId = COP_Obras.ID  WHERE  CB.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CB.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CB.TipoDoc='" + tipodoc + "' ORDER BY CB.DataDoc")

        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function


    Public Function daListaRegistosPontos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        If getMOLDULOP() = "I" Then
            If func = "0" Then
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecInternos.Id as IdCabecInternos,CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc, CabecInternos.Serie,TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao,TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao  FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                ' InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario AS Utilizador IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            Else
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome,CabecInternos.Id as IdCabecInternos, CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc,CabecInternos.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                '   InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecInternos.TipoDoc, dbo.CabecInternos.NumDoc,CabecInternos.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecInternos.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecInternos ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecInternos.Id  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            End If
        Else

            If func = "0" Then
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecDoc.Id as IdCabecInternos,CabecDoc.TipoDoc, CabecDoc.NumDoc, CabecDoc.Serie,TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao,TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDocStatus.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao  FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id LEFT OUTER  JOIN CabecdocStatus  ON dbo.CabecDocStatus.IdCabecDoc = dbo.CabecDoc.ID WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                ' InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome, CabecDoc.Id as IdCabecInternos,CabecDoc.TipoDoc, CabecDoc.NumDoc, CabecDoc.Serie,TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao,TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDocStatus.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde  FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id INNER JOIN CabecdocStatus  ON dbo.CabecDocStatus.IdCabecDoc = dbo.CabecDoc.ID WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and   CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            Else
                lista = consulta("SELECT TDU_APSCabecRegistoFolhaPonto.CDU_DataFinal,TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome,CabecDoc.Id as IdCabecInternos, CabecDoc.TipoDoc, CabecDoc.NumDoc,CabecDoc.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDocStatus.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Requisicao,'') as Requisicao FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id LEFT OUTER JOIN CabecdocStatus  ON dbo.CabecDocStatus.IdCabecDoc = dbo.CabecDoc.ID  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
                '   InputBox("", "", "SELECT TDU_APSCabecRegistoFolhaPonto.CDU_ID AS Id, TDU_APSCabecRegistoFolhaPonto.CDU_Entidade AS Entidade, Clientes.Nome,CabecDoc.Id as IdCabecInternos, CabecDoc.TipoDoc, CabecDoc.NumDoc,CabecDoc.Serie, TDU_APSCabecRegistoFolhaPonto.CDU_DataInicial AS Data, TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo AS Artigo, Artigo.Descricao, TDU_APSLinhasRegistoFolhaPonto.CDU_Armazem AS Armazem,TDU_APSLinhasRegistoFolhaPonto.CDU_Quantidade AS Quantidade, CabecDoc.Estado, TDU_APSLinhasRegistoFolhaPonto.CDU_Projecto AS Projecto, TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario AS Utilizador, Funcionarios.Nome AS NomeFunc,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Problema,'') as Problema,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Observacoes,'') as Observacoes,ISNULL(TDU_APSCabecRegistoFolhaPonto.CDU_Molde,'') as Molde FROM  TDU_APSCabecRegistoFolhaPonto LEFT OUTER JOIN TDU_APSLinhasRegistoFolhaPonto ON TDU_APSCabecRegistoFolhaPonto.CDU_ID = dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_IdTDUCabec LEFT OUTER JOIN Funcionarios ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Funcionario = Funcionarios.Codigo LEFT OUTER JOIN Clientes ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_Entidade = dbo.Clientes.Cliente LEFT OUTER JOIN Artigo ON dbo.TDU_APSLinhasRegistoFolhaPonto.CDU_Artigo = dbo.Artigo.Artigo LEFT OUTER JOIN CabecDoc ON dbo.TDU_APSCabecRegistoFolhaPonto.CDU_IdCabec = dbo.CabecDoc.Id  WHERE CDU_DataInicial>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataInicial<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and  CDU_Funcionario='" + func + "'  ORDER BY CDU_DataInicial,TDU_APSLinhasRegistoFolhaPonto.CDU_NumLinha")
            End If
        End If


        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="func"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date) As Double

        Dim restricaoCT As String
        restricaoCT = ""
        If centroTrabalho <> "Todos" Then
            restricaoCT = "AND TDU_RegistoFolhaPonto.CDU_Posto='" + centroTrabalho + "'"
        End If

        If func = "0" Then
            daTotalRegistos = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,Funcionarios WHERE TDU_RegistoFolhaPonto.CDU_Funcionario=Funcionarios.Codigo AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "') " + restricaoCT)
        Else
            daTotalRegistos = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,Funcionarios WHERE TDU_RegistoFolhaPonto.CDU_Funcionario=Funcionarios.Codigo AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario ='" + func + "' " + restricaoCT)
        End If
    End Function



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="func"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistos(ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double

        Dim tipodoc As String
        Dim campo As String
        If modulo = "S" Then
            campo = "CabecInternos.Entidade"
            tipodoc = tipoDocS_
        Else
            tipodoc = tipoDocP_
            campo = "CabecInternos.Utilizador"
        End If

        If func = "0" Then
            daTotalRegistos = consultaValor("Select ISNULL(SUM(-Quantidade),0) as Tempo  FROM CabecInternos,LinhasInternos WHERE CabecInternos.TipoDoc='" + tipodoc + "' and CabecInternos.Id=LinhasInternos.IdCabecInternos AND CabecInternos.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecInternos.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and " + campo + " IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')")
        Else
            daTotalRegistos = consultaValor("Select ISNULL(SUM(-Quantidade),0) as Tempo  FROM CabecInternos,LinhasInternos WHERE CabecInternos.TipoDoc='" + tipodoc + "' and CabecInternos.Id=LinhasInternos.IdCabecInternos AND CabecInternos.Data>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecInternos.Data<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and " + campo + " ='" + func + "' ")
        End If
    End Function

    Public Function daTotalRegistosDocumentoCompra(ByVal tipodoc As String, ByVal centroTrabalho As String, ByVal func As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double

        '   Dim tipodoc As String
        ' tipodoc = tipoDocC_
        Return consultaValor("Select ISNULL(SUM(Quantidade),0) as Tempo  FROM CabecCompras,LinhasCompras WHERE CabecCompras.TipoDoc='" + tipodoc + "' and  CabecCompras.Id=LinhasCompras.IdCabecCompras AND CabecCompras.DataDoc>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CabecCompras.DataDoc<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105)")

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="centroTrabalho"></param>
    ''' <param name="dataI"></param>
    ''' <param name="dataF"></param>
    ''' <param name="modulo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daTotalRegistosCT(ByVal centroTrabalho As String, ByVal dataI As Date, ByVal dataF As Date, ByVal modulo As String) As Double
        Dim tipodoc As String

        If modulo = "S" Then
            tipodoc = tipoDocS_
        Else
            tipodoc = tipoDocP_
        End If
        If centroTrabalho = "0" Then
            daTotalRegistosCT = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,TDU_Postos WHERE TDU_RegistoFolhaPonto.CDU_Posto=TDU_Postos.CDU_Descricao AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and CDU_Funcionario IN (SELECT cdu_utilizadorPermissao FROM [TDU_UTILIZADORESPERMISSOES] where CDU_Utilizador='" + funcionario_ + "' UNION SELECT '" + funcionario_ + "')")
        Else
            daTotalRegistosCT = consultaValor("Select ISNULL(SUM( CDU_Tempo),0) as Tempo  FROM TDU_RegistoFolhaPonto,TDU_Postos WHERE TDU_RegistoFolhaPonto.CDU_Posto=TDU_Postos.CDU_Descricao AND CDU_DataFinal>=Convert(datetime,'" + CStr(dataI) + " 00:00:01',105) AND CDU_DataFinal<=Convert(datetime,'" + CStr(dataF) + " 23:59:59',105) and TDU_RegistoFolhaPonto.CDU_Posto ='" + centroTrabalho + "'")
        End If
    End Function

    Public Function daListaFuncionariosPermissoes() As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = consulta("SELECT '0' as Codigo,'Todos' as Nome UNION  SELECT Codigo,Nome FROM Funcionarios WHERE Codigo='" + funcionario_ + "' UNION SELECT cdu_utilizadorPermissao as Codigo,Funcionarios.Nome FROM [TDU_UTILIZADORESPERMISSOES],Funcionarios where TDU_UTILIZADORESPERMISSOES.cdu_utilizadorPermissao=Funcionarios.Codigo and CDU_Utilizador='" + funcionario_ + "'")
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todos os postos ao qual o funcionario tem acesso
    ''' </summary>
    ''' <param name="funcionario"></param>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaPostos(ByVal funcionario As String, ByVal restricao As String, Optional ByVal colocaTodos As Boolean = False) As StdBELista
        Try
            Dim lista As StdBELista
            Dim sqlTodos As String
            Dim ordenacao As String
            sqlTodos = ""
            ordenacao = ""
            If colocaTodos Then
                sqlTodos = "SELECT '0' as Codigo,'Todos' as Nome,'0' as Ordenacao  UNION "
            End If
            ordenacao = "ORDER BY Ordenacao"

            If restricao <> "" Then
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') and Nome LIKE '%" + restricao + "%' " + ordenacao)
            Else
                ' InputBox("", "", sqlTodos + "SELECT Codigo,Nome FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') ORDER BY Nome")
                lista = consulta(sqlTodos + "SELECT Codigo,Nome,Nome as Ordenacao FROM APS_GP_Postos WHERE CDU_Posto IN (SELECT CDU_Posto FROM TDU_UtilizadoresPostos WHERE CDU_Utilizador='" + funcionario + "') " + ordenacao)
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todos os funcionarios ou os funcionarios de um determinado posto
    ''' </summary>
    ''' <param name="postoTrabalho"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaFuncionarios(ByVal restricao As String, Optional ByVal postoTrabalho As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                restricao = " AND Nome LIKE '%" + restricao + "%'"
            End If
            If postoTrabalho <> "" Then
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_Funcionarios WHERE CDU_Posto='" + postoTrabalho + "' " + restricao + "ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_Funcionarios Where 1=1 " + restricao + " ORDER BY Nome")
            End If
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaCentrosTrabalho(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_CentrosTrabalho WHERE Nome LIKE '" + restricao + "%' ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_CentrosTrabalho Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaMoldes(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_Moldes WHERE Nome LIKE '" + restricao + "%' ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome FROM APS_GP_Moldes Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaPecas(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT * FROM APS_GP_Pecas WHERE Nome LIKE '" + restricao + "%' Order by Nome")
            Else
                lista = consulta("SELECT * FROM APS_GP_Pecas Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaOperacoes(ByVal funcionario As String, Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            'If restricao <> "" Then
            '    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "') and Nome LIKE '%" + restricao + "%' Order By Nome")
            'Else
            '    lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Codigo IN (SELECT CDU_Operacao FROM TDU_UtilizadoresOperacoes WHERE CDU_Utilizador='" + funcionario + "') Order By Nome")
            'End If

            If restricao <> "" Then
                lista = consulta("SELECT * FROM APS_GP_Operacoes WHERE Nome LIKE '" + restricao + "%'")
            Else
                lista = consulta("SELECT * FROM APS_GP_Operacoes")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar todas as Peças
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaProblemas(ByVal funcionario As String, Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista
            If restricao <> "" Then
                lista = consulta("SELECT * FROM APS_GP_Problemas WHERE Nome LIKE '%" + restricao + "%' Order By Nome")
            Else
                lista = consulta("SELECT * FROM APS_GP_Problemas Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    '''  Ir buscar a lista de estados de trabalho
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaEstadosTrabalho() As StdBELista
        Try
            Dim lista As StdBELista
            lista = consulta("SELECT CDU_Estado as Codigo,CDU_Descricao as Descricao FROM TDU_EstadosTrabalho Order By CDU_Descricao")
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    '''  Ir buscar a lista de empresas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaEmpresas() As StdBELista
        Try
            Dim lista As StdBELista
            '     InputBox("", "", "SELECT Codigo, IDNome as Nome FROM PRIEMPRE..EMPRESAS WHERE Codigo IN ('" + empresas_ + "')")
            lista = consulta("SELECT Codigo, IDNome as Nome FROM PRIEMPRE..EMPRESAS WHERE Codigo IN ('" + empresas_ + "')")
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função que permite ir buscar os documentos de compra que é necessário criar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaDocumentos() As StdBELista
        Try
            Dim lista As StdBELista
            lista = consulta("SELECT Documento as Codigo, Descricao as Nome FROM DocumentosCompra WHERE Documento IN ('" + listaDocumentos + "')")
            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcao que permite ir buscar os Moldes inicializados
    ''' </summary>
    ''' <param name="restricao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daListaClientes(Optional ByVal restricao As String = "") As StdBELista
        Try
            Dim lista As StdBELista

            If restricao <> "" Then
                lista = consulta("SELECT Codigo,Nome,CDU_Armazem FROM APS_GP_Clientes WHERE Nome LIKE '" + restricao + "%' ORDER BY Nome")
            Else
                lista = consulta("SELECT Codigo,Nome,CDU_Armazem FROM APS_GP_Clientes Order By Nome")
            End If

            Return lista
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub removerRegisto(ByVal registo As Registo)
        Dim comando As String
        Dim idCabecDoc As String
        Dim idCabecStk As String
        If registo.ID = "" Then
            registo.ID = consultaValor("SELECT CDU_ID FROM  TDU_APSCabecRegistoFolhaPonto WHERE CDU_Funcionario='" + registo.Funcionario + "' AND CDU_Molde='" + registo.Molde + "' AND CDu_TipoEntidade='" + registo.TipoEntidade + "' AND CDU_Entidade='" + registo.Entidade + "' AND CDU_Operacao='" + registo.Operacao + "' AND CDU_Posto='" + registo.Posto + "' and CDU_Idcabec IS NULL")
        End If

        Try
            If registo.ID <> "" Then
                bso.IniciaTransaccao()
                idCabecDoc = consultaValor("SELECT CDU_IdCabec FROM  TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'")
                idCabecStk = consultaValor("SELECT id from cabecstk where id in (select idcabecorig from linhasstk where id in (select cdu_IdLinhaInternos from TDU_APSLINHASREGISTOFOLHAPONTO where CDU_TipoArtigo='MC' and CDU_IdTDUCabec='" + registo.ID + "'))")
                If getMOLDULOP() = "I" Then
                    removerDocumentoInterno(idCabecDoc)
                Else
                    removerDocumentoVenda(idCabecDoc)
                End If

                removerDocumentoStock(idCabecStk)

                comando = "DELETE FROM  TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'"
                executarComando(comando)
                comando = "DELETE FROM  TDU_APSLinhasRegistoFolhaPonto WHERE CDU_IdTDUCabec='" + registo.ID + "'"
                executarComando(comando)
                bso.TerminaTransaccao()
                MsgBox("Registo removido com sucesso", MsgBoxStyle.Information)
            Else
                MsgBox("Registo Inexistente " + vbCrLf + vbCrLf + "Impossivel remover o registo", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            bso.DesfazTransaccao()
        End Try

    End Sub

    ''' <summary>
    '''  funcão que permite iniciar uma operacao
    ''' </summary>
    ''' <param name="centroTrabalho"></param>
    ''' <param name="cliente"></param>
    ''' <param name="operacao"></param>
    ''' <param name="molde"></param>
    ''' <param name="dtUMR"></param>
    ''' <param name="dtUMC"></param>
    ''' <param name="numeroKg"></param>
    ''' <param name="idLinha"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function iniciarOperacao(ByVal registo As Registo, ByVal terminouponto As Boolean) As String
        Dim msg As String
        Dim comando As String
        iniciarOperacao = ""
        '  If idLinha = "" Then
        msg = obraOperacaoJaIniciada(registo.Posto, registo.Operacao, funcionario_, True)
        If msg = "" Then
            Try
                bso.IniciaTransaccao()
                Dim id As String
                id = consultaValor("SELECT NEWID()")
                If terminouponto Then
                    comando = "INSERT INTO TDU_APSCabecRegistoFolhaPonto(CDU_ID,CDU_Funcionario,CDU_TipoEntidade,CDU_Entidade,CDU_Posto,CDU_Operacao,CDU_Molde,CDU_Requisicao,CDU_DataInicial,CDU_DataFinal,CDU_TempoDescontado,CDU_IdCabec,CDU_Tempo,CDU_Observacoes,CDU_Problema) VALUES('" + id + "','" + registo.Funcionario + "','" + registo.TipoEntidade + "','" + registo.Entidade + "','" + registo.Posto + "','" + registo.Operacao + "','" + registo.Molde + "','" + registo.Requisicao + "',Convert(datetime,'" + CStr(registo.DataInicial) + "',105),Convert(datetime,'" + CStr(registo.DataFinal) + "',105),Convert(datetime,'" + CStr(registo.TempoDescontado) + "',105)," + IIf(registo.CDU_IdCabec = "", "NULL", "'" + registo.CDU_IdCabec + "'") + "," + Replace(registo.LinhaRegistoMaoObra.Quantidade.ToString, ",", ".") + ",'" + registo.Observacoes + "','" + registo.Problema + "')"
                Else
                    comando = "INSERT INTO TDU_APSCabecRegistoFolhaPonto(CDU_ID,CDU_Funcionario,CDU_TipoEntidade,CDU_Entidade,CDU_Posto,CDU_Operacao,CDU_Molde,CDU_Requisicao,CDU_DataInicial) VALUES('" + id + "','" + registo.Funcionario + "','" + registo.TipoEntidade + "','" + registo.Entidade + "','" + registo.Posto + "','" + registo.Operacao + "','" + registo.Molde + "','" + registo.Requisicao + "',Convert(datetime,'" + CStr(Now) + "',105))"
                End If
                ' InputBox("", "", comando)
                executarComando(comando)
                registo.ID = id
                inserirArtigos(registo)
                bso.TerminaTransaccao()
                If terminouponto Then
                    Return registo.ID
                Else
                    Return ""
                End If
            Catch ex As Exception
                bso.DesfazTransaccao()
                ' "Motor L-339 : " + ex.Message, MsgBoxStyle.Critical
                iniciarOperacao = "Motor L-339 : " + ex.Message
            End Try
        Else
            Return "O Posto " + registo.Posto + " já se encontra iniciado com o utilizador:" + msg
        End If
        Return ""
    End Function

    Private Sub inserirArtigos(ByVal registo As Registo)
        Dim artigo As String
        Dim comando As String
        Dim numlinha As Integer

        ' bso.IniciaTransaccao()
        Try


            'Apagar todas as linhas do documento
            comando = "DELETE  FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_IdTDUCabec='" + registo.ID + "'"
            executarComando(comando)

            Dim linha As LinhaRegisto

            linha = registo.LinhaRegistoMaoObra

            numlinha = 1
            If linha.Artigo <> "" Then
                If linha.CDU_IDLinhaInterno = "" Then
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_MAOOBRA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "')"
                Else
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_MAOOBRA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + linha.CDU_IDLinhaInterno + "')"
                End If
                ' InputBox("", "", comando)
                executarComando(comando)
                numlinha = numlinha + 1
            End If

            linha = registo.LinhaRegistoMaterialLimpeza


            If Not linha Is Nothing Then
                If linha.Artigo <> "" And linha.Quantidade <> 0 Then
                    If linha.CDU_IDLinhaInterno = "" Then
                        comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_LIMPEZA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "')"
                    Else
                        comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_LIMPEZA + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(CStr(linha.Quantidade), ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + linha.CDU_IDLinhaInterno + "')"
                    End If
                    ' InputBox("", "", comando)
                    executarComando(comando)
                    numlinha = numlinha + 1
                End If
            End If


            Dim idLinha As String
            Dim lista As List(Of LinhaRegisto)

            lista = registo.LinhasRegistosUMR
            Dim i As Integer
            For i = 0 To lista.Count - 1
                linha = lista(i)
                idLinha = linha.CDU_IDLinhaInterno
                If idLinha = "" Then
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_RTJ + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "')"
                Else
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_RTJ + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + idLinha + "')"
                End If
                executarComando(comando)
                numlinha = numlinha + 1
            Next


            lista = registo.LinhasRegistosUMC

            For i = 0 To lista.Count - 1
                linha = lista(i)
                idLinha = linha.CDU_IDLinhaInterno
                If idLinha = "" Then
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_RTJ + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "')"
                Else
                    comando = "INSERT INTO TDU_APSLinhasRegistoFolhaPonto(CDU_ID,CDU_IdTDUCabec,CDU_NumLinha,CDU_TipoArtigo,CDU_Artigo,CDU_Armazem,CDU_Quantidade,CDU_Projecto,CDU_IdCabecInternos,CDU_IdLinhaInternos) VALUES(NEWID(),'" + registo.ID + "'," + CStr(numlinha) + ",'" + MATERIAL_CLIENTE + "','" + linha.Artigo + "','" + linha.Armazem + "'," + Replace(linha.Quantidade.ToString, ",", ".") + ",'" + registo.Posto + "','" + registo.CDU_IdCabec + "','" + idLinha + "')"
                End If
                executarComando(comando)
                numlinha = numlinha + 1
            Next
            '  bso.TerminaTransaccao()
        Catch ex As Exception
            ' bso.DesfazTransaccao()
        End Try
    End Sub
    Public Sub TerminarOperacaoFinal(ByVal registo As Registo, ByVal data As Date)

        Dim comando As String


        If registo.ID = "" Then
            registo.ID = consultaValor("SELECT CDU_ID FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Operacao='" + registo.Operacao + "' and CDU_Posto='" + registo.Posto + "' and CDU_Funcionario='" + funcionario_ + "' and CDU_Entidade='" + registo.ID + "' and CDU_TipoEntidade='" + tipoEntidadeP_ + "'  AND CDU_IdCabec IS NULL")
        End If
        If registo.ID = "" Then
            registo.ID = iniciarOperacao(registo, True)
            inserirArtigos(registo)
        Else
            comando = "UPDATE TDU_APSCabecRegistoFolhaPonto SET CDU_Entidade='" + registo.Entidade + "',CDU_TipoEntidade='" + registo.TipoEntidade + "',  CDU_Molde='" + registo.Molde + "', CDU_Requisicao='" + registo.Requisicao + "',CDU_Operacao='" + registo.Operacao + "', CDU_Posto='" + registo.Posto + "',CDU_DataInicial=Convert(datetime,'" + CStr(registo.DataInicial) + "',105),CDU_DataFinal=Convert(datetime,'" + CStr(registo.DataFinal) + "',105),CDU_TempoDescontado=Convert(datetime,'" + CStr(registo.TempoDescontado) + "',105),CDU_Tempo='" + Replace(registo.LinhaRegistoMaoObra.Quantidade.ToString, ",", ".") + "',CDU_Observacoes='" + registo.Observacoes + "',CDU_Problema='" + registo.Problema + "',CDU_IdCabec=" + IIf(registo.CDU_IdCabec = "", "NULL", "'" + registo.CDU_IdCabec + "'") + " WHERE CDU_Id='" + registo.ID + "'"
            executarComando(comando)
            inserirArtigos(registo)
        End If

    End Sub

    ''' <summary>
    ''' Função que permite validar se um posto já se encontra inicializado
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="molde"></param>
    ''' <param name="peca"></param>
    ''' <param name="operacao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obraOperacaoJaIniciada(ByVal posto As String, ByVal operacao As String, ByRef funcionarioPosto As String, Optional ByVal mostraMSG As Boolean = True) As String
        Dim lista As StdBELista
        lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Posto='" + posto + "' and CDU_DataFinal IS NULL")
        obraOperacaoJaIniciada = ""
        If Not lista.NoFim Then
            funcionarioPosto = lista.Valor("CDU_Funcionario")
            obraOperacaoJaIniciada = lista.Valor("CDU_Funcionario")
            If mostraMSG Then
                obraOperacaoJaIniciada = vbCrLf + vbCrLf + consultaValor("Select Nome FROM Funcionarios where Codigo='" + lista.Valor("CDU_Funcionario") + "'")
                obraOperacaoJaIniciada += vbCrLf + vbCrLf + "Cliente: " + consultaValor("Select Nome FROM Clientes where Cliente='" + lista.Valor("CDU_Entidade") + "'")
                obraOperacaoJaIniciada += vbCrLf + "Operação: " + lista.Valor("CDU_Operacao") 'consultaValor("SELECT CDU_Descricao FROM TDU_ObraN2 WHERE CDU_ObraN2='" + lista.Valor("CDU_Operacao") + "'")
            End If
            If obraOperacaoJaIniciada = "" Then
                obraOperacaoJaIniciada = " "
            End If
        End If
    End Function

    ''' <summary>
    ''' Função que permite validar se um posto já se encontra inicializado
    ''' </summary>
    ''' <param name="posto"></param>
    ''' <param name="molde"></param>
    ''' <param name="peca"></param>
    ''' <param name="operacao"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obraPodeSerTerminada(ByVal posto As String, ByVal entidade As String, ByVal operacao As String, Optional ByVal mostraMSG As Boolean = True) As String
        Dim lista As StdBELista
        lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_TipoEntidade='" + tipoEntidadeP_ + "' AND CDU_Entidade='" + entidade + "' AND CDU_Operacao='" + operacao + "' AND CDU_Posto='" + posto + "' and CDU_IdCabec IS NULL")
        obraPodeSerTerminada = ""
        If lista.NoFim Then
            obraPodeSerTerminada = funcionario_
            If mostraMSG Then
                lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Posto='" + posto + "' and CDU_IdCabec IS NULL")
                If Not lista.NoFim Then
                    obraPodeSerTerminada = consultaValor("Select Nome FROM Funcionarios where Codigo='" + lista.Valor("CDU_Funcionario") + "'")
                    obraPodeSerTerminada += vbCrLf + vbCrLf + "Cliente: " + consultaValor("Select Nome FROM Clientes where Cliente='" + lista.Valor("CDU_Entidade") + "'")
                    '  obraPodeSerTerminada += vbCrLf + "Peça: " + lista.Valor("CDU_Peca")
                    obraPodeSerTerminada += vbCrLf + "Operação: " + lista.Valor("CDU_Operacao") 'consultaValor("SELECT CDU_Descricao FROM TDU_ObraN2 WHERE CDU_ObraN2='" + lista.Valor("CDU_Operacao") + "'")
                End If
            End If
        End If
    End Function


    Public Function TerminarOperacao(ByVal registo As Registo) As Integer
        TerminarOperacao = abrirFormTerminar(registo)
    End Function

    Public Function abrirFormTerminar(ByVal registo As Registo) As Integer

    
        Dim frmT As FrmTerminar
        frmT = New FrmTerminar
        frmT.Registo = registo


        Try
            frmT.ShowDialog()
            abrirFormTerminar = frmT.BotaoSeleccionado
            frmT.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            abrirFormTerminar = 3
        End Try

    End Function
    Public Function NuloToDate(ByVal obj As Object, ByVal def As DateTime) As DateTime
        If IsDBNull(obj) Then
            NuloToDate = def
        Else
            If Not IsDate(obj) Then
                NuloToDate = def
            Else
                NuloToDate = obj
            End If
        End If
    End Function

    Public Function NuloToDouble(ByVal obj) As Double
        If IsDBNull(obj) Then
            NuloToDouble = 0
        Else
            If Not IsNumeric(obj) Then
                NuloToDouble = 0
            Else
                NuloToDouble = CDbl(obj)
            End If
        End If
    End Function

    Public Function NuloToString(ByVal obj) As String
        If IsDBNull(obj) Then
            NuloToString = ""
        Else
            NuloToString = CStr(obj)
        End If

    End Function

    Public Function actualizarDocumentoInterno(ByVal registo As Registo, ByVal data As Date) As String
        Dim docI As GcpBEDocumentoInterno
        Dim linhaI As GcpBELinhaDocumentoInterno
        Dim id As String
        Dim index As Integer
        docI = Nothing
        ' Dim dataI As String
        'dataI = getDateDocument(data)
        Dim idCabecdoc As String
        Dim idCabecStk As String
        Dim ultimoNumDoc As Long
        Dim ultimoNumDocBase As Long
        ultimoNumDoc = -1
        ultimoNumDocBase = -1
        If registo.ID <> "" Then
            idCabecdoc = consultaValor("SELECT CDU_IdCabec FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'")
            idCabecStk = consultaValor("SELECT id from cabecstk where id in (select idcabecorig from linhasstk where id in (select cdu_IdLinhaInternos from TDU_APSLINHASREGISTOFOLHAPONTO where CDU_TipoArtigo='MC' and CDU_IdTDUCabec='" + registo.ID + "'))")

            If idCabecdoc <> "" Then
                ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
                id = idCabecdoc
                Try
                    docI = bso.Comercial.Internos.EditaID(id)
                Catch ex As Exception
                    ' MsgBox(ex.Message, MsgBoxStyle.Critical)
                    docI = Nothing
                End Try

                If Not docI Is Nothing Then
                    removerDocumentoInterno(id)
                    ultimoNumDoc = docI.NumDoc - 1
                    docI = Nothing
                End If
            End If

            If idCabecStk <> "" Then
                removerDocumentoStock(idCabecStk)
            End If
        End If

        If docI Is Nothing Then
            docI = New GcpBEDocumentoInterno
            docI.Tipodoc = tipoDocP_
            If serieP_ <> "" Then
                docI.Serie = serieP_
            Else
                docI.Serie = bso.Comercial.Series.DaSerieDefeito("N", tipoDocP_, data)
            End If
        End If
        If registo.Problema <> "" Then
            docI.Observacoes = registo.Problema
        End If

        If registo.Observacoes <> "" Then
            If registo.Problema <> "" Then docI.Observacoes = docI.Observacoes + vbCrLf + vbCrLf
            docI.Observacoes = docI.Observacoes + registo.Observacoes
        End If
        '   docI.Observacoes = registo.Problema + vbCrLf + vbCrLf + registo.Observacoes

        docI.Entidade = registo.Entidade
        docI.TipoEntidade = registo.TipoEntidade
        bso.Comercial.Internos.PreencheDadosRelacionados(docI)

        If registo.Funcionario = "" Then
            docI.Utilizador = funcionario_
        Else
            docI.Utilizador = registo.Funcionario
        End If
        docI.Data = CDate(data)
        docI.DataVencimento = docI.Data
        docI.Referencia = registo.Molde
        'remove todas as linhas
        docI.Linhas.RemoveTodos()

        Dim idProjecto As String
        Dim linha As LinhaRegisto

        idProjecto = daIdProjecto(registo.Posto)

        linha = registo.LinhaRegistoMaoObra

        'Preço Mao de Obra
        If Not linha Is Nothing Then
            If linha.Artigo <> "" Then

                bso.Comercial.Internos.AdicionaLinha(docI, linha.Artigo, linha.Armazem)
                linhaI = docI.Linhas(docI.Linhas.NumItens)
                Dim precunit As Double

                precunit = daValorPrecoArtigo(docI, linhaI.Artigo, linhaI.Unidade)

                If precunit <> -1 Then
                    linhaI.PrecoUnitario = precunit
                End If

                linhaI.Quantidade = linha.Quantidade
                linhaI.ObraID = idProjecto
                linhaI.CamposUtil(campoMolde) = registo.Molde
                linhaI.CamposUtil(campoRequisicao) = registo.Requisicao

                linha.CDU_IDLinhaInterno = linhaI.ID

            Else
                MsgBox("Não existe artigo configurado para o Posto " + registo.Posto, MsgBoxStyle.Critical)
            End If
        End If

        linha = registo.LinhaRegistoMaterialLimpeza
        'Material de Limpeza

        If Not linha Is Nothing Then
            If linha.Quantidade <> 0 Then
                bso.Comercial.Internos.AdicionaLinha(docI, linha.Artigo, linha.Armazem)
                linhaI = docI.Linhas(docI.Linhas.NumItens)
                linhaI.Quantidade = linha.Quantidade
                linhaI.ObraID = idProjecto
                linhaI.CamposUtil(campoMolde) = registo.Molde
                linhaI.CamposUtil(campoRequisicao) = registo.Requisicao
                linha.CDU_IDLinhaInterno = linhaI.ID
            End If
        End If


        Dim lista As List(Of LinhaRegisto)

        lista = registo.LinhasRegistosUMR
        Dim i As Integer
        For i = 0 To lista.Count - 1
            linha = lista(i)
            bso.Comercial.Internos.AdicionaLinha(docI, linha.Artigo, linha.Armazem)
            linhaI = docI.Linhas(docI.Linhas.NumItens)
            linhaI.Quantidade = linha.Quantidade
            linhaI.ObraID = idProjecto
            linhaI.CamposUtil(campoMolde) = registo.Molde
            linhaI.CamposUtil(campoRequisicao) = registo.Requisicao
            linha.CDU_IDLinhaInterno = linhaI.ID
        Next

        actualizarDocumentoMaterialCliente(registo, idProjecto, data)

        Try
            If docI.Linhas.NumItens > 0 Then

                If ultimoNumDoc <> -1 Then
                    ultimoNumDocBase = bso.Comercial.Series.DaValorAtributo("N", tipoDocP_, docI.Serie, "Numerador")
                    bso.Comercial.Series.ActualizaNumerador("N", tipoDocP_, docI.Serie, ultimoNumDoc, data)
                End If
                bso.Comercial.Internos.Actualiza(docI)

                If ultimoNumDoc <> -1 Then
                    bso.Comercial.Series.ActualizaNumerador("N", tipoDocP_, docI.Serie, ultimoNumDocBase, data)
                End If
                Return docI.ID
            Else
                Return ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Private Sub actualizarDocumentoMaterialCliente(ByRef registo As Registo, ByVal idProjecto As String, ByVal data As Date)
        Dim lista As List(Of LinhaRegisto)
        Dim doc As GcpBEDocumentoStock
        Dim linhaS As GcpBELinhaDocumentoStock
        Dim linha As LinhaRegisto
        lista = registo.LinhasRegistosUMC

        doc = New GcpBEDocumentoStock

        If seriePS_ <> "" Then
            doc.Serie = seriePS_
        Else
            doc.Serie = bso.Comercial.Series.DaSerieDefeito("S", tipoDocPS_, data)
        End If
        doc.Tipodoc = tipoDocPS_
        bso.Comercial.Stocks.PreencheDadosRelacionados(doc)
        For i = 0 To lista.Count - 1
            linha = lista(i)
            bso.Comercial.Stocks.AdicionaLinha(doc, linha.Artigo)
            linhaS = doc.Linhas(doc.Linhas.NumItens)
            linhaS.Armazem = linha.Armazem
            linhaS.Quantidade = linha.Quantidade
            linhaS.IDObra = idProjecto
            linha.CDU_IDLinhaInterno = linhaS.IdLinha
        Next

        bso.Comercial.Stocks.Actualiza(doc)

    End Sub

    Public Function actualizarDocumentoVenda(ByVal registo As Registo, ByVal data As Date) As String
        Dim docV As GcpBEDocumentoVenda
        Dim linhaV As GcpBELinhaDocumentoVenda
        Dim id As String
        Dim index As Integer
        docV = Nothing
        ' Dim dataI As String
        'dataI = getDateDocument(data)
        Dim idCabecdoc As String
        Dim idCabecStk As String
        Dim ultimoNumDoc As Long
        Dim ultimoNumDocBase As Long
        ultimoNumDoc = -1
        ultimoNumDocBase = -1
        If Registo.ID <> "" Then
            idCabecdoc = consultaValor("SELECT CDU_IdCabec FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Id='" + registo.ID + "'")
            idCabecStk = consultaValor("SELECT id from cabecstk where id in (select idcabecorig from linhasstk where id in (select cdu_IdLinhaInternos from TDU_APSLINHASREGISTOFOLHAPONTO where CDU_TipoArtigo='MC' and CDU_IdTDUCabec='" + registo.ID + "'))")

            If idCabecdoc <> "" Then
                ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
                id = idCabecdoc
                Try
                    docV = bso.Comercial.Vendas.EditaID(id)
                Catch ex As Exception
                    ' MsgBox(ex.Message, MsgBoxStyle.Critical)
                    docV = Nothing
                End Try

                If Not docV Is Nothing Then
                    removerDocumentoVenda(id)
                    ultimoNumDoc = docV.NumDoc - 1
                    docV = Nothing
                End If
            End If

            If idCabecStk <> "" Then
                removerDocumentoStock(idCabecStk)
            End If

        End If

        If docV Is Nothing Then
            docV = New GcpBEDocumentoVenda
            docV.Tipodoc = tipoDocP_
            If serieP_ <> "" Then
                docV.Serie = serieP_
            Else
                docV.Serie = bso.Comercial.Series.DaSerieDefeito("V", tipoDocP_, data)
            End If
        End If
        If Registo.Problema <> "" Then
            docV.Observacoes = registo.Problema
        End If

        If Registo.Observacoes <> "" Then
            If registo.Problema <> "" Then docV.Observacoes = docV.Observacoes + vbCrLf + vbCrLf
            docV.Observacoes = docV.Observacoes + registo.Observacoes
        End If
        '   docI.Observacoes = registo.Problema + vbCrLf + vbCrLf + registo.Observacoes

        docV.Entidade = registo.Entidade
        docV.TipoEntidade = registo.TipoEntidade
        bso.Comercial.Vendas.PreencheDadosRelacionados(docV)

        If Registo.Funcionario = "" Then
            docV.Utilizador = funcionario_
        Else
            docV.Utilizador = registo.Funcionario
        End If
        docV.DataDoc = CDate(FormatDateTime(data, DateFormat.ShortDate))
        docV.DataVenc = docV.DataDoc
        docV.Referencia = registo.Requisicao '+ " / " + registo.Molde
        docV.Requisicao = registo.Requisicao '+ " / " + registo.Molde
        'remove todas as linhas
        docV.Linhas.RemoveTodos()

        Dim idProjecto As String
        Dim linha As LinhaRegisto

        idProjecto = daIdProjecto(Registo.Posto)

        linha = Registo.LinhaRegistoMaoObra


        'Preço Mao de Obra
        If Not linha Is Nothing Then
            If linha.Artigo <> "" Then

                bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                Dim precunit As Double

                precunit = daValorPrecoArtigo(docV, linhaV.Artigo, linhaV.Unidade)

                If precunit <> -1 Then
                    linhaV.PrecUnit = precunit
                End If

                linhaV.Quantidade = linha.Quantidade
                linhaV.IDObra = idProjecto
                linhaV.CamposUtil(campoMolde) = registo.Molde
                linhaV.CamposUtil(campoRequisicao) = registo.Requisicao
                linha.CDU_IDLinhaInterno = linhaV.IdLinha
            Else
                MsgBox("Não existe artigo configurado para o Posto " + Registo.Posto, MsgBoxStyle.Critical)
            End If
        End If

        linha = Registo.LinhaRegistoMaterialLimpeza
        'Material de Limpeza

        If Not linha Is Nothing Then
            If linha.Quantidade <> 0 Then
                bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
                linhaV = docV.Linhas(docV.Linhas.NumItens)
                linhaV.Quantidade = linha.Quantidade
                linhaV.IDObra = idProjecto
                linhaV.CamposUtil(campoMolde) = registo.Molde
                linhaV.CamposUtil(campoRequisicao) = registo.Requisicao
                linha.CDU_IDLinhaInterno = linhaV.IdLinha
            End If
        End If


        Dim lista As List(Of LinhaRegisto)

        lista = Registo.LinhasRegistosUMR
        Dim i As Integer
        For i = 0 To lista.Count - 1
            linha = lista(i)
            bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
            linhaV = docV.Linhas(docV.Linhas.NumItens)
            linhaV.Quantidade = linha.Quantidade
            linhaV.IDObra = idProjecto
            linhaV.CamposUtil(campoMolde) = registo.Molde
            linhaV.CamposUtil(campoRequisicao) = registo.Requisicao
            linha.CDU_IDLinhaInterno = linhaV.IdLinha
        Next

        'lista = Registo.LinhasRegistosUMC

        'For i = 0 To lista.Count - 1
        '    linha = lista(i)
        '    bso.Comercial.Vendas.AdicionaLinha(docV, linha.Artigo, linha.Quantidade, linha.Armazem, linha.Armazem)
        '    linhaV = docV.Linhas(docV.Linhas.NumItens)
        '    linhaV.Quantidade = linha.Quantidade
        '    linhaV.IDObra = idProjecto
        '    linha.CDU_IDLinhaInterno = linhaV.IdLinha
        'Next

        actualizarDocumentoMaterialCliente(registo, idProjecto, data)

        Try
            If docV.Linhas.NumItens > 0 Then

                If ultimoNumDoc <> -1 Then
                    ultimoNumDocBase = bso.Comercial.Series.DaValorAtributo("V", tipoDocP_, docV.Serie, "Numerador")
                    bso.Comercial.Series.ActualizaNumerador("V", tipoDocP_, docV.Serie, ultimoNumDoc, data)
                End If
                bso.Comercial.Vendas.Actualiza(docV)

                If ultimoNumDoc <> -1 Then
                    bso.Comercial.Series.ActualizaNumerador("V", tipoDocP_, docV.Serie, ultimoNumDocBase, data)
                End If
                Return docV.ID
            Else
                Return ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Private Function daValorPrecoArtigo(ByVal docI As GcpBEDocumentoInterno, ByVal artigo As String, ByVal unidade As String) As Double

        Dim precUnit As Double
        precUnit = -1
        If docI.TipoEntidade = "C" Then
            Dim entidade As GcpBECliente
            Dim preco As GcpBEArtigoMoeda
            preco = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, unidade)
            entidade = bso.Comercial.Clientes.Edita(docI.Entidade)
            Select Case entidade.LinhaPrecos
                Case 0 : precUnit = preco.PVP1
                Case 1 : precUnit = preco.PVP2
                Case 2 : precUnit = preco.PVP3
                Case 3 : precUnit = preco.PVP4
                Case 4 : precUnit = preco.PVP5
                Case 5 : precUnit = preco.PVP6
            End Select
        End If
        Return precUnit

    End Function

    Private Function daValorPrecoArtigo(ByVal docI As GcpBEDocumentoVenda, ByVal artigo As String, ByVal unidade As String) As Double

        Dim precUnit As Double
        precUnit = -1
        If docI.TipoEntidade = "C" Then
            Dim entidade As GcpBECliente
            Dim preco As GcpBEArtigoMoeda
            preco = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, unidade)
            entidade = bso.Comercial.Clientes.Edita(docI.Entidade)
            Select Case entidade.LinhaPrecos
                Case 0 : precUnit = preco.PVP1
                Case 1 : precUnit = preco.PVP2
                Case 2 : precUnit = preco.PVP3
                Case 3 : precUnit = preco.PVP4
                Case 4 : precUnit = preco.PVP5
                Case 5 : precUnit = preco.PVP6
            End Select
        End If
        Return precUnit

    End Function

    Private Function daArtigoProjecto(ByVal projecto As String)
        daArtigoProjecto = ""
        daArtigoProjecto = consultaValor("SELECT CDU_Artigo FROM COP_Obras WHERE Codigo='" + projecto + "'")
    End Function


    Private Function daArtigoMLProjecto(ByVal projecto As String)
        daArtigoMLProjecto = ""
        daArtigoMLProjecto = consultaValor("SELECT CDU_ArtigoML FROM COP_Obras WHERE Codigo='" + projecto + "'")

        If daArtigoMLProjecto = "" Then
            daArtigoMLProjecto = artigoML
        End If
    End Function

    Private Function daIdProjecto(ByVal projecto As String)
        daIdProjecto = ""
        daIdProjecto = consultaValor("SELECT Id FROM COP_Obras WHERE Codigo='" + projecto + "'")
    End Function

    Private Sub removerLinhaDocumentoVenda(ByVal idLinha As String)
        Dim id As String
        Dim docI As GcpBEDocumentoInterno
        Dim linha As GcpBELinhaDocumentoInterno

        id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idLinha + "'")
        If id = "" Then
            Exit Sub
        Else
            docI = bso.Comercial.Internos.EditaID(id)
        End If

        Dim i As Integer
        i = 1
        For Each linha In docI.Linhas
            If linha.ID = idLinha Then
                ' linhaI = linha
                docI.Linhas.Remove(i)
                Exit For
            End If
            i = i + 1
        Next

        Try
            bso.Comercial.Internos.Actualiza(docI)
        Catch ex As Exception

        End Try


    End Sub

    '''' <summary>
    '''' Função que permite criar toda a extrutura da BD exitente
    '''' </summary>
    '''' <remarks></remarks>
    'Private Sub inicializarAplicacao()
    '    Dim lista As StdBELista
    '    lista = consulta("SELECT *  FROM sysobjects WHERE  name='APS_GP_Funcionarios'")
    '    If lista.NoFim Then
    '        executarComando("CREATE VIEW APS_GP_Funcionarios AS SELECT TOP (100) PERCENT dbo.Funcionarios.* FROM Funcionarios")
    '    End If
    'End Sub

    ''' <summary>
    ''' Funcao que permite validar se uma peça existe
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function pecaExiste(ByVal peca As String) As Boolean
        Dim peca1 As String
        peca1 = consultaValor("SELECT CDU_ObraN1 FROm TDU_ObraN1 WHERE CDU_ObraN1='" + peca + "'")
        pecaExiste = peca1 <> ""
    End Function

    ''' <summary>
    ''' Função que permite inserir uma peça no sistema
    ''' </summary>
    ''' <param name="peca"></param>
    ''' <param name="descricao"></param>
    ''' <remarks></remarks>
    Public Sub inserirPeca(ByVal peca As String, ByVal descricao As String)
        executarComando("INSERT INTO TDU_ObraN1(CDu_ObraN1,CDU_Descricao) VALUES('" + peca + "','" + descricao + "')")
    End Sub


    Private Function getDateDocument(ByVal data As Date) As String
        Dim dataI As String
        dataI = ""
        If modoFuncionamento = "D" Then
            dataI = CStr(data.Day) + "-" + CStr(data.Month) + "-" + CStr(data.Year)
        End If

        If modoFuncionamento = "S" Then
            data = data.AddDays(-data.DayOfWeek + 1)
            dataI = CStr(data.Day) + "-" + CStr(data.Month) + "-" + CStr(data.Year)
        End If

        If modoFuncionamento = "M" Then
            dataI = "01" + "-" + CStr(Month(data)) + "-" + CStr(Year(data))
        End If
        Return dataI
    End Function
    'Private Function buscarIdDocumento1(ByVal func As String, ByVal data As Date) As String
    '    Dim dataI As String
    '    dataI = getDateDocument(data)
    '    buscarIdDocumento1 = consultaValor("SELECT Id FROM CabecInternos where Entidade='" + func + "' and TipoDoc='" + tipoDoc_ + "' and Serie='" + serie_ + "' and Data=Convert(Datetime,'" + dataI + "',105)")
    'End Function

    Public Function daListaArtigos(ByVal view As String, ByVal pesquisa As String, ByVal armazem As String, ByVal artigosExluir As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable

        Dim filtro As String
        filtro = ""
        If pesquisa <> "" Then
            If artigosExluir <> "" Then
                filtro = "Artigo not in (" + artigosExluir + ") and "
            End If

            If armazem <> "" Then
                filtro += " Armazem ='" + armazem + "' AND"
            End If

            lista = consulta("SELECT * FROM " + view + " WHERE " + filtro + " (Artigo Like '%" + pesquisa + "%' OR Descricao Like '%" + pesquisa + "%')")
        Else
            If artigosExluir <> "" Then
                filtro = "WHERE Artigo not in (" + artigosExluir + ")"
            End If

            If armazem <> "" Then
                If filtro <> "" Then
                    filtro += "AND Armazem ='" + armazem + "'"
                Else
                    filtro += "WHERE Armazem ='" + armazem + "'"
                End If
            End If
            ' InputBox("", "", "SELECT * FROM " + view + " " + filtro)
            lista = consulta("SELECT * FROM " + view + " " + filtro)
        End If

        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daListaArtigosDocumento(ByVal idDocumento As String, ByVal modulo As String) As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        If modulo = "I" Then
            lista = consulta("SELECT APS_GP_Artigos.*,LinhasInternos.Quantidade FROM APS_GP_Artigos INNER JOIN LinhasInternos ON APS_GP_Artigos.Artigo=LinhasInternos.Artigo WHERE LinhasInternos.IdCabecInternos='" + idDocumento + "'")
        End If

        If modulo = "C" Then
            lista = consulta("SELECT APS_GP_Artigos.*,LinhasCompras.Quantidade FROM APS_GP_Artigos INNER JOIN LinhasCompras ON APS_GP_Artigos.Artigo=LinhasCompras.Artigo WHERE LinhasCompras.IdCabecCompras='" + idDocumento + "'")
        End If
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function actualizarDocumentoInterno(ByVal func As String, ByVal molde As String, ByVal data As Date, ByVal recordsGrid As ReportRecords, Optional ByVal idCabecdoc As String = "") As String

        'ola
        Const COLUMN_ARTIGO As Integer = 0
        Const COLUMN_QUANTIDADE As Integer = 3

        Dim docI As GcpBEDocumentoInterno
        Dim linhaI As GcpBELinhaDocumentoInterno

        Dim id As String

        docI = Nothing

        If idCabecdoc <> "" Then
            ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
            id = idCabecdoc
            docI = bso.Comercial.Internos.EditaID(id)
        End If

        If docI Is Nothing Then
            docI = New GcpBEDocumentoInterno
            docI.Tipodoc = tipoDocS_
            If serieS_ <> "" Then
                docI.Serie = serieS_
            Else
                docI.Serie = bso.Comercial.Series.DaSerieDefeito("N", tipoDocS_, data)
            End If

            docI.Entidade = func
            docI.TipoEntidade = tipoEntidadeS_
            bso.Comercial.Internos.PreencheDadosRelacionados(docI)
        End If
        docI.Utilizador = utilizador
        docI.Data = CDate(data)
        docI.DataVencimento = docI.Data

        docI.Linhas.RemoveTodos()
        Dim artigo As String
        Dim quantidade As Double
        Dim row As ReportRecord
        Dim unidade As String

        For Each row In recordsGrid
            artigo = row(COLUMN_ARTIGO).Value
            quantidade = row(COLUMN_QUANTIDADE).Value
            unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
            bso.Comercial.Internos.AdicionaLinha(docI, artigo, , , , , , quantidade)
            linhaI = docI.Linhas(docI.Linhas.NumItens)
            linhaI.Quantidade = quantidade
            linhaI.PrecoUnitario = bso.Comercial.ArtigosPrecos.Edita(artigo, docI.Moeda, unidade).PVP1
            linhaI.ObraID = bso.Comercial.Projectos.DaValorAtributo(molde, "ID")
        Next

        Try
            If docI.Linhas.NumItens > 0 Or idCabecdoc <> "" Then
                bso.Comercial.Internos.Actualiza(docI)
                Return docI.ID
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function

    Public Function actualizarDocumentoCompra(ByVal tipodoc As String, ByVal entidade As String, ByVal func As String, ByVal molde As String, ByVal data As Date, ByVal recordsGrid As ReportRecords, Optional ByVal idCabecdoc As String = "") As String

        'ola
        Const COLUMN_ARTIGO As Integer = 0
        Const COLUMN_QUANTIDADE As Integer = 3

        Dim docC As GcpBEDocumentoCompra
        Dim linhaC As GcpBELinhaDocumentoCompra

        Dim id As String

        docC = Nothing

        If idCabecdoc <> "" Then
            ' id = consultaValor("SELECT IdCabecInternos FROM LinhasInternos where id='" + idCabecdoc + "'")
            id = idCabecdoc
            docC = bso.Comercial.Compras.EditaID(id)
        End If

        If docC Is Nothing Then
            docC = New GcpBEDocumentoCompra
            docC.Tipodoc = tipodoc
            If serieC_ <> "" Then
                docC.Serie = serieC_
            Else
                docC.Serie = bso.Comercial.Series.DaSerieDefeito("C", tipodoc, data)
            End If

            docC.Entidade = entidade
            docC.TipoEntidade = tipoEntidadeC_
            bso.Comercial.Compras.PreencheDadosRelacionados(docC)
        End If
        docC.Utilizador = func
        docC.DataDoc = CDate(data)
        docC.DataVenc = docC.DataDoc
        docC.DataIntroducao = docC.DataDoc
        docC.NumDocExterno = docC.Tipodoc + " / " + docC.Serie + " / " + CStr(docC.NumDoc)
        docC.Linhas.RemoveTodos()
        Dim artigo As String
        Dim quantidade As Double
        Dim row As ReportRecord
        Dim unidade As String

        For Each row In recordsGrid
            artigo = row(COLUMN_ARTIGO).Value
            quantidade = row(COLUMN_QUANTIDADE).Value
            unidade = bso.Comercial.Artigos.DaValorAtributo(artigo, "UnidadeBase")
            bso.Comercial.Compras.AdicionaLinha(docC, artigo, , , , , , quantidade)
            linhaC = docC.Linhas(docC.Linhas.NumItens)
            linhaC.Quantidade = quantidade
            linhaC.PrecUnit = bso.Comercial.ArtigosPrecos.Edita(artigo, docC.Moeda, unidade).PVP1
            linhaC.IDObra = bso.Comercial.Projectos.DaValorAtributo(molde, "ID")
        Next

        Try
            If docC.Linhas.NumItens > 0 Or idCabecdoc <> "" Then
                bso.Comercial.Compras.Actualiza(docC)
                Return docC.ID
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return ""
        End Try
        Return ""
    End Function


    Public Function artigoValido(ByVal artigo As String, ByVal moduloStocks As Boolean) As Boolean

        Dim tipoArtigo As String
        Dim intTipoDoc As Integer
        Dim movStock As String

        Dim tipoArtPer As GcpBeTipoArtigoPermissao
        movStock = bso.Comercial.Artigos.DaValorAtributo(artigo, "MovStock")

        If movStock = "N" Then Return False

        tipoArtigo = bso.Comercial.Artigos.DaValorAtributo(artigo, "TipoArtigo")
        If moduloStocks Then
            intTipoDoc = bso.Comercial.TabInternos.DaValorAtributo(tipoDocS_, "TipoDocumento")
        Else
            intTipoDoc = bso.Comercial.TabInternos.DaValorAtributo(tipoDocP_, "TipoDocumento")
        End If
        tipoArtPer = bso.Comercial.TiposArtigosPermissoes.Edita(tipoArtigo, "N", intTipoDoc)

        Return tipoArtPer.Permitido


    End Function


    Public Function daDataDocumento(ByVal id As String, ByVal dataDefaut As Date, ByVal modulo As String) As Date
        If modulo = "I" Then
            If bso.Comercial.Internos.ExisteID(id) Then
                Return bso.Comercial.Internos.DaValorAtributoID(id, "Data")
            Else
                Return dataDefaut
            End If
        End If

        If modulo = "C" Then
            If bso.Comercial.Compras.ExisteID(id) Then
                Return bso.Comercial.Compras.DaValorAtributoID(id, "DataDoc")
            Else
                Return dataDefaut
            End If
        End If

    End Function

    Public Sub daEntidadeDocumento(ByVal id As String, ByRef entidade As String, ByRef nome As String, ByVal modulo As String)
        If modulo = "I" Then
            If bso.Comercial.Internos.ExisteID(id) Then
                entidade = bso.Comercial.Internos.DaValorAtributoID(id, "Entidade")
                nome = bso.Comercial.Internos.DaValorAtributoID(id, "Nome")
            End If
        End If
        If modulo = "C" Then
            If bso.Comercial.Compras.ExisteID(id) Then
                entidade = bso.Comercial.Compras.DaValorAtributoID(id, "Entidade")
                nome = bso.Comercial.Compras.DaValorAtributoID(id, "Nome")
            End If
        End If

    End Sub

    ''' <summary>
    ''' Função que permite ir buscar o numero de Kg de uma determinada máquina
    ''' </summary>
    ''' <param name="centroTrabalho"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daNumeroKgCT(ByVal centroTrabalho As String) As String

        Dim kg As String

        kg = bso.Comercial.Projectos.DaValorAtributo(centroTrabalho, "CDU_KgLimpeza")

        If IsNumeric(kg) Then
            Return kg
        Else
            Return "0"
        End If
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function daExtruturaDataTableArtigos() As DataTable
        Dim lista As StdBELista
        Dim dt As DataTable
        lista = consulta("SELECT Artigo,ArmazemSugestao as Armazem,Descricao,0.0 as StkActual,0.0 as Quantidade FROM APS_GP_Artigos WHERE 1=0")
        dt = convertRecordSetToDataTable(lista.DataSet)
        Return dt
    End Function

    Public Function daStockActual(ByVal view As String, ByVal artigo As String) As String
        Return consultaValor("Select StkActual FROM " + view + " WHERE Artigo='" + artigo + "'")
    End Function

    Private Sub removerDocumentoInterno(ByVal idCabecDoc As String)
        If idCabecDoc <> "" Then
            If bso.Comercial.Internos.ExisteID(idCabecDoc) Then
                executarComando("DELETE FROM  LinhasStk WHERE IdCabecOrig='" + idCabecDoc + "'")
                bso.Comercial.Internos.RemoveID(idCabecDoc)
            End If
        End If

    End Sub

    Public Sub removerDocumentoVenda(ByVal idCabecDoc As String)
        If idCabecDoc <> "" Then
            If bso.Comercial.Vendas.ExisteID(idCabecDoc) Then
                Dim doc As GcpBEDocumentoVenda
                doc = bso.Comercial.Vendas.EditaID(idCabecDoc)
                executarComando("DELETE FROM  LinhasStk WHERE IdCabecOrig='" + idCabecDoc + "'")
                bso.Comercial.Vendas.Remove(doc.Filial, doc.Tipodoc, doc.Serie, doc.NumDoc)
            End If
        End If

    End Sub

    Public Sub removerDocumentoStock(ByVal idCabecStock As String)
        If idCabecStock <> "" Then
            If bso.Comercial.Stocks.ExisteID(idCabecStock) Then
                Dim lista As StdBELista
                lista = bso.Consulta("SELECT * FROM CabecStk Where id='" + idCabecStock + "'")
                'executarComando("DELETE FROM  LinhasStk WHERE IdCabecOrig='" + idCabecDoc + "'")
                bso.Comercial.Stocks.Remove(lista.Valor("Filial"), "S", lista.Valor("Tipodoc"), lista.Valor("Serie"), lista.Valor("NumDoc"))
            End If
        End If

    End Sub



    Public Function daObjectoRegisto(ByVal posto As String, ByVal entidade As String, ByVal operacao As String, ByVal molde As String, ByVal requisicao As String, ByVal numeroKg As Double, ByVal dtUMR As DataTable, ByVal dtUMC As DataTable, Optional ByVal idRegisto As String = "") As Registo
        Dim lista As StdBELista
        Dim registo As Registo

        registo = New Registo

        If idRegisto = "" Then
            lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_Posto='" + posto + "' AND CDU_Operacao='" + operacao + "' AND CDU_Entidade='" + entidade + "' and CDU_TipoEntidade='" + tipoEntidadeP_ + "' and CDU_IdCabec IS NULL")
        Else
            lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_ID='" + idRegisto + "'")
        End If

        registo.ArmazemDefault = ArmazemDefault
        registo.ID = idRegisto
        If Not lista.NoFim Then
            registo.ID = lista.Valor("CDU_Id")
            registo.DataInicial = NuloToDate(lista.Valor("CDU_DataInicial"), Now)
            registo.DataFinal = NuloToDate(lista.Valor("CDU_DataFinal"), Now)
            registo.TempoDescontado = NuloToDate(lista.Valor("CDU_TempoDescontado"), CDate(CStr(Date.Today) + " 00:00:00"))
            If CStr(lista.Valor("CDU_Tempo")) <> "" Then
                registo.Tempo = NuloToDouble(lista.Valor("CDU_Tempo"))
            End If
            registo.Funcionario = lista.Valor("CDU_Funcionario")
            registo.Observacoes = lista.Valor("CDU_Observacoes")
            registo.Problema = NuloToString(lista.Valor("CDU_Problema"))
        Else
            registo.Funcionario = funcionario_
        End If

        If registo.DataInicial Is Nothing Then
            registo.DataInicial = DateAdd(DateInterval.Minute, -15, Now)
        End If
        If registo.DataFinal Is Nothing Then
            registo.DataFinal = Now
        End If

        If registo.TempoDescontado Is Nothing Then
            registo.TempoDescontado = CDate(CStr(Date.Today) + " 00:00:00")
        End If


        registo.Entidade = entidade
        registo.TipoEntidade = tipoEntidadeP_
        registo.Posto = posto
        registo.Operacao = operacao
        registo.Molde = molde
        registo.Requisicao = requisicao

        If registo.Tempo Is Nothing Then
            registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(posto), calculaTempoMaoObra(registo.DataInicial, registo.DataFinal))
        Else
            registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(posto), registo.Tempo)
        End If

        registo.adicionaRegistoMaterialLimpeza(daArtigoMLProjecto(posto), numeroKg)

        registo.adicionaLinhasMaterialRTJ(dtUMR)
        registo.adicionaLinhasMaterialCliente(dtUMC)

        Return registo
    End Function

    Public Function calculaTempoMaoObra(ByVal dataInicial As Date, ByVal dataFinal As Date) As Double

        Dim data As Date



        data = CDate(dataFinal)
        Dim minutos As Double

        minutos = DateDiff(DateInterval.Minute, dataInicial, dataFinal)

        'minutos = minutos / 15
        'minutos = minutos + 0.5
        'minutos = Math.Round(minutos, 0)
        'If minutos = 0 Then minutos = 1

        'minutos = minutos * 15

        'calculaTempoMaoObra = (minutos / 60)
        minutos = minutos / 15
        If minutos > Math.Round(minutos) Then
            minutos = Math.Round(minutos) + 1
        Else
            minutos = Math.Round(minutos)
        End If

        minutos = minutos * 15
        calculaTempoMaoObra = (minutos / 60)

    End Function


    Public Function VerficarFuncionarioAdm(ByVal funcionario As String) As Boolean
        Dim lista As StdBELista
        Try
            lista = bso.Consulta("SELECT ISNULL(CDU_Administrador,0) FROM APS_GP_Funcionarios WHERE Codigo='" + funcionario + "'")
            If lista.NoFim Then
                Return False
            Else
                Return CBool(lista.Valor(0))
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Função que permite verificar se o fornecedor existe
    ''' </summary>
    ''' <param name="fornecedor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeFornecedor(ByVal fornecedor As String) As Boolean
        Return bso.Comercial.Fornecedores.Existe(fornecedor)
    End Function

    Public Sub enviarEmailDocumento(ByVal idDocumento As String, ByVal modulo As String)
        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(idDocumento)
            Dim str As String
            Dim mapa As Mapa
            mapa = New Mapa
            Dim gcpSerie As GcpBESerie
            Dim tabCompras As GcpBETabCompra
            Dim tempFolder As String
            tempFolder = System.IO.Path.GetTempPath + "GCPMailTemp\Document.pdf"
            tabCompras = bso.Comercial.TabCompras.Edita(doc.Tipodoc)

            gcpSerie = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
            mapa.iniciarComponente(gcpSerie.Config, codEmpresa, objConfApl.Instancia, utilizador, password, PlataformaPrimavera)
            mapa.Inicializar()
            str = "{CabecCompras.Filial}='" & doc.Filial & "' And {CabecCompras.Serie}='" & doc.Serie & "' And {CabecCompras.TipoDoc}='" & doc.Tipodoc & "' and {CabecCompras.NumDoc}=" & CStr(doc.NumDoc)
            mapa.selectionFormula(str)
            mapa.exportarDiscoPDF(tempFolder)
            PlataformaPrimavera.Mail.Inicializa()

            Dim emailDestino As String
            Dim profile As String
            profile = ""
            emailDestino = ""
            If PlataformaPrimavera.PrefUtilStd.EmailMAPI Then
                profile = PlataformaPrimavera.PrefUtilStd.EmailMAPIProfile
            Else
                ' PlataformaPrimavera.Mail.SMTPServer = PlataformaPrimavera.PrefUtilStd.EmailServSMTP
                '  PlataformaPrimavera.Mail.EnderecoLocal = PlataformaPrimavera.PrefUtilStd.EmailEndereco
            End If

            If tabCompras.EmailFixo Then
                emailDestino = tabCompras.EmailTo
            Else
                If tabCompras.EmailTo <> "" Then
                    emailDestino = consultaValor("Select Email FROM LinhasContactoEntidades WHERE TipoEntidade='" + doc.TipoEntidade + "' and Entidade='" + doc.Entidade + "' and TipoContacto='" + tabCompras.EmailTo + "'")
                End If
            End If

            If emailDestino <> "" Then
                Try

                    PlataformaPrimavera.Mail.EnviaMail(profile, emailDestino, tabCompras.EmailCC, tabCompras.EmailCC, doc.Tipodoc + " " + CStr(doc.NumDoc) + "/" + doc.Serie, tabCompras.EMailTexto, tempFolder, tabCompras.EmailVisualizar, Not PlataformaPrimavera.PrefUtilStd.EmailMAPI)
                    MsgBox("Email enviado com sucesso", MsgBoxStyle.Information)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Else
                MsgBox("O Email da entidade " + doc.Entidade + " não se encontra configurado", MsgBoxStyle.Critical)
            End If


        End If


    End Sub


    Public Function daNumeracaoDocumentoCompra(ByVal documentoCompra As String) As Integer
        daNumeracaoDocumentoCompra = -1
        If tipoDocC_ <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            End If
            Return bso.Comercial.Series.ProximoNumero("C", documentoCompra, serieC_)
        End If
    End Function

    Public Function daValorNumeracaoMinimaDocumentoCompra(ByVal documentoCompra As String) As Integer
        daValorNumeracaoMinimaDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            End If
            Return bso.Comercial.Series.DaValorAtributo("C", documentoCompra, serieC_, "LimiteInferior")
        End If
    End Function

    Public Function daValorNumeracaoMaximaDocumentoCompra(ByVal documentoCompra As String) As Integer
        daValorNumeracaoMaximaDocumentoCompra = -1
        If documentoCompra <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", documentoCompra)
            End If
            Return bso.Comercial.Series.DaValorAtributo("C", documentoCompra, serieC_, "LimiteSuperior")
        End If
    End Function

    ''' <summary>
    ''' Função que permite verificar se um determinado documento de Compra existe
    ''' </summary>
    ''' <param name="numdoc"></param>
    ''' <param name="projecto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function existeDocumentoCompra(ByVal documentoCompra As String, ByVal numdoc As Integer, ByRef projecto As String) As String
        existeDocumentoCompra = ""
        Dim idProj As String
        If tipoDocC_ <> "" Then
            If serieC_ = "" Then
                serieC_ = bso.Comercial.Series.DaSerieDefeito("C", tipoDocC_)
            End If
            If bso.Comercial.Compras.Existe("000", documentoCompra, serieC_, numdoc) Then
                existeDocumentoCompra = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "Id")
                idProj = bso.Comercial.Compras.DaValorAtributo(documentoCompra, numdoc, serieC_, "000", "ObraID")
                projecto = bso.Comercial.Projectos.DaValorAtributoID(idProj, "Codigo")
            End If
        End If
    End Function


    Public Sub imprimirDocumento(ByVal idDocumento As String, ByVal modulo As String)
        If modulo = "C" Then
            Dim doc As GcpBEDocumentoCompra
            doc = bso.Comercial.Compras.EditaID(idDocumento)
            imprimir(doc)
            doc = Nothing
        End If
    End Sub

    Private Sub imprimir(ByVal doc As GcpBEDocumentoCompra)

        Dim se As GcpBESerie
        se = bso.Comercial.Series.Edita("C", doc.Tipodoc, doc.Serie)
        Dim numVia As String
        For i = 1 To se.NumVias
            numVia = ""
            Select Case i
                Case 1 : numVia = "'" + se.DescricaoVia01 + "'"
                Case 2 : numVia = "'" + se.DescricaoVia02 + "'"
                Case 3 : numVia = "'" + se.DescricaoVia03 + "'"
                Case 4 : numVia = "'" + se.DescricaoVia04 + "'"
                Case 5 : numVia = "'" + se.DescricaoVia05 + "'"
                Case 6 : numVia = "'" + se.DescricaoVia06 + "'"
            End Select
            imprimirMapa(se.Config, IIf(se.Previsao, "W", "P"), "{CabecCompras.Filial}='" + doc.Filial + "' AND {CabecCompras.Serie}='" + doc.Serie + "' AND {CabecCompras.TipoDoc}='" + doc.Tipodoc + "' AND {CabecCompras.Numdoc}=" + CStr(doc.NumDoc), numVia)
        Next i
    End Sub
    Private Sub imprimirMapa(ByVal report As String, ByVal destiny As String, ByVal selectionFormula As String, ByVal numVia As String)
        Try


            PlataformaPrimavera.Mapas.Inicializar("GCP")
            PlataformaPrimavera.Mapas.SelectionFormula = selectionFormula
            PlataformaPrimavera.Mapas.AddFormula("NumVia", numVia)
            PlataformaPrimavera.Mapas.ImprimeListagem(report, "Mapa", destiny, 1, "S", selectionFormula, , , , , True)
            PlataformaPrimavera.Mapas.TerminaJanelas()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub gravarDocumento(ByVal idRegisto As String)
        Dim reg As Registo
        reg = daObjectoRegisto(idRegisto)
        Dim idCabecDoc As String
        If getMOLDULOP() = "I" Then
            idCabecDoc = actualizarDocumentoInterno(reg, reg.DataInicial)
        Else
            idCabecDoc = actualizarDocumentoVenda(reg, reg.DataInicial)
        End If

        If idCabecDoc <> "" Then
            executarComando("UPDATE TDU_APSCabecRegistoFolhaPonto set CDU_IdCabec='" + idCabecDoc + "' WHERE CDU_ID='" + idRegisto + "' ")

            Dim linhaR As LinhaRegisto
            For Each linhaR In reg.LinhasRegistos
                If Not linhaR Is Nothing Then
                    executarComando("UPDATE TDU_APSLinhasRegistoFolhaPonto set CDU_IdCabecInternos='" + idCabecDoc + "',CDU_IdLinhaInternos='" + linhaR.CDU_IDLinhaInterno + "' WHERE CDU_ID='" + linhaR.ID + "' ")
                End If
            Next
        End If
    End Sub

    Public Function daObjectoRegisto(ByVal idRegisto As String) As Registo
        Dim lista As StdBELista
        Dim registo As Registo

        registo = New Registo

        lista = consulta("SELECT * FROM TDU_APSCabecRegistoFolhaPonto WHERE CDU_ID='" + idRegisto + "'")

        registo.ArmazemDefault = ArmazemDefault
        registo.ID = idRegisto
        If Not lista.NoFim Then
            registo.ID = lista.Valor("CDU_Id")
            registo.DataInicial = NuloToDate(lista.Valor("CDU_DataInicial"), Now)
            registo.DataFinal = NuloToDate(lista.Valor("CDU_DataFinal"), Now)
            registo.TempoDescontado = NuloToDate(lista.Valor("CDU_TempoDescontado"), CDate(CStr(Date.Today) + " 00:00:00"))
            If CStr(lista.Valor("CDU_Tempo")) <> "" Then
                registo.Tempo = NuloToDouble(lista.Valor("CDU_Tempo"))
            End If
            registo.Funcionario = lista.Valor("CDU_Funcionario")
            registo.Observacoes = lista.Valor("CDU_Observacoes")
            registo.Problema = lista.Valor("CDU_Problema")
        Else
            registo.Funcionario = funcionario_
        End If

        If registo.DataInicial Is Nothing Then
            registo.DataInicial = DateAdd(DateInterval.Minute, -15, Now)
        End If
        If registo.DataFinal Is Nothing Then
            registo.DataFinal = Now
        End If

        If registo.TempoDescontado Is Nothing Then
            registo.TempoDescontado = CDate(CStr(Date.Today) + " 00:00:00")
        End If


        registo.Entidade = lista.Valor("CDU_Entidade")
        registo.TipoEntidade = lista.Valor("CDU_TipoEntidade")
        registo.Posto = lista.Valor("CDU_Posto")
        registo.Operacao = lista.Valor("CDU_Operacao")
        registo.Molde = lista.Valor("CDU_Molde")

        Dim valor As String

        valor = consultaValor("SELECT CDU_Quantidade FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_MAOOBRA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
        If valor <> "" Then
            Dim id As String
            id = consultaValor("SELECT CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_MAOOBRA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
            registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(registo.Posto), valor, id)
        End If


        'If registo.Tempo Is Nothing Then
        '    registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(registo.Posto), calculaTempoMaoObra(registo.DataInicial, registo.DataFinal))
        'Else
        '    registo.adicionaRegistoMaterialMaoObra(daArtigoProjecto(registo.Posto), registo.Tempo)
        'End If

        valor = consultaValor("SELECT CDU_Quantidade FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_LIMPEZA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
        If valor <> "" Then
            Dim id As String
            id = consultaValor("SELECT CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_LIMPEZA + "' and CDU_IdTDUCabec='" + idRegisto + "'")
            registo.adicionaRegistoMaterialLimpeza(daArtigoMLProjecto(registo.Posto), valor, id)
        End If

        registo.adicionaLinhasMaterialRTJ(convertRecordSetToDataTable(consulta("SELECT CDU_Artigo as Artigo,CDU_Armazem as Armazem, CDU_Quantidade as Quantidade,CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_RTJ + "' and CDU_IdTDUCabec='" + idRegisto + "'").DataSet))
        registo.adicionaLinhasMaterialCliente(convertRecordSetToDataTable(consulta("SELECT CDU_Artigo as Artigo,CDU_Armazem as Armazem, CDU_Quantidade as Quantidade,CDU_ID FROM TDU_APSLinhasRegistoFolhaPonto WHERE CDU_TipoArtigo='" + MATERIAL_CLIENTE + "' and CDU_IdTDUCabec='" + idRegisto + "'").DataSet))

        Return registo
    End Function
End Class
