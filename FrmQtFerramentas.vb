﻿Imports XtremeCommandBars
Imports XtremeCommandBars.XTPBarPosition
Imports XtremeCommandBars.XTPControlType
Imports XtremeCommandBars.XTPButtonStyle


Public Class FrmQtFerramentas

    Const MSGERRO1 As String = "Valor de stock Inválido"
    Const MSGPERGUNTA1 As String = "Deseja realmente remover o registo?"
    Dim limpar As Boolean


    Dim valor_ As Double = 0
    Dim artigo As String = ""
    Dim valorC1 As String = ""
    Dim valorC2 As String = ""
    Dim valorC3 As String = ""
    Dim valorC4 As String = ""
    Dim erro As Boolean

    Property Valor() As Double
        Get
            If IsNumeric(txtQuant.Text) Then
                Valor = CDbl(txtQuant.Text)
            Else
                Valor = 1
            End If
        End Get
        Set(ByVal value As Double)
            valor_ = value
        End Set
    End Property


    ReadOnly Property getValorCampo(campo As Integer) As String
        Get
            Select Case campo
                Case 1 : Return cmbCompartimento.Text
                Case 2 : Return cmbEquipamento.Text
                Case 3 : Return cmbRazaoTroca.Text
                Case 4 : Return cmbAccaoRealizar.Text
            End Select
            Return ""
        End Get
    End Property


    Public Sub setValorCampo(campo As Integer, valor As String)
        Select Case campo
            Case 1 : valorC1 = valor
            Case 2 : valorC2 = valor
            Case 3 : valorC3 = valor
            Case 4 : valorC4 = valor
        End Select
    End Sub

    Public Sub setValorArtigo(valor As String)
        artigo = valor
    End Sub

    Dim botaoSeleccionado_ As Integer
    ReadOnly Property BotaoSeleccionado() As Integer
        Get
            BotaoSeleccionado = botaoSeleccionado_
        End Get

    End Property

    Private Sub FrmTerminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        botaoSeleccionado_ = 3
        limpar = True
        iniciarForm()
        txtQuant.Text = valor_
        erro = False
        Dim comp As String
        comp = buscarCompartimentoArtigo(artigo)

        If comp <> "" Then
            cmbCompartimento.Text = comp
        End If

        If valorC1 <> "" Then
            cmbCompartimento.Text = valorC1
        End If
        If valorC2 <> "" Then
            cmbEquipamento.Text = valorC2
        End If
        If valorC3 <> "" Then
            cmbRazaoTroca.Text = valorC3
        End If
        If valorC4 <> "" Then
            cmbAccaoRealizar.Text = valorC4
        End If

    End Sub

    Private Sub iniciarToolBox()
        CommandBarsFrame1.DeleteAll()
        Dim Toolbar As CommandBar
        Dim Control As CommandBarControl
        CommandBarsFrame1.Icons = ImageManager.Icons
        Toolbar = CommandBarsFrame1.Add("Standard", xtpBarTop)
        CommandBarsFrame1.Left = 0
        CommandBarsFrame1.Top = 0
        Toolbar.ContextMenuPresent = False
        Toolbar.Customizable = False
        Toolbar.CustomizeDialogPresent = False
        Toolbar.Closeable = False
        Toolbar.ShowExpandButton = False
        With Toolbar.Controls
            Control = .Add(xtpControlSplitButtonPopup, 1, " Confirmar", -1, False)
            Control.Style = xtpButtonIconAndCaptionBelow
            Control.DescriptionText = "Confirmar"

            Control = .Add(xtpControlButton, 2, " Cancelar", -1, False)
            Control.BeginGroup = True
            Control.DescriptionText = "Cancelar"
            Control.Style = xtpButtonIconAndCaptionBelow
        End With
    End Sub
    Private Sub iniciarForm()
        iniciarToolBox()
        iniciarComboboxs()
        txtQuant.Text = 1
        txtQuant.Focus()
    End Sub


    Private Sub colocarQuantidadeStk(ByVal valor As String)
        If limpar Then
            txtQuant.Text = ""
            limpar = False
        End If
        If Valor = "" Then
            txtQuant.Text = ""
        Else
            If IsNumeric(txtQuant.Text + Valor) Then txtQuant.Text = txtQuant.Text + Valor
        End If
    End Sub



    Private Sub habilitarBotoes(ByVal b As Boolean)
        btn1.Enabled = b
        btn2.Enabled = b
        btn3.Enabled = b
        btn4.Enabled = b
        If b = True Then btn5.Enabled = b
        btn6.Enabled = b
        btn7.Enabled = b
        btn8.Enabled = b
        btn9.Enabled = b
        btn0.Enabled = b
        btnP.Enabled = b
    End Sub

    Private Sub btnP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnP.Click
        '  habilitarBotoes(False)
        colocarQuantidadeStk(",")
    End Sub

    Private Sub btnC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnC.Click
        colocarQuantidadeStk("")
        ' habilitarBotoes(True)
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        colocarQuantidadeStk(btn1.Tag)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        colocarQuantidadeStk(btn2.Tag)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        colocarQuantidadeStk(btn3.Tag)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        colocarQuantidadeStk(btn4.Tag)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        colocarQuantidadeStk(btn5.Tag)
        'If btn0.Enabled = False Then
        '    btn5.Enabled = False
        'End If
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        colocarQuantidadeStk(btn6.Tag)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        colocarQuantidadeStk(btn7.Tag)
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        colocarQuantidadeStk(btn8.Tag)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        colocarQuantidadeStk(btn9.Tag)
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        colocarQuantidadeStk(btn0.Tag)
    End Sub


    Private Sub CommandBarsFrame1_Execute(ByVal sender As System.Object, ByVal e As AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent) Handles CommandBarsFrame1.Execute

        Me.Validate()

        If erro Then Exit Sub
        botaoSeleccionado_ = e.control.Index

        ' terminar o registo
        If e.control.Index = 1 Then
            If IsNumeric(txtQuant.Text) Then
                Me.Hide()
            Else
                MsgBox(MSGERRO1, MsgBoxStyle.Critical)
            End If

        End If


        'fechar o serviço
        If e.control.Index = 2 Then
            Me.Close()
        End If
    End Sub

    Private Sub iniciarComboboxs()
        Dim m As Motor
        m = Motor.GetInstance
        Dim dt As DataTable

        dt = m.consultaDataTable("SELECT CDU_Codigo as Codigo,CDU_Descricao as Descricao FROM TDU_FERNumCompartimento")

        'If Not dt Is Nothing Then
        '    cmbCompartimento.DataSource = dt
        '    cmbCompartimento.DisplayMember = "Descricao"
        'End If


        If Not dt Is Nothing Then
            cmbCompartimento.DropDownStyle = ComboBoxStyle.DropDown
            cmbCompartimento.DataSource = dt
            cmbCompartimento.DisplayMember = "Descricao"
            cmbCompartimento.AutoCompleteSource = AutoCompleteSource.ListItems
            cmbCompartimento.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cmbCompartimento.DisplayMember = "Descricao"
            cmbCompartimento.ValueMember = "Codigo"
        End If

       

        dt = m.consultaDataTable("SELECT CDU_Codigo as Codigo,CDU_Descricao as Descricao FROM TDU_FERNumEquipamento")

        'If Not dt Is Nothing Then
        '    cmbEquipamento.DataSource = dt
        '    cmbEquipamento.DisplayMember = "Descricao"
        'End If

        If Not dt Is Nothing Then
            cmbEquipamento.DropDownStyle = ComboBoxStyle.DropDown
            cmbEquipamento.DataSource = dt
            cmbEquipamento.DisplayMember = "Descricao"
            cmbEquipamento.AutoCompleteSource = AutoCompleteSource.ListItems
            cmbEquipamento.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cmbEquipamento.DisplayMember = "Descricao"
            cmbEquipamento.ValueMember = "Codigo"
        End If

        dt = m.consultaDataTable("SELECT CDU_Codigo as Codigo,CDU_Descricao as Descricao FROM TDU_FERAccaoRealizar")

        'If Not dt Is Nothing Then
        '    cmbAccaoRealizar.DataSource = dt
        '    cmbAccaoRealizar.DisplayMember = "Descricao"
        'End If
        If Not dt Is Nothing Then
            cmbAccaoRealizar.DropDownStyle = ComboBoxStyle.DropDown
            cmbAccaoRealizar.DataSource = dt
            cmbAccaoRealizar.DisplayMember = "Descricao"
            cmbAccaoRealizar.AutoCompleteSource = AutoCompleteSource.ListItems
            cmbAccaoRealizar.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cmbAccaoRealizar.DisplayMember = "Descricao"
            cmbAccaoRealizar.ValueMember = "Codigo"
        End If


        dt = m.consultaDataTable("SELECT CDU_Codigo as Codigo,CDU_Descricao as Descricao FROM TDU_FERRazaoTroca")

        'If Not dt Is Nothing Then
        '    cmbRazaoTroca.DataSource = dt
        '    cmbRazaoTroca.DisplayMember = "Descricao"
        'End If

        If Not dt Is Nothing Then
            cmbRazaoTroca.DropDownStyle = ComboBoxStyle.DropDown
            cmbRazaoTroca.DataSource = dt
            cmbRazaoTroca.DisplayMember = "Descricao"
            cmbRazaoTroca.AutoCompleteSource = AutoCompleteSource.ListItems
            cmbRazaoTroca.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cmbRazaoTroca.DisplayMember = "Descricao"
            cmbRazaoTroca.ValueMember = "Codigo"
        End If


    End Sub

    Private Function validarCombobox(combo As ComboBox) As Boolean
        erro = False
        If combo.SelectedValue Is Nothing Then
            MsgBox("Valor introduzido incorrecto", MsgBoxStyle.Critical)
            erro = True
            Return True
        Else
            Return False
        End If
    End Function

   
    Private Sub cmbCompartimento_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cmbCompartimento.Validating
        e.Cancel = validarCombobox(sender)
    End Sub

    Private Sub cmbEquipamento_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cmbEquipamento.Validating
        e.Cancel = validarCombobox(sender)
    End Sub

    Private Sub cmbAccaoRealizar_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cmbAccaoRealizar.Validating
        e.Cancel = validarCombobox(sender)
    End Sub

    Private Sub cmbRazaoTroca_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cmbRazaoTroca.Validating
        e.Cancel = validarCombobox(sender)
    End Sub

    Public Function buscarCompartimentoArtigo(art As String)
        Dim index As Integer
        art = art.ToUpper()
        index = art.LastIndexOf("CP")
        If index <> -1 Then
            Return art.Substring(index, Len(art) - index)
        End If
        Return ""

    End Function
End Class