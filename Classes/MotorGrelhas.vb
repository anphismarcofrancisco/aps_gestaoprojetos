﻿Imports AxXtremeReportControl
Imports Interop.StdBE900
Imports XtremeReportControl
Public Class MotorGrelhas

    Dim dt As DataTable
    Dim admin As Boolean
    Dim modulo As String
    '  Dim projeto As String
    Dim mostraForn As Double
    Dim ht As Hashtable
    Dim htCamposSistema As Hashtable
    Dim multiplicador As Integer = 1
    Const COR_ALTERACAO As String = "add8e6"
    Const COR_LINHAALTERACAO As String = "C390D4"

    Dim tipoDoc As String
    Const COR_BRANCO As UInteger = 4294967295


    Const ESTADO_NORMAL = ""
    Const ESTADO_INTERNO = "I"
    Const ESTADO_PROBLEMAS = "P"
    Const ESTADO_COT = "C"
    Const ESTADO_ECF = "E"
    Const ESTADO_VER = "V"
    Const ESTADO_CONF = "F"
    Const ESTADO_A = "A"
    Const ESTADO_CLIENTE = "L"
    Const ESTADO_SUB = "S"
    Const ESTADO_BLOQ = "B"

    Public Sub New(modulo As String, dtColunas As DataTable, userAdmin As Boolean, mostraForn As Boolean, documento As String)
        dt = dtColunas
        admin = userAdmin
        Me.modulo = modulo
        Me.mostraForn = mostraForn
        ht = New Hashtable
        htCamposSistema = New Hashtable

        Dim motor As Motor
        motor = Motor.GetInstance()
        Dim tipoDocumento As String
        If modulo = "C" Then
            tipoDocumento = motor.consultaValor("SELECT  TipoDocumento FROM DocumentosCompra WHERE Documento='" + documento + "'")
            If tipoDocumento = "3" Then
                multiplicador = -1
            End If
        End If

        tipoDoc = documento

    End Sub

    Public ReadOnly Property getMultiplicador() As Integer
        Get
            Return multiplicador
        End Get
    End Property



    Public Sub inserirColunas(ByVal grid As AxReportControl, colunasistema As Boolean)
        Dim m As MotorAPL
        Dim motor As Motor
        motor = Motor.GetInstance()
        m = New MotorAPL(motor)
        Dim Column As ReportColumn
        Dim row As DataRow
        Dim tabela As String
        Dim campo As String

        tabela = daTabelaModulo()
        If dt Is Nothing Then Exit Sub

        If Not dt.Columns.Contains("TIPOCAMPO") Then
            dt.Columns.Add("TIPOCAMPO")
        End If

        Dim existeCampo As Boolean

        For Each row In dt.Rows
            campo = UCase(m.NuloToString(row("CDU_Campo")))

            existeCampo = m.existeCampo(tabela, campo)
            If existeCampo Then
                If Not htCamposSistema.ContainsKey(campo) Then
                    htCamposSistema.Add(campo, row)
                End If

            End If

            If (Not ColunasSistema(campo)) Or colunasistema Then
                If Not ht.ContainsKey(campo) Then
                    ht.Add(campo, row)
                    If m.existeCampo(tabela, campo) Or ColunasSistema(campo) Then
                        Column = inserirColuna(grid, m.NuloToDouble(row("CDU_Ordem")), m.NuloToString(row("CDU_Descricao")), m.NuloToDouble(row("CDU_Tamanho")), False, True)

                        '''If Not ColunasSistema(campo) Then
                        If campo <> "CHECKBOX" Then
                            Column.Tag = campo
                        End If

                        'End If

                        Column.EditOptions.AllowEdit = True
                        Column.Visible = (m.NuloToBoolean(row("CDU_Administrador")) And admin) Or m.NuloToBoolean(row("CDU_Administrador")) = False
                        If m.NuloToString(row("CDU_SUBQUERY")) <> "" Then
                            Column.EditOptions.AddExpandButton()
                            Column.Tag = campo
                        End If
                        row("TIPOCAMPO") = m.daTipoCampo(tabela, campo)
                        Column.Alignment = buscarTipoAlinhamento(row("TIPOCAMPO"))


                        If Not mostraForn And campo = "FORNECEDOR" Then
                            Column.Visible = False
                        End If

                        If campo = "CHECKBOX" Then
                            Column.Icon = 1
                        End If
                        If campo = "NUMEROALTERACOES" Then
                            Column.Alignment = XTPColumnAlignment.xtpAlignmentCenter
                        End If

                        If campo = "QUANTIDADE" Or campo = "QUANTENT" Or campo = "QUANTFALTA" Then
                            Column.Alignment = XTPColumnAlignment.xtpAlignmentCenter
                            Column.Tag = campo
                            row("TIPOCAMPO") = "float"
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    ''' <summary>
    ''' Permite verificar se um determinado campo, é um campo de sistema
    ''' </summary>
    ''' <param name="campo"></param>
    ''' <returns></returns>
    ''' 

    Public Function ColunasSistema(campo) As Boolean
        If campo = "CHECKBOX" Or campo = "NUMEROALTERACOES" Or InStr(campo, "FORNECEDOR") <> 0 Or campo = "QUANTENT" Or campo = "QUANTFALTA" Or campo = "DATAENTREGA" Or campo = "CDU_DATARECEPCAO" Then
            Return True
        End If
    End Function
    Private Function inserirColuna(ByVal grid As AxXtremeReportControl.AxReportControl, ByVal index As Long, ByVal texto As String, ByVal tamanho As Long, ByVal editavel As Boolean, ByVal Resizable As Boolean) As ReportColumn
        Dim Column As ReportColumn
        Column = grid.Columns.Add(index, texto, tamanho, Resizable)
        Column.EditOptions.AllowEdit = editavel
        Column.EditOptions.SelectTextOnEdit = True
        Column.HeaderAlignment = XTPColumnAlignment.xtpAlignmentCenter
        inserirColuna = Column
    End Function


    Private Function buscarTipoAlinhamento(tipoCampo As String) As XTPColumnAlignment
        buscarTipoAlinhamento = XTPColumnAlignment.xtpAlignmentLeft
        Select Case tipoCampo
            Case "bit" : buscarTipoAlinhamento = XTPColumnAlignment.xtpAlignmentRight
            Case "float" : buscarTipoAlinhamento = XTPColumnAlignment.xtpAlignmentRight
            Case "real" : buscarTipoAlinhamento = XTPColumnAlignment.xtpAlignmentRight
            Case "datetime" : buscarTipoAlinhamento = XTPColumnAlignment.xtpAlignmentCenter
            Case "smalldatetime" : buscarTipoAlinhamento = XTPColumnAlignment.xtpAlignmentCenter
        End Select
    End Function

    Public Function formatarIdsLinha(id As String)
        id = id.Replace("{", "")
        id = id.Replace("}", "")
        id = UCase(id)
        Return id
    End Function

    Public Sub inserirLinha(grid As AxReportControl, linha As ReportRecord, hashtable As Hashtable, projeto As String)
        Dim record As ReportRecord

        Dim item As ReportRecordItem
        Dim campo As String
        Dim existeCampo As Boolean
        Dim dtrow As DataRow
        Dim m As MotorAPL
        Dim motor As Motor
        Dim id As String

        motor = Motor.GetInstance()
        m = New MotorAPL(motor)

        record = grid.Records.Add()

        dtrow = linha.Tag

        record.Tag = dtrow

        id = formatarIdsLinha(dtrow("ID").ToString())

        hashtable.Add(id, record.Index)

        For Each row In dt.Rows
            campo = UCase(m.NuloToString(row("CDU_Campo")))

            existeCampo = htCamposSistema.ContainsKey(campo)

            If existeCampo Or ColunasSistema(campo) Then

                If existeCampo Then

                    If campo = "DATAENTREGA" Or campo = "CDU_DATARECEPCAO" Then
                        dtrow(campo) = DBNull.Value
                    End If
                    item = record.AddItem(formatarCampo(row("TIPOCAMPO"), campo, dtrow(campo), projeto))
                    item.Editable = m.NuloToBoolean(row("CDU_EDITAVEL"))

                Else

                    If campo = "NUMEROALTERACOES" Then
                        item = record.AddItem(dtrow("NumAlt").ToString())
                        item.Editable = False
                    End If

                    If campo = "FORNECEDOR" Then
                        item = record.AddItem(buscarEntidade(dtrow("Id").ToString(), m.NuloToString(dtrow("CDU_ObraN8").ToString()), projeto))
                        item.Editable = False
                    End If

                    If campo = "CHECKBOX" Then
                        item = record.AddItem("")
                        item.HasCheckbox = True
                        item.Checked = True
                        item.Tag = ""
                    End If

                End If

            End If
        Next

    End Sub

    Public Sub inserirLinha(grid As AxReportControl, dtRow As DataRow, estado As String, cor As String, indexLinha As Integer, hashTable As Hashtable, nova As Boolean, validaCheked As Boolean, datasVazias As Boolean, doc As Objeto)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim editavel As Boolean = True
        Dim m As MotorAPL
        Dim motor As Motor
        Dim tabela As String
        Dim checked As Boolean
        Dim dtrowBase As DataRow = Nothing

        motor = Motor.GetInstance()
        m = New MotorAPL(motor)
        tabela = daTabelaModulo()
        Item = Nothing
        'record = New ReportRecord()

        If indexLinha = -1 Then
            record = grid.Records.Add()
        Else
            dtrowBase = grid.Records(indexLinha).Tag
            record = grid.Records.Insert(indexLinha + 1)
        End If

        Dim id As String

        id = formatarIdsLinha(dtRow("ID").ToString())

        hashTable.Add(id, record.Index)

        record.Tag = dtRow
        Dim existeCampo As Boolean

        Dim campo As String
        For Each row In dt.Rows
            campo = UCase(m.NuloToString(row("CDU_Campo")))

            existeCampo = htCamposSistema.ContainsKey(campo)

            If existeCampo Or ColunasSistema(campo) Then
                If existeCampo Then
                    'tipodoc checked
                    If row("TIPOCAMPO") = "bit" Then
                        Item = record.AddItem("")
                        Item.HasCheckbox = True
                        Item.Checked = m.NuloToBoolean(dtRow(campo))
                    Else
                        Item = record.AddItem(formatarCampo(row("TIPOCAMPO"), campo, dtRow(campo), doc.Projeto, True, datasVazias, dtRow("ID").ToString()))
                    End If

                    If indexLinha <> -1 Then
                        If dtrowBase Is Nothing Or (nova And ColunasSistema(campo)) Then
                            Item.BackColor = cor
                        Else
                            Item.BackColor = buscarCor(grid, campo, record.Index, m.NuloToString(dtRow(campo)), m.NuloToString(dtrowBase(campo)), cor)
                        End If
                        Item.Tooltip = m.NuloToString(dtrowBase(campo))
                    Else
                        Item.BackColor = cor
                    End If
                    '  dtRow(campo) = Item.Value
                    Item.Tag = dtRow(campo)
                    Item.Editable = m.NuloToBoolean(row("CDU_EDITAVEL"))

                Else

                    If campo = "NUMEROALTERACOES" Then
                        '    Item = record.AddItem(buscarNumeroAlteracoes(dtRow("Id").ToString()))
                        Item = record.AddItem(dtRow("NumAlt").ToString())
                        Item.BackColor = cor
                        Item.Editable = False
                    End If

                    If campo = "FORNECEDOR" Then
                        'Item = record.AddItem("")                        '-
                        '   Item = record.AddItem(buscarEntidade(dtRow("Id").ToString()))
                        Item = record.AddItem(buscarEntidade(dtRow("Id").ToString(), m.NuloToString(dtRow("CDU_ObraN8").ToString()), doc.Projeto))
                        Item.BackColor = cor
                        Item.Editable = m.NuloToBoolean(row("CDU_EDITAVEL"))
                        '  Item.Tag = campo
                    End If

                    If campo = "CHECKBOX" Then
                        checked = Not IsDBNull(dtRow("CDU_DataRecepcao"))
                        Item = record.AddItem("")
                        Item.HasCheckbox = True
                        Item.Checked = checked And validaCheked
                        Item.BackColor = cor
                        Item.Tag = estado
                    End If

                    If campo = "QUANTENT" Then
                        'Item = record.AddItem("")                        '-
                        'Item = record.AddItem(buscarEntidade(dtRow("Id").ToString()))
                        Item = record.AddItem(formatarCampo("float", campo, buscarQuantidadeEntregue(doc.BaseDados, dtRow("Id").ToString()), doc.Projeto))
                        Item.BackColor = cor
                        Item.Editable = False
                        'Item.Tag = campo
                    End If

                    If campo = "QUANTFALTA" Then
                        'Item = record.AddItem("")                        '-
                        'Item = record.AddItem(buscarEntidade(dtRow("Id").ToString()))
                        Item = record.AddItem(formatarCampo("float", campo, buscarQuantidadeFalta(doc.BaseDados, dtRow("Id").ToString(), m.NuloToDouble(dtRow("Quantidade"))), doc.Projeto))
                        Item.BackColor = cor
                        Item.Editable = m.NuloToBoolean(row("CDU_EDITAVEL"))
                    End If
                End If

                Item.Editable = itemEditavel(grid.Name, Item, estado)


            End If
        Next

    End Sub


    Private Function itemEditavel(gridName As String, item As ReportRecordItem, estado As String) As Boolean

        Return True

        If gridName <> "gridArtigos" Then
            Return True
        End If


        'Return True

        Const ESTADO_NORMAL = ""
        Const ESTADO_INTERNO = "I"
        Const ESTADO_PROBLEMAS = "P"
        Const ESTADO_COT = "C"
        Const ESTADO_ECF = "E"
        Const ESTADO_VER = "V"
        Const ESTADO_CONF = "F"
        Const ESTADO_A = "A"
        Const ESTADO_CLIENTE = "L"
        Const ESTADO_SUB = "S"
        Const ESTADO_BLOQ = "B"




        Select Case estado
            Case ESTADO_NORMAL : Return True
            Case ESTADO_INTERNO : Return False
            Case ESTADO_PROBLEMAS : Return False
            Case ESTADO_COT : Return True
            Case ESTADO_ECF : Return False
            Case ESTADO_VER : Return False
            Case ESTADO_CONF : Return False
            Case ESTADO_A : Return False
            Case ESTADO_SUB : Return True
            Case ESTADO_BLOQ : Return False
            Case ESTADO_CLIENTE : Return False
        End Select

        Return item.Editable

    End Function
    Public Sub actualizarLinhaRegisto(grid As AxReportControl, indexLinha As Integer, dtRow As DataRow, cor As String, doc As Objeto)
        Dim m As MotorAPL
        Dim motor As Motor
        motor = Motor.GetInstance()
        m = New MotorAPL(motor)
        Dim record As ReportRecord
        '  Dim cor As UInteger
        Dim campo As String
        Dim indexColuna As Integer
        Dim existeAlt As Boolean
        Dim houveAlt As Boolean

        houveAlt = False

        For Each row In dt.Rows

            campo = UCase(m.NuloToString(row("CDU_Campo")))
            record = grid.Records(indexLinha)
            If Not ColunasSistema(campo) Then
                indexColuna = buscarIndexCampo(campo)
                existeAlt = existeAlteracao(record(indexColuna).Value, formatarCampo(row("TIPOCAMPO"), campo, dtRow(campo), doc.Projeto))

                If campo <> "PRECUNIT" And campo <> "ARTIGO" Then

                    If existeAlt Then
                        houveAlt = True
                        record(indexColuna).Tooltip = record(indexColuna).Value
                    End If

                    '   If dtRow("CDu_ObraN5") <> "M" Then
                    record(indexColuna).BackColor = IIf(existeAlt, System.Convert.ToUInt32(HexToDecimal(COR_ALTERACAO)), cor)
                    ' Else
                    ' record(indexColuna).BackColor = IIf(existeAlt, System.Convert.ToUInt32(HexToDecimal(COR_ALTERACAO)), cor)
                    'End If

                    record(indexColuna).Value = formatarCampo(row("TIPOCAMPO"), campo, dtRow(campo), doc.Projeto)
                    record(indexColuna).Caption = record(indexColuna).Value
                End If
            End If
        Next

        If houveAlt Then
            dtRow = grid.Records(indexLinha).Tag
            dtRow("CDu_ObraN5") = "M"
            cor = System.Convert.ToUInt32(HexToDecimal(COR_LINHAALTERACAO))
            For i = 0 To grid.Columns.Count - 1
                If grid.Records(indexLinha).Item(i).BackColor <> System.Convert.ToUInt32(HexToDecimal(COR_ALTERACAO)) Then
                    grid.Records(indexLinha).Item(i).BackColor = cor
                End If
            Next
        End If
    End Sub

    Private Function existeAlteracao(valor1 As String, valor2 As String) As Boolean
        valor2 = Replace(valor2, "–", "-")

        Return valor1 <> valor2
    End Function

    Public Sub inserirLinhaAlt(grid As AxReportControl, dtRow As DataRow, dtrowBase As DataRow, projeto As String)
        Dim record As ReportRecord
        Dim Item As ReportRecordItem
        Dim editavel As Boolean = True
        Dim m As MotorAPL
        Dim motor As Motor
        Dim tabela As String
        Dim checked As Boolean


        motor = Motor.GetInstance()
        m = New MotorAPL(motor)
        tabela = daTabelaModulo()

        record = grid.Records.Add()
        record.Tag = dtRow
        Dim cor As UInteger
        cor = "4294967295"
        Dim campo As String
        For Each row In dt.Rows
            Try


                campo = UCase(m.NuloToString(row("CDU_Campo")))

                If m.existeCampo(tabela, campo) Or ColunasSistema(campo) Then
                    If m.existeCampo(tabela, campo) Then
                        Item = record.AddItem(formatarCampo(m.NuloToString(row("TIPOCAMPO")), campo, dtRow(campo), projeto))
                        Item.BackColor = buscarCor(grid, campo, record.Index, m.NuloToString(dtRow(campo)), m.NuloToString(dtrowBase(campo)), cor)

                        Item.Tag = dtRow(campo)
                    Else
                        If campo = "NUMEROALTERACOES" Then
                            Item = record.AddItem(buscarNumeroAlteracoes(dtRow("Id").ToString()))
                            Item.BackColor = cor
                            Item.Editable = False
                        End If
                        If campo = "FORNECEDOR" Then
                            'Item = record.AddItem("")
                            Item = record.AddItem(buscarEntidade(dtRow("Id").ToString(), m.NuloToString(dtRow("CDU_ObraN8").ToString()), projeto))
                            Item.BackColor = cor
                            Item.Editable = False
                        End If

                        If campo = "CHECKBOX" Then
                            checked = Not IsDBNull(dtRow("CDU_DataRecepcao"))
                            Item = record.AddItem("")
                            Item.HasCheckbox = True
                            Item.Checked = checked
                            Item.BackColor = cor
                            '  Item.Tag = campo
                        End If

                    End If
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next

    End Sub

    Private Function daTabelaModulo() As String
        Dim tabela As String
        tabela = ""
        Select Case modulo
            Case "V" : tabela = "LinhasDoc"
            Case "C" : tabela = "LinhasCompras"
        End Select
        Return tabela
    End Function

    Public Function buscarSubQuery(campo As String) As String
        Dim m As Motor
        m = Motor.GetInstance
        For Each row In dt.Rows
            If UCase(m.NuloToString(row("CDU_CAMPO"))) = campo Then
                Return m.NuloToString(row("CDU_SUBQUERY"))
            End If
        Next
        Return ""
    End Function


    Public Function buscarCampoLinhaAssociada(projeto As String, idLinha As String, campo As String, valorCampo As String) As String

        If valorCampo <> "" Then
            Return valorCampo
        End If

        If Trim(idLinha) <> "" Then
            Dim m As MotorLM
            m = MotorLM.GetInstance()
            Dim query As String
            Dim lista As StdBELista

            query = "SELECT  DISTINCT CDU_BDDestino as BD FROM TDU_GP_LigacaoProjetos WHERE CDU_ProjetoOrigem='" + projeto + "' UNION SELECT '" + m.Empresa + "'"


            lista = m.consulta(query)
            While Not lista.NoFim
                valorCampo = m.buscarCampoLinhaAssociada1("PRI" + lista.Valor("BD"), idLinha, campo)
                If valorCampo <> "" Then
                    Exit While
                End If
                lista.Seguinte()
            End While



            If valorCampo = "01-01-1900 00:00:00" Then
                valorCampo = ""
            End If

            If valorCampo <> "" Then
                m.executarComando("UPDATE LinhasCompras Set " + campo + "=convert(datetime,'" + valorCampo + "',105) WHERE Id='" + idLinha + "'")
            Else
                m.executarComando("UPDATE LinhasCompras set " + campo + "=convert(datetime,'01-01-1900',105) WHERE Id='" + idLinha + "'")
            End If
            Return valorCampo
        End If
        Return ""

    End Function

    Public Function buscarEntidade(idLinha As String, entidade As String, projeto As String) As String

        If entidade <> "" Then Return entidade
        If Trim(idLinha) <> "" Then
            Dim m As MotorLM
            '  Dim entidade As String
            m = MotorLM.GetInstance()
            entidade = m.buscarEntidadesLinha(idLinha, projeto)
            If entidade <> "" Then
                m.executarComando("UPDATE LinhasCompras Set CDU_ObraN8='" + Left(entidade, 255) + "' WHERE Id='" + idLinha + "'")
            Else
                m.executarComando("UPDATE LinhasCompras set CDU_ObraN8=NULL WHERE Id='" + idLinha + "'")
            End If

            Return entidade
        End If
        Return ""

    End Function

    Public Function buscarNumeroAlteracoes(idLinha As String) As String
        If Trim(idLinha) <> "" Then
            Dim m As MotorLM
            m = MotorLM.GetInstance()
            Return m.consultaValor("SELECT Count(*) FROM LinhasCompras INNER JOIN CABECCOMPRAS ON LinhasCompras.IdCabecCompras=CABECCOMPRAS.ID  WHERE TipoDoc='" + m.InformModLstMaterial.TIPODOCALT + "' and  IdLinhaOrigemCopia='" + idLinha + "'")
        Else
            Return "0"
        End If
    End Function
    Private Function formatarCampo(tipo As String, campo As String, valor As Object, projeto As String, Optional aplicarMultiplicador As Boolean = True, Optional datasVazias As Boolean = False, Optional idLinha As String = "") As Object
        'Dim m As Motor
        'm = Motor.GetInstance()
        Dim m As MotorLM
        m = MotorLM.GetInstance()
        Try
            Select Case tipo
                Case "nvarchar"

                    ' valor = m.NuloToString(valor)
                    'Problemas no clicas
                    valor = m.NuloToString(valor).Replace(Chr(148), Chr(34))

                    Return valor
                Case "bit" : Return m.NuloToBoolean(valor)
                Case "float", "real"
                    Return FormatNumber(IIf(m.NuloToDouble(valor) > 0, m.NuloToDouble(valor), m.NuloToDouble(valor) * -1), m.NumeroCasasDecimaisPrecUnit)
                Case "datetime"
                    Return formatarCampoDatetime(idLinha, campo, valor, m, datasVazias, projeto)
                Case "smalldatetime"
                    Return formatarCampoDatetime(idLinha, campo, valor, m, datasVazias, projeto) 'FormatDateTime(m.NuloToDate(valor, Now), DateFormat.ShortDate)
                Case Else : Return m.NuloToString(valor)
            End Select
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function formatarCampoDatetime(idLinha As String, campo As String, valor As Object, m As MotorLM, datasVazias As Boolean, projeto As String) As Object

        If datasVazias Then
            If IsDBNull(valor) Then
                If campo = "DATAENTREGA" Or campo = "CDU_DATARECEPCAO" Then
                    valor = buscarCampoLinhaAssociada(projeto, idLinha, campo, NuloToString(valor))
                    If valor <> "" Then
                        Return FormatDateTime(m.NuloToDate(valor, Now), DateFormat.ShortDate)
                    Else
                        Return ""
                    End If
                Else
                    Return True
                End If
            Else
                If IsDate(valor.ToString()) Then
                    If DateDiff(DateInterval.Day, CDate(valor.ToString()), New Date(1900, 1, 1)) = 0 Then
                        Return ""
                    Else
                        Return FormatDateTime(m.NuloToDate(valor, Now), DateFormat.ShortDate)
                    End If
                Else
                    Return ""
                End If
            End If
        Else
            Return FormatDateTime(m.NuloToDate(valor, Now), DateFormat.ShortDate)
        End If
    End Function
    Public Function buscarIndexCampo(campo As String) As Integer
        If ht.ContainsKey(campo) Then
            Dim row As DataRow
            row = ht(campo)
            Return row("CDU_Ordem")
        Else
            Return -1
        End If
    End Function


    Public Function buscarCampoIndex(index As Integer) As String

        Dim row As DataRow
        Dim campo As String

        For Each campo In ht.Keys
            row = ht(campo)
            If row("CDU_Ordem") = index Then
                Return row("CDU_Campo")
            End If
        Next

    End Function


    Public Function formatarValorCampo(campo As String, valor As Object, Optional aplicarMultiplicador As Boolean = True, Optional dataValida As Boolean = False) As Object
        If campo Is Nothing Then
            Return ""
        End If
        If ht.ContainsKey(campo) Then
            Dim row As DataRow
            Dim tipocampo As String
            row = ht(campo)
            tipocampo = row("TIPOCAMPO")
            Return formatarCampo(tipocampo, campo, valor, aplicarMultiplicador, aplicarMultiplicador, dataValida)
        Else
            Return ""
        End If
    End Function

    Public Function validarCampo(campo As String, valor As Object) As String
        If campo Is Nothing Then
            Return "O campo não existe"
        End If


        If ht.ContainsKey(campo) Then
            Dim row As DataRow
            Dim tipocampo As String
            row = ht(campo)
            tipocampo = row("TIPOCAMPO")

            Select Case tipocampo
                Case "nvarchar" : Return "" 'IIf(validarValor(campo, valor), "", "O valor introduzido não existe!")
                Case "bit" : Return ""
                Case "float", "real" : Return IIf(IsNumeric(valor), "", "Tem de indicar um valor numérico válido!")
                Case "datetime" : Return IIf(IsDate(valor), "", "Tem de indicar uma data válida!")
                Case "smalldatetime" : Return IIf(IsDate(valor), "", "Tem de indicar uma data válida!")
            End Select

            Return ""

        Else
            Return "O campo não existe"
        End If
    End Function


    Public Function formatarCampo(campo As String, valor As Object) As Object
        If campo Is Nothing Then
            Return "O campo não existe"
        End If


        If ht.ContainsKey(campo) Then
            Dim row As DataRow
            Dim tipocampo As String
            row = ht(campo)
            tipocampo = row("TIPOCAMPO")

            Select Case tipocampo
                Case "nvarchar" : Return "" 'IIf(validarValor(campo, valor), "", "O valor introduzido não existe!")
                Case "bit" : Return ""
                Case "float", "real" : Return IIf(IsNumeric(valor), "", "Tem de indicar um valor numérico válido!")
                Case "datetime" : Return IIf(IsDate(valor), "", "Tem de indicar uma data válida!")
                Case "smalldatetime" : Return IIf(IsDate(valor), "", "Tem de indicar uma data válida!")
            End Select

            Return ""

        Else
            Return "O campo não existe"
        End If
    End Function


    Private Function validarValor(campo As String, valor As String) As Boolean
        Dim query As String
        query = buscarSubQuery(campo)
        If query <> "" Then
            Dim m As Motor
            Dim dt As DataTable
            m = Motor.GetInstance()
            dt = m.consultaDataTable("Select * FROM (" + query + ") SubQuery WHERE " + campo + "='" + valor + "'")
            If Not dt Is Nothing Then
                Return dt.Rows.Count > 0
            Else
                Return True
            End If
        Else
            Return True
        End If

    End Function
    Public Function NuloToString(ByVal obj) As String
        If IsDBNull(obj) Then
            NuloToString = ""
        Else
            NuloToString = CStr(obj)
        End If

    End Function

    Private Function buscarCor(grid As AxReportControl, campo As String, linha As Integer, valor As String, valorBase As String, cor As UInteger) As String

        If linha > 0 Then
            Dim dtRowAnt As DataRow
            dtRowAnt = grid.Records(linha - 1).Tag

            If NuloToString(dtRowAnt(campo)) <> valor Then
                Return System.Convert.ToUInt32(HexToDecimal(COR_ALTERACAO))
            Else
                Return cor
            End If
        Else
            If valorBase <> valor Then
                Return System.Convert.ToUInt32(HexToDecimal(COR_ALTERACAO))
            Else
                Return cor '"4294967295"
            End If

        End If
    End Function


    Public Function HexToDecimal(ByVal HexString As String) As Integer
        Dim HexColor As Char() = HexString.ToCharArray()
        Dim DecimalColor As Integer = 0
        Dim iLength As Integer = HexColor.Length - 1
        Dim iDecimalNumber As Integer

        Dim cHexValue As Char
        For Each cHexValue In HexColor
            If Char.IsNumber(cHexValue) Then
                iDecimalNumber = Integer.Parse(cHexValue.ToString())
            Else
                iDecimalNumber = Convert.ToInt32(cHexValue) - 55
            End If

            DecimalColor += iDecimalNumber * (Convert.ToInt32(Math.Pow(16, iLength)))
            iLength -= 1
        Next cHexValue
        Return DecimalColor
    End Function


    ''' <summary>
    ''' Função que permite calcular o total de Quantidade entregue
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function buscarQuantidadeEntregue(bd As String, id As String)
        Dim m As Motor
        m = Motor.GetInstance()
        Dim strqtd As String
        Dim qtd As Double
        strqtd = m.consultaValor("SELECT SUM(Quantidade) FROM " + bd + "..LinhasCompras WHERE IdLinhaOrigemCopia='" + id + "'")
        qtd = m.NuloToDouble(strqtd)
        If qtd < 0 Then qtd = qtd * -1
        Return qtd
    End Function


    ''' <summary>
    ''' Função que permite ir buscar as quantidades em falta 
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="quantOrig"></param>
    ''' <returns></returns>
    Public Function buscarQuantidadeFalta(bd As String, id As String, quantOrig As Double)
        Dim m As Motor
        m = Motor.GetInstance()
        Dim strqtd As String
        Dim qtd As Double
        strqtd = m.consultaValor("SELECT SUM(Quantidade) FROM " + bd + "..LinhasCompras WHERE IdLinhaOrigemCopia='" + id + "'")
        qtd = m.NuloToDouble(strqtd)

        If quantOrig < 0 Then quantOrig = quantOrig * -1
        If qtd < 0 Then qtd = qtd * -1

        qtd = quantOrig - qtd
        If qtd < 0 Then qtd = 0
        Return qtd

    End Function


End Class
