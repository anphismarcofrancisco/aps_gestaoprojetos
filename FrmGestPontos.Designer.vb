﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGestPontos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGestPontos))
        Me.lblPostos = New System.Windows.Forms.Label()
        Me.txtPesqCT = New AxXtremeSuiteControls.AxFlatEdit()
        Me.lblOperacao = New System.Windows.Forms.Label()
        Me.txtPesqOperacao = New AxXtremeSuiteControls.AxFlatEdit()
        Me.lstOperacao = New System.Windows.Forms.ListBox()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtPesqClientes = New AxXtremeSuiteControls.AxFlatEdit()
        Me.lstCliente = New System.Windows.Forms.ListBox()
        Me.txtVMolde = New AxXtremeSuiteControls.AxFlatEdit()
        Me.lblMCliente = New System.Windows.Forms.Label()
        Me.lblUML = New System.Windows.Forms.Label()
        Me.txtNumeroKG = New AxXtremeSuiteControls.AxFlatEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.gridAMRTJ = New AxXtremeReportControl.AxReportControl()
        Me.gridAMC = New AxXtremeReportControl.AxReportControl()
        Me.btnParar = New System.Windows.Forms.Button()
        Me.btnIniciar = New System.Windows.Forms.Button()
        Me.ckbUMC = New System.Windows.Forms.CheckBox()
        Me.ckbUMR = New System.Windows.Forms.CheckBox()
        Me.ckbUML = New System.Windows.Forms.CheckBox()
        Me.lstCentroTrab = New System.Windows.Forms.ListBox()
        Me.btnFechar = New System.Windows.Forms.Button()
        Me.btnRegistos = New System.Windows.Forms.Button()
        Me.txtVRequisicao = New AxXtremeSuiteControls.AxFlatEdit()
        Me.lblVRequisicao = New System.Windows.Forms.Label()
        CType(Me.txtPesqCT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqOperacao, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPesqClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVMolde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroKG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridAMRTJ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridAMC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVRequisicao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPostos
        '
        Me.lblPostos.AutoSize = True
        Me.lblPostos.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostos.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblPostos.Location = New System.Drawing.Point(358, 9)
        Me.lblPostos.Name = "lblPostos"
        Me.lblPostos.Size = New System.Drawing.Size(154, 25)
        Me.lblPostos.TabIndex = 16
        Me.lblPostos.Text = "Centro Trabalho"
        '
        'txtPesqCT
        '
        Me.txtPesqCT.Location = New System.Drawing.Point(360, 37)
        Me.txtPesqCT.Name = "txtPesqCT"
        Me.txtPesqCT.OcxState = CType(resources.GetObject("txtPesqCT.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqCT.Size = New System.Drawing.Size(168, 32)
        Me.txtPesqCT.TabIndex = 15
        '
        'lblOperacao
        '
        Me.lblOperacao.AutoSize = True
        Me.lblOperacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperacao.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblOperacao.Location = New System.Drawing.Point(358, 360)
        Me.lblOperacao.Name = "lblOperacao"
        Me.lblOperacao.Size = New System.Drawing.Size(99, 25)
        Me.lblOperacao.TabIndex = 19
        Me.lblOperacao.Text = "Operação"
        '
        'txtPesqOperacao
        '
        Me.txtPesqOperacao.Location = New System.Drawing.Point(360, 388)
        Me.txtPesqOperacao.Name = "txtPesqOperacao"
        Me.txtPesqOperacao.OcxState = CType(resources.GetObject("txtPesqOperacao.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqOperacao.Size = New System.Drawing.Size(165, 32)
        Me.txtPesqOperacao.TabIndex = 18
        '
        'lstOperacao
        '
        Me.lstOperacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOperacao.ForeColor = System.Drawing.Color.Black
        Me.lstOperacao.FormattingEnabled = True
        Me.lstOperacao.ItemHeight = 16
        Me.lstOperacao.Location = New System.Drawing.Point(360, 430)
        Me.lstOperacao.Name = "lstOperacao"
        Me.lstOperacao.Size = New System.Drawing.Size(165, 260)
        Me.lstOperacao.TabIndex = 17
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblCliente.Location = New System.Drawing.Point(7, 9)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(73, 25)
        Me.lblCliente.TabIndex = 22
        Me.lblCliente.Text = "Cliente"
        '
        'txtPesqClientes
        '
        Me.txtPesqClientes.Location = New System.Drawing.Point(12, 37)
        Me.txtPesqClientes.Name = "txtPesqClientes"
        Me.txtPesqClientes.OcxState = CType(resources.GetObject("txtPesqClientes.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtPesqClientes.Size = New System.Drawing.Size(337, 32)
        Me.txtPesqClientes.TabIndex = 21
        '
        'lstCliente
        '
        Me.lstCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstCliente.FormattingEnabled = True
        Me.lstCliente.ItemHeight = 16
        Me.lstCliente.Location = New System.Drawing.Point(12, 74)
        Me.lstCliente.Name = "lstCliente"
        Me.lstCliente.Size = New System.Drawing.Size(337, 612)
        Me.lstCliente.TabIndex = 20
        '
        'txtVMolde
        '
        Me.txtVMolde.Location = New System.Drawing.Point(534, 37)
        Me.txtVMolde.Name = "txtVMolde"
        Me.txtVMolde.OcxState = CType(resources.GetObject("txtVMolde.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtVMolde.Size = New System.Drawing.Size(159, 32)
        Me.txtVMolde.TabIndex = 23
        '
        'lblMCliente
        '
        Me.lblMCliente.AutoSize = True
        Me.lblMCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMCliente.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblMCliente.Location = New System.Drawing.Point(529, 9)
        Me.lblMCliente.Name = "lblMCliente"
        Me.lblMCliente.Size = New System.Drawing.Size(96, 25)
        Me.lblMCliente.TabIndex = 24
        Me.lblMCliente.Text = "V \ Molde"
        '
        'lblUML
        '
        Me.lblUML.AutoSize = True
        Me.lblUML.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUML.Location = New System.Drawing.Point(859, 76)
        Me.lblUML.Name = "lblUML"
        Me.lblUML.Size = New System.Drawing.Size(141, 13)
        Me.lblUML.TabIndex = 33
        Me.lblUML.Text = "Utiliza Material Limpeza"
        '
        'txtNumeroKG
        '
        Me.txtNumeroKG.Location = New System.Drawing.Point(924, 92)
        Me.txtNumeroKG.Name = "txtNumeroKG"
        Me.txtNumeroKG.OcxState = CType(resources.GetObject("txtNumeroKG.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtNumeroKG.Size = New System.Drawing.Size(70, 36)
        Me.txtNumeroKG.TabIndex = 34
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(752, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Utiliza Material Emp."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(578, 388)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 13)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "Utiliza Material Cliente"
        '
        'gridAMRTJ
        '
        Me.gridAMRTJ.Location = New System.Drawing.Point(534, 136)
        Me.gridAMRTJ.Name = "gridAMRTJ"
        Me.gridAMRTJ.OcxState = CType(resources.GetObject("gridAMRTJ.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridAMRTJ.Size = New System.Drawing.Size(462, 218)
        Me.gridAMRTJ.TabIndex = 52
        Me.gridAMRTJ.Tag = "0"
        '
        'gridAMC
        '
        Me.gridAMC.Location = New System.Drawing.Point(534, 419)
        Me.gridAMC.Name = "gridAMC"
        Me.gridAMC.OcxState = CType(resources.GetObject("gridAMC.OcxState"), System.Windows.Forms.AxHost.State)
        Me.gridAMC.Size = New System.Drawing.Size(462, 276)
        Me.gridAMC.TabIndex = 53
        Me.gridAMC.Tag = "0"
        '
        'btnParar
        '
        Me.btnParar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.PFinalizar
        Me.btnParar.Location = New System.Drawing.Point(777, 14)
        Me.btnParar.Name = "btnParar"
        Me.btnParar.Size = New System.Drawing.Size(59, 59)
        Me.btnParar.TabIndex = 55
        Me.btnParar.UseVisualStyleBackColor = True
        '
        'btnIniciar
        '
        Me.btnIniciar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Play1
        Me.btnIniciar.Location = New System.Drawing.Point(712, 14)
        Me.btnIniciar.Name = "btnIniciar"
        Me.btnIniciar.Size = New System.Drawing.Size(59, 59)
        Me.btnIniciar.TabIndex = 54
        Me.btnIniciar.UseVisualStyleBackColor = True
        '
        'ckbUMC
        '
        Me.ckbUMC.Appearance = System.Windows.Forms.Appearance.Button
        Me.ckbUMC.AutoSize = True
        Me.ckbUMC.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        Me.ckbUMC.Location = New System.Drawing.Point(534, 375)
        Me.ckbUMC.Name = "ckbUMC"
        Me.ckbUMC.Size = New System.Drawing.Size(38, 38)
        Me.ckbUMC.TabIndex = 36
        Me.ckbUMC.UseVisualStyleBackColor = True
        '
        'ckbUMR
        '
        Me.ckbUMR.Appearance = System.Windows.Forms.Appearance.Button
        Me.ckbUMR.AutoSize = True
        Me.ckbUMR.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        Me.ckbUMR.Location = New System.Drawing.Point(708, 92)
        Me.ckbUMR.Name = "ckbUMR"
        Me.ckbUMR.Size = New System.Drawing.Size(38, 38)
        Me.ckbUMR.TabIndex = 32
        Me.ckbUMR.UseVisualStyleBackColor = True
        '
        'ckbUML
        '
        Me.ckbUML.Appearance = System.Windows.Forms.Appearance.Button
        Me.ckbUML.AutoSize = True
        Me.ckbUML.Image = Global.APS_GestaoProjectos.My.Resources.Resources.nochecked1
        Me.ckbUML.Location = New System.Drawing.Point(876, 90)
        Me.ckbUML.Name = "ckbUML"
        Me.ckbUML.Size = New System.Drawing.Size(38, 38)
        Me.ckbUML.TabIndex = 31
        Me.ckbUML.UseVisualStyleBackColor = True
        '
        'lstCentroTrab
        '
        Me.lstCentroTrab.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstCentroTrab.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstCentroTrab.ForeColor = System.Drawing.Color.Black
        Me.lstCentroTrab.FormattingEnabled = True
        Me.lstCentroTrab.ItemHeight = 16
        Me.lstCentroTrab.Location = New System.Drawing.Point(361, 76)
        Me.lstCentroTrab.Name = "lstCentroTrab"
        Me.lstCentroTrab.Size = New System.Drawing.Size(165, 276)
        Me.lstCentroTrab.TabIndex = 56
        '
        'btnFechar
        '
        Me.btnFechar.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Fechar
        Me.btnFechar.Location = New System.Drawing.Point(941, 14)
        Me.btnFechar.Name = "btnFechar"
        Me.btnFechar.Size = New System.Drawing.Size(61, 61)
        Me.btnFechar.TabIndex = 57
        Me.btnFechar.UseVisualStyleBackColor = True
        '
        'btnRegistos
        '
        Me.btnRegistos.Image = Global.APS_GestaoProjectos.My.Resources.Resources.Registos
        Me.btnRegistos.Location = New System.Drawing.Point(876, 14)
        Me.btnRegistos.Name = "btnRegistos"
        Me.btnRegistos.Size = New System.Drawing.Size(59, 59)
        Me.btnRegistos.TabIndex = 58
        Me.btnRegistos.UseVisualStyleBackColor = True
        '
        'txtVRequisicao
        '
        Me.txtVRequisicao.Location = New System.Drawing.Point(534, 98)
        Me.txtVRequisicao.Name = "txtVRequisicao"
        Me.txtVRequisicao.OcxState = CType(resources.GetObject("txtVRequisicao.OcxState"), System.Windows.Forms.AxHost.State)
        Me.txtVRequisicao.Size = New System.Drawing.Size(159, 32)
        Me.txtVRequisicao.TabIndex = 59
        '
        'lblVRequisicao
        '
        Me.lblVRequisicao.AutoSize = True
        Me.lblVRequisicao.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVRequisicao.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblVRequisicao.Location = New System.Drawing.Point(534, 72)
        Me.lblVRequisicao.Name = "lblVRequisicao"
        Me.lblVRequisicao.Size = New System.Drawing.Size(138, 25)
        Me.lblVRequisicao.TabIndex = 60
        Me.lblVRequisicao.Text = "V \ Requisição"
        '
        'FrmGestPontos
        '
        Me.ClientSize = New System.Drawing.Size(1008, 702)
        Me.Controls.Add(Me.lblVRequisicao)
        Me.Controls.Add(Me.txtVRequisicao)
        Me.Controls.Add(Me.btnRegistos)
        Me.Controls.Add(Me.btnFechar)
        Me.Controls.Add(Me.lstCentroTrab)
        Me.Controls.Add(Me.btnParar)
        Me.Controls.Add(Me.btnIniciar)
        Me.Controls.Add(Me.gridAMC)
        Me.Controls.Add(Me.gridAMRTJ)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ckbUMC)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNumeroKG)
        Me.Controls.Add(Me.lblUML)
        Me.Controls.Add(Me.ckbUMR)
        Me.Controls.Add(Me.ckbUML)
        Me.Controls.Add(Me.lblMCliente)
        Me.Controls.Add(Me.txtVMolde)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.txtPesqClientes)
        Me.Controls.Add(Me.lstCliente)
        Me.Controls.Add(Me.lblOperacao)
        Me.Controls.Add(Me.txtPesqOperacao)
        Me.Controls.Add(Me.lstOperacao)
        Me.Controls.Add(Me.lblPostos)
        Me.Controls.Add(Me.txtPesqCT)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmGestPontos"
        Me.Text = "Registo Pontos"
        CType(Me.txtPesqCT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqOperacao, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPesqClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVMolde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroKG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridAMRTJ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridAMC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVRequisicao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPostos As System.Windows.Forms.Label
    Friend WithEvents txtPesqCT As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lblOperacao As System.Windows.Forms.Label
    Friend WithEvents txtPesqOperacao As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lstOperacao As System.Windows.Forms.ListBox
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtPesqClientes As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lstCliente As System.Windows.Forms.ListBox
    Friend WithEvents txtVMolde As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lblMCliente As System.Windows.Forms.Label
    Friend WithEvents ckbUML As System.Windows.Forms.CheckBox
    Friend WithEvents ckbUMR As System.Windows.Forms.CheckBox
    Friend WithEvents lblUML As System.Windows.Forms.Label
    Friend WithEvents txtNumeroKG As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ckbUMC As System.Windows.Forms.CheckBox
    Friend WithEvents gridAMRTJ As AxXtremeReportControl.AxReportControl
    Friend WithEvents gridAMC As AxXtremeReportControl.AxReportControl
    Friend WithEvents btnParar As System.Windows.Forms.Button
    Friend WithEvents btnIniciar As System.Windows.Forms.Button
    Friend WithEvents lstCentroTrab As System.Windows.Forms.ListBox
    Friend WithEvents btnFechar As System.Windows.Forms.Button
    Friend WithEvents btnRegistos As System.Windows.Forms.Button
    Friend WithEvents txtVRequisicao As AxXtremeSuiteControls.AxFlatEdit
    Friend WithEvents lblVRequisicao As System.Windows.Forms.Label
End Class
